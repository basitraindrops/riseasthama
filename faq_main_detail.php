<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')=="" && $sessionObj->read("front_user_id")=="" && $sessionObj->read("front_login_as")=="" )
{
    header("Location:page_login.php");
}


require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'FAQ_DETAIL';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <!-- <link href="../assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />


    <!--  Light Bootstrap Dashboard core CSS    -->
    <!-- <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>assets/css/faq.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!-- <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style type="text/css">
    
    </style>
    <?php require('common/header.php'); ?>
</head>

<body>
<?php require("common/sidebar.php"); ?>

  <?php 
       require(CLASS_DIR."faq.class.php");
       $pb=new faq($mysqli);
       $f_id=$_REQUEST['id'];
       $data=$pb->faq_main_detail($f_id);
       $id=$data[0]->faq_id;
  ?>   
    <div class="wrapper" id="wrapper">


        <div class="main-panel" id="main">

           <nav class="navbar navbar-default">
          <div class="container-fluid">

          <div class="navbar-header">

            <div class=" col-md-1 navbar-minimize">

                <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
            <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
            <i class="fa fa-navicon visible-on-sidebar-mini"></i>
          </button>
            </div>
            <div class="col-md-9 nav-container">
                <ul class="nav nav-icons" role="tablist">
                    <li>
                        <a href="#fullchapter" role="tab" data-toggle="tab">
                                       
                                       Full chapter
                                    </a>
                    </li>
                    <li>
                        <a href="#textsummary" role="tab" data-toggle="tab">
                                   
                                        Text summary
                                    </a>
                    </li>
                    <li class="active">
                        <a href="#faq_detail" role="tab" data-toggle="tab" id="faq">
                                   
                                        FAQ 
                                    </a>
                    </li>


                </ul>
            </div>
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
            <div class="col md-2 collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <?php require("common/notifications.php"); ?>
                    <?php require("common/topsettings.php"); ?>


                </ul>

             </div>
          </div>

        </div>

      </nav>

           <?php 
               
                require_once(CLASS_DIR."faq_topic.class.php");
                $pb1=new faq_topic($mysqli);
                $d=$pb1->faq_detail_topic1($id);
                //echo "<pre>";print_r($d);exit;
           ?>

           <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="tab-content">
                        <div class="tab-pane active" id="faq_detail">
                          <?php require("faq_main.php"); ?>
                        </div>

                          <?php if($id==1){
                              require("method/thestory/storycontent.php"); 
                          }
                          else if($id==2){
                              require("method/introduction/fullchapter.php"); 
                              require("method/introduction/textsummary.php"); 
                          }
                          else if($id==3){
                              require("method/vitamind/fullchapter.php"); 
                              require("method/vitamind/textsummary.php"); 
                          }
                          else if($id==4){
                              require("method/omega3/fullchapter.php"); 
                              require("method/omega3/textsummary.php"); 
                          }
                          else if($id==5){
                              require("method/eliminategutpressure/fullchapter.php"); 
                              require("method/eliminategutpressure/textsummary.php"); 
                          }
                          else if($id==6){
                              require("method/homeostasis/fullchapter.php"); 
                              require("method/homeostasis/textsummary.php"); 
                          }
                          else if($id==7){
                              require("method/stress/fullchapter.php"); 
                              require("method/stress/textsummary.php"); 
                          }
                          else {
                              require("method/summary/summarycontent.php"); 
                           
                          } ?>
                         
            <!-- container fluid    -->

              </div>
            </div>
             </div>
            </div>

        </div>
    </div>


</body>  

<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>



</html>
