 <?php 
 include 'inc/config.php'; 
require(INC_DIR.'init.php');

require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

require(dirname(__FILE__) . '/init.php');
?>
<!DOCTYPE html>
 <html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>
  <meta name="HandheldFriendly" content="true" />
  <meta name="apple-mobile-web-app-capable" content="YES" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>assets/css/page.css" media="screen">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
</head>
<body>
 <section class="top_headersection">
  <div class="wrapper">
    <div class="main_div">
      <div class="left_contain">
        <div class="review_img">
           <a href="https://www.riseasthma.com/">
            <img class="site_image" src="<?php echo SITE_URL; ?>assets/img/logo2.png">
          </a>
        </div>

      <p class="review_header">120+ reviews from real people with real results</p>
      <p class="review_desc">Instant view the step by step videos, read text summaries, see FAQs and access live support for you to get better fast</p>
      <p class="review_main_price">$70</p>
      <p class="review_sub_price"> $49</p>
      <p class="discount">TODAY 30% OFF</p>
      <p class="last_p">A no-hassle <b>100% Money Back Guarantee</b> for 360 days.</p>
      <div class="left_contain_icon">
        <ul class="left_icon">
          <li><img src="<?php echo SITE_URL; ?>assets/img/index.png" alt=""></li>
          <li><img src="<?php echo SITE_URL; ?>assets/img/index1.png" alt=""></li>
          <li><img src="<?php echo SITE_URL; ?>assets/img/index2.jpeg" alt=""></li>
          <li><img src="<?php echo SITE_URL; ?>assets/img/index2.png" alt=""></li>
          <li><img src="<?php echo SITE_URL; ?>assets/img/index3.png" alt=""></li>
        </ul>
      </div>
      </div>
      <div class="right_contain">
      <form method="post" action="insert_data.php" class="register_detail" id="payment-form">
          <div class="your_detail">
              <p class="user_detail">Your details</p>
              <input id="user_name" name="user_name" placeholder="Name" class="form_user_name" type="text" >
             <label id="name-error" class="error" for="email"></label>
             <input id="user_email" name="user_email" placeholder="Email" class="form_user_email" type="email" >
             <label id="email-error" class="error" for="email"></label>
          </div>
          <div class="payment_main">
            <div class="payment_sub">
                <p class="Payment_detail">Secure Payment</p>
                <div class="card_img">
                  <img src="<?php echo SITE_URL; ?>assets/img/card.png" alt="">
                </div>
                <div class="credit_card">
                <input id="stripe_number" name="stripe_number" placeholder="Card number"  class=" form_card_number" type="text"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onfocus="" onkeypress=''> 
                </div>
                <div class="credit_card1">
                <div class="date">
                <!-- <input id="stripe_expirydate" name="stripe_expirydate" placeholder="MM/YY" class="form_tripe_expirydate" type="text" onkeypress='return validateExpire(event);'> -->
                 <input onkeypress="return validateExpire(event);" id="expirydate" maxlength="5" class="form_tripe_expirydate" placeholder="MM/YY" name="stripe_expirydate" spellcheck="false" autocapitalize="off" autocorrect="off" autocomplete="off" old_val="2" type="text">
                </div>
                <div class="cvc">
                <input id="stripe_cvc" name="stripe_cvc" placeholder="CVV" class="form_stripe_cvc" onkeypress="return validateExpire(event);" maxlength="3" minlength="3" type="text">
                </div>
                </div>
                 <div id="card-errors1" class="">
                 <label id="credit_card-error" class="error col-sm-12" for="credit_card"></label>
                 <label id="expirydate-error" class="error col-sm-12" for="credit_card"></label>
                  <label id="cvv-error" class="error col-sm-12" for="credit_card"></label>
                 </div>     
            </div>
            <div class="payment_btn">
            <button class="btn submit_button" >Access RISE now</button>
            </div>
          </div>
      </form>
      </div>
    </div>
  </div>
 </section>

 <section class="testimonial">
  <div class="wrapper">
  <div class="q_ans_header">
    <div class="middle_contain">
      <p class="middle_contain_desc">"RISE was easy to learn and follow as all its items actually make sense. I can finally breath without an inhaler after 10 years of heavy medication. Thanks a lot!" </p>
    </div>
    <div class="middle_contain_image">
      <img src="<?php echo SITE_URL; ?>assets/img/19060656-0-t2-03.png" style="height: 100%; width: 100%; margin-top: 0px;" alt="">
    </div>
    <div class="middle_contain_person">
      <b class="person_name">Pontus Bergmark, Sweden</b>
    </div>
  </div>
 </div> 
 </section>
 <section class="q_ans">
 <div class="wrapper">
   <div class="page_contain">
      <div class="page_contain_title">
         <p class="page_title">RISE is a natural asthma-free method your doctor will approve</p>
      </div>
      <div class="page_left">
         <p class="left_que">What is RISE?</p>
          <p>RISE is a drug-free, common sense 
            method designed to help people that are struggling with allergies and 
            asthma. It will reduce the frequency, intensity, and duration of your 
            asthma and allergy symptoms.
          </p>
          <p>RISE is also about hope. We understand how difficult it
            is to cope with being unable to breathe. Our goal is to help you add 
            color back to your life and get the freedom to live the way you want.</p>
      </div>
      <div class="page_left">
          <p class="left_que">What RISE is NOT?</p>
          <p>RISE is not an alternative to medical treatment. It is a complementary approach to reduce your asthma and allergy symptoms.
          </p>
          <p>RISE is not a cure for asthma. We’ll discuss this topic later in the FAQ section.
          </p>
          <p>RISE is not a “secret formula.” It doesn’t sell 
            products (no snake oil or magic beans). It is plain and honest advice 
            built by people just like you; people who have dealt with asthma for 
            most of their lives, but found a way out of it.</p>
      </div>
       <div class="page_left">
          <p class="left_que">Will RISE cure my asthma?</p>
          <p>"Cure" is a very pretentious word. 
            Most of the doctors and asthma experts would disregard any treatment, 
            program, and method that can "cure" asthma. And so do we.
          </p>
          <p>The difference though, is that the doctors will tell 
            you can control asthma with proper medical treatment. RISE is not about 
            controlling asthma. It is about thriving in spite of it. It is about 
            getting the freedom to transform your life. It is about looking back and
            thinking of asthma as a bad distant memory.
          </p>
      </div>
        <div class="page_left">
          <p class="left_que">Are there any guarantees that the method will help me?</p>
          <p>"Cure" is a very pretentious word. 
            Most of the doctors and asthma experts would disregard any treatment, 
            program, and method that can "cure" asthma. And so do we.
          </p>
          <p>We can ensure that, should you 
            follow the steps in our process, you will improve your symptoms. 
            Nevertheless, people are different, and asthma is a complex disease. Not
            everyone will experience the same progress in the same time frame. 
            However, we know you will get better!
          </p>
      </div>
   </div>
 </div>
 </section>  
<section class="footer">
 <div class="wrapper">
  <div class="footer_icon">
   <ul class="link_icon">
               <li><a href="https://www.riseasthma.com/" class="url-link">HOME</a></li>
               <li><a href="https://www.riseasthma.com/how-i-defeated-asthma-after-24years" class="url-link">THE STORY BEHIND</a></li>
               <li><a href="https://www.riseasthma.com/the-origins-and-causes-of-asthma" class="url-link">METHOD</a></li>
               <li><a href="https://www.riseasthma.com/blog" class="url-link">BLOG</a></li>
   </ul>
  </div>
  <div class="social_icon">
  <ul class="social_image">
     <li> <a href="https://www.facebook.com/RISEasthma/">
            <img src="<?php echo SITE_URL; ?>assets/img/19061041-0-f.png"  alt="">
     </a></li>
   <li> <a href="https://twitter.com/RISEAsthma">
      <img src="<?php echo SITE_URL; ?>assets/img/19061051-0-tw.png" alt="">
    </a></li>
    <li><a href="https://www.linkedin.com/company-beta/13250233/">
            <img src="<?php echo SITE_URL; ?>assets/img/19061046-0-lk.png"  alt="">
    
    </a></li>
    </ul>
  </div>
  <div class="footer_contain">
   <p>DISCLAIMER:
 RISE is not a medical program and it is not meant to replace your 
current medical treatment. Ask for you doctor's advice before starting 
RISE.</p>
  </div>
  <div class="copyrights">
   <p>© RISE Asthma, All rights reserved.</p>
   <a href="https://www.riseasthma.com/" style="width: 100%; height: 100%;">
    <img src="<?php echo SITE_URL; ?>assets/img/logo2.png" style="height: 100%; width: 100%; margin-top: 0px;" alt="">
   </a>
  </div>

 </div>
</section>
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://js.stripe.com/v2/"></script>

       
<script type="text/javascript">

function validateExpire(event) {
            //var key = window.event ? event.keyCode : event.which;
            var key = event.keyCode || event.which;
            if (event.keyCode == 8 || event.keyCode == 46 ||
                event.keyCode == 37 || event.keyCode == 39) {
                return true;
            } else if (key > 57) {
                return false;
            } else return true;
        }

  function validateNumber(number) {

            error = true
            number = number.trim();
            var count = number.length;
           
            if (count == 19) {
                sp = number.split(" ").length - 1;
                if (sp == 3) {
                    error = false;
                }
            }
            if (count == 16) {
                sp = number.split(" ").length - 1;
                if (sp == 0) {
                    error = false;
                }
            }

            if (error) {
                $("#credit_card-error").html('Please Enter Valid Card Number!');
                $("#credit_card-error").show();
            } else {
                $("#credit_card-error").html('');
                $("#credit_card-error").hide();
            }

            return error;
        }

        function validateExpiry(expiry) {
            error = false
            res = '';
            count = expiry.length;

            if (count == 5) {
                sp = expiry.split("/").length - 1;
                if (sp != 1) {
                    error = true;
                    res = ' lenght ';
                }
            } else {
                error = true;
                res = ' lenght2 ';
            }
            if (!error) {

                var $split = expiry.split("/");

                if ($split[0].length != 2 || $split[0] > 12) {
                    error = true;
                    res = ' lenght3 ';
                }

                if (!error) {
                    var year = new Date().getFullYear().toString().substr(-2);
                    if ($split[1] < year) {
                        error = true;

                        res = ' lenght5 ';
                    }
                }
            }
            if (error) {
                $("#expirydate-error").html('Please enter valid Expiry Date!');
                $("#expirydate-error").show();
            } else {
                $("#expirydate-error").html('');
                $("#expirydate-error").hide();
            }

            return error;
        }

        function validateCVV(cvv) {

            error = false

            count = cvv.length;
            if (count != 3) {
                error = true
            }

            if (error) {
                $("#cvv-error").html('Please enter valid CVV !');
                $("#cvv-error").show();
            } else {
                $("#cvv-error").html('');
                $("#cvv-error").hide();
            }
            return error;

        }

        $( "#user_email" ).change(function() {
          var email = $("#payment-form :input[name='user_email']").val();
          var name = $("#payment-form :input[name='user_name']").val();
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if(!re.test(email))
          {
              alert("Enter valid email address!");
              $("#user_email").val('');
              $("#email-error").html('Enter valid email address!');
              $("#email-error").show();
              document.getElementById("user_email").focus();
          }
         
          $.ajax({
            type: "POST",
            dataType: "html",
            url: "emailcheck.php",
            data: {
              email_val: email
             },
            success: function(result) { 
        
            if(result == 'false')
            {
                alert("You have already register with this address!");
                $("#user_email").val('');
                $("#email-error").html('You have already register with this address!');
                document.getElementById("user_email").focus();

                $("#email-error").show();
                $("#payment-form").show();
                return false;
            }
            else
            {
                $("#email-error").html('');
                $("#email-error").hide();
            }
            return false;
            }

          });
        });
         $( "#user_name" ).change(function() {
         
          var name = $("#payment-form :input[name='user_name']").val();
         if(name != '')
          {
            $("#name-error").html('');
            $("#name-error").hide();
           }

          
        });
          
jQuery(function($) {
       // Stripe.setPublishableKey('<?php //echo STRIPE_PUBLIC_KEY;?>');
         Stripe.setPublishableKey('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
            $('#payment-form').submit(function(e) {
                if ($('#payment-form').attr('action') == 'insert_data.php') {
                  var $form = $(this);
                  var expiry = $("#payment-form :input[name='stripe_expirydate']").val()
                    var stripe = expiry.split("/");
                    var cvv = $("#payment-form :input[name='stripe_cvc']").val();
                    error = false;

                    number = $("#payment-form :input[name='stripe_number']").val();

                    var name = $("#payment-form :input[name='user_name']").val();
                    var email = $("#payment-form :input[name='user_email']").val();


                    if(name == '')
                    {
                     //alert("Please enter your name");
                    $("#name-error").html('Please enter your name');
                    $("#name-error").show();
                     }
                    
                    if(email == '')
                    {
                       $("#email-error").html('Please enter your email address');
                       $("#email-error").show();
                    }
                    if(email != '')
                    {
                       $("#email-error").html('');
                       $("#email-error").show();
                    }

                   errorNumber = validateNumber(number);
                    errorExpiry = validateExpiry(expiry);
                    errorCvv = validateCVV(cvv);


                    if (errorNumber || errorExpiry || errorCvv) {
                        $("#card-errors1").show();
                        return false
                    } else {

                  $form.find('.btn').prop('disabled', true);
          
                  // Request a token from Stripe:
                      Stripe.card.createToken({
                            number: $("#payment-form :input[name='stripe_number']").val(), //values['stripe_number'],
                            cvc: $("#payment-form :input[name='stripe_cvc']").val(),
                            exp_month: stripe[0],
                            exp_year: stripe[1]
                        }, stripeResponseHandler);
                  // Prevent the form from being submitted:
                  return false;
                }
               }
            });
        });
    function stripeResponseHandler(status, response) {
      // Grab the form:
      var $form = $('#payment-form');
      if (response.error) { // Problem
      // Show the errors on the form:
        $("#card-errors1").show();

                $("#credit_card-error").html('Please Enter Correct Detail!');
                $("#credit_card-error").show();
      $form.find('.btn').prop('disabled', false); // Re-enable submission
  
        } else { // Token was created!
      
      // Get the token ID:
      var token = response.id;

      // Insert the token ID into the form so it gets submitted to the server:
      $form.append($('<input type="hidden" name="stripeToken">').val(token));
  
      // Submit the form:
      $form.get(0).submit();
      }
  };

    $('document').ready(function() {
           


            $(document).on('blur', "#payment-form :input[name='stripe_expirydate']", function(e) {
                validateExpiry($(this).val())
            })
            $(document).on('keyup', "#payment-form :input[name='stripe_expirydate']", function(e) {

                oldVal = $(this).attr('old_val');

                oldCount = oldVal.length;
                var count = $(this).val().length;

                if (oldCount > count) {
                    $(this).attr('old_val', $(this).val());
                    return
                }


                val = $(this).val().replace(/[^0-9/]/g, '');

                if (!($(this).val() == val)) {
                    $(this).val(val)
                    $(this).attr('old_val', val);
                    return
                }

                if (count > 5) {
                    $(this).attr('old_val', $(this).val().slice(0, 5));
                    $(this).focus().val($(this).val().slice(0, 5))
                }


                if ((parseInt($(this).val()) > 12 || $(this).val() == 00) && count == 2) {
                    $("#card-errors").show();
                    $("#expirydate-error").html('Please Enter Valid Month');
                    $("#expirydate-error").show();

                    $(this).attr('old_val', $(this).val());
                    $(this).focus()
                } else if (count == 2) { // && e.keyCode != 8){

                    if ($(this).val().indexOf('/') < 0) {
                        $(this).attr('old_val', $(this).val() + "/");

                        $(this).focus().val(function(index, value) {
                            return value + "/";
                        });
                    } else {
                        $(this).attr('old_val', "0" + $(this).val());
                        $(this).focus().val(function(index, value) {
                            return "0" + value;
                        });
                    }
                    $("#expirydate-error").html('');
                    $("#expirydate-error").hide();
                } else {
                    $(this).attr('old_val', $(this).val());

                    if ($(this).val().indexOf('/')) {
                        var $split = $(this).val().split("/");
                        var year = new Date().getFullYear().toString().substr(-2);

                        if (count == 5 && $split[1] < year) {
                            $(this).focus()
                            $("#card-errors").show();
                            $("#expirydate-error").html('Please Enter Valid Year');
                            $("#expirydate-error").show();
                        } else {
                            $("#expirydate").html('');
                            $("#expirydate-error").hide()
                        }
                    }
                }

            });



            $(document).on('blur', "#payment-form :input[name='stripe_number']", function(e) {
               
                $(this).val($(this).val().trim())
                validateNumber($(this).val())
            })
            $(document).on('keyup', "#payment-form :input[name='stripe_number']", function(e) {

                var count = $(this).val().length;
                var key = e.keyCode || e.which;

                aa = $(this).val();
                val = aa.replace(/[^0-9 ]/g, '');
                count2 = val.length;
                /*if(count2 > 16){*/
                val = val.slice(0, 19)
                //}
                count2 = val.length;


                if (aa !== val) {
                    
                    valTmp = $(this).val().replace(/[^0-9]/g, '');
                    c3 = valTmp.length;
                    if (c3 > 16) {
                        valTmp = valTmp.slice(0, 16)
                        $(this).val(function(index, value) {
                            valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            valTmp = valTmp.trim();
                            return valTmp;
                        });
                    } else {
                        return $(this).val(val);
                    }
                }
                if (count2 < 19) {
                        
                    if (count2 % 5 == 0) {
                        $(this).val(function(index, value) {
                            return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            val = val.trim();
                            return val;
                        });
                    }
                }
            });

            $(document).on('keyup', "#payment-form :input[name='stripe_cvc']", function(e) {



                var count = $(this).val().length;
                val = $(this).val().replace(/[^0-9/]/g, '');

                if (!($(this).val() == val)) {
                    $(this).val(val)
                    return
                }

                if (count > 3) {
                    $(this).focus().val($(this).val().slice(0, 3))
                }

            })
            $(document).on('blur', "#payment-form :input[name='stripe_cvc']", function(e) {
                validateCVV($(this).val())
            })

        });


</script>
</body>
</html>