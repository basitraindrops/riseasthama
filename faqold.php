<?php 
ini_set("display_errors", "1");
  error_reporting(E_ALL);
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')=="" && $sessionObj->read("front_user_id")=="" && $sessionObj->read("front_login_as")=="" )
{
    header("Location:page_login.php");
}


require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'FAQ';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <!-- <link href="../assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href=".<?php echo SITE_URL; ?>/assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>/assets/css/faq.css" rel="stylesheet">

    <!--  Light Bootstrap Dashboard core CSS    -->
    <!-- <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
    <link href="<?php echo SITE_URL; ?>assets/css/style.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="<?php echo SECURE ?>://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='<?php echo SECURE ?>://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!-- <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    <style type="text/css">
    
    
    </style>
</head>

<body>

    <?php require("common/sidebar.php"); ?>
  
    <div class="wrapper" id="wrapper">

      <div class="main-panel" id="main">

             <?php require("common/top_new.php"); ?>
          

       <div class="content" >
          <div class="faq_search">
            <i class="fa fa-search" aria-hidden="true"></i>
            <input type="text" name="search" id="txt_search" placeholder="Search.......">
          </div>
         <div class="faq_main" id="default">
              <?php 
              require(CLASS_DIR."faq.class.php");
                $pb=new faq($mysqli);
                $data=$pb->faq_cat_detail(); 
                 //echo "<pre>";print_r($data);exit; ?> 

             <div style="width:100%;">
              <?php for($i=0; $i<count($data); $i++) { ?>
                <div class="block_faq">
                  <div class="img_block">
                  <i <?php if($data[$i]->faq_cat_id==1){ ?> class="pe-7s-map" <?php } else if($data[$i]->faq_cat_id==2){ ?> class="pe-7s-map" <?php } else if($data[$i]->faq_cat_id==3){ ?> class="pe-7s-sun" <?php } else if($data[$i]->faq_cat_id==4){ ?> class="pe-7s-graph" <?php } else if($data[$i]->faq_cat_id==5){?> class="pe-7s-leaf" <?php } else if($data[$i]->faq_cat_id==6){ ?> class="pe-7s-loop" <?php }  else if($data[$i]->faq_cat_id==7){ ?> class="pe-7s-smile" <?php } else{?> class="pe-7s-date" <?php } ?>></i>
                  <a href="faq_detail.php?id=<?php echo $data[$i]->faq_cat_id; ?>"> 
                       <h4 style="text-align:center"><?php echo $data[$i]->faq_cat_name; ?></h4>
                  </a>
                  <p style="font-size:12px;text-align:center"><?php echo $data[$i]->faq_cat_des; ?></p>
                  </div>
                  <div class="faq_update">
                      <img src="img/profile_user.jpg" class="img-circle" alt="Cinque Terre" width="25" height="25" style="display: inline-block;vertical-align: middle;">
                      <p style="font-size:10px;display: inline-block;vertical-align: middle;">Updated 13 days ago by Crislan Andrel</p>
                  </div>
                </div>
                <?php }?> 
            </div>
            </div>
        </div>
            <!-- container fluid    -->

            <?php //require("common/footer.php"); ?>

        </div>
    </div>

</body>  
<script>
$('#txt_search').change(function(){
   var txt = document.getElementById('txt_search').value;
   
    $.ajax({
        type: "POST",
        dataType:"json",
        url: "search.php",
       data: { txt_val:txt
       },
       success: function(result) {
        var htmlText = '';
        	htmlText += '<div style="width:100%;">';
        if(result == "" || result == 'undefined' || result == undefined){
        htmlText += '<p>No result found!</p>';
         } else {
          for ( var key in result ) {
          htmlText += ' <div class="block_faq"><div class="img_block">';
           if(result[key].id ==1){ htmlText += '<i class="pe-7s-map"></i>'; }else if(result[key].id ==2){ htmlText += '<i class="pe-7s-map"></i>'; }else if(result[key].id ==3){ htmlText += '<i class="pe-7s-sun"></i>';}else if(result[key].id ==4){ htmlText += '<i class="pe-7s-graph"></i>';}else if(result[key].id ==5){ htmlText += '<i class="pe-7s-leaf"></i>';}else if(result[key].id ==6){ htmlText += '<i class="pe-7s-loop"></i>';}else{ htmlText += '<i class="pe-7s-date"></i>';}
          htmlText += ' <a href="faq_detail.php?id='+result[key].id+'"> <h4 style="text-align:center"> ' + result[key].name + '</h4></a>';
          htmlText += '<p style="font-size:12px;text-align:center"> ' + result[key].des + '</p> </div><div class="faq_update"><img src="img/profile_user.jpg" class="img-circle" alt="Cinque Terre" width="25" height="25" style="display: inline-block;vertical-align: middle;"><p style="font-size:10px;display: inline-block;vertical-align: middle;">Updated 13 days ago by Crislan Andrel</p> </div>';
          htmlText += '</div>';       
          }
        }
        htmlText += '</div>';
        htmlText += '</div>';
        htmlText += '</div>';
        // $('#default').html('');
        $('#default').html(htmlText);
      },
     
   });
  });

</script>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>



</html>
