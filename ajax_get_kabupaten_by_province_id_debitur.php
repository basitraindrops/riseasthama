<?php
include getcwd()."/inc/config.php"; // Configuration php file
require(INC_DIR.'init.php');
require(CLASS_DIR.'user.class.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

if(isset($_POST['province_id']))
{
	require_once(CLASS_DIR."kabupaten.class.php");
	$kab=new kabupaten($mysqli);
	$data=$kab->get_by_province_id($_POST['province_id']);
	echo '<select name="debitur_kota" id="debitur_kota">';
	for($i=0;$i<count($data);$i++)
	{
		echo '<option value="'.$data[$i]->kabupaten_id.'">'.$data[$i]->kabupaten_name.'</option>';
	}
	echo '</select>';
}
?>