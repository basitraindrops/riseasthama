<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>Reduce Inflammation with Vitamin D</h1>
                <p>In this&nbsp; <div class="tooltip_text"> chapter <span id="tooltip_content"><h2 class="demo_left">Vitamin D Suppliments</h2><img src="https://asthmamethod.com/assets/img/vitamind/1.png" class="demo_right">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s </span> </div> we are going to explore what exactly Vitamin D is, how is it produced, and why it is critical in dealing with asthma and allergies. We are also going to understand Vitamin D deficiency and how to restore it to reach optimal levels and improve our health.</p>
            </div>
            <div class="video-container">

                <iframe id="vzvd-9890690" name="vzvd-1152805" title="vzaar video player" type="text/html" frameborder="0" allowFullScreen allowTransparency="true" mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/9890690/player"></iframe>

            </div>

            <div class="nuttshell">
                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p>We have evolved using Vitamin D as a cornerstone of our health, regulating almost 3,000 genes in us and playing a vital role in maintaining the proper functioning of the primary systems of our body.</p>
                    <p>There are a variety of scientific studies showing the benefits of Vitamin D on our general health, as well as in treating asthma and allergies. </p>
                    <p>It is estimated that more than one-third of the global population nowadays is Vitamin D deficient, caused by getting minimal sun exposure given our modern lifestyle. The optimal Vitamin D blood levels in humans are around 55 ng/ml. To reach these levels, it is recommended to: </p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Take Vitamin D supplements</h3>

                        <ul class="general-list">
                            <li>Children - 1,000 IU of Vitamin D a day for every 25 lbs of their weight, rounded up. </li>
                            <li>Adults - 5,000 IU of Vitamin D a day; People also go up to 10,000 IU of vitamin D a day which is unlikely to do any harm.</li>
                            <li>Pregnant mothers - 6,000 IU of Vitamin D a day. </li>

                        </ul>
                    </div>
                </div>

                <div class="do2">
                    <div class="dodont-content">

                        <h3>Expose yourself to sun frequently but with moderation</h3>

                        <ul class="general-list">
                            <li>Be careful not to get a sunburn. Start low and then gradually increase the duration of sun exposure according to your skin tolerance. A good indicator is to remain exposed for half the time required for your skin to turn pink. Then, cover up with clothing and go into the shade.</li>
                            <li>Don’t apply any sunscreen while exposing to the sun.</li>
                            <li>The best time to sunbath is in the afternoon, between 1-5 PM. Try to avoid the intense sun at noon.</li>
                            <li>People with dark skin need about 5-6 times the duration of sun exposure compared in order to fair-skinned individuals to synthesize the same amount of Vitamin D.</li>
                            <li>Combine sun exposure with exercise and other stress management techniques that will help treat your asthma - see <a href="/managestress.php">Chapter 4 - Manage Stress</a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <div class="content content-full-width">
                <h2>Earth’s Energy Flow</h2>
                <p>I ran my first mountain marathon shortly after I started to recover from grappling with asthma and allergies. It was a bright day in Transylvania, as the sun lay gently over the beautiful Carpathian Mountains. </p>
                <p>One hour into the race, I took my shirt off to enjoy the high altitude breeze. I continued running like this for the next four hours.</p>
                <p>At the end, my body was undergoing peculiar, contradictory sensations. On the one hand, I was exhausted from the effort. On the other hand, I felt an unusual flow of energy inside me - like every cell in my anatomy was active. I felt alive and healthy.</p>
                <p>Later, I was realized it had been right to feel that way; my awareness was caused, amongst other things, by Vitamin D circulating throughout my body.</p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/1.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="2">

            <div class="content content-full-width">
                <h2>Vitamin D - A Cornerstone of our Body</h2>

                <p><span style="font-weight: 400;">The story of Vitamin D - or the sunshine vitamin - is a tale as old as life itself. &nbsp;</span></p>
                <p><span style="font-weight: 400;">We are living in a complex ecosystem that interconnects all the forms of being through an energy flow that starts with solar energy. Then, the plants convert it into chemical energy through photosynthesis. Further, it is passed through the food chain to animals and humans.</span></p>
                <p><span style="font-weight: 400;">Humans are also designed - through evolution - to process sunlight through various systems. In our species, the UVB rays from the sun strike our skin and set off a chemical reaction pre-programmed in our DNA.</span></p>
                <p><span style="font-weight: 400;">In its precise form, Vitamin D is a secosteroid hormone. After produced, it enters the bloodstream until it reaches our vital organs where it plays a determined role in the proper functioning of the primary structures of our body </span><a href="https://paperpile.com/c/zC8saG/JD7h"><span style="font-weight: 400;">[1]</span></a><span style="font-weight: 400;">. It regulates the immune system </span><a href="https://paperpile.com/c/zC8saG/SHZN"><span style="font-weight: 400;">[2]</span></a><span style="font-weight: 400;">, digestion </span><a href="https://paperpile.com/c/zC8saG/ZlkY"><span style="font-weight: 400;">[3]</span></a><span style="font-weight: 400;">, cardiovascular </span><a href="https://paperpile.com/c/zC8saG/zSPT"><span style="font-weight: 400;">[4]</span></a> <span style="font-weight: 400;">and musculoskeletal systems </span><a href="https://paperpile.com/c/zC8saG/qCSL"><span style="font-weight: 400;">[5]</span></a><span style="font-weight: 400;"> and insulin metabolism </span><a href="https://paperpile.com/c/zC8saG/IPkh"><span style="font-weight: 400;">[6]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">That day in the mountains, I combined a long sustained effort with sun exposure.I had a glance into the primary </span><span style="font-weight: 400;">daily activity</span><span style="font-weight: 400;"> of the ancestral humans. I felt that every part of me was operating in harmony. But why?</span></p>
                <p><span style="font-weight: 400;">There is evidence that almost 3,000 genes in our body have Vitamin D receptors that upregulate them </span><a href="https://paperpile.com/c/zC8saG/WaX5"><span style="font-weight: 400;">[7]</span></a><span style="font-weight: 400;">. The bigger picture starts to unveil when we estimate that the human anatomy contains, in total, 25,000 genes. </span><span style="font-weight: 400;"><a href="https://paperpile.com/c/zC8saG/dHz3">[8]</a></span></p>
                <p><strong>Vitamin D is indeed a cornerstone of our body as our very own cells crave it.</strong></p>
                <p><span style="font-weight: 400;">I have come across of hundreds books, scientific studies, articles and experiments on Vitamin D. It has been called the &ldquo;Miracle Vitamin&rdquo; and attributed magical properties. Even the skeptics admit the probability of its efficiency and urge for more experiments to be conducted.</span></p>
                <p><span style="font-weight: 400;">Even if some of these claims sound overstated, the facts don&rsquo;t lie. Since its first medical use at the beginning of the 20th century (for the treatment of rickets), Vitamin D has ongoingly proved its efficiency for countless human disorders, including cardiovascular health, &nbsp;bone health, depression, and autoimmune diseases. </span><a href="https://paperpile.com/c/zC8saG/tcPz"><span style="font-weight: 400;">[9]</span></a> <a href="https://paperpile.com/c/zC8saG/m939"><span style="font-weight: 400;">[10]</span></a></p>
                <p><strong><br /><strong>Asthma is one of these disorders. </strong></strong>
                </p>
                <div class="nuttshell">
                    <div class="nutshell-content">
                        <p><span style="font-weight: 400;">Bottom Line</span></p>
                        <p><em>You have been designed - through natural selection - to process and utilize Vitamin D for the proper functioning of every vital system in your body. Vitamin D has been used to treat numerous health disorders since its discovery at the beginning of the 20th century.</em></p>
                    </div>
                </div>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/2.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="3">
            <div class="content content-full-width">
                <p><span style="font-weight: 400;">In the first chapter - <a href="/introduction.php">The Causes of Asthma </a> - we explored how the gradual reduction of Vitamin D levels contributed to the development and aggravation of allergies and asthma in our species.</span></p>
                <p><span style="font-weight: 400;">Turning my focus on Vitamin D to treat my asthma was, for me, the logical thing to do, but in a world of silver bullets and snake oil treatments for everything from the common cold to cancer, I needed reliable scientific back-ups.</span></p>
                <p><span style="font-weight: 400;">I found that Professor Adrian Martineau from the Asthma UK Centre for Applied Research analyzed some of the available evidence </span><a href="https://paperpile.com/c/zC8saG/MIuP"><span style="font-weight: 400;">[11]</span></a><span style="font-weight: 400;">. He connected the results from seven trials involving 435 children and two studies involving 658 adults. The results confirmed </span><strong>Vitamin D reduced the risk of severe asthma attacks requiring hospital admission and decreased the rate of asthma attacks needing treatment with steroid tablets.</strong></p>
                <p><span style="font-weight: 400;">On the other side of the world, a Japanese study </span><a href="https://paperpile.com/c/zC8saG/lqLQ"><span style="font-weight: 400;">[12]</span></a> <span style="font-weight: 400;">found giving Vitamin D supplements to school students with a history of asthma resulted in an 83% reduction in their risks of catching Influenza A.</span></p>
                <p><span style="font-weight: 400;">A closer look shows experiments and fieldwork done in every corner of the planet with similar related findings. Researchers are on a scientific frenzy to understand Vitamin D's power.</span></p>
                <p><span style="font-weight: 400;">In 2011, a team from the University of Western Australia </span><a href="https://paperpile.com/c/zC8saG/CUlr"><span style="font-weight: 400;">[13]</span></a><span style="font-weight: 400;"> discovered that low Vitamin D levels at the age of 6 were a &ldquo;significant predictor&rdquo; of allergy sensitivity and developing asthma by the age of 14.</span></p>
                <p><span style="font-weight: 400;">In 2012, the Arab Emirate of Qatar confirmed the Australian study </span><a href="https://paperpile.com/c/zC8saG/Kv05"><span style="font-weight: 400;">[14]</span></a><span style="font-weight: 400;">, noting that the Vitamin D deficiency is &ldquo;the major predictor of asthma in Qatari children&rdquo;.</span></p>
                <p><span style="font-weight: 400;">Also in 2012, a study </span><a href="https://paperpile.com/c/zC8saG/9YsT"><span style="font-weight: 400;">[15]</span></a><span style="font-weight: 400;"> of 1024 asthmatic American children in a random trial found that those with high Vitamin D levels had twice as much health benefit from their corticosteroid asthma medication as children with low Vitamin D.</span></p>
                <p><span style="font-weight: 400;">Research in Canada </span><a href="https://paperpile.com/c/zC8saG/S8xn"><span style="font-weight: 400;">[16]</span></a> <span style="font-weight: 400;">involving people between the ages of 13 and 69 years found &nbsp;individuals with low Vitamin D are twice as likely to have had asthma at one point in their life.</span></p>
                <p><span style="font-weight: 400;">This list can go on and on and could fill many more pages.</span></p>
                <p><span style="font-weight: 400;">We presently have irrefutable evidence that Vitamin D plays a determining role in treating or reducing asthma and allergy related symptoms, but the mechanism behind it is complex, and science still has to connect all of the relevant dots.</span></p>
                <p><span style="font-weight: 400;">On a deep scientific level, researchers believe Vitamin D is promoting </span><strong>T Cells</strong> <strong>which control the immune response and inhibit the airway inflammation and hyperresponsiveness </strong><a href="https://paperpile.com/c/zC8saG/vxKf"><span style="font-weight: 400;">[17]</span></a><strong>. </strong><span style="font-weight: 400;">Increasing evidence also demonstrates Vitamin D genes are actively regulated in the developing human fetal lung and a disproportionate number of these genes are differentially adjusted in asthma and other autoimmune diseases </span><a href="https://paperpile.com/c/zC8saG/bBQQ"><span style="font-weight: 400;">[18]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Stresswise Vitamin D is ramping up the levels of the brain&rsquo;s serotonin activity - an important neurotransmitter depression that is anxiety related - and exerts an antidepressant effect.</span></p>
                <p><span style="font-weight: 400;">Indirectly, Vitamin D is controlling the immune system by increasing the bacterial richness in the upper gastrointestinal tract and reducing opportunistic pathogens opportunity to thrive </span><a href="https://paperpile.com/c/zC8saG/MpVc"><span style="font-weight: 400;">[19]</span></a><span style="font-weight: 400;">. </span><strong>In simple words, it can help fix the </strong><strong>leaky gut</strong><strong> and plug the barrier that prevents our immune system from over-reacting.</strong></p>
                <p>&nbsp;</p>

                <div class="nutshell-content">

                    <p><span style="font-weight: 400;">Bottom Line</span><strong><strong>&nbsp;</strong></strong>
                    </p>
                    <p><em><span style="font-weight: 400;">High quality evidence from numerous international studies suggest that Vitamin D plays a conclusive role in the treatment of asthma and maintaining optimal health. </span></em></p>
                </div>
            </div>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/3.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="4">
        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Optimal levels</span></h2>
            <p><span style="font-weight: 400;">To understand how to address your breathing and allergy issues with the aid of Vitamin D, we have to put our lab coat on for an instant.</span></p>
            <p><span style="font-weight: 400;">The most accurate way to measure how much Vitamin D our body has is the </span><strong>25-hydroxy Vitamin D blood tes</strong><span style="font-weight: 400;">t. The result is usually displayed in nanograms per milliliter - ng/mL. This test determines whether a person is deficient, sufficient, or toxic in vitamin D.</span></p>
            <p><span style="font-weight: 400;">However, for years, there hasn't been a consensus in medicine regarding what blood levels should define these categories.</span></p>
            <p><span style="font-weight: 400;">In 2016, Dr. Martine Luxwolda and her colleagues at the Dutch Medical Center Groningen conducted a brilliant study in Eastern Africa, which is believed to be the cradle of humanity </span><a href="https://paperpile.com/c/zC8saG/S0Bu"><span style="font-weight: 400;">[20]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">They measured the vitamin D levels of 60 pastoral &ldquo;hunter-gatherers&rdquo; - 35 Maasai and 25 Hadzabe -, who reside within a few degrees of the equator in Tanzania.</span></p>
            <p><span style="font-weight: 400;">These two tribes are among the last remainders living the </span><span style="font-weight: 400;">lifestyle of our ancestors.</span><span style="font-weight: 400;"> Their diet consists of meat, occasional fish, honey, fruits, and tubers. They have no personal possessions and don&rsquo;t wear any clothes above the waist. The Maasai's Vitamin D levels are a </span><span style="font-weight: 400;">reliable indicator</span><span style="font-weight: 400;"> of the optimal degree in humans.</span></p>
            <p><strong>Their mean Vitamin D blood level is 44 ng/ml reaching up to 52 ng/ml in some individuals.</strong></p>
            <p><span style="font-weight: 400;">There is also other evidence that supports these findings:</span></p>
            <ul class="general-list">
                <li style="font-weight: 400;"><span style="font-weight: 400;">Our closest &ldquo;simian relative&rdquo; the chimpanzee, has levels between 40 - 60 ng/ml. </span><a href="https://paperpile.com/c/zC8saG/tPF1"><span style="font-weight: 400;">[21]</span></a></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Lifeguards in summertime have 40 - 60 ng/ml levels without taking any supplements. </span><a href="https://paperpile.com/c/zC8saG/iVAw"><span style="font-weight: 400;">[22]</span></a></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">It takes a mother having a level of between 40 - 60 ng/ml herself to then have Vitamin D show up in breast milk for her baby. </span><a href="https://paperpile.com/c/zC8saG/n75t"><span style="font-weight: 400;">[23]</span></a></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Humans do not &ldquo;store&rdquo; Vitamin D until they reach a level of 40 ng/ml. </span><a href="https://paperpile.com/c/zC8saG/696F"><span style="font-weight: 400;">[24]</span></a></li>
            </ul>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line </span></p>
                    <p><em><span style="font-weight: 400;">Despite the lack of consensus, new compelling evidence suggest that that optimal levels of Vitamin D in humans is between 40- 60 ng/ml.</span></em></p>
                </div>
            </div>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/4.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="5">
        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Is Vitamin D Deficiency a Common Problem?</span></h2>
            <p><span style="font-weight: 400;">Vitamin D deficiency is nowadays a serious globally ignored epidemic </span><a href="https://paperpile.com/c/zC8saG/aump"><span style="font-weight: 400;">[25]</span></a> <a href="https://paperpile.com/c/zC8saG/nasP"><span style="font-weight: 400;">[26]</span></a> <a href="https://paperpile.com/c/zC8saG/OjLt"><span style="font-weight: 400;">[27]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
            <p><span style="font-weight: 400;">While it is more common in young women, infants, the elderly and people who have dark skin, the Global Vitamin D levels are alarmingly low. It spares no geographical location.</span></p>
            <p><span style="font-weight: 400;">The average Vitamin D level in the </span><strong>United States</strong><span style="font-weight: 400;"> in 2004 was 24 ng/ml. A 2005-2006 national survey conducted in nearly 4,500 adults found that over 40% of people had levels below 20 ng/ml rising to 82% in African Americans and 69% in Hispanics </span><a href="https://paperpile.com/c/zC8saG/1bVf"><span style="font-weight: 400;">[28]</span></a><span style="font-weight: 400;">. A nationally representative survey from </span><strong>Canada</strong><span style="font-weight: 400;"> collected data on 5306 individuals, aged 6-79 years between 2007 and 2009. The mean concentration of 25(OH)D in this population was 21 ng/ml </span><a href="https://paperpile.com/c/zC8saG/jNzp"><span style="font-weight: 400;">[29]</span></a><span style="font-weight: 400;">. In </span><strong>Australia</strong><span style="font-weight: 400;">, the percentage of people having less than 40 ng/mg was 86%, and 60% have dangerous levels of &lt;20 ng/mg </span><a href="https://paperpile.com/c/zC8saG/JjZm"><span style="font-weight: 400;">[30]</span></a><span style="font-weight: 400;">. A study compared the Vitamin D status of 824 seniors living in 11 </span><strong>European</strong><span style="font-weight: 400;"> countries. 46% of the women and 36% of the men had levels of 10 ng/ml, and it decreased with age </span><a href="https://paperpile.com/c/zC8saG/qWaL"><span style="font-weight: 400;">[31]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">In 2013, one-third of the </span><strong>global population</strong><span style="font-weight: 400;"> was considered to be Vitamin D deficient. Please note, back then, the accepted levels of Vitamin D deficiency was still deemed to be below 25 ng/ml </span><a href="https://paperpile.com/c/zC8saG/PD35"><span style="font-weight: 400;">[32]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">Taking into account the newly discovered 50 ng/ml benchmark today, we can only assume that this percentage is presently much higher.</span></p>
            <p><strong>It probably includes you as well.</strong></p>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line </span></p>
                    <p><em><span style="font-weight: 400;">We are now confronted with a depth and seriously ignored epidemic of Vitamin D deficiency sweeping across our modern world affecting all populations and all age groups.</span></em></p>
                </div>
            </div>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/5.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="6">
        <div class="content content-full-width">
            <p><span style="font-weight: 400;">Anthony, 42, is an African American from New York City. He works as a mechanical engineer for the subway system. He leaves home early in the morning and returns from work around sunset. For lunch, he drives his car to a restaurant or orders food at his office.</span></p>
            <p><span style="font-weight: 400;">His diet is </span><span style="font-weight: 400;">American Standard</span><span style="font-weight: 400;"> - high in dairy, refined sugar and processed food. He is slightly overweight. Over the weekend, Anthony watches TV, surfs the internet, or goes shopping in one of the huge grocery stores or malls. For entertainment, he meets up with his friends in a pub for drinks.</span></p>
            <p><span style="font-weight: 400;">For holidays, he takes his family to Florida or other warm places. While on the beach, he always wears sunscreen as he is aware of so-called ill-effects of the sun, including skin cancer, wrinkles, and aging spots.</span></p>
            <p><span style="font-weight: 400;">Anthony developed allergies and asthma in his late twenties and is suspected to be type 1 diabetic.</span></p>
            <p><strong>He is a representative of the millions of individuals around the world who share a common thread - the modern way of living. The number one cause of the global Vitamin D deficiency is the present-day lifestyle.</strong></p>
            <p><span style="font-weight: 400;">Vitamin D is accumulated in our body through sun exposure and diet. Millions of people, like Anthony, restrict their sun exposure to a couple of minutes a day. When they do it, they use sunscreen that further fends off the Vitamin D from being produced by the skin. Their diet is low on Vitamin D and high on health-damaging foods.</span></p>
            <p><span style="font-weight: 400;">Vitamin D is fat soluble. In overweight and obese individuals, there is an unbalanced storage of Vitamin D that prevents it from being passed into the bloodstream to reach the vital organs </span><a href="https://paperpile.com/c/zC8saG/UdPK"><span style="font-weight: 400;">[33]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">For dark skin people like Anthony, the Vitamin D levels are even</span><span style="font-weight: 400;"> lower </span><a href="https://paperpile.com/c/zC8saG/T7EM"><span style="font-weight: 400;">[34]</span></a><span style="font-weight: 400;">. This type of skin is richer in </span><span style="font-weight: 400;">melanin </span><span style="font-weight: 400;">that acts as a natural sunscreen and reduces the body&rsquo;s ability to produce Vitamin D.</span></p>
            <p><span style="font-weight: 400;">For 2.5 million years - or for 84,000 generations - our complex body has evolved in perfectly harmony with its surroundings. We are designed to be immersed in nature &nbsp;- to run, swim, climb, and fight. 10,000 years ago humans became prisoners of their comfort and efficiency. We moved away from nature and settled into villages that later became polluted metropolises. We gradually traded our &ldquo;natural&rdquo; way of living for the modern lifestyle, and our body didn&rsquo;t have enough time to adapt. That was just 330 generations ago, a blink of an eye compared to the entire human existence.</span></p>
            <p><span style="font-weight: 400;">By taking the above in into consideration, I am not surprised to see the that global Vitamin D levels are at their lowest since our birth as a species, nor am I surprised that people all over the world are overweight and over medicated with persistent and aggravating disorders.</span></p>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line</span></p>
                    <p><strong><em><span>The key root factor of the current global Vitamin D deficiency is the present-day lifestyle.</span></em></strong></p>
                </div>
            </div>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/6.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="7">
        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">How to Restore your Health Using Vitamin D</span></h2>

            <p><span style="font-weight: 400;">The most important aspect in managing a health disorder is to be aware of its causes. Now that we have a better understanding of the roots of the Vitamin D insufficiency, we can move forward and take action.</span></p>
            <p><span style="font-weight: 400;">The process implied is simple and straightforward, but it does require a small degree of effort and perseverance. </span><strong>The first step in restoring the optimal levels of Vitamin D to 50 ng/ml is to determine your current baseline values. </strong><span style="font-weight: 400;">This can be achieved through a simple blood test. Read </span><span style="font-weight: 400;">here</span><span style="font-weight: 400;"> for a detailed guideline on how to run the test and interpret the results.</span></p>
            <p><strong><span style="font-weight: 400;">However, the problem is that most people will not have their blood tested unless their doctor recommends it. </span></strong></p>

            <h2><span style="font-weight: 400;">Take Vitamin D Supplements</span></h2>

            <p>For individuals that cannot have their blood levels tested, we suggest a recommended dose that is easy to obtain at most pharmacies. This will get most people above 40 ng/ml and close to around 50 ng/ml and will not cause anyone to get to toxic levels.
            </p>

            <p style="text-align: left;">RISE follows The Vitamin D Council&rsquo;s - a fascinating non-profit organization - <a href="https://www.google.co.id/search?q=vitami+d+council+intake+recommendations&amp;oq=vitami+d+council+intake+recommendations&amp;aqs=chrome..69i57j0l5.5824j0j4&amp;sourceid=chrome&amp;ie=UTF-8" target="_blank">recommendations</a>:</p>

            <ul class="general-list">
                <li>Children - 1,000 IU of Vitamin D a day for every 25 lbs of their weight, rounded up.</li>
                <li>Adults - 5,000 IU of Vitamin D a day; People also go up to 10,000 IU of vitamin D a day which is unlikely to do any harm.</li>
                <li>Pregnant mothers - 6,000 IU of Vitamin D a day. </li>
            </ul>
            <p><span style="font-weight: 400;">For those who want a more careful calculation, 70-80 IU/day/kg of body weight total input is needed to obtain a 25(OH)D of 40 ng/ml.</span></p>
            <p><strong><span style="font-weight: 400;">Note: This dosage is higher than the currently advisable one of 400-600 I.U. by various medical associations, but many scientific studies have clearly demonstrated that these recommendations are outdated.</span></strong></p>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line</span></p>
                    <p><strong><em><span style="font-weight: 400;">Take a Vitamin D blood test and work with your doctor to reach a 50 ng/ml level. If the blood test is not possible, take 5000 I.U of Vitamin D per day until the optimal level is attained. </span></em></strong></p>
                </div>
            </div>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/7.png">
        <!-- image    -->

        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Expose Your Skin to Sun Moderately but Frequently</span></h2>
            <p><span style="font-weight: 400;">How much sun exposure can provide you with Vitamin D depends on latitude, time of the day, altitude, season, age, skin pigment, and if you use sunscreen or not. Therefore, it is not scientifically correct to recommend a universal dosage.
</span></p>
            <p><span style="font-weight: 400;">However, we can provide some general guidelines to follow:</span></p>
            <ul class="general-list">
                <li style="font-weight: 400;"><span style="font-weight: 400;">Be careful not to get sunburned. Start low and then gradually increase the duration of sun exposure according to your skin tolerance. A good indicator is half the time required for your skin to turn pink. Then, cover up with clothing and go into the shade.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">The best time to sunbath is in the afternoon, between 1 and5 PM. Try to avoid the intense sun at noon.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Don’t apply any sunscreen while exposing yourself to the sun.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">People with dark skin need about 5-6 times the duration of sun exposure compared to fair-skinned individuals in order to synthesize the same amount of Vitamin D.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Combine sun exposure with exercise and other stress management techniques that will help treat your asthma. </span></li>
            </ul>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line</span></p>
                    <p><em><span style="font-weight: 400;">Getting the optimal level of Vitamin D from the sun depends on numerous factors. We recommend sun exposure tobe done habitually but with moderation.</span></em></p>
                </div>
            </div>
            <h2><span style="font-weight: 400;">The Healing Time Frame</span></h2>
            <p><span style="font-weight: 400;">A proper level of 40-60 ng/mg Vitamin D can be attained within one to three months. It depends on your baseline values and your body&rsquo;s ability to absorb it </span><a href="https://paperpile.com/c/zC8saG/Sai4"><span style="font-weight: 400;">[35]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">When optimal levels are reached, it is important they are maintained. Taking Vitamin D and getting sun exposure should become part of your daily routine.</span></p>
            <p><span style="font-weight: 400;">Asthma and allergy related: the time frame for the first results to be noticed also depends on each individual. Optimizing the Vitamin D intake and combining it with the other three steps in the RISE method can lead to results as fast as three to four weeks.</span></p>

        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/8.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="8">

        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Toxicity of Vitamin D Supplements</span></h2>
            <p><span style="font-weight: 400;">When I started to take 6000 UI/day of Vitamin D, I was cautioned about Vitamin D toxicity.</span></p>
            <p><span style="font-weight: 400;">Various articles written in newspapers and magazines tend to scare readers about Vitamin D supplements to a point where people decide not to take them at all.</span></p>
            <p><span style="font-weight: 400;">Vitamin D toxicity, where it can be harmful, usually happens if you take 40,000 IU per day for a couple of months or longer, or take a very large one-time dose </span><a href="https://paperpile.com/c/zC8saG/Z7wE"><span style="font-weight: 400;">[36]</span></a><span style="font-weight: 400;">. When your 25(OH)D levels are too high, dangerous levels of calcium can be developed in your blood. Read </span><span style="font-weight: 400;">here</span><span style="font-weight: 400;"> for more information about the Vitamin D toxicity symptoms and how to address them.</span></p>
            <p><strong><span style="font-weight: 400;">But again, this happens in enormous doses - 8 times higher than the recommended 5000 IU/ day dose.</span></strong></p>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <p><span style="font-weight: 400;">Bottom Line</span></p>
                    <p><em><span style="font-weight: 400;">You don’t need to worry about Vitamin D’s toxicity as long as you follow RISE’s 5000 IU /day recommended dosage.</span></em></p>
                </div>
            </div>

        </div>
        <!-- sectiune capitol     -->


        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Side Effects of Sun Exposure</span></h2>
            <p><span style="font-weight: 400;">There is a lot of confusion and debate on sun exposure, skin cancer, and sunscreen usage.</span></p>
            <p><span style="font-weight: 400;">Various physicians make extreme recommendations depending upon their specialty. Dermatologists exaggerate the fear of skin cancer and recommend avoiding the sun, while physicians solely interested in vitamin D support liberal sun exposure and minimize the fear of skin cancer.</span></p>
            <p><span style="font-weight: 400;">This is, unfortunately, a basic flaw inherent to modern medicine. As is usually the case, </span><strong>the proper way to address this issue is through a common-sense compromise: the sun exposure has to be done frequently but with moderation </strong><a href="https://paperpile.com/c/zC8saG/zWRq"><span style="font-weight: 400;">[37]</span></a><strong>.</strong></p>
            <p><span style="font-weight: 400;">While there is evidence that UVB causes melanoma </span><a href="https://paperpile.com/c/zC8saG/Kyry"><span style="font-weight: 400;">[38]</span></a><span style="font-weight: 400;"> - skin cancer - the mortality rate has doubled between 1975 and 2010, despite the rise in the use of sunscreens </span><a href="https://paperpile.com/c/zC8saG/KddZ"><span style="font-weight: 400;">[39]</span></a><span style="font-weight: 400;">. Most authorities agree that the risk for melanoma comes from family history, indoor tanning, fair skin, freckles, the number of moles on your skin, exposure to ultraviolet radiation and severe sunburns </span><a href="https://paperpile.com/c/zC8saG/2WRE"><span style="font-weight: 400;">[40]</span></a><span style="font-weight: 400;">.</span></p>
            <p><span style="font-weight: 400;">Paradoxically, researchers have also identified mechanisms where vitamin D inhibits all the three types of skin cancer </span><a href="https://paperpile.com/c/zC8saG/wLGR"><span style="font-weight: 400;">[41]</span></a><span style="font-weight: 400;">.</span></p>
            <p><strong><span style="font-weight: 400;">However, the risk remains. To reduce or even eliminate its probability, follow RISE Asthma&rsquo;s - common sense - general guidelines for sun exposure listed above. </span></strong></p>
        </div>
        <!-- sectiune capitol     -->

        <img class="img-responsive" src="<?php echo SITE_URL;?>assets/img/vitamind/9.png">
        <!-- image    -->
    </div>
    <div class="scroll" data-type="9">
        <div class="content content-full-width">
            <h2><span style="font-weight: 400;">Conclusion</span></h2>
            <p><span style="font-weight: 400;">Optimizing the Vitamin D intake is the cornerstone of the RISE method. This powerful hormone is the link between various elements that cause and exacerbate allergies and asthma.</span></p>
            <p><span style="font-weight: 400;">Our world is now facing a Vitamin D deficiency epidemic, predominantly due to our modern lifestyle, misconceptions about the sun exposure and the suboptimal daily recommended dose of Vitamin D.</span></p>
            <p><span style="font-weight: 400;">Armed with this new knowledge, it is time to act accordingly. Taking Vitamin D supplements and getting closer to the sun needs to become apart of our daily routine.</span></p>
            <p><strong><span style="font-weight: 400;">We promise that the results will not fail to appear and will outweigh any effort.</span></strong></p>
        </div>
        <!-- sectiune capitol     -->



        <div class="content content-full-width">


            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">
                            <a data-target="#collapseOne" href="#" data-toggle="collapse" id="myBtn">
                                                       References (click to open)

                                                        <b class="caret"></b>
                                                    </a>
                        </h1>
                    </div>
                    <div id="myModal" class="modal">
                            <div class="modal-content">
                                <span class="close">&times;</span>
                                <p><span style="font-weight: 400;">[1]</span> <a href="http://paperpile.com/b/zC8saG/JD7h"><span style="font-weight: 400;">A. W. Norman, &ldquo;From vitamin D to hormone D: fundamentals of the vitamin D endocrine system essential for good health,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, p. 491S&ndash;499S, Aug. 2008.</span></a></p>
                            <p><span style="font-weight: 400;">[2]</span> <a href="http://paperpile.com/b/zC8saG/SHZN"><span style="font-weight: 400;">C. Aranow, &ldquo;Vitamin D and the immune system,&rdquo; </span><em><span style="font-weight: 400;">J. Investig. Med.</span></em><span style="font-weight: 400;">, vol. 59, no. 6, pp. 881&ndash;886, Aug. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[3]</span> <a href="http://paperpile.com/b/zC8saG/ZlkY"><span style="font-weight: 400;">M. Raman, A. N. Milestone, J. R. F. Walters, A. L. Hart, and S. Ghosh, &ldquo;Vitamin D and gastrointestinal diseases: inflammatory bowel disease and colorectal cancer,&rdquo; </span><em><span style="font-weight: 400;">Therap. Adv. Gastroenterol.</span></em><span style="font-weight: 400;">, vol. 4, no. 1, pp. 49&ndash;62, Jan. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[4]</span> <a href="http://paperpile.com/b/zC8saG/zSPT"><span style="font-weight: 400;">I. Gouni-Berthold, W. Krone, and H. K. Berthold, &ldquo;Vitamin D and cardiovascular disease,&rdquo; </span><em><span style="font-weight: 400;">Curr. Vasc. Pharmacol.</span></em><span style="font-weight: 400;">, vol. 7, no. 3, pp. 414&ndash;422, Jul. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[5]</span> <a href="http://paperpile.com/b/zC8saG/qCSL"><span style="font-weight: 400;">M. F. Holick, &ldquo;Vitamin D and bone health,&rdquo; </span><em><span style="font-weight: 400;">J. Nutr.</span></em><span style="font-weight: 400;">, vol. 126, no. 4 Suppl, p. 1159S&ndash;64S, Apr. 1996.</span></a></p>
                            <p><span style="font-weight: 400;">[6]</span> <a href="http://paperpile.com/b/zC8saG/IPkh"><span style="font-weight: 400;">A. Talaei, M. Mohamadi, and Z. Adgi, &ldquo;The effect of vitamin D on insulin resistance in patients with type 2 diabetes,&rdquo; </span><em><span style="font-weight: 400;">Diabetol. Metab. Syndr.</span></em><span style="font-weight: 400;">, vol. 5, no. 1, p. 8, Feb. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[7]</span> <a href="http://paperpile.com/b/zC8saG/WaX5"><span style="font-weight: 400;">S. V. Ramagopalan </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;A ChIP-seq defined genome-wide map of vitamin D receptor binding: associations with disease and evolution,&rdquo; </span><em><span style="font-weight: 400;">Genome Res.</span></em><span style="font-weight: 400;">, vol. 20, no. 10, pp. 1352&ndash;1360, Oct. 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[8]</span> <a href="http://paperpile.com/b/zC8saG/dHz3"><span style="font-weight: 400;">&ldquo;How many genes do you REALLY have?,&rdquo; </span><em><span style="font-weight: 400;">Science 2.0</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.science20.com/adaptive_complexity/blog/how_many_genes_do_you_really_have"><span style="font-weight: 400;">http://www.science20.com/adaptive_complexity/blog/how_many_genes_do_you_really_have</span></a><a href="http://paperpile.com/b/zC8saG/dHz3"><span style="font-weight: 400;">. [Accessed: 20-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[9]</span> <a href="http://paperpile.com/b/zC8saG/tcPz"><span style="font-weight: 400;">R. Nair and A. Maseeh, &ldquo;Vitamin D: The &lsquo;sunshine&rsquo; vitamin,&rdquo; </span><em><span style="font-weight: 400;">J. Pharmacol. Pharmacother.</span></em><span style="font-weight: 400;">, vol. 3, no. 2, pp. 118&ndash;126, Apr. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[10]</span> <a href="http://paperpile.com/b/zC8saG/m939"><span style="font-weight: 400;">M. F. Holick, &ldquo;Vitamin D: A millenium perspective,&rdquo; </span><em><span style="font-weight: 400;">J. Cell. Biochem.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, pp. 296&ndash;307, Feb. 2003.</span></a></p>
                            <p><span style="font-weight: 400;">[11]</span> <a href="http://paperpile.com/b/zC8saG/MIuP"><span style="font-weight: 400;">A. R. Martineau </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D for the management of asthma,&rdquo; in </span><em><span style="font-weight: 400;">Cochrane Database of Systematic Reviews</span></em><span style="font-weight: 400;">, John Wiley &amp; Sons, Ltd, 1996.</span></a></p>
                            <p><span style="font-weight: 400;">[12]</span> <a href="http://paperpile.com/b/zC8saG/lqLQ"><span style="font-weight: 400;">M. Urashima, T. Segawa, M. Okazaki, M. Kurihara, Y. Wada, and H. Ida, &ldquo;Randomized trial of vitamin D supplementation to prevent seasonal influenza A in schoolchildren,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 91, no. 5, pp. 1255&ndash;1260, May 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[13]</span> <a href="http://paperpile.com/b/zC8saG/CUlr"><span style="font-weight: 400;">E. M. Hollams </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D and atopy and asthma phenotypes in children: a longitudinal cohort study,&rdquo; </span><em><span style="font-weight: 400;">Eur. Respir. J.</span></em><span style="font-weight: 400;">, vol. 38, no. 6, pp. 1320&ndash;1327, Dec. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[14]</span> <a href="http://paperpile.com/b/zC8saG/Kv05"><span style="font-weight: 400;">A. Bener, M. S. Ehlayel, M. K. Tulic, and Q. Hamid, &ldquo;Vitamin D deficiency as a strong predictor of asthma in children,&rdquo; </span><em><span style="font-weight: 400;">Int. Arch. Allergy Immunol.</span></em><span style="font-weight: 400;">, vol. 157, no. 2, pp. 168&ndash;175, 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[15]</span> <a href="http://paperpile.com/b/zC8saG/9YsT"><span style="font-weight: 400;">G. Paul, J. M. Brehm, J. F. Alcorn, F. Holgu&iacute;n, S. J. Aujla, and J. C. Celed&oacute;n, &ldquo;Vitamin D and asthma,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Respir. Crit. Care Med.</span></em><span style="font-weight: 400;">, vol. 185, no. 2, pp. 124&ndash;132, Jan. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[16]</span> <a href="http://paperpile.com/b/zC8saG/S8xn"><span style="font-weight: 400;">S. J. Niruban, K. Alagiakrishnan, J. Beach, and A. Senthilselvan, &ldquo;Association between vitamin D and respiratory outcomes in Canadian adolescents and adults,&rdquo; </span><em><span style="font-weight: 400;">J. Asthma</span></em><span style="font-weight: 400;">, vol. 52, no. 7, pp. 653&ndash;661, Sep. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[17]</span> <a href="http://paperpile.com/b/zC8saG/vxKf"><span style="font-weight: 400;">N. E. Lange, A. Litonjua, C. M. Hawrylowicz, and S. Weiss, &ldquo;Vitamin D, the immune system and asthma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 5, no. 6, pp. 693&ndash;702, Nov. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[18]</span> <a href="http://paperpile.com/b/zC8saG/bBQQ"><span style="font-weight: 400;">A. T. Kho </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D related genes in lung development and asthma pathogenesis,&rdquo; </span><em><span style="font-weight: 400;">BMC Med. Genomics</span></em><span style="font-weight: 400;">, vol. 6, p. 47, Nov. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[19]</span> <a href="http://paperpile.com/b/zC8saG/MpVc"><span style="font-weight: 400;">M. Bashir </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Effects of high doses of vitamin D3 on mucosa-associated gut microbiome vary between regions of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 55, no. 4, pp. 1479&ndash;1489, Jun. 2016.</span></a></p>
                            <p><span style="font-weight: 400;">[20]</span> <a href="http://paperpile.com/b/zC8saG/S0Bu"><span style="font-weight: 400;">M. F. Luxwolda, R. S. Kuipers, I. P. Kema, D. A. J. Dijck-Brouwer, and F. A. J. Muskiet, &ldquo;Traditionally living populations in East Africa have a mean serum 25-hydroxyvitamin D concentration of 115 nmol/l,&rdquo; </span><em><span style="font-weight: 400;">Br. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 108, no. 9, pp. 1557&ndash;1561, Nov. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[21]</span> <a href="http://paperpile.com/b/zC8saG/tPF1"><span style="font-weight: 400;">R. Vieth, &ldquo;Why the minimum desirable serum 25-hydroxyvitamin D level should be 75 nmol/L (30 ng/ml),&rdquo; </span><em><span style="font-weight: 400;">Best Pract. Res. Clin. Endocrinol. Metab.</span></em><span style="font-weight: 400;">, vol. 25, no. 4, pp. 681&ndash;691, Aug. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[22]</span> <a href="http://paperpile.com/b/zC8saG/iVAw"><span style="font-weight: 400;">&ldquo;Most S. Calif lifeguards have good level of vitamin D (above 40 ng) - Aug 2016 | Vitamin D Wiki.&rdquo; [Online]. Available: </span></a><a href="http://paperpile.com/b/zC8saG/iVAw"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[23]</span> <a href="http://paperpile.com/b/zC8saG/n75t"><span style="font-weight: 400;">B. W. Hollis </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Maternal Versus Infant Vitamin D Supplementation During Lactation: A Randomized Controlled Trial,&rdquo; </span><em><span style="font-weight: 400;">Pediatrics</span></em><span style="font-weight: 400;">, vol. 136, no. 4, pp. 625&ndash;634, Oct. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[24]</span> <a href="http://paperpile.com/b/zC8saG/696F"><span style="font-weight: 400;">R. P. Heaney, &ldquo;Vitamin D in health and disease,&rdquo; </span><em><span style="font-weight: 400;">Clin. J. Am. Soc. Nephrol.</span></em><span style="font-weight: 400;">, vol. 3, no. 5, pp. 1535&ndash;1541, Sep. 2008.</span></a></p>
                            <p><span style="font-weight: 400;">[25]</span> <a href="http://paperpile.com/b/zC8saG/aump"><span style="font-weight: 400;">C. Palacios and L. Gonzalez, &ldquo;Is vitamin D deficiency a major global public health problem?,&rdquo; </span><em><span style="font-weight: 400;">J. Steroid Biochem. Mol. Biol.</span></em><span style="font-weight: 400;">, vol. 144 Pt A, pp. 138&ndash;145, Oct. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[26]</span> <a href="http://paperpile.com/b/zC8saG/nasP"><span style="font-weight: 400;">Z. Naeem, &ldquo;Vitamin d deficiency- an ignored epidemic,&rdquo; </span><em><span style="font-weight: 400;">Int. J. Health Sci. </span></em><span style="font-weight: 400;">, vol. 4, no. 1, pp. V&ndash;VI, Jan. 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[27]</span> <a href="http://paperpile.com/b/zC8saG/OjLt"><span style="font-weight: 400;">&ldquo;THE GLOBAL EPIDEMIOLOGY OF VITAMIN D STATUS &bull; JARCP The Journal of Aging Research &amp; Clinical Practice.&rdquo; [Online]. Available: </span></a><a href="http://www.jarcp.com/703-the-global-epidemiology-of-vitamin-d-status.html"><span style="font-weight: 400;">http://www.jarcp.com/703-the-global-epidemiology-of-vitamin-d-status.html</span></a><a href="http://paperpile.com/b/zC8saG/OjLt"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[28]</span> <a href="http://paperpile.com/b/zC8saG/1bVf"><span style="font-weight: 400;">K. Y. Z. Forrest and W. L. Stuhldreher, &ldquo;Prevalence and correlates of vitamin D deficiency in US adults,&rdquo; </span><em><span style="font-weight: 400;">Nutr. Res.</span></em><span style="font-weight: 400;">, vol. 31, no. 1, pp. 48&ndash;54, Jan. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[29]</span> <a href="http://paperpile.com/b/zC8saG/jNzp"><span style="font-weight: 400;">K. Sarafin </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Standardizing 25-hydroxyvitamin D values from the Canadian Health Measures Survey,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 102, no. 5, pp. 1044&ndash;1050, Nov. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[30]</span> <a href="http://paperpile.com/b/zC8saG/JjZm"><span style="font-weight: 400;">T. K. Gill </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D levels in an Australian population,&rdquo; </span><em><span style="font-weight: 400;">BMC Public Health</span></em><span style="font-weight: 400;">, vol. 14, p. 1001, Sep. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[31]</span> <a href="http://paperpile.com/b/zC8saG/qWaL"><span style="font-weight: 400;">R. P. van der Wielen </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Serum vitamin D concentrations among elderly people in Europe,&rdquo; </span><em><span style="font-weight: 400;">Lancet</span></em><span style="font-weight: 400;">, vol. 346, no. 8969, pp. 207&ndash;210, Jul. 1995.</span></a></p>
                            <p><span style="font-weight: 400;">[32]</span> <a href="http://paperpile.com/b/zC8saG/PD35"><span style="font-weight: 400;">J. Hilger </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;A systematic review of vitamin D status in populations worldwide,&rdquo; </span><em><span style="font-weight: 400;">Br. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 111, no. 1, pp. 23&ndash;45, Jan. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[33]</span> <a href="http://paperpile.com/b/zC8saG/UdPK"><span style="font-weight: 400;">&ldquo;Office of Dietary Supplements - Vitamin D.&rdquo; [Online]. Available: </span></a><a href="https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/"><span style="font-weight: 400;">https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/</span></a><a href="http://paperpile.com/b/zC8saG/UdPK"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[34]</span> <a href="http://paperpile.com/b/zC8saG/T7EM"><span style="font-weight: 400;">S. A. Linnebur, S. F. Vondracek, J. P. Vande Griend, J. M. Ruscin, and M. T. McDermott, &ldquo;Prevalence of vitamin D insufficiency in elderly ambulatory outpatients in Denver, Colorado,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Geriatr. Pharmacother.</span></em><span style="font-weight: 400;">, vol. 5, no. 1, pp. 1&ndash;8, Mar. 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[35]</span> <a href="http://paperpile.com/b/zC8saG/Sai4"><span style="font-weight: 400;">F. Alshahrani and N. Aljohani, &ldquo;Vitamin D: deficiency, sufficiency and toxicity,&rdquo; </span><em><span style="font-weight: 400;">Nutrients</span></em><span style="font-weight: 400;">, vol. 5, no. 9, pp. 3605&ndash;3616, Sep. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[36]</span> <a href="http://paperpile.com/b/zC8saG/Z7wE"><span style="font-weight: 400;">J. N. Hathcock, A. Shao, R. Vieth, and R. Heaney, &ldquo;Risk assessment for vitamin D,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 85, no. 1, pp. 6&ndash;18, Jan. 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[37]</span> <a href="http://paperpile.com/b/zC8saG/zWRq"><span style="font-weight: 400;">J. Reichrath and B. N&uuml;rnberg, &ldquo;Cutaneous vitamin D synthesis versus skin cancer development: The Janus faces of solar UV-radiation,&rdquo; </span><em><span style="font-weight: 400;">Dermatoendocrinol.</span></em><span style="font-weight: 400;">, vol. 1, no. 5, pp. 253&ndash;261, Sep. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[38]</span> <a href="http://paperpile.com/b/zC8saG/Kyry"><span style="font-weight: 400;">B. Anna, Z. Blazej, G. Jacqueline, C. J. Andrew, R. Jeffrey, and S. Andrzej, &ldquo;Mechanism of UV-related carcinogenesis and its contribution to nevi/melanoma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Dermatol.</span></em><span style="font-weight: 400;">, vol. 2, no. 4, pp. 451&ndash;469, 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[39]</span> <a href="http://paperpile.com/b/zC8saG/KddZ"><span style="font-weight: 400;">&ldquo;Browse the Tables and Figures - SEER Cancer Statistics Review (CSR) 1975-2010.&rdquo; [Online]. Available: </span></a><a href="https://seer.cancer.gov/archive/csr/1975_2010/browse_csr.php?section=16&amp;page=sect_16_table.05.html"><span style="font-weight: 400;">https://seer.cancer.gov/archive/csr/1975_2010/browse_csr.php?section=16&amp;page=sect_16_table.05.html</span></a><a href="http://paperpile.com/b/zC8saG/KddZ"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[40]</span> <a href="http://paperpile.com/b/zC8saG/2WRE"><span style="font-weight: 400;">&ldquo;Skin Cancer Research Highlights.&rdquo; [Online]. Available: </span></a><a href="https://www.cancer.org/research/skin-cancer-research-highlights.html"><span style="font-weight: 400;">https://www.cancer.org/research/skin-cancer-research-highlights.html</span></a><a href="http://paperpile.com/b/zC8saG/2WRE"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[41]</span> <a href="http://paperpile.com/b/zC8saG/wLGR"><span style="font-weight: 400;">&ldquo;Vitamin D and Cancer Prevention,&rdquo; </span><em><span style="font-weight: 400;">National Cancer Institute</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="https://www.cancer.gov/about-cancer/causes-prevention/risk/diet/vitamin-d-fact-sheet"><span style="font-weight: 400;">https://www.cancer.gov/about-cancer/causes-prevention/risk/diet/vitamin-d-fact-sheet</span></a><a href="http://paperpile.com/b/zC8saG/wLGR"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><br /><br /></p>
                            </div>
                    </div>
                   <!-- <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><span style="font-weight: 400;">[1]</span> <a href="http://paperpile.com/b/zC8saG/JD7h"><span style="font-weight: 400;">A. W. Norman, &ldquo;From vitamin D to hormone D: fundamentals of the vitamin D endocrine system essential for good health,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, p. 491S&ndash;499S, Aug. 2008.</span></a></p>
                            <p><span style="font-weight: 400;">[2]</span> <a href="http://paperpile.com/b/zC8saG/SHZN"><span style="font-weight: 400;">C. Aranow, &ldquo;Vitamin D and the immune system,&rdquo; </span><em><span style="font-weight: 400;">J. Investig. Med.</span></em><span style="font-weight: 400;">, vol. 59, no. 6, pp. 881&ndash;886, Aug. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[3]</span> <a href="http://paperpile.com/b/zC8saG/ZlkY"><span style="font-weight: 400;">M. Raman, A. N. Milestone, J. R. F. Walters, A. L. Hart, and S. Ghosh, &ldquo;Vitamin D and gastrointestinal diseases: inflammatory bowel disease and colorectal cancer,&rdquo; </span><em><span style="font-weight: 400;">Therap. Adv. Gastroenterol.</span></em><span style="font-weight: 400;">, vol. 4, no. 1, pp. 49&ndash;62, Jan. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[4]</span> <a href="http://paperpile.com/b/zC8saG/zSPT"><span style="font-weight: 400;">I. Gouni-Berthold, W. Krone, and H. K. Berthold, &ldquo;Vitamin D and cardiovascular disease,&rdquo; </span><em><span style="font-weight: 400;">Curr. Vasc. Pharmacol.</span></em><span style="font-weight: 400;">, vol. 7, no. 3, pp. 414&ndash;422, Jul. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[5]</span> <a href="http://paperpile.com/b/zC8saG/qCSL"><span style="font-weight: 400;">M. F. Holick, &ldquo;Vitamin D and bone health,&rdquo; </span><em><span style="font-weight: 400;">J. Nutr.</span></em><span style="font-weight: 400;">, vol. 126, no. 4 Suppl, p. 1159S&ndash;64S, Apr. 1996.</span></a></p>
                            <p><span style="font-weight: 400;">[6]</span> <a href="http://paperpile.com/b/zC8saG/IPkh"><span style="font-weight: 400;">A. Talaei, M. Mohamadi, and Z. Adgi, &ldquo;The effect of vitamin D on insulin resistance in patients with type 2 diabetes,&rdquo; </span><em><span style="font-weight: 400;">Diabetol. Metab. Syndr.</span></em><span style="font-weight: 400;">, vol. 5, no. 1, p. 8, Feb. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[7]</span> <a href="http://paperpile.com/b/zC8saG/WaX5"><span style="font-weight: 400;">S. V. Ramagopalan </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;A ChIP-seq defined genome-wide map of vitamin D receptor binding: associations with disease and evolution,&rdquo; </span><em><span style="font-weight: 400;">Genome Res.</span></em><span style="font-weight: 400;">, vol. 20, no. 10, pp. 1352&ndash;1360, Oct. 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[8]</span> <a href="http://paperpile.com/b/zC8saG/dHz3"><span style="font-weight: 400;">&ldquo;How many genes do you REALLY have?,&rdquo; </span><em><span style="font-weight: 400;">Science 2.0</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.science20.com/adaptive_complexity/blog/how_many_genes_do_you_really_have"><span style="font-weight: 400;">http://www.science20.com/adaptive_complexity/blog/how_many_genes_do_you_really_have</span></a><a href="http://paperpile.com/b/zC8saG/dHz3"><span style="font-weight: 400;">. [Accessed: 20-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[9]</span> <a href="http://paperpile.com/b/zC8saG/tcPz"><span style="font-weight: 400;">R. Nair and A. Maseeh, &ldquo;Vitamin D: The &lsquo;sunshine&rsquo; vitamin,&rdquo; </span><em><span style="font-weight: 400;">J. Pharmacol. Pharmacother.</span></em><span style="font-weight: 400;">, vol. 3, no. 2, pp. 118&ndash;126, Apr. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[10]</span> <a href="http://paperpile.com/b/zC8saG/m939"><span style="font-weight: 400;">M. F. Holick, &ldquo;Vitamin D: A millenium perspective,&rdquo; </span><em><span style="font-weight: 400;">J. Cell. Biochem.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, pp. 296&ndash;307, Feb. 2003.</span></a></p>
                            <p><span style="font-weight: 400;">[11]</span> <a href="http://paperpile.com/b/zC8saG/MIuP"><span style="font-weight: 400;">A. R. Martineau </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D for the management of asthma,&rdquo; in </span><em><span style="font-weight: 400;">Cochrane Database of Systematic Reviews</span></em><span style="font-weight: 400;">, John Wiley &amp; Sons, Ltd, 1996.</span></a></p>
                            <p><span style="font-weight: 400;">[12]</span> <a href="http://paperpile.com/b/zC8saG/lqLQ"><span style="font-weight: 400;">M. Urashima, T. Segawa, M. Okazaki, M. Kurihara, Y. Wada, and H. Ida, &ldquo;Randomized trial of vitamin D supplementation to prevent seasonal influenza A in schoolchildren,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 91, no. 5, pp. 1255&ndash;1260, May 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[13]</span> <a href="http://paperpile.com/b/zC8saG/CUlr"><span style="font-weight: 400;">E. M. Hollams </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D and atopy and asthma phenotypes in children: a longitudinal cohort study,&rdquo; </span><em><span style="font-weight: 400;">Eur. Respir. J.</span></em><span style="font-weight: 400;">, vol. 38, no. 6, pp. 1320&ndash;1327, Dec. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[14]</span> <a href="http://paperpile.com/b/zC8saG/Kv05"><span style="font-weight: 400;">A. Bener, M. S. Ehlayel, M. K. Tulic, and Q. Hamid, &ldquo;Vitamin D deficiency as a strong predictor of asthma in children,&rdquo; </span><em><span style="font-weight: 400;">Int. Arch. Allergy Immunol.</span></em><span style="font-weight: 400;">, vol. 157, no. 2, pp. 168&ndash;175, 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[15]</span> <a href="http://paperpile.com/b/zC8saG/9YsT"><span style="font-weight: 400;">G. Paul, J. M. Brehm, J. F. Alcorn, F. Holgu&iacute;n, S. J. Aujla, and J. C. Celed&oacute;n, &ldquo;Vitamin D and asthma,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Respir. Crit. Care Med.</span></em><span style="font-weight: 400;">, vol. 185, no. 2, pp. 124&ndash;132, Jan. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[16]</span> <a href="http://paperpile.com/b/zC8saG/S8xn"><span style="font-weight: 400;">S. J. Niruban, K. Alagiakrishnan, J. Beach, and A. Senthilselvan, &ldquo;Association between vitamin D and respiratory outcomes in Canadian adolescents and adults,&rdquo; </span><em><span style="font-weight: 400;">J. Asthma</span></em><span style="font-weight: 400;">, vol. 52, no. 7, pp. 653&ndash;661, Sep. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[17]</span> <a href="http://paperpile.com/b/zC8saG/vxKf"><span style="font-weight: 400;">N. E. Lange, A. Litonjua, C. M. Hawrylowicz, and S. Weiss, &ldquo;Vitamin D, the immune system and asthma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 5, no. 6, pp. 693&ndash;702, Nov. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[18]</span> <a href="http://paperpile.com/b/zC8saG/bBQQ"><span style="font-weight: 400;">A. T. Kho </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D related genes in lung development and asthma pathogenesis,&rdquo; </span><em><span style="font-weight: 400;">BMC Med. Genomics</span></em><span style="font-weight: 400;">, vol. 6, p. 47, Nov. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[19]</span> <a href="http://paperpile.com/b/zC8saG/MpVc"><span style="font-weight: 400;">M. Bashir </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Effects of high doses of vitamin D3 on mucosa-associated gut microbiome vary between regions of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 55, no. 4, pp. 1479&ndash;1489, Jun. 2016.</span></a></p>
                            <p><span style="font-weight: 400;">[20]</span> <a href="http://paperpile.com/b/zC8saG/S0Bu"><span style="font-weight: 400;">M. F. Luxwolda, R. S. Kuipers, I. P. Kema, D. A. J. Dijck-Brouwer, and F. A. J. Muskiet, &ldquo;Traditionally living populations in East Africa have a mean serum 25-hydroxyvitamin D concentration of 115 nmol/l,&rdquo; </span><em><span style="font-weight: 400;">Br. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 108, no. 9, pp. 1557&ndash;1561, Nov. 2012.</span></a></p>
                            <p><span style="font-weight: 400;">[21]</span> <a href="http://paperpile.com/b/zC8saG/tPF1"><span style="font-weight: 400;">R. Vieth, &ldquo;Why the minimum desirable serum 25-hydroxyvitamin D level should be 75 nmol/L (30 ng/ml),&rdquo; </span><em><span style="font-weight: 400;">Best Pract. Res. Clin. Endocrinol. Metab.</span></em><span style="font-weight: 400;">, vol. 25, no. 4, pp. 681&ndash;691, Aug. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[22]</span> <a href="http://paperpile.com/b/zC8saG/iVAw"><span style="font-weight: 400;">&ldquo;Most S. Calif lifeguards have good level of vitamin D (above 40 ng) - Aug 2016 | Vitamin D Wiki.&rdquo; [Online]. Available: </span></a><a href="http://paperpile.com/b/zC8saG/iVAw"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[23]</span> <a href="http://paperpile.com/b/zC8saG/n75t"><span style="font-weight: 400;">B. W. Hollis </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Maternal Versus Infant Vitamin D Supplementation During Lactation: A Randomized Controlled Trial,&rdquo; </span><em><span style="font-weight: 400;">Pediatrics</span></em><span style="font-weight: 400;">, vol. 136, no. 4, pp. 625&ndash;634, Oct. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[24]</span> <a href="http://paperpile.com/b/zC8saG/696F"><span style="font-weight: 400;">R. P. Heaney, &ldquo;Vitamin D in health and disease,&rdquo; </span><em><span style="font-weight: 400;">Clin. J. Am. Soc. Nephrol.</span></em><span style="font-weight: 400;">, vol. 3, no. 5, pp. 1535&ndash;1541, Sep. 2008.</span></a></p>
                            <p><span style="font-weight: 400;">[25]</span> <a href="http://paperpile.com/b/zC8saG/aump"><span style="font-weight: 400;">C. Palacios and L. Gonzalez, &ldquo;Is vitamin D deficiency a major global public health problem?,&rdquo; </span><em><span style="font-weight: 400;">J. Steroid Biochem. Mol. Biol.</span></em><span style="font-weight: 400;">, vol. 144 Pt A, pp. 138&ndash;145, Oct. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[26]</span> <a href="http://paperpile.com/b/zC8saG/nasP"><span style="font-weight: 400;">Z. Naeem, &ldquo;Vitamin d deficiency- an ignored epidemic,&rdquo; </span><em><span style="font-weight: 400;">Int. J. Health Sci. </span></em><span style="font-weight: 400;">, vol. 4, no. 1, pp. V&ndash;VI, Jan. 2010.</span></a></p>
                            <p><span style="font-weight: 400;">[27]</span> <a href="http://paperpile.com/b/zC8saG/OjLt"><span style="font-weight: 400;">&ldquo;THE GLOBAL EPIDEMIOLOGY OF VITAMIN D STATUS &bull; JARCP The Journal of Aging Research &amp; Clinical Practice.&rdquo; [Online]. Available: </span></a><a href="http://www.jarcp.com/703-the-global-epidemiology-of-vitamin-d-status.html"><span style="font-weight: 400;">http://www.jarcp.com/703-the-global-epidemiology-of-vitamin-d-status.html</span></a><a href="http://paperpile.com/b/zC8saG/OjLt"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[28]</span> <a href="http://paperpile.com/b/zC8saG/1bVf"><span style="font-weight: 400;">K. Y. Z. Forrest and W. L. Stuhldreher, &ldquo;Prevalence and correlates of vitamin D deficiency in US adults,&rdquo; </span><em><span style="font-weight: 400;">Nutr. Res.</span></em><span style="font-weight: 400;">, vol. 31, no. 1, pp. 48&ndash;54, Jan. 2011.</span></a></p>
                            <p><span style="font-weight: 400;">[29]</span> <a href="http://paperpile.com/b/zC8saG/jNzp"><span style="font-weight: 400;">K. Sarafin </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Standardizing 25-hydroxyvitamin D values from the Canadian Health Measures Survey,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 102, no. 5, pp. 1044&ndash;1050, Nov. 2015.</span></a></p>
                            <p><span style="font-weight: 400;">[30]</span> <a href="http://paperpile.com/b/zC8saG/JjZm"><span style="font-weight: 400;">T. K. Gill </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Vitamin D levels in an Australian population,&rdquo; </span><em><span style="font-weight: 400;">BMC Public Health</span></em><span style="font-weight: 400;">, vol. 14, p. 1001, Sep. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[31]</span> <a href="http://paperpile.com/b/zC8saG/qWaL"><span style="font-weight: 400;">R. P. van der Wielen </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Serum vitamin D concentrations among elderly people in Europe,&rdquo; </span><em><span style="font-weight: 400;">Lancet</span></em><span style="font-weight: 400;">, vol. 346, no. 8969, pp. 207&ndash;210, Jul. 1995.</span></a></p>
                            <p><span style="font-weight: 400;">[32]</span> <a href="http://paperpile.com/b/zC8saG/PD35"><span style="font-weight: 400;">J. Hilger </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;A systematic review of vitamin D status in populations worldwide,&rdquo; </span><em><span style="font-weight: 400;">Br. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 111, no. 1, pp. 23&ndash;45, Jan. 2014.</span></a></p>
                            <p><span style="font-weight: 400;">[33]</span> <a href="http://paperpile.com/b/zC8saG/UdPK"><span style="font-weight: 400;">&ldquo;Office of Dietary Supplements - Vitamin D.&rdquo; [Online]. Available: </span></a><a href="https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/"><span style="font-weight: 400;">https://ods.od.nih.gov/factsheets/VitaminD-HealthProfessional/</span></a><a href="http://paperpile.com/b/zC8saG/UdPK"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[34]</span> <a href="http://paperpile.com/b/zC8saG/T7EM"><span style="font-weight: 400;">S. A. Linnebur, S. F. Vondracek, J. P. Vande Griend, J. M. Ruscin, and M. T. McDermott, &ldquo;Prevalence of vitamin D insufficiency in elderly ambulatory outpatients in Denver, Colorado,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Geriatr. Pharmacother.</span></em><span style="font-weight: 400;">, vol. 5, no. 1, pp. 1&ndash;8, Mar. 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[35]</span> <a href="http://paperpile.com/b/zC8saG/Sai4"><span style="font-weight: 400;">F. Alshahrani and N. Aljohani, &ldquo;Vitamin D: deficiency, sufficiency and toxicity,&rdquo; </span><em><span style="font-weight: 400;">Nutrients</span></em><span style="font-weight: 400;">, vol. 5, no. 9, pp. 3605&ndash;3616, Sep. 2013.</span></a></p>
                            <p><span style="font-weight: 400;">[36]</span> <a href="http://paperpile.com/b/zC8saG/Z7wE"><span style="font-weight: 400;">J. N. Hathcock, A. Shao, R. Vieth, and R. Heaney, &ldquo;Risk assessment for vitamin D,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 85, no. 1, pp. 6&ndash;18, Jan. 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[37]</span> <a href="http://paperpile.com/b/zC8saG/zWRq"><span style="font-weight: 400;">J. Reichrath and B. N&uuml;rnberg, &ldquo;Cutaneous vitamin D synthesis versus skin cancer development: The Janus faces of solar UV-radiation,&rdquo; </span><em><span style="font-weight: 400;">Dermatoendocrinol.</span></em><span style="font-weight: 400;">, vol. 1, no. 5, pp. 253&ndash;261, Sep. 2009.</span></a></p>
                            <p><span style="font-weight: 400;">[38]</span> <a href="http://paperpile.com/b/zC8saG/Kyry"><span style="font-weight: 400;">B. Anna, Z. Blazej, G. Jacqueline, C. J. Andrew, R. Jeffrey, and S. Andrzej, &ldquo;Mechanism of UV-related carcinogenesis and its contribution to nevi/melanoma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Dermatol.</span></em><span style="font-weight: 400;">, vol. 2, no. 4, pp. 451&ndash;469, 2007.</span></a></p>
                            <p><span style="font-weight: 400;">[39]</span> <a href="http://paperpile.com/b/zC8saG/KddZ"><span style="font-weight: 400;">&ldquo;Browse the Tables and Figures - SEER Cancer Statistics Review (CSR) 1975-2010.&rdquo; [Online]. Available: </span></a><a href="https://seer.cancer.gov/archive/csr/1975_2010/browse_csr.php?section=16&amp;page=sect_16_table.05.html"><span style="font-weight: 400;">https://seer.cancer.gov/archive/csr/1975_2010/browse_csr.php?section=16&amp;page=sect_16_table.05.html</span></a><a href="http://paperpile.com/b/zC8saG/KddZ"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[40]</span> <a href="http://paperpile.com/b/zC8saG/2WRE"><span style="font-weight: 400;">&ldquo;Skin Cancer Research Highlights.&rdquo; [Online]. Available: </span></a><a href="https://www.cancer.org/research/skin-cancer-research-highlights.html"><span style="font-weight: 400;">https://www.cancer.org/research/skin-cancer-research-highlights.html</span></a><a href="http://paperpile.com/b/zC8saG/2WRE"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><span style="font-weight: 400;">[41]</span> <a href="http://paperpile.com/b/zC8saG/wLGR"><span style="font-weight: 400;">&ldquo;Vitamin D and Cancer Prevention,&rdquo; </span><em><span style="font-weight: 400;">National Cancer Institute</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="https://www.cancer.gov/about-cancer/causes-prevention/risk/diet/vitamin-d-fact-sheet"><span style="font-weight: 400;">https://www.cancer.gov/about-cancer/causes-prevention/risk/diet/vitamin-d-fact-sheet</span></a><a href="http://paperpile.com/b/zC8saG/wLGR"><span style="font-weight: 400;">. [Accessed: 22-Jan-2017].</span></a></p>
                            <p><br /><br /></p>
                        </div>
                    </div>-->
                </div>
            </div>

        </div>
        <!-- sectiune capitol     -->
    </div>
</div>
<!--  card     -->


<!--  tabe    -->
