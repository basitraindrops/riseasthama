<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>
        <div class="textscrollsummary textsummaryfirst" data-Texttype="1">
            <div class="nuttshell">

                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p>We have evolved using Vitamin D as a cornerstone of our health, regulating almost 3,000 genes in us and playing a vital role in maintaining the proper functioning of the primary systems of our body.</p>
                    <p>There are a variety of scientific studies showing the benefits of Vitamin D on our general health, as well as in treating asthma and allergies. </p>
                    <p>It is estimated that more than one-third of the global population nowadays is Vitamin D deficient, caused by getting minimal sun exposure given our modern lifestyle. The optimal Vitamin D blood levels in humans are around 55 ng/ml. To reach these levels, it is recommended to: </p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Take Vitamin D supplements</h3>

                        <ul class="general-list">
                            <li>Children - 1,000 IU of Vitamin D a day for every 25 lbs of their weight, rounded up. </li>
                            <li>Adults - 5,000 IU of Vitamin D a day; People also go up to 10,000 IU of vitamin D a day which is unlikely to do any harm.</li>
                            <li>Pregnant mothers - 6,000 IU of Vitamin D a day. </li>

                        </ul>
                    </div>
                </div>
                <div class="do2">
                    <div class="dodont-content">

                        <h3>Expose yourself to sun frequently but with moderation</h3>

                        <ul class="general-list">
                            <li>Be careful not to get a sunburn. Start low and then gradually increase the duration of sun exposure according to your skin tolerance. A good indicator is to remain exposed for half the time required for your skin to turn pink. Then, cover up with clothing and go into the shade.</li>
                            <li>Don’t apply any sunscreen while exposing to the sun.</li>
                            <li>The best time to sunbath is in the afternoon, between 1-5 PM. Try to avoid the intense sun at noon.</li>
                            <li>People with dark skin need about 5-6 times the duration of sun exposure compared in order to fair-skinned individuals to synthesize the same amount of Vitamin D.</li>
                            <li>Combine sun exposure with exercise and other stress management techniques that will help treat your asthma - see <a href="/managestress.php">Chapter 4 - Manage Stress</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="textscrollsummary" data-Texttype="2">
            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">Vitamin D - A Cornerstone of our Body</span></h2>

                <p><span style="font-weight: 400;">Vitamin D - or the sunshine vitamin - is essential for life.</span></p>
                <p><span style="font-weight: 400;">We are living in a complex ecosystem interconnecting all the forms of being through an energy flow that starts with solar energy. This life force is transferred from the sun to plants, through photosynthesis and finally to animals and humans.</span></p>
                <p><span style="font-weight: 400;">Our species has a unique way to synthesize Vitamin D. When the UVB rays from the sun strike our skin, they set off a chemical reaction pre-programmed in our DNA. After produced, it enters the bloodstream until it reaches our vital organs, where it plays an important role in the proper functioning of the primary structures of our body.</span></p>
                <p><span style="font-weight: 400;">There is evidence that almost 3,000 genes (out of 25,000) in our body have Vitamin D receptors that upregulate them.</span></p>



                <h2><span style="font-weight: 400;">The Vitamin D's Impact on Allergies and Asthma</span></h2>

                <p><span style="font-weight: 400;">Since its first medical use at the beginning of the 20th century (for the treatment of rickets), Vitamin D has continually proved its efficiency for countless human disorders, from cardiovascular to bone health and from depression to autoimmune diseases.</span></p>
                <p><span style="font-weight: 400;">Asthma is one of these disorders.</span></p>
                <p><span style="font-weight: 400;">High-quality evidence from numerous international studies suggests that Vitamin D plays a conclusive role in the treatment of asthma and maintaining optimal health.</span></p>
                <p><strong><span style="font-weight: 400;">Researchers believe that Vitamin D is promoting T Cells that control the immune response and inhibit the airway inflammation and hyperresponsiveness. Indirectly, Vitamin D is controlling the immune system by increasing the bacterial richness in the upper gastrointestinal tract and reducing opportunistic pathogens opportunity to thrive. In addition, sun exposure is a proven efficient way to keep our stress levels low. Excessive levels of stress are linked to increased prevalence of asthma and allergy symptoms. </span></strong></p>


                <h2><span style="font-weight: 400;">Vitamin D Optimal Levels and Global Deficiency</span></h2>


                <p><span style="font-weight: 400;">The most accurate way to measure how much Vitamin D our body has, is the 25-hydroxy Vitamin D blood test. The result is usually displayed in nanograms per milliliter - ng/mL.</span></p>
                <p><span style="font-weight: 400;">There has been an ongoing debate about what the optimal Vitamin D blood levels value should be. To shed light on the matter, researchers have conducted a brilliant study in what is believed to be the cradle of humanity - Eastern Africa - among two hunter-gatherer tribes that are among the last remainders living the lifestyle of our ancestors.</span></p>
                <p><span style="font-weight: 400;">Their mean Vitamin D blood level is 44 ng/ml reaching up to 52 ng/ml in some individuals.</span></p>
                <p><span style="font-weight: 400;">There is also other evidence that supports these findings:</span></p>
                <p><span style="font-weight: 400;">Our closest &ldquo;simian relative&rdquo;, the chimpanzee, has levels between 40 - 60 ng/ml. Lifeguards in summertime have 40 - 60 ng/ml levels without taking any supplements. It takes a mother having a level of between 40 - 60 ng/ml herself to then have Vitamin D show up in breast milk for her baby.</span></p>
                <p><span style="font-weight: 400;">But where do we stand nowadays?</span></p>
                <p><span style="font-weight: 400;">The average Vitamin D level in the </span><strong>United States</strong><span style="font-weight: 400;"> in 2004 was 24 ng/ml. A nationally representative survey from </span><strong>Canada</strong><span style="font-weight: 400;"> concluded the mean concentration of 25(OH)D in this population was 21 ng/ml. In </span><strong>Australia</strong><span style="font-weight: 400;">, the percentage of people having less than 40 ng/mg was 86% and 60% have dangerous levels of &lt;20 ng/mg. </span></p>
                <p><span style="font-weight: 400;">In </span><strong>Europe</strong><span style="font-weight: 400;">, 46% of the women and 36% of the men had levels of 10 ng/ml and it decreases with age.</span></p>
                <p><strong><span style="font-weight: 400;">All in all, it is believed that two-thirds of the global population nowadays is considered to be Vitamin D deficient. </span></strong></p>



                <h2><span style="font-weight: 400;">How do we Become Vitamin D Deficient</span></h2>

                <p>Vitamin D is accumulated in our body through sun exposure and diet. Modern lifestyle is the culprit of the Vitamin D deficiency. Millions of people restrict their sun exposure to a couple of minutes a day. When they do it, they use sunscreen and wear clothes that further fends off the Vitamin D from being produced by the skin. Their diet is low on Vitamin D and high on health-damaging foods.</p>


                <h2><span style="font-weight: 400;">How to Restore our Health using Vitamin D?</span></h2>

                <p><span style="font-weight: 400;">The first step in restoring the optimal levels of Vitamin D to 50 ng/ml is to determine your current baseline values.</span></p>
                <p><strong><span style="font-weight: 400;">This can be achieved through a simple blood test. However, the problem is that most people will not have their blood tested unless their doctor recommends it. </span></strong></p>


                <h2><span style="font-weight: 400;">Take Vitamin D Supplements</span></h2>

                <p><span style="font-weight: 400;">For individuals that cannot have their blood levels tested, we suggest a recommended dose that is easy to obtain at most pharmacies and will get most people above 40 ng/ml and close to around 50 ng/ml, but will not cause anyone to get to toxic levels.</span></p>
                <p><span style="font-weight: 400;">RISE follows </span><span style="font-weight: 400;">The Vitamin D Council&rsquo;s</span><span style="font-weight: 400;"> - a fascinating science backed non profit organization - recommendations:</span></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><strong>Children</strong><span style="font-weight: 400;"> - &nbsp;1,000 IU of Vitamin D a day for every 25 lbs of their weight, rounded up. </span></li>
                    <li style="font-weight: 400;"><strong>Adults </strong><span style="font-weight: 400;">- &nbsp;5,000 IU of Vitamin D a day; People also go up to 10,000 IU of vitamin D a day which is unlikely to do any harm.</span></li>
                    <li style="font-weight: 400;"><strong>Pregnant mothers</strong><span style="font-weight: 400;"> - &nbsp;6,000 IU of Vitamin D a day. </span></li>
                </ul>
                <p><span style="font-weight: 400;">Note: This dosage is higher than the currently advisable one of 400-600 I.U. by various medical associations. However,  many scientific studies have clearly demonstrated these recommendations are outdated. </span></p>

                <h2><span style="font-weight: 400;">Expose your Skin to the Sun Frequently but with Moderation</span></h2>

                <p>The amount of sun exposure that can provide you with enough Vitamin D depends on latitude, time of the day, altitude, season, age, skin pigment, and if you use sunscreen or not. Therefore, it is not scientifically correct to recommend a universal dosage. However, we can provide some general guidelines to follow:</p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Be careful not to get sunburned. Start low and then gradually increase the duration of sun exposure, according to your skin tolerance. A good indicator is half the time required for your skin to turn pink. Then, cover up with clothing and go into the shade.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">The best time to sunbath is in the afternoon, between 1-5 PM. Try to avoid the intense sun at noon.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Don’t apply any sunscreen while exposing yourself to the sun.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">People with dark skin need about 5-6 times the duration of sun exposure as compared to fair-skinned individuals to synthesize the same amount of Vitamin D. </span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Combine sun exposure with exercise and other stress management techniques that will help treat your asthma.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">The same general recommendations apply for children, but please consult with a paediatrician before exposing your kids to sun.</span></li>
                </ul>

            </div>
        </div>
    </div>
</div>
