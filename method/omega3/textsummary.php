<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>
        <div class="textscrollsummary textsummaryfirst" data-Texttype="1">

            <div class="nuttshell">
                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p>To accommodate the high energy demands of our large brains, early humans changed their diet to one that contained natural fats. Gradually, fats became a crucial element in forming the main structures of our central nervous system and providing the building blocks for the immune system. While living in nature provided us with a perfect balance between different kind of fats, the modern lifestyle has totally changed things. The Omega 3 to Omega 6 fatty acids ratio that used to be 1:1 is nowadays somewhere between 1:20 to 1:40. This imbalance is causing multiple health disorders and negatively affects our asthma symptoms. To correct this imbalance, we have to increase the Omega 3 intake and decrease the ingestion of Omega 6.
                    </p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Increase Your Omega 3 Intake</h3>

                        <ul class="general-list">
                            <li>Take 3 to 4 g of a high quality Omega 3 fish oil per day if you don’t make any changes to the Omega 6 intake</li>
                            <li>Take 1 g of Omega 3 fish oil per day if you reduce your Omega 6 intake;</li>

                        </ul>
                    </div>
                </div>
                <div class="dont">
                    <div class="dodont-content">

                        <h3>Decrease Your Omega 6 Intake</h3>

                        <ul class="general-list">
                            <li>Avoid processed seed and vegetable oils, as well as all the processed foods that contain them. Sunflower, Corn, Soybean and Cottonseed oils are by far the worst. Better options are coconut oil, butter, or olive oil.</li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div class="textscrollsummary" data-Texttype="2">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The Importance of Fats in our Species’ Brain Development</span></h2>

                <p><span style="font-weight: 400;">To accommodate the high energy demands of our large brains, early humans changed their diet to one that was three to four times denser in energy than those of our primate kin. This new diet contained natural fats, and it is one of the perks we received from hunting for wild game and fishing.</span></p>
                <p><span style="font-weight: 400;">Gradually, our bodies evolved to employ fat, not only as fuel but also for many other vital functions. They form the main structures of the brain, central nervous system, and cell membranes, and provide the building blocks of our immune system. They help absorb certain nutrients such Vitamins A, D, E and K and antioxidants. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Fats come in different forms: saturated and unsaturated. Among the later, some are essentials fats (the body cannot synthesize them) such as Omega 3 and Omega 6.</span></p>
                <p><span style="font-weight: 400;">The beauty of living in nature is that it supplied us with the perfect ratio between these types of fat. For example, we have evolved consuming an Omega 3 - Omega 6 fats ratio of somewhere between 1:1 to 4:1. Back then, we were in excellent health and didn&rsquo;t suffer from the chronic diseases that are currently killing Westerners by the millions.</span></p>
                <p><span style="font-weight: 400;">Even if they are responsible for the optimal functioning of our body, the natural fats have been a subject of debate for the last 50 years. To shed light on the issue, we have to take context into consideration.</span></p>
                <p><span style="font-weight: 400;">First, excessive consumption of fats (similar to water or oxygen) could lead to severe health disorders. They should be consumed in moderation and according to one&rsquo;s lifestyle.</span></p>
                <p><span style="font-weight: 400;">Secondly, the problem with fats occurs when the ratio between them deviates from the natural balance required by our body to function optimally. With the typical Western diet, we changed the natural 1:1 Omega 3 to Omega 6 ratio to somewhere between 1:20 and 1:50.</span></p>
                <p><span style="font-weight: 400;">Researchers around the world have started to link this Omega 3 deficiency to obesity, heart problems, cancer, arthritis, poor memory, dry skin, mood swings or depression, poor circulation, and others.</span></p>
                <p><span style="font-weight: 400;">One of these disorders is the one that interests you the most: allergies and asthma.</span></p>


                <h2><span style="font-weight: 400;">Omega 3, Allergies and Asthma</span></h2>

                <p><span style="font-weight: 400;">The story of Omega 3 and asthma begins in 1970 when researchers first connected an essential fatty acid imbalance to autoimmune diseases. Since then, a body of international research has started exploring this connection in depth.</span></p>
                <p><span style="font-weight: 400;">There are now three (known) related links between Omega 3, allergies, and asthma.</span></p>
                <p><span style="font-weight: 400;">First, Omega 3 exercises a direct influence on our immune system by enhancing the activity of white blood cells known as B cell.</span></p>
                <p><span style="font-weight: 400;">Second, Omega 3 plays a crucial role in stress and anger management that can indirectly lead to asthma. Last, Omega 3 helps plug the &ldquo;leaky&rdquo; gut - &nbsp;the intestinal disorder related with the inception and aggravations of asthma and allergies in our species.</span></p>


                <h2><span style="font-weight: 400;">Fix the Omega 3 - Omega 6 Imbalance</span></h2>
                <p><span style="font-weight: 400;">The important thing to consider is any recommendation for Omega 3 intake that does not take the background of Omega 6 consumption into account is wholly inadequate. So, we have to address both.</span></p>


                <h3><span style="font-weight: 400;">How to Increase your Omega 3 Intake:</span></h3>

                <p>The simplest way to increase your Omega 3 intake is with fish oil supplements (the other option is through diet). &nbsp;</p>
                <p><span style="font-weight: 400;">However, fish oils supplement are varying in quality. There is a substantial difference in the ingredients and therapeutic benefit of what is available today. Various important factors to consider are purity, freshness, potency, sustainability, cost, etc.</span></p>
                <p><span style="font-weight: 400;">The best approach is to always choose the highest quality fish oil supplement you can afford.</span></p>
                <p><span style="font-weight: 400;">Quantity-wise, we have to take Omega 6 into consideration:</span></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Take </span><strong>3 to 4 g</strong><span style="font-weight: 400;"> of Omega 3 fish oil per day if you don&rsquo;t make any changes to the Omega-6 intake;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Take </span><strong>1 g</strong><span style="font-weight: 400;"> of Omega 3 fish oil per day if you reduce your Omega 6 intake;</span></li>
                </ul>

                <h3><span style="font-weight: 400;">How to Decrease your Omega 6 Intake</span></h3>

                <p><span style="font-weight: 400;">The single most important thing you can do to reduce your Omega 6 intake is to avoid processed seed and vegetable oils high in Omega 6, as well as all the processed foods that contain them. </span></p>
                <p><span style="font-weight: 400;">Sunflower, Corn, Soybean and Cottonseed oils are by far the worst. </span></p>
                <p><span style="font-weight: 400;">Not only should you not cook with them, but it is best to avoid them altogether.</span></p>
                <p><span style="font-weight: 400;">As an alternative, when it comes to high heat cooking, coconut oil is the wisest choice. This oil is semi-solid at room temperature, and it can last for months and years without going rancid. Butter and olive oil come next.</span></p>
                <p><span style="font-weight: 400;">On a side note, nuts and seeds are pretty high in Omega 6, but they are whole foods that have plenty of health benefits. They are fine to eat as long as if you are eating adequate amounts of Omega 3 fatty acids.</span></p>


                <h2><span style="font-weight: 400;">For Children</span></h2>

                <p><span style="font-weight: 400;">First, a pure, high-quality Omega 3 fish oil should be included in a woman&rsquo;s supplement regimen before and during pregnancy, as well as during </span><a href="http://americanpregnancy.org/first-year-of-life/breastfeeding-overview/"><span style="font-weight: 400;">breastfeeding</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Children can have anywhere from 500 to 1,200 milligrams of fish oil per day, depending on their age, height, and weight. They can take fish oil capsules straight, or you can mix it in with their food.</span></p>
                <p><span style="font-weight: 400;">Please consult with your pediatrician before giving your children Omega 3 supplements.</span></p>
                <p><span style="font-weight: 400;">As far as Omega 6 is concerned, the same principles apply. Don&rsquo;t feed your kids food fried in vegetable seed oils. </span></p>

            </div>
        </div>
    </div>
</div>
