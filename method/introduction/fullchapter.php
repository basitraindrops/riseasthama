<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">

                <h1>The Origins and Underlying Causes of Asthma and Allergies</h1>

            </div>

            <div class="video-player video-container">
                <object id="vzvd-10663764" height="auto" style="width: 100%;">
                    <param name="allowScriptAccess" value="always" /> 
                    <param name="allowFullScreen" value="true" /> 
                    <param name="wmode" value="transparent" />
                    <param name="flashvars" value="showplaybutton=true" />
                    <video  height="auto" style="width: 100%;" poster="//view.vzaar.com/9890690/image" preload="none" controls="controls" onclick="this.play();" id="vzvid" >                        
                            <source src="//view.vzaar.com/10663764/video">
                    </video>
                </object>
            </div>
            <div class="nuttshell">

                <div class="nutshell-content">

                    <h2>Intro</h2>

                    <p><span style="font-weight: 400;">Asthma is frustrating and complex. Nevertheless, it is not a complete mystery as some still claim, and it can be conquered by knowledge.</span></p>
                    <p><span style="font-weight: 400;">RISE is a scientific and functional method. It is going to explain the real underlying causes of this disease and how to address them.</span></p>
                    <p><span style="font-weight: 400;">Some doctors and other experts say that asthma is caused by inflammation. We agree, but also ask &ldquo;what triggers inflammation&rdquo;? After doing intense scientific research, we realized there are multiple factors, including the &ldquo;increased intestinal permeability&rdquo; - simply called &ldquo;leaky gut&rdquo;. Further, we investigated what caused the leaky gut to occur and why it happened to our species in the first place. For answers, we had to look even deeper into our long evolutionary process. The logic is that we have evolved for 2.5 million years and how we lived during this extended period of time shaped the way our body should function.</span></p>
                    <p><span style="font-weight: 400;">You are going to hear a lot about the &ldquo;Agricultural Revolution&rdquo; that totally changed the game for our species, about sun and Vitamin D, about the Omega 3 to Omega 6 fatty acids ratio, the intestinal bacterial flora, and the importance of managing our stress levels.</span></p>
                    <p><span style="font-weight: 400;">The more I explored these topics, the more fascinated I became on how elements interconnect in a logical and beautiful way.</span>&nbsp;</p>
                    <p><span style="font-weight: 400;">When I acted on my discoveries, I managed to eliminate asthma from my life for good after 24 years of struggling, and I am confident that with a small ounce of effort, you will be able to do it as well. </span></p>

                </div>

            </div>


            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The Journey</span></h2>

                <p><span style="font-weight: 400;">Our journey starts with the sun. For 4.5 billion years it has shone on the surface of this planet, and all the energy available to life derives from it.</span></p>
                <p><span style="font-weight: 400;">In this state of affairs, we - Homo Sapiens - have evolved. It happened somewhere in Eastern Africa, a bright sunny place, around 200,000 years ago.</span></p>
                <p><span style="font-weight: 400;">We were given an amazing body of immense complexity and adapted to live in perfect harmony with our surroundings. Our large brain was designed to carry out multiple activities to help us survive in nature. Our digestive system evolved to process the food available to us. Our lungs, heart and locomotor system allowed us to run, swim, climb, to gather edible plants and hunt for food.</span></p>
                <p><span style="font-weight: 400;">And, our DNA code was designed to process sunlight.</span></p>
                <p><span style="font-weight: 400;">Type B Ultraviolet rays (UVB) from the sun act on our skin to produce a hormone called 7-dehydrocholesterol. It then leaves the skin and gets into the bloodstream until it reaches our vital organs. We know this hormone better as Vitamin D. </span><strong>It is a cornerstone of every part of our body </strong><a href="https://paperpile.com/c/oJce8t/x4Sa"><span style="font-weight: 400;">[1]</span></a><strong>. </strong></p>


            </div>

            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/introduction/1.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="2">
            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">The Game Changer</span></h2>

                <p><span style="font-weight: 400;">A couple of events started to happen gradually.</span></p>
                <p><span style="font-weight: 400;">First, the Cognitive Revolution began. Gradually, we evolved to become more efficient, smarter and able to cooperate in large numbers. Strong groups started to migrate further north looking for food, defeating competition (like our cousins - Homo Neanderthalis) and conquering the entire planet </span><a href="https://paperpile.com/c/oJce8t/2D3G"><span style="font-weight: 400;">[2]</span></a><span style="font-weight: 400;"> . </span></p>
                <p><span style="font-weight: 400;">When we reached colder regions, where the sun was less potent, there was a need for adaptation. We manufactured clothes that helped us survive, but that also prevented the sun from reaching our skin. Since the Vitamin D intake needed to be optimized for normal functioning, our body decreased the level of melanin, a dermal pigment, that serves as a natural sunscreen. Our skin got whiter as a consequence, and our Vitamin D levels started to subside </span><a href="https://paperpile.com/c/oJce8t/QtoB"><span style="font-weight: 400;">[3]</span></a><span style="font-weight: 400;"> .</span></p>
                <p><span style="font-weight: 400;">Then, armed with a brain always looking for efficiency, about 10,000 BC, the Agricultural Revolution occurred. This one was a real game changer: we domesticated plants and animals and went from being hunters and gatherers to living a more sedentary lifestyle </span><a href="https://paperpile.com/c/oJce8t/2D3G"><span style="font-weight: 400;">[2]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Little by little, we traded our old ways of living for new, apparently more convenient, ways. We spent less time in nature and more time inside the house. We didn't run naked anymore but instead cared for animals while wearing clothes.</span></p>
                <p><strong><span style="font-weight: 400;">Our diet also changed drastically. We started to eat less healthy fats and vegetables and more carbohydrates. Grains were an easy, plentiful source of calories for people, and milk was readily available. </span><strong>However, the new diets did not agree with everyone.</strong></strong>
                </p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/introduction/2.png">
            <!-- image    -->

            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">The Brain-Gut Connection and Intestinal Barrier</span></h2>

                <p><span style="font-weight: 400;">To appreciate what happened, we need to understand than in the evolutionary process, nature has given us two distinct &ldquo;brains,&rdquo; perfectly synchronized.</span></p>
                <p><span style="font-weight: 400;">Hidden in the walls of the digestive system, the second brain is our gut. The one that gives us "butterflies" when we are nervous </span><a href="https://paperpile.com/c/oJce8t/2cGj"><span style="font-weight: 400;">[4]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Besides its connection with the brain, the gut is crucial to our overall health. It allows properly digested food to pass into the bloodstream so it may be used for fuel. The gut also acts as a barrier to keep out foreign invaders. Along with other elements, this tight junction of cells uses Vitamin D to function properly. </span><strong>Vitamin D increases the bacterial richness in the upper gastrointestinal tract and does not allow opportunistic pathogens to thrive </strong><a href="https://paperpile.com/c/oJce8t/26lC"><span style="font-weight: 400;">[5]</span></a><strong>. </strong></p>
                <p><span style="font-weight: 400;">Now, with less Vitamin D in our body and the continuous assimilation of large amounts of still unfamiliar foods, the first cracks in the barrier started to occur. For some of us, the gut became leaky. The result was poorly digested food particles flooding our system.</span></p>
                <p><span style="font-weight: 400;">Next, the immune system woke up, and it was not happy. It started building up antibodies (the soldiers it sends to fight the threat) to certain foods that got leaked. The moment those foods were eaten again, the body immediately recognized them as foreign, and the inflammation reaction took place </span><a href="https://paperpile.com/c/oJce8t/z0oW"><span style="font-weight: 400;">[6]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Another problem occurred. Pathogens, which had once been exclusive to animals, leaped the species gap and mutated into human diseases. The hunter/gatherers frequently shifted camp and left behind disease-causing agents, including human waste which accumulates microbes and worm larvae. In the new circumstances, the human population became large and concentrated enough to evolve and sustain these crowd diseases. </span></p>
                <p><span style="font-weight: 400;">Current information suggests that 8 of the temperate diseases (diphtheria, influenza A, measles, mumps, pertussis, rotavirus, smallpox, tuberculosis) probably reached humans from domestic animals </span><a href="https://paperpile.com/c/oJce8t/MBJK"><span style="font-weight: 400;">[7]</span></a><span style="font-weight: 400;">. </span></p>
                <p><strong>The continued decrease of Vitamin D intake, the leaky gut determined by the new diet, the proximity to animal parasites, and other (still) unknown factors, all contributed to the beginning of allergies and asthma in Homo Sapiens </strong><a href="https://paperpile.com/c/oJce8t/SKT2"><span style="font-weight: 400;">[8]</span></a> <a href="https://paperpile.com/c/oJce8t/DROA"><span style="font-weight: 400;">[9]</span></a><strong>. </strong></p>
                <p><span style="font-weight: 400;">We will never know for sure when the first allergic or asthmatic patient lived, but the first written information dates back to the invention of writing around 3600 BC. The earliest known report of an allergy was that of King Menses of Egypt, who died after a wasp sting sometime between 3640 and 3300 BC.</span></p>
                <p><strong><span style="font-weight: 400;">But, the first asthma patient may have lived thousands of years before, during that period of great changes in human behavior </span><a href="https://paperpile.com/c/oJce8t/KwYK"><span style="font-weight: 400;">[10]</span></a><span style="font-weight: 400;">. </span></strong></p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/introduction/3.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="3">
            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">The Perpetuation of Susceptibility </span></h2>

                <p><span style="font-weight: 400;">For an extended period, our body has been shaped by natural selection. According to Darwin, only the organisms that best adapt to their environment tend to survive and transmit their genetic characters in increasing numbers to succeeding generations. Read </span><a href="http://evolution.berkeley.edu/evolibrary/article/evo_25"><strong>here</strong></a><span style="font-weight: 400;"> for</span> <span style="font-weight: 400;">a simple explanation on</span><span style="font-weight: 400;"> how natural selection works.</span></p>
                <p><span style="font-weight: 400;">In normal circumstances, the problems determined by all the changes presented above should have died with the individuals in which they occurred.</span></p>
                <p><strong><span style="font-weight: 400;">However, our species was already moving away from natural selection. We cooperated and cared for one another like no other group on this planet. This allowed for the less fit individuals to survive and pass their genes forward. </span><strong>As a consequence, this sensitivity and predisposition to allergies was starting to perpetuate in our species' DNA.</strong></strong>
                </p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/introduction/4.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="4">
            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">Modern Times</span></h2>

                <p><span style="font-weight: 400;">Fast forward to the post-industrial era. We have almost forgotten where we came from and what our body was designed for.</span></p>
                <p><span style="font-weight: 400;">Most of us now live in polluted areas. Our diet includes sugar and processed food. Sugar is considered to be a major contributor to a damaged immune system and increases the prevalence of autoimmune disorders. The average American consumes 70 kg of it per year </span><a href="https://paperpile.com/c/oJce8t/8sGg"><span style="font-weight: 400;">[11]</span></a> <a href="https://paperpile.com/c/oJce8t/g5CY"><span style="font-weight: 400;">[12]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Antibiotics are everywhere. You can find them in the food chain and in the majority of medical treatments, along with steroids and other damaging chemicals. Studies have shown that antibiotic use reduces the positive bacteria in our gut and favors the growth of yeasts like Candida Albicans. </span><strong>Candida delays healing and exacerbates diseases </strong><a href="https://paperpile.com/c/oJce8t/ucjL+CGRD"><span style="font-weight: 400;">[13], [14]</span></a><strong>. </strong></p>
                <p><span style="font-weight: 400;">Our time in nature is limited. If we exercise, we mostly do it indoors. Outside, we wear clothes and sunscreen. "Stay away from the sun" the doctors keep saying.</span></p>
                <p><span style="font-weight: 400;">Stress has taken over due to finances, jobs, and relationships. In susceptible individuals, stress can favor the manifestation of allergic disease and exacerbate, as well as complicate, the control of the existing ones </span><a href="https://paperpile.com/c/oJce8t/Q8Ke"><span style="font-weight: 400;">[15]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Again, the brain-gut connection proves to be critical. There is so much pressure on the intestines, and there is so little Vitamin D to increase the biodiversity in the gut and protect the natural barrier in there. </span></p>
                <p><span style="font-weight: 400;">The gut gets leakier. Our immune system is confused and overreacts.</span></p>
                <p><span style="font-weight: 400;">Pregnant women all over the planet with low Vitamin D levels and distorted immune systems are giving birth to sensitive babies.</span></p>

                <p><span style="font-weight: 400;">What Happened Next?</span></p>

                <p><span style="font-weight: 400;">A pandemic that includes more than 80 autoimmune disorders and increases in both the incidence and prevalence of diseases such as Crohn's disease, rheumatoid arthritis, multiple sclerosis, and type I diabetes began</span><a href="https://paperpile.com/c/oJce8t/d3Gb"><span style="font-weight: 400;">[16]</span></a><span style="font-weight: 400;">. </span></p>
                <p><strong>In addition, an estimated number of 320 million people worldwide are struggling with not being able to breathe. Many die in asthma attacks. Allergies are making people&rsquo;s lives miserable. Frequent colds plunge us into depression, and the prevalence for all the above is increasing rapidly.</strong></p>
                <p><span style="font-weight: 400;">Researchers are still looking for the "real" causes of asthma: a wrong protein, molecule, or bacteria, maybe bizarre twist in the DNA. But, what caused these issues in the first place?</span></p>
                <p><span style="font-weight: 400;">To our great relief, Big Pharma is always "5 years away" from curing asthma for good by addressing these sources. Until then, they say we can only control it by taking more steroids and more chemicals.</span></p>
                <p><span style="font-weight: 400;">As mentioned before, the first step of our journey is knowing the related facts which led to our condition. Now that we have started to understand the underlying causes, we can move forward to step two, and act on them.</span></p>
                <p><span style="font-weight: 400;">Since (the lack of) Vitamin D is at the heart of the problem, we first have to turn our attention to it and optimize its levels.</span></p>
                <p><span style="font-weight: 400;">The leaky gut comes next. We need to plug it by eliminating the intake of damaging chemicals, regulating our body&rsquo;s PH, and eating the right food.</span></p>
                <p><span style="font-weight: 400;">Finally, we need to address the stress that adds further pressure on our guts and immune system.</span></p>
                <p><span style="font-weight: 400;">It might sound complicated. Many patients can feel overwhelmed at the start of any new program, and it is natural, but we can assure you it is simple, straightforward and fun. The benefits here outweigh any amount of effort. Where there&rsquo;s a will, there&rsquo;s a way.</span></p>
                <p><span style="font-weight: 400;">Here at RISE Asthma, we are people like you who shared the same suffering, and we'll help you along the way. Don&rsquo;t hesitate to reach out.</span></p>



            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/introduction/5.png">
            <!-- image    -->



            <div class="content content-full-width">


                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                <a data-target="#introduction-reference" href="#" data-toggle="modal">References (click to open)<b class="caret"></b></a>
                            </h1>
                        </div>
                        <div id="introduction-reference" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">References</h4>
                                    </div>
                                    <div class="modal-body">
                                        <ol>
                                            <li><a href="http://paperpile.com/b/oJce8t/x4Sa"><span style="font-weight: 400;">A. W. Norman, &ldquo;From vitamin D to hormone D: fundamentals of the vitamin D endocrine system essential for good health,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, p. 491S&ndash;499S, Aug. 2008.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/2D3G"><span style="font-weight: 400;">Y. N. Harari, </span><em><span style="font-weight: 400;">Sapiens: A brief history of humankind</span></em><span style="font-weight: 400;">. Random House, 2014.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/QtoB"><span style="font-weight: 400;">N. G. Jablonski and G. Chaplin, &ldquo;Human skin pigmentation, migration and disease susceptibility,&rdquo; </span><em><span style="font-weight: 400;">Philos. Trans. R. Soc. Lond. B Biol. Sci.</span></em><span style="font-weight: 400;">, vol. 367, no. 1590, pp. 785&ndash;792, Mar. 2012.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/2cGj"><span style="font-weight: 400;">P. A. Smith, &ldquo;The tantalizing links between gut microbes and the brain,&rdquo; </span><em><span style="font-weight: 400;">Nature</span></em><span style="font-weight: 400;">, vol. 526, no. 7573, pp. 312&ndash;314, Oct. 2015.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/26lC"><span style="font-weight: 400;">M. Bashir </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Effects of high doses of vitamin D3 on mucosa-associated gut microbiome vary between regions of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 55, no. 4, pp. 1479&ndash;1489, Jun. 2016.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/z0oW"><span style="font-weight: 400;">J. R. Rapin and N. Wiernsperger, &ldquo;Possible links between intestinal permeablity and food processing: a potential therapeutic niche for glutamine,&rdquo; </span><em><span style="font-weight: 400;">Clinics</span></em><span style="font-weight: 400;">, 2010.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/MBJK"><span style="font-weight: 400;">N. D. Wolfe, C. P. Dunavan, and J. Diamond, &ldquo;Origins of major human infectious diseases,&rdquo; </span><em><span style="font-weight: 400;">Nature</span></em><span style="font-weight: 400;">, vol. 447, no. 7142, pp. 279&ndash;283, May 2007.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/SKT2"><span style="font-weight: 400;">K. M. Lammers </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Gliadin induces an increase in intestinal permeability and zonulin release by binding to the chemokine receptor CXCR3,&rdquo; </span><em><span style="font-weight: 400;">Gastroenterology</span></em><span style="font-weight: 400;">, vol. 135, no. 1, pp. 194&ndash;204.e3, Jul. 2008.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/DROA"><span style="font-weight: 400;">J. Wang and A. H. Liu, &ldquo;Food allergies and asthma,&rdquo; </span><em><span style="font-weight: 400;">Curr. Opin. Allergy Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 11, no. 3, pp. 249&ndash;254, Jun. 2011.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/KwYK"><span style="font-weight: 400;">F. Estelle and R. Simons, &ldquo;Ancestors of allergy,&rdquo; </span><em><span style="font-weight: 400;">Aerobiologia </span></em><span style="font-weight: 400;">, vol. 2, no. 12, p. 154, 1996.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/8sGg"><span style="font-weight: 400;">N. E. Berentzen </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Associations of sugar-containing beverages with asthma prevalence in 11-year-old children: the PIAMA birth cohort,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 69, no. 3, pp. 303&ndash;308, Mar. 2015.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/g5CY"><span style="font-weight: 400;">R. Mansi, S. V. Joshi, S. R. Pandloskar, and H. L. Dhar, &ldquo;Correlation between blood sugar, cholesterol and asthma status,&rdquo; </span><em><span style="font-weight: 400;">Indian J Allergy Asthma Immunol</span></em><span style="font-weight: 400;">, vol. 21, no. 1, pp. 31&ndash;34, 2007.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/ucjL"><span style="font-weight: 400;">K. Sonoyama, A. Miki, R. Sugita, H. Goto, M. Nakata, and N. Yamaguchi, &ldquo;Gut colonization by Candida albicans aggravates inflammation in the gut and extra-gut tissues in mice,&rdquo; </span><em><span style="font-weight: 400;">Med. Mycol.</span></em><span style="font-weight: 400;">, vol. 49, no. 3, pp. 237&ndash;247, Apr. 2011.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/CGRD"><span style="font-weight: 400;">D. A. Hill </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Metagenomic analyses reveal antibiotic-induced temporal and spatial changes in intestinal microbiota with associated alterations in immune cell homeostasis,&rdquo; </span><em><span style="font-weight: 400;">Mucosal Immunol.</span></em><span style="font-weight: 400;">, vol. 3, no. 2, pp. 148&ndash;158, Mar. 2010.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/Q8Ke"><span style="font-weight: 400;">N. D. Dave, L. Xiang, K. E. Rehm, and G. D. Marshall Jr, &ldquo;Stress and allergic diseases,&rdquo; </span><em><span style="font-weight: 400;">Immunol. Allergy Clin. North Am.</span></em><span style="font-weight: 400;">, vol. 31, no. 1, pp. 55&ndash;68, Feb. 2011.</span></a></li>

                                            <li><a href="http://paperpile.com/b/oJce8t/d3Gb"><span style="font-weight: 400;">G. A. W. Rook and L. R. Brunet, &ldquo;Microbes, immunoregulation, and the gut,&rdquo; </span><em><span style="font-weight: 400;">Gut</span></em><span style="font-weight: 400;">, vol. 54, no. 3, pp. 317&ndash;320, Mar. 2005.</span></a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><span style="font-weight: 400;">[1]</span> <a href="http://paperpile.com/b/oJce8t/x4Sa"><span style="font-weight: 400;">A. W. Norman, &ldquo;From vitamin D to hormone D: fundamentals of the vitamin D endocrine system essential for good health,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 88, no. 2, p. 491S&ndash;499S, Aug. 2008.</span></a></p>
                                <p><span style="font-weight: 400;">[2]</span> <a href="http://paperpile.com/b/oJce8t/2D3G"><span style="font-weight: 400;">Y. N. Harari, </span><em><span style="font-weight: 400;">Sapiens: A brief history of humankind</span></em><span style="font-weight: 400;">. Random House, 2014.</span></a></p>
                                <p><span style="font-weight: 400;">[3]</span> <a href="http://paperpile.com/b/oJce8t/QtoB"><span style="font-weight: 400;">N. G. Jablonski and G. Chaplin, &ldquo;Human skin pigmentation, migration and disease susceptibility,&rdquo; </span><em><span style="font-weight: 400;">Philos. Trans. R. Soc. Lond. B Biol. Sci.</span></em><span style="font-weight: 400;">, vol. 367, no. 1590, pp. 785&ndash;792, Mar. 2012.</span></a></p>
                                <p><span style="font-weight: 400;">[4]</span> <a href="http://paperpile.com/b/oJce8t/2cGj"><span style="font-weight: 400;">P. A. Smith, &ldquo;The tantalizing links between gut microbes and the brain,&rdquo; </span><em><span style="font-weight: 400;">Nature</span></em><span style="font-weight: 400;">, vol. 526, no. 7573, pp. 312&ndash;314, Oct. 2015.</span></a></p>
                                <p><span style="font-weight: 400;">[5]</span> <a href="http://paperpile.com/b/oJce8t/26lC"><span style="font-weight: 400;">M. Bashir </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Effects of high doses of vitamin D3 on mucosa-associated gut microbiome vary between regions of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Nutr.</span></em><span style="font-weight: 400;">, vol. 55, no. 4, pp. 1479&ndash;1489, Jun. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[6]</span> <a href="http://paperpile.com/b/oJce8t/z0oW"><span style="font-weight: 400;">J. R. Rapin and N. Wiernsperger, &ldquo;Possible links between intestinal permeablity and food processing: a potential therapeutic niche for glutamine,&rdquo; </span><em><span style="font-weight: 400;">Clinics</span></em><span style="font-weight: 400;">, 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[7]</span> <a href="http://paperpile.com/b/oJce8t/MBJK"><span style="font-weight: 400;">N. D. Wolfe, C. P. Dunavan, and J. Diamond, &ldquo;Origins of major human infectious diseases,&rdquo; </span><em><span style="font-weight: 400;">Nature</span></em><span style="font-weight: 400;">, vol. 447, no. 7142, pp. 279&ndash;283, May 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[8]</span> <a href="http://paperpile.com/b/oJce8t/SKT2"><span style="font-weight: 400;">K. M. Lammers </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Gliadin induces an increase in intestinal permeability and zonulin release by binding to the chemokine receptor CXCR3,&rdquo; </span><em><span style="font-weight: 400;">Gastroenterology</span></em><span style="font-weight: 400;">, vol. 135, no. 1, pp. 194&ndash;204.e3, Jul. 2008.</span></a></p>
                                <p><span style="font-weight: 400;">[9]</span> <a href="http://paperpile.com/b/oJce8t/DROA"><span style="font-weight: 400;">J. Wang and A. H. Liu, &ldquo;Food allergies and asthma,&rdquo; </span><em><span style="font-weight: 400;">Curr. Opin. Allergy Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 11, no. 3, pp. 249&ndash;254, Jun. 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[10]</span> <a href="http://paperpile.com/b/oJce8t/KwYK"><span style="font-weight: 400;">F. Estelle and R. Simons, &ldquo;Ancestors of allergy,&rdquo; </span><em><span style="font-weight: 400;">Aerobiologia </span></em><span style="font-weight: 400;">, vol. 2, no. 12, p. 154, 1996.</span></a></p>
                                <p><span style="font-weight: 400;">[11]</span> <a href="http://paperpile.com/b/oJce8t/8sGg"><span style="font-weight: 400;">N. E. Berentzen </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Associations of sugar-containing beverages with asthma prevalence in 11-year-old children: the PIAMA birth cohort,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 69, no. 3, pp. 303&ndash;308, Mar. 2015.</span></a></p>
                                <p><span style="font-weight: 400;">[12]</span> <a href="http://paperpile.com/b/oJce8t/g5CY"><span style="font-weight: 400;">R. Mansi, S. V. Joshi, S. R. Pandloskar, and H. L. Dhar, &ldquo;Correlation between blood sugar, cholesterol and asthma status,&rdquo; </span><em><span style="font-weight: 400;">Indian J Allergy Asthma Immunol</span></em><span style="font-weight: 400;">, vol. 21, no. 1, pp. 31&ndash;34, 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[13]</span> <a href="http://paperpile.com/b/oJce8t/ucjL"><span style="font-weight: 400;">K. Sonoyama, A. Miki, R. Sugita, H. Goto, M. Nakata, and N. Yamaguchi, &ldquo;Gut colonization by Candida albicans aggravates inflammation in the gut and extra-gut tissues in mice,&rdquo; </span><em><span style="font-weight: 400;">Med. Mycol.</span></em><span style="font-weight: 400;">, vol. 49, no. 3, pp. 237&ndash;247, Apr. 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[14]</span> <a href="http://paperpile.com/b/oJce8t/CGRD"><span style="font-weight: 400;">D. A. Hill </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Metagenomic analyses reveal antibiotic-induced temporal and spatial changes in intestinal microbiota with associated alterations in immune cell homeostasis,&rdquo; </span><em><span style="font-weight: 400;">Mucosal Immunol.</span></em><span style="font-weight: 400;">, vol. 3, no. 2, pp. 148&ndash;158, Mar. 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[15]</span> <a href="http://paperpile.com/b/oJce8t/Q8Ke"><span style="font-weight: 400;">N. D. Dave, L. Xiang, K. E. Rehm, and G. D. Marshall Jr, &ldquo;Stress and allergic diseases,&rdquo; </span><em><span style="font-weight: 400;">Immunol. Allergy Clin. North Am.</span></em><span style="font-weight: 400;">, vol. 31, no. 1, pp. 55&ndash;68, Feb. 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[16]</span> <a href="http://paperpile.com/b/oJce8t/d3Gb"><span style="font-weight: 400;">G. A. W. Rook and L. R. Brunet, &ldquo;Microbes, immunoregulation, and the gut,&rdquo; </span><em><span style="font-weight: 400;">Gut</span></em><span style="font-weight: 400;">, vol. 54, no. 3, pp. 317&ndash;320, Mar. 2005.</span></a></p>
                                <p><br /><br /></p>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
            <!-- sectiune capitol     -->
        </div>

    </div>
    <!--  card     -->
</div>
