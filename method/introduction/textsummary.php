<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>

        <div class="nuttshell">

            <div class="nutshell-content">
                <h2>The Journey</h2>

                <p>Since the evolution of the Homo species 2.5 million years ago, we have relied on the sun for our body to function properly. Our skin produces a hormone called Vitamin D that it is being transferred through the bloodstream to every organ of our body becoming a cornerstone for our well-being.</p>
                <p>With the Cognitive Revolution, 70,000 years ago, we have started to migrate north in colder regions where the sun was less potent. We have also developed clothes that further reduced the Vitamin D intake.</p>
                <p>With the Agricultural Revolution, 12,000 years ago, we drastically changed our way of living. We stopped being hunters and gatherers and settled to domesticate plants and animals. Our diet changed from healthy fats and vegetables to carbohydrates and dairy.</p>
                <p>The Vitamin D level reduction and the new diet had a negative impact on our intestinal wall. This gut barrier inside us became leaky and allowed for undigested food particles to pass into the blood stream causing the immune system to overreact (a condition called "Increased Intestinal Permeability" or the "Leaky" gut).</p>
                <p>The "Leaky" gut, the proximity to animal parasites and other unknown factors determined the appearances of the first allergies and asthma.</p>
                <p>The post-industrial - modern lifestyle - &nbsp;era brought a couple of related aspects that had a further great impact on our health:</p>
                <ol class="general-list">

                    <li>Lower levels of Vitamin D</li>
                    <li>Sugar and processed food</li>
                    <li>Overuse of antibiotics and other damaging chemicals</li>
                    <li>Pollution</li>
                    <li>Stress</li>
                    <li>Sedentarism</li>
                </ol>

                <p>The result is that more than 80 autoimmune disorders increased in both incidence and prevalence. Also, 320 million people suffer today from asthma and allergies.</p>
                <p>Now, that we have started to understand the underlying causes, we can move forward to step two, and that is acting on them.</p>
                <p>Since (the lack of) Vitamin D is at the heart of the problem, we first have to turn our attention to it and optimize its levels.</p>
                <p>The leaky gut comes next. We need to plug it by eliminating the intake of damaging chemicals, regulating our body&rsquo;s PH, and eating the right food.</p>
                <p>Finally, we need to address the stress that adds further pressure on our guts and immune system.</p>
                <p>It might sound complicated. Many patients can feel overwhelmed at the start of any new program and it is natural. But we can assure you it is simple, straightforward and fun. And the benefits here outweigh any amount of effort. Where there&rsquo;s a will, there&rsquo;s a way they say.</p>
                <p><strong>Here at RISE Asthma, we are people like you who shared the same suffering. And we'll help you along the way. Don&rsquo;t hesitate to reach out to us.</strong></p>


            </div>


        </div>
    </div>
</div>
