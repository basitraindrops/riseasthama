<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>Everything Together</h1>


            </div>
            <div class="nuttshell">
                <div class="nutshell-content">
                    <h2> What really causes asthma?</h2>
                    <p>Asthma and allergies are diseases not strictly related to lungs or immune system. Instead, they are complex manifestations of various imbalances the body taken as a whole. These disparities include:</p><strong>
                    <p>The Vitamin D deficiency;<br />The fatty acid ratio;<br />The dysbiosis - or the microflora imbalance.<br /> <br /></strong> As a tool in balancing the internal systems, the human body performs homeostasis - the continuous process of maintaining a condition of equilibrium when dealing with internal and external changes. But this process alone is not enough to ensure optimal health when constant strain is applied.<br /> <br />Also, the daily stress represents a significant aggravating factor that adds more pressure on our system and disrupts the body&rsquo;s ability to balance itself. <br /> <br />All the factors above are causing a condition called the "Increased Intestinal Permeability" (or the &ldquo;Leaky&rdquo; gut) that, along with genetic and other (still) unknown factors trigger allergies and asthma.</p>
                </div>
                <div class="do">
                    <div class="dodont-content">
                        <h2>Take on a daily basis:</h2>
                        <ul class="general-list">
                            <li>Vitamin D supplements
                                <label data-tooltip-content="#tooltip_content-first" class="tooltip all-tooltip">
                                    <i class="pe-7s-help1"></i>
                                </label>
                            </li>
                            <li>High-quality Omega 3 fish oil capsules <label data-tooltip-content="#omega3" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Apple Cider Vinegar with bicarbonate soda and water<label data-tooltip-content="#acv" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                        </ul>
                        <p>Optional</p>
                        <ul class="general-list">
                            <li>Lemon juice with bicarbonate soda and water <label data-tooltip-content="#lemon" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Pre and probiotics <label data-tooltip-content="#probiotics" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>

                        </ul>

                    </div>
                </div>
                <div class="do3">
                    <div class="dodont-content">
                        <h2>Dietary changes</h2>
                        <ul class="general-list">
                            <li>Eat whole and fresh foods <label data-tooltip-content="#foods" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Eat lots of fiber <label data-tooltip-content="#fiber" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Adapt the fruit consumption to your lifestyle <label data-tooltip-content="#fruits" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Run a two-week carbohydrates intolerance test and experiment with gluten and dairy <label data-tooltip-content="#test" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Cut out refined sugar <label data-tooltip-content="#sugar" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Cut out processed food <label data-tooltip-content="#processed" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label>
                                <li>Cut out refined carbohydrates <label data-tooltip-content="#refined" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label>
                                </li>
                        </ul>
                    </div>
                </div>
                <div class="do4">
                    <div class="dodont-content">
                        <h2>Lifestyle changes</h2>
                        <ul class="general-list">
                            <li>Have frequent but moderated sun exposure <label data-tooltip-content="#sun" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Exercise daily by keeping the effort aerobic and purposeful <label data-tooltip-content="#exercise" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Connect more with people <label data-tooltip-content="#people" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Get high quality sleep <label data-tooltip-content="#sleep" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Limit the use of antibiotics <label data-tooltip-content="#antibiotics" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>
                            <li>Pursue small goals and have a greater purpose<label data-tooltip-content="#goals" class="tooltip all-tooltip"><i class="pe-7s-help1"></i></label></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="tooltip_templates" style="display:none;width:50%">
    <span id="tooltip_content-first">
        <h2 class="demo_left">Vitamin D Suppliments </h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/vitamind/1.png">
        Vitamin D is a cornestone of our body. Due to the modern lifestyle, most of us suffer from Vitamin D deficiency that is one of root cause of allergies and asthma. To reach out an optimal level of 50ng/ml in our blood we shoud take: <br/>
        <ul>
            <li>Children -1,000 IU of vitamin D a day for every 25Lbs of their weight, rounded up.</li>
            <li>Adults - 5,000 IU of vitamin D a day.People also go up to 10,000 IU of vitamin D a day which is unlikely to do any harm.</li>
            <li>Pregnant mothers -6,000 IU of vitamin D a day.</li>
        </ul>
    </span>
    <span id="omega3">
  <h2 class="demo_left">Omega 3 Suppliments </h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/omega3/1.png">
        The Omega 3 to Omega 6 fatty acids ratio that, for early humans, used to be 1:1 is nowadays somewhere between 1:20 to 1:40. This imbalance is causing multiple health disorders and negatively affects our asthma symptoms. To correct this imbalance, we have to increase the Omega 3 intake and decrease the ingestion of Omega 6.<br/> 
        <ul>
            <li>Take 3 to 4 g of a high quality Omega 3 fish oil per day if you don’t make any changes to the Omega 6 intake;</li>
            <li>Take 1 g of Omega 3 fish oil per day if you reduce your Omega 6 intake;</li>
        </ul>
    </span>
    <span id="acv">
       <h2 class="demo_left">Apple Cider Vinegar</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/homeostasis/1.png">
    Our body operates a continuous process of maintaining a condition of equilibrium when dealing with internal and external changes. Various causes can negatively affect this process, leading to general health problems including asthma and allergies. A simple and fast way to boost the homeostasis is to take Apple Cider Vinegar. In a glass mix:

        <ul>
            <li>2 tablespoons of a high quality Apple Cider Vinegar</li>
            <li>1/2 tablespoon of baking soda</li>
            <li>6 oz (200 ml) of water</li>
        </ul>
        Take this remedy twice a day on an empty stomach.
    </span>
    <span id="lemon">
       <h2 class="demo_left">Lemon juice</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/homeostasis/9.png">
    Lemons and limes have healthy amounts of vitamins (such as Vitamin C), fiber content, and macro-minerals like magnesium and potassium as well as necessary micro-minerals, all of which are beneficial to bodily and cellular processes.
In a glass, mix


        <ul>
            <li>1 squeezed lemon or lime</li>
            <li>½ tablespoon of baking soda</li>
            <li>6 oz (200 ml) of water</li>
        </ul>
        Take this remedy twice a day on an empty stomach.
    </span>
    <span id="probiotics">
       <h2 class="demo_left">Pre and probiotics </h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/homeostasis/5.png">
   While having to take antibiotics is never ideal, there are many cases where it is necessary. However, you should take pre and probiotics both during and after a course of antibiotics to minimize their damage and encourage regrowth and diversification of your gut flora.
They can also be taken even if outside an antibiotics course. 

    </span>
    <span id="foods">
       <h2 class="demo_left">Eat whole and fresh foods</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/9.png">
  Eat fresh organic food. Consider how long it took from the moment it was produced to when it ended up on your plate. Also, eat whole foods. They are unprocessed and are the closest to what we would get in the wild.

    </span>
    <span id="fiber">
       <h2 class="demo_left">Eat lots of fiber</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/11.png">
“Eat more fiber.” is the statement usually agreed upon the health authorities and the proponents of various (sometimes contracting) diets.
Fiber is beneficial predominantly because, as we have already discussed, it feeds the “good” bacteria in the intestine. Fiber plays a similar role for our gut fauna as the prebiotics do.

    </span>
    <span id="fruits">
       <h2 class="demo_left">Adapt the fruit consumption to your lifestyle</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/15.png">
Like most things when it comes to a nutritious diet, the consumption of fructose should be balanced, and one also has to take the context into consideration.
People who are healthy, lean and active can afford to eat some fructose. Instead of being turned into fat, it will go towards replenishing glycogen stores in the liver. There is also evidence that the consumption of fruits might reduce the prevalence of adult asthma.
However, for people who are inactive and eat a high-carb Western diet, consuming a lot of fructose can cause harm.
As for fruit juice, there is no fiber in it, and no chewing resistance. It contains nearly the same amount of sugar as  soda, so try to avoid it.

    </span>
    <span id="test">
       <h2 class="demo_left">Run a two-week carbohydrates intolerance test</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/12.png">
   This test is a great, simple instrument used to determine which foods we are sensitive to. The brilliance of it is with some slight adaption to fit the asthma context, we can quickly rule out in two weeks the usual food suspects that might wreak havoc on our bodies. Read the full extend of it in the <a href="/eliminategutpressure.php">Eliminate Gut Pressure</a> chapter.

    </span>
    <span id="sugar">
       <h2 class="demo_left">Cut out refined sugar </h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/homeostasis/7.png">
 Discussions surrounding excess sugar consumption is often targeting obesity or heart issues, to which it does contribute.  However, this is a matter of far more than weight gain alone. When we consume too much sugar, it leads to severe imbalances in our body, including microbial changes.
We also know that sugar consumption is associated with asthma and allergies to an alarming extent. In addition, sugar makes us dumber and can lead to memory loss.

    </span>
    <span id="processed">
       <h2 class="demo_left">Cut out processed food</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/17.png">
Glucose, salt, emulsifiers, organic solvents, microbial transglutaminase, nanoparticles and others, are extensively and increasingly used by the food industry to improve the “qualities” of food and make it last longer.
However, all of the additives mentioned above increase the intestinal permeability by breaching the integrity of the tight junction barrier. The occurrences of autoimmune diseases has continually been growing, along with the expansion of industrial food processing and food additive consumption.
And we do have compelling evidence connecting fast food junk with asthma and allergies.

    </span>

    <span id="refined">
       <h2 class="demo_left">Cut out refined carbohydrates</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/18.png">
Numerous studies show that refined carbohydrate consumption is associated with health problems like obesity and type 2 diabetes. They tend to cause significant spikes in blood sugar levels, which leads to a subsequent crash that can trigger hunger and cravings for more high-carb foods. 

    </span>
    <span id="sun">
       <h2 class="demo_left">Have frequent but moderated sun exposure</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/vitamind/2.png">
Optimizing the Vitamin D intake is the cornerstone of the RISE method. This powerful hormone is the link between various elements that cause and exacerbate allergies and asthma. Sun exposure is a crucial element. Be careful not to get sunburned. Start low and then gradually increase the duration of sun exposure according to your skin tolerance. A good indicator is half the time required for your skin to turn pink. Then, cover up with clothing and go into the shade. 

    </span>
    <span id="exercise">
       <h2 class="demo_left">Exercise daily by keeping the effort aerobic and purposeful</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/stress/10.png">
 Exercise impacts the brain the same as antidepressant medication. There are also multiple studies on the benefits of exercise in connection with asthma and allergies. It is important to keep the effort aerobic, especially those suffering from exercise induced asthma.

    </span>
    <span id="people">
       <h2 class="demo_left">Connect more with people</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/introduction/5.png">
   We are born to connect with one another. This urge is etched deeply into our genes. To connect more with people start by fueling your passions — they work wonders bringing people together — and join a club or community. Also, try to give back as often as possible by finding volunteering activities. In addition, avoid negativity and disclose and open yourself to your friends and family.

    </span>
    <span id="sleep">
       <h2 class="demo_left">Get high quality sleep</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/stress/14.png">
   Sleep is indispensable for our health. Aim for a good quality 8 hours a day sleep. 

    </span>
    <span id="antibiotics">
       <h2 class="demo_left">Limit the use of antibiotics</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/gut/16.png">
   While having to take antibiotics is never ideal, there are many cases where it is necessary. However, you should take pre and probiotics both during and after a course of antibiotics to minimize their damage and encourage regrowth and diversification of your gut flora.
They can also be taken even if outside an antibiotics course. 

    </span>
    <span id="goals">
       <h2 class="demo_left">Pursue small goals and have a greater purpose</h2>
        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/stress/15.png">
 Rumination is undoubtedly tempting for the brain. It promises to provide the goods, but it rarely delivers. The perks of an active mind are that it disrupts this strenuous process.The same way it is best to exercise our body in a meaningful way purpose, we should also train the mind by aiding ourselves with small goals. In addition, according to recent research, having purpose and meaning actually increases our health at a cellular level, providing us with a stronger immune response profile. Purpose and meaning are important for us both emotionally and physically.

    </span>

</div>

<!-- Modal display only once about tool tips-->
<div id="rise_tips" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="center"><b>RISE Tips</b></h4>
                        <p>Hover over the questions marks to get a brief summary of the section.</p>
                    </div>
                </div>
                <div class="row green-bg">
                    <div class="col-md-3">
                        <i class="pe-7s-help1 display_big_question"></i>
                    </div>
                    <div class="col-md-9 white-bg">
                        <h2 class="demo_left">Vitamin D Suppliments</h2>
                        <img class="demo_right" src="<?php echo SITE_URL; ?>assets/img/vitamind/1.png">
                        <p>
                            Vitamin D is a cornestone of our body. Due to the modern lifestyle, most of us suffer from Vitamin D deficiency that is one of root cause of allergies and asthma. To reach out an optimal level of 50ng/ml in our blood we shoud take: </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="center">
                            <button type="button" class="got-it-btn" data-dismiss="modal">Got it</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php

?>

<?php
if(!isset($_COOKIE["rise_tips"])) {
    $cookie_name = "rise_tips";
    $cookie_value = "1";
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            //$("#openmodel").trigger("click");
            $('#rise_tips').modal('show');
        });

    </script>
    <?php        
} 
?>

    <!-- <script type="text/javascript">    
    $( document ).ready(function() {   
        //$("#openmodel").trigger("click");
        $('#rise_tips').modal('show');
    });
</script> -->

    <style type="text/css">
        .modal-backdropfff {
            z-index: 0 !important;
        }

        .display_big_question {
            font-size: 120px;
            margin-top: 190px;
            margin-bottom: 190px;
        }

        .green-bg {
            background-color: #eaf4e6;
            padding: 20px;
        }

        .white-bg {
            background-color: #ffffff;
        }

        .button-footer,
        .center {
            text-align: center;
        }

        .got-it-btn {

            padding: 10px;
            background-color: #ea7137;
            width: 120px;
            border-radius: 21px;
        }

    </style>
