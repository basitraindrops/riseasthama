<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>Everything Together</h1>


            </div>
            <div class="nuttshell">
                <div class="do">
                    <div class="dodont-content">
                        <h2>Take on a daily basis:</h2>
                        <ul class="general-list">
                            <li>Vitamin D supplements <i class="pe-7s-help1"></i></li>
                            <li>High-quality Omega 3 fish oil capsules <i class="pe-7s-help1"></i></li>
                            <li>Apple Cider Vinegar mixed with bicarbonate soda and water <i class="pe-7s-help1"></i></li>
                        </ul>
                        <p>Optional</p>
                        <ul class="general-list">
                            <li>Lemon juice with bicarbonate soda and water <i class="pe-7s-help1"></i></li>
                            <li>Pre and probiotics <i class="pe-7s-help1"></i></li>
                        </ul>

                    </div>
                </div>
                <div class="do3">
                    <div class="dodont-content">
                        <h2>Dietary changes</h2>
                        <ul class="general-list">
                            <li>Eat whole and fresh foods <i class="pe-7s-help1"></i></li>
                            <li>Eat lots of fiber <i class="pe-7s-help1"></i></li>
                            <li>Adapt the fruit consumption to your lifestyle <i class="pe-7s-help1"></i></li>
                            <li>Run a two-week carbohydrates intolerance test and experiment with gluten and dairy <i class="pe-7s-help1"></i></li>
                            <li>Cut out refined sugar <i class="pe-7s-help1"></i></li>
                            <li>Cut out processed food <i class="pe-7s-help1"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="do4">
                    <div class="dodont-content">
                        <h2>Lifestyle changes</h2>
                        <ul class="general-list">
                            <li>Have frequent but moderated sun exposure <i class="pe-7s-help1"></i></li>
                            <li>Exercise daily by keeping the effort aerobically and purposeful <i class="pe-7s-help1"></i></li>
                            <li>Connect more with people <i class="pe-7s-help1"></i></li>
                            <li>Get high quality sleep <i class="pe-7s-help1"></i></li>
                            <li>Limit the use of antibiotics<i class="pe-7s-help1"></i></li>
                            <li>Pursue small goals and have a greater purpose <i class="pe-7s-help1"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
