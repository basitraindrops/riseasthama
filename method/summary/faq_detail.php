 <?php 
                $f_id = 8;
                require_once(CLASS_DIR."faq_topic.class.php");
                $pb=new faq_topic($mysqli);
                $pb->faq_detail_topic($f_id);   
 ?> 
            <div class="tab-pane" id="faq_detail">
            <div class="faq_search">
            <br>
               <div class="icon_search"><i class="fa fa-search" aria-hidden="true"></i></div>
                 <input type="text" name="search" id="txt_search" placeholder="Search.......">
                  <input type="hidden" name="id" id="f_id" value="8">
            </div>
            <div class="faq_breadcum">
                <a href="faq.php" class="all_faq">All Topics</a> > <a href="" class="child_faq"><?php echo $pb->faq_cat_name; ?></a>
            </div>
             <div style="float: left; padding: 30px 15px 5px !important; width: 111%; background:#fff;margin-left:-44px;" id="default">
              
              <?php 
                require(CLASS_DIR."faq.class.php");
                $pb=new faq($mysqli);
                $f_id=8;
                $data=$pb->faq_detail($f_id);
               //echo "<pre>";print_r($data);exit;
              for($i=0; $i<count($data); $i++) { ?>
                <div class="block_faq block_new">
                  <div class="img_block detail_faq">
               
                  <a href="faq_main_detail.php?id=<?php echo $data[$i]->faq_cat_id; ?>" > 
                       <h4><?php echo $data[$i]->faq_cat_name; ?></h4>
                  </a>
                  <p style="font-size:12px;">
                       <?php
                    $string =   htmlspecialchars_decode($data[$i]->faq_cat_des);
                    if (strlen($string) > 170) {                   
                        $stringCut = substr($string, 0, 170);
                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
                    } 
                    ?>
                    <?php echo $string ?>
                  </p>
                </div>
                 
                </div>
                <?php }?>
           </div>
           </div>     
<script>
$('#txt_search').change(function(){
   var txt = document.getElementById('txt_search').value;
   var id = document.getElementById('f_id').value;
    $.ajax({
        type: "POST",
        dataType:"json",
        url: "search_faq_detail.php",
       data: { txt_val:txt,f_id:id
       },
       success: function(result) {
        var htmlText = '';
        htmlText +=   '<div class="wrapper">'
       
        htmlText += '<div style="width:100%;">';
        if (result == "" || result == 'undefined' || result == undefined) 
        {
              htmlText += '<p>No result found!</p>';
        }
        else
        {
         for ( var key in result ) {
                
                 htmlText += '<div class="block_faq block_new"><div class="img_block detail_faq">';
                htmlText += ' <a href="faq_main_detail.php?id='+result[key].id+'"> <h4> ' + result[key].name + '</h4></a>';
                htmlText += '<p style="font-size:12px;"> ' + result[key].des + '</p></div>  ';
              
                htmlText += '</div>';
                
            }
        }
                htmlText += '</div>';
                htmlText += '</div>';
                htmlText += '</div>';
                $('#default').html('');
                $('#default').append(htmlText);
       }

    });
});

</script>           
        
