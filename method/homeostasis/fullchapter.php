<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>Increase Homeostasis</h1>
                <p>Homeostasis is the optimal functioning of our body's systems. In this chapter, we are going to explore why we need to improve our general health in order to beat asthma and allergies and how Apple Cider Vinegar helps reduce symptoms.</p>
            </div>
            <!--
            <div class="video-container">
                <iframe id="vzvd-10494887" name="vzvd-10494887" title="vzaar video player" type="text/html" frameborder="0" allowFullScreen allowTransparency="true" mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/10494887/player"></iframe>
            </div>
            -->

            <div class="video-player video-container">             
                <object id="vzvd-10494887"  height="auto" style="width: 100%;">
                    <param name="allowScriptAccess" value="always" /> 
                    <param name="allowFullScreen" value="true" /> 
                    <param name="wmode" value="transparent" />
                    <param name="flashvars" value="showplaybutton=true" />
                    <video  height="auto" style="width: 100%;" poster="//view.vzaar.com/10494887/image" preload="none" controls="controls" onclick="this.play();" id="vzvid" >                        
                            <source src="//view.vzaar.com/10494887/video">
                    </video>
                </object>
            </div>

            <div class="nuttshell">
                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p><strong><span style="font-weight: 400;">Our body operates a continuous process of maintaining a condition of equilibrium when dealing with internal and external changes. Various causes can negatively affect this process, leading to general health problems including asthma and allergies. A simple and fast way to boost the homeostasis is to:
</span></strong></p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Take Apple Cider Vinegar with bicarbonate soda and water</h3>
                        <p>In a glass mix:</p>

                        <ul class="general-list">
                            <li>2 tablespoons of a high quality Apple Cider Vinegar</li>
                            <li>1/2 tablespoon of baking soda</li>
                            <li>6 oz (200 ml) of water</li>

                        </ul>

                        <p>This remedy should be taken twice a day on an empty stomach, preferably 15-30 minutes before eating to allow for the digestive juices to better breakdown your food.</p>
                    </div>
                </div>
                <div class="do2">
                    <div class="dodont-content">

                        <h3>Take lemon juice with bicarbonate soda and water (optional)</h3>
                        <p>In a glass mix:</p>

                        <ul class="general-list">
                            <li>1 squeezed lemon or lime</li>
                            <li>1/2 tablespoon of baking soda</li>
                            <li>6 oz (200 ml) of water</li>

                        </ul>

                        <p>Take the solution once or twice a day on an empty stomach.</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="scroll" data-type="2">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">A Small Win</span></h2>
                <p><span style="font-weight: 400;">A year has passed since I decided asthma was not going to ruin my life. A year was spent trying diverse treatments, from both the traditional eastern medicine and the western allopathic, but all seemed to be in vain. I was building up tension and craved a breakthrough.</span></p>
                <p><span style="font-weight: 400;">One day, someone told me about the &nbsp;</span><strong>Apple Cider Vinegar</strong><span style="font-weight: 400;"> and how</span> <span style="font-weight: 400;">it &ldquo;</span><em><span style="font-weight: 400;">helps for all kinds of disorders</span></em><span style="font-weight: 400;">&rdquo;. </span></p>
                <p><span style="font-weight: 400;">&ldquo;</span><em><span style="font-weight: 400;">Is too simple</span></em><span style="font-weight: 400;">&rdquo;, I thought. It&rsquo;s vinegar. How would a salad dressing help me? I had zero expectations, but I tried it nonetheless. </span></p>
                <p><span style="font-weight: 400;">To my great surprise, initial relief came two weeks after I began. I had the first day in years without sneezing or itchy eyes. I could breathe. I could see and &nbsp;enjoy all the beautiful colors around me. </span></p>
                <p><span style="font-weight: 400;">This &ldquo;small win&rdquo; boosted my determination. I became more creative, more focused, and my appetite for knowledge whetted. </span></p>
                <p><span style="font-weight: 400;">With my eyes open, I began to see how everything is alluringly connected. </span></p>
                <p><span style="font-weight: 400;">Finally, I realized if I wanted an asthma free life, I needed something essential, something that I always lacked: balance. </span></p>


            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/1.png">
            <!-- image    -->


            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Balance</span></h2>
                <p><span style="font-weight: 400;">Balance is everywhere and in everything. Poets say that to light a candle is to cast a shadow. Philosophers state no joy can exist without an equal weight of pain to balance it out on some unknown scale.</span></p>
                <p><span style="font-weight: 400;">From a macro scientific perspective, the whole universe holds an equilibrium between the natural laws that created the galaxies, solar systems, and planets.</span></p>
                <p><span style="font-weight: 400;">Our planet performs multiple balancing twirls that connect the lithosphere (land), hydrosphere (water), biosphere (living things), and atmosphere (air).</span></p>
                <p><span style="font-weight: 400;">Among these equilibriums, one is represented by the pH - potential Hydrogen - or the constant struggle between acidity and alkalinity. </span></p>
                <p><span style="font-weight: 400;">The oceans have a pH that makes marine life possible. For instance, as the oceans&rsquo; pHs have dropped by just fractions from around 8.2 to 8.1 (due to increased CO2 deposits), coral reef has started to die off at an alarming rate</span><strong>. </strong></p>
                <p><span style="font-weight: 400;">Even the soil where plants grow has an ideal pH level to ensure the ideal mix of minerals and nutrients. </span></p>
                <p><span style="font-weight: 400;">Humans are no different. We must maintain a perfect blood pH level of 7.4 for our primary systems to function properly. </span><a href="https://paperpile.com/c/uDplkC/Uul1"><span style="font-weight: 400;">[1]</span></a></p>
                <p><strong>The effort that our organisms perform to maintain this inner pH balance it&rsquo;s part of our body&rsquo;s homeostasis. </strong></p>

            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/2.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="3">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Homeostasis</span></h2>
                <p><span style="font-weight: 400;">To put it simply, the homeostasis is your body working hard to keep you alive, healthy, and happy. It is a continuous, dynamic equilibrium process.</span></p>
                <p><span style="font-weight: 400;">In more elaborate terms, homeostasis is our organism&rsquo;s ability to keep a constant working environment. Due to internal and external pressuring factors, adjustments must continually be made to keep us at or near the normal level. </span></p>
                <p><span style="font-weight: 400;">Each body system - circulatory, digestive, nervous, respiratory - contributes to the homeostasis of other systems and the entire organism. The well-being of a person depends upon the welfare of all the interacting body systems. A disruption within one triggers a consequence in the rest. </span><a href="https://paperpile.com/c/uDplkC/61pi"><span style="font-weight: 400;">[2]</span></a></p>
                <p><span style="font-weight: 400;">Hence, the same process determines your allergies and asthma symptoms. The lungs and immune system are unbalanced due to many other dis equilibriums in the body such as the Vitamin D deficiency, Omega 3-6 Ratio, the &ldquo;leaky&rdquo; gut, stress, and others. </span><a href="https://paperpile.com/c/uDplkC/zj6Y+aK4u+DHPS"><span style="font-weight: 400;">[3]&ndash;[5]</span></a> <a href="https://paperpile.com/c/uDplkC/eYul"><span style="font-weight: 400;">[6]</span></a></p>
                <p><span style="font-weight: 400;">However, a particular kind of homeostasis requires increased awareness regulating the body's pH. The pH homeostasis is heavily susceptible to lifestyle changes and has a determined impact on our allergies and asthma.</span></p>
                <p><span style="font-weight: 400;">Bottom Line</span></p>

                <p><em><span style="font-weight: 400;">Homeostasis is our body’s ability to seek and maintain a condition of equilibrium while dealing with internal and external changes. As a whole, the elements and the steps of the RISE method are designed to improve your body’s general homeostasis and its ability to regulate itself. </span></em>
                </p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/3.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Understanding Acidity</span></h2>
                <p><span style="font-weight: 400;">Similar to fats, acids are paramount for our general well being. In addition, they too have been unwittingly vilified. </span></p>
                <p><span style="font-weight: 400;">Imagine you take a carrot (or any other root) out of the ground and eat it raw and unwashed, the way humans have done for millennials. </span></p>
                <p><span style="font-weight: 400;">The carrot ends up in your stomach which starts producing acid that kills the opportunistic bacteria and digests the food for proper nutrition. Without sufficient stomach acid, the pathogens and bacteria&nbsp;will take a hold on your body </span><a href="https://paperpile.com/c/uDplkC/e8fc"><span style="font-weight: 400;">[7]</span></a><span style="font-weight: 400;">. Also, it would be nearly impossible to digest protein and absorb nutrients adequately. </span></p>
                <p><span style="font-weight: 400;">For similar reasons, all of our external organs have an acidic pH.</span></p>
                <p><span style="font-weight: 400;">The skin has an acid mantle that limits the growth of pathogens and prevents it from cracking and become vulnerable to infection </span><a href="https://paperpile.com/c/uDplkC/0Sjp"><span style="font-weight: 400;">[8]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">The sinuses are acidic </span><a href="https://paperpile.com/c/uDplkC/haJZ"><span style="font-weight: 400;">[9]</span></a><span style="font-weight: 400;">. The eyes are acidic </span><a href="https://paperpile.com/c/uDplkC/BY2D"><span style="font-weight: 400;">[10]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">And, the small intestines are acidic. Many of the pathogenic bacteria and fungi we encounter tend to thrive at a pH that is typically more alkaline than the standard pH of the intestines. To protects us, our gut contains a wide variety of acid-forming bacteria that help to create an unfavorable environment for these pathogenic organisms to thrive </span><a href="https://paperpile.com/c/uDplkC/Lfly"><span style="font-weight: 400;">[11]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">The only problem with acid is the excess of it. Just as water, fats or even oxygen, too much could lead to health problems or even death, unless the body stops performing what it knows best: balance through homeostasis.</span></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">Acidity is crucial for our body, offering a protective barrier from pathogens and helping perform food digestion. </span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/4.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="4">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Digestion and Homeostasis</span></h2>

                <p><span style="font-weight: 400;">You have probably heard of the alkaline/acidity debate and the two sides that are fiercely contradicting each other. On the one hand, there are those that praise the miracles of alkalinity. On the opposite, we have the scientific community that disregards it as a myth while bestowing empirical evidence. </span><a href="https://paperpile.com/c/uDplkC/hk57"><span style="font-weight: 400;">[12]</span></a> <a href="https://paperpile.com/c/uDplkC/biO5"><span style="font-weight: 400;">[13]</span></a> <a href="https://paperpile.com/c/uDplkC/t8BN"><span style="font-weight: 400;">[14]</span></a></p>
                <p><span style="font-weight: 400;">But, if we are to shovel deeper than just scratching the surface of this discourse, we&rsquo;ll encounter a close connection and even consensus between the two schools of thought. They both speak the same language of health but with a different accent.</span></p>
                <p><span style="font-weight: 400;">To understand what happens, we need to put our lab coat on again and take on a different kind of journey - the voyage of food into our bodies </span><a href="https://paperpile.com/c/uDplkC/RbfK"><span style="font-weight: 400;">[15]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">First, the food we intake reaches the stomach which produces the acid meant to digest food and transform it into nutrients. The results from this first part of digestion is a substance called chyme (nutrients and acid) and is very acidic (hundreds of times more acidic than water).</span></p>
                <p><span style="font-weight: 400;">Next, the stomach empties and passes the chyme into the small intestine. As a consequence, the body starts to trigger the pH homeostasis process. </span></p>
                <p><span style="font-weight: 400;">To prevent any damages the acidic chyme might cause to the intestines, our pancreas produces juice high in bicarbonate sodium. </span></p>
                <p><span style="font-weight: 400;">This &ldquo;natural baking soda&rdquo; is highly alkaline and balances the pH of the chyme until it reaches a level that is tolerated by the intestine. It is then ready to pass into the bloodstream for the nutrients to reach our vital organs. </span></p>
                <p><span style="font-weight: 400;">But now, the body starts to work its pure magic. </span></p>
                <p><span style="font-weight: 400;">To further compensate the acidity from disrupting the natural PH level of 7.4, the blood itself (along with all our body fluids) acts as the first line of defense. The chemicals in it readily combine to create a perfectly balanced pH environment. </span></p>
                <p><span style="font-weight: 400;">Nonetheless, blood is not alone in this struggle. Our respiratory system breaths out CO2, a process that further deals with much of the acid excess. Then, our kidneys take care of the rest. </span></p>
                <p><span style="font-weight: 400;">The result is that our PH balance is maintained, and we happily stay alive. On the other hand, the same systems and principles apply if there is too much alkalinity in our system. </span></p>
                <p><span style="font-weight: 400;">By reflecting on the above, I suggest we take a moment and thank our bodies for always being on duty.</span></p>
                <p><span style="font-weight: 400;">However, if our bodies were to reply, in most cases they would complain. They would tell us about how tired they are for operating this never ending homeostasis, and how much they would require our assistance </span><a href="https://paperpile.com/c/uDplkC/Xiex"><span style="font-weight: 400;">[16]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">So, how can we help?</span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">A consensus can exist between the two schools of thought debating alkaline vs. acidity. Our body’s homeostasis always autoregulates to reach the optimal 7.4 blood pH environment, but the constant pressure on our vital systems might negatively impact our body’s ability to self-adjust, and here is where some of the alkaline theory methods might play a defined role.</span></em></p>

            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/5.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="5">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Protect the Acid, Improve the Homeostasis</span></h2>

                <p><span style="font-weight: 400;">Imagine yourself in the African Savannah, and you spot a threatening lion. </span></p>
                <p><span style="font-weight: 400;">The body's survival instincts (the sympathetic system or fight or flight response) takeover </span><a href="https://paperpile.com/c/uDplkC/SKWw"><span style="font-weight: 400;">[17]</span></a><span style="font-weight: 400;">. The priorities become dull.</span></p>
                <p><span style="font-weight: 400;">Your brain needs to process the threat, analyze the context, and tell your muscles whether to run away or fight the predator. This process is a crucial part of what helped us survive and thrive as a species.</span></p>
                <p><span style="font-weight: 400;">But what about digestion? In this stressful situation, metabolism becomes the least of our body&rsquo;s priorities. The blood is being diverted from the stomach and gut to feed the cerebrum and the locomotion system. </span></p>
                <p><span style="font-weight: 400;">In consequence, the digestive system&rsquo;s ability to produce acid and other digestive juices is disrupted </span><a href="https://paperpile.com/c/uDplkC/lK7x"><span style="font-weight: 400;">[18]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Nowadays, the modern lifestyle has replaced predators as the primary source of stress and anxiety.On the one hand, we have finances, jobs, and relationships which are sufficient factors for us to stress and worry, but to make things worse, we ingest processed junk food and over-medicate ourselves with antibiotics, all of which constitute a different kind of strain.</span></p>
                <p><span style="font-weight: 400;">This never ending tension constantly keeps us in the &ldquo;flight or fight&rdquo; mode, where the sympathetic dominant state restricts the activity in the digestive tract and causes reduced acid secretion </span><a href="https://paperpile.com/c/uDplkC/BY1k+l5Kd"><span style="font-weight: 400;">[19], [20]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">To put it simply, the result is low stomach acid. The reduced acid level creates a vicious cycle of poor digestion, leaky gut, microbial overgrowth, elevated stress hormones and lowered nutrient absorption </span><a href="https://paperpile.com/c/uDplkC/47AP"><span style="font-weight: 400;">[21]</span></a> <a href="https://paperpile.com/c/uDplkC/J6dd+Mw6m+Lux6+85z2+iA51"><span style="font-weight: 400;">[22]&ndash;[26]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">The above are all underlying causes of asthma and allergies.</span></p>
                <p><span style="font-weight: 400;">And yes, your body has to work hard to compensate the imbalance through homeostasis. </span></p>
                <p><span style="font-weight: 400;">However, the same modern lifestyle, with all its strain and lack of Vitamin D, exercise, proper food, and general lack of connection with other human beings, all affect our body's ability to steady itself.</span></p>
                <p><span style="font-weight: 400;">You have already received two valuable pieces of information from RISE on how to help your body perform homeostasis: get enough Vitamin D - &nbsp;sun exposure - and balance your Omega 3-6 ratio.</span></p>
                <p><span style="font-weight: 400;">Furthermore, you have to eliminate stress, both mental and metabolic. Chapter 3 - Eliminate Gut pressure and Chapter 4 &nbsp;- Manage Stress will address these two problems in particular.</span></p>
                <p><span style="font-weight: 400;">Still, there&rsquo;s something else we can do. Nature is full of surprises, and, as it is always the case, if we listen to its tune and follow its guidelines, we will only garner the benefits.</span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Various elements can influence our body’s ability to produce a proper level of stomach acid and maintain a healthy pH homeostasis: Vitamin D, stress, a good fatty acid ratio, and a general healthy gut. </span></em></p>

            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/6.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="6">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Take Apple Cider Vinegar with Bicarbonate Soda and Water</span></h2>

                <p><span style="font-weight: 400;">Nature has created Apple Cider Vinegar, and it has truly over delivered. </span></p>
                <p><span style="font-weight: 400;">The ACV contains over 90 different nutrients and vitamins, including a number of beneficial compounds such as acetic and malic acid, potassium, Vitamin A, Vitamin B6, Vitamin C, and others. </span></p>
                <p><span style="font-weight: 400;">With this impressive combo of nutrients, the ACV can positively impact our vital systems and ensure they operate full throttle. The ACV directly assists the general homeostasis that finally leads to reduced asthma and allergy symptoms, but how exactly? </span></p>
                <p><span style="font-weight: 400;">First, vinegar can help kill pathogens, including bacteria and stimulate the healthy flora of our gut </span><a href="https://paperpile.com/c/uDplkC/CewV+0Hvr+OOoL+ioVx+5Kpz"><span style="font-weight: 400;">[27]&ndash;[31]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Numerous other studies show vinegar can increase insulin sensitivity and significantly lower blood sugar responses during meals </span><a href="https://paperpile.com/c/uDplkC/Z7yA+mDnP+xQzW+HhrX"><span style="font-weight: 400;">[32]&ndash;[35]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Third, the potassium in the apple cider vinegar helps the cell growth cycle and organ regeneration </span><a href="https://paperpile.com/c/uDplkC/A3b9"><span style="font-weight: 400;">[36]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Fourth, ACV has an impressive antioxidant perspective. A 100g apple contains three times the antioxidants of an orange and eight times that of a banana. Researchers found the compounds in apple cider vinegar convert superoxide and other harmful free radicals into less harmful oxygen and hydrogen peroxide </span><a href="https://paperpile.com/c/uDplkC/k3uk"><span style="font-weight: 400;">[37]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">Fifth, do you remember why fats are essential, but too much of them could cause health issues? According to a Japanese study, ACV helps deal with the excess fat storage in our tissues and sustains body recovery. In addition, there are two studies unveiling the potential impact of ACV on allergies and asthma:</span></p>
                <p><span style="font-weight: 400;">In a study published in the American Journal Of Respiratory Critical Care Medicine, researchers reported that adults who ate at least two apples a week reduced their asthma risk by up to a third </span><a href="https://paperpile.com/c/uDplkC/C0iz"><span style="font-weight: 400;">[38]</span></a><span style="font-weight: 400;">. In a separate study, children of mothers who ate apples during pregnancy were found to be significantly less asthma-prone in their first five years </span><a href="https://paperpile.com/c/uDplkC/qEfQ"><span style="font-weight: 400;">[39]</span></a><span style="font-weight: 400;">.</span></p>
                <p><strong><span style="font-weight: 400;">Another, no less important piece of evidence is the testimony of </span><span style="font-weight: 400;">tens of thousands of people</span><span style="font-weight: 400;"> who are successfully using ACV to treat various health disorders, allergies and asthma included. </span></strong></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/homeostasis/7.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="7">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;"> How to take ACV</span></h2>
                <p><span style="font-weight: 400;">Like with the rest of the RISE’s supplements and routine changes, a guideline is required to ensure that you make the most out of the ACV healthy nutrients, but also that the potential side effects are reduced or eliminated. </span></p>
                <p><span style="font-weight: 400;">In a glass, mix:</span></p>
                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">2 tablespoons of a high-quality Apple Cider Vinegar</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">&frac12; tablespoon of baking soda</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">6 oz (200 ml) of water</span></li>
                </ul>
                <p><span style="font-weight: 400;">This remedy should be taken twice a day on an empty stomach, preferably 15-30 minutes before eating. It helps the digestive enzymes to better breakdown the food.</span></p>
                <p><span style="font-weight: 400;">Some people take raw and undiluted organic ACV, but this could have certain effects that are not beneficial to the body. ACV is relatively acidic and tends to burn the esophagus and creates holes or cavities in teeth when regularly taken raw.</span></p>
                <p><span style="font-weight: 400;">A better option is to follow the RISE guidelines and take the ACV with baking soda and water which represents its post-digested form. Therefore, the ACV nutrients will be absorbed while the body is conserving its bicarbonates and energy.</span></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">With its diverse chemical composition, Apple Cider Vinegar is a nutritional wonder of nature that can drastically improve our body’s homeostasis process and have a positive influence over asthma and allergies.  </span></em></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/homeostasis/8.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Take lemon juice with bicarbonate soda and water</span></h2>
                <p><span style="font-weight: 400;">As a complementary option, drink the mix of lemons and limes bicarbonate soda and water. </span></p>
                <p><span style="font-weight: 400;">Lemons and limes have healthy amounts of vitamins (such as Vitamin C), fiber content, and macro-minerals like magnesium and potassium as well as necessary micro-minerals, all of which are beneficial to bodily and cellular processes </span><a href="https://paperpile.com/c/uDplkC/O9Gi"><span style="font-weight: 400;">[40]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">It can enhance our e immune function, protect against various diseases and increase the absorption of iron </span><a href="https://paperpile.com/c/uDplkC/0qXb+6MCg+fTQY+cOI2"><span style="font-weight: 400;">[41]&ndash;[44]</span></a><span style="font-weight: 400;">. Also, research concludes that the vitamin C in the lemon juice helps reduce the exercise-induced asthma symptoms </span><a href="https://paperpile.com/c/uDplkC/LAV2"><span style="font-weight: 400;">[45]</span></a><span style="font-weight: 400;">. In addition, lemons have been associated with other numerous health benefits like cardiovascular health, prevention of kidney stones, prevention of anemia, and even cancer </span><a href="https://paperpile.com/c/uDplkC/uPk5+6MCg+WQIO+6SfJ+fTQY+R0Wr"><span style="font-weight: 400;">[42], [43], [46]&ndash;[49]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">In a glass, mix:</span></p>
                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">1 squeezed lemon or lime</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">&frac12; tablespoon of baking soda</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">6 oz (200 ml) of water</span></li>
                </ul>
                <p><strong><span style="font-weight: 400;">Take this solution once or twice a day on an empty stomach.</span></strong></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/homeostasis/9.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">For Children</span></h2>
                <p><span style="font-weight: 400;">In general, it is safe to give your children ACV or lemon juice with water and bicarbonate soda. I haven&rsquo;t come across any study that would caution the use of these treatments. However, it is important to use raw high-quality ACV and appropriately dilute it with water. </span></p>
                <p><span style="font-weight: 400;">As with the other steps in the RISE method, always consult your pediatrician.</span></p>



                <h2><span style="font-weight: 400;">Conclusion</span></h2>

                <p><span style="font-weight: 400;">Earth&rsquo;s habitats and environments are incredibly interconnected. They even say a flap of a butterfly's wings in Brazil could set off a tornado in Texas.</span></p>
                <p><span style="font-weight: 400;">The same connectivity defines our body&rsquo;s vital systems. The control of temperature, blood pressure, pH, and glucose concentration are all examples of the unceasing homeostasis taking place inside us.</span></p>
                <p><span style="font-weight: 400;">Asthma and allergies are not offspring of a malfunctioning immune system (or lungs) exclusively. They are deeply related to our gut flora, Vitamin D levels, fatty acids ratio, stress levels, and the optimal body functioning as a whole.</span></p>
                <p><span style="font-weight: 400;">We owe a great deal of gratitude to the hundreds of years of research and scientific breakthroughs &nbsp;Western medicine has brought to our species. However, without sounding too critical, I also cannot fail to notice a persisting negative trend in the medical approach to chronic diseases.</span></p>
                <p><span style="font-weight: 400;">I&rsquo;ve seen highly specialized doctors everywhere with a narrow funnel vision that fails to spot this amazing interdependence of the human body. I&rsquo;ve seen an over medication of patients, treating symptoms only while the functional underlying cause is ignored. I sadly look at the general health statistics where the number of chronic diseases is increasing everywhere and affecting millions of lives.</span></p>

            </div>
            <!-- sectiune capitol     -->
        </div>
        <div class="scroll" data-type="8">


            <div class="content content-full-width">


                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                <a data-target="#homeostasis-reference" href="#" data-toggle="modal">References (click to open)<b class="caret"></b></a>
                            </h1>
                        </div>
                        <div id="homeostasis-reference" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">References</h4>
                                    </div>
                                    <div class="modal-body">
                                        <ol>
                                        <li><a href="http://paperpile.com/b/uDplkC/Uul1"><span style="font-weight: 400;">&ldquo;Acidosis/Alkalosis.&rdquo; [Online]. Available: </span></a><a href="https://labtestsonline.org/understanding/conditions/acidosis/"><span style="font-weight: 400;">https://labtestsonline.org/understanding/conditions/acidosis/</span></a><a href="http://paperpile.com/b/uDplkC/Uul1"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/61pi"><span style="font-weight: 400;">&ldquo;Mader.&rdquo; [Online]. Available: </span></a><a href="http://www.mhhe.com/biosci/genbio/maderbiology/supp/homeo.html"><span style="font-weight: 400;">http://www.mhhe.com/biosci/genbio/maderbiology/supp/homeo.html</span></a><a href="http://paperpile.com/b/uDplkC/61pi"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/zj6Y"><span style="font-weight: 400;">N. E. Lange, A. Litonjua, C. M. Hawrylowicz, and S. Weiss, &ldquo;Vitamin D, the immune system and asthma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 5, no. 6, pp. 693&ndash;702, Nov. 2009.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/aK4u"><span style="font-weight: 400;">P. Min&aacute;rik and P. Mlkv&yacute;, &ldquo;The role of calcium and vitamin D in the prevention of colorectal cancer,&rdquo; </span><em><span style="font-weight: 400;">Gastroent Hepatol</span></em><span style="font-weight: 400;">, vol. 70, no. 2, pp. 157&ndash;171, Apr. 2016.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/DHPS"><span style="font-weight: 400;">R. Chovatiya and R. Medzhitov, &ldquo;Stress, inflammation, and defense of homeostasis,&rdquo; </span><em><span style="font-weight: 400;">Mol. Cell</span></em><span style="font-weight: 400;">, vol. 54, no. 2, pp. 281&ndash;288, Apr. 2014.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/eYul"><span style="font-weight: 400;">W. S. Garrett, J. I. Gordon, and L. H. Glimcher, &ldquo;Homeostasis and inflammation in the intestine,&rdquo; </span><em><span style="font-weight: 400;">Cell</span></em><span style="font-weight: 400;">, vol. 140, no. 6, pp. 859&ndash;870, Mar. 2010.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/e8fc"><span style="font-weight: 400;">J. B. Dressman </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Upper gastrointestinal (GI) pH in young, healthy men and women,&rdquo; </span><em><span style="font-weight: 400;">Pharm. Res.</span></em><span style="font-weight: 400;">, vol. 7, no. 7, pp. 756&ndash;761, Jul. 1990.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/0Sjp"><span style="font-weight: 400;">H. Lambers, S. Piessens, A. Bloem, H. Pronk, and P. Finkel, &ldquo;Natural skin surface pH is on average below 5, which is beneficial for its resident flora,&rdquo; </span><em><span style="font-weight: 400;">Int. J. Cosmet. Sci.</span></em><span style="font-weight: 400;">, vol. 28, no. 5, pp. 359&ndash;370, Oct. 2006.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/haJZ"><span style="font-weight: 400;">B.-G. Kim </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Nasal pH in patients with chronic rhinosinusitis before and after endoscopic sinus surgery,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Otolaryngol.</span></em><span style="font-weight: 400;">, vol. 34, no. 5, pp. 505&ndash;507, Sep. 2013.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/BY2D"><span style="font-weight: 400;">M. Q. Rahman, K.-S. Chuah, E. C. A. Macdonald, J. P. M. Trusler, and K. Ramaesh, &ldquo;The effect of pH, dilution, and temperature on the viscosity of ocular lubricants&mdash;shift in rheological parameters and potential clinical significance,&rdquo; </span><em><span style="font-weight: 400;">Eye </span></em><span style="font-weight: 400;">, vol. 26, no. 12, pp. 1579&ndash;1584, Oct. 2012.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/Lfly"><span style="font-weight: 400;">J. Fallingborg, &ldquo;Intraluminal pH of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Dan. Med. Bull.</span></em><span style="font-weight: 400;">, 1999.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/hk57"><span style="font-weight: 400;">G. K. Schwalfenberg, &ldquo;The alkaline diet: is there evidence that an alkaline pH diet benefits health?,&rdquo; </span><em><span style="font-weight: 400;">J. Environ. Public Health</span></em><span style="font-weight: 400;">, vol. 2012, p. 727630, 2012.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/biO5"><span style="font-weight: 400;">N. G. de Santo, G. Capasso, G. Malnic, P. Anastasio, L. Spitali, and A. D&rsquo;Angelo, &ldquo;Effect of an acute oral protein load on renal acidification in healthy humans and in patients with chronic renal failure,&rdquo; </span><em><span style="font-weight: 400;">J. Am. Soc. Nephrol.</span></em><span style="font-weight: 400;">, vol. 8, no. 5, pp. 784&ndash;792, May 1997.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/t8BN"><span style="font-weight: 400;">T. R. Fenton and T. Huang, &ldquo;Systematic review of the association between dietary acid load, alkaline water and cancer,&rdquo; </span><em><span style="font-weight: 400;">BMJ Open</span></em><span style="font-weight: 400;">, vol. 6, no. 6, p. e010438, Jun. 2016.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/RbfK"><span style="font-weight: 400;">&ldquo;Your Digestive System and How It Works.&rdquo; [Online]. Available: </span></a><a href="https://www.niddk.nih.gov/health-information/health-topics/Anatomy/your-digestive-system/Pages/anatomy.aspx"><span style="font-weight: 400;">https://www.niddk.nih.gov/health-information/health-topics/Anatomy/your-digestive-system/Pages/anatomy.aspx</span></a><a href="http://paperpile.com/b/uDplkC/RbfK"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/Xiex"><span style="font-weight: 400;">C.-12 Foundation, &ldquo;Disruption of Homeostasis | CK-12 Foundation,&rdquo; 17-Jan-2011. [Online]. Available: </span></a><a href="http://www.ck12.org/user:ZXpvdWJpbmFAaG90bWFpbC5jb20./section/Disruption-of-Homeostasis-%253A%253Aof%253A%253A-Homeostasis-and-Regulation/"><span style="font-weight: 400;">http://www.ck12.org/user:ZXpvdWJpbmFAaG90bWFpbC5jb20./section/Disruption-of-Homeostasis-%253A%253Aof%253A%253A-Homeostasis-and-Regulation/</span></a><a href="http://paperpile.com/b/uDplkC/Xiex"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/SKWw"><span style="font-weight: 400;">&ldquo;Sympathetic nervous system,&rdquo; </span><em><span style="font-weight: 400;">ScienceDaily</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="https://www.sciencedaily.com/terms/sympathetic_nervous_system.htm"><span style="font-weight: 400;">https://www.sciencedaily.com/terms/sympathetic_nervous_system.htm</span></a><a href="http://paperpile.com/b/uDplkC/SKWw"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/lK7x"><span style="font-weight: 400;">R. H. Straub, R. Wiest, U. G. Strauch, P. H&auml;rle, and J. Sch&ouml;lmerich, &ldquo;The role of the sympathetic nervous system in intestinal inflammation,&rdquo; </span><em><span style="font-weight: 400;">Gut</span></em><span style="font-weight: 400;">, vol. 55, no. 11, pp. 1640&ndash;1649, Nov. 2006.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/BY1k"><span style="font-weight: 400;">H. H. Publications, &ldquo;Understanding the stress response - Harvard Health,&rdquo; </span><em><span style="font-weight: 400;">Harvard Health</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.health.harvard.edu/staying-healthy/understanding-the-stress-response"><span style="font-weight: 400;">http://www.health.harvard.edu/staying-healthy/understanding-the-stress-response</span></a><a href="http://paperpile.com/b/uDplkC/BY1k"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/l5Kd"><span style="font-weight: 400;">&ldquo;Stress Effects on the Body,&rdquo; </span><em><span style="font-weight: 400;">http://www.apa.org</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.apa.org/helpcenter/stress-body.aspx"><span style="font-weight: 400;">http://www.apa.org/helpcenter/stress-body.aspx</span></a><a href="http://paperpile.com/b/uDplkC/l5Kd"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/47AP"><span style="font-weight: 400;">E. T. Champagne, &ldquo;Low gastric hydrochloric acid secretion and mineral bioavailability,&rdquo; </span><em><span style="font-weight: 400;">Adv. Exp. Med. Biol.</span></em><span style="font-weight: 400;">, vol. 249, pp. 173&ndash;184, 1989.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/J6dd"><span style="font-weight: 400;">S. D. Krasinski </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Fundic atrophic gastritis in an elderly population. Effect on hemoglobin and several serum nutritional indicators,&rdquo; </span><em><span style="font-weight: 400;">J. Am. Geriatr. Soc.</span></em><span style="font-weight: 400;">, vol. 34, no. 11, pp. 800&ndash;806, Nov. 1986.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/Mw6m"><span style="font-weight: 400;">D. A. Greenwald, &ldquo;Aging, the gastrointestinal tract, and risk of acid-related disease,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Med.</span></em><span style="font-weight: 400;">, vol. 117 Suppl 5A, p. 8S&ndash;13S, Sep. 2004.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/Lux6"><span style="font-weight: 400;">H. J. Sugerman, &ldquo;Increased intra-abdominal pressure and GERD/Barrett&rsquo;s esophagus,&rdquo; </span><em><span style="font-weight: 400;">Gastroenterology</span></em><span style="font-weight: 400;">, vol. 133, no. 6, p. 2075; author reply 2075&ndash;6, Dec. 2007.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/85z2"><span style="font-weight: 400;">W. S. Yancy Jr, D. Provenzale, and E. C. Westman, &ldquo;Improvement of gastroesophageal reflux disease after initiation of a low-carbohydrate diet: five brief case reports,&rdquo; </span><em><span style="font-weight: 400;">Altern. Ther. Health Med.</span></em><span style="font-weight: 400;">, vol. 7, no. 6, pp. 120, 116&ndash;9, Nov. 2001.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/iA51"><span style="font-weight: 400;">G. L. Austin, M. T. Thiny, E. C. Westman, W. S. Yancy Jr, and N. J. Shaheen, &ldquo;A very low-carbohydrate diet improves gastroesophageal reflux and its symptoms,&rdquo; </span><em><span style="font-weight: 400;">Dig. Dis. Sci.</span></em><span style="font-weight: 400;">, vol. 51, no. 8, pp. 1307&ndash;1312, Aug. 2006.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/CewV"><span style="font-weight: 400;">W. A. Rutala, S. L. Barbee, N. C. Aguiar, M. D. Sobsey, and D. J. Weber, &ldquo;Antimicrobial activity of home disinfectants and natural products against potential human pathogens,&rdquo; </span><em><span style="font-weight: 400;">Infect. Control Hosp. Epidemiol.</span></em><span style="font-weight: 400;">, vol. 21, no. 1, pp. 33&ndash;38, Jan. 2000.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/0Hvr"><span style="font-weight: 400;">E. Entani, M. Asai, S. Tsujihata, Y. Tsukamoto, and M. Ohta, &ldquo;Antibacterial action of vinegar against food-borne pathogenic bacteria including Escherichia coli O157:H7,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 61, no. 8, pp. 953&ndash;959, Aug. 1998.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/OOoL"><span style="font-weight: 400;">J. Lukasik </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Reduction of poliovirus 1, bacteriophages, Salmonella montevideo, and Escherichia coli O157:H7 on strawberries by physical and disinfectant washes,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 66, no. 2, pp. 188&ndash;193, Feb. 2003.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/ioVx"><span style="font-weight: 400;">I. Yucel Sengun and M. Karapinar, &ldquo;Effectiveness of household natural sanitizers in the elimination of Salmonella typhimurium on rocket (Eruca sativa Miller) and spring onion (Allium cepa L.),&rdquo; </span><em><span style="font-weight: 400;">Int. J. Food Microbiol.</span></em><span style="font-weight: 400;">, vol. 98, no. 3, pp. 319&ndash;323, Feb. 2005.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/5Kpz"><span style="font-weight: 400;">C. Vijayakumar and C. E. Wolf-Hall, &ldquo;Evaluation of household sanitizers for reducing levels of Escherichia coli on iceberg lettuce,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 65, no. 10, pp. 1646&ndash;1650, Oct. 2002.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/Z7yA"><span style="font-weight: 400;">C. S. Johnston, I. Steplewska, C. A. Long, L. N. Harris, and R. H. Ryals, &ldquo;Examination of the antiglycemic properties of vinegar in healthy adults,&rdquo; </span><em><span style="font-weight: 400;">Ann. Nutr. Metab.</span></em><span style="font-weight: 400;">, vol. 56, no. 1, pp. 74&ndash;79, 2010.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/mDnP"><span style="font-weight: 400;">H. Liljeberg and I. Bj&ouml;rck, &ldquo;Delayed gastric emptying rate may explain improved glycaemia in healthy subjects to a starchy meal with added vinegar,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 52, no. 5, pp. 368&ndash;371, May 1998.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/xQzW"><span style="font-weight: 400;">M. Leeman, E. Ostman, and I. Bj&ouml;rck, &ldquo;Vinegar dressing and cold storage of potatoes lowers postprandial glycaemic and insulinaemic responses in healthy subjects,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 59, no. 11, pp. 1266&ndash;1271, Nov. 2005.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/HhrX"><span style="font-weight: 400;">K. Ebihara and A. Nakajima, &ldquo;Effect of acetic acid and vinegar on blood glucose and insulin responses to orally administered sucrose and starch,&rdquo; </span><em><span style="font-weight: 400;">Agric. Biol. Chem.</span></em><span style="font-weight: 400;">, vol. 52, no. 5, pp. 1311&ndash;1312, 1988.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/A3b9"><span style="font-weight: 400;">D. Urrego, A. P. Tomczak, F. Zahed, W. St&uuml;hmer, and L. A. Pardo, &ldquo;Potassium channels in cell cycle and cell proliferation,&rdquo; </span><em><span style="font-weight: 400;">Philos. Trans. R. Soc. Lond. B Biol. Sci.</span></em><span style="font-weight: 400;">, vol. 369, no. 1638, p. 20130094, Mar. 2014.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/k3uk"><span style="font-weight: 400;">A. C. Seydim, Z. B. Guzel-Seydim, D. K. Doguc, M. Cagrı Savas, and H. N. Budak, &ldquo;Effects of Grape Wine and Apple Cider Vinegar on Oxidative and Antioxidative Status in High Cholesterol-Fed Rats,&rdquo; </span><em><span style="font-weight: 400;">Functional Foods in Health and Disease</span></em><span style="font-weight: 400;">, vol. 6, no. 9, pp. 569&ndash;577, Sep. 2016.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/C0iz"><span style="font-weight: 400;">J. Boyer and R. H. Liu, &ldquo;Apple phytochemicals and their health benefits,&rdquo; </span><em><span style="font-weight: 400;">Nutr. J.</span></em><span style="font-weight: 400;">, vol. 3, p. 5, May 2004.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/qEfQ"><span style="font-weight: 400;">S. M. Willers </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Maternal food consumption during pregnancy and asthma, respiratory and atopic symptoms in 5-year-old children,&rdquo; </span><em><span style="font-weight: 400;">Thorax</span></em><span style="font-weight: 400;">, vol. 62, no. 9, pp. 773&ndash;779, Sep. 2007.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/O9Gi"><span style="font-weight: 400;">E. Tripoli, M. L. Guardia, S. Giammanco, D. D. Majo, and M. Giammanco, &ldquo;Citrus flavonoids: Molecular structure, biological activity and nutritional properties: A review,&rdquo; </span><em><span style="font-weight: 400;">Food Chem.</span></em><span style="font-weight: 400;">, vol. 104, no. 2, pp. 466&ndash;479, 2007.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/0qXb"><span style="font-weight: 400;">E. S. Wintergerst, S. Maggini, and D. H. Hornig, &ldquo;Immune-enhancing role of vitamin C and zinc and effect on clinical conditions,&rdquo; </span><em><span style="font-weight: 400;">Ann. Nutr. Metab.</span></em><span style="font-weight: 400;">, vol. 50, no. 2, pp. 85&ndash;94, 2006.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/6MCg"><span style="font-weight: 400;">K. J. Joshipura </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;The effect of fruit and vegetable intake on risk for coronary heart disease,&rdquo; </span><em><span style="font-weight: 400;">Ann. Intern. Med.</span></em><span style="font-weight: 400;">, vol. 134, no. 12, pp. 1106&ndash;1114, Jun. 2001.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/fTQY"><span style="font-weight: 400;">E. Meiyanto, A. Hermawan, and Anindyajati, &ldquo;Natural products for cancer-targeted therapy: citrus flavonoids as potent chemopreventive agents,&rdquo; </span><em><span style="font-weight: 400;">Asian Pac. J. Cancer Prev.</span></em><span style="font-weight: 400;">, vol. 13, no. 2, pp. 427&ndash;436, 2012.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/cOI2"><span style="font-weight: 400;">S. P&eacute;neau </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Relationship between iron status and dietary fruit and vegetables based on their vitamin C and fiber content,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 87, no. 5, pp. 1298&ndash;1305, May 2008.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/LAV2"><span style="font-weight: 400;">H. A. Cohen, I. Neuman, and H. Nahum, &ldquo;Blocking effect of vitamin C in exercise-induced asthma,&rdquo; </span><em><span style="font-weight: 400;">Arch. Pediatr. Adolesc. Med.</span></em><span style="font-weight: 400;">, vol. 151, no. 4, pp. 367&ndash;370, Apr. 1997.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/uPk5"><span style="font-weight: 400;">J. E. Enstrom, L. E. Kanim, and M. A. Klein, &ldquo;Vitamin C intake and mortality among a sample of the United States population,&rdquo; </span><em><span style="font-weight: 400;">Epidemiology</span></em><span style="font-weight: 400;">, vol. 3, no. 3, pp. 194&ndash;202, May 1992.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/WQIO"><span style="font-weight: 400;">K. L. Penniston, T. H. Steele, and S. Y. Nakada, &ldquo;Lemonade therapy increases urinary citrate and urine volumes in patients with recurrent calcium oxalate stone formation,&rdquo; </span><em><span style="font-weight: 400;">Urology</span></em><span style="font-weight: 400;">, vol. 70, no. 5, pp. 856&ndash;860, Nov. 2007.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/6SfJ"><span style="font-weight: 400;">B. Aras </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Can lemon juice be an alternative to potassium citrate in the treatment of urinary calcium stones in patients with hypocitraturia? A prospective randomized study,&rdquo; </span><em><span style="font-weight: 400;">Urol. Res.</span></em><span style="font-weight: 400;">, vol. 36, no. 6, pp. 313&ndash;317, Dec. 2008.</span></a></li>
                                        
                                        <li><a href="http://paperpile.com/b/uDplkC/R0Wr"><span style="font-weight: 400;">J. Kim, G. K. Jayaprakasha, R. M. Uckoo, and B. S. Patil, &ldquo;Evaluation of chemopreventive and cytotoxic effect of lemon seed extracts on human breast cancer (MCF-7) cells,&rdquo; </span><em><span style="font-weight: 400;">Food Chem. Toxicol.</span></em><span style="font-weight: 400;">, vol. 50, no. 2, pp. 423&ndash;430, Feb. 2012.</span></a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">

                                <p><span style="font-weight: 400;">[1]</span> <a href="http://paperpile.com/b/uDplkC/Uul1"><span style="font-weight: 400;">&ldquo;Acidosis/Alkalosis.&rdquo; [Online]. Available: </span></a><a href="https://labtestsonline.org/understanding/conditions/acidosis/"><span style="font-weight: 400;">https://labtestsonline.org/understanding/conditions/acidosis/</span></a><a href="http://paperpile.com/b/uDplkC/Uul1"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[2]</span> <a href="http://paperpile.com/b/uDplkC/61pi"><span style="font-weight: 400;">&ldquo;Mader.&rdquo; [Online]. Available: </span></a><a href="http://www.mhhe.com/biosci/genbio/maderbiology/supp/homeo.html"><span style="font-weight: 400;">http://www.mhhe.com/biosci/genbio/maderbiology/supp/homeo.html</span></a><a href="http://paperpile.com/b/uDplkC/61pi"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[3]</span> <a href="http://paperpile.com/b/uDplkC/zj6Y"><span style="font-weight: 400;">N. E. Lange, A. Litonjua, C. M. Hawrylowicz, and S. Weiss, &ldquo;Vitamin D, the immune system and asthma,&rdquo; </span><em><span style="font-weight: 400;">Expert Rev. Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 5, no. 6, pp. 693&ndash;702, Nov. 2009.</span></a></p>
                                <p><span style="font-weight: 400;">[4]</span> <a href="http://paperpile.com/b/uDplkC/aK4u"><span style="font-weight: 400;">P. Min&aacute;rik and P. Mlkv&yacute;, &ldquo;The role of calcium and vitamin D in the prevention of colorectal cancer,&rdquo; </span><em><span style="font-weight: 400;">Gastroent Hepatol</span></em><span style="font-weight: 400;">, vol. 70, no. 2, pp. 157&ndash;171, Apr. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[5]</span> <a href="http://paperpile.com/b/uDplkC/DHPS"><span style="font-weight: 400;">R. Chovatiya and R. Medzhitov, &ldquo;Stress, inflammation, and defense of homeostasis,&rdquo; </span><em><span style="font-weight: 400;">Mol. Cell</span></em><span style="font-weight: 400;">, vol. 54, no. 2, pp. 281&ndash;288, Apr. 2014.</span></a></p>
                                <p><span style="font-weight: 400;">[6]</span> <a href="http://paperpile.com/b/uDplkC/eYul"><span style="font-weight: 400;">W. S. Garrett, J. I. Gordon, and L. H. Glimcher, &ldquo;Homeostasis and inflammation in the intestine,&rdquo; </span><em><span style="font-weight: 400;">Cell</span></em><span style="font-weight: 400;">, vol. 140, no. 6, pp. 859&ndash;870, Mar. 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[7]</span> <a href="http://paperpile.com/b/uDplkC/e8fc"><span style="font-weight: 400;">J. B. Dressman </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Upper gastrointestinal (GI) pH in young, healthy men and women,&rdquo; </span><em><span style="font-weight: 400;">Pharm. Res.</span></em><span style="font-weight: 400;">, vol. 7, no. 7, pp. 756&ndash;761, Jul. 1990.</span></a></p>
                                <p><span style="font-weight: 400;">[8]</span> <a href="http://paperpile.com/b/uDplkC/0Sjp"><span style="font-weight: 400;">H. Lambers, S. Piessens, A. Bloem, H. Pronk, and P. Finkel, &ldquo;Natural skin surface pH is on average below 5, which is beneficial for its resident flora,&rdquo; </span><em><span style="font-weight: 400;">Int. J. Cosmet. Sci.</span></em><span style="font-weight: 400;">, vol. 28, no. 5, pp. 359&ndash;370, Oct. 2006.</span></a></p>
                                <p><span style="font-weight: 400;">[9]</span> <a href="http://paperpile.com/b/uDplkC/haJZ"><span style="font-weight: 400;">B.-G. Kim </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Nasal pH in patients with chronic rhinosinusitis before and after endoscopic sinus surgery,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Otolaryngol.</span></em><span style="font-weight: 400;">, vol. 34, no. 5, pp. 505&ndash;507, Sep. 2013.</span></a></p>
                                <p><span style="font-weight: 400;">[10]</span> <a href="http://paperpile.com/b/uDplkC/BY2D"><span style="font-weight: 400;">M. Q. Rahman, K.-S. Chuah, E. C. A. Macdonald, J. P. M. Trusler, and K. Ramaesh, &ldquo;The effect of pH, dilution, and temperature on the viscosity of ocular lubricants&mdash;shift in rheological parameters and potential clinical significance,&rdquo; </span><em><span style="font-weight: 400;">Eye </span></em><span style="font-weight: 400;">, vol. 26, no. 12, pp. 1579&ndash;1584, Oct. 2012.</span></a></p>
                                <p><span style="font-weight: 400;">[11]</span> <a href="http://paperpile.com/b/uDplkC/Lfly"><span style="font-weight: 400;">J. Fallingborg, &ldquo;Intraluminal pH of the human gastrointestinal tract,&rdquo; </span><em><span style="font-weight: 400;">Dan. Med. Bull.</span></em><span style="font-weight: 400;">, 1999.</span></a></p>
                                <p><span style="font-weight: 400;">[12]</span> <a href="http://paperpile.com/b/uDplkC/hk57"><span style="font-weight: 400;">G. K. Schwalfenberg, &ldquo;The alkaline diet: is there evidence that an alkaline pH diet benefits health?,&rdquo; </span><em><span style="font-weight: 400;">J. Environ. Public Health</span></em><span style="font-weight: 400;">, vol. 2012, p. 727630, 2012.</span></a></p>
                                <p><span style="font-weight: 400;">[13]</span> <a href="http://paperpile.com/b/uDplkC/biO5"><span style="font-weight: 400;">N. G. de Santo, G. Capasso, G. Malnic, P. Anastasio, L. Spitali, and A. D&rsquo;Angelo, &ldquo;Effect of an acute oral protein load on renal acidification in healthy humans and in patients with chronic renal failure,&rdquo; </span><em><span style="font-weight: 400;">J. Am. Soc. Nephrol.</span></em><span style="font-weight: 400;">, vol. 8, no. 5, pp. 784&ndash;792, May 1997.</span></a></p>
                                <p><span style="font-weight: 400;">[14]</span> <a href="http://paperpile.com/b/uDplkC/t8BN"><span style="font-weight: 400;">T. R. Fenton and T. Huang, &ldquo;Systematic review of the association between dietary acid load, alkaline water and cancer,&rdquo; </span><em><span style="font-weight: 400;">BMJ Open</span></em><span style="font-weight: 400;">, vol. 6, no. 6, p. e010438, Jun. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[15]</span> <a href="http://paperpile.com/b/uDplkC/RbfK"><span style="font-weight: 400;">&ldquo;Your Digestive System and How It Works.&rdquo; [Online]. Available: </span></a><a href="https://www.niddk.nih.gov/health-information/health-topics/Anatomy/your-digestive-system/Pages/anatomy.aspx"><span style="font-weight: 400;">https://www.niddk.nih.gov/health-information/health-topics/Anatomy/your-digestive-system/Pages/anatomy.aspx</span></a><a href="http://paperpile.com/b/uDplkC/RbfK"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[16]</span> <a href="http://paperpile.com/b/uDplkC/Xiex"><span style="font-weight: 400;">C.-12 Foundation, &ldquo;Disruption of Homeostasis | CK-12 Foundation,&rdquo; 17-Jan-2011. [Online]. Available: </span></a><a href="http://www.ck12.org/user:ZXpvdWJpbmFAaG90bWFpbC5jb20./section/Disruption-of-Homeostasis-%253A%253Aof%253A%253A-Homeostasis-and-Regulation/"><span style="font-weight: 400;">http://www.ck12.org/user:ZXpvdWJpbmFAaG90bWFpbC5jb20./section/Disruption-of-Homeostasis-%253A%253Aof%253A%253A-Homeostasis-and-Regulation/</span></a><a href="http://paperpile.com/b/uDplkC/Xiex"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[17]</span> <a href="http://paperpile.com/b/uDplkC/SKWw"><span style="font-weight: 400;">&ldquo;Sympathetic nervous system,&rdquo; </span><em><span style="font-weight: 400;">ScienceDaily</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="https://www.sciencedaily.com/terms/sympathetic_nervous_system.htm"><span style="font-weight: 400;">https://www.sciencedaily.com/terms/sympathetic_nervous_system.htm</span></a><a href="http://paperpile.com/b/uDplkC/SKWw"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[18]</span> <a href="http://paperpile.com/b/uDplkC/lK7x"><span style="font-weight: 400;">R. H. Straub, R. Wiest, U. G. Strauch, P. H&auml;rle, and J. Sch&ouml;lmerich, &ldquo;The role of the sympathetic nervous system in intestinal inflammation,&rdquo; </span><em><span style="font-weight: 400;">Gut</span></em><span style="font-weight: 400;">, vol. 55, no. 11, pp. 1640&ndash;1649, Nov. 2006.</span></a></p>
                                <p><span style="font-weight: 400;">[19]</span> <a href="http://paperpile.com/b/uDplkC/BY1k"><span style="font-weight: 400;">H. H. Publications, &ldquo;Understanding the stress response - Harvard Health,&rdquo; </span><em><span style="font-weight: 400;">Harvard Health</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.health.harvard.edu/staying-healthy/understanding-the-stress-response"><span style="font-weight: 400;">http://www.health.harvard.edu/staying-healthy/understanding-the-stress-response</span></a><a href="http://paperpile.com/b/uDplkC/BY1k"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[20]</span> <a href="http://paperpile.com/b/uDplkC/l5Kd"><span style="font-weight: 400;">&ldquo;Stress Effects on the Body,&rdquo; </span><em><span style="font-weight: 400;">http://www.apa.org</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="http://www.apa.org/helpcenter/stress-body.aspx"><span style="font-weight: 400;">http://www.apa.org/helpcenter/stress-body.aspx</span></a><a href="http://paperpile.com/b/uDplkC/l5Kd"><span style="font-weight: 400;">. [Accessed: 24-Feb-2017].</span></a></p>
                                <p><span style="font-weight: 400;">[21]</span> <a href="http://paperpile.com/b/uDplkC/47AP"><span style="font-weight: 400;">E. T. Champagne, &ldquo;Low gastric hydrochloric acid secretion and mineral bioavailability,&rdquo; </span><em><span style="font-weight: 400;">Adv. Exp. Med. Biol.</span></em><span style="font-weight: 400;">, vol. 249, pp. 173&ndash;184, 1989.</span></a></p>
                                <p><span style="font-weight: 400;">[22]</span> <a href="http://paperpile.com/b/uDplkC/J6dd"><span style="font-weight: 400;">S. D. Krasinski </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Fundic atrophic gastritis in an elderly population. Effect on hemoglobin and several serum nutritional indicators,&rdquo; </span><em><span style="font-weight: 400;">J. Am. Geriatr. Soc.</span></em><span style="font-weight: 400;">, vol. 34, no. 11, pp. 800&ndash;806, Nov. 1986.</span></a></p>
                                <p><span style="font-weight: 400;">[23]</span> <a href="http://paperpile.com/b/uDplkC/Mw6m"><span style="font-weight: 400;">D. A. Greenwald, &ldquo;Aging, the gastrointestinal tract, and risk of acid-related disease,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Med.</span></em><span style="font-weight: 400;">, vol. 117 Suppl 5A, p. 8S&ndash;13S, Sep. 2004.</span></a></p>
                                <p><span style="font-weight: 400;">[24]</span> <a href="http://paperpile.com/b/uDplkC/Lux6"><span style="font-weight: 400;">H. J. Sugerman, &ldquo;Increased intra-abdominal pressure and GERD/Barrett&rsquo;s esophagus,&rdquo; </span><em><span style="font-weight: 400;">Gastroenterology</span></em><span style="font-weight: 400;">, vol. 133, no. 6, p. 2075; author reply 2075&ndash;6, Dec. 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[25]</span> <a href="http://paperpile.com/b/uDplkC/85z2"><span style="font-weight: 400;">W. S. Yancy Jr, D. Provenzale, and E. C. Westman, &ldquo;Improvement of gastroesophageal reflux disease after initiation of a low-carbohydrate diet: five brief case reports,&rdquo; </span><em><span style="font-weight: 400;">Altern. Ther. Health Med.</span></em><span style="font-weight: 400;">, vol. 7, no. 6, pp. 120, 116&ndash;9, Nov. 2001.</span></a></p>
                                <p><span style="font-weight: 400;">[26]</span> <a href="http://paperpile.com/b/uDplkC/iA51"><span style="font-weight: 400;">G. L. Austin, M. T. Thiny, E. C. Westman, W. S. Yancy Jr, and N. J. Shaheen, &ldquo;A very low-carbohydrate diet improves gastroesophageal reflux and its symptoms,&rdquo; </span><em><span style="font-weight: 400;">Dig. Dis. Sci.</span></em><span style="font-weight: 400;">, vol. 51, no. 8, pp. 1307&ndash;1312, Aug. 2006.</span></a></p>
                                <p><span style="font-weight: 400;">[27]</span> <a href="http://paperpile.com/b/uDplkC/CewV"><span style="font-weight: 400;">W. A. Rutala, S. L. Barbee, N. C. Aguiar, M. D. Sobsey, and D. J. Weber, &ldquo;Antimicrobial activity of home disinfectants and natural products against potential human pathogens,&rdquo; </span><em><span style="font-weight: 400;">Infect. Control Hosp. Epidemiol.</span></em><span style="font-weight: 400;">, vol. 21, no. 1, pp. 33&ndash;38, Jan. 2000.</span></a></p>
                                <p><span style="font-weight: 400;">[28]</span> <a href="http://paperpile.com/b/uDplkC/0Hvr"><span style="font-weight: 400;">E. Entani, M. Asai, S. Tsujihata, Y. Tsukamoto, and M. Ohta, &ldquo;Antibacterial action of vinegar against food-borne pathogenic bacteria including Escherichia coli O157:H7,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 61, no. 8, pp. 953&ndash;959, Aug. 1998.</span></a></p>
                                <p><span style="font-weight: 400;">[29]</span> <a href="http://paperpile.com/b/uDplkC/OOoL"><span style="font-weight: 400;">J. Lukasik </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Reduction of poliovirus 1, bacteriophages, Salmonella montevideo, and Escherichia coli O157:H7 on strawberries by physical and disinfectant washes,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 66, no. 2, pp. 188&ndash;193, Feb. 2003.</span></a></p>
                                <p><span style="font-weight: 400;">[30]</span> <a href="http://paperpile.com/b/uDplkC/ioVx"><span style="font-weight: 400;">I. Yucel Sengun and M. Karapinar, &ldquo;Effectiveness of household natural sanitizers in the elimination of Salmonella typhimurium on rocket (Eruca sativa Miller) and spring onion (Allium cepa L.),&rdquo; </span><em><span style="font-weight: 400;">Int. J. Food Microbiol.</span></em><span style="font-weight: 400;">, vol. 98, no. 3, pp. 319&ndash;323, Feb. 2005.</span></a></p>
                                <p><span style="font-weight: 400;">[31]</span> <a href="http://paperpile.com/b/uDplkC/5Kpz"><span style="font-weight: 400;">C. Vijayakumar and C. E. Wolf-Hall, &ldquo;Evaluation of household sanitizers for reducing levels of Escherichia coli on iceberg lettuce,&rdquo; </span><em><span style="font-weight: 400;">J. Food Prot.</span></em><span style="font-weight: 400;">, vol. 65, no. 10, pp. 1646&ndash;1650, Oct. 2002.</span></a></p>
                                <p><span style="font-weight: 400;">[32]</span> <a href="http://paperpile.com/b/uDplkC/Z7yA"><span style="font-weight: 400;">C. S. Johnston, I. Steplewska, C. A. Long, L. N. Harris, and R. H. Ryals, &ldquo;Examination of the antiglycemic properties of vinegar in healthy adults,&rdquo; </span><em><span style="font-weight: 400;">Ann. Nutr. Metab.</span></em><span style="font-weight: 400;">, vol. 56, no. 1, pp. 74&ndash;79, 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[33]</span> <a href="http://paperpile.com/b/uDplkC/mDnP"><span style="font-weight: 400;">H. Liljeberg and I. Bj&ouml;rck, &ldquo;Delayed gastric emptying rate may explain improved glycaemia in healthy subjects to a starchy meal with added vinegar,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 52, no. 5, pp. 368&ndash;371, May 1998.</span></a></p>
                                <p><span style="font-weight: 400;">[34]</span> <a href="http://paperpile.com/b/uDplkC/xQzW"><span style="font-weight: 400;">M. Leeman, E. Ostman, and I. Bj&ouml;rck, &ldquo;Vinegar dressing and cold storage of potatoes lowers postprandial glycaemic and insulinaemic responses in healthy subjects,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 59, no. 11, pp. 1266&ndash;1271, Nov. 2005.</span></a></p>
                                <p><span style="font-weight: 400;">[35]</span> <a href="http://paperpile.com/b/uDplkC/HhrX"><span style="font-weight: 400;">K. Ebihara and A. Nakajima, &ldquo;Effect of acetic acid and vinegar on blood glucose and insulin responses to orally administered sucrose and starch,&rdquo; </span><em><span style="font-weight: 400;">Agric. Biol. Chem.</span></em><span style="font-weight: 400;">, vol. 52, no. 5, pp. 1311&ndash;1312, 1988.</span></a></p>
                                <p><span style="font-weight: 400;">[36]</span> <a href="http://paperpile.com/b/uDplkC/A3b9"><span style="font-weight: 400;">D. Urrego, A. P. Tomczak, F. Zahed, W. St&uuml;hmer, and L. A. Pardo, &ldquo;Potassium channels in cell cycle and cell proliferation,&rdquo; </span><em><span style="font-weight: 400;">Philos. Trans. R. Soc. Lond. B Biol. Sci.</span></em><span style="font-weight: 400;">, vol. 369, no. 1638, p. 20130094, Mar. 2014.</span></a></p>
                                <p><span style="font-weight: 400;">[37]</span> <a href="http://paperpile.com/b/uDplkC/k3uk"><span style="font-weight: 400;">A. C. Seydim, Z. B. Guzel-Seydim, D. K. Doguc, M. Cagrı Savas, and H. N. Budak, &ldquo;Effects of Grape Wine and Apple Cider Vinegar on Oxidative and Antioxidative Status in High Cholesterol-Fed Rats,&rdquo; </span><em><span style="font-weight: 400;">Functional Foods in Health and Disease</span></em><span style="font-weight: 400;">, vol. 6, no. 9, pp. 569&ndash;577, Sep. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[38]</span> <a href="http://paperpile.com/b/uDplkC/C0iz"><span style="font-weight: 400;">J. Boyer and R. H. Liu, &ldquo;Apple phytochemicals and their health benefits,&rdquo; </span><em><span style="font-weight: 400;">Nutr. J.</span></em><span style="font-weight: 400;">, vol. 3, p. 5, May 2004.</span></a></p>
                                <p><span style="font-weight: 400;">[39]</span> <a href="http://paperpile.com/b/uDplkC/qEfQ"><span style="font-weight: 400;">S. M. Willers </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Maternal food consumption during pregnancy and asthma, respiratory and atopic symptoms in 5-year-old children,&rdquo; </span><em><span style="font-weight: 400;">Thorax</span></em><span style="font-weight: 400;">, vol. 62, no. 9, pp. 773&ndash;779, Sep. 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[40]</span> <a href="http://paperpile.com/b/uDplkC/O9Gi"><span style="font-weight: 400;">E. Tripoli, M. L. Guardia, S. Giammanco, D. D. Majo, and M. Giammanco, &ldquo;Citrus flavonoids: Molecular structure, biological activity and nutritional properties: A review,&rdquo; </span><em><span style="font-weight: 400;">Food Chem.</span></em><span style="font-weight: 400;">, vol. 104, no. 2, pp. 466&ndash;479, 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[41]</span> <a href="http://paperpile.com/b/uDplkC/0qXb"><span style="font-weight: 400;">E. S. Wintergerst, S. Maggini, and D. H. Hornig, &ldquo;Immune-enhancing role of vitamin C and zinc and effect on clinical conditions,&rdquo; </span><em><span style="font-weight: 400;">Ann. Nutr. Metab.</span></em><span style="font-weight: 400;">, vol. 50, no. 2, pp. 85&ndash;94, 2006.</span></a></p>
                                <p><span style="font-weight: 400;">[42]</span> <a href="http://paperpile.com/b/uDplkC/6MCg"><span style="font-weight: 400;">K. J. Joshipura </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;The effect of fruit and vegetable intake on risk for coronary heart disease,&rdquo; </span><em><span style="font-weight: 400;">Ann. Intern. Med.</span></em><span style="font-weight: 400;">, vol. 134, no. 12, pp. 1106&ndash;1114, Jun. 2001.</span></a></p>
                                <p><span style="font-weight: 400;">[43]</span> <a href="http://paperpile.com/b/uDplkC/fTQY"><span style="font-weight: 400;">E. Meiyanto, A. Hermawan, and Anindyajati, &ldquo;Natural products for cancer-targeted therapy: citrus flavonoids as potent chemopreventive agents,&rdquo; </span><em><span style="font-weight: 400;">Asian Pac. J. Cancer Prev.</span></em><span style="font-weight: 400;">, vol. 13, no. 2, pp. 427&ndash;436, 2012.</span></a></p>
                                <p><span style="font-weight: 400;">[44]</span> <a href="http://paperpile.com/b/uDplkC/cOI2"><span style="font-weight: 400;">S. P&eacute;neau </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Relationship between iron status and dietary fruit and vegetables based on their vitamin C and fiber content,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 87, no. 5, pp. 1298&ndash;1305, May 2008.</span></a></p>
                                <p><span style="font-weight: 400;">[45]</span> <a href="http://paperpile.com/b/uDplkC/LAV2"><span style="font-weight: 400;">H. A. Cohen, I. Neuman, and H. Nahum, &ldquo;Blocking effect of vitamin C in exercise-induced asthma,&rdquo; </span><em><span style="font-weight: 400;">Arch. Pediatr. Adolesc. Med.</span></em><span style="font-weight: 400;">, vol. 151, no. 4, pp. 367&ndash;370, Apr. 1997.</span></a></p>
                                <p><span style="font-weight: 400;">[46]</span> <a href="http://paperpile.com/b/uDplkC/uPk5"><span style="font-weight: 400;">J. E. Enstrom, L. E. Kanim, and M. A. Klein, &ldquo;Vitamin C intake and mortality among a sample of the United States population,&rdquo; </span><em><span style="font-weight: 400;">Epidemiology</span></em><span style="font-weight: 400;">, vol. 3, no. 3, pp. 194&ndash;202, May 1992.</span></a></p>
                                <p><span style="font-weight: 400;">[47]</span> <a href="http://paperpile.com/b/uDplkC/WQIO"><span style="font-weight: 400;">K. L. Penniston, T. H. Steele, and S. Y. Nakada, &ldquo;Lemonade therapy increases urinary citrate and urine volumes in patients with recurrent calcium oxalate stone formation,&rdquo; </span><em><span style="font-weight: 400;">Urology</span></em><span style="font-weight: 400;">, vol. 70, no. 5, pp. 856&ndash;860, Nov. 2007.</span></a></p>
                                <p><span style="font-weight: 400;">[48]</span> <a href="http://paperpile.com/b/uDplkC/6SfJ"><span style="font-weight: 400;">B. Aras </span><em><span style="font-weight: 400;">et al.</span></em><span style="font-weight: 400;">, &ldquo;Can lemon juice be an alternative to potassium citrate in the treatment of urinary calcium stones in patients with hypocitraturia? A prospective randomized study,&rdquo; </span><em><span style="font-weight: 400;">Urol. Res.</span></em><span style="font-weight: 400;">, vol. 36, no. 6, pp. 313&ndash;317, Dec. 2008.</span></a></p>
                                <p><span style="font-weight: 400;">[49]</span> <a href="http://paperpile.com/b/uDplkC/R0Wr"><span style="font-weight: 400;">J. Kim, G. K. Jayaprakasha, R. M. Uckoo, and B. S. Patil, &ldquo;Evaluation of chemopreventive and cytotoxic effect of lemon seed extracts on human breast cancer (MCF-7) cells,&rdquo; </span><em><span style="font-weight: 400;">Food Chem. Toxicol.</span></em><span style="font-weight: 400;">, vol. 50, no. 2, pp. 423&ndash;430, Feb. 2012.</span></a></p>
                                <h2><br /><br /></h2>


                            </div>
                        </div>-->
                    </div>
                </div>

            </div>
            <!-- sectiune capitol     -->
        </div>
    </div>
    <!--  card     -->
</div>
<!--  tabe    -->
