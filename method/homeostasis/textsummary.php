<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>
        <div class="textscrollsummary textsummaryfirst" data-Texttype="1">

            <div class="nuttshell">
                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p><strong><span style="font-weight: 400;">Our body operates a continuous process of maintaining a condition of equilibrium when dealing with internal and external changes. Various causes can negatively affect this process, leading to general health problems including asthma and allergies. A simple and fast way to boost the homeostasis is to:
</span></strong></p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Take Apple Cider Vinegar with bicarbonate soda and water</h3>
                        <p>In a glass mix:</p>

                        <ul class="general-list">
                            <li>2 tablespoons of a high quality Apple Cider Vinegar</li>
                            <li>1/2 tablespoon of baking soda</li>
                            <li>6 oz (200 ml) of water</li>

                        </ul>

                        <p>This remedy should be taken twice a day on an empty stomach, preferably 15-30 minutes before eating to allow for the digestive juices to better breakdown your food.</p>
                    </div>
                </div>
                <div class="do2">
                    <div class="dodont-content">

                        <h3>Take lemon juice with bicarbonate soda and water (optional)</h3>
                        <p>In a glass mix:</p>

                        <ul class="general-list">
                            <li>1 squeezed lemon or lime</li>
                            <li>1/2 tablespoon of baking soda</li>
                            <li>6 oz (200 ml) of water</li>

                        </ul>

                        <p>Take the solution once or twice a day on an empty stomach.</p>
                    </div>
                </div>

            </div>

        </div>
        <div class="textscrollsummary" data-Texttype="2">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Balance and Homeostasis</span></h2>

                <p>Balance is everywhere and in everything. The universe holds an equilibrium between its natural laws. Our planet performs multiple balancing twirls, connecting its systems to form a complex intrinsic whole, and our bodies operate a continuous process of maintaining a condition of equilibrium when dealing with internal and external changes.</p>
                <p><span style="font-weight: 400;">This process is called homeostasis. &nbsp;Each body system - circulatory, digestive, nervous, respiratory - contributes to the homeostasis of other systems and the entire organism.</span></p>
                <p><span style="font-weight: 400;">A particular kind of homeostasis requires increased awareness - the one that regulates the body's pH - or the constant struggle between acidity and alkalinity. We must maintain a perfect blood pH level of 7.4 for our primary systems to function properly.</span></p>
                <p><span style="font-weight: 400;">Nevertheless, the pH homeostasis is heavily susceptible to lifestyle changes and has a determined impact on our allergies and asthma. </span></p>
                <p><span style="font-weight: 400;">On one hand, a reduced acid level creates a vicious cycle of poor digestion, &ldquo;leaky&rdquo; gut, microbial overgrowth, elevated stress hormones and lowered nutrient absorption. </span></p>
                <p><span style="font-weight: 400;">On the the other hand, excessive overall acid is again an issue. As a consequence, our ability to naturally produce baking soda - the acid&rsquo;s compensatory element - &nbsp;through blood, respiratory and renal system may get overburdened.</span></p>
                <p><span style="font-weight: 400;">Both extremes negatively affect our body&rsquo;s homeostasis and can indirectly create an environment that leads to or increases the intensity of asthma and allergies. </span></p>

                <h2><span style="font-weight: 400;">Protect the Acid, Improve the Homeostasis</span></h2>

                <p><span style="font-weight: 400;">Various things can influence our body&rsquo;s ability to produce a proper level of stomach acid and maintain a healthy pH homeostasis: Vitamin D, stress, the fatty acid ratio, and the health of our gut flora. &nbsp;We are discussing the above in detail in Chapters 1, 3, and 4.</span></p>
                <p><span style="font-weight: 400;">Still, there&rsquo;s something else we can do. Nature is full of surprises, and, as it is always the case, if we immerse in it and follow its guidelines, we will benefit. </span></p>


                <h2><span style="font-weight: 400;">Take Apple Cider Vinegar with Bicarbonate Soda and Water</span></h2>

                <p><span style="font-weight: 400;">The ACV contains over 90 different nutrients and vitamins, including some beneficial compounds such as acetic and malic acid, potassium, Vitamin A, Vitamin B6, Vitamin C, and others. </span></p>
                <p><span style="font-weight: 400;">With this impressive combo of nutrients, the ACV can positively impact our vital systems and ensure they operate the full throttle. </span></p>
                <p><span style="font-weight: 400;">First, vinegar can help kill pathogens, including bacteria and stimulate the healthy flora of our gut. In addition, vinegar can increase insulin sensitivity and significantly lower blood sugar responses during meals. Also, ACV has an important antioxidant perspective converting superoxide and other harmful free radicals into less harmful oxygen and hydrogen peroxide.</span></p>
                <p><span style="font-weight: 400;">Finally, various studies are exploring the positive potential impact of ACV on allergies and asthma.</span></p>


                <h3><span style="font-weight: 400;">How to take it:</span></h3>

                <p>In a glass mix:</p>

                <ul class="general-list">
                    <li>2 tablespoons of a high quality Apple Cider Vinegar</li>
                    <li>1/2 tablespoon of baking soda</li>
                    <li>6 oz (200 ml) of water</li>

                </ul>

                <p>This remedy should be taken twice a day on an empty stomach, preferably 15-30 minutes before eating to allow for the digestive juices to better breakdown your food.</p>

                <h2><span style="font-weight: 400;">Take Lemon Juice with Bicarbonate Soda and Water</span></h2>

                <p><span style="font-weight: 400;">The lemon juice enhances our immune function, protects against various diseases, and increase the absorption of iron. </span></p>
                <p><span style="font-weight: 400;">Also, research concludes Vitamin C in the lemon juice helps reduce the exercise-induced asthma symptoms. </span></p>
                <p><span style="font-weight: 400;">Ultimately, lemons have been associated with other numerous health benefits like cardiovascular health, prevention of kidney stones, prevention of anemia, and even cancer.</span></p>



                <h3><span style="font-weight: 400;">How to take it:</span></h3>

                <h3>Take lemon juice with bicarbonate soda and water (optional)</h3>
                <p>In a glass mix:</p>

                <ul class="general-list">
                    <li>1 squeezed lemon or lime</li>
                    <li>1/2 tablespoon of baking soda</li>
                    <li>6 oz (200 ml) of water</li>

                </ul>

                <p>Take the solution once or twice a day on an empty stomach.</p>

                <h2><span style="font-weight: 400;">For Children</span></h2>

                <p>In general, it is safe to give your children ACV or lemon juice with water and bicarbonate soda. I haven’t come across any study that would caution the use of these treatments. However, it is important to use raw high-quality ACV and appropriately diluted it with water. As with the other steps in the RISE method, always consult with your pediatrician.
                </p>

            </div>




        </div>
    </div>
</div>
