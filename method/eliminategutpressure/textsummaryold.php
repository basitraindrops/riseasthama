<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>
        <div class="textscrollsummary textsummaryfirst" data-Texttype="1">
            <div class="nuttshell">
                <div class="nutshell-content">
                    <h2><span style="font-weight: 400;">In a nutshell</span></h2>
                    <p><span style="font-weight: 400;">The bacterial microbiota in our gut regulates the so called intestinal barrier that allows properly digested food to pass into our bloodstream. If the microbiota is unhealthy then this barrier might get damaged and small particles of food or other substances could leaky in our system. </span></p>
                    <p><span style="font-weight: 400;">The long term consequence is that our immune system will to continue recognize to &nbsp;these substances as foreign invaders and trigger inflammation when they are encountered. This syndrome is called &ldquo;the leaky gut&rdquo; and causes or exacerbates asthma and allergies. The key is to ensure that the bacterias stay healthy so that the intestinal barrier is optimal. How to do it:</span></p>



                </div>

                <div class="do">
                    <div class="dodont-content">


                        <ul class="general-list">
                            <li>Eat whole and fresh foods</li>
                            <li>Eat lots of fiber</li>
                            <li>Adapt the fruit consumption to your lifestyle</li>
                            <li>Run a two week carbohydrates intolerance test and experiment with gluten and dairy</li>
                            <li>Take pre and probiotics if an antibiotic treatment is absolutely required</li>
                        </ul>
                    </div>
                </div>


                <div class="dont">
                    <div class="dodont-content">
                        <ul class="general-list">
                            <li>Don't eat processed-junk food</li>
                            <li>Don't eat refined sugar</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="textscrollsummary" data-Texttype="2">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The fascinating universe of bacteria</span></h2>
                <p><span style="font-weight: 400;">Bacteria represents the most abundant and diverse organism on Earth playing a vital role for both the environment and the human health.</span></p>
                <p><span style="font-weight: 400;">We harbor a galaxy of 100 trillion bacteria in our body outnumbering the human cells by 1/10. We are 10% human and 90% a walking bacteria colony. Humans are colonized with bacteria when they pass through the birth canal, after which various factors determine the microbiome composition. A good ratio between the &ldquo;good&rdquo; and &ldquo;bad&rdquo; bacteria leads to health benefits. The opposite is called dysbiosis can have an adverse impact on our well-being.</span></p>
                <p><span style="font-weight: 400;">To survive, the bacteria need food, space to grow, and a healthy host to support them. For attaining their goals, the microbes evolved to coexist with us in a symbiotic relationship by providing specific invaluable &ldquo;services&rdquo; or playing mind &ldquo;tricks&rdquo;.</span></p>
                <p><span style="font-weight: 400;">A first service is that they help digest certain foods like fiber or sugar polymers and extract additional nutrients for the host. We feed them, and they feed us.</span></p>
                <p><span style="font-weight: 400;">Second, different types bacteria have different menu preferences and through the fascinating brain-gut connection, they can influence the way we think and crave for food. The implications are complex as it can determine mood changes or even lead to serious mental disorders.</span></p>
                <p><span style="font-weight: 400;">Finally, our microbial friends fight with pathogens - such as yeast or fungus - for space and resources. But one the their most important function is that they play a crucial role in regulating our immune system. A healthy host is a host that they will thrive in.</span></p>
                <h2><span style="font-weight: 400;">The microbiome, the &ldquo;leaky gut&rdquo; and asthma</span></h2>
                <p><span style="font-weight: 400;">The microbiome modulates the so-called intestinal barrier that allows the absorption of nutrients, electrolytes, and water while maintaining an efficient defense against toxins and antigens. Simply put, this tight junction of cells allows the digested food flow into the bloodstream, but stops any opportunistic pathogens.</span></p>
                <p><span style="font-weight: 400;">Sometimes the cells lining the gut are damaged, or the places where the junctions are joined to each other are weakened. When this happens, microscopic holes are formed through which some of the contents of the gut can leak out into the bloodstream.</span></p>
                <p><span style="font-weight: 400;">Next, our immune cells recognize them as foreign invaders and mount a response against them. The consequence is that when the immune system encounters these foods and substances again, it might be perceived them as threatening and trigger inflammation. </span></p>
                <p><span style="font-weight: 400;">The result is that this &ldquo;leaky gut&rdquo; syndrome is present in every autoimmune disease in which it has been tested and also in asthma and allergies. It might have even determined all the major chronic diseases in our species (in the agricultural revolution period) including asthma and allergies.</span></p>
                <p><span style="font-weight: 400;">The implications of what we have discussed so far are clear: anything we can do to improve the gut microbiota - help and protect the good guys and prevent the evil ones from taking over - will have a significant positive effect on both our asthma symptoms and general health. </span></p>
                <h2><span style="font-weight: 400;">Let&rsquo;s start with food</span></h2>
                <p><span style="font-weight: 400;">First, I believe that the majority of diets can be easily be unified into simple common sense guidelines that we can apply to transform our eating routine into a healthy habit.</span></p>
                <p><span style="font-weight: 400;">Secondly, we are all different and process food in varying ways. The foremost way to determine what foods can cause us harm is to experiment. I will present an incredible simple framework to investigate this aspect.</span></p>
                <h3><span style="font-weight: 400;">Cut the refined sugar</span></h3>
                <p><span style="font-weight: 400;">Sugar is the food that nobody needs, but everybody craves for - the average American consuming 70 kg of it per year. &nbsp;</span></p>
                <p><span style="font-weight: 400;">And besides other health disorders that it causes, there is solid substantial evidence confirming that the sugar consumption is associated as well with asthma and allergies to an alarming extent.</span></p>
                <h3><span style="font-weight: 400;">Cut processed food</span></h3>
                <p><span style="font-weight: 400;">Glucose, salt, emulsifiers, organic solvents, microbial transglutaminase, nanoparticles and other, are extensively and increasingly used by the food industry and are damaging to our health.</span></p>
                <p><span style="font-weight: 400;">Research has also connected junk fast food with asthma and allergies.</span></p>
                <h3><span style="font-weight: 400;">Eat fresh and whole foods</span></h3>
                <p><span style="font-weight: 400;">Eat fresh organic food. Consider how long did it take from the moment they were produced until they ended up on your plate. Eat whole foods. They are unprocessed and are the closest to what we would get in the wild. Eat more fiber, as it is relevant to your gut microbes. And finally, adapt the fruit consumption to your lifestyle.</span></p>
                <h3><span style="font-weight: 400;">A note about gluten and dairy</span></h3>
                <p><span style="font-weight: 400;">Some people cannot tolerate gluten or dairy. Their ingestion could lead to inflammatory results and even asthma. The best way to rule them out is to experiment by temporarily removing them from your diet. </span></p>
                <h3><span style="font-weight: 400;">Take omega 3 supplements and reduce omega 6 intake</span></h3>
                <p><span style="font-weight: 400;">Some people cannot tolerate gluten or dairy. Their ingestion could lead to inflammatory results and even asthma. The best way to rule them out is to experiment by temporarily removing them from your diet.</span></p>
                <p><span style="font-weight: 400;">Preliminary research suggests that Omega 3 determine a positive impact on our gut microbiota in part, to increases in butyrate-producing bacteria.</span></p>
                <p><span style="font-weight: 400;">Chapter 1- Omega 3- explores this topic in detail.</span></p>
                <h2><span style="font-weight: 400;">The two-week carbohydrates intolerance test</span></h2>
                <p><span style="font-weight: 400;">Dr. Phil&rsquo;s Maffton&rsquo;s</span><span style="font-weight: 400;"> two-week carbohydrate test is a great simple instrument to learn what foods we are sensitive to. The brilliance of it is that, with some slight adaption to fit the asthma context, we can quickly rule out in two weeks the usual food suspects that might wreak havoc on our bodies.</span></p>
                <p><span style="font-weight: 400;">Click </span><span style="font-weight: 400;">here</span><span style="font-weight: 400;"> for details.</span></p>
                <h2><span style="font-weight: 400;">Use of antibiotics</span></h2>
                <p><span style="font-weight: 400;">Excessive and inappropriate use of these drugs may be causing serious long-term consequences. &nbsp;However, there are things you can do both during and after a course of antibiotics to minimize their damage and encourage re growth and diversification of your gut flora: combine</span><strong> probiotics</strong><span style="font-weight: 400;"> and </span><strong>prebiotics</strong><span style="font-weight: 400;">; they form a symbiotic combo that can do wonders for your gut microbiome.</span></p>
                <h2><span style="font-weight: 400;">Mode of delivery and breastfeeding</span></h2>
                <p><span style="font-weight: 400;">There is substantial evidence that infants born through elective or planned caesarean section are more likely to develop asthma than those born via normal spontaneous delivery. Also, breastfeeding contributes to the formation of a healthy immune system.</span></p>
                <h2><span style="font-weight: 400;">Other important elements:</span></h2>

                <ul class="general-list">
                    <li><strong><span style="font-weight: 400;">Get sun exposure and Vitamin D supplements</span></strong></li>
                    <li><strong><span style="font-weight: 400;">Manage your stress levels</span></strong></li>
                    <li><strong><span style="font-weight: 400;">Get good quality sleep</span></strong></li>
                </ul>

                <p><span style="font-weight: 400;">Chapters 1 and 4 are covering in detail the topics above.</span></p>

            </div>
        </div>
    </div>
</div>
