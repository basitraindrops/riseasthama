<div class="tab-pane" id="fullchapter">
    <div class="card ">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>The story behind RISE</h1>
                <h3>How I turned from a pale and depressed person into an ultra- marathon runner and adventure traveller</h3>
                <p>For 24 years I have struggled with allergies and asthma. After I developed RISE, my life turned 180 degrees.</p>
            </div>
            <img class="img-responsive" src="assets/img/thestory/1.png">
        <!-- image    -->


            <div class="content content-full-width ">
                <h2><span style="font-weight: 400;">The good days</span></h2>
                <p>I was born in 1986 in a small city in Eastern Romania. My childhood days were filled with joy and happiness. The adventure was everywhere: running barefoot, trekking, biking the hills. These small undertakings shaped my character and whetted my appetite for looking into the unknown. Those days, I had no doubt I that I was to become an explorer.</p>
                <h2><span style="font-weight: 400;">The beginning of asthma</span></h2>
                <p class="font_8">But the good days were about to end. Frequent colds were holding me inside my house. The city&rsquo;s industry was starting to recover, and the pollution got worse. I began to develop allergies.</p>
                <p class="font_8">I remember more and more doctor visits. I remember the Moldamin injections, and how much they hurt.</p>
                <p class="font_8">One day, my cold got severe. I started to cough; my lungs were burning, and my mother took me straight to the hospital. The doctor said something about asthma and about how I will always have to live with it. It meant nothing to me. I just wanted to get back on my bike.</p>
                <p class="font_8">But my mother looked worried.</p>
            </div>
        <!-- sectiune capitol     -->
        </div>
        <div class="scroll" data-type="2">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="assets/img/thestory/2.png" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/3.png" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/4.png" alt="Flower">
                    </div>
                    <div class="item">
                        <img src="assets/img/thestory/5.png" alt="Flower">
                    </div>



                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>



            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The struggle</span></h2>
                <p class="font_8">I immediately started the asthma treatment: inhalers, corticosteroids, antihistamines, and other expensive drugs. I tried them all.</p>
                <p class="font_8">Nevertheless, I was fighting a losing battle. My asthma was becoming more intense. My allergies were keeping me awake at night. I spent more and more time inside my house just wondering if things would ever get better.</p>
                <p class="font_8">But I was a kid. And kids are immune to depression.</p>
                <p class="font_8">That happens later.</p>
                <h2><span style="font-weight: 400;">The rock bottom</span></h2>
                <p class="font_8">My high school years showed the true impact of this disease. I was weak, skinny and almost always on leave.</p>
                <p class="font_8">I loved sports and was pretty good at them. Yet, I was forbidden to play any. &ldquo;They might trigger an attack,&rdquo; the doctors said.&nbsp;</p>
                <p class="font_8">I remember watching my classmates play from the sidelines, just wanting to take that ball and run with it. But this, again, could have "triggered an attack." So I didn't do it.</p>
                <p class="font_8">I never ceased seeing doctors. The same white coats, the same questions, the same tests, the same treatment, the same struggle.</p>
                <p class="font_8">It continued for years. I was turning from a pale teenager into a broken adult. But I was burning with desire to see the world. The mountains near my city looked gorgeous. I remember my time as a kid when I wanted to become an explorer. I wanted to go. But I couldn&rsquo;t. I felt that my life was over before it began.</p>
            </div>
        <!-- sectiune capitol     -->
        </div>
        <div class="scroll" data-type="3">
            <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel2" data-slide-to="1"></li>
                    <li data-target="#myCarousel2" data-slide-to="2"></li>


                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="assets/img/thestory/6.png" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/7.png" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/8.png" alt="Flower">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Hope</span></h2>
                <p class="font_8">One day, after my 24th birthday, I decided that I would not accept not being able to live how I wanted. I was determined to put up a fight and enrolled in a different kind of exploration.</p>
                <p class="font_8">Over the next couple of years, I plunged into a deep learning experience. I wanted to know how asthma works inside out. I wasn't satisfied by what the journals said: "Asthma is caused by inflamed airways." I wanted to know what caused the airways to inflame and how I can address these underlying issues.</p>
                <p class="font_8">I began speaking with doctors, researchers and asthma experts from all around the world. I also engaged with other asthma patients. I wanted to hear their stories, their struggle.</p>
                <p class="font_8">There was a lot a lot of despair. People were fed up. People like me, that just wanted to see the light, to add the color back to their lives.</p>
                <p class="font_8">But there was also hope around. Some patients seemed to have found their solution: a small change in their routine, a supplement or a vitamin they were taking, a change in approach and mentality.</p>
                <p class="font_8">I began experimenting on my own. The internet was both a blessing and a curse. There was so much information, so many opinions, and so many charlatans believing in snake oil and &ldquo;silver bullets.&rdquo;</p>
                <p class="font_8">Nevertheless, I tried every single remedy I could find: homeopathy, acupuncture, massage, water therapy, vitamins, supplements, meditation, diet, shiatsu, etc.</p>
                <p class="font_8">My approach was very systematic. I kept journals, analyzed the results, eliminated false leads and pursued the most promising ones. I consistently looked for the scientific basis to understand what was happening with my body in the wake of these treatments.</p>
                <p class="font_8">Shortly after, my efforts finally started to pay off: I saw the first positive results. That was all that I needed. A small win to motivate me to continue. Soon, the effect was exponential. My asthma was backing off. The allergies reduced in intensity, and I didn&rsquo;t get a cold in months. One day, I stopped using my inhaler. And I haven&rsquo;t used it ever since.</p>
                <p class="font_8">I concluded that from all these treatments I was on, four different steps &mdash; four items that I integrated into my daily routine &mdash; had the biggest impact on my condition. These four things are today the principal elements of the RISE method.</p>
                <h2><span style="font-weight: 400;">A new life</span></h2>
                <p class="font_8">The mountains became my playground. I picked up rock climbing. Shy at the beginning, I started climbing higher and more difficult routes until I found myself tackling steep 400-meter walls. I felt exuberant. My body was finally working. Every single piece of it was in harmony.</p>
                <p class="font_8">I also started running. I finished my first 21-kilometer mountain race, and by the end of it, I felt that I was a different person. Shortly after, I ran four marathons in four consecutive weeks. Ultra marathons came next.</p>
                <p class="font_8">Fueled by my latest achievements, my childhood dreams of becoming an explorer re-emerged. I organized my first charitable expedition from Europe to Africa. I crossed the Sahara Desert to reach out to Guinea Bissau where I helped build an IT lab in a local school.</p>
                <p class="font_8">From a work perspective, I ended up Asia working in online health-care. Somehow, health-care made sense.</p>
                <p class="font_8">Asia opened the door for new exciting adventures: trekking the Himalayas, living with the indigenous population in the Cambodian jungle, teaching children in a small village in Borneo- Malaysia, volunteering in central Vietnam.</p>
                <p class="font_8">Meanwhile, I forgot about asthma. I forgot I ever had it and all the suffering that came with it.</p>
            </div>
        <!-- sectiune capitol     -->
        </div>
        <div class="scroll" data-type="4">
            <div id="myCarousel3" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel3" data-slide-to="1"></li>
                    <li data-target="#myCarousel3" data-slide-to="2"></li>
                    <li data-target="#myCarousel3" data-slide-to="3"></li>
                    <li data-target="#myCarousel3" data-slide-to="4"></li>


                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="assets/img/thestory/9.png" alt="">
                    </div>
                    <div class="item">
                        <img src="assets/img/thestory/10.png" alt="">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/11.png" alt="">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/12.png" alt="">
                    </div>
                    <div class="item">
                        <img src="assets/img/thestory/13.png" alt="">
                    </div>

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel3" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel3" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Giving back</span></h2>
                <p class="font_8">In 2015, I traveled to Tagaytay, Philippines to run a 50-kilometer ultramarathon in the tropical forest. I got lost and ended up running 65.</p>
                <p class="font_8">And that&rsquo;s where I met Shawi. Her brother died a couple of years back from an asthma attack. She listened to my story and told me: "Andrei, what you have done is wonderful. Try to reach out to as many people as possible. There are so many patients suffering from asthma. You could help them." I will always be thankful to Shawi for her advice.&nbsp;</p>
                <img class="img-responsive" src="assets/img/thestory/shawi.png">
                <!-- image    -->
                <p class="font_8">In the next period, I have started to spread my story and my asthma method around the world. I spoke with hundreds of patients and helped them change their lives.</p>
                <p class="font_8">This is how RISE was created. I am not ashamed to admit that I was down and defeated by asthma. But I managed to RISE. And now, I wish other people could rise as well.</p>
            </div>  
        <!-- sectiune capitol     -->
      
            <div id="myCarousel4" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel3" data-slide-to="1"></li>
                    <li data-target="#myCarousel3" data-slide-to="2"></li>
                    <li data-target="#myCarousel3" data-slide-to="3"></li>
                    <li data-target="#myCarousel3" data-slide-to="4"></li>


                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="assets/img/thestory/14.png" alt="">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/15.png" alt="">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/16.png" alt="">
                    </div>

                    <div class="item">
                        <img src="assets/img/thestory/17.png" alt="">
                    </div>
                    <div class="item">
                        <img src="assets/img/thestory/18.png" alt="">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel4" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel4" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>






    </div>
    <!--  card     -->
</div>
<!--  tabe    -->
