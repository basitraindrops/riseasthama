<div class="tab-pane" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">

                <h1>Manage Stress</h1>
                <p><strong><span style="font-weight: 400;">&ldquo;</span><em><span style="font-weight: 400;">If you ask what is the single most important key to longevity, I would have to say it is avoiding worry, stress and tension. And if you didn&rsquo;t ask me, I&rsquo;d still have to say it.</span></em><span style="font-weight: 400;">&rdquo;</span> George Burns</strong></p>

            </div>
            <!--
            <div class="video-container">
                <iframe id="vzvd-9899340" name="vzvd-1152805" title="vzaar video player" type="text/html" frameborder="0" allowFullScreen allowTransparency="true" mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/9899340/player"></iframe>
            </div>-->

            <div class="video-player video-container">             
                <object id="vzvd-9899340"  height="auto" style="width: 100%;">
                    <param name="allowScriptAccess" value="always" /> 
                    <param name="allowFullScreen" value="true" /> 
                    <param name="wmode" value="transparent" />
                    <param name="flashvars" value="showplaybutton=true" />
                    <video  height="auto" style="width: 100%;" poster="//view.vzaar.com/9899340/image" preload="none" controls="controls" onclick="this.play();" id="vzvid" >                        
                            <source src="//view.vzaar.com/9899340/video">
                    </video>
                </object>
            </div>

            <div class="nuttshell">

                <div class="nutshell-content">
                    <h2>In a Nutshell</h2>
                    <p><span style="font-weight: 400;">For an extended period of time stress was beneficial for our species as it represented a highly complex problem-solving instrument</span></p>
                    <p><span style="font-weight: 400;">Changes that occurred in the Agricultural Revolution and then magnified during modern times transformed stress from a useful tool into a self-destruction weapon linked with numerous health disorders, as well as with asthma and allergies.</span></p>
                    <p><span style="font-weight: 400;">RISE stress management technique draws inspiration from the life of that our ancestors lived for more than two million years:</span></p>



                </div>

                <div class="do">
                    <div class="dodont-content">


                        <ul class="general-list">
                            <li>Exercise through natural movement</li>
                            <li>Foster human connections</li>
                            <li>Fix your Omega 3 to Omega 6 ratio</li>
                            <li>Get sun exposure and Vitamin D supplements</li>
                            <li>Eliminate gut pressure by eating the right food</li>
                            <li>Get enough sleep</li>
                            <li>Pursue small goals </li>
                            <li>Have a greater purpose</li>
                        </ul>
                    </div>
                </div>




            </div>
        </div>
        <div class="scroll" data-type="2">

            <div class="content content-full-width">
                <p><span style="font-weight: 400;">Thousands of pages can be written on stress and all the hardship that comes with it. One can debate for hours on its effects on human health. Physicians that often disagree on other topics, all share common ground when stress is brought up.</span></p>
                <p><span style="font-weight: 400;">Low mood, stress, anxiety, and depression are indeed terrible foes. For us, asthmatics, the struggle is even more grueling. Once stress takes over, our symptoms get worse. In turn, this exacerbates the problems, sparking a vicious downward cycle of illness that often proves difficult to break </span><a href="https://paperpile.com/c/rQl0Ss/4vv2"><span style="font-weight: 400;">[1]</span></a> <a href="https://paperpile.com/c/rQl0Ss/qBjc"><span style="font-weight: 400;">[2]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">There is hope. As strenuous as it might be, stress can and should be defeated.</span></p>
                <p><span style="font-weight: 400;">This will be a rather long chapter, but only because the topic is incredibly complex. We need to explore the depths of stress, see how it started, what it represents, where it went wrong, and what we can do to fix it once and for all. </span></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/1.png">
            <!-- image    -->



            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">The Key to Knowledge</span></h2>

                <p><span style="font-weight: 400;">Have you ever wondered why you feel pleasure when you eat?</span></p>
                <p><span style="font-weight: 400;">Why do you get those &ldquo;butterflies&rdquo; in your stomach when you meet the love of your life? Why that intense surge when you have sex?</span></p>
                <p><span style="font-weight: 400;">What about pain? Why do you hurt? Why do you fear, suffer or grieve?</span></p>
                <p><span style="font-weight: 400;">The answer relies deeply on your underlying biological purpose. The same one that you share with all the living beings on this planet: </span><strong>survival</strong><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">This is an essential basis of Charles Darwin&rsquo;s evolutionary natural selection mechanism </span><a href="https://paperpile.com/c/rQl0Ss/pqmE"><span style="font-weight: 400;">[3]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">We don&rsquo;t know why things are the way they are and probably never will as life began too far back in the past about 3.5 billion years ago. But natural selection is as valid as any other universal law, such as gravity or electromagnetism, and it is the key to understanding what is happening with us, stress- and asthma-wise.</span></p>

                <h2><span style="font-weight: 400;">The Survival Mechanism</span></h2>

                <p><span style="font-weight: 400;">Survival can be broken down into two main aspects:</span></p>
                <p><span style="font-weight: 400;">First, maintaining our physical, mental, and spiritual health.</span></p>
                <p><span style="font-weight: 400;">Second, reproduction and passing our genes forward to the next generations. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Through evolution, nature has given us a complex inner system (made of billions of chemical reactions) responsible for providing powerful sensations and emotions that come to us as rewards for performing survival-related activities </span><a href="https://paperpile.com/c/rQl0Ss/EPqE"><span style="font-weight: 400;">[4]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">When we eat, we absorb the nutrients required to fuel our vital organs; when we mate, our offspring ensure our DNA will survive; when we love, care and connect with people it&rsquo;s because a tribe has better chances of survival than an individual person.</span></p>
                <p><span style="font-weight: 400;">On the other hand, the same chemical reactions give us the feeling of pain to prevent us from damaging ourselves.</span></p>
                <p><span style="font-weight: 400;">It would be painful to throw ourselves off a cliff, eat rotten food, or burn our hands in a fire. It is as painful to lose someone. Even the thought of a dear one getting hurt could be devastating. Pain is indeed an excellent teacher. It keeps us alive, and it makes us care for one another. </span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">Evolution has given us a complex inner chemical system — made of hormones and neurotransmitters — that triggers pleasure or pain depending on the type of activity performed. The final purpose of this process is to encourage our biological survival. </span></em></strong></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/2.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="3">

            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">Stress — A Problem Solving Trait</span></h2>

                <p><span style="font-weight: 400;">Let&rsquo;s go back in time and imagine that you are an early Homo sapien trying to survive in dangerous surroundings.</span></p>
                <p><span style="font-weight: 400;">You are a happy person living joyfully with your tribe. However, most of the time you have to plan ahead. You need to be aware some plants might be poisonous. You have to keep an eye on predators and worry about competing tribes fighting for your territory. This particular amount of stress helps to keep you alive.</span></p>
                <p><span style="font-weight: 400;">One day, a lion threatens you. Your body reacts, and your excitement and anxiety take over. The brain and locomotion system get most of your energy. You need to solve this life-threatening problem by either running away or fighting. Since you are in excellent shape and your friends are backing you; you chose to fight. You win.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">These kinds of hurdles happen very often. If you are sturdy enough and your brain is sending the right stress signals, then you survive and make it back to your tribe in one piece.</span></p>
                <p><span style="font-weight: 400;">One day, one of your brothers gets killed. You grieve for him and understand how important he was to you. You want to avoid losing a dear one ever again.</span></p>
                <p><span style="font-weight: 400;">You start to think about what has happened and realize that the problem is indeed overwhelming. There are simply too many predators. The food is scarce, and aggressive competing tribes always challenge you. Your group needs a new location, but somewhere not too far away, since you have elders and pregnant women to care for.</span></p>
                <p><span style="font-weight: 400;">Usually, it is hard to concentrate on issues. Distractions are everywhere. A beautiful female approach with offerings of a good time. The food you gathered smells delicious, and your friends are meeting to discuss events.</span></p>
                <p><span style="font-weight: 400;">Yet, this time is different. Your mood is getting lower and you retreat into yourself. You are not welcoming social interaction, sex or even food. The isolation helps further analyze and dissect the problem. The more you think, the more you realize that each component is not as difficult as you initially thought. Everything becomes more manageable. If you could focus harder, you might find a way out of the problem and ensure you and your tribe&rsquo;s survival.</span></p>
                <p><span style="font-weight: 400;">You lose sleep that night, but the next day you have everything figured out. You know exactly what to do. You are in control.</span></p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/3.png">
            <!-- image    -->

            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">Losing Control</span></h2>

                <<p><em><span style="font-weight: 400;">Fast forward to 10,000 BC</span></em></p>
                    <p><span style="font-weight: 400;">You have finally settled. You are now smarter and have some of the answers as to how to make your life easier. You don&rsquo;t hunt anymore. You managed to domesticate wild plants and animals. Your tribe is growing fast.</span></p>
                    <p><span style="font-weight: 400;">However, there are more and more problems. No matter the actions you take, your animals often get sick and the crops die. You sometimes think your family might face starvation.</span></p>
                    <p><span style="font-weight: 400;">There is also a different kind of pressure. You have changed your diet by eating more grains, and you don&rsquo;t feel as healthy as you used to.</span></p>
                    <p><span style="font-weight: 400;">Your mood lowers and you go into solution mode. You stay like that for days hoping for answers to unveil, but they fail to do so.</span></p>
                    <p><span style="font-weight: 400;">There might be something. You turn your attention to spirits and gods and ask for their help. This makes you feel better, but your animals keep dying, and you keep stressing.</span></p>
                    <p><span style="font-weight: 400;">You feel you are losing control. </span></p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/4.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="4">

            <div class="content content-full-width">

                <h2><span style="font-weight: 400;">The Tool Becomes a Self Destruction Weapon</span></h2>

                <p><em><span style="font-weight: 400;">Nowadays</span></em><span style="font-weight: 400;">, your tribe is ruling the planet.</span></p>
                <p><span style="font-weight: 400;">Life is safe and comfortable. You don&rsquo;t have to worry anymore about food or predators. Your tribe is globally interconnected using this new technology your smart brain has created. You are blessed.</span></p>
                <p><span style="font-weight: 400;">However, in your deeper thoughts, you sense that something is wrong. There are so many requests that your tribe asks of you and so much pressure everywhere.</span></p>
                <p><span style="font-weight: 400;">People around you look and behave differently. Some of them are sad, angry and overweight. They are very often retreating into themselves trying to figure out their problems.</span></p>
                <p><span style="font-weight: 400;">Your body is no longer a perfectly working machine. It is barely functioning. You don&rsquo;t run, climb, swim and fight like you used to. </span></p>
                <p><span style="font-weight: 400;">You hardly see the sun anymore.</span></p>
                <p><span style="font-weight: 400;">You are confused and seek to connect with others. Your tribe is everywhere, and you see your friends using technology on a daily basis, but the emotional connection is weak, and you feel that they are not really there for you.</span></p>
                <p><span style="font-weight: 400;">Some chemicals in your brain are changing, becoming imbalanced. You isolate yourself more often while looking for solutions. Day after day you contemplate and eventually find it hard to stop.</span></p>
                <p><strong><span style="font-weight: 400;">You feel lost and depressed. </span><a href="https://paperpile.com/c/rQl0Ss/IZHx"><span style="font-weight: 400;">[5]</span></a> <a href="https://paperpile.com/c/rQl0Ss/vJdn"><span style="font-weight: 400;">[6]</span></a> <a href="https://paperpile.com/c/rQl0Ss/k2qA"><span style="font-weight: 400;">[7]</span></a> <a href="https://paperpile.com/c/rQl0Ss/LhG5"><span style="font-weight: 400;">[8]</span></a> <a href="https://paperpile.com/c/rQl0Ss/PkL6"><span style="font-weight: 400;">[9]</span></a> <a href="https://paperpile.com/c/rQl0Ss/soLq"><span style="font-weight: 400;">[10]</span></a></strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>

                <p><em><span style="font-weight: 400;">Stress started as a mechanism to help ancestral humans better focus on survival. People experienced short-term stress back then; life was relatively simple. They had a solid understanding of their surroundings and a good amount of general control.</span></em></p>
                <p><strong><em><span style="font-weight: 400;">From the Agricultural Revolution to modern times, the problems/dilemmas have been continually multiplying,</span></em> <em><span style="font-weight: 400;">and the level of control has gradually been fading. As a consequence, stress became excessive and a problem in itself. </span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/5.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="5">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The Science Behind Stress</span></h2>

                <p><span style="font-weight: 400;">Research suggests that low mood, anxiety, stress and depression started not as disorders where the brain was operating in a haphazard way or malfunctioning. Instead, they form an intricate, highly organized piece of machinery that performs specific functions </span><a href="https://paperpile.com/c/rQl0Ss/9E4f"><span style="font-weight: 400;">[11]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">The &ldquo;fight or flight&rdquo; stress response is the first one. Without it, we would be a like a dead gazelle torn apart by mighty lions.</span></p>
                <p><span style="font-weight: 400;">The second is the analytical problem-solving skill. Analysis requires a lot of uninterrupted thought. Low mood coordinates many changes in the body to help people analyze their problems without getting distracted. In a region of the brain known as the ventrolateral prefrontal cortex (VLPFC), neurons must fire continuously for people to avoid being distracted. This is a highly energy-demanding process that our body wants to avoid </span><a href="https://paperpile.com/c/rQl0Ss/4uBE"><span style="font-weight: 400;">[12]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">The desire for social isolation further helps a person with a low mood to avoid situations that would require thinking about other things. Similarly, the inability to derive pleasure from sex or other activities prevents distraction from focusing on the problem. Even the loss of appetite, often seen in stressed situations, could be viewed as distractions because chewing and other oral activity interfere with the brain&rsquo;s ability to process information </span><a href="https://paperpile.com/c/rQl0Ss/9E4f"><span style="font-weight: 400;">[11]</span></a><span style="font-weight: 400;">.</span></p>
                <p><strong><span style="font-weight: 400;">From a strictly social perspective, various studies have found people in depressed mood states are better at solving social dilemmas by thoroughly analyzing the costs and benefits of the different options that they may take </span><a href="https://paperpile.com/c/rQl0Ss/WDPU"><span style="font-weight: 400;">[13]</span></a> <a href="https://paperpile.com/c/rQl0Ss/jj9Z"><span style="font-weight: 400;">[14]</span></a><span style="font-weight: 400;">. It can also work the other way around. Low mood triggers empathy in friends and family or even strangers </span><a href="https://paperpile.com/c/rQl0Ss/M0Jm"><span style="font-weight: 400;">[15]</span></a><span style="font-weight: 400;">. Don&rsquo;t we all feel the urge to help a dear one when they feel sad and isolate themselves?</span></strong></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/6.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="6">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The Problem with too Many Problems</span></h2>

                <p><span style="font-weight: 400;">I am not saying that low mood and stress are not problems. As you have seen above, we have taken an incredibly useful instrument and turned it into a weapon we now use against ourselves </span><a href="https://paperpile.com/c/rQl0Ss/fqrd"><span style="font-weight: 400;">[16]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">In scientific terms, stress can be defined as an automatic physical response to any stimulus that requires you to adjust to change. Every real or perceived threat to your body triggers a cascade of hormones that produces physiological changes. Among these, we have acetylcholine (for memory), serotonin (that regulates sleep, appetite and mood), norepinephrine (for blood pressure), dopamine (essential to movement and motivation), glutamate, and many others.</span></p>
                <p><span style="font-weight: 400;">Our internal chemistry holds a perfect natural balance between these substances as part of our body's homeostasis. However, after prolonged periods of stress, the chemical equilibrium gets disrupted and health disorders appear </span><a href="https://paperpile.com/c/rQl0Ss/mmIo"><span style="font-weight: 400;">[17]</span></a> <a href="https://paperpile.com/c/rQl0Ss/PteO"><span style="font-weight: 400;">[18]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Imagine a car going uphill on a dangerous mountain road. Its engine takes in more and more fuel to overcome the slope, but it might eventually break down and crack.</span></p>
                <p><span style="font-weight: 400;">The stress response starts with a signal from the part of your brain known as the hypothalamus, the part that begins to produce cortisol. This substance makes the heart beat faster, the blood pressure rise and breathing to quicken and absorb extra oxygen. It is part of a process also involving</span> <span style="font-weight: 400;">the amygdala that coordinates thoughts, behaviors, emotional reactions, and involuntary responses. &nbsp;</span></p>
                <p><span style="font-weight: 400;">What we get out of it is sharpened senses, such as sight and hearing. In addition, it makes us more alert and analytical.</span></p>
                <p><span style="font-weight: 400;">Typically, a feedback loop allows the body to turn off this "fight-or-flight" defense when the threat passes.</span><strong> However, as a consequence of our modern lifestyle, the floodgates never close properly, and cortisol levels rise too often or simply stay high.</strong></p>
                <p><strong><span style="font-weight: 400;">One of the results is severe and life-threatening &mdash; depression </span><a href="https://paperpile.com/c/rQl0Ss/XKxs"><span style="font-weight: 400;">[19]</span></a><span style="font-weight: 400;">.</span></strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Our body performs a never ending intensive homeostasis to keep our internal brain chemistry in balance. However, after prolonged periods of stress, this chemical equilibrium gets disrupted, a fact that leads to multiple health disorders.</span></em></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/7.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="7">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Stress and Asthma</span></h2>

                <p><span style="font-weight: 400;">When stress starts interfering with your ability to live a normal life for an extended period, it wears and tears on your body.&nbsp;</span></p>
                <p><span style="font-weight: 400;">Some people experience digestive symptoms, while others may have headaches, sleeplessness, sadness, anger or irritability. People under chronic stress are prone to more frequent and severe viral infections, such as the flu or common cold. Moreover, stress may affect behaviors and factors that increase heart disease risk and even cancer. &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/n7bv"><span style="font-weight: 400;">[20]</span></a> <span style="font-weight: 400;"><a href="https://paperpile.com/c/rQl0Ss/jT96+9q8n+OBAL">[21&ndash;23]</a></span></p>
                <p><span style="font-weight: 400;">Stress has a substantial negative impact on allergies and asthma </span><a href="https://paperpile.com/c/rQl0Ss/1amq"><span style="font-weight: 400;">[24]</span></a> <a href="https://paperpile.com/c/rQl0Ss/LQrM"><span style="font-weight: 400;">[25]</span></a> <a href="https://paperpile.com/c/rQl0Ss/rebi"><span style="font-weight: 400;">[26]</span></a> <a href="https://paperpile.com/c/rQl0Ss/M32U"><span style="font-weight: 400;">[27]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Psychological stress operates by altering the magnitude of the airway inflammatory response that irritants, allergens, and infections bring about in people with asthma. Stress is not capable of modifying immune functions in a way that leads to asthmatic symptoms. Rather, it is viewed as a process that accentuates the airway inflammatory response to environmental triggers and, in doing so, increases the frequency, duration, and severity of a patient's symptoms </span><a href="https://paperpile.com/c/rQl0Ss/j1nL"><span style="font-weight: 400;">[28]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Simply put, it is disturbing the body&rsquo;s natural homeostasis by reducing the secretion of stomach acid or leading to &ldquo;leaky&rdquo; gut, for example </span><a href="https://paperpile.com/c/rQl0Ss/06Ya"><span style="font-weight: 400;">[29]</span></a> <a href="https://paperpile.com/c/rQl0Ss/A3UW+QE6V+qYNY"><span style="font-weight: 400;">[30&ndash;32]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">In the last part of the RISE method, we will discuss how stress is related &mdash; through the brain-gut axis &mdash; to the microbiota in our gut.</span></p>
                <p><span style="font-weight: 400;">Indirectly, stress can even lead to or exacerbate your allergies and asthma.</span></p>
                <p><span style="font-weight: 400;">In the second part of this chapter, we will explore what actions we can take to eliminate excess stress for good and prevent it from further damaging our body and causing diseases.</span></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Stress and asthma connect in very complex ways by involving the gut’s microbiota and accentuating the airway inflammatory.</span></em></p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/8.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="8">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Happiness Comes from our Ancestors</span></h2>

                <p><span style="font-weight: 400;">In a study of 2000 Kaluli aborigines from Papua New Guinea, only one marginal case of clinical depression was found </span><a href="https://paperpile.com/c/rQl0Ss/G2ll"><span style="font-weight: 400;">[33]</span></a><span style="font-weight: 400;">. Why? The Kaluli lifestyle is very similar to our hunter-gatherer ancestors. They go about naturally performing activities that keep them from getting depressed, activities stronger than any drug or medication.</span></p>
                <p><span style="font-weight: 400;">Strong evidence points out the key to a stress and asthma- free existence stands in that original hunter and gatherer way of life that lasted for nearly </span><span style="font-weight: 400;">two </span><span style="font-weight: 400;">million years before agriculture </span><a href="https://paperpile.com/c/rQl0Ss/Cedk"><span style="font-weight: 400;">[34]</span></a> <a href="https://paperpile.com/c/rQl0Ss/BywY"><span style="font-weight: 400;">[35]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">However, it would be almost impossible for us to go to that extreme. Even if our physical, mental and emotional architecture hasn't changed too much in the last 70,000 years, we are drastically different from a cultural and social perspective </span><a href="https://paperpile.com/c/rQl0Ss/vJdn"><span style="font-weight: 400;">[6]</span></a><span style="font-weight: 400;">. here is no question that we will continue to change further. We are industrious creatures with needs for efficiency. We will always push the limits of knowledge and technology for extra comfort and safety.</span></p>
                <p><span style="font-weight: 400;">However, we shouldn't ignore that we are also designed to live in nature. Our body is intrinsically crafted to operate in natural surroundings </span><a href="https://paperpile.com/c/rQl0Ss/JKAF"><span style="font-weight: 400;">[36]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Our intricacy goes beyond that. Psychologically, we are selfish and struggle for resources, pleasure, and prestige. However, we are also shaped by group selection to be hive creatures with needs of love and attachment &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/k2qA"><span style="font-weight: 400;">[7]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Defeating our health disorders implies understanding the incredible human complexity and taking actions to reach a balance between all of its elements. This stability is the core essence of RISE, and all of its steps are interlinked to restore it.</span></p>
                <p><span style="font-weight: 400;">Even if we cannot go back to living as our early ancestors did, the closer we get to that natural lifestyle, the more healthy benefits we will get.</span></p>
                <p><strong><span style="font-weight: 400;">However, you need no</span><span style="font-weight: 400;">t</span> <span style="font-weight: 400;">stress. What you are about to read is nothing extreme, but common sense, simple and fun.</span></strong></p>

            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/stress/9.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="9">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Before You Start - See a Doctor</span></h2>

                <p><span style="font-weight: 400;">If you suffer from excessive stress, have nervous breakdowns, feel depressed or are overly anxious, then I suggest you see a (good) psychiatrist.</span></p>
                <p><span style="font-weight: 400;">It is possible that your brain&rsquo;s chemical substances are imbalanced. Proper medication may be required to get an initial relief from your symptoms. From there, you can continue with other less invasive nocive treatments &mdash; like the RISE method proposes.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">I advise you to ignore the stigma and the rest of the myths attached to seeing a therapist. You are not &ldquo;crazy&rdquo; and accepting help is not a sign of weakness. Your health is above all this nonsense.</span></p>
                <p><span style="font-weight: 400;">The earlier you get help, the easier it is to get through the issue as there will be less time and strain involved.</span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>

                <p>Strong evidence suggests the key to a stress and asthma free existence stands in the original hunter and gatherer way of life which lasted for nearly 2 million years before agriculture. RISE stress management techniques draws its inspiration from this natural lifestyle. </p>

                <h2><span style="font-weight: 400;">RISE Stress Management Elements</span></h2>

                <p><span style="font-weight: 400;">You will notice how beautifully symmetric the RISE method is and how its elements are naturally interconnected. Here’s what we need to do:</span></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Exercise through natural movement</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Fos</span><span style="font-weight: 400;">ter </span><span style="font-weight: 400;">human connections </span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Fix your Omega 3 to Omega 6 ratio</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Get sun exposure and Vitamin D supplements</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Eliminate gut pressure by eating the right food</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Get enough sleep</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Pursue small goals</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Have a greater purpos</span></li>
                </ul>


                <h2><span style="font-weight: 400;">Exercise Through Natural Movement</span></h2>

                <p><span style="font-weight: 400;">Life is like a bicycle. If you want balance, you have to keep moving.</span></p>
                <p><span style="font-weight: 400;">Doctors have been telling us for years we need to get more exercise. There is an extensive list of benefits that accompany physical activity: lower blood pressure, boosted immune function, greater bone density and a reduced risk of diabetes, obesity and heart disease &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/wRdP+1LpL"><span style="font-weight: 400;">[37, 38]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">There are also multiple studies on the benefits of exercise and its connection to asthma and allergies (such as improved lung capacity and overall fitness) </span><a href="https://paperpile.com/c/rQl0Ss/ccmc+HSnQ+5vLg"><span style="font-weight: 400;">[39&ndash;41]</span></a> <a href="https://paperpile.com/c/rQl0Ss/KCs1"><span style="font-weight: 400;">[42]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">In addition, exercise is equally important to maintaining our mental health.</span></p>


                <h3><span style="font-weight: 400;">Why Does it Matter?</span></h3>

                <p><span style="font-weight: 400;">Exercise impacts the brain the same way antidepressant medication does. It increases the activity of important chemicals (like serotonin and dopamine). It also stimulates the brain's release of an essential growth hormone (BDNF), which in turn helps reverse the toxic brain-damaging effects of a low mood and depression </span><a href="https://paperpile.com/c/rQl0Ss/so7k"><span style="font-weight: 400;">[43]</span></a><span style="font-weight: 400;">. It sharpens memory and concentration and helps us think more clearly.</span></p>
                <p><span style="font-weight: 400;">Simply put, exercise is one of the best available medicines.</span></p>


                <h3><span style="font-weight: 400;">Why is it Difficult to Keep Exercising Long Term?</span></h3>

                <p><span style="font-weight: 400;">The term &ldquo;exercise&rdquo; is relatively new and came along with the modern lifestyle.</span></p>
                <p><span style="font-weight: 400;">A cheetah doesn't exercise but also never fails to reach speeds up to 100 km/h, and it does so by natural movement.</span></p>
                <p><span style="font-weight: 400;">Our ancestors also never worked out. They used to run tens of miles hunting, hauling water, fighting and scouting out campsites. All their physical effort always implied a final purpose or goal. Extra "working out" (like doing push ups) would not have been a sage choice regarding burning precious extra calories.</span></p>
                <p><strong><span style="font-weight: 400;">Our body remembers. When we jump on a treadmill our mind doesn&rsquo;t recognize the immediate natural purpose of running on it, and our body screams a big &ldquo;NO&rdquo;. That is why we pay a full month gym membership but end up quitting after 2-3 sessions &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/icnw"><span style="font-weight: 400;">[44]</span></a><span style="font-weight: 400;">.</span></strong></p>

                <h3><span style="font-weight: 400;">A Better Way</span></h3>

                <p><strong><em>Make it social, natural and purposeful.</em></strong>
                </p>

                <p><span style="font-weight: 400;">Have you noticed how time really does fly whenever you&rsquo;re caught in an enjoyable and meaningful activity, even if there is physical exercise involved?</span></p>
                <p><span style="font-weight: 400;">Let&rsquo;s start with the simplest and most natural of all exercises: walking or running.</span></p>
                <p><span style="font-weight: 400;">When I run, I usually do it with a friend. We immerse ourselves deep into the mountains, preferably somewhere sunny.We have a clear objective such as reaching a beautiful peak and to get back to play guitar at the camp fire. I cannot tell when 6 hours have passed, and, in the end, I feel exuberant and look forward to the next time.</span></p>
                <p><span style="font-weight: 400;">Ask me to run for an hour on a track field or treadmill, and after 45 minutes, I am already tired and eager for it to end.</span></p>
                <p><span style="font-weight: 400;">What I just described is the key to establishing a long-term exercise routine.</span></p>
                <p><em><span style="font-weight: 400;">Make it social, do it with your friends.</span></em><span style="font-weight: 400;"> Laugh, sing and open your heart. Spending time with others is highly absorbing, and it makes the workout pass more quickly </span><a href="https://paperpile.com/c/rQl0Ss/hCEm"><span style="font-weight: 400;">[45]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Related to the above</span><em><span style="font-weight: 400;">, play a team sport</span></em><span style="font-weight: 400;">. Sports are fun because your mind subconsciously associates playing with going hunting with your tribe (the ball being the prey) </span><a href="https://paperpile.com/c/rQl0Ss/WQ9M"><span style="font-weight: 400;">[47]</span></a><span style="font-weight: 400;">.</span>&nbsp;</p>
                <p><em><span style="font-weight: 400;">Do it in nature</span></em><span style="font-weight: 400;">: forests, the seaside, mountains, desert, even a park downtown is better than a gym </span><a href="https://paperpile.com/c/rQl0Ss/arvT"><span style="font-weight: 400;">[46]</span></a><span style="font-weight: 400;">. Absorb the sunlight. Winter and cold temperature shouldn't be an excuse.</span></p>
                <p><em><span style="font-weight: 400;">Opt for natural movement</span></em><span style="font-weight: 400;">: walking, running, hiking, climbing, swimming, cycling, rowing, hauling, etc. Your body recognizes natural movement and doesn't oppose it </span><a href="https://paperpile.com/c/rQl0Ss/icnw"><span style="font-weight: 400;">[44]</span></a><span style="font-weight: 400;">. &nbsp;&nbsp;</span></p>
                <p><em><span style="font-weight: 400;">Have a purpose.</span></em><span style="font-weight: 400;"> Run to a place that is dear to you. Swim to beat your last record, cycle to meet a loved one or haul to fix something in your yard </span><a href="https://paperpile.com/c/rQl0Ss/VXQo"><span style="font-weight: 400;">[48]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><strong><em><span style="font-weight: 400;">Start slow and comfortable</span></em><span style="font-weight: 400;">. Aim for at least 30 minutes every day and gradually increase. Also, age shouldn't be an excuse either. </span></strong></p>
            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/10.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="10">


            <div class="content content-full-width">
                <h3><span style="font-weight: 400;">Exercise Below the Anaerobic Threshold</span><strong><strong>&nbsp;</strong></strong>
                </h3>

                <p><span style="font-weight: 400;">When I picked up running, I used to give everything in the first 30 minutes. I was aggressive. My lungs were burning, and my heart rate usually went through the roof. However, the night after, my lungs often produced a heavy mucus. They were irritated by the sudden stress that I was applying.</span></p>
                <p><strong>The first thing that helped was taking 2 g of Vitamin C right before the effort. It helps reduce the hyperventilation that usually causes exercise-induced asthma </strong><a href="https://paperpile.com/c/rQl0Ss/axSO+yXE9"><span style="font-weight: 400;">[49, 50]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Secondly, it is imperative to exercise at a relatively low heart rate &mdash; or aerobic effort </span><a href="https://paperpile.com/c/rQl0Ss/7HVI+h9JJ"><span style="font-weight: 400;">[51, 52]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">To determine the adequate heart rate, I use <a href="https://philmaffetone.com/180-formula/" target="_blank">Dr. Phill Maffeton&rsquo;s 180 formula</a>. It is brilliantly simple, and it has helped thousands of athletes on their path to performance.</span></p>

                <ol class="general-list">
                    <li style="font-weight: 400;"><strong> </strong><span style="font-weight: 400;">Get a good heart rate monitor (the chest bands work the best);</span></li>
                    <li style="font-weight: 400;"><strong></strong><span style="font-weight: 400;">Subtract your age from 180;</span></li>
                    <li style="font-weight: 400;"><strong></strong><span style="font-weight: 400;">Subtract an additional 10 if you are recovering from major illnesses or 5 if you are simply out of shape; or</span></li>
                    <li style="font-weight: 400;"><strong></strong><span style="font-weight: 400;">Add 5 if you train consistently.</span></li>
                </ol>

                <p><span style="font-weight: 400;">The number that you get should be your maximum exercising heart rate.</span></p>
                <p><span style="font-weight: 400;">For example, I use </span><strong>180 - 30</strong><span style="font-weight: 400;"> (my age) + </span><strong>5</strong><span style="font-weight: 400;"> (I am already in a good physical shape) to get a </span><strong>155</strong><span style="font-weight: 400;"> BPM maximum heart rate.&nbsp;</span></p>
                <p><span style="font-weight: 400;">I can go for hours under or at this heart rate. However, if I get too competitive and speed up, I might start to hyperventilate and end up running slower than before increasing the pace.</span></p>
                <p><span style="font-weight: 400;">For more information, please check the original Phil's Maffetone <a href="https://philmaffetone.com/180-formula/" target="_blank">website</a>. </span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Exercise impacts the brain the same as antidepressant medication. There are also multiple studies on the benefits of exercise in connection with asthma and allergies. It is important to keep the effort aerobic, especially those suffering from exercise induced asthma.</span></em></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/11.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Get Human Connection </span></h2>

                <p><span style="font-weight: 400;">Hunter-gatherers almost never spent time alone. Even though the average tribe number was rather small (around 50 strong), their everyday activities were full of social occasions </span><a href="https://paperpile.com/c/rQl0Ss/fqlf"><span style="font-weight: 400;">[53]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Hunting, cooking, gossiping, eating, playing, foraging, sleeping, grooming &mdash; they were all carried out in the company of loved ones. Loneliness and social isolation were practically unknown.</span></p>
                <p><span style="font-weight: 400;">We are born to connect with one another. This urge is etched deeply into our genes. From the first moments of life, babies need social contact to help regulate their breathing, heart rate, alertness, etc. They are exquisitely attuned to the biological rhythms of their mothers and mimic these natural cadences to keep their bodies in sync </span><a href="https://paperpile.com/c/rQl0Ss/2e3c"><span style="font-weight: 400;">[54]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Ten years ago, I wasn't aware of all that. My struggle led me to pull away from everyone and gradually shutting down. I was hard to deal with and aggressive. My friends and family couldn&rsquo;t figure out what was happening, and I was becoming a source of pain and frustration for everyone.</span></p>
                <p><span style="font-weight: 400;">The initial release from my symptoms got me into rock-climbing, which represented my first social breakthrough. The climbing community was warm and welcoming. Our vertical passion was the original bond, but gradually we discovered many other things in common.</span></p>
                <p><span style="font-weight: 400;">We all had a great deal of love and respect for nature and everything it represents. &nbsp;We spent time outdoors trekking the mountains, climbing beautiful rock walls, enjoying the sun and the colors around us. We spent nights singing and telling stories by the fire (and yeah, we had a few beers with us as well).</span></p>
                <p><span style="font-weight: 400;">My heart started to open little by little. My mood was improving, and my stress and asthma symptoms faded with it. I welcomed old friends into my life, and I became closer with my family.</span></p>
                <p><span style="font-weight: 400;">All in all, I can say my friends are my blessing. They helped me turn into a better person.</span></p>


                <h3><span style="font-weight: 400;">Some Practical Advice</span></h3>

                <p><span style="font-weight: 400;">We are all different, complex social creatures, and a specific approach to relationships (besides common sense) is impossible to put together.</span></p>
                <p><span style="font-weight: 400;">However, there are a few general things we can do to improve our social life.</span></p>


                <h3><span style="font-weight: 400;">Fuel Your Passions
</span></h3>

                <p><span style="font-weight: 400;">Passions connect people. What is yours? Cooking, running, music, Zumba, reading, sports, traveling, yoga, motorcycling, astronomy, etc?</span></p>
                <p><strong><span style="font-weight: 400;">Join a club or community. Passions work in strange ways. You&rsquo;ll soon discover that you have more things in common with these people than you initially expected. They can become real friends. Watch a beautiful clip </span><span style="font-weight: 400;">here</span><span style="font-weight: 400;"> about how similar people really are.</span></strong></p>

                <h3><span style="font-weight: 400;">Disclose and Ask for Help</span></h3>

                <p><span style="font-weight: 400;">I know people that are resistant to let even their closest friends know about their grapple with stress and its effects.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">The cause of that is, again, the stigma and the risk of being viewed as crazy, weak or lazy, but our friends and family have a right to know what is happening with us, especially if we are dealing with a treacherous foe such as depression </span><a href="https://paperpile.com/c/rQl0Ss/RBdj"><span style="font-weight: 400;">[55]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Sincere disclosure about our strains is indispensable to maintaining or reestablishing the vigor of any healthy friendship.</span></p>


                <h3><span style="font-weight: 400;">Avoid Negativity</span></h3>

                <p><span style="font-weight: 400;">Spending time with friends is helpful in part because it&rsquo;s a forceful way to disrupt rumination, the process of dwelling on troubling thoughts.</span></p>
                <p><strong><span style="font-weight: 400;">However, it is imperative that the social interaction focuses on something other than the depressing elements themselves. Excess sharing of negative reflection can easily backfire and lead to a full-blown episode of rumination </span><a href="https://paperpile.com/c/rQl0Ss/6dht"><span style="font-weight: 400;">[56]</span></a><span style="font-weight: 400;">. Try to stay positive and occasionally add humor as a way of conveying your upsetting feelings.</span></strong></p>

                <h3><span style="font-weight: 400;">Give Back</span></h3>

                <p><span style="font-weight: 400;">In the early human times, a mother who loved and cared for her baby increased their chances of survival (therefore her DNA survival) in comparison to a mother who was cold or absent.</span></p>
                <p><span style="font-weight: 400;">In addition, aiding another member of the tribe would have triggered a quid pro quo response for when you would have required help. Lending a hand could indeed lead to your survival.</span></p>
                <p><span style="font-weight: 400;">As a consequence, the feeling of support, love and care has gradually been written deep inside our DNA </span><a href="https://paperpile.com/c/rQl0Ss/J6r3"><span style="font-weight: 400;">[57]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">A new paper by researchers at the University of Exeter Medical School, UK, reviewed 40 studies from the past 20 years on the link between volunteering and health </span><a href="https://paperpile.com/c/rQl0Ss/FTky"><span style="font-weight: 400;">[58]</span></a><span style="font-weight: 400;">. The results conclude volunteering is associated with lower depression, increased well-being, and a 22% reduction in the risk of dying.</span></p>
                <p><span style="font-weight: 400;">Nowadays, the number of volunteer possibilities is nearly infinite. Run an online search and pick one that suits you the best.</span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">To connect more with people start by fueling your passions &mdash; they work wonders bringing people together &mdash; and join a club or community. Also, try to give back as often as possible by finding volunteering activities. In addition, avoid negativity and disclose and open yourself to your friends and family.</span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/12.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="11">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Get Enough Sunlight and Vitamin D</span></h2>

                <p><span style="font-weight: 400;">Amongst other things, sunlight stimulates the brain&rsquo;s production of serotonin, the neurotransmitter with widespread effects on mood and behavior </span><a href="https://paperpile.com/c/rQl0Ss/ys2Y+f3sK"><span style="font-weight: 400;">[59,60]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;"><a href="/vitamind.php">Chapter 1 - Vitamin D</a><span style="font-weight: 400;"> - captures this topic in particular.</span></span>
                </p>

                <h2><span style="font-weight: 400;">Fix your Omega 3  to Omega 6 Ratio</span></h2>

                <p><span style="font-weight: 400;">The human brain is about 60% fat by dry weight. Fat molecules (sometimes called fatty acids) play a crucial role in the construction of brain cells and the insulation of nerve fibers </span><a href="https://paperpile.com/c/rQl0Ss/GPNv"><span style="font-weight: 400;">[61]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Clinical researchers have even started using Omega-3 supplements to treat depression, and the results, so far, have been highly encouraging </span><a href="https://paperpile.com/c/rQl0Ss/zUoW"><span style="font-weight: 400;">[62]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;"><a href="/vitamind.php">Chapter 1 - Omega 3 </a> explores how this important fatty acid is paramount for our mental and physical health. </span></p>


                <h2><span style="font-weight: 400;">Eliminate Gut Pressure</span></h2>

                <p><span style="font-weight: 400;">At any given moment, you have somewhere between 10 trillion and 100 trillion microorganisms inhabiting your gut; that&rsquo;s more microbes in your bowels than there are cells in your body </span><a href="https://paperpile.com/c/rQl0Ss/Jnxy"><span style="font-weight: 400;">[63]</span></a><span style="font-weight: 400;">. Science is suggesting these bacteria in the gut are crucial for the brain and mental health </span><a href="https://paperpile.com/c/rQl0Ss/gklb"><span style="font-weight: 400;">[64]</span></a><span style="font-weight: 400;">.</span></p>
                <p><a href="/eliminategutpressure.php"> Chapter 3 - Eliminate Gut Pressure</a> address how you can experiment with food and use Apple Cider Vinegar to beat stress and reduce your allergies and asthma symptoms.</p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/13.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Have Good and Proper Sleep</span></h2>

                <p><span style="font-weight: 400;">Sleep is indispensable for our health. In fact, it is only during sleep that the body gets to perform the restoration from the fine damage suffered by millions of cells over the course of each day.</span></p>
                <p><span style="font-weight: 400;">Because sleep is paramount to our wellness, it takes only a couple of nights of deprivation before negative effects start accumulating: memory and concentration wane, energy dims, judgment grows poor, reaction times get slower, coordination deteriorates, and the immune system becomes deficient </span><a href="https://paperpile.com/c/rQl0Ss/M1FS"><span style="font-weight: 400;">[65]</span></a> <a href="https://paperpile.com/c/rQl0Ss/RKWX"><span style="font-weight: 400;">[66]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Unfortunately, sleep disturbance and stress are strongly interlinked &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/QSPm"><span style="font-weight: 400;">[67]</span></a><span style="font-weight: 400;">. Not only is disordered sleep one of the tell tale results of stress, but it also plays an important role in sparking the illness.</span></p>
                <p><span style="font-weight: 400;">The implications are clear: anything we can do to improve the quantity and quality of our sleep can help combat low mood and stress, and render the disorder less likely to occur in the future.</span></p>
                <p><span style="font-weight: 400;">But, how much sleep do we really need?</span></p>
                <p><strong><span style="font-weight: 400;">The National Sleep Foundation released the results of a world-class study that took more than two years of research to complete </span><a href="https://paperpile.com/c/rQl0Ss/CIgF"><span style="font-weight: 400;">[68]</span></a><span style="font-weight: 400;">. It concluded the following:</span></strong></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><strong>Newborns: </strong><span style="font-weight: 400;">: 14-17 hours </span></li>
                    <li style="font-weight: 400;"><strong>Infants, Toddlers, Preschoolers: </strong><span style="font-weight: 400;">10-15 hours</span></li>
                    <li style="font-weight: 400;"><strong>Preschoolers, school age children: </strong><span style="font-weight: 400;">10-13 hours</span></li>
                    <li style="font-weight: 400;"><strong>Teenagers: :</strong><span style="font-weight: 400;"> 8-10 hours </span></li>
                    <li style="font-weight: 400;"><strong>Adults : </strong><span style="font-weight: 400;">&nbsp;7-9 hours</span></li>
                    <li style="font-weight: 400;"><strong>Older adults: </strong><span style="font-weight: 400;">7-8 hours </span></li>
                </ul>

                <p><span style="font-weight: 400;">The most common objection I hear from adults is &ldquo;</span><em><span style="font-weight: 400;">I cannot afford to spend so much time sleeping</span></em><span style="font-weight: 400;">.&rdquo; Again, this behavior is the product of our hard-pressed modern lifestyle.</span></p>
                <p><strong><span style="font-weight: 400;">The only answer I have is, if we are seriousl</span><span style="font-weight: 400;">y</span><span style="font-weight: 400;"> concerned about our health, then we should find the time for things that truly matter. At least give it a try; sleep 8 hours a night for a week, and see how it feels. I am sure you will be amazed by the results.</span></strong></p>

                <h3><span style="font-weight: 400;">How to Improve the Quality of Sleep</span></h3>

                <p><span style="font-weight: 400;">Should you follow the other related elements presented in the RISE method, you will see significant positive progress sleep-wise:</span></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Exercise through natural movement</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Vitamin D through sunshine or supplement</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Apple Cider Vinegar and reduced gut tension</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Omega 3 to Omega 6 ratio</span></li>
                </ul>

                <p><span style="font-weight: 400;">In addition:</span></p>

                <h3><span style="font-weight: 400;">Turn off Electronics in the Bedroom</span></h3>

                <p><strong><span style="font-weight: 400;">To put it simply, avoid bright screens within 1-2 hours of your bedtime. Robust scientific data is documenting the role of light in promoting wakefulness </span><a href="https://paperpile.com/c/rQl0Ss/D9Fo"><span style="font-weight: 400;">[69]</span></a><span style="font-weight: 400;">. The blue light emitted by screens on cell phones, computers, tablets, and televisions restrain the production of melatonin, the hormone that controls your sleep/wake cycle or circadian rhythm. Reducing melatonin makes it harder to fall asleep and stay asleep </span><a href="https://paperpile.com/c/rQl0Ss/zqD8"><span style="font-weight: 400;">[70]</span></a><span style="font-weight: 400;">. </span></strong></p>

                <h3><span style="font-weight: 400;">Adjust Temperature</span></h3>

                <p><span style="font-weight: 400;">The role of temperature has received increased attention after a study found sleep may be more tightly regulated by temperature than by light </span><a href="https://paperpile.com/c/rQl0Ss/rQoL"><span style="font-weight: 400;">[71]</span></a><span style="font-weight: 400;">. What&rsquo;s more, core body temperature, which tends to fluctuate by a few degrees over the course of the day, needs to drop to help initiate sleep.</span></p>
                <p><span style="font-weight: 400;">The nonprofit National Sleep Foundation typically recommends room temperatures for sleep between 60 and 67 degrees.</span></p>

                <h3><span style="font-weight: 400;">Avoid Alcohol and Caffeine </span></h3>

                <p><strong><span style="font-weight: 400;">Although it may be tempting to drink alcohol to relieve the stress and make it easier to fall asleep, you&rsquo;re more likely to sleep lighter and wake up in the middle of the night when the effects of the alcohol wear off. Caffeine can help keep you awake longer, but it can also make it harder for you to fall asleep if its effects haven&rsquo;t worn off by the time you are ready to go to bed. Therefore, it&rsquo;s best to consume caffeine only in the morning and not in the afternoon &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/23Ol"><span style="font-weight: 400;">[72]</span></a><span style="font-weight: 400;">. </span></strong></p>

                <h3><span style="font-weight: 400;">Avoid Large Meals and Beverages Late at Night </span></h3>
                <p><span style="font-weight: 400;">A light snack is okay, but a large meal can cause indigestion that interferes with sleep </span><a href="https://paperpile.com/c/rQl0Ss/H0PT"><span style="font-weight: 400;">[73]</span></a><span style="font-weight: 400;">. Also, drinking too many fluids at night can cause frequent awakenings to urinate.</span></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Sleep is indispensable for our health. Aim for a good quality 8 hours a day sleep.</span></em></p>

            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/14.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="12">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Have Small Goals </span></h2>

                <p><span style="font-weight: 400;">An active mind is a healthy mind.</span></p>
                <p><span style="font-weight: 400;">A study on more than 700 people with an average age of 80 were observed, and i</span><span style="font-weight: 400;">t was </span><span style="font-weight: 400;">concluded that those consistently engaged in cognitively stimulating activities </span><em><span style="font-weight: 400;">&mdash; </span></em><span style="font-weight: 400;">reading the newspaper, playing chess, seeing plays, reading and visiting the library </span><em><span style="font-weight: 400;">&mdash; </span></em><span style="font-weight: 400;">were 2.6 times less likely to develop dementia and Alzheimer's than those who did not engage in such activities &nbsp;</span><a href="https://paperpile.com/c/rQl0Ss/yHCi"><span style="font-weight: 400;">[74]</span></a><strong>.</strong></p>
                <p><span style="font-weight: 400;">Rumination is undoubtedly tempting for the brain. It promises to provide the goods, but it rarely delivers. The perks of an active mind are that it disrupts this strenuous process.</span></p>
                <p><span style="font-weight: 400;">The same way it is best to exercise our body in a meaningful way purpose, we should also train the mind by aiding ourselves with small goals.</span></p>
                <p><strong><span style="font-weight: 400;">Find pleasant activities and turn them into personal projects: research and become an expert in something, write a blog, learn a foreign language or play an instrument </span><a href="https://paperpile.com/c/rQl0Ss/KOnu"><span style="font-weight: 400;">[75]</span></a> <a href="https://paperpile.com/c/rQl0Ss/OAcT"><span style="font-weight: 400;">[76]</span></a> <a href="https://paperpile.com/c/rQl0Ss/yBtP"><span style="font-weight: 400;">[77]</span></a><span style="font-weight: 400;">. Any small progress can lead to substantial long-term benefits.</span></strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">Keep your mind active to disrupt rumination. </span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/15.png">
            <!-- image    -->


            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Have a Greater Purpose</span></h2>

                <p><span style="font-weight: 400;">Several polls over multiple time periods and cultures concluded &ldquo;having a purpose or meaning in life&rdquo; is important for 80% to 90% of the respondents, while money was chosen by around 16%.</span></p>
                <p><span style="font-weight: 400;">According to recent research, having purpose and meaning actually increases our health at a cellular level, providing us with a stronger immune response profile. Purpose and meaning are important for us both emotionally and physically.</span></p>
                <p><span style="font-weight: 400;">The question &ldquo;what is our higher purpose&rdquo; has always been puzzling </span><span style="font-weight: 400;">to </span><span style="font-weight: 400;">both scientist and philosophers.</span></p>
                <p><span style="font-weight: 400;">Nevertheless, I am going to try to add my own piece of knowledge </span><span style="font-weight: 400;">to </span><span style="font-weight: 400;">it. I believe the quest for a greater purpose and happiness emerges from knowledge and understanding.</span></p>
                <p><span style="font-weight: 400;">First, we have to grasp our baseline architecture. We are both selfish and social creatures. We thrive for pleasure and prestige, but we also need to connect, care and love. We were given a brain that always thrives for efficiency, comfort, and safety, but a body designed to perform simple and natural pursuits. Contentment means a wide awareness of this inner planning and harmonizing all of its components.</span></p>
                <p><span style="font-weight: 400;">Secondly, popular culture is right: we are all special, and this uniqueness is reflected in our personal distinctive DNA. We are born with different skills, traits, and preferences that will determine a large chunk of our future actions. At the moment of our birth, our genes assemble the draft of a book </span><a href="https://paperpile.com/c/rQl0Ss/Oj5r"><span style="font-weight: 400;">[78]</span></a><span style="font-weight: 400;">. We live and keep refining all its pages, hoping the result turns out to be a masterpiece.</span></p>
                <p><strong>It is exactly this exclusiveness that we need to master.</strong></p>
                <p><span style="font-weight: 400;">Finding your</span><em><span style="font-weight: 400;"> true self </span></em><span style="font-weight: 400;">is easier said than done. We grow up living in social matrixes and become the by-products of the teachings of our families, neighbors, and professors. We are being told the world is governed by meritocracy. We are sold specific &ldquo;dreams&rdquo; that are to provide us with glory and &ldquo;success&rdquo;. We are given certain expectations and pressure ourselves hard to turn them into reality.</span></p>
                <p><span style="font-weight: 400;">However, for most of us, the reality is fickle and our predictions very often fade. We blame ourselves and think of us as unworthy in a world where apparently the luminary thrives, and we get sad and depressed.</span></p>
                <p><span style="font-weight: 400;">So, how can we break the matrix, become free from our cultural strings and unveil the real, underlying us?</span></p>
                <p><span style="font-weight: 400;">These thoughts battled me for years, but eventually, my </span>true self started<span style="font-weight: 400;"> to unveil gradually.</span></p>
                <p><span style="font-weight: 400;">First, I began realizing that meritocracy is just another myth. There are so many variables involved (such as your country of birth, family, genetics, or even luck) that meritocracy is simply a superficial way of looking at things.</span></p>
                <p><span style="font-weight: 400;">Secondly, I disregarded everything my culture told about the &ldquo;right&rdquo; path to happiness.</span></p>
                <p><span style="font-weight: 400;">I looked deep into myself and followed my instincts. I began to travel and immersed myself in various different cultures. I wanted to explore how other people live and learn from it.</span></p>
                <p><span style="font-weight: 400;">To my surprise, I began, through comparison, to discover myself </span><em><span style="font-weight: 400;">&mdash;</span></em><span style="font-weight: 400;"> who I really am when stripped from all cultural strings.</span></p>
                <p><span style="font-weight: 400;">I discovered I live for experiences. I want to evolve and constantly become a better, wiser and healthier person. I thrive on emotions and my heart to beat in multiple diverse ways. Finally, I realized I want to help others, to leave a mark.</span></p>
                <p><span style="font-weight: 400;">&ldquo;Be ashamed to die, until you have scored some victory for humanity&rdquo;, </span><strong>Horace Mann.</strong></p>
                <p><span style="font-weight: 400;">What is your greater purpose?</span></p>


            </div>
            <!-- sectiune capitol     -->
            <img class="img-responsive" src="assets/img/stress/16.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="13">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">A Note Regarding Children</span></h2>

                <p><span style="font-weight: 400;">It doesn&rsquo;t make any evolutionary sense for children to get depressed. Their brains should function normally until the age where they are able to reproduce.</span></p>
                <p><span style="font-weight: 400;">However, it is not uncommon nowadays for children to be diagnosed with both depression and general anxiety. Maybe a mix of genetics combined with environmental factors triggers the disorder.</span></p>
                <p><span style="font-weight: 400;">In general, the stress management techniques we have discussed above should be valid for children: make sure they get enough physical exercise, they are cared for, interact and connect with others, they get plenty vitamin D, they spend time outdoors and they keep an active mind.</span></p>


                <h2><span style="font-weight: 400;">Conclusion</span></h2>

                <p><span style="font-weight: 400;">I never cease to wonder about the fascinating way nature has crafted our bodies and minds. Every organ, cell, and chemical interaction inside us play a determined role in the survival of our entire organism and, on a deeper philosophical perspective, in our pursuit for contempt and happiness.</span></p>
                <p><span style="font-weight: 400;">Stress itself plays a substantial role ensuring that our minds are fit and focused enough to pass through any challenge life might throw at us. But the level of pressure that comes with the modern lifestyle is sometimes excessive. We start to focus and ruminate unrestrainedly on issues that we lack control of. And that is when things start to go south.</span></p>
                <p><span style="font-weight: 400;">The solution is not in heavy medication (even if it might be required in specific cases), but in natural, simple, and common sense lifestyle changes.</span></p>
                <p><span style="font-weight: 400;">All in all, the life of inner peace is the easiest type of existence.</span></p>


            </div>
            <!-- sectiune capitol     -->



            <div class="content content-full-width">


                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                <a data-target="#stress-reference" href="#" data-toggle="modal">References (click to open)<b class="caret"></b></a>
                            </h1>
                        </div>
                        <div id="stress-reference" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">References</h4>
                                    </div>
                                    <div class="modal-body">
                                        <ol>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/4vv2"><span style="font-weight: 400;">Lee Y-C, Lee CT-C, Lai Y-R, Chen VC-H, Stewart R. Association of asthma and anxiety: A nationwide population-based study in Taiwan. J Affect Disord. 2016;189: 98&ndash;105. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jad.2015.09.040"><span style="font-weight: 400;">10.1016/j.jad.2015.09.040</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/qBjc"><span style="font-weight: 400;">Ciprandi G, Schiavetti I, Rindone E, Ricciardolo FLM. The impact of anxiety and depression on outpatients with asthma. Ann Allergy Asthma Immunol. 2015;115: 408&ndash;414. doi:</span></a><a href="http://dx.doi.org/10.1016/j.anai.2015.08.007"><span style="font-weight: 400;">10.1016/j.anai.2015.08.007</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/pqmE"><span style="font-weight: 400;">Darwin C, Bynum WF. The origin of species by means of natural selection: or, the preservation of favored races in the struggle for life. ias.ac.in; 2009; Available: </span></a><a href="http://www.ias.ac.in/describe/article/reso/014/02/0204-0208"><span style="font-weight: 400;">http://www.ias.ac.in/describe/article/reso/014/02/0204-0208</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/EPqE"><span style="font-weight: 400;">Rossano M. Evolutionary Psychology: The Science of Human Behavior and Evolution [Internet]. Wiley; 2002. Available: </span></a><a href="https://market.android.com/details?id=book-eHBRAAAAYAAJ"><span style="font-weight: 400;">https://market.android.com/details?id=book-eHBRAAAAYAAJ</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/IZHx"><span style="font-weight: 400;">Bard J. Principles of Evolution: Systems, Species, and the History of Life [Internet]. Garland Science; 2016. Available: </span></a><a href="https://market.android.com/details?id=book-kRUeDQAAQBAJ"><span style="font-weight: 400;">https://market.android.com/details?id=book-kRUeDQAAQBAJ</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/vJdn"><span style="font-weight: 400;">Harari YN. Sapiens: A brief history of humankind. Random House; 2014.</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/k2qA"><span style="font-weight: 400;">Haidt J. The happiness hypothesis: Finding modern truth in ancient wisdom [Internet]. Basic Books; 2006. Available: </span></a><a href="http://cdn4.libris.ro/userdocspdf/433/Teoria%20fericirii.pdf"><span style="font-weight: 400;">http://cdn4.libris.ro/userdocspdf/433/Teoria%20fericirii.pdf</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/LhG5"><span style="font-weight: 400;">Braithwaite J. Hunter-gatherer human nature and health system safety: an evolutionary cleft stick? Int J Qual Health Care. Oxford University Press; 2005;17: 541&ndash;545. doi:</span></a><a href="http://dx.doi.org/10.1093/intqhc/mzi060"><span style="font-weight: 400;">10.1093/intqhc/mzi060</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/PkL6"><span style="font-weight: 400;">Diamond J. The worst mistake in the history of the human race. Discover. 1987;8: 64&ndash;66. Available: </span></a><a href="http://isite.lps.org/cmorgan/web/documents/WorstMistake_000.pdf"><span style="font-weight: 400;">http://isite.lps.org/cmorgan/web/documents/WorstMistake_000.pdf</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/soLq"><span style="font-weight: 400;">Milton K. Hunter-gatherer diets&mdash;a different perspective. Am J Clin Nutr. 2000;71: 665&ndash;667. Available: </span></a><a href="http://ajcn.nutrition.org/content/71/3/665.short"><span style="font-weight: 400;">http://ajcn.nutrition.org/content/71/3/665.short</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/9E4f"><span style="font-weight: 400;">Andrews PW, Thomson JA Jr. The bright side of being blue: depression as an adaptation for analyzing complex problems. Psychol Rev. 2009;116: 620&ndash;654. doi:</span></a><a href="http://dx.doi.org/10.1037/a0016242"><span style="font-weight: 400;">10.1037/a0016242</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/4uBE"><span style="font-weight: 400;">Diehl MM, Romanski LM. Responses of prefrontal multisensory neurons to mismatching faces and vocalizations. J Neurosci. 2014;34: 11233&ndash;11243. doi:</span></a><a href="http://dx.doi.org/10.1523/JNEUROSCI.5168-13.2014"><span style="font-weight: 400;">10.1523/JNEUROSCI.5168-13.2014</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/WDPU"><span style="font-weight: 400;">Evolutionary Psychology | SAGE Journals [Internet]. [cited 11 Feb 2017]. Available: </span></a><a href="http://journals.sagepub.com/home/evp"><span style="font-weight: 400;">http://journals.sagepub.com/home/evp</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/jj9Z"><span style="font-weight: 400;">Barbic SP, Durisko Z, Andrews PW. Measuring the bright side of being blue: a new tool for assessing analytical rumination in depression. PLoS One. 2014;9: e112077. doi:</span></a><a href="http://dx.doi.org/10.1371/journal.pone.0112077"><span style="font-weight: 400;">10.1371/journal.pone.0112077</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/M0Jm"><span style="font-weight: 400;">Sheeber L, Hops H, Davis B. Family processes in adolescent depression. Clin Child Fam Psychol Rev. 2001;4: 19&ndash;35. doi:</span></a><a href="http://dx.doi.org/10.1023/A:1009524626436"><span style="font-weight: 400;">10.1023/A:1009524626436</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/fqrd"><span style="font-weight: 400;">Jung N, Wranke C, Hamburger K, Knauff M. How emotions affect logical reasoning: evidence from experiments with mood-manipulated participants, spider phobics, and people with exam anxiety. Front Psychol. 2014;5: 570. doi:</span></a><a href="http://dx.doi.org/10.3389/fpsyg.2014.00570"><span style="font-weight: 400;">10.3389/fpsyg.2014.00570</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/mmIo"><span style="font-weight: 400;">Slavich GM, Irwin MR. From stress to inflammation and major depressive disorder: a social signal transduction theory of depression. Psychol Bull. 2014;140: 774&ndash;815. doi:</span></a><a href="http://dx.doi.org/10.1037/a0035302"><span style="font-weight: 400;">10.1037/a0035302</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/PteO"><span style="font-weight: 400;">Snyder JS, Soumier A, Brewer M, Pickel J, Cameron HA. Adult hippocampal neurogenesis buffers stress responses and depressive behaviour. Nature. 2011;476: 458&ndash;461. doi:</span></a><a href="http://dx.doi.org/10.1038/nature10287"><span style="font-weight: 400;">10.1038/nature10287</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/XKxs"><span style="font-weight: 400;">Gall SL, Sanderson K, Smith KJ, Patton G, Dwyer T, Venn A. Bi-directional associations between healthy lifestyles and mood disorders in young adults: The Childhood Determinants of Adult Health Study. Psychol Med. 2016;46: 2535&ndash;2548. doi:</span></a><a href="http://dx.doi.org/10.1017/S0033291716000738"><span style="font-weight: 400;">10.1017/S0033291716000738</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/n7bv"><span style="font-weight: 400;">Dimsdale JE. Psychological stress and cardiovascular disease. J Am Coll Cardiol. 2008;51: 1237&ndash;1246. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jacc.2007.12.024"><span style="font-weight: 400;">10.1016/j.jacc.2007.12.024</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/jT96"><span style="font-weight: 400;">Tawakol A, Ishai A, Takx RAP, Figueroa AL, Ali A, Kaiser Y, et al. Relation between resting amygdalar activity and cardiovascular events: a longitudinal and cohort study. Lancet. Elsevier; 2017;0. doi:</span></a><a href="http://dx.doi.org/10.1016/S0140-6736(16)31714-7"><span style="font-weight: 400;">10.1016/S0140-6736(16)31714-7</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/9q8n"><span style="font-weight: 400;">Blanc-Lapierre A, Rousseau M-C, Weiss D, El-Zein M, Siemiatycki J, Parent M-&Eacute;. Lifetime report of perceived stress at work and cancer among men: A case-control study in Montreal, Canada. Prev Med. 2016;96: 28&ndash;35. doi:</span></a><a href="http://dx.doi.org/10.1016/j.ypmed.2016.12.004"><span style="font-weight: 400;">10.1016/j.ypmed.2016.12.004</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/OBAL"><span style="font-weight: 400;">Segerstrom SC, Miller GE. Psychological stress and the human immune system: a meta-analytic study of 30 years of inquiry. Psychol Bull. 2004;130: 601&ndash;630. doi:</span></a><a href="http://dx.doi.org/10.1037/0033-2909.130.4.601"><span style="font-weight: 400;">10.1037/0033-2909.130.4.601</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/1amq"><span style="font-weight: 400;">Wright RJ. Epidemiology of stress and asthma: from constricting communities and fragile families to epigenetics. Immunol Allergy Clin North Am. 2011;31: 19&ndash;39. doi:</span></a><a href="http://dx.doi.org/10.1016/j.iac.2010.09.011"><span style="font-weight: 400;">10.1016/j.iac.2010.09.011</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/LQrM"><span style="font-weight: 400;">Li HL, He XL, Liang BM, Zhang HP, Wang Y, Wang G. Anxiety but not depression symptoms are associated with greater perceived dyspnea in asthma during bronchoconstriction. Allergy Asthma Proc. 2015;36: 447&ndash;457. doi:</span></a><a href="http://dx.doi.org/10.2500/aap.2015.36.3897"><span style="font-weight: 400;">10.2500/aap.2015.36.3897</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/rebi"><span style="font-weight: 400;">McLeish AC, Luberto CM, O&rsquo;Bryan EM. Anxiety Sensitivity and Reactivity to Asthma-Like Sensations Among Young Adults With Asthma. Behav Modif. 2016;40: 164&ndash;177. doi:</span></a><a href="http://dx.doi.org/10.1177/0145445515607047"><span style="font-weight: 400;">10.1177/0145445515607047</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/M32U"><span style="font-weight: 400;">Han Y-Y, Forno E, Marsland AL, Miller GE, Celed&oacute;n JC. Depression, Asthma, and Bronchodilator Response in a Nationwide Study of US Adults. J Allergy Clin Immunol Pract. 2016;4: 68&ndash;73.e1. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jaip.2015.10.004"><span style="font-weight: 400;">10.1016/j.jaip.2015.10.004</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/j1nL"><span style="font-weight: 400;">Chen E, Miller GE. Stress and inflammation in exacerbations of asthma. Brain Behav Immun. 2007;21: 993&ndash;999. doi:</span></a><a href="http://dx.doi.org/10.1016/j.bbi.2007.03.009"><span style="font-weight: 400;">10.1016/j.bbi.2007.03.009</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/06Ya"><span style="font-weight: 400;">Schiavone S, Morgese MG, Mhillaj E, Bove M, De Giorgi A, Cantatore FP, et al. Chronic Psychosocial Stress Impairs Bone Homeostasis: A Study in the Social Isolation Reared Rat. Front Pharmacol. 2016;7: 152. doi:</span></a><a href="http://dx.doi.org/10.3389/fphar.2016.00152"><span style="font-weight: 400;">10.3389/fphar.2016.00152</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/A3UW"><span style="font-weight: 400;">Ciarletta P, Destrade M, Gower AL. On residual stresses and homeostasis: an elastic theory of functional adaptation in living matter. Sci Rep. 2016;6: 24390. doi:</span></a><a href="http://dx.doi.org/10.1038/srep24390"><span style="font-weight: 400;">10.1038/srep24390</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/QE6V"><span style="font-weight: 400;">Kelly JR, Kennedy PJ, Cryan JF, Dinan TG, Clarke G, Hyland NP. Breaking down the barriers: the gut microbiome, intestinal permeability and stress-related psychiatric disorders. Front Cell Neurosci. 2015;9: 392. doi:</span></a><a href="http://dx.doi.org/10.3389/fncel.2015.00392"><span style="font-weight: 400;">10.3389/fncel.2015.00392</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/qYNY"><span style="font-weight: 400;">Konturek PC, Brzozowski T, Konturek SJ. Stress and the gut: pathophysiology, clinical consequences, diagnostic approach and treatment options. J Physiol Pharmacol. 2011;62: 591&ndash;599. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22314561"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22314561</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/G2ll"><span style="font-weight: 400;">Herdt GH. Rituals of Manhood: Male Initiation in Papua New Guinea [Internet]. Transaction Publishers; 1997. Available: </span></a><a href="https://market.android.com/details?id=book-5N3qpwV7OlsC"><span style="font-weight: 400;">https://market.android.com/details?id=book-5N3qpwV7OlsC</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/Cedk"><span style="font-weight: 400;">Raichlen DA, Pontzer H, Harris JA. Physical activity patterns and biomarkers of cardiovascular disease risk in hunter‐gatherers. American Journal of. Wiley Online Library; 2016; Available: </span></a><a href="http://onlinelibrary.wiley.com/doi/10.1002/ajhb.22919/full"><span style="font-weight: 400;">http://onlinelibrary.wiley.com/doi/10.1002/ajhb.22919/full</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/BywY"><span style="font-weight: 400;">Pontzer H, Raichlen DA, Wood BM, Mabulla AZP, Racette SB, Marlowe FW. Hunter-Gatherer Energetics and Human Obesity. PLoS One. Public Library of Science; 2012;7: e40503. doi:</span></a><a href="http://dx.doi.org/10.1371/journal.pone.0040503"><span style="font-weight: 400;">10.1371/journal.pone.0040503</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/JKAF"><span style="font-weight: 400;">Gluckman P, Hanson M. Mismatch: Why our world no longer fits our bodies: Why our world no longer fits our bodies [Internet]. OUP Oxford; 2006. Available: </span></a><a href="https://books.google.com/books?hl=en&amp;lr=&amp;id=A3Gk0M-Y-rMC&amp;oi=fnd&amp;pg=PR11&amp;dq=Gluckman+P+Hanson+M+Mismatch+(2006)+Oxford+University+Press&amp;ots=qChbAdutvE&amp;sig=u--PWAmOV9PVzCIYun0SJfNbiRM"><span style="font-weight: 400;">https://books.google.com/books?hl=en&amp;lr=&amp;id=A3Gk0M-Y-rMC&amp;oi=fnd&amp;pg=PR11&amp;dq=Gluckman+P+Hanson+M+Mismatch+(2006)+Oxford+University+Press&amp;ots=qChbAdutvE&amp;sig=u--PWAmOV9PVzCIYun0SJfNbiRM</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/wRdP"><span style="font-weight: 400;">Warburton DER, Nicol CW, Bredin SSD. Health benefits of physical activity: the evidence. CMAJ. 2006;174: 801&ndash;809. doi:</span></a><a href="http://dx.doi.org/10.1503/cmaj.051351"><span style="font-weight: 400;">10.1503/cmaj.051351</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/1LpL"><span style="font-weight: 400;">Reiner M, Niermann C, Jekauc D, Woll A. Long-term health benefits of physical activity -- a systematic review of longitudinal studies. BMC Public Health. 2013;13: 813. doi:</span></a><a href="http://dx.doi.org/10.1186/1471-2458-13-813"><span style="font-weight: 400;">10.1186/1471-2458-13-813</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/ccmc"><span style="font-weight: 400;">Eichenberger PA, Diener SN, Kofmehl R, Spengler CM. Effects of exercise training on airway hyperreactivity in asthma: a systematic review and meta-analysis. Sports Med. 2013;43: 1157&ndash;1170. doi:</span></a><a href="http://dx.doi.org/10.1007/s40279-013-0077-2"><span style="font-weight: 400;">10.1007/s40279-013-0077-2</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/HSnQ"><span style="font-weight: 400;">Dogra S, Kuk JL, Baker J, Jamnik V. Exercise is associated with improved asthma control in adults. Eur Respir J. 2011;37: 318&ndash;323. doi:</span></a><a href="http://dx.doi.org/10.1183/09031936.00182209"><span style="font-weight: 400;">10.1183/09031936.00182209</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/5vLg"><span style="font-weight: 400;">Scott HA, Latham JR, Callister R, Pretto JJ, Baines K, Saltos N, et al. Acute exercise is associated with reduced exhaled nitric oxide in physically inactive adults with asthma. Ann Allergy Asthma Immunol. 2015;114: 470&ndash;479. doi:</span></a><a href="http://dx.doi.org/10.1016/j.anai.2015.04.002"><span style="font-weight: 400;">10.1016/j.anai.2015.04.002</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/KCs1"><span style="font-weight: 400;">Westergren T, Fegran L, Nilsen T, Haraldstad K, Kittang OB, Berntsen S. Active play exercise intervention in children with asthma: a PILOT STUDY. BMJ Open. British Medical Journal Publishing Group; 2016;6: e009721. doi:</span></a><a href="http://dx.doi.org/10.1136/bmjopen-2015-009721"><span style="font-weight: 400;">10.1136/bmjopen-2015-009721</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/so7k"><span style="font-weight: 400;">Gomez-Pinilla F, Hillman C. The influence of exercise on cognitive abilities. Compr Physiol. 2013;3: 403&ndash;428. doi:</span></a><a href="http://dx.doi.org/10.1002/cphy.c110063"><span style="font-weight: 400;">10.1002/cphy.c110063</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/icnw"><span style="font-weight: 400;">O&rsquo;Keefe JH, Vogel R, Lavie CJ, Cordain L. Exercise like a hunter-gatherer: a prescription for organic physical fitness. Progress in cardiovascular. Elsevier; 2011; Available: </span></a><a href="http://www.sciencedirect.com/science/article/pii/S0033062011000648"><span style="font-weight: 400;">http://www.sciencedirect.com/science/article/pii/S0033062011000648</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/hCEm"><span style="font-weight: 400;">National Research Council (US) Committee on Aging Frontiers in Social Psychology, Personality, and Adult Developmental Psychology, Carstensen LL, Hartel CR. Social Engagement and Cognition [Internet]. National Academies Press (US); 2006. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/books/NBK83766/"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/books/NBK83766/</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/arvT"><span style="font-weight: 400;">Bratman GN, Hamilton JP, Hahn KS, Daily GC, Gross JJ. Nature experience reduces rumination and subgenual prefrontal cortex activation. Proc Natl Acad Sci U S A. 2015;112: 8567&ndash;8572. doi:</span></a><a href="http://dx.doi.org/10.1073/pnas.1510459112"><span style="font-weight: 400;">10.1073/pnas.1510459112</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/WQ9M"><span style="font-weight: 400;">Amorosi M. Correlation between sport and depression. Psychiatr Danub. 2014;26 Suppl 1: 208&ndash;210. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/25413542"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/25413542</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/VXQo"><span style="font-weight: 400;">Duckworth AL, Peterson C, Matthews MD, Kelly DR. Grit: perseverance and passion for long-term goals. J Pers Soc Psychol. 2007;92: 1087&ndash;1101. doi:</span></a><a href="http://dx.doi.org/10.1037/0022-3514.92.6.1087"><span style="font-weight: 400;">10.1037/0022-3514.92.6.1087</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/axSO"><span style="font-weight: 400;">Milan SJ, Hart A, Wilkinson M. Vitamin C for asthma and exercise-induced bronchoconstriction. Cochrane Database Syst Rev. 2013; CD010391. doi:</span></a><a href="http://dx.doi.org/10.1002/14651858.CD010391.pub2"><span style="font-weight: 400;">10.1002/14651858.CD010391.pub2</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/yXE9"><span style="font-weight: 400;">Cohen HA, Neuman I, Nahum H. Blocking effect of vitamin C in exercise-induced asthma. Arch Pediatr Adolesc Med. 1997;151: 367&ndash;370. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/9111435"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/9111435</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/7HVI"><span style="font-weight: 400;">Mersy DJ. Health benefits of aerobic exercise. Postgrad Med. 1991;90: 103&ndash;7, 110&ndash;2. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/2062750"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/2062750</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/h9JJ"><span style="font-weight: 400;">Rendi M, Szabo A, Szab&oacute; T, Velenczei A, Kov&aacute;cs A. Acute psychological benefits of aerobic exercise: a field study into the effects of exercise characteristics. Psychol Health Med. 2008;13: 180&ndash;184. doi:</span></a><a href="http://dx.doi.org/10.1080/13548500701426729"><span style="font-weight: 400;">10.1080/13548500701426729</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/fqlf"><span style="font-weight: 400;">Hamilton MJ, Milne BT, Walker RS, Burger O, Brown JH. The complex structure of hunter-gatherer social networks. Proc Biol Sci. 2007;274: 2195&ndash;2202. doi:</span></a><a href="http://dx.doi.org/10.1098/rspb.2007.0564"><span style="font-weight: 400;">10.1098/rspb.2007.0564</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/2e3c"><span style="font-weight: 400;">Thomas KA, Burr RL, Spieker S, Lee J, Chen J. Mother-infant circadian rhythm: development of individual patterns and dyadic synchrony. Early Hum Dev. 2014;90: 885&ndash;890. doi:</span></a><a href="http://dx.doi.org/10.1016/j.earlhumdev.2014.09.005"><span style="font-weight: 400;">10.1016/j.earlhumdev.2014.09.005</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/RBdj"><span style="font-weight: 400;">Halter MJ. Stigma and help seeking related to depression: a study of nursing students. J Psychosoc Nurs Ment Health Serv. 2004;42: 42&ndash;51. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/14982108"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/14982108</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/6dht"><span style="font-weight: 400;">Rose AJ, Carlson W, Waller EM. Prospective associations of co-rumination with friendship and emotional adjustment: considering the socioemotional trade-offs of co-rumination. Dev Psychol. 2007;43: 1019&ndash;1031. doi:</span></a><a href="http://dx.doi.org/10.1037/0012-1649.43.4.1019"><span style="font-weight: 400;">10.1037/0012-1649.43.4.1019</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/J6r3"><span style="font-weight: 400;">Haidt J. The Righteous Mind: Why Good People Are Divided by Politics and Religion [Internet]. Knopf Doubleday Publishing Group; 2012. Available: </span></a><a href="https://market.android.com/details?id=book-ItuzJhbcpMIC"><span style="font-weight: 400;">https://market.android.com/details?id=book-ItuzJhbcpMIC</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/FTky"><span style="font-weight: 400;">Robinson D. University of Exeter. Available: </span></a><a href="https://www.exeter.ac.uk/news/featurednews/title_315358_en.html"><span style="font-weight: 400;">https://www.exeter.ac.uk/news/featurednews/title_315358_en.html</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/ys2Y"><span style="font-weight: 400;">Lambert GW, Reid C, Kaye DM, Jennings GL, Esler MD. Effect of sunlight and season on serotonin turnover in the brain. Lancet. 2002;360: 1840&ndash;1842. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/12480364"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/12480364</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/f3sK"><span style="font-weight: 400;">Young SN. How to increase serotonin in the human brain without drugs. J Psychiatry Neurosci. 2007;32: 394&ndash;399. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/18043762"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/18043762</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/GPNv"><span style="font-weight: 400;">Chang C-Y, Ke D-S, Chen J-Y. Essential fatty acids and human brain. Acta Neurol Taiwan. 2009;18: 231&ndash;241. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/20329590"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/20329590</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/zUoW"><span style="font-weight: 400;">Osher Y, Belmaker RH. Omega-3 fatty acids in depression: a review of three studies. CNS Neurosci Ther. 2009;15: 128&ndash;133. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/19499625"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/19499625</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/Jnxy"><span style="font-weight: 400;">Ursell LK, Metcalf JL, Parfrey LW, Knight R. Defining the human microbiome. Nutr Rev. 2012;70 Suppl 1: S38&ndash;44. doi:</span></a><a href="http://dx.doi.org/10.1111/j.1753-4887.2012.00493.x"><span style="font-weight: 400;">10.1111/j.1753-4887.2012.00493.x</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/gklb"><span style="font-weight: 400;">Quigley EMM. Gut bacteria in health and disease. Gastroenterol Hepatol . 2013;9: 560&ndash;569. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/24729765"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/24729765</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/M1FS"><span style="font-weight: 400;">Why Is Sleep Important? - NHLBI, NIH [Internet]. [cited 12 Feb 2017]. Available: </span></a><a href="https://www.nhlbi.nih.gov/health/health-topics/topics/sdd/why"><span style="font-weight: 400;">https://www.nhlbi.nih.gov/health/health-topics/topics/sdd/why</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/RKWX"><span style="font-weight: 400;">Alhola P, Polo-Kantola P. Sleep deprivation: Impact on cognitive performance. Neuropsychiatr Dis Treat. 2007;3: 553&ndash;567. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/19300585"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/19300585</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/QSPm"><span style="font-weight: 400;">Han KS, Kim L, Shim I. Stress and sleep disorder. Exp Neurobiol. 2012;21: 141&ndash;150. doi:</span></a><a href="http://dx.doi.org/10.5607/en.2012.21.4.141"><span style="font-weight: 400;">10.5607/en.2012.21.4.141</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/CIgF"><span style="font-weight: 400;">Hirshkowitz M, Whiton K, Albert SM, Alessi C, Bruni O, DonCarlos L, et al. National Sleep Foundation&rsquo;s sleep time duration recommendations: methodology and results summary. Sleep Health: Journal of the National Sleep Foundation. Elsevier; 2015;1: 40&ndash;43. doi:</span></a><a href="http://dx.doi.org/10.1016/j.sleh.2014.12.010"><span style="font-weight: 400;">10.1016/j.sleh.2014.12.010</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/D9Fo"><span style="font-weight: 400;">Duffy JF, Czeisler CA. Effect of Light on Human Circadian Physiology. Sleep Med Clin. 2009;4: 165&ndash;177. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jsmc.2009.01.004"><span style="font-weight: 400;">10.1016/j.jsmc.2009.01.004</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/zqD8"><span style="font-weight: 400;">Holzman DC. What&rsquo;s in a color? The unique human health effect of blue light. Environ Health Perspect. 2010;118: A22&ndash;7. doi:</span></a><a href="http://dx.doi.org/10.1289/ehp.118-a22"><span style="font-weight: 400;">10.1289/ehp.118-a22</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/rQoL"><span style="font-weight: 400;">Benloucif S, Guico MJ, Reid KJ. Stability of melatonin and temperature as circadian phase markers and their relation to sleep times in humans. Journal of biological. journals.sagepub.com; 2005; Available: </span></a><a href="http://journals.sagepub.com/doi/abs/10.1177/0748730404273983"><span style="font-weight: 400;">http://journals.sagepub.com/doi/abs/10.1177/0748730404273983</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/23Ol"><span style="font-weight: 400;">Březinov&aacute; V. Effect of caffeine on sleep: EEG study in late middle age people. Br J Clin Pharmacol. 1974;1: 203&ndash;208. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22454948"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22454948</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/H0PT"><span style="font-weight: 400;">Kinsey AW, Ormsbee MJ. The health impact of nighttime eating: old and new perspectives. Nutrients. 2015;7: 2648&ndash;2662. doi:</span></a><a href="http://dx.doi.org/10.3390/nu7042648"><span style="font-weight: 400;">10.3390/nu7042648</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/yHCi"><span style="font-weight: 400;">Bennett DA, Schneider JA, Buchman AS, Barnes LL, Boyle PA, Wilson RS. Overview and findings from the rush Memory and Aging Project. Curr Alzheimer Res. 2012;9: 646&ndash;663. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22471867"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22471867</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/KOnu"><span style="font-weight: 400;">Gortner E-M, Rude SS, Pennebaker JW. Benefits of expressive writing in lowering rumination and depressive symptoms. Behav Ther. 2006;37: 292&ndash;303. doi:</span></a><a href="http://dx.doi.org/10.1016/j.beth.2006.01.004"><span style="font-weight: 400;">10.1016/j.beth.2006.01.004</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/OAcT"><span style="font-weight: 400;">Graf MC, Gaudiano BA, Geller PA. Written emotional disclosure: a controlled study of the benefits of expressive writing homework in outpatient psychotherapy. Psychother Res. 2008;18: 389&ndash;399. doi:</span></a><a href="http://dx.doi.org/10.1080/10503300701691664"><span style="font-weight: 400;">10.1080/10503300701691664</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/yBtP"><span style="font-weight: 400;">Hayes AM, Beevers CG, Feldman GC, Laurenceau J-P, Perlman C. Avoidance and processing as predictors of symptom change and positive growth in an integrative therapy for depression. Int J Behav Med. 2005;12: 111&ndash;122. doi:</span></a><a href="http://dx.doi.org/10.1207/s15327558ijbm1202_9"><span style="font-weight: 400;">10.1207/s15327558ijbm1202_9</span></a></li>
                                            <li><a href="http://paperpile.com/b/rQl0Ss/Oj5r"><span style="font-weight: 400;">Ott J. Jonathan Haidt, The Happiness Hypothesis; Putting Ancient Wisdom to the Test of Modern Science. J Happiness Stud. Kluwer Academic Publishers; 2007;8: 299&ndash;301. doi:</span></a><a href="http://dx.doi.org/10.1007/s10902-007-9049-2"><span style="font-weight: 400;">10.1007/s10902-007-9049-2</span></a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <!-- <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">

                                <ol>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/4vv2"><span style="font-weight: 400;">Lee Y-C, Lee CT-C, Lai Y-R, Chen VC-H, Stewart R. Association of asthma and anxiety: A nationwide population-based study in Taiwan. J Affect Disord. 2016;189: 98&ndash;105. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jad.2015.09.040"><span style="font-weight: 400;">10.1016/j.jad.2015.09.040</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/qBjc"><span style="font-weight: 400;">Ciprandi G, Schiavetti I, Rindone E, Ricciardolo FLM. The impact of anxiety and depression on outpatients with asthma. Ann Allergy Asthma Immunol. 2015;115: 408&ndash;414. doi:</span></a><a href="http://dx.doi.org/10.1016/j.anai.2015.08.007"><span style="font-weight: 400;">10.1016/j.anai.2015.08.007</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/pqmE"><span style="font-weight: 400;">Darwin C, Bynum WF. The origin of species by means of natural selection: or, the preservation of favored races in the struggle for life. ias.ac.in; 2009; Available: </span></a><a href="http://www.ias.ac.in/describe/article/reso/014/02/0204-0208"><span style="font-weight: 400;">http://www.ias.ac.in/describe/article/reso/014/02/0204-0208</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/EPqE"><span style="font-weight: 400;">Rossano M. Evolutionary Psychology: The Science of Human Behavior and Evolution [Internet]. Wiley; 2002. Available: </span></a><a href="https://market.android.com/details?id=book-eHBRAAAAYAAJ"><span style="font-weight: 400;">https://market.android.com/details?id=book-eHBRAAAAYAAJ</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/IZHx"><span style="font-weight: 400;">Bard J. Principles of Evolution: Systems, Species, and the History of Life [Internet]. Garland Science; 2016. Available: </span></a><a href="https://market.android.com/details?id=book-kRUeDQAAQBAJ"><span style="font-weight: 400;">https://market.android.com/details?id=book-kRUeDQAAQBAJ</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/vJdn"><span style="font-weight: 400;">Harari YN. Sapiens: A brief history of humankind. Random House; 2014.</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/k2qA"><span style="font-weight: 400;">Haidt J. The happiness hypothesis: Finding modern truth in ancient wisdom [Internet]. Basic Books; 2006. Available: </span></a><a href="http://cdn4.libris.ro/userdocspdf/433/Teoria%20fericirii.pdf"><span style="font-weight: 400;">http://cdn4.libris.ro/userdocspdf/433/Teoria%20fericirii.pdf</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/LhG5"><span style="font-weight: 400;">Braithwaite J. Hunter-gatherer human nature and health system safety: an evolutionary cleft stick? Int J Qual Health Care. Oxford University Press; 2005;17: 541&ndash;545. doi:</span></a><a href="http://dx.doi.org/10.1093/intqhc/mzi060"><span style="font-weight: 400;">10.1093/intqhc/mzi060</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/PkL6"><span style="font-weight: 400;">Diamond J. The worst mistake in the history of the human race. Discover. 1987;8: 64&ndash;66. Available: </span></a><a href="http://isite.lps.org/cmorgan/web/documents/WorstMistake_000.pdf"><span style="font-weight: 400;">http://isite.lps.org/cmorgan/web/documents/WorstMistake_000.pdf</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/soLq"><span style="font-weight: 400;">Milton K. Hunter-gatherer diets&mdash;a different perspective. Am J Clin Nutr. 2000;71: 665&ndash;667. Available: </span></a><a href="http://ajcn.nutrition.org/content/71/3/665.short"><span style="font-weight: 400;">http://ajcn.nutrition.org/content/71/3/665.short</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/9E4f"><span style="font-weight: 400;">Andrews PW, Thomson JA Jr. The bright side of being blue: depression as an adaptation for analyzing complex problems. Psychol Rev. 2009;116: 620&ndash;654. doi:</span></a><a href="http://dx.doi.org/10.1037/a0016242"><span style="font-weight: 400;">10.1037/a0016242</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/4uBE"><span style="font-weight: 400;">Diehl MM, Romanski LM. Responses of prefrontal multisensory neurons to mismatching faces and vocalizations. J Neurosci. 2014;34: 11233&ndash;11243. doi:</span></a><a href="http://dx.doi.org/10.1523/JNEUROSCI.5168-13.2014"><span style="font-weight: 400;">10.1523/JNEUROSCI.5168-13.2014</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/WDPU"><span style="font-weight: 400;">Evolutionary Psychology | SAGE Journals [Internet]. [cited 11 Feb 2017]. Available: </span></a><a href="http://journals.sagepub.com/home/evp"><span style="font-weight: 400;">http://journals.sagepub.com/home/evp</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/jj9Z"><span style="font-weight: 400;">Barbic SP, Durisko Z, Andrews PW. Measuring the bright side of being blue: a new tool for assessing analytical rumination in depression. PLoS One. 2014;9: e112077. doi:</span></a><a href="http://dx.doi.org/10.1371/journal.pone.0112077"><span style="font-weight: 400;">10.1371/journal.pone.0112077</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/M0Jm"><span style="font-weight: 400;">Sheeber L, Hops H, Davis B. Family processes in adolescent depression. Clin Child Fam Psychol Rev. 2001;4: 19&ndash;35. doi:</span></a><a href="http://dx.doi.org/10.1023/A:1009524626436"><span style="font-weight: 400;">10.1023/A:1009524626436</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/fqrd"><span style="font-weight: 400;">Jung N, Wranke C, Hamburger K, Knauff M. How emotions affect logical reasoning: evidence from experiments with mood-manipulated participants, spider phobics, and people with exam anxiety. Front Psychol. 2014;5: 570. doi:</span></a><a href="http://dx.doi.org/10.3389/fpsyg.2014.00570"><span style="font-weight: 400;">10.3389/fpsyg.2014.00570</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/mmIo"><span style="font-weight: 400;">Slavich GM, Irwin MR. From stress to inflammation and major depressive disorder: a social signal transduction theory of depression. Psychol Bull. 2014;140: 774&ndash;815. doi:</span></a><a href="http://dx.doi.org/10.1037/a0035302"><span style="font-weight: 400;">10.1037/a0035302</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/PteO"><span style="font-weight: 400;">Snyder JS, Soumier A, Brewer M, Pickel J, Cameron HA. Adult hippocampal neurogenesis buffers stress responses and depressive behaviour. Nature. 2011;476: 458&ndash;461. doi:</span></a><a href="http://dx.doi.org/10.1038/nature10287"><span style="font-weight: 400;">10.1038/nature10287</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/XKxs"><span style="font-weight: 400;">Gall SL, Sanderson K, Smith KJ, Patton G, Dwyer T, Venn A. Bi-directional associations between healthy lifestyles and mood disorders in young adults: The Childhood Determinants of Adult Health Study. Psychol Med. 2016;46: 2535&ndash;2548. doi:</span></a><a href="http://dx.doi.org/10.1017/S0033291716000738"><span style="font-weight: 400;">10.1017/S0033291716000738</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/n7bv"><span style="font-weight: 400;">Dimsdale JE. Psychological stress and cardiovascular disease. J Am Coll Cardiol. 2008;51: 1237&ndash;1246. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jacc.2007.12.024"><span style="font-weight: 400;">10.1016/j.jacc.2007.12.024</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/jT96"><span style="font-weight: 400;">Tawakol A, Ishai A, Takx RAP, Figueroa AL, Ali A, Kaiser Y, et al. Relation between resting amygdalar activity and cardiovascular events: a longitudinal and cohort study. Lancet. Elsevier; 2017;0. doi:</span></a><a href="http://dx.doi.org/10.1016/S0140-6736(16)31714-7"><span style="font-weight: 400;">10.1016/S0140-6736(16)31714-7</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/9q8n"><span style="font-weight: 400;">Blanc-Lapierre A, Rousseau M-C, Weiss D, El-Zein M, Siemiatycki J, Parent M-&Eacute;. Lifetime report of perceived stress at work and cancer among men: A case-control study in Montreal, Canada. Prev Med. 2016;96: 28&ndash;35. doi:</span></a><a href="http://dx.doi.org/10.1016/j.ypmed.2016.12.004"><span style="font-weight: 400;">10.1016/j.ypmed.2016.12.004</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/OBAL"><span style="font-weight: 400;">Segerstrom SC, Miller GE. Psychological stress and the human immune system: a meta-analytic study of 30 years of inquiry. Psychol Bull. 2004;130: 601&ndash;630. doi:</span></a><a href="http://dx.doi.org/10.1037/0033-2909.130.4.601"><span style="font-weight: 400;">10.1037/0033-2909.130.4.601</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/1amq"><span style="font-weight: 400;">Wright RJ. Epidemiology of stress and asthma: from constricting communities and fragile families to epigenetics. Immunol Allergy Clin North Am. 2011;31: 19&ndash;39. doi:</span></a><a href="http://dx.doi.org/10.1016/j.iac.2010.09.011"><span style="font-weight: 400;">10.1016/j.iac.2010.09.011</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/LQrM"><span style="font-weight: 400;">Li HL, He XL, Liang BM, Zhang HP, Wang Y, Wang G. Anxiety but not depression symptoms are associated with greater perceived dyspnea in asthma during bronchoconstriction. Allergy Asthma Proc. 2015;36: 447&ndash;457. doi:</span></a><a href="http://dx.doi.org/10.2500/aap.2015.36.3897"><span style="font-weight: 400;">10.2500/aap.2015.36.3897</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/rebi"><span style="font-weight: 400;">McLeish AC, Luberto CM, O&rsquo;Bryan EM. Anxiety Sensitivity and Reactivity to Asthma-Like Sensations Among Young Adults With Asthma. Behav Modif. 2016;40: 164&ndash;177. doi:</span></a><a href="http://dx.doi.org/10.1177/0145445515607047"><span style="font-weight: 400;">10.1177/0145445515607047</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/M32U"><span style="font-weight: 400;">Han Y-Y, Forno E, Marsland AL, Miller GE, Celed&oacute;n JC. Depression, Asthma, and Bronchodilator Response in a Nationwide Study of US Adults. J Allergy Clin Immunol Pract. 2016;4: 68&ndash;73.e1. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jaip.2015.10.004"><span style="font-weight: 400;">10.1016/j.jaip.2015.10.004</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/j1nL"><span style="font-weight: 400;">Chen E, Miller GE. Stress and inflammation in exacerbations of asthma. Brain Behav Immun. 2007;21: 993&ndash;999. doi:</span></a><a href="http://dx.doi.org/10.1016/j.bbi.2007.03.009"><span style="font-weight: 400;">10.1016/j.bbi.2007.03.009</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/06Ya"><span style="font-weight: 400;">Schiavone S, Morgese MG, Mhillaj E, Bove M, De Giorgi A, Cantatore FP, et al. Chronic Psychosocial Stress Impairs Bone Homeostasis: A Study in the Social Isolation Reared Rat. Front Pharmacol. 2016;7: 152. doi:</span></a><a href="http://dx.doi.org/10.3389/fphar.2016.00152"><span style="font-weight: 400;">10.3389/fphar.2016.00152</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/A3UW"><span style="font-weight: 400;">Ciarletta P, Destrade M, Gower AL. On residual stresses and homeostasis: an elastic theory of functional adaptation in living matter. Sci Rep. 2016;6: 24390. doi:</span></a><a href="http://dx.doi.org/10.1038/srep24390"><span style="font-weight: 400;">10.1038/srep24390</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/QE6V"><span style="font-weight: 400;">Kelly JR, Kennedy PJ, Cryan JF, Dinan TG, Clarke G, Hyland NP. Breaking down the barriers: the gut microbiome, intestinal permeability and stress-related psychiatric disorders. Front Cell Neurosci. 2015;9: 392. doi:</span></a><a href="http://dx.doi.org/10.3389/fncel.2015.00392"><span style="font-weight: 400;">10.3389/fncel.2015.00392</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/qYNY"><span style="font-weight: 400;">Konturek PC, Brzozowski T, Konturek SJ. Stress and the gut: pathophysiology, clinical consequences, diagnostic approach and treatment options. J Physiol Pharmacol. 2011;62: 591&ndash;599. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22314561"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22314561</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/G2ll"><span style="font-weight: 400;">Herdt GH. Rituals of Manhood: Male Initiation in Papua New Guinea [Internet]. Transaction Publishers; 1997. Available: </span></a><a href="https://market.android.com/details?id=book-5N3qpwV7OlsC"><span style="font-weight: 400;">https://market.android.com/details?id=book-5N3qpwV7OlsC</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/Cedk"><span style="font-weight: 400;">Raichlen DA, Pontzer H, Harris JA. Physical activity patterns and biomarkers of cardiovascular disease risk in hunter‐gatherers. American Journal of. Wiley Online Library; 2016; Available: </span></a><a href="http://onlinelibrary.wiley.com/doi/10.1002/ajhb.22919/full"><span style="font-weight: 400;">http://onlinelibrary.wiley.com/doi/10.1002/ajhb.22919/full</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/BywY"><span style="font-weight: 400;">Pontzer H, Raichlen DA, Wood BM, Mabulla AZP, Racette SB, Marlowe FW. Hunter-Gatherer Energetics and Human Obesity. PLoS One. Public Library of Science; 2012;7: e40503. doi:</span></a><a href="http://dx.doi.org/10.1371/journal.pone.0040503"><span style="font-weight: 400;">10.1371/journal.pone.0040503</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/JKAF"><span style="font-weight: 400;">Gluckman P, Hanson M. Mismatch: Why our world no longer fits our bodies: Why our world no longer fits our bodies [Internet]. OUP Oxford; 2006. Available: </span></a><a href="https://books.google.com/books?hl=en&amp;lr=&amp;id=A3Gk0M-Y-rMC&amp;oi=fnd&amp;pg=PR11&amp;dq=Gluckman+P+Hanson+M+Mismatch+(2006)+Oxford+University+Press&amp;ots=qChbAdutvE&amp;sig=u--PWAmOV9PVzCIYun0SJfNbiRM"><span style="font-weight: 400;">https://books.google.com/books?hl=en&amp;lr=&amp;id=A3Gk0M-Y-rMC&amp;oi=fnd&amp;pg=PR11&amp;dq=Gluckman+P+Hanson+M+Mismatch+(2006)+Oxford+University+Press&amp;ots=qChbAdutvE&amp;sig=u--PWAmOV9PVzCIYun0SJfNbiRM</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/wRdP"><span style="font-weight: 400;">Warburton DER, Nicol CW, Bredin SSD. Health benefits of physical activity: the evidence. CMAJ. 2006;174: 801&ndash;809. doi:</span></a><a href="http://dx.doi.org/10.1503/cmaj.051351"><span style="font-weight: 400;">10.1503/cmaj.051351</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/1LpL"><span style="font-weight: 400;">Reiner M, Niermann C, Jekauc D, Woll A. Long-term health benefits of physical activity -- a systematic review of longitudinal studies. BMC Public Health. 2013;13: 813. doi:</span></a><a href="http://dx.doi.org/10.1186/1471-2458-13-813"><span style="font-weight: 400;">10.1186/1471-2458-13-813</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/ccmc"><span style="font-weight: 400;">Eichenberger PA, Diener SN, Kofmehl R, Spengler CM. Effects of exercise training on airway hyperreactivity in asthma: a systematic review and meta-analysis. Sports Med. 2013;43: 1157&ndash;1170. doi:</span></a><a href="http://dx.doi.org/10.1007/s40279-013-0077-2"><span style="font-weight: 400;">10.1007/s40279-013-0077-2</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/HSnQ"><span style="font-weight: 400;">Dogra S, Kuk JL, Baker J, Jamnik V. Exercise is associated with improved asthma control in adults. Eur Respir J. 2011;37: 318&ndash;323. doi:</span></a><a href="http://dx.doi.org/10.1183/09031936.00182209"><span style="font-weight: 400;">10.1183/09031936.00182209</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/5vLg"><span style="font-weight: 400;">Scott HA, Latham JR, Callister R, Pretto JJ, Baines K, Saltos N, et al. Acute exercise is associated with reduced exhaled nitric oxide in physically inactive adults with asthma. Ann Allergy Asthma Immunol. 2015;114: 470&ndash;479. doi:</span></a><a href="http://dx.doi.org/10.1016/j.anai.2015.04.002"><span style="font-weight: 400;">10.1016/j.anai.2015.04.002</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/KCs1"><span style="font-weight: 400;">Westergren T, Fegran L, Nilsen T, Haraldstad K, Kittang OB, Berntsen S. Active play exercise intervention in children with asthma: a PILOT STUDY. BMJ Open. British Medical Journal Publishing Group; 2016;6: e009721. doi:</span></a><a href="http://dx.doi.org/10.1136/bmjopen-2015-009721"><span style="font-weight: 400;">10.1136/bmjopen-2015-009721</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/so7k"><span style="font-weight: 400;">Gomez-Pinilla F, Hillman C. The influence of exercise on cognitive abilities. Compr Physiol. 2013;3: 403&ndash;428. doi:</span></a><a href="http://dx.doi.org/10.1002/cphy.c110063"><span style="font-weight: 400;">10.1002/cphy.c110063</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/icnw"><span style="font-weight: 400;">O&rsquo;Keefe JH, Vogel R, Lavie CJ, Cordain L. Exercise like a hunter-gatherer: a prescription for organic physical fitness. Progress in cardiovascular. Elsevier; 2011; Available: </span></a><a href="http://www.sciencedirect.com/science/article/pii/S0033062011000648"><span style="font-weight: 400;">http://www.sciencedirect.com/science/article/pii/S0033062011000648</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/hCEm"><span style="font-weight: 400;">National Research Council (US) Committee on Aging Frontiers in Social Psychology, Personality, and Adult Developmental Psychology, Carstensen LL, Hartel CR. Social Engagement and Cognition [Internet]. National Academies Press (US); 2006. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/books/NBK83766/"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/books/NBK83766/</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/arvT"><span style="font-weight: 400;">Bratman GN, Hamilton JP, Hahn KS, Daily GC, Gross JJ. Nature experience reduces rumination and subgenual prefrontal cortex activation. Proc Natl Acad Sci U S A. 2015;112: 8567&ndash;8572. doi:</span></a><a href="http://dx.doi.org/10.1073/pnas.1510459112"><span style="font-weight: 400;">10.1073/pnas.1510459112</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/WQ9M"><span style="font-weight: 400;">Amorosi M. Correlation between sport and depression. Psychiatr Danub. 2014;26 Suppl 1: 208&ndash;210. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/25413542"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/25413542</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/VXQo"><span style="font-weight: 400;">Duckworth AL, Peterson C, Matthews MD, Kelly DR. Grit: perseverance and passion for long-term goals. J Pers Soc Psychol. 2007;92: 1087&ndash;1101. doi:</span></a><a href="http://dx.doi.org/10.1037/0022-3514.92.6.1087"><span style="font-weight: 400;">10.1037/0022-3514.92.6.1087</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/axSO"><span style="font-weight: 400;">Milan SJ, Hart A, Wilkinson M. Vitamin C for asthma and exercise-induced bronchoconstriction. Cochrane Database Syst Rev. 2013; CD010391. doi:</span></a><a href="http://dx.doi.org/10.1002/14651858.CD010391.pub2"><span style="font-weight: 400;">10.1002/14651858.CD010391.pub2</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/yXE9"><span style="font-weight: 400;">Cohen HA, Neuman I, Nahum H. Blocking effect of vitamin C in exercise-induced asthma. Arch Pediatr Adolesc Med. 1997;151: 367&ndash;370. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/9111435"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/9111435</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/7HVI"><span style="font-weight: 400;">Mersy DJ. Health benefits of aerobic exercise. Postgrad Med. 1991;90: 103&ndash;7, 110&ndash;2. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/2062750"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/2062750</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/h9JJ"><span style="font-weight: 400;">Rendi M, Szabo A, Szab&oacute; T, Velenczei A, Kov&aacute;cs A. Acute psychological benefits of aerobic exercise: a field study into the effects of exercise characteristics. Psychol Health Med. 2008;13: 180&ndash;184. doi:</span></a><a href="http://dx.doi.org/10.1080/13548500701426729"><span style="font-weight: 400;">10.1080/13548500701426729</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/fqlf"><span style="font-weight: 400;">Hamilton MJ, Milne BT, Walker RS, Burger O, Brown JH. The complex structure of hunter-gatherer social networks. Proc Biol Sci. 2007;274: 2195&ndash;2202. doi:</span></a><a href="http://dx.doi.org/10.1098/rspb.2007.0564"><span style="font-weight: 400;">10.1098/rspb.2007.0564</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/2e3c"><span style="font-weight: 400;">Thomas KA, Burr RL, Spieker S, Lee J, Chen J. Mother-infant circadian rhythm: development of individual patterns and dyadic synchrony. Early Hum Dev. 2014;90: 885&ndash;890. doi:</span></a><a href="http://dx.doi.org/10.1016/j.earlhumdev.2014.09.005"><span style="font-weight: 400;">10.1016/j.earlhumdev.2014.09.005</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/RBdj"><span style="font-weight: 400;">Halter MJ. Stigma and help seeking related to depression: a study of nursing students. J Psychosoc Nurs Ment Health Serv. 2004;42: 42&ndash;51. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/14982108"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/14982108</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/6dht"><span style="font-weight: 400;">Rose AJ, Carlson W, Waller EM. Prospective associations of co-rumination with friendship and emotional adjustment: considering the socioemotional trade-offs of co-rumination. Dev Psychol. 2007;43: 1019&ndash;1031. doi:</span></a><a href="http://dx.doi.org/10.1037/0012-1649.43.4.1019"><span style="font-weight: 400;">10.1037/0012-1649.43.4.1019</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/J6r3"><span style="font-weight: 400;">Haidt J. The Righteous Mind: Why Good People Are Divided by Politics and Religion [Internet]. Knopf Doubleday Publishing Group; 2012. Available: </span></a><a href="https://market.android.com/details?id=book-ItuzJhbcpMIC"><span style="font-weight: 400;">https://market.android.com/details?id=book-ItuzJhbcpMIC</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/FTky"><span style="font-weight: 400;">Robinson D. University of Exeter. Available: </span></a><a href="https://www.exeter.ac.uk/news/featurednews/title_315358_en.html"><span style="font-weight: 400;">https://www.exeter.ac.uk/news/featurednews/title_315358_en.html</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/ys2Y"><span style="font-weight: 400;">Lambert GW, Reid C, Kaye DM, Jennings GL, Esler MD. Effect of sunlight and season on serotonin turnover in the brain. Lancet. 2002;360: 1840&ndash;1842. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/12480364"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/12480364</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/f3sK"><span style="font-weight: 400;">Young SN. How to increase serotonin in the human brain without drugs. J Psychiatry Neurosci. 2007;32: 394&ndash;399. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/18043762"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/18043762</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/GPNv"><span style="font-weight: 400;">Chang C-Y, Ke D-S, Chen J-Y. Essential fatty acids and human brain. Acta Neurol Taiwan. 2009;18: 231&ndash;241. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/20329590"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/20329590</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/zUoW"><span style="font-weight: 400;">Osher Y, Belmaker RH. Omega-3 fatty acids in depression: a review of three studies. CNS Neurosci Ther. 2009;15: 128&ndash;133. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/19499625"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/19499625</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/Jnxy"><span style="font-weight: 400;">Ursell LK, Metcalf JL, Parfrey LW, Knight R. Defining the human microbiome. Nutr Rev. 2012;70 Suppl 1: S38&ndash;44. doi:</span></a><a href="http://dx.doi.org/10.1111/j.1753-4887.2012.00493.x"><span style="font-weight: 400;">10.1111/j.1753-4887.2012.00493.x</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/gklb"><span style="font-weight: 400;">Quigley EMM. Gut bacteria in health and disease. Gastroenterol Hepatol . 2013;9: 560&ndash;569. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/24729765"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/24729765</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/M1FS"><span style="font-weight: 400;">Why Is Sleep Important? - NHLBI, NIH [Internet]. [cited 12 Feb 2017]. Available: </span></a><a href="https://www.nhlbi.nih.gov/health/health-topics/topics/sdd/why"><span style="font-weight: 400;">https://www.nhlbi.nih.gov/health/health-topics/topics/sdd/why</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/RKWX"><span style="font-weight: 400;">Alhola P, Polo-Kantola P. Sleep deprivation: Impact on cognitive performance. Neuropsychiatr Dis Treat. 2007;3: 553&ndash;567. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/19300585"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/19300585</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/QSPm"><span style="font-weight: 400;">Han KS, Kim L, Shim I. Stress and sleep disorder. Exp Neurobiol. 2012;21: 141&ndash;150. doi:</span></a><a href="http://dx.doi.org/10.5607/en.2012.21.4.141"><span style="font-weight: 400;">10.5607/en.2012.21.4.141</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/CIgF"><span style="font-weight: 400;">Hirshkowitz M, Whiton K, Albert SM, Alessi C, Bruni O, DonCarlos L, et al. National Sleep Foundation&rsquo;s sleep time duration recommendations: methodology and results summary. Sleep Health: Journal of the National Sleep Foundation. Elsevier; 2015;1: 40&ndash;43. doi:</span></a><a href="http://dx.doi.org/10.1016/j.sleh.2014.12.010"><span style="font-weight: 400;">10.1016/j.sleh.2014.12.010</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/D9Fo"><span style="font-weight: 400;">Duffy JF, Czeisler CA. Effect of Light on Human Circadian Physiology. Sleep Med Clin. 2009;4: 165&ndash;177. doi:</span></a><a href="http://dx.doi.org/10.1016/j.jsmc.2009.01.004"><span style="font-weight: 400;">10.1016/j.jsmc.2009.01.004</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/zqD8"><span style="font-weight: 400;">Holzman DC. What&rsquo;s in a color? The unique human health effect of blue light. Environ Health Perspect. 2010;118: A22&ndash;7. doi:</span></a><a href="http://dx.doi.org/10.1289/ehp.118-a22"><span style="font-weight: 400;">10.1289/ehp.118-a22</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/rQoL"><span style="font-weight: 400;">Benloucif S, Guico MJ, Reid KJ. Stability of melatonin and temperature as circadian phase markers and their relation to sleep times in humans. Journal of biological. journals.sagepub.com; 2005; Available: </span></a><a href="http://journals.sagepub.com/doi/abs/10.1177/0748730404273983"><span style="font-weight: 400;">http://journals.sagepub.com/doi/abs/10.1177/0748730404273983</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/23Ol"><span style="font-weight: 400;">Březinov&aacute; V. Effect of caffeine on sleep: EEG study in late middle age people. Br J Clin Pharmacol. 1974;1: 203&ndash;208. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22454948"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22454948</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/H0PT"><span style="font-weight: 400;">Kinsey AW, Ormsbee MJ. The health impact of nighttime eating: old and new perspectives. Nutrients. 2015;7: 2648&ndash;2662. doi:</span></a><a href="http://dx.doi.org/10.3390/nu7042648"><span style="font-weight: 400;">10.3390/nu7042648</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/yHCi"><span style="font-weight: 400;">Bennett DA, Schneider JA, Buchman AS, Barnes LL, Boyle PA, Wilson RS. Overview and findings from the rush Memory and Aging Project. Curr Alzheimer Res. 2012;9: 646&ndash;663. Available: </span></a><a href="https://www.ncbi.nlm.nih.gov/pubmed/22471867"><span style="font-weight: 400;">https://www.ncbi.nlm.nih.gov/pubmed/22471867</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/KOnu"><span style="font-weight: 400;">Gortner E-M, Rude SS, Pennebaker JW. Benefits of expressive writing in lowering rumination and depressive symptoms. Behav Ther. 2006;37: 292&ndash;303. doi:</span></a><a href="http://dx.doi.org/10.1016/j.beth.2006.01.004"><span style="font-weight: 400;">10.1016/j.beth.2006.01.004</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/OAcT"><span style="font-weight: 400;">Graf MC, Gaudiano BA, Geller PA. Written emotional disclosure: a controlled study of the benefits of expressive writing homework in outpatient psychotherapy. Psychother Res. 2008;18: 389&ndash;399. doi:</span></a><a href="http://dx.doi.org/10.1080/10503300701691664"><span style="font-weight: 400;">10.1080/10503300701691664</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/yBtP"><span style="font-weight: 400;">Hayes AM, Beevers CG, Feldman GC, Laurenceau J-P, Perlman C. Avoidance and processing as predictors of symptom change and positive growth in an integrative therapy for depression. Int J Behav Med. 2005;12: 111&ndash;122. doi:</span></a><a href="http://dx.doi.org/10.1207/s15327558ijbm1202_9"><span style="font-weight: 400;">10.1207/s15327558ijbm1202_9</span></a></li>
                                    <li><a href="http://paperpile.com/b/rQl0Ss/Oj5r"><span style="font-weight: 400;">Ott J. Jonathan Haidt, The Happiness Hypothesis; Putting Ancient Wisdom to the Test of Modern Science. J Happiness Stud. Kluwer Academic Publishers; 2007;8: 299&ndash;301. doi:</span></a><a href="http://dx.doi.org/10.1007/s10902-007-9049-2"><span style="font-weight: 400;">10.1007/s10902-007-9049-2</span></a></li>
                                </ol>
                                <h2><br /><br /></h2>
                            </div>
                        </div>-->
                    </div>
                </div>

            </div>
            <!-- sectiune capitol     -->
        </div>
    </div>
    <!--  card     -->
</div>
