<div class="tab-pane" id="textsummary">
    <div class="card">
        <br/><br/>
        <div class="textscrollsummary textsummaryfirst" data-Texttype="1">
            <div class="nuttshell">

                <div class="nutshell-content">
                    <h2>In a Nutshell</h2>
                    <p><span style="font-weight: 400;">For an extended period of time stress was beneficial for our species as it represented a highly complex problem-solving instrument</span></p>
                    <p><span style="font-weight: 400;">Changes that occurred in the Agricultural Revolution and then magnified during modern times transformed stress from a useful tool into a self-destruction weapon linked with numerous health disorders, as well as with asthma and allergies.</span></p>
                    <p><span style="font-weight: 400;">RISE stress management technique draws inspiration from the life of that our ancestors lived for more than two million years:</span></p>



                </div>

                <div class="do">
                    <div class="dodont-content">


                        <ul class="general-list">
                            <li>Exercise through natural movement</li>
                            <li>Foster human connections</li>
                            <li>Fix your Omega 3 to Omega 6 ratio</li>
                            <li>Get sun exposure and Vitamin D supplements</li>
                            <li>Eliminate gut pressure by eating the right food</li>
                            <li>Get enough sleep</li>
                            <li>Pursue small goals </li>
                            <li>Have a greater purpose</li>
                        </ul>
                    </div>
                </div>




            </div>
        </div>
        <div class="textscrollsummary" data-Texttype="2">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The origins of stress</span></h2>

                <p><span style="font-weight: 400;">Research suggests low mood, anxiety, stress and depression started not as disorders where the brain was operating in a haphazard way or malfunctioning, but instead, they are an intricate, highly organized piece of machinery that helps us become more analytical and better problem solvers.</span></p>
                <p><span style="font-weight: 400;">The lack of desire for anything from social interaction to food or even sexual intercourse - which can be perceived as distractions - was meant to help us concentrate on the issues needed to be addressed.</span></p>
                <p><span style="font-weight: 400;">While the ancient life was simple enough to have a stable level of control over day to day problems, the modern lifestyle has totally changed the game. The number of problems we currently need to solve is tremendous, and the general control we have is limited. As a consequence, the constant pressure is disturbing the chemical balance in our brain, and the useful problem-solving skill - that stress represented - is gradually becoming a weapon of self-destruction. &nbsp;</span></p>


                <h2><span style="font-weight: 400;">Stress, Allergies, and Asthma</span></h2>

                <p><span style="font-weight: 400;">When stress starts interfering with our ability to live a normal life for an extended period, it can wreak havoc on our bodies.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">When dealing with Asthma, stress has a complex but crucial role.</span></p>
                <p><span style="font-weight: 400;">First, our gut is the home of trillions of bacteria that form an intricate nerve center symbiotically connected with our brain.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">When we are under excessive and long-term stress, the response reaches every cell and organ in our body and has a huge toll on the gut. The bacteria perceives the strain and starts sending their stress signals which add further tension on our body. This process could spark a strenuous and vicious cycle difficult to disrupt and have large consequences on our immune system. By inducing a syndrome known as the &ldquo;the leaky gut&rdquo;, stress, along with other factors, &nbsp;can be perceived as a cause for asthma and allergies.</span></p>
                <p><span style="font-weight: 400;">Second, there is substantial evidence that psychological stress operates by altering the magnitude of the airway inflammatory response that causes irritants, allergens, and infections in people with asthma.</span></p>
                <p><span style="font-weight: 400;">All in all, just by properly managing the stress levels, one can see tremendous positive effects on their health and drastically reduce the frequency, duration, and intensity of asthma and allergy symptoms.</span></p>


                <h2><span style="font-weight: 400;">Stress Management Techniques</span></h2>

                <p><span style="font-weight: 400;">In a study of 2000 Kaluli, hunter-gatherer aborigines from Papua New Guinea, there was only one marginal case of clinical depression. Substantial evidence says the key to stress and asthma free existence stands in that first hunter and gatherer way of life which lasted for nearly 2 million years before agriculture was invented.</span></p>
                <p><span style="font-weight: 400;">The RISE stress management technique draws inspiration from this natural lifestyle of our ancestors, but even if we cannot go back living as early humans, the closer we get to their behavior, the more healthy benefits we will get.</span></p>


                <h3><strong>Exercise Through Natural Movement</strong><span style="font-weight: 400;">t</span></h3>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Exercise through natural movement</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Vitamin D through sunshine or supplement</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Apple Cider Vinegar and reduced gut tension</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Omega 3 to Omega 6 ratio</span></li>
                </ul>

                <p>For example, I use 180 - 30 (my age) + 5 (I am already in a good physical shape) to get a 155 BPM maximum heart rate.
                </p>

                <h3><span style="font-weight: 400;">Get Human Connection</span></h3>

                <p><span style="font-weight: 400;">We are born to connect with one another. The urge is etched deeply into our genes. Our hunter-gatherer ancestors rarely spent time alone. Even though the average tribe number was rather small, their everyday activities were full of social occasions.</span></p>
                <p><span style="font-weight: 400;">To connect with people more, start by fueling your passions; they work wonders in bringing people together. Join a club or community. Also, try to give back as often as possible by finding volunteering activities. In addition, disclose and open yourself to your friends and family.</span></p>


                <h3><span style="font-weight: 400;">Get Enough Sleep</span></h3>

                <p><span style="font-weight: 400;">Sleep is indispensable for our health. In fact, it is only during sleep that the body gets to perform the restoration from the fine damage suffered by millions of cells over the course of each day. It takes only a couple of nights of deprivation before negative effects start accumulating: memory and concentration wane, energy dims, judgment grows poor, reaction times get slower, coordination deteriorates and the immune system becomes deficient</span></p>
                <p><span style="font-weight: 400;">Aim for a good quality 8 hours a day of sleep. </span></p>

                <h3><span style="font-weight: 400;">Have Small Goals</span></h3>

                <p><span style="font-weight: 400;">Rumination - &nbsp;the deep and considered thought about something -</span> <span style="font-weight: 400;">&nbsp;is undoubtedly tempting for the brain. It promises to provide goods, but it rarely delivers. The perks of an active mind are that it disrupts this strenuous process.</span></p>
                <p><span style="font-weight: 400;">Find pleasant activities, and turn them into personal projects: research and become an expert in something, write a blog, learn a foreign language or play an instrument. Any small progress can lead to substantial long-term benefits.</span></p>


                <h3><span style="font-weight: 400;">Have a Greater Purpose</span></h3>
                <p><span style="font-weight: 400;">According to recent research by Fredrickson and Cole, having purpose and meaning increases our health at the cellular level and provides us with a better immune response profile, but the question of &ldquo;what is our higher purpose&rdquo; has always puzzled both scientist and philosophers.</span></p>
                <p><span style="font-weight: 400;">For myself, happiness and contentment come with a deep awareness of our complex inner planning - social but selfish creatures crafted to operate in nature but also with a need for effectiveness and efficiency - and harmonizing all its components.</span></p>
                <p><span style="font-weight: 400;">Another path to happiness comes by escaping our social matrixes, losing cultural strings and discovering our true selves. Only then will we be able to find our real underlying lodgings and &ldquo;purpose".</span></p>


                <h3><span style="font-weight: 400;">Other Important Elements:</span></h3>

                <ul class="general-list">
                    <li><span style="font-weight: 400;">Fix your Omega 3 / Omega 6 ratio</span></li>
                    <li><span style="font-weight: 400;">Get sun exposure and Vitamin D supplements</span></li>
                    <li><span style="font-weight: 400;">Eliminate the gut pressure by eating the right food</span><strong><strong><br /></strong></strong>
                    </li>
                </ul>


                <h2><span style="font-weight: 400;">A Note Regarding Children</span></h2>

                <p><span style="font-weight: 400;">It doesn&rsquo;t make any evolutionary sense for children to get depressed. Their brains should function normally until the age where they are able to reproduce.</span></p>
                <p><span style="font-weight: 400;">However, it is not uncommon nowadays for children to be diagnosed with both depression and an anxiety disorder, or depression and general anxiety. Maybe a mix of genetics combined with environmental factors triggers the disorder.</span></p>
                <p><span style="font-weight: 400;">In general, the stress management techniques that we have discussed above should be valid for children: make sure they get enough physical exercises, they are cared for, they interact and connect with others, they get plenty of Vitamin D, spend time outdoors, and keep an active mind. </span></p>



            </div>
        </div>
    </div>
</div>
