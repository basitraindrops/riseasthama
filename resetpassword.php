<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')<>"")
{
	header("Location:dashboard.php");
}


require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

require(dirname(__FILE__) . '/init.php');
$error="";
$error2="";
$error3="";
if(isset($_GET['email']))
{
   $status=1;

  //  echo $charge;
    
    require_once CLASS_DIR.'front_user_patient.class.php';
    $up=new front_user_patient($mysqli);
    $encryptObj = new encryption();
    $up->get_by_user_email_key($_GET['email'],$_GET['key']);
    
    if($up->user_id=="")
    {
        $error="Error : No Email with Forget Password Key Found! Please make sure you click the newest url!";
    }
    if($error=="")
    {
        if(isset($_POST['new_password']) && isset($_POST['confirm_password']))
        {
            if($_POST['new_password']==$_POST['confirm_password'])
            {
                $encryptObj = new encryption();
                $up->update_password($encryptObj->encrypt_password($_POST['new_password']), $up->user_name, $up->user_id);
                
                        require 'inc/mailer/PHPMailerAutoload.php';

                        //Create a new PHPMailer instance
                        $mail = new PHPMailer;
                        //Set who the message is to be sent from
                        $mail->setFrom(ADMIN_EMAIL, ' hope@riseasthma.com');
                        //Set an alternative reply-to address
                        $mail->addReplyTo(ADMIN_EMAIL, 'hope@riseasthma.com');
                        //Set who the message is to be sent to
                        $mail->addAddress($_GET['email'], $up->user_nama);
                        //Set the subject line
                        $mail->Subject = 'Your Password has been reset at Riseasthmamethod.com';
                        //Read an HTML message body from an external file, convert referenced images to embedded,
                        //convert HTML into a basic plain-text alternative body
                        $message = 'Hi '.$up->user_nama."<br/>";
                        $message.= "Someone or may be you have  reset your password<br/><br/>";
                        $message.= "Regards<br/><br/>";
                        $message.= "Web Admin";
                        $mail->msgHTML($message);
                        //Replace the plain text body with one created manually
                        $mail->AltBody = 'Email from Riseasthmamethod.com';
                        //Attach an image file
                        //$mail->addAttachment('images/phpmailer_mini.png');
                        $mail->isSendmail();
                        $mail->Sendmail = '/usr/sbin/sendmail';
                        //$mail->isSMTP();

                        //send the message, check for errors
                        if (!$mail->send()) {
                            die( "Mailer Error: " . $mail->ErrorInfo);
                        }

            }
            else
            {
                $error2="New Password must be the same as Confirm Password!";
            }
        }
    
/*   require_once CLASS_DIR.'payment.class.php';
    $p=new payment($mysqli);
    $p->insert($user_id, $amount=49.00, $payment_type='First Time Register - Credit Card', $created_by=$_POST['email']);
  
 * 
 */  
    }
//} else {
//    echo "Message sent!";
//}
/*    		$to      = $_POST['email'];
		$subject = 'Successfully Registering into Riseasthmamethod.com';
		$message = 'Hi '.$_POST['email']."<br/>";
		$message.= "You have successfully Registered into Riseasthmamethod.com<br/><br/>Login Detail : <br/>Email : ".$_POST['email']."<br/>Password : ".$_POST['email']."<br/><br/>";
		$message.= "Regards<br/><br/>";
		$message.= "Web Admin";
		$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
		'Reply-To: '.ADMIN_EMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);
                
                echo $_POST['email'].ADMIN_EMAIL;
  */

}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
    <div class="image-container set-full-height" style="background-image: url('assets/img/loginbackground.png')">
        <!--   Creative Tim Branding   -->

        <div class="logo-container">
            <div class="logo">

            </div>

        </div>


        <!--   Big container   -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form action="resetpassword.php?key=<?php echo isset($_GET['key']) ? $_GET['key'] : ''; ?>&email=<?php echo isset($_GET['email']) ? $_GET['email'] : ''; ?>" method="post">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

                                <div class="wizard-header">

                                    <div class="wizard-navigation">
                                        <div class="progress-with-circle">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                        </div>
                                        <ul>

                                            <li>
                                                <a href="#details" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-briefcase"></i>
                                                    </div>

                                                </a>
                                            </li>

                                        </ul>

                                    </div>

                                </div>


                                <div class="tab-content">


                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="info-text">Reset password</h2>
                                            </div>
                                            <div class="col-sm-12">
                                                <?php if($error<>"") { ?><div class="alert alert-danger"> <?php echo "<center>".$error."</center>"; ?></div><?php } ?>
                                                <?php if($error2<>"") { ?><div class="alert alert-danger"> <?php echo "<center>".$error2."</center>"; ?></div><?php } ?>
                                                <?php if($error2=="" && $error=="" && isset($_POST['new_password'])) { ?><div class="alert alert-success"> <?php echo "<center>Your Password has been successfully reset</center><br><center>Click <a href='".SITE_URL."page_login.php'>here</a> To login</center>"; ?></div><?php } ?>
                                            </div>
                                            <div class="col-sm-10 col-sm-offset-1">

                                                <div class="form-group">
                                                <?php if($error2=="" && $error=="" && isset($_POST['new_password'])) {  } else { ?>
                                                    <label>New Password</label>
                                                    <input type="password" class="form-control" id="exampleInputEmail1" name="new_password">
                                                    <label>Confirm Password</label>
                                                    <input type="password" class="form-control" id="exampleInputEmail1" name="confirm_password">

                                                    <div class="footer text-center">
                                                        <button type="submit" class="btn btn-fill btn-warning btn-wd">Reset password</button>
                                                    </div>
                                                <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>



                                </div>


                            </form>
                        </div>
                    </div>
                    <!-- wizard container -->
                </div>
            </div>
            <!-- row -->
        </div>
        <!--  big container -->

        <div class="footer">
            <div class="container text-center">
                Don't have an account? Sign up <a href="http://www.creative-tim.com/product/paper-bootstrap-wizard">here.</a>
            </div>
            <div>
                <img src="assets/img/logo.png" class="img-responsive logo-footer">
            </div>
        </div>
    </div>

</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>

</html>
