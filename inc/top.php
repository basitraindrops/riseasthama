<?php
/**
 * top.php
 *
 * Author: pixelcave
 *
 * The first block of code used in every page of the template
 * Start of html, <head> tag, as well as the header of the page are included here
 *
 */
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $template['title'] ?></title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo SITE_URL;?>images/icon.jpg">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo SITE_URL;?>img/icon152.png" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo SITE_URL;?>css/bootstrap.css">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="<?php echo SITE_URL;?>css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?php echo SITE_URL;?>css/main.css">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?>
        <link id="theme-link" rel="stylesheet" href="<?php echo SITE_URL;?>css/themes/<?php echo $template['theme']; ?>.css">
        <?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo SITE_URL;?>css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="<?php echo SITE_URL;?>js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>media/css/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>resources/syntax/shCore.css">
		<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/responsive/1.0.3/css/dataTables.responsive.css">

		<!--<link rel="stylesheet" type="text/css" href="resources/demo.css">-->
		<style type="text/css" class="init">

	</style>

    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body<?php if ($template['layout'] == 'fixed') echo ' class="fixed"'; ?>>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <header class="navbar navbar-inverse<?php if ($template['header'] == 'fixed-top') echo ' navbar-fixed-top'; else if ($template['header'] == 'fixed-bottom') echo ' navbar-fixed-bottom'; ?>">
                <!-- Mobile Navigation, Shows up  on smaller screens -->
                <ul class="navbar-nav-custom pull-right hidden-md hidden-lg">
<!--                    <li class="divider-vertical"></li>-->
                    <li>
                        <!-- It is set to open and close the main navigation on smaller screens. The class .navbar-main-collapse was added to aside#page-sidebar -->
                        <a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-main-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
                <!-- END Mobile Navigation -->

                <!-- Logo -->
                <a href="index.php" class="navbar-brand"><img src="<?php echo SITE_URL; ?>images/logo_relasi.png" alt="logo" height="40"></a>

                <!-- Loading Indicator, Used for demostrating how loading of widgets could happen, check main.js - uiDemo() -->
                <div id="loading" class="pull-left"><i class="fa fa-certificate fa-spin"></i></div>

                <!-- Header Widgets -->
                <!-- You can create the widgets you want by replicating the following. Each one exists in a <li> element -->
				<nav>
                <ul id="widgets" class=" visible-md visible-lg navbar-nav-custom pull-right">
					<?php
					if($sessionObj->read("front_user_email")=="")
					{
						?>
				<li class="pull-right"><a href="register_investor.php">Register as Investor</a></li>
				<li class="pull-right"><a href="register_debitur.php">Register as Debitur</a></li>
				<li class="pull-right"><a href="page_login.php">Login</a></li>
					<?php
					}
					?>

					<!-- Just a divider -->

<!--                    <li class="divider-vertical"></li>

                    <li class="divider-vertical"></li>


                    <li class="divider-vertical"></li>


                    <li class="divider-vertical"></li>
-->
                    <!-- User Menu -->
					<?php
					if($sessionObj->read("front_user_email")!="")
					{
						?>
                    <li class="dropdown pull-right dropdown-user">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
						<?php 				
						//require_once(CLASS_DIR.'user.class.php'); 
						//$sessionObj=new SecureSession();
						echo $front_user_email;
						?></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="password_update_personal.php"><i class="fa fa-lock"></i> Change Password</a>
                            </li>
                            <li>
                                <a href="logout.php"><i class="fa fa-power-off"></i> Log out</a>
                            </li>
                        </ul>
                    </li>
					<?php 
					}
								?>
                    <!-- END User Menu -->
                </ul>
				</nav>
                <!-- END Header Widgets -->
            </header>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
