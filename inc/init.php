<?php
ob_start();
require(CLASS_DIR.'session_encrypt.class.php');
global $sessionObj;
global $front_user_email;
global $front_user_id;
$sessionObj=new SecureSession();
$sessionPath = sys_get_temp_dir();
session_save_path($sessionPath);
session_start();

/* GET DATABASE AND SETTINGS */
require(INC_DIR.'database.php');
require_once(INC_DIR.'functions.php');

/* GET CLASS SECURITY TO SECURE INPUT */
include_once(CLASS_DIR.'security.class.php');


/* GET SETTINGS OF WEBSITE */
require_once(CLASS_DIR.'app_setting.class.php');
global $settings;
$settings=new settings($mysqli);
$settings->get_by_id(4);



date_default_timezone_set('Asia/Jakarta');

if(isset($_COOKIE['front_email'])&&$sessionObj->read('front_user_email')=="")
{
	$settings=new settings($mysqli);
	$settings->get_by_id(4);
	if($_COOKIE['front_login_as']=="investor")
	{
		require_once(CLASS_DIR.'front_user_investor.class.php');
		$user_obj=new front_user_investor($mysqli);
		$user_obj->get_by_user_email(decrypt($_COOKIE['front_email'],$settings->salt_key));
		if($user_obj->user_email!="")
		{
			$sessionObj->write('front_user_email',encrypt($user_obj->user_email,$settings->salt_key));
			$sessionObj->write('front_user_id',encrypt($user_obj->user_id,$settings->salt_key));
			$sessionObj->write('front_login_as',$_COOKIE["front_login_as"]);
		}
		else
		{
			header("location:cookies_error.php");
		}
	
	}
	elseif($_COOKIE['front_login_as']=="debitur")
	{
		require_once(CLASS_DIR.'front_user_debitur.class.php');
		$user_obj=new front_user_debitur($mysqli);
		$user_obj->get_by_user_email(decrypt($_COOKIE['front_email'],$settings->salt_key));
		if($user_obj->user_email!="")
		{
			$sessionObj->write('front_user_email',encrypt($user_obj->user_email,$settings->salt_key));
			$sessionObj->write('front_user_id',encrypt($user_obj->user_id,$settings->salt_key));
			$sessionObj->write('front_login_as',$_COOKIE["front_login_as"]);
		}
		else
		{
			header("location:cookies_error.php");
		}
	
	}

}
/*
echo $sessionObj->read("front_user_id");
echo '<br>';
echo $sessionObj->read("front_login_as");*/

if($sessionObj->read("front_user_id")!="" )
{
	$settings=new settings($mysqli);
	$settings->get_by_id(4);
	/*if($sessionObj->read("front_login_as")=="patient")
	{ */
		require_once(CLASS_DIR.'front_user_patient.class.php');
		$user_obj=new front_user_patient($mysqli);

		//echo decrypt($sessionObj->read("front_user_email"),$settings->salt_key);

//		exit;

		$user_obj->get_by_user_email(decrypt($sessionObj->read("front_user_email"),$settings->salt_key));
		
		if($user_obj->user_email=="")
		{
			header("location:session_error.php");

		}
		else
		{
			$front_user_email = $user_obj->user_email;
			$front_user_id = $user_obj->user_id;
		}
	/*}
	elseif($sessionObj->read("front_login_as")=="doctor")
	{
		require_once(CLASS_DIR.'front_user_doctor.class.php');
		$user_obj=new front_user_doctor($mysqli);
		$user_obj->get_by_user_email(decrypt($sessionObj->read("front_user_email"),$settings->salt_key));
		
		if($user_obj->user_email=="")
		{
			header("location:session_error.php");

		}
		else
		{
			$front_user_email = $user_obj->user_email;
			$front_user_id = $user_obj->user_id;
		}
	} */
}

date_default_timezone_set('Asia/Jakarta');

// Template variables
$template = array(
    'name'        => 'uAdmin',
    'version'     => '1.6.1',
    'author'      => 'indigomike7@gmail.com - EZLinksystem',
    'title'       => $settings->meta_title,
    'description' => $settings->description,
    'header'      => '', // 'fixed-top', 'fixed-bottom'
    'layout'      => '', // 'fixed'
    'theme'       => '', // 'deepblue', 'deepwood', 'deeppurple', 'deepgreen', '' empty for default
    'active_page' => basename($_SERVER['PHP_SELF'])
);

			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'url'   => SITE_URL,
					'icon'  => 'fa fa-home',
					'parent_id'=>0,
					'menu_id'=>1
				)
			);
?>