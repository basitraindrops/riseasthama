<?php
global $script_page;
$script_page=basename($_SERVER['PHP_SELF']);
$slug=isset($_GET['slug']) ? $_GET['slug'] : "";
$data=meta($mysqli,$script_page,$slug);
//echo print_r($data);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $data['meta_description']; ?>">
    <meta name="author" content="<?php echo $data['author']; ?>">
    <meta name="publisher" content="<?php echo $data['publisher']; ?>">
    <meta name="keywords" content="<?php echo $data['meta_keyword']; ?>">
    <title>
       <?php echo $data['title']; ?>
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo SITE_URL; ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/animate.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo SITE_URL; ?>css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo SITE_URL; ?>css/custom.css" rel="stylesheet">

    <script src="<?php echo SITE_URL; ?>js/respond.min.js"></script>

    <link rel="shortcut icon" href="<?php echo SITE_URL; ?>favicon.png">

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="<?php echo SITE_URL; ?>js/jquery-1.11.0.min.js"%3E%3C/script%3E'));</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
</head>

<body>

    <!-- *** TOPBAR ***
 _________________________________________________________ -->
    <div id="top">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
                <a href="#" class="btn btn-success btn-sm" data-animate-hover="shake">Offer of the day</a>  <a href="#">Get Discount 35% off on Register your CV!</a>
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
			<?php 
			if($sessionObj->read('employer_email_address')=="" && $sessionObj->read('candidate_email_address')=="") 
			{
				?>
					<ul class="menu">
						<li><a href="<?php echo SITE_URL; ?>post-category/news.html">News</a>
						</li>
						<li><a href="<?php echo SITE_URL; ?>post-category/blog.html">Blog</a>
						</li>
						<li><a href="<?php echo SITE_URL; ?>login">Login</a>
						</li>
					</ul>
				<?php
			}
			if($sessionObj->read('employer_email_address')!="") 
			{
				?>
					<ul class="menu">
						<li><a href="<?php echo SITE_URL; ?>logout-employer">Logout</a>
						</li>
					</ul>
				<?php
			}
			if($sessionObj->read('candidate_email_address')!="") 
			{
				?>
					<ul class="menu">
						<li><a href="<?php echo SITE_URL; ?>logout-candidate">Logout</a>
						</li>
					</ul>
				<?php
			}

				?>
            </div>
        </div>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Customer login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="login" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email-modal" placeholder="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password-modal" placeholder="password">
                            </div>
                            <div class="form-group">
                                <input type="radio" id="login_as" name="login_as" value="employer"> Employer<br/><input type="radio" id="login_as"  name="login_as" value="candidate"> Candidate
							</div>

                            <p class="text-center">
                                <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>

                        </form>

                        <p class="text-center text-muted">Not registered yet?</p>
                        <p class="text-center text-muted"><a href="register.html"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="<?php echo SITE_URL;?>" data-animate-hover="bounce">
                    <img src="<?php echo SITE_URL; ?>img/logo.png" alt="Obaju logo" class="hidden-xs">
                    <img src="<?php echo SITE_URL; ?>img/logo.png" alt="Obaju logo" class="visible-xs"><span class="sr-only">Obaju - go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">
			<?php 
			if($sessionObj->read('employer_email_address')=="" && $sessionObj->read('candidate_email_address')=="") 
			{
				?>

                <ul class="nav navbar-nav navbar-left">
                    <li><a href="<?php echo SITE_URL; ?>">Home</a>
                    </li>
                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Projects <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12">
<!--                                            <h5>Clothing</h5>
-->
                                            <ul>
												<li><i class="fa fa-check"></i>
													Unlimited job postings
												</li>
												<li><i class="fa fa-check"></i>
													No placement fees or margins
												<li><i class="fa fa-check"></i>
													Find new employees or freelancers
												</li>
												<li><i class="fa fa-check"></i>
													Advertise online or headhunt on the quiet
												</li>
												<li><i class="fa fa-check"></i>
													Search extensive database by skills, location, experience, availability, industry, etc
												</li>
												<li><i class="fa fa-check"></i>
													Avoid re-inventing the wheel with existing database 
												</li>
												<li><i class="fa fa-check"></i>
													Improve your talent pool & brand awareness
												</li>
												<p align="right">
												<a class="btn btn-primary navbar-btn" href="<?php echo SITE_URL; ?>register_employer.html">
													<i class="fa  fa-angle-double-right"></i>
													<span class="hidden-sm">Free to Register</span>
												</a>
												</p>
											</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Peoples <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul>
                                                <li><i class="fa fa-check"></i>
													Be found by those who need you
												</li>
                                                <li><i class="fa fa-check"></i>
													No recruitment agencies taking a slice of your rate
												</li>
                                                <li><i class="fa fa-check"></i>
													Create a public or private profile
												</li>
                                                <li><i class="fa fa-check"></i>
													Contract & Permanent roles
												</li>
                                                <li><i class="fa fa-check"></i>
													Engage direct with hiring managers & end-user companies
												</li>
                                                <li><i class="fa fa-check"></i>
													Better plan your next role
												</li>
                                                <li><i class="fa fa-check"></i>
													Find real requirements direct to the hiring firm
												</li>
                                                <li><i class="fa fa-check"></i>
													Set your preferences on location, target salary, role type, etc
												</li>
                                                <li><i class="fa fa-check"></i>
													Get notified of target positions
												</li>
                                                <li><i class="fa fa-check"></i>
													Get informed: market info on salaries, roles, employment trends.
                                                </li>
												<p align="right">
												<a class="btn btn-primary navbar-btn" href="<?php echo SITE_URL; ?>register_employer.html">
													<i class="fa  fa-angle-double-right"></i>
													<span class="hidden-sm">From 10$/month</span>
												</a>
												</p>
                                            </ul>
                                        </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">How It Works <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12" style="text-align:center;">
												<h5>Employers know what they need and when they need it</h5>
												<i class="fa fa-arrow-down"></i><br/>
												<h5>People know when they�re looking or available for work</h5>
												<i class="fa fa-arrow-down"></i><br/>
												<h5>No-one likes recruitment agencies or ringing around for availability</h5>
												<i class="fa fa-arrow-down"></i><br/>
												<h5>Advertising requirements is too expensive with slow results</h5><br/><br/>

												<h5>Availery is an online platform for:</h5>
                                            <ul>
												<li>
													<i class="fa fa-check"></i>
													Employers to find available skills and post requirements
												</li>
												<li>
													<i class="fa fa-check"></i>
													People to be found directly for their preference roles
												</li>
                                            </ul>
                                        </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                </ul>
			<?php 
								}
								?>
			<?php 
			if($sessionObj->read('employer_email_address')!="") 
			{
				?>
                <ul class="nav navbar-nav navbar-left">
                    <li class="active"><a href="<?php echo SITE_URL; ?>">Home</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>employer-post-jobs">Post Jobs</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>employer-posted-jobs">My Posted Jobs</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>employer-messages">Messages</a>
                    </li>
                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profile <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul>
												<li><a href="<?php echo SITE_URL."employer-company-profile";?>">Company Profile</a>
												</li>
												<li><a href="<?php echo SITE_URL."employer-user-profile";?>">User Profile</a>
												</li>
											</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                </ul>
				<?php
			}
			if($sessionObj->read('candidate_email_address')!="") 
			{
				?>
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="<?php echo SITE_URL; ?>">Home</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>employer-post-jobs">Jobs</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>candidate-job-application">My Job Application</a>
                    </li>
                    <li><a href="<?php echo SITE_URL; ?>candidate-messages">Messages</a>
                    </li>
                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profile <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul>
												<li><a href="<?php echo SITE_URL."candidate-skills";?>">Skills</a>
												</li>
												<li><a href="<?php echo SITE_URL."candidate-resume-cv";?>">CV/Resume</a>
												</li>
												<li><a href="<?php echo SITE_URL."candidate-user-profile";?>">User Profile</a>
												</li>
											</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                </ul>
				<?php
			}

				?>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <!--/.nav-collapse -->

                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

		    </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->



    <div id="all">
