<?php
class province
{
	public $province_id;
	public $province_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$province_id='',$province_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->province_id = $province_id;
		$this->province_name = $province_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_province_id($province_id)
	{
		$security = new security();
		$query="select *
		from general_province
		where province_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($province_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($province_id,$province_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$province_id,$province_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function province_all()
	{
		$query="select * 
		from general_province 
		order by province_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($province_id,$province_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$province=new province($this->conn,$province_id,$province_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$province;
		}
		return $data;
	}
	function update($province_name,$updated_by,$province_id)
	{
		$security = new security();
		$query="update general_province set province_name=?,updated_date=?,updated_by=? where 
		province_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($province_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($province_id)));
		$stmt->execute();
	}
	function insert($province_name,$created_by)
	{
		$security = new security();
		$query="insert into general_province(province_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($province_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($province_id)
	{
		$security = new security();
		$query="delete from general_province where province_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($province_id)));
		$stmt->execute();
	}
}
?>