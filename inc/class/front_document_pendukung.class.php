<?php
class front_document_pendukung
{
	public $id;
	public $name;
	public $fui_id;
	public $fud_id;
	public $document_type_id;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn=''
	,$id=''
	,$name=''
	,$fui_id=''
	,$fud_id=''
	,$document_type_id=''
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by=''
	)
	{
		$this->conn = $conn;
		$this->id=$id;
		$this->name= $name;
		$this->fui_id=$fui_id;
		$this->fud_id= $fud_id;
		$this->document_type_id=$document_type_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function get_by_fu_id($id)
	{
		$security = new security();
		$query="select *
		from front_document_pendukung
		where id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function get_by_fud_id($fud_id)
	{
		$security = new security();
		$query="select *
		from front_document_pendukung
		where fud_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($fud_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function dp_all()
	{
		$query="select * 
		from front_document_pendukung 
		order by id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_debitur($this->conn
			,$id
			,$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$updated_by
			,$id
)
	{
		$security = new security();
		$query="update front_front_document_pendukung set 
		name=?
		,fui_id=?
		,fud_id=?
		,document_type_id=?
		,updated_date=?
		,updated_by=?
		where 
		id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssi',($security->xss_clean($name))
			,($security->xss_clean($fui_id))
			,($security->xss_clean($fud_id))
			,($security->xss_clean($document_type_id))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($id)));
		$stmt->execute();
	}
	function insert(			
			$name
			,$fui_id
			,$fud_id
			,$document_type_id
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_document_pendukung(
			name
			,fui_id
			,fud_id
			,document_type_id
			,created_date
			,created_by
		)values(?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssss',($security->xss_clean($name))
			,($security->xss_clean($fui_id))
			,($security->xss_clean($fud_id))
			,($security->xss_clean($document_type_id))
			,(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute() or die(mysqli_error($this->conn));
	}
	function delete($id)
	{
		$security = new security();
		$query="delete from front_document_pendukung where id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($id)));
		$stmt->execute();
	}
}
?>
