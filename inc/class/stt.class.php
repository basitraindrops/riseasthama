<?php
class stt
{
	public $stt_id;
	public $stt_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$stt_id='',$stt_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->stt_id = $stt_id;
		$this->stt_name = $stt_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_stt_id($stt_id)
	{
		$security = new security();
		$query="select *
		from general_status_tempat_tinggal
		where stt_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($stt_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($stt_id,$stt_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$stt_id,$stt_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function stt_all()
	{
		$query="select * 
		from general_status_tempat_tinggal 
		order by stt_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($stt_id,$stt_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$stt=new stt($this->conn,$stt_id,$stt_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$stt;
		}
		return $data;
	}
	function update($stt_name,$updated_by,$stt_id)
	{
		$security = new security();
		$query="update general_status_tempat_tinggal set stt_name=?,updated_date=?,updated_by=? where 
		stt_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($stt_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($stt_id)));
		$stmt->execute();
	}
	function insert($stt_name,$created_by)
	{
		$security = new security();
		$query="insert into general_status_tempat_tinggal(stt_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($stt_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($stt_id)
	{
		$security = new security();
		$query="delete from general_status_tempat_tinggal where stt_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($stt_id)));
		$stmt->execute();
	}
}
?>