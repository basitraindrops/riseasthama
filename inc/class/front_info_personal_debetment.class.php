<?php
class front_info_personal_debetment
{
	public $pd_id;
	public $fu_id;
	public $income_per_month;
	public $marriage_status;
	public $jlh_anak;
	public $status_tempat_tinggal;
	public $ktp_file;
	public $kk_file;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn=''
	,$pd_id=''
	,$fu_id=''
	,$income_per_month=''
	,$marriage_status=''
	,$jlh_anak=''
	,$status_tempat_tinggal=''
	,$ktp_file=''
	,$kk_file=''
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by=''
	)
	{
		$this->conn = $conn;
		$this->pd_id=$pd_id;
		$this->fu_id= $fu_id;
		$this->income_per_month=$income_per_month;
		$this->marriage_status= $marriage_status;
		$this->jlh_anak=$jlh_anak;
		$this->status_tempat_tinggal= $status_tempat_tinggal;
		$this->ktp_file=$ktp_file;
		$this->kk_file=$kk_file;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_fu_id($title_id)
	{
		$security = new security();
		$query="select *
		from front_info_personal_debetment
		where fu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($title_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$pd_id
			,$fu_id
			,$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$pd_id
			,$fu_id
			,$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
		
	}
	function personal_debetment_all()
	{
		$query="select * 
		from front_info_personal_debetment 
		order by user_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$pd_id
			,$fu_id
			,$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_debitur($this->conn
			,$pd_id
			,$fu_id
			,$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$updated_by
			,$fu_id
)
	{
		$security = new security();
		$query="update front_user_debitur set income_per_month=?
			,marriage_status=?
			,jlh_anak=?
			,status_tempat_tinggal=?
			,ktp_file=?
			,kk_file=?
			,updated_date=?
			,updated_by=?
		where 
		fu_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssi',($security->xss_clean($user_nama))
			,($security->xss_clean($income_per_month))
			,($security->xss_clean($marriage_status))
			,($security->xss_clean($jlh_anak))
			,($security->xss_clean($status_tempat_tinggal))
			,($security->xss_clean($ktp_file))
			,($security->xss_clean($kk_file))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($fu_id)));
		$stmt->execute();
	}
	function insert(			
			$fu_id
			,$income_per_month
			,$marriage_status
			,$jlh_anak
			,$status_tempat_tinggal
			,$ktp_file
			,$kk_file
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_info_personal_debetment(
			fu_id
			,income_per_month
			,marriage_status
			,jlh_anak
			,status_tempat_tinggal
			,ktp_file
			,kk_file
			,created_date
			,created_by
		)values(?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssss',($security->xss_clean($fu_id))
			,($security->xss_clean($income_per_month))
			,($security->xss_clean($marriage_status))
			,($security->xss_clean($jlh_anak))
			,($security->xss_clean($status_tempat_tinggal))
			,($security->xss_clean($ktp_file))
			,($security->xss_clean($kk_file))
			,(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute() or die(mysqli_error($this->conn));
	}
	function delete($pd_id)
	{
		$security = new security();
		$query="delete from front_info_personal_debetment where pd_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($pd_id)));
		$stmt->execute();
	}
}
?>
