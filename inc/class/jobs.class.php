<?php
class access
{
	public $jobs_id;
	public $employer_id;
	public $jobs_title;
	public $jobs_expired_date;
	public $jobs_description;
	public $years_experience;
	public $education_level;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	private $conn;
	function __construct($conn='',$jobs_id='',$employer_id='',$jobs_title='',$jobs_expired_date='',$jobs_description="",$years_experience='',$education_level='',$created_by='',$created_date='',$updated_by='',$updated_date="")
	{
		$this->conn = $conn;
		$this->jobs_id = $jobs_id;
		$this->employer_id=$employer_id;
		$this->jobs_title = $jobs_title;
		$this->jobs_expired_date=$jobs_expired_date;
		$this->jobs_description=$jobs_description
		$this->years_experience = $years_experience;
		$this->education_level=$education_level
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function jobs_all()
	{
		$query="select * 
		from jobs j
		left join years_experience ye
		on j.years_experience = ye.years_experience_name 
		left join education_level el
		on j.education_level = el.education_level_name 
		left join employer e
		on j.employer_id = e.employer_id
		order by ye.jobs_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($jobs_id,$employer_id,$jobs_title,$jobs_expired_date,$jobs_description,$years_experience,$education_level,$created_by,$created_date,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$jobs=new jobs($this->conn,$jobs_id,$employer_id,$jobs_title,$jobs_expired_date,$jobs_description,$years_experience,$education_level,$created_by,$created_date,$updated_by,$updated_date);
			$data[]=$jobs;
		}
		return $data;
	}
	function update($employer_id,$jobs_title,$jobs_expired_date,$jobs_description,$years_experience,$education_level,$updated_by,$jobs_id)
	{
		$security = new security();
		$query="update jobs set employer_id=?, jobs_title=?,jobs_expired_date=?,jobs_description=?,years_experience=?,education_level=?,updated_date=?,updated_by=? where 
		jobs_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('isssssssi'
			,($security->xss_clean($employer_id))
			,($security->xss_clean($jobs_title))
			,($security->xss_clean($jobs_expired_date))
			,($security->xss_clean($jobs_description))
			,($security->xss_clean($years_experience))
			,($security->xss_clean($education_level))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($updated_by))
			,($security->xss_clean($access_id))
				);
		$stmt->execute();
	}
	function insert($employer_id,$jobs_title,$jobs_expired_date,$jobs_description,$years_experience,$education_level,$created_by)
	{
		$security = new security();
		$query="insert into jobs(employer_id
		,jobs_title
		,jobs_expired_date
		,jobs_description
		,years_experience
		,education_level
		,created_date
		,created_by)values(?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('isssssss'
			,($security->xss_clean($employer_id))
			,($security->xss_clean($jobs_title))
			,($security->xss_clean($jobs_expired_date))
			,($security->xss_clean($jobs_description))
			,($security->xss_clean($years_experience))
			,($security->xss_clean($education_level))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($created_by))
				);
		$stmt->execute();
	}
	function delete($jobs_id)
	{
		$security = new security();
		$query="delete from jobs where jobs_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($jobs_id)));
		$stmt->execute();
	}
}
?>