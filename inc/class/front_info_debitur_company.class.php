<?php
class front_info_debitur_company
{
	public $ic_id;
	public $fu_id;
	public $company_name;
	public $address;
	public $province_id;
	public $kota_id;
	public $no_npwp;
	public $no_sk_badan;
	public $no_siup;
	public $no_akte;
	public $no_tdp;
	public $jenis_badan_hukum;
	public $company_phone;
	public $company_nama_penanggung_jawab;
	public $company_jabatan;
	public $company_penanggung_jawab_phone;
	public $company_penanggung_jawab_mobile;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $conn;
	function __construct($conn=''
	,$ic_id=''
	,$fu_id=''
	,$company_name=''
	,$address=''
	,$province_id=''
	,$kota_id=''
	,$no_npwp=''
	,$no_sk_badan=''
	,$no_siup=''
	,$no_akte=''
	,$no_tdp=''
	,$jenis_badan_hukum=''
	,$company_phone=''
	,$company_nama_penanggung_jawab=''
	,$company_jabatan=''
	,$company_penanggung_jawab_phone=''
	,$company_penanggung_jawab_mobile=''
	,$created_date=''
	,$created_by=''
	,$updated_by=''
	,$updated_date=''
	)
	{
		$this->conn = $conn;
		$this->ic_id=$ic_id;
		$this->fu_id= $fu_id;
		$this->company_name=$company_name;
		$this->address= $address;
		$this->province_id= $province_id;
		$this->kota_id= $kota_id;
		$this->no_npwp=$no_npwp;
		$this->no_sk_badan=$no_sk_badan;
		$this->no_siup=$no_siup;
		$this->no_akte=$no_akte;
		$this->no_tdp=$no_tdp;
		$this->jenis_badan_hukum=$jenis_badan_hukum;
		$this->company_phone=$company_phone;
		$this->company_nama_penanggung_jawab=$company_nama_penanggung_jawab;
		$this->company_jabatan=$company_jabatan;
		$this->company_penanggung_jawab_phone=$company_penanggung_jawab_phone;
		$this->company_penanggung_jawab_mobile=$company_penanggung_jawab_mobile;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_fu_id($fu_id)
	{
		$security = new security();
		$query="select *
		from front_info_debitur_company
		where fu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($fu_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$ic_id
			,$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$ic_id
			,$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function title_all()
	{
		$query="select * 
		from front_info_debitur_company 
		order by fu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$ic_id
			,$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_debitur($this->conn
			,$ic_id
			,$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$updated_by
		,$ic_id
)
	{
		$security = new security();
		$query="update front_info_debitur_company set fu_id=?
			,company_name=?
			,address=?
			,province_id=?
			,kota_id=?
			,no_npwp=?
			,no_sk_badan=?
			,no_siup=?
			,no_akte=?
			,no_tdp=?
			,jenis_badan_hukum=?
			,company_phone=?
			,company_nama_penanggung_jawab=?
			,company_jabatan=?
			,company_penanggung_jawab_phone=?
			,company_penanggung_jawab_mobile=?
			,updated_date
			,updated_by=?
		where 
		user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssssssssssssi',($security->xss_clean($fu_id))
			,($security->xss_clean($company_name))
			,($security->xss_clean($address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_sk_badan))
			,($security->xss_clean($no_siup))
			,($security->xss_clean($no_akte))
			,($security->xss_clean($no_tdp))
			,($security->xss_clean($jenis_badan_hukum))
			,($security->xss_clean($company_phone))
			,($security->xss_clean($company_nama_penanggung_jawab))
			,($security->xss_clean($company_jabatan))
			,($security->xss_clean($company_penanggung_jawab_phone))
			,($security->xss_clean($company_penanggung_jawab_mobile))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function insert(			
			$fu_id
			,$company_name
			,$address
			,$province_id
			,$kota_id
			,$no_npwp
			,$no_sk_badan
			,$no_siup
			,$no_akte
			,$no_tdp
			,$jenis_badan_hukum
			,$company_phone
			,$company_nama_penanggung_jawab
			,$company_jabatan
			,$company_penanggung_jawab_phone
			,$company_penanggung_jawab_mobile
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_info_debitur_company(
			fu_id
			,company_name
			,address
			,province_id
			,kota_id
			,no_npwp
			,no_sk_badan
			,no_siup
			,no_akte
			,no_tdp
			,jenis_badan_hukum
			,company_phone
			,company_nama_penanggung_jawab
			,company_jabatan
			,company_penanggung_jawab_phone
			,company_penanggung_jawab_mobile
			,created_date
			,created_by
		)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssssssssssssssssss'
			,($security->xss_clean($fu_id))
			,($security->xss_clean($company_name))
			,($security->xss_clean($address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_sk_badan))
			,($security->xss_clean($no_siup))
			,($security->xss_clean($no_akte))
			,($security->xss_clean($no_tdp))
			,($security->xss_clean($jenis_badan_hukum))
			,($security->xss_clean($company_phone))
			,($security->xss_clean($company_nama_penanggung_jawab))
			,($security->xss_clean($company_jabatan))
			,($security->xss_clean($company_penanggung_jawab_phone))
			,($security->xss_clean($company_penanggung_jawab_mobile))
			,(date("Y-m-d H:i:s")),($security->xss_clean($created_by)))or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($ic_id)
	{
		$security = new security();
		$query="delete from front_info_debitur_company where ic_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($ic_id)));
		$stmt->execute();
	}
}
?>
