<?php
class security_clearance
{
	public $security_clearance_id;
	public $security_clearance_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$security_clearance_id='',$security_clearance_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->security_clearance_id = $security_clearance_id;
		$this->security_clearance_name = $security_clearance_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_security_clearance_id($security_clearance_id)
	{
		$security = new security();
		$query="select *
		from security_clearance
		where security_clearance_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($security_clearance_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($security_clearance_id,$security_clearance_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$security_clearance_id,$security_clearance_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function security_clearance_all()
	{
		$query="select * 
		from security_clearance 
		order by security_clearance_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($security_clearance_id,$security_clearance_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$security_clearance=new security_clearance($this->conn,$security_clearance_id,$security_clearance_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$security_clearance;
		}
		return $data;
	}
	function update($security_clearance_name,$updated_by,$security_clearance_id)
	{
		$security = new security();
		$query="update security_clearance set security_clearance_name=?,updated_date=?,updated_by=? where 
		security_clearance_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($security_clearance_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($security_clearance_id)));
		$stmt->execute();
	}
	function insert($security_clearance_name,$created_by)
	{
		$security = new security();
		$query="insert into security_clearance(security_clearance_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($security_clearance_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($security_clearance_id)
	{
		$security = new security();
		$query="delete from security_clearance where security_clearance_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($security_clearance_id)));
		$stmt->execute();
	}
}
?>