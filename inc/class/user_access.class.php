<?php
class user_access
{
	public $ua_id;
	public $access_id;
	public $user_id;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	private $conn;
	function __construct($conn='',$ua_id='',$access_id='',$user_id='',$created_date='',$created_by='',$updated_date='',$updated_by="")
	{
		$this->conn = $conn;
		$this->ua_id=$ua_id;
		$this->access_id = $access_id;
		$this->user_id= $user_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function get1($access_id,$user_id)
	{
		$security = new security();
		$query="select *
		from user_access  
		where access_id = ? and user_id=? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii', ($security->xss_clean($access_id)), ($security->xss_clean($user_id)));
		$stmt->execute();

		$stmt->bind_result($ua_id,$access_id,$user_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$ua_id,$access_id,$user_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get2($access_name,$user_id)
	{
		$security = new security();
		$query="select uga.* 
		from user_group_access uga 
		left join access_settings ass 
		on uga.access_id = ass.access_id 
		left join user_group ug 
		on uga.user_group_id = ug.user_group_id 
		left join user u on u.user_group_id = ug.user_group_id 
		where ass.access_name=? and u.user_id=?   limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('si', ($security->xss_clean($access_name)), ($security->xss_clean($user_id)));
		$stmt->execute();

		$stmt->bind_result($ua_id,$access_id,$user_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$ua_id,$access_id,$user_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function insert($access_id,$user_id,$created_by)
	{
		$security = new security();
		$query="insert into user_access
		(access_id
		, user_id
		, created_date
		, created_by)
		values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('iiss',($security->xss_clean($access_id)),($security->xss_clean($user_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($access_id,$user_id)
	{
		$security = new security();
		$query="delete from user_access where access_id=? and user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii',($security->xss_clean($access_id)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
}
?>