<?php
class user_group
{
	public $user_group_id;
	public $user_group_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$user_group_id='',$user_group_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->user_group_id = $user_group_id;
		$this->user_group_name = $user_group_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_user_group_id($user_group_id)
	{
		$security = new security();
		$query="select *
		from user_group
		where user_group_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($user_group_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($user_group_id,$user_group_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$user_group_id,$user_group_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function user_group_all()
	{
		$query="select * 
		from user_group 
		order by user_group_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($user_group_id,$user_group_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$user_group=new user_group($this->conn,$user_group_id,$user_group_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$user_group;
		}
		return $data;
	}
	function update($user_group_name,$updated_by,$user_group_id)
	{
		$security = new security();
		$query="update user_group set user_group_name=?,updated_date=?,updated_by=? where 
		user_group_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($user_group_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($user_group_id)));
		$stmt->execute();
	}
	function insert($user_group_name,$created_by)
	{
		$security = new security();
		$query="insert into user_group(user_group_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($user_group_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($user_group_id)
	{
		$security = new security();
		$query="delete from user_group where user_group_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($user_group_id)));
		$stmt->execute();
	}
}
?>