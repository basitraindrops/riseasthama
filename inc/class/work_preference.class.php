<?php
class work_preference
{
	public $work_preference_id;
	public $work_preference_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$work_preference_id='',$work_preference_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->work_preference_id = $work_preference_id;
		$this->work_preference_name = $work_preference_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_work_preference_id($work_preference_id)
	{
		$security = new security();
		$query="select *
		from work_preference
		where work_preference_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($work_preference_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($work_preference_id,$work_preference_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$work_preference_id,$work_preference_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function work_preference_all()
	{
		$query="select * 
		from work_preference 
		order by work_preference_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($work_preference_id,$work_preference_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$work_preference=new work_preference($this->conn,$work_preference_id,$work_preference_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$work_preference;
		}
		return $data;
	}
	function update($work_preference_name,$updated_by,$work_preference_id)
	{
		$security = new security();
		$query="update work_preference set work_preference_name=?,updated_date=?,updated_by=? where 
		work_preference_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($work_preference_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($work_preference_id)));
		$stmt->execute();
	}
	function insert($work_preference_name,$created_by)
	{
		$security = new security();
		$query="insert into work_preference(work_preference_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($work_preference_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($work_preference_id)
	{
		$security = new security();
		$query="delete from work_preference where work_preference_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($work_preference_id)));
		$stmt->execute();
	}
}
?>