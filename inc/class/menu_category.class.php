<?php
class menu_category
{
	public $menu_category_id;
	public $menu_category_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$menu_category_id='',$menu_category_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->menu_category_id = $menu_category_id;
		$this->menu_category_name = $menu_category_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_menu_category_id($menu_category_id)
	{
		$security = new security();
		$query="select *
		from menu_category
		where menu_category_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($menu_category_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($menu_category_id,$menu_category_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$menu_category_id,$menu_category_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function menu_category_all()
	{
		$query="select * 
		from menu_category 
		order by menu_category_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($menu_category_id,$menu_category_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$menu_category=new menu_category($this->conn,$menu_category_id,$menu_category_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$menu_category;
		}
		return $data;
	}
	function update($menu_category_name,$updated_by,$menu_category_id)
	{
		$security = new security();
		$query="update menu_category set menu_category_name=?,updated_date=?,updated_by=? where 
		menu_category_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($menu_category_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($menu_category_id)));
		$stmt->execute();
	}
	function insert($menu_category_name,$created_by)
	{
		$security = new security();
		$query="insert into menu_category(menu_category_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($menu_category_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($menu_category_id)
	{
		$security = new security();
		$query="delete from menu_category where menu_category_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($menu_category_id)));
		$stmt->execute();
	}
}
?>