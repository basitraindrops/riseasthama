<?php
class front_info_company_investment
{
	public $ci_id;
	public $fu_id;
	public $lama_investasi;
	public $minimum_bunga_perbulan;
	public $dana_investasi;
	public $akte_file;
	public $npwp_file;

	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	public $conn;
	function __construct($conn=''
	,$ci_id=''
	,$fu_id=''
	,$lama_investasi=''
	,$minimum_bunga_perbulan=''
	,$dana_investasi=''
	,$akte_file=''
	,$npwp_file=''
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by=''
	)
	{
		$this->conn = $conn;
		$this->ci_id=$ci_id;
		$this->fu_id= $fu_id;
		$this->lama_investasi=$lama_investasi;
		$this->minimum_bunga_perbulan= $minimum_bunga_perbulan;
		$this->dana_investasi= $dana_investasi;
		$this->akte_file= $akte_file;
		$this->npwp_file=$npwp_file;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function get_by_fu_id($fu_id)
	{
		$security = new security();
		$query="select *
		from front_info_company_investment
		where fu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($fu_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$ci_id
			,$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$ci_id
			,$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
		
	}
	function ci_all()
	{
		$query="select * 
		from front_info_company_investment 
		order by fu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$ci_id
			,$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_debitur($this->conn
			,$ci_id
			,$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$updated_by
			,$ci_id

)
	{
		$security = new security();
		$query="update front_info_company_investment set fu_id=?
			,lama_investasi=?
			,minimum_bunga_perbulan=?
			,dana_investasi=?
			,akte_file=?
			,npwp_file=?
			,updated_date=/
			,updated_by=?
		where 
		ci_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssi',($security->xss_clean($fu_id))
			,($security->xss_clean($lama_investasi))
			,($security->xss_clean($minimum_bunga_perbulan))
			,($security->xss_clean($dana_investasi))
			,($security->xss_clean($akte_file))
			,($security->xss_clean($npwp_file))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($ci_id)));
		$stmt->execute();
	}
	function insert(			
			$fu_id
			,$lama_investasi
			,$minimum_bunga_perbulan
			,$dana_investasi
			,$akte_file
			,$npwp_file
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_info_company_investment(
			fu_id
			,lama_investasi
			,minimum_bunga_perbulan
			,dana_investasi
			,akte_file
			,npwp_file
			,created_date
			,created_by
		)values(?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssss'
			,($security->xss_clean($fu_id))
			,($security->xss_clean($lama_investasi))
			,($security->xss_clean($minimum_bunga_perbulan))
			,($security->xss_clean($dana_investasi))
			,($security->xss_clean($akte_file))
			,($security->xss_clean($npwp_file))
			,(date("Y-m-d H:i:s")),($security->xss_clean($created_by)))or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($ci_id)
	{
		$security = new security();
		$query="delete from front_info_company_investment where ci_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($ci_id)));
		$stmt->execute();
	}
}
?>
