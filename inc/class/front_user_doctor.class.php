<?php
class front_user_doctor
{
	public $user_id;
	public $user_nama;
	public $user_email;
	public $user_name;
	public $user_address;
	public $province_id;
	public $kota_id;
	public $no_telp;
	public $no_hp;
	public $no_npwp;
	public $no_ktp_sim;
	public $tempat_lahir;
	public $tanggal_lahir;
	public $jenis_kelamin;
	public $pendidikan;
	public $pekerjaan;
	public $nama_perusahaan;
	public $jabatan;
	public $user_password;
	public $forgot_password_key;
	public $forgot_status;
	public $status;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $conn;
	function __construct($conn=''
	,$user_id=''
	,$user_nama=''
	,$user_email=''
	,$user_name=''
	,$user_address=''
	,$province_id=''
	,$kota_id=''
	,$no_telp=''
	,$no_hp=''
	,$no_npwp=''
	,$no_ktp_sim=''
	,$tempat_lahir=''
	,$tanggal_lahir=''
	,$jenis_kelamin=''
	,$pendidikan=''
	,$pekerjaan=''
	,$nama_perusahaan=''
	,$jabatan=''
	,$user_password=''
	,$forgot_password_key=''
	,$forgot_status=''
	,$status=''
	,$created_date=''
	,$created_by=''
	,$updated_by=''
	,$updated_date=''
	)
	{
		$this->conn = $conn;
		$this->user_id=$user_id;
		$this->user_nama= $user_nama;
		$this->user_email=$user_email;
		$this->user_name= $user_name;
		$this->user_address= $user_address;
		$this->province_id=$province_id;
		$this->kota_id= $kota_id;
		$this->no_telp=$no_telp;
		$this->no_hp=$no_hp;
		$this->no_npwp=$no_npwp;
		$this->no_ktp_sim=$no_ktp_sim;
		$this->tempat_lahir=$tempat_lahir;
		$this->tanggal_lahir=$tanggal_lahir;
		$this->jenis_kelamin=$jenis_kelamin;
		$this->pendidikan=$pendidikan;
		$this->pekerjaan=$pekerjaan;
		$this->nama_perusahaan=$nama_perusahaan;
		$this->jabatan=$jabatan;
		$this->user_password=$user_password;
		$this->forgot_password_key=$forgot_password_key;
		$this->forgot_status=$forgot_status;
		$this->status=$status;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function loginby_email_password($email,$password)
	{
//		echo $email." ".$password;
		$query="select *
		from front_user_doctor where user_email = ? or user_name = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$stmt->bind_param('ss', ($security->xss_clean($email)),($security->xss_clean($email)));
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		$stmt->fetch();

//		echo $user_email;
		$encryptObj = new encryption();
		//echo $password;
		if($encryptObj->validate_password($password, $user_password))
		{
			$this->__construct($this->conn=''
				,$user_id
				,$user_nama
				,$user_email
				,$user_name
				,$user_address
				,$province_id
				,$kota_id
				,$no_telp
				,$no_hp
				,$no_npwp
				,$no_ktp_sim
				,$tempat_lahir
				,$tanggal_lahir
				,$jenis_kelamin
				,$pendidikan
				,$pekerjaan
				,$nama_perusahaan
				,$jabatan
				,$user_password
				,$forgot_password_key
				,$forgot_status
				,$status
				,$created_date
				,$created_by
				,$updated_by
				,$updated_date
				);
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_by_user_id($title_id)
	{
		$security = new security();
		$query="select *
		from front_user_doctor
		where user_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($title_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function get_by_user_email($user_email)
	{
		$security = new security();
		$query="select *
		from front_user_doctor
		where user_email = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($user_email);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function title_all()
	{
		$query="select * 
		from front_user_doctor 
		order by user_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_doctor($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$user_id
)
	{
		$security = new security();
		$query="update front_user_doctor set user_nama=?
			,user_email=?
			,user_name=?
			,user_address=?
			,province_id=?
			,kota_id=?
			,no_telp=?
			,no_hp=?
			,no_npwp=?
			,no_ktp_sim=?
			,tempat_lahir=?
			,tanggal_lahir=?
			,jenis_kelamin=?
			,pendidikan=?
			,pekerjaan=?
			,nama_perusahaan=?
			,jabatan=?
			,user_password=?
			,forgot_password_key=?
			,forgot_status=?
			,status=?
			,updated_date=?
			,updated_by=?
		where 
		user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssssssssssssssssssssi',($security->xss_clean($user_nama))
			,($security->xss_clean($user_email))
			,($security->xss_clean($user_name))
			,($security->xss_clean($user_address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_telp))
			,($security->xss_clean($no_hp))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_ktp_sim))
			,($security->xss_clean($tempat_lahir))
			,($security->xss_clean($tanggal_lahir))
			,($security->xss_clean($jenis_kelamin))
			,($security->xss_clean($pendidikan))
			,($security->xss_clean($pekerjaan))
			,($security->xss_clean($nama_perusahaan))
			,($security->xss_clean($jabatan))
			,($security->xss_clean($user_password))
			,($security->xss_clean($forgot_password_key))
			,($security->xss_clean($forgot_status))
			,($security->xss_clean($status))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function insert(			
			$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
			,$status
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_user_doctor(
			user_nama
			,user_email
			,user_name
			,user_address
			,province_id
			,kota_id
			,no_telp
			,no_hp
			,no_npwp
			,no_ktp_sim
			,tempat_lahir
			,tanggal_lahir
			,jenis_kelamin
			,pendidikan
			,pekerjaan
			,nama_perusahaan
			,jabatan
			,user_password
			,forgot_password_key
			,forgot_status
			,status
			,created_date
			,created_by
		)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssssssssssssssssss',($security->xss_clean($user_nama))
			,($security->xss_clean($user_email))
			,($security->xss_clean($user_name))
			,($security->xss_clean($user_address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_telp))
			,($security->xss_clean($no_hp))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_ktp_sim))
			,($security->xss_clean($tempat_lahir))
			,($security->xss_clean($tanggal_lahir))
			,($security->xss_clean($jenis_kelamin))
			,($security->xss_clean($pendidikan))
			,($security->xss_clean($pekerjaan))
			,($security->xss_clean($nama_perusahaan))
			,($security->xss_clean($jabatan))
			,($security->xss_clean($user_password))
			,($security->xss_clean($forgot_password_key))
			,($security->xss_clean($forgot_status))
			,($security->xss_clean($status)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)))or die("error insert user".mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function update_password($new_password,$user_email,$user_id)
	{
		$security = new security();
		$query="update front_user_doctor set user_password=?,updated_date=?,updated_by=? where user_id = ?";
		$stmt = $this->conn->prepare($query)or die(mysqli_error($this->conn));
		$stmt->bind_param('sssi',($security->xss_clean($new_password)),(date("Y-m-d H:i:s")),($security->xss_clean($user_email)),($security->xss_clean($user_id)))or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($user_id)
	{
		$security = new security();
		$query="delete from front_user_doctor where user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($user_id)));
		$stmt->execute();
	}
}
?>
