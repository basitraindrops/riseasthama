<?php
class title
{
	public $title_id;
	public $title_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$title_id='',$title_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->title_id = $title_id;
		$this->title_name = $title_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_title_id($title_id)
	{
		$security = new security();
		$query="select *
		from general_title
		where title_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($title_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($title_id,$title_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$title_id,$title_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function title_all()
	{
		$query="select * 
		from general_title 
		order by title_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($title_id,$title_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$title=new title($this->conn,$title_id,$title_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$title;
		}
		return $data;
	}
	function update($title_name,$updated_by,$title_id)
	{
		$security = new security();
		$query="update general_title set title_name=?,updated_date=?,updated_by=? where 
		title_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($title_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($title_id)));
		$stmt->execute();
	}
	function insert($title_name,$created_by)
	{
		$security = new security();
		$query="insert into general_title(title_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($title_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($title_id)
	{
		$security = new security();
		$query="delete from general_title where title_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($title_id)));
		$stmt->execute();
	}
}
?>