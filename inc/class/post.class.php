<?php
class post
{
	public $post_id;
	public $post_title;
	public $post_content;
	public $html_title;
	public $meta_description;
	public $meta_keyword;
	public $seo_url;
	public $post_category_id;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$post_id='',$post_title='',$post_content='',$html_title='',$meta_description='',$meta_keyword='',$seo_url='',$post_category_id='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->post_id = $post_id;
		$this->post_title = $post_title;
		$this->post_content = $post_content;
		$this->html_title = $html_title;
		$this->meta_description = $meta_description;
		$this->meta_keyword = $meta_keyword;
		$this->seo_url = $seo_url;
		$this->post_category_id = $post_category_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_post_id($post_id)
	{
		$security = new security();
		$query="select *
		from post
		where post_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($post_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_seo_url($seo_url)
	{
		$security = new security();
		$query="select *
		from post
		where seo_url = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($seo_url);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_seo_url_and_id($seo_url,$id)
	{
		$security = new security();
		$query="select *
		from post
		where seo_url = ? and post_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		
		$stmt->bind_param('si',($security->xss_clean($seo_url)), ($security->xss_clean($id)));
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function post_all()
	{
		$query="select * 
		from post 
		order by post_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$post=new post($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$post;
		}
		return $data;
	}
	function post_by_category_id($post_category_id,$page_number,$count_row_posted)
	{
		$data=array();
		$security = new security();
		$query="select count(*) as count_row_all from post  where post_category_id = ? 
		order by post_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($post_category_id)));
		$stmt->execute();
		$stmt->bind_result($count_row_all);
		$stmt->fetch();
		if($count_row_all>0)
		{
			$data['pagination_data']=pagination_data($page_number,$count_row_posted,$count_row_all);
			$start=$data['pagination_data']['start'];
			$end=$data['pagination_data']['end'];
			$stmt=null;
			$query="select * from post where post_category_id = ? order by post_id desc limit ?,? ";
			$stmt = $this->conn->prepare($query);
			$stmt->bind_param('iii',($security->xss_clean($post_category_id)),($security->xss_clean($start)),($security->xss_clean($end)));
			$stmt->execute();

			$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data2=array();
			while($stmt->fetch())
			{
				$post=new post($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
				$data2[]=$post;
			}
			$data['data']=$data2;
		}
		return $data;
	}
}
?>