<?php
class citizenship
{
	public $citizenship_id;
	public $citizenship_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$citizenship_id='',$citizenship_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->citizenship_id = $citizenship_id;
		$this->citizenship_name = $citizenship_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_citizenship_id($citizenship_id)
	{
		$security = new security();
		$query="select *
		from citizenship
		where citizenship_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($citizenship_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($citizenship_id,$citizenship_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$citizenship_id,$citizenship_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function citizenship_all()
	{
		$query="select * 
		from citizenship 
		order by citizenship_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($citizenship_id,$citizenship_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$citizenship=new citizenship($this->conn,$citizenship_id,$citizenship_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$citizenship;
		}
		return $data;
	}
	function update($citizenship_name,$updated_by,$citizenship_id)
	{
		$security = new security();
		$query="update citizenship set citizenship_name=?,updated_date=?,updated_by=? where 
		citizenship_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($citizenship_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($citizenship_id)));
		$stmt->execute();
	}
	function insert($citizenship_name,$created_by)
	{
		$security = new security();
		$query="insert into citizenship(citizenship_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($citizenship_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($citizenship_id)
	{
		$security = new security();
		$query="delete from citizenship where citizenship_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($citizenship_id)));
		$stmt->execute();
	}
}
?>