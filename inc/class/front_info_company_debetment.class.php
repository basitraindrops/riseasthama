<?php
class front_info_company_debetment
{
	public $pd_id;
	public $fu_id;
	public $income_per_month;
	public $asset_value;
	public $ktp_file;
	public $kk_file;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $conn;
	function __construct($conn=''
	,$pd_id=''
	,$fu_id=''
	,$income_per_month=''
	,$asset_value=''
	,$ktp_file=''
	,$kk_file=''
	,$created_date=''
	,$created_by=''
	,$updated_by=''
	,$updated_date=''
	)
	{
		$this->conn = $conn;
		$this->pd_id=$pd_id;
		$this->fu_id= $fu_id;
		$this->income_per_month=$income_per_month;
		$this->asset_value= $asset_value;
		$this->ktp_file= $ktp_file;
		$this->kk_file= $kk_file;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_fu_id($fu_id)
	{
		$security = new security();
		$query="select *
		from front_info_company_debetment
		where fu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($fu_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$pd_id
			,$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$pd_id
			,$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		
	}
	function cd_all()
	{
		$query="select * 
		from front_info_company_debetment 
		order by fu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$pd_id
			,$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_debitur($this->conn
			,$pd_id
			,$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$updated_by
			,$pd_id

)
	{
		$security = new security();
		$query="update front_info_company_debetment set fu_id=?
			,income_per_month=?
			,asset_value=?
			,ktp_file=?
			,kk_file=?
			,updated_date=?
			,updated_by=?
		where 
		user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssi',($security->xss_clean($fu_id))
			,($security->xss_clean($income_per_month))
			,($security->xss_clean($asset_value))
			,($security->xss_clean($ktp_file))
			,($security->xss_clean($kk_file))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function insert(			
			$fu_id
			,$income_per_month
			,$asset_value
			,$ktp_file
			,$kk_file
			,$created_by
)
	{
		$security = new security();
		$query="insert into front_info_company_debetment(
			fu_id
			,income_per_month
			,asset_value
			,ktp_file
			,kk_file
			,created_date
			,created_by
		)values(?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssss'
			,($security->xss_clean($fu_id))
			,($security->xss_clean($income_per_month))
			,($security->xss_clean($asset_value))
			,($security->xss_clean($ktp_file))
			,($security->xss_clean($kk_file))
			,(date("Y-m-d H:i:s")),($security->xss_clean($created_by)))or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($ic_id)
	{
		$security = new security();
		$query="delete from front_info_company_debetment where ic_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($ic_id)));
		$stmt->execute();
	}
}
?>
