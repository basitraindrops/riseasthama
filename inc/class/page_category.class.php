<?php
class page_category
{
	public $page_category_id;
	public $page_category_name;
	public $seo_url;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$page_category_id='',$page_category_name='',$seo_url='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->page_category_id = $page_category_id;
		$this->page_category_name = $page_category_name;
		$this->seo_url = $seo_url;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_page_category_id($page_category_id)
	{
		$security = new security();
		$query="select *
		from page_category
		where page_category_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($page_category_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_slug($slug)
	{
		$security = new security();
		$query="select *
		from page_category
		where seo_url = ? limit 0,1";
//		echo $query;
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($slug);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result($page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function page_category_all()
	{
		$query="select * 
		from page_category 
		order by page_category_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$page_category=new page_category($this->conn,$page_category_id,$page_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$page_category;
		}
		return $data;
	}
}
?>