<?php
class badan_hukum
{
	public $bh_id;
	public $bh_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$bh_id='',$bh_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->bh_id = $bh_id;
		$this->bh_name = $bh_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_bh_id($bh_id)
	{
		$security = new security();
		$query="select *
		from front_badan_hukum
		where bh_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($bh_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($bh_id,$bh_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$bh_id,$bh_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function badan_hukum_all()
	{
		$query="select * 
		from front_badan_hukum 
		order by bh_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($bh_id,$bh_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$badan_hukum=new badan_hukum($this->conn,$bh_id,$bh_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$badan_hukum;
		}
		return $data;
	}
	function update($bh_name,$updated_by,$bh_id)
	{
		$security = new security();
		$query="update front_badan_hukum set bh_name=?,updated_date=?,updated_by=? where 
		bh_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($bh_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($bh_id)));
		$stmt->execute();
	}
	function insert($bh_name,$created_by)
	{
		$security = new security();
		$query="insert into front_badan_hukum(bh_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($bh_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($bh_id)
	{
		$security = new security();
		$query="delete from front_badan_hukum where bh_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($bh_id)));
		$stmt->execute();
	}
}
?>