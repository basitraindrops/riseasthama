<?php
class state
{
	public $state_id;
	public $state_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$state_id='',$state_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->state_id = $state_id;
		$this->state_name = $state_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_state_id($state_id)
	{
		$security = new security();
		$query="select *
		from state
		where state_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($state_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($state_id,$state_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$state_id,$state_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function state_all()
	{
		$query="select * 
		from state 
		order by state_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($state_id,$state_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$state=new state($this->conn,$state_id,$state_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$state;
		}
		return $data;
	}
	function update($state_name,$updated_by,$state_id)
	{
		$security = new security();
		$query="update state set state_name=?,updated_date=?,updated_by=? where 
		state_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($state_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($state_id)));
		$stmt->execute();
	}
	function insert($state_name,$created_by)
	{
		$security = new security();
		$query="insert into state(state_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($state_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($state_id)
	{
		$security = new security();
		$query="delete from state where state_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($state_id)));
		$stmt->execute();
	}
}
?>