<?php
class kabupaten
{
	public $kabupaten_id;
	public $kabupaten_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$kabupaten_id='',$province_id='',$kabupaten_name='',$created_date='',$created_by='',$updated_date="",$updated_by='',$province_name='')
	{
		$this->conn = $conn;
		$this->kabupaten_id = $kabupaten_id;
		$this->province_id=$province_id;
		$this->kabupaten_name = $kabupaten_name;
		$this->province_name = $province_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_kabupaten_id($kabupaten_id)
	{
		$security = new security();
		$query="select *
		from general_kabupaten
		where kabupaten_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($kabupaten_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_province_id($province_id)
	{
		$security = new security();
		$query="select *
		from general_kabupaten
		where province_id = ?";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($province_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$kabupaten=new kabupaten($this->conn,$kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$kabupaten;
		}
		return $data;
		
	}
	function kabupaten_all()
	{
		$query="select a.* ,b.province_name
		from general_kabupaten a left join general_province b on a.province_id = b.province_id
		order by a.kabupaten_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_date,$updated_by,$province_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$kabupaten=new kabupaten($this->conn,$kabupaten_id,$province_id,$kabupaten_name,$created_date,$created_by,$updated_date,$updated_by,$province_name);
			$data[]=$kabupaten;
		}
		return $data;
	}
	function update($province_id,$kabupaten_name,$updated_by,$kabupaten_id)
	{
		$security = new security();
		$query="update general_kabupaten set province_id=?,kabupaten_name=?,updated_date=?,updated_by=? where 
		kabupaten_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('isssi',($security->xss_clean($province_id)),($security->xss_clean($kabupaten_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($kabupaten_id)));
		$stmt->execute();
	}
	function insert($province_id,$kabupaten_name,$created_by)
	{
		$security = new security();
		$query="insert into general_kabupaten(province_id,kabupaten_name,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('isss',($security->xss_clean($province_id)),($security->xss_clean($kabupaten_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($kabupaten_id)
	{
		$security = new security();
		$query="delete from general_kabupaten where kabupaten_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($kabupaten_id)));
		$stmt->execute();
	}
}
?>