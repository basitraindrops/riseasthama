<?php
class years_experience
{
	public $years_experience_id;
	public $years_experience_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$years_experience_id='',$years_experience_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->years_experience_id = $years_experience_id;
		$this->years_experience_name = $years_experience_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_years_experience_id($years_experience_id)
	{
		$security = new security();
		$query="select *
		from years_experience
		where years_experience_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($years_experience_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($years_experience_id,$years_experience_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$years_experience_id,$years_experience_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function years_experience_all()
	{
		$query="select * 
		from years_experience 
		order by years_experience_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($years_experience_id,$years_experience_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$years_experience=new years_experience($this->conn,$years_experience_id,$years_experience_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$years_experience;
		}
		return $data;
	}
	function update($years_experience_name,$updated_by,$years_experience_id)
	{
		$security = new security();
		$query="update years_experience set years_experience_name=?,updated_date=?,updated_by=? where 
		years_experience_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($years_experience_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($years_experience_id)));
		$stmt->execute();
	}
	function insert($years_experience_name,$created_by)
	{
		$security = new security();
		$query="insert into years_experience(years_experience_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($years_experience_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($years_experience_id)
	{
		$security = new security();
		$query="delete from years_experience where years_experience_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($years_experience_id)));
		$stmt->execute();
	}
}
?>