<?php
class widget
{
	public $widget_id;
	public $widget_name;
	public $widget_content;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$widget_id='',$widget_name='',$widget_content='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->widget_id = $widget_id;
		$this->widget_name = $widget_name;
		$this->widget_content = $widget_content;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_widget_id($widget_id)
	{
		$security = new security();
		$query="select *
		from widget
		where widget_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($widget_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_widget_name($widget_id)
	{
		$security = new security();
		$query="select *
		from widget
		where widget_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($widget_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function widget_all()
	{
		$query="select * 
		from widget 
		order by widget_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$widget=new widget($this->conn,$widget_id,$widget_name,$widget_content,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$widget;
		}
		return $data;
	}
	function update($widget_name,$widget_content,$updated_by,$widget_id)
	{
		$security = new security();
		$query="update widget set widget_name=?, widget_content=?,updated_date=?,updated_by=? where 
		widget_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssi',($security->xss_clean($widget_name)),($security->xss_clean($widget_content)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($widget_id)));
		$stmt->execute();
	}
	function insert($widget_name,$widget_content,$created_by)
	{
		$security = new security();
		$query="insert into widget(widget_name,widget_content,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($widget_name)),($security->xss_clean($widget_content)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($widget_id)
	{
		$security = new security();
		$query="delete from widget where widget_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($widget_id)));
		$stmt->execute();
	}
}
?>