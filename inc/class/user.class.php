<?php
class user
{
	public $user_id;
	public $user_name;
	public $user_email;
	private $user_password;
	public $user_first_name;
	public $user_last_name;
	public $user_last_login;
	public $user_group_id;
	public $user_birth_place;
	public $user_birth_date;
	public $user_address;
	public $user_ktp_number;
	public $user_phone_number;
	public $user_ktp_file;
	public $user_photo_file;
	public $user_pendidikan_terakhir;
	public $user_gender;
	public $user_marital_status;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	private $conn;
	function __construct($conn=''
	,$user_id=''
	,$user_name=''
	,$user_email=''
	,$user_password=''
	,$user_first_name=''
	,$user_last_name=''
	,$user_last_login=''
	,$user_group_id=''
	,$user_birth_place=''
	,$user_birth_date=''
	,$user_address=''
	,$user_ktp_number=''
	,$user_phone_number=''
	,$user_ktp_file=''
	,$user_photo_file=''
	,$user_pendidikan_terakhir=''
	,$user_gender=''
	,$user_marital_status=''
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by='')
	{
		$this->conn = $conn;
		$this->user_id = $user_id;
		$this->user_name=$user_name;
		$this->user_email = $user_email;
		$this->user_password=$user_password;
		$this->user_first_name=$user_first_name;
		$this->user_last_name=$user_last_name;
		$this->user_last_login=$user_last_login;
		$this->user_group_id=$user_group_id;
		$this->user_birth_place=$user_birth_place;
		$this->user_birth_date=$user_birth_date;
		$this->user_address=$user_address;
		$this->user_ktp_number=$user_ktp_number;
		$this->user_phone_number=$user_phone_number;
		$this->user_ktp_file=$user_ktp_file;
		$this->user_photo_file=$user_photo_file;
		$this->user_pendidikan_terakhir=$user_pendidikan_terakhir;
		$this->user_gender=$user_gender;
		$this->user_marital_status=$user_marital_status;
		$this->user_group_id=$user_group_id;
		$this->created_date=$created_date;
		$this->created_by=$created_by;
		$this->updated_date=$updated_date;
		$this->updated_by=$updated_by;
	}
	function loginby_email_password($email,$password)
	{
		$query="select *
		from user where user_email = ? or user_name = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$stmt->bind_param('ss', ($security->xss_clean($email)),($security->xss_clean($email)));
		$stmt->execute();

		$stmt->bind_result(
				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_by
				,$created_date
				,$updated_by
				,$updated_date
			);
		$stmt->fetch();
		$encryptObj = new encryption();
		//echo $password;
		if($encryptObj->validate_password($password, $user_password))
		{
			$this->__construct($this->conn=''
				,$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
				);
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_by_user_id($user_id)
	{
		$query="select * from user where user_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $user_id);
		$stmt->execute();

		$stmt->bind_result(				
			$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		
	}
	function get_by_email($email)
	{
		$query="select * from user where user_email = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('s', $email);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,				
				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		
	}
	function get_by_user_name($username)
	{
		$query="select * from user where user_name = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('s', $username);
		$stmt->execute();

		$stmt->bind_result(				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();			$this->__construct($this->conn,				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
	}
	function user_all()
	{
		$query="select *
		from user 
		order by user_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
							$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$user= new user($this->conn,
				$user_id
				,$user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by

			);
			$data[]=$user;
		}
		return $data;
	}
	function insert($user_name
				,$user_email
				,$user_password
				,$user_first_name
				,$user_last_name
				,$user_last_login
				,$user_group_id
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$created_by
	)
	{
		$security = new security();
		$query="insert into user
		(
					user_name
					,user_email
					,user_password
					,user_first_name
					,user_last_name
					,user_last_login
					,user_group_id
					,user_birth_place
					,user_birth_date
					,user_address
					,user_ktp_number
					,user_phone_number
					,user_ktp_file
					,user_photo_file
					,user_pendidikan_terakhir
					,user_gender
					,user_marital_status
					,created_date
					,created_by
		)
		values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssssssssssssss'
					,($security->xss_clean($user_name))
					,($security->xss_clean($user_email))
					,($security->xss_clean($user_password))
					,($security->xss_clean($user_first_name))
					,($security->xss_clean($user_last_name))
					,($security->xss_clean($user_last_login))
					,($security->xss_clean($user_group_id))
					,($security->xss_clean($user_birth_place))
					,($security->xss_clean($user_birth_date))
					,($security->xss_clean($user_address))
					,($security->xss_clean($user_ktp_number))
					,($security->xss_clean($user_phone_number))
					,($security->xss_clean($user_ktp_file))
					,($security->xss_clean($user_photo_file))
					,($security->xss_clean($user_pendidikan_terakhir))
					,($security->xss_clean($user_gender))
					,($security->xss_clean($user_marital_status))
					,(date("Y-m-d H:i:s"))
					,($security->xss_clean($created_by)));
		$stmt->execute();

	}
	function update($user_name
				,$user_email
				,$user_first_name
				,$user_last_name
				,$user_birth_place
				,$user_birth_date
				,$user_address
				,$user_ktp_number
				,$user_phone_number
				,$user_ktp_file
				,$user_photo_file
				,$user_pendidikan_terakhir
				,$user_gender
				,$user_marital_status
				,$updated_by
				,$user_id
	)
	{
		$security = new security();
		$query="update user
				set	user_name=?
					,user_email=?
					,user_first_name=?
					,user_last_name=?
					,user_birth_place=?
					,user_birth_date=?
					,user_address=?
					,user_ktp_number=?
					,user_phone_number=?
					,user_ktp_file=?
					,user_photo_file=?
					,user_pendidikan_terakhir=?
					,user_gender=?
					,user_marital_status=?
					,updated_date=?
					,updated_by=?
					where user_id=?
";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssssssi'
					,($security->xss_clean($user_name))
					,($security->xss_clean($user_email))
					,($security->xss_clean($user_first_name))
					,($security->xss_clean($user_last_name))
					,($security->xss_clean($user_birth_place))
					,($security->xss_clean($user_birth_date))
					,($security->xss_clean($user_address))
					,($security->xss_clean($user_ktp_number))
					,($security->xss_clean($user_phone_number))
					,($security->xss_clean($user_ktp_file))
					,($security->xss_clean($user_photo_file))
					,($security->xss_clean($user_pendidikan_terakhir))
					,($security->xss_clean($user_gender))
					,($security->xss_clean($user_marital_status))
					,(date("Y-m-d H:i:s"))
					,($security->xss_clean($updated_by))
					,($security->xss_clean($user_id))
						);
		$stmt->execute();

	}
	function update_password($new_password,$user_name,$user_id)
	{
		$security = new security();
		$query="update user set user_password=?,updated_date=?,updated_by=? where user_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($new_password)),(date("Y-m-d H:i:s")),($security->xss_clean($user_name)),($security->xss_clean($user_id)));
		$stmt->execute();
	}

	function update_group($user_group_id,$user_name,$user_id)
	{
		$security = new security();
//		echo $user_group_id.$user_name.$user_id;
		$query="update user set user_group_id=?,updated_date=?,updated_by=? where user_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('issi',($security->xss_clean($user_group_id)),(date("Y-m-d H:i:s")),($security->xss_clean($user_name)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function logout($email)
	{	
		$query="update user set user_last_login=date('Y-m-d H:i:s') where user_email = '$email'";
		$stmt = $this->conn->prepare($query);
		//$stmt->bind_param('is', date("Y-m-d H:i:s"),$email);
		$query = $stmt->execute();
	}
	function delete($user_id)
	{
		$security = new security();
		$query="delete from user where user_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', ($security->xss_clean($user_id)));
		$stmt->execute();
	}
}
?>