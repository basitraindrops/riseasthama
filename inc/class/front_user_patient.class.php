<?php
class front_user_patient
{
	public $user_id;
	public $user_nama;
	public $user_email;
	public $user_name;
	public $user_address;
	public $province_id;
	public $kota_id;
	public $no_telp;
	public $no_hp;
	public $no_npwp;
	public $no_ktp_sim;
	public $tempat_lahir;
	public $tanggal_lahir;
	public $jenis_kelamin;
	public $pendidikan;
	public $pekerjaan;
	public $nama_perusahaan;
	public $jabatan;
	public $user_password;
	public $forgot_password_key;
	public $forgot_status;
        public $payment_register_type;
        public $age;
        public $country;
	public $status;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $user_role;	
	public $asthma_time;	
	public $allergy;	
	public $conn;
	public $profile_pic;

	function __construct($conn=''
	,$user_id=''
	,$user_nama=''
	,$user_email=''
	,$user_name=''
	,$user_address=''
	,$province_id=''
	,$kota_id=''
	,$no_telp=''
	,$no_hp=''
	,$no_npwp=''
	,$no_ktp_sim=''
	,$tempat_lahir=''
	,$tanggal_lahir=''
	,$jenis_kelamin=''
	,$pendidikan=''
	,$pekerjaan=''
	,$nama_perusahaan=''
	,$jabatan=''
	,$user_password=''
	,$forgot_password_key=''
	,$forgot_status=''
        ,$payment_register_type=''
        ,$age=''
        ,$country=''
	,$status=''
	,$created_date=''
	,$created_by=''
	,$updated_by=''
	,$updated_date=''
	,$profile_pic=''
	,$user_role=''
	,$asthma_time=''
	,$allergy=''
	)
	{
		$this->conn = $conn;
		$this->user_id=$user_id;
		$this->user_nama= $user_nama;
		$this->user_email=$user_email;
		$this->user_name= $user_name;
		$this->user_address= $user_address;
		$this->province_id=$province_id;
		$this->kota_id= $kota_id;
		$this->no_telp=$no_telp;
		$this->no_hp=$no_hp;
		$this->no_npwp=$no_npwp;
		$this->no_ktp_sim=$no_ktp_sim;
		$this->tempat_lahir=$tempat_lahir;
		$this->tanggal_lahir=$tanggal_lahir;
		$this->jenis_kelamin=$jenis_kelamin;
		$this->pendidikan=$pendidikan;
		$this->pekerjaan=$pekerjaan;
		$this->nama_perusahaan=$nama_perusahaan;
		$this->jabatan=$jabatan;
		$this->user_password=$user_password;
		$this->forgot_password_key=$forgot_password_key;
		$this->forgot_status=$forgot_status;
			$this->payment_register_type=$payment_register_type;
			$this->age=$age;
			$this->country=$country;
		$this->status=$status;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
		$this->user_role = $user_role;
		$this->profile_pic = $profile_pic;
		$this->asthma_time = $asthma_time;
		$this->allergy = $allergy;
	}
	function loginby_email_password($email,$password)
	{	
		
//		echo $email." ".$password;
		$query="select *
		from front_user_patient where user_email = ? or user_name = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$email1 = ($security->xss_clean($email));
		$stmt->bind_param('ss', $email1,$email1);
		$stmt->execute();

	
		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status

			,$payment_register_type
			,$age
			,$country
			
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
			);
		$stmt->fetch();

		$encryptObj = new encryption();

		$payment_register_type = '';
		$age = '';
		$country = '';

		if($encryptObj->validate_password($password, $user_password))
		{
			$this->__construct($this->conn=''
				,$user_id
				,$user_nama
				,$user_email
				,$user_name
				,$user_address
				,$province_id
				,$kota_id
				,$no_telp
				,$no_hp
				,$no_npwp
				,$no_ktp_sim
				,$tempat_lahir
				,$tanggal_lahir
				,$jenis_kelamin
				,$pendidikan
				,$pekerjaan
				,$nama_perusahaan
				,$jabatan
				,$user_password
				,$forgot_password_key
				,$forgot_status
					,$payment_register_type
					,$age
					,$country
				,$status
				,$created_date
				,$created_by
				,$updated_by
				,$updated_date
				,$profile_pic
				,$user_role
				,$asthma_time
				,$allergy
				);
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_by_user_id($title_id)
	{
		$security = new security();
		$query="select *
		from front_user_patient
		where user_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($title_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
				,$payment_register_type
				,$age
				,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		//$stmt->fetch();
		if($stmt->fetch()){
		$this->__construct($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
				,$payment_register_type
				,$age
				,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
			);
			return true;
		}else{
			return true;
		}
		
	}
	function get_by_user_email($user_email)
	{
		$security = new security();
		$query="select *
		from front_user_patient
		where user_email = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($user_email);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
			);
		
	}
	function get_by_user_email_key($user_email,$key)
	{
		$security = new security();
		$query="select *
		from front_user_patient
		where user_email = ? and forgot_password_key = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($user_email);
		$key=$security->xss_clean($key);
		$stmt->bind_param('ss', $id,$key);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
			);
		
	}
        function title_all()
	{
		$query="select * 
		from front_user_patient 
		order by user_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_patient($this->conn
			,$user_id
			,$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$profile_pic
			,$user_role
			,$asthma_time
			,$allergy
			);
			$data[]=$title;
		}
		return $data;
	}



	function updateMail($user_email){		
		$query="select user_email from front_user_patient where user_email = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$stmt->bind_param('s', ($security->xss_clean($user_email)));
		$stmt->execute() or die(mysqli_error($this->conn));
		$stmt->bind_result($user_emailf);	
		$stmt->fetch();

		if(is_null($user_emailf)){
			$this->user_email = $user_email;
			return true;
		}else{
			return false;
		}

	}

	function save(){

		$this->update(
			$this->user_nama
			,$this->user_email
			,$this->user_name
			,$this->user_address
			,$this->province_id
			,$this->kota_id
			,$this->no_telp
			,$this->no_hp
			,$this->no_npwp
			,$this->no_ktp_sim
			,$this->tempat_lahir
			,$this->tanggal_lahir
			,$this->jenis_kelamin
			,$this->pendidikan
			,$this->pekerjaan
			,$this->nama_perusahaan
			,$this->jabatan
			,$this->user_password
			,$this->forgot_password_key
			,$this->forgot_status
                        ,$this->payment_register_type
                        ,$this->age
                        ,$this->country
			,$this->status
			,$this->created_date
			,$this->created_by
			,$this->updated_by
			,date("Y-m-d H:i:s")
			,$this->user_id
			,$this->profile_pic);

		return true;

	}

	function update(			
			$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			,$user_id
			,$profile_pic = ''
			
)
	{
		$security = new security();
		$query="update front_user_patient set user_nama=?
			,user_email=?
			,user_name=?
			,user_address=?
			,province_id=?
			,kota_id=?
			,no_telp=?
			,no_hp=?
			,no_npwp=?
			,no_ktp_sim=?
			,tempat_lahir=?
			,tanggal_lahir=?
			,jenis_kelamin=?
			,pendidikan=?
			,pekerjaan=?
			,nama_perusahaan=?
			,jabatan=?
			,user_password=?
			,forgot_password_key=?
			,forgot_status=?
                        ,payment_register_type=?
                        ,age=?
                        ,country=?
			,status=?
			,updated_date=?
			,updated_by=?
			,profile_pic=?
		where 
		user_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('sssssssssssssssssssssssssssi',($security->xss_clean($user_nama))
			,($security->xss_clean($user_email))
			,($security->xss_clean($user_name))
			,($security->xss_clean($user_address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_telp))
			,($security->xss_clean($no_hp))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_ktp_sim))
			,($security->xss_clean($tempat_lahir))
			,($security->xss_clean($tanggal_lahir))
			,($security->xss_clean($jenis_kelamin))
			,($security->xss_clean($pendidikan))
			,($security->xss_clean($pekerjaan))
			,($security->xss_clean($nama_perusahaan))
			,($security->xss_clean($jabatan))
			,($security->xss_clean($user_password))
			,($security->xss_clean($forgot_password_key))
			,($security->xss_clean($forgot_status))
                        ,($security->xss_clean($payment_register_type))
                        ,($security->xss_clean($age))
                        ,($security->xss_clean($country))
			,($security->xss_clean($status))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($updated_by))
			,$profile_pic
			,($security->xss_clean($user_id))
			
		)or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	
	}
	function insert(			
			$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_by
			,$user_role
			,$asthma_time
			,$allergy
)
	{


		//demo
	
		// prepare and bind

		//user_id,user_nama,user_email,user_name

		//user_address,no_telp,no_hp,no_npwp,no_ktp_sim,tempat_lahir

/*
$stmt = $this->conn->prepare("INSERT INTO aaa_front_user_patient (	
user_nama,
user_email,
user_name,
user_address,
province_id,
kota_id,
no_telp,
no_hp,
no_npwp,
no_ktp_sim,
tempat_lahir,
tanggal_lahir,
jenis_kelamin,
pendidikan,
pekerjaan,
nama_perusahaan,
jabatan,
user_password,
forgot_password_key,
forgot_status,
status,
created_date,
created_by,
updated_date,
updated_by) 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

		$id = 1;
		var_dump($stmt);
		echo '<br><br>';

		$d = date("Y-m-d H:i:s");

		$stmt->bind_param("sssssssssssssssssssssssss",
			$firstname,
			$lastname,
			$email,
			$user_address,	
			$id,		
			$id,
			$lastname,
			$email,
			$user_address,	
			$no_telp,
			$firstname,
			$lastname,
			$email,
			$id,	
			$id,
			$firstname,
			$lastname,
			$email,
			$user_address,	
			$id,
			$id,

			$d,
			$email,
			$d,	
			$no_telp
			);

		// set parameters and execute
		$firstname = "John";
		$lastname = "Doe";
		$email = "johnexample.com";
		$user_address = "adsa";
		$no_telp = 1;

		$id = 1;
		$stmt->execute() or die(mysqli_error($this->conn));
		exit('demo end');*/
		
		$security = new security();
		$query="insert into front_user_patient(
			user_nama
			,user_email
			,user_name
			,user_address
			,province_id
			,kota_id
			,no_telp
			,no_hp
			,no_npwp
			,no_ktp_sim
			,tempat_lahir
			,tanggal_lahir
			,jenis_kelamin
			,pendidikan
			,pekerjaan
			,nama_perusahaan
			,jabatan
			,user_password
			,forgot_password_key
			,forgot_status
			,age          
			,status
			,created_date
			,created_by
			,user_role
			,asthma_time
			,allergy
		)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssssssssssssssssssssssss',
			($security->xss_clean($user_nama))
			,($security->xss_clean($user_email))
			,($security->xss_clean($user_name))
			,($security->xss_clean($user_address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_telp))
			,($security->xss_clean($no_hp))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_ktp_sim))
			,($security->xss_clean($tempat_lahir))
			,($security->xss_clean($tanggal_lahir))
			,($security->xss_clean($jenis_kelamin))
			,($security->xss_clean($pendidikan))
			,($security->xss_clean($pekerjaan))
			,($security->xss_clean($nama_perusahaan))
			,($security->xss_clean($jabatan))
			,($security->xss_clean($user_password))
			,($security->xss_clean($forgot_password_key))
			,($security->xss_clean($forgot_status))	
			,($security->xss_clean($age))			
			,($security->xss_clean($status))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($created_by))
			,($security->xss_clean($user_role))
			,($security->xss_clean($asthma_time))
			,($security->xss_clean($allergy))

			)or die("error insert user".mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));		
	}
	function update_password($new_password,$user_name,$user_id)
	{
		$security = new security();
		$query="update front_user_patient set user_password=?,updated_date=?,updated_by=? where user_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($new_password)),(date("Y-m-d H:i:s")),($security->xss_clean($user_name)),($security->xss_clean($user_id)))or die(mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($user_id)
	{
		$security = new security();
		$query="delete from front_user_patient where user_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($user_id)));
		$stmt->execute();
	}

	function insert_onepage_user(			
			$user_nama
			,$user_email
			,$user_name
			,$user_address
			,$province_id
			,$kota_id
			,$no_telp
			,$no_hp
			,$no_npwp
			,$no_ktp_sim
			,$tempat_lahir
			,$tanggal_lahir
			,$jenis_kelamin
			,$pendidikan
			,$pekerjaan
			,$nama_perusahaan
			,$jabatan
			,$user_password
			,$forgot_password_key
			,$forgot_status
                        ,$payment_register_type
                        ,$age
                        ,$country
			,$status
			,$created_by
			,$user_role
		)
		{


		$security = new security();
		$query="insert into one_user(
			user_nama
			,user_email
			,user_name
			,user_address
			,province_id
			,kota_id
			,no_telp
			,no_hp
			,no_npwp
			,no_ktp_sim
			,tempat_lahir
			,tanggal_lahir
			,jenis_kelamin
			,pendidikan
			,pekerjaan
			,nama_perusahaan
			,jabatan
			,user_password
			,forgot_password_key
			,forgot_status           
			,status
			,created_date
			,created_by
			,user_role
		)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssssssssssssss',
			($security->xss_clean($user_nama))
			,($security->xss_clean($user_email))
			,($security->xss_clean($user_name))
			,($security->xss_clean($user_address))
			,($security->xss_clean($province_id))
			,($security->xss_clean($kota_id))
			,($security->xss_clean($no_telp))
			,($security->xss_clean($no_hp))
			,($security->xss_clean($no_npwp))
			,($security->xss_clean($no_ktp_sim))
			,($security->xss_clean($tempat_lahir))
			,($security->xss_clean($tanggal_lahir))
			,($security->xss_clean($jenis_kelamin))
			,($security->xss_clean($pendidikan))
			,($security->xss_clean($pekerjaan))
			,($security->xss_clean($nama_perusahaan))
			,($security->xss_clean($jabatan))
			,($security->xss_clean($user_password))
			,($security->xss_clean($forgot_password_key))
			,($security->xss_clean($forgot_status))		
			,($security->xss_clean($status))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($created_by))
			,($security->xss_clean($user_role))

			)or die("error insert user".mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));		
	}
}
?>
