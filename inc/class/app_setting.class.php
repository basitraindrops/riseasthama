<?php
class settings
{
	public $settings_id;
	public $description;
	public $meta_title;
	public $meta_description;
	public $meta_keyword;
	public $admin_email;
	public $no_reply_email;
	public $cs_email_1;
	public $cs_email_2;
	public $cs_email_3;
	public $cs_email_4;
	public $author;
	public $publisher;
	public $time_zone;
	public $primary_address;
	public $primary_phone;
	public $primary_mobile_phone;
	public $facebook;
	public $twitter;
	public $instagram;
	public $linked_in;
	public $google_plus;
	public $salt_key;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn=''
	,$settings_id=''
	,$description=''
	,$meta_title=''
	,$meta_description=''
	,$meta_keyword=''
	,$admin_email=''
	,$no_reply_email=""
	,$cs_email_1=""
	,$cs_email_2=""
	,$cs_email_3=""
	,$cs_email_4=""
	,$author=""
	,$publisher=""
	,$time_zone=""
	,$primary_address=""
	,$primary_phone=""
	,$primary_mobile_phone=""
	,$facebook=""
	,$twitter=""
	,$instagram=""
	,$linked_in=""
	,$google_plus=""
	,$salt_key=""
	,$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->settings_id = $settings_id;
		$this->description=$description;
		$this->meta_title = $meta_title;
		$this->meta_description = $meta_description;
		$this->meta_keyword = $meta_keyword;
		$this->admin_email = $admin_email;
		$this->no_reply_email=$no_reply_email;
		$this->cs_email_1=$cs_email_1;
		$this->cs_email_2=$cs_email_2;
		$this->cs_email_3=$cs_email_3;
		$this->cs_email_4=$cs_email_4;
		$this->author=$author;
		$this->publisher=$publisher;
		$this->time_zone=$time_zone;
		$this->primary_address=$primary_address;
		$this->primary_phone=$primary_phone;
		$this->primary_mobile_phone=$primary_mobile_phone;
		$this->facebook=$facebook;
		$this->twitter=$twitter;
		$this->instagram=$instagram;
		$this->linked_in=$linked_in;
		$this->google_plus=$google_plus;
		$this->salt_key=$salt_key;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function settings_all()
	{
		$security = new security();
		$query="select *
		from settings
		";
		$stmt = $this->conn->prepare($query);
		$stmt->execute() or die("error execute sql!");

		$stmt->bind_result($settings_id
			,$description
			,$meta_title
			,$meta_description
			,$meta_keyword
			,$admin_email
			,$no_reply_email
			,$cs_email_1
			,$cs_email_2
			,$cs_email_3
			,$cs_email_4
			,$author
			,$publisher
			,$time_zone
			,$primary_address
			,$primary_phone
			,$primary_mobile_phone
			,$facebook
			,$twitter
			,$instagram
			,$linked_in
			,$google_plus
			,$salt_key
			,$created_date,$created_by,$updated_date,$updated_by) or die("error binding sql");
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$s= new settings($this->conn,$settings_id
			,$description
			,$meta_title
			,$meta_description
			,$meta_keyword
			,$admin_email
			,$no_reply_email
			,$cs_email_1
			,$cs_email_2
			,$cs_email_3
			,$cs_email_4
			,$author
			,$publisher
			,$time_zone
			,$primary_address
			,$primary_phone
			,$primary_mobile_phone
			,$facebook
			,$twitter
			,$instagram
			,$linked_in
			,$google_plus
			,$salt_key
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by);
			$data[]=$s;
//			echo print_r($s);
		}

		return $data;
	}
	function get_by_id($settings_id)
	{
	
		$security = new security();
		$query="select *
		from settings
		where settings_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($settings_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($settings_id
			,$description
			,$meta_title
			,$meta_description
			,$meta_keyword
			,$admin_email
			,$no_reply_email
			,$cs_email_1
			,$cs_email_2
			,$cs_email_3
			,$cs_email_4
			,$author
			,$publisher
			,$time_zone
			,$primary_address
			,$primary_phone
			,$primary_mobile_phone
			,$facebook
			,$twitter
			,$instagram
			,$linked_in
			,$google_plus
			,$salt_key
			,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$settings_id
			,$description
			,$meta_title
			,$meta_description
			,$meta_keyword
			,$admin_email
			,$no_reply_email
			,$cs_email_1
			,$cs_email_2
			,$cs_email_3
			,$cs_email_4
			,$author
			,$publisher
			,$time_zone
			,$primary_address
			,$primary_phone
			,$primary_mobile_phone
			,$facebook
			,$twitter
			,$instagram
			,$linked_in
			,$google_plus
			,$salt_key
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by);
		
	}
	function update($description
		,$meta_title
		,$meta_description
		,$meta_keyword
		,$admin_email
		,$no_reply_email
		,$cs_email_1
		,$cs_email_2
		,$cs_email_3
		,$cs_email_4
		,$author
		,$publisher
		,$time_zone
		,$primary_address
		,$primary_phone
		,$primary_mobile_phone
		,$facebook
		,$twitter
		,$instagram
		,$linked_in
		,$google_plus
		,$salt_key
		,$updated_by,$settings_id)
	{
		$security = new security();
		$query="update settings set description=?,meta_title=?
		,meta_description=?
		,meta_keyword=?
		,admin_email=?
		,no_reply_email=?
		,cs_email_1=?
		,cs_email_2=?
		,cs_email_3=?
		,cs_email_4=?
		,author=?
		,publisher=?
		,time_zone=?
		,primary_address=?
		,primary_phone=?
		,primary_mobile_phone=?
		,facebook=?
		,twitter=?
		,instagram=?
		,linked_in=?
		,google_plus=?
		,salt_key=?,updated_date=?,updated_by=? where 
		settings_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssssssssssssssssssssssssi'
			,($security->xss_clean($description))
			,($security->xss_clean($meta_title))
			,($security->xss_clean($meta_description))
			,($security->xss_clean($meta_keyword))
			,($security->xss_clean($admin_email))
			,($security->xss_clean($no_reply_email))
			,($security->xss_clean($cs_email_1))
			,($security->xss_clean($cs_email_2))
			,($security->xss_clean($cs_email_3))
			,($security->xss_clean($cs_email_4))
			,($security->xss_clean($author))
			,($security->xss_clean($publisher))
			,($security->xss_clean($time_zone))
			,($security->xss_clean($primary_address))
			,($security->xss_clean($primary_phone))
			,($security->xss_clean($primary_mobile_phone))
			,($security->xss_clean($facebook))
			,($security->xss_clean($twitter))
			,($security->xss_clean($instagram))
			,($security->xss_clean($linked_in))
			,($security->xss_clean($google_plus))
			,($security->xss_clean($salt_key))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($updated_by)),($security->xss_clean($settings_id))) or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function insert($description,$meta_title
		,$meta_description
		,$meta_keyword
		,$admin_email
		,$no_reply_email
		,$cs_email_1
		,$cs_email_2
		,$cs_email_3
		,$cs_email_4
		,$author
		,$publisher
		,$time_zone
		,$primary_address
		,$primary_phone
		,$primary_mobile_phone
		,$facebook
		,$twitter
		,$instagram
		,$linked_in
		,$google_plus
		,$salt_key
		,$created_by)
	{
		$security = new security();
		$query="insert into settings (description,meta_title
		,meta_description
		,meta_keyword
		,admin_email
		,no_reply_email
		,cs_email_1
		,cs_email_2
		,cs_email_3
		,cs_email_4
		,author
		,publisher
		,time_zone
		,primary_address
		,primary_phone
		,primary_mobile_phone
		,facebook
		,twitter
		,instagram
		,linked_in
		,google_plus
		,salt_key,created_date,created_by)
		values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query)or die(mysqli_error($this->conn));
		$stmt->bind_param('ssssssssssssssssssssssss'
			,($security->xss_clean($description))
			,($security->xss_clean($meta_title))
			,($security->xss_clean($meta_description))
			,($security->xss_clean($meta_keyword))
			,($security->xss_clean($admin_email))
			,($security->xss_clean($no_reply_email))
			,($security->xss_clean($cs_email_1))
			,($security->xss_clean($cs_email_2))
			,($security->xss_clean($cs_email_3))
			,($security->xss_clean($cs_email_4))
			,($security->xss_clean($author))
			,($security->xss_clean($publisher))
			,($security->xss_clean($time_zone))
			,($security->xss_clean($primary_address))
			,($security->xss_clean($primary_phone))
			,($security->xss_clean($primary_mobile_phone))
			,($security->xss_clean($facebook))
			,($security->xss_clean($twitter))
			,($security->xss_clean($instagram))
			,($security->xss_clean($linked_in))
			,($security->xss_clean($google_plus))
			,($security->xss_clean($salt_key))
			,(date("Y-m-d H:i:s"))
			,($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($settings_id)
	{
		$security = new security();
		$query="delete from settings where settings_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($settings_id)));
		$stmt->execute();
	}
}
?>