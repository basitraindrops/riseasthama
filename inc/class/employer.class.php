<?php
class employer
{
	public $employer_id;
	public $logo;
	public $company_name;
	public $address;
	public $state;
	public $post_code;
	public $country;
	public $company_type;
	public $website;
	public $twitter;
	public $linked_in_page;
	public $contact_name;
	public $email_address;
	public $phone_number;
	public $password;
	public $profile_picture;
	public $img_src;
	public $confirm_email_address_code;
	public $email_address_confirmed;
	public $reset_password_code;
	public $reset_password_done;
	public $is_registered;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn=''
	,$employer_id=''
	,$logo=''
	,$company_name=''
	,$address=''
	,$state=''
	,$post_code=''
	,$country=''
	,$company_type=''
	,$website=''
	,$twitter=''
	,$linked_in_page=''
	,$contact_name=''
	,$email_address=''
	,$phone_number=''
	,$password=''
	,$profile_picture=''
	,$img_src=''
	,$confirm_email_address_code=''
	,$email_address_confirmed=''
	,$reset_password_code=''
	,$reset_password_done=''
	,$is_registered=''
	,$created_date=''
	,$created_by=''
	,$updated_date=""
	,$updated_by=''
	)
	{
		$this->conn = $conn;
		$this->employer_id = $employer_id;
		$this->logo = $logo;
		$this->company_name=$company_name;
		$this->address=$address;
		$this->state=$state;
		$this->post_code=$post_code;
		$this->country=$country;
		$this->company_type=$company_type;
		$this->website=$website;
		$this->twitter=$twitter;
		$this->linked_in_page=$linked_in_page;
		$this->contact_name=$contact_name;
		$this->email_address=$email_address;
		$this->phone_number=$phone_number;
		$this->password=$password;
		$this->profile_picture=$profile_picture;
		$this->img_src=$img_src;
		$this->confirm_email_address_code = $confirm_email_address_code;
		$this->email_address_confirmed=$email_address_confirmed;
		$this->reset_password_code=$reset_password_code;
		$this->reset_password_done=$reset_password_done;
		$this->is_registered=$is_registered;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function loginby_email_password($email,$password)
	{
		$query="select *
		from employer where email_address = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$stmt->bind_param('s',($security->xss_clean($email)));
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$user_password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		$stmt->fetch();
		$encryptObj = new encryption();
		//echo $password;
		if($encryptObj->validate_password($password, $user_password))
		{
//		echo $email_address." ".$password." ".$user_password;
			$this->__construct($this->conn
				,$employer_id
				,$logo
				,$company_name
				,$address
				,$state
				,$post_code
				,$country
				,$company_type
				,$website
				,$twitter
				,$linked_in_page
				,$contact_name
				,$email_address
				,$phone_number
				,$user_password
				,$profile_picture
				,$img_src
				,$confirm_email_address_code
				,$email_address_confirmed
				,$reset_password_code
				,$reset_password_done
				,$is_registered
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_by_employer_id($employer_id)
	{
		$security = new security();
		$query="select *
		from employer
		where employer_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($employer_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		
	}
	function get_by_employer_email($employer_email)
	{
		$security = new security();
		$query="select *
		from employer
		where email_address = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($employer_email);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		
	}
	function check_employer_email_exist($email_address)
	{
		$security = new security();
		$query="select *
		from employer
		where email_address = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($email_address);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();

		if($contact_name!="" && $contact_name!=null)
			return true;
		else
			return false;
		
	}
	function check_employer_phone_exist($phone_number)
	{
		$security = new security();
		$query="select *
		from employer
		where phone_number = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($phone_number);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();

		if($contact_name!="" && $contact_name!=null)
			return true;
		else
			return false;
		
	}
	function get_by_confirmed_email($confirmed_code)
	{
		$security = new security();
		$query="select *
		from employer
		where confirm_email_address_code = ? and  email_address_confirmed=0 limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($confirmed_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		if($contact_name!="")
			return true;
		else
			return false;
	}
	function get_by_reset_password($reset_password_code)
	{
		$security = new security();
		$query="select *
		from employer
		where reset_password_code = ? and  reset_password_done=0 limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($reset_password_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		if($contact_name!="")
			return true;
		else
			return false;
	}
	function activate_email($confirm_email_address_code)
	{
		$security = new security();
		$query="update employer set email_address_confirmed=1,updated_date=?,updated_by=? where 
		confirm_email_address_code=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($confirm_email_address_code)));
		$stmt->execute();

		$query="select *
		from employer
		where confirm_email_address_code = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($confirm_email_address_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$employer_id
			,$logo
			,$company_name
			,$address
			,$state
			,$post_code
			,$country
			,$company_type
			,$website
			,$twitter
			,$linked_in_page
			,$contact_name
			,$email_address
			,$phone_number
			,$password
			,$profile_picture
			,$img_src
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		

	}
	function resend_activation_email($email_address,$confirm_email_address_code)
	{
		$security = new security();
		$query="update employer set email_address_confirmed=0,confirm_email_address_code=?,updated_date=?,updated_by=? where 
		 email_address=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($confirm_email_address_code)),(date("Y-m-d H:i:s")),($security->xss_clean($email_address)),($security->xss_clean($email_address)));
		$stmt->execute();
	}
	function update_contact($contact_name,$phone_number,$updated_by,$employer_id)
	{
		$security = new security();
		$query="update employer set contact_name=?,phone_number=?,updated_date=?,updated_by=? where 
		employer_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssi',($security->xss_clean($contact_name)),($security->xss_clean($phone_number)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($employer_id)));
		$stmt->execute();
	}
	function update_company($logo,$company_name,$address,$state,$post_code,$country,$company_type,$website,$twitter,$linked_in_page,$updated_by,$employer_id)
	{
		$security = new security();
		$query="update employer set logo=?,company_name=?,address=?,state=?,post_code=?,country=?,company_type=?,website=?,twitter=?,linked_in_page=?,updated_date=?,updated_by=? where 
		employer_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssi'
		,($security->xss_clean($logo))
		,($security->xss_clean($company_name))
		,($security->xss_clean($address))
		,($security->xss_clean($state))
		,($security->xss_clean($post_code))
		,($security->xss_clean($country))
		,($security->xss_clean($company_type))
		,($security->xss_clean($website))
		,($security->xss_clean($twitter))
		,($security->xss_clean($linked_in_page))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($employer_id)));
		$stmt->execute();
	}
	function update_password($password,$updated_by,$employer_id)
	{
		$security = new security();
		$query="update employer set password=?,updated_date=?,updated_by=? where 
		employer_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi'
		,($security->xss_clean($password))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($employer_id)));
		$stmt->execute();
	}
	function resend_reset_password($email_address)
	{
		/*RESEND RESET PASSWORD HERE*/
	}
	function update_reset_password($email_address,$reset_code)
	{
		$security = new security();
		$query="update employer set reset_password_code=?,updated_date=?,updated_by=? where 
		email_address=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss'
		,($security->xss_clean($reset_code))
		,(date("Y-m-d H:i:s")),($security->xss_clean($email_address)),($security->xss_clean($email_address)));
		$stmt->execute();
	}
	function update_picture($profile_picture,$img_src,$updated_by,$employer_id)
	{
		$security = new security();
		$query="update employer set profile_picture=?,img_src=?,updated_date=?,updated_by=? where 
		employer_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssi'
		,($security->xss_clean($profile_picture))
		,($security->xss_clean($img_src))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($employer_id)));
		$stmt->execute();
	}
	function insert_contact($contact_name,$email_address,$phone_number,$password,$confirm_email_address_code,$created_by)
	{
		$security = new security();
		$query="insert into employer(contact_name,email_address,phone_number,password,confirm_email_address_code,created_date,created_by)values(?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssss'
		,($security->xss_clean($contact_name))
		,($security->xss_clean($email_address))
		,($security->xss_clean($phone_number))
		,($security->xss_clean($password))
		,($security->xss_clean($confirm_email_address_code))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($created_by))
		);
		$stmt->execute();
	}

}
?>