<?php
class education_level
{
	public $education_level_id;
	public $education_level_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$education_level_id='',$education_level_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->education_level_id = $education_level_id;
		$this->education_level_name = $education_level_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_education_level_id($education_level_id)
	{
		$security = new security();
		$query="select *
		from general_education_level
		where education_level_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($education_level_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($education_level_id,$education_level_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$education_level_id,$education_level_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function education_level_all()
	{
		$query="select * 
		from general_education_level 
		order by education_level_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($education_level_id,$education_level_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$education_level=new education_level($this->conn,$education_level_id,$education_level_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$education_level;
		}
		return $data;
	}
	function update($education_level_name,$updated_by,$education_level_id)
	{
		$security = new security();
		$query="update general_education_level set education_level_name=?,updated_date=?,updated_by=? where 
		education_level_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($education_level_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($education_level_id)));
		$stmt->execute();
	}
	function insert($education_level_name,$created_by)
	{
		$security = new security();
		$query="insert into general_education_level(education_level_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($education_level_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($education_level_id)
	{
		$security = new security();
		$query="delete from general_education_level where education_level_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($education_level_id)));
		$stmt->execute();
	}
}
?>