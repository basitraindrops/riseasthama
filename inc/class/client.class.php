<?php
class client
{
	public $id;
	public $client_since;
	public $company_name;
	public $first_name;
	public $last_name;
	public $email;
	public $addr1;
	public $addr2;
	public $city;
	public $state;
	public $zip;
	public $country;
	public $phone;
	public $data;
	private $conn;
	function __construct($conn='',$client_since='',$company_name='',$first_name='',$last_name='',$email='',$addr1='',$addr2='',$city='',$state='',$zip='',$country='',$phone='')
	{
		$this->conn = $conn;
		$this->client_since=$client_since;
		$this->company_name=$company_name;
		$this->first_name=$first_name;
		$this->last_name=$last_name;
		$this->email=$email;
		$this->addr1=$addr1;
		$this->addr2 = $addr2;
		$this->city=$city;
		$this->state=$state;
		$this->zip=$zip;
		$this->country=$country;
		$this->phone=$phone;
	}
	function get_by_id($id)
	{
		$query="select * from clients where id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($result_id,$result_client_since,$result_company_name,$result_first_name,$result_last_name,$result_email,$result_addr1,$result_addr2,$result_city,$result_state,$result_zip,$result_country,$result_phone);
		$stmt->fetch();
		$this->__construct($this->conn,$result_username,$result_client_since,$result_company_name,$result_first_name,$result_last_name,$result_email,$result_addr1,$result_addr2,$result_city,$result_state,$result_zip,$result_country,$result_phone);
		
	}
	function get_grid()
	{
		$query = "select * from clients order by id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($result_id,$result_client_since,$result_company_name,$result_first_name,$result_last_name,$result_email,$result_addr1,$result_addr2,$result_city,$result_state,$result_zip,$result_country,$result_phone);
		$this->data=array();
		while($stmt->fetch())
		{
			$client_obj=new client($this->conn);
			$client_obj->get_by_id($result_id);
			$this->data[]=$client_obj;
		}
		return $this->data;
	}
	function insert($client_since='',$company_name='',$first_name='',$last_name='',$email='',$addr1='',$addr2='',$city='',$state='',$zip='',$country='',$phone='')
	{
		$query="insert into clients(client_since,company_name,first_name,last_name,email,addr1,addr2,city,state,zip,country,phone)
		values(?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssss', $client_since,$company_name,$first_name,$last_name,$email,$addr1,$addr2,$city,$state,$zip,$country,$phone);
		$stmt->execute();
	}
	function update($id,$client_since='',$company_name='',$first_name='',$last_name='',$email='',$addr1='',$addr2='',$city='',$state='',$zip='',$country='',$phone='')
	{
		$query="update clients set client_since=?,company_name=?,first_name=?,last_name=?,email=?,addr1=?,addr2=?,city=?,state=?,zip=?,country=?,phone=? where id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssi', $client_since,$company_name,$first_name,$last_name,$email,$addr1,$addr2,$city,$state,$zip,$country,$phone);
		$stmt->execute();
	}
	function deletes($id='')
	{
		$query="delete from clients where id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $id);
		$stmt->execute();
	}
}
?>