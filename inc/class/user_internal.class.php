<?php
class user_internal
{
	public $ui_id;
	public $ui_nip;
	public $user_id;
	public $status;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;

	private $conn;
	
	function __construct($conn=''
	,$ui_id=''
	,$ui_nip=""
	,$user_id=""
	,$status=""
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by=''
	)
	{
		$this->conn = $conn;
		$this->ui_id=$ui_id;
		$this->ui_nip=$ui_nip;
		$this->user_id=$user_id;
		$this->status=$status;
		$this->created_date=$created_date;
		$this->created_by=$created_by;
		$this->updated_date=$updated_date;
		$this->updated_by=$updated_by;

	}
	function get_by_ui_id($ui_id)
	{
		$security = new security();
		$query="select * 
		from user_internal
		where ui_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', ($security->xss_clean($ui_id)));
		$stmt->execute();

		$stmt->bind_result(					
				$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		$stmt->fetch();
		$this->__construct($this->conn
				,$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		
	}
	function get_by_user_id($ui_id)
	{
		$security = new security();
		$query="select * 
		from user_internal
		where user_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', ($security->xss_clean($ui_id)));
		$stmt->execute();

		$stmt->bind_result(					
				$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		$stmt->fetch();
		$this->__construct($this->conn
				,$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		
	}
	function ui_all()
	{
		$query="select *
		from user_internal 
		order by ui_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(					
				$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
		$data=array();
		while($stmt->fetch())
		{
			$user_internal=new user_internal($this->conn=""
				,$ui_id
				,$ui_nip
				,$user_id
				,$status
				,$created_date
				,$created_by
				,$updated_date
				,$updated_by
			);
			$data[]=$user_internal;
		}
		return $data;
	}
	function update_status($status,$user_name,$user_id)
	{
		$security = new security();
		$query="update user_internal set status=?,updated_date=?,updated_by=? where ui_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('issi'
		,($security->xss_clean($status))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($user_name))
		,($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function update(
				$ui_nip,$updated_by
				,$ui_id
	)
	{
		$security = new security();
		$query="update user_internal 
		set ui_nip=? ,updated_date=?
					,updated_by=?
					where 
					ui_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi'
		,($security->xss_clean($ui_nip))
					,(date("Y-m-d H:i:s"))
					,($security->xss_clean($updated_by))
					,($security->xss_clean($ui_id)));
//		echo $ui_nip,$updated_by,$ui_id;
		$stmt->execute();
	}
	function insert($ui_nip
				,$user_id
				,$status
				,$created_by
	)
	{
		$security = new security();
		$query="insert into user_internal
		(ui_nip
				,user_id
				,status
				,created_date
				,created_by
		)
		values(?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sisss'
					,($security->xss_clean($ui_nip))
					,($security->xss_clean($user_id))
					,($security->xss_clean($status))
					,(date("Y-m-d H:i:s"))
					,($security->xss_clean($created_by)));
		$stmt->execute();
	}

	function delete($ui_id)
	{
		$security=new security();
		$query="delete from user_internal where ui_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($ui_id)));
		$stmt->execute();
	}
}
?>