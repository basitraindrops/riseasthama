<?php
class front_document_type
{
	public $dt_id;
	public $dt_name;
	public $dt_description;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$dt_id='',$dt_name='',$dt_description="",$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->dt_id = $dt_id;
		$this->dt_name = $dt_name;
		$this->dt_description = $dt_description;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_dt_id($dt_id)
	{
		$security = new security();
		$query="select *
		from front_document_type
		where dt_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($dt_id);
		$stmt->bind_param('i', $id);
		$stmt->execute() or die("error");

		$stmt->bind_result($dt_id,$dt_name,$dt_description,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$dt_id,$dt_name,$dt_description,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function dt_all()
	{
		$query="select * 
		from  front_document_type
		order by dt_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute() or die("error");

		$stmt->bind_result($dt_id,$dt_name,$dt_description,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$dt=new front_document_type($this->conn,$dt_id,$dt_name,$dt_description,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$dt;
		}
		return $data;
	}
	function update($dt_name,$dt_description,$updated_by,$dt_id)
	{
		$security = new security();
		$query="update  front_document_type set dt_name=?,dt_description=?,updated_date=?,updated_by=? where 
		dt_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssi',($security->xss_clean($dt_name)),($security->xss_clean($dt_description)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($dt_id)));
		$stmt->execute() or die("error");
	}
	function insert($dt_name,$dt_description,$created_by)
	{
		$security = new security();
		$query="insert into  front_document_type(dt_name,dt_description,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($dt_name)),($security->xss_clean($dt_description)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute() or die("error");
	}
	function delete($dt_id)
	{
		$security = new security();
		$query="delete from  front_document_type where dt_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($dt_id)));
		$stmt->execute() or die("error");
	}
}
?>