<?php
class candidate
{
	public $candidate_id;
	public $picture;
	public $first_name;
	public $last_name;
	public $title;
	public $city;
	public $state;
	public $country;
	public $sap_certified;
	public $education_level;
	public $can_travel;
	public $willing_to_rellocate;
	public $available;
	public $notice_period;
	public $linked_in_profile;
	public $summary;
	public $skillset;
	public $years_sap_experience;
	public $total_years_experience;
	public $industry_experience;
	public $mobile_number;
	public $email_address;
	public $current_rate;
	public $desired_rate;
	public $citizenship_status;
	public $security_clearance;
	public $gender;
	public $nationality;
	public $referee_1_name;
	public $referee_1_number;
	public $referee_1_email;
	public $referee_2_name;
	public $referee_2_number;
	public $referee_2_email;
	public $prefer;
	public $password;
	public $confirm_email_address_code;
	public $email_address_confirmed;
	public $reset_password_code;
	public $reset_password_done;
	public $is_registered;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn=''
	,$candidate_id=''
	,$picture=''
	,$first_name=''
	,$last_name=''
	,$title=''
	,$city=''
	,$state=''
	,$country=''
	,$sap_certified=''
	,$education_level=''
	,$can_travel=''
	,$willing_to_rellocate=''
	,$available=''
	,$notice_period=''
	,$linked_in_profile=''
	,$summary=''
	,$skillset=''
	,$years_sap_experience=''
	,$total_years_experience=''
	,$industry_experience=''
	,$mobile_number=''
	,$email_address=''
	,$current_rate=''
	,$desired_rate=''
	,$citizenship_status=''
	,$security_clearance=''
	,$gender=''
	,$nationality=''
	,$referee_1_name=''
	,$referee_1_number=''
	,$referee_1_email=''
	,$referee_2_name=''
	,$referee_2_number=''
	,$referee_2_email=''
	,$prefer=''
	,$password=''
	,$confirm_email_address_code=''
	,$email_address_confirmed=''
	,$reset_password_code=''
	,$reset_password_done=''
	,$is_registered=''
	,$created_date=''
	,$created_by=''
	,$updated_by=''
	,$updated_date=''
	)
	{
		$this->conn = $conn;
		$this->candidate_id=$candidate_id;
		$this->picture=$picture;
		$this->first_name=$first_name;
		$this->last_name=$last_name;
		$this->title=$title;
		$this->city=$city;
		$this->state=$state;
		$this->country=$country;
		$this->sap_certified=$sap_certified;
		$this->education_level=$education_level;
		$this->can_travel=$can_travel;
		$this->willing_to_rellocate=$willing_to_rellocate;
		$this->available=$available;
		$this->notice_period=$notice_period;
		$this->linked_in_profile=$linked_in_profile;
		$this->summary=$summary;
		$this->skillset=$summary;
		$this->years_sap_experience=$years_sap_experience;
		$this->total_years_experience=$total_years_experience;
		$this->industry_experience=$industry_experience;
		$this->mobile_number=$mobile_number;
		$this->email_address=$email_address;
		$this->current_rate=$current_rate;
		$this->desired_rate=$desired_rate;
		$this->citizenship_status=$citizenship_status;
		$this->security_clearance=$security_clearance;
		$this->gender=$gender;
		$this->nationality=$nationality;
		$this->referee_1_name=$referee_1_name;
		$this->referee_1_number=$referee_1_number;
		$this->referee_1_email=$referee_1_email;
		$this->referee_2_name=$referee_2_name;
		$this->referee_2_number=$referee_2_number;
		$this->referee_2_email=$referee_2_email;
		$this->prefer=$prefer;
		$this->password=$password;
		$this->confirm_email_address_code=$confirm_email_address_code;
		$this->email_address_confirmed=$email_address_confirmed;
		$this->reset_password_code=$reset_password_code;
		$this->reset_password_done=$reset_password_done;
		$this->is_registered=$is_registered;
		$this->created_date=$created_date;
		$this->created_by=$created_by;
		$this->updated_by=$updated_by;
		$this->updated_date=$updated_date;
	}
	function get_by_candidate_id($candidate_id)
	{
		$security = new security();
		$query="select *
		from candidate
		where candidate_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($candidate_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		
	}
	function get_by_candidate_email($candidate_email)
	{
		$security = new security();
		$query="select *
		from candidate
		where email_address = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($candidate_email);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date

			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		
	}
	function check_candidate_email_exist($email_address)
	{
		$security = new security();
		$query="select *
		from candidate
		where email_address = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($email_address);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();

		if($first_name!="" && $first_name!=null)
			return true;
		else
			return false;
		
	}
	function check_candidate_phone_exist($phone_number)
	{
		$security = new security();
		$query="select *
		from candidate
		where mobile_number = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($phone_number);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();

		if($first_name!="" && $first_name!=null)
			return true;
		else
			return false;
		
	}
	function get_by_confirmed_email($confirmed_code)
	{
		$security = new security();
		$query="select *
		from candidate
		where confirm_email_address_code = ? and  email_address_confirmed=0 limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($confirmed_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		if($first_name!="" && $first_name!=null)
			return true;
		else
			return false;
	}
	function get_by_reset_password($reset_password_code)
	{
		$security = new security();
		$query="select *
		from candidate
		where reset_password_code = ? and  reset_password_done=0 limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($reset_password_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		if($first_name!="" && $last_name!=null)
			return true;
		else
			return false;
	}
	function activate_email($confirm_email_address_code)
	{
		$security = new security();
		$query="update candidate set email_address_confirmed=1,updated_date=?,updated_by=? where 
		confirm_email_address_code=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($confirm_email_address_code)));
		$stmt->execute();

		$query="select *
		from candidate
		where confirm_email_address_code = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($confirm_email_address_code);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result(
			$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$candidate_id
			,$picture
			,$first_name
			,$last_name
			,$title
			,$city
			,$state
			,$country
			,$sap_certified
			,$education_level
			,$can_travel
			,$willing_to_rellocate
			,$available
			,$notice_period
			,$linked_in_profile
			,$summary
			,$skillset
			,$years_sap_experience
			,$total_years_experience
			,$industry_experience
			,$mobile_number
			,$email_address
			,$current_rate
			,$desired_rate
			,$citizenship_status
			,$security_clearance
			,$gender
			,$nationality
			,$referee_1_name
			,$referee_1_number
			,$referee_1_email
			,$referee_2_name
			,$referee_2_number
			,$referee_2_email
			,$prefer
			,$password
			,$confirm_email_address_code
			,$email_address_confirmed
			,$reset_password_code
			,$reset_password_done
			,$is_registered
			,$created_date
			,$created_by
			,$updated_by
			,$updated_date
		);
		

	}
	function resend_activation_email($email_address,$confirm_email_address_code)
	{
		$security = new security();
		$query="update candidate set email_address_confirmed=0,confirm_email_address_code=?,updated_date=?,updated_by=? where 
		 email_address=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($confirm_email_address_code)),(date("Y-m-d H:i:s")),($security->xss_clean($email_address)),($security->xss_clean($email_address)));
		$stmt->execute();
	}
	function update_contact($first_name,$last_name,$email_address,$phone_number,$updated_by,$candidate_id)
	{
		$security = new security();
		$query="update candidate set first_name=?,last_name=?,email_address=?,phone_number=?,updated_date=?,updated_by=? where 
		candidate_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssi',($security->xss_clean($first_name)),($security->xss_clean($last_name)),($security->xss_clean($email_address)),($security->xss_clean($phone_number)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($candidate_id)));
		$stmt->execute();
	}
	function update_company($logo,$company_name,$address,$state,$post_code,$country,$company_type,$website,$twitter,$linked_in_page,$updated_by,$candidate_id)
	{
		$security = new security();
		$query="update candidate set logo=?,company_name=?,address=?,state=?,post_code=?,country=?,company_type=?,website=?,twitter=?,linked_in_page=?,updated_date=?,updated_by=? where 
		candidate_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssi'
		,($security->xss_clean($logo))
		,($security->xss_clean($company_name))
		,($security->xss_clean($address))
		,($security->xss_clean($state))
		,($security->xss_clean($post_code))
		,($security->xss_clean($country))
		,($security->xss_clean($company_type))
		,($security->xss_clean($website))
		,($security->xss_clean($twitter))
		,($security->xss_clean($linked_in_page))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($candidate_id)));
		$stmt->execute();
	}
	function resend_reset_password($email_address)
	{
		/*RESEND RESET PASSWORD HERE*/
	}
	function update_reset_password($email_address,$reset_code)
	{
		$security = new security();
		$query="update candidate set reset_password_code=?,updated_date=?,updated_by=? where 
		email_address=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss'
		,($security->xss_clean($reset_code))
		,(date("Y-m-d H:i:s")),($security->xss_clean($email_address)),($security->xss_clean($email_address)));
		$stmt->execute();
	}
	function insert_contact($first_name,$last_name,$email_address,$phone_number,$password,$confirm_email_address_code,$created_by)
	{
		$security = new security();
		$query="insert into candidate(first_name,last_name,email_address,mobile_number,password,confirm_email_address_code,created_date,created_by)values(?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssss'
		,($security->xss_clean($first_name))
		,($security->xss_clean($last_name))
		,($security->xss_clean($email_address))
		,($security->xss_clean($mobile_number))
		,($security->xss_clean($password))
		,($security->xss_clean($confirm_email_address_code))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($created_by))
		);
		$stmt->execute();
	}
}
?>