<?php
error_reporting(0);
class faq
{	
	public $faq_cat_id;
	public $faq_cat_name;
	public $faq_cat_des;
	public $faq_id;
	public $faq_name;
	public $faq_des;
	public $faq_category;

	
	private $conn;
	function __construct($conn='',$faq_cat_id='',$faq_cat_name='',$faq_cat_des='',$faq_id='',$faq_name='',$faq_des='',$faq_category='')
	{
		$this->conn = $conn;
		$this->faq_cat_id = $faq_cat_id;
		$this->faq_cat_name = $faq_cat_name;
		$this->faq_cat_des = $faq_cat_des;
		$this->faq_id = $faq_id;
		$this->faq_name = $faq_name;
		$this->faq_des = $faq_des;
		$this->faq_category = $faq_category;
		
	}
	function faq_cat_detail()
	{
		
		$query="select * 
		from faq_category";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$stmt->bind_result(
			 $faq_cat_id
			,$faq_cat_name
			,$faq_cat_des

		);
		$data=array();
		while($stmt->fetch())
		{
			$title=new faq($this->conn
			,$faq_cat_id
			,$faq_cat_name
			,$faq_cat_des
			);
			$data[]=$title;
		}
		return $data;
	}
	function faq_topic($f_id)
	{
		
		 $query="select * 
		from faq_category 
		where faq_cat_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $f_id);
		$stmt->execute();
		

		$stmt->bind_result(
			 $faq_cat_id
			,$faq_cat_name
			,$faq_cat_des
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$faq_cat_id
			,$faq_cat_name
			,$faq_cat_des
			);
	}
	function faq_detail($f_id)
	{
		
		 $query="select * 
		from faq 
		where faq_category_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $f_id);
		$stmt->execute();

	
		$stmt->bind_result(
			 $faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{

		$title=new faq($this->conn
			,$faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
			);
			$data[]=$title;
		}
		return $data;
	}
	function faq_main_detail($f_id)
	{
		
		 $query="select * 
		from faq 
		where faq_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $f_id);
		$stmt->execute();

	
		$stmt->bind_result(
			 $faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{

		$title=new faq($this->conn
			,$faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
			);
			$data[]=$title;
		}
		return $data;
	}
	
}
?>