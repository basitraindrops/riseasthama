<?php
function user_access($mysqli,$user_id)
{
//	require_once(CLASS_DIR.'encryption.class.php');
	require_once(CLASS_DIR."user_access.class.php");
//	$sessionObj=new SecureSession();
	$ua = new user_access($mysqli);
	$post_name=basename($_SERVER['PHP_SELF']);
	switch($post_name)
	{
		case "user_group.php":
			$ua->get2("view_user_group",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_delete_ajax.php":
			$ua->get2("delete_user_group",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "access.php":
			$ua->get2("view_access_settings",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "access_delete_ajax.php":
			$ua->get2("delete_access_settings",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "kabupaten.php":
			$ua->get2("view_kabupaten",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "kabupaten_delete_ajax.php":
			$ua->get2("delete_kabupaten",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "menu.php":
			$ua->get2("view_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "menu_delete_ajax.php":
			$ua->get2("delete_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province.php":
			$ua->get2("view_province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province_delete_ajax.php":
			$ua->get2("delete_province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_menu.php":
			$ua->get2("user_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_menu_ajax.php":
			$ua->get2("user_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_access.php":
			$ua->get2("user_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_access_ajax.php":
			$ua->get2("user_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_access.php":
			$ua->get2("user_group_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_access_ajax.php":
			$ua->get2("user_group_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "password_update.php":
			$ua->get2("administration_user_password",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "perusahaan_delete_ajax.php":
			$ua->get2("delete_perusahaan",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_internal_delete_ajax.php":
			$ua->get2("delete_user_internal",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		default:
			break;

	}
}
function user_access_each($mysqli,$access_name,$user_id)
{
	require_once(CLASS_DIR."user_access.class.php");
	$ua = new user_access($mysqli);
	$ua->get2($access_name,$user_id);
	if(!isset($ua->ua_id))
		return false;
	else
		return true;	
}
function generate_password($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=|~`';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}
function get_directory($user_id,$mysqli)
{
	require_once(CLASS_DIR.'user.class.php');
	require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user_internal.class.php');
	$user=new user($mysqli);
	$user->get_by_user_id($user_id);
	$dir="";
	if($user->user_group_id==2)
	{
		$dir="file/";
		$company=new company($mysqli);
		$company->get_by_user_id($user_id);
		$dir.=$company->registration_number."/";
	}
	else
	{
		$dir="file_ui";
		$ui=new user_internal($mysqli);
		$ui->get_by_user_id($user_id);
		$dir.=$ui->registration_number."/";
	}
	return $dir;
}
function get_profile_picture($user_id,$mysqli)
{
	require_once(CLASS_DIR.'user.class.php');
	require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user_internal.class.php');
	$user=new user($mysqli);
	$user->get_by_user_id($user_id);
	$pic=$user->user_photo_file;
	return $pic;
}
function format_uri( $string, $separator = '-' )
{
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $special_cases = array( '&' => 'and', "'" => '');
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return $string;
}
function dirToArray($dir) { 
   
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 
   
   return $result; 
} 
function encrypt($string,$secretKey)
{
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secretKey, $string,
								  MCRYPT_MODE_ECB, $iv);
	return base64_encode($crypttext);
}
	
function decrypt($string,$secretKey)
{
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
//	echo $iv."<br/>";
	$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secretKey, base64_decode($string),
                                MCRYPT_MODE_ECB, $iv);

// Drop nulls from end of string
	$decrypttext = rtrim($decrypttext, "\0");
	return $decrypttext;

}
function pagination_data($page_number,$count_row_posted,$count_row_all)
{
	$count_page=ceil($count_row_all/$count_row_posted);
	$start=0;
	$end=1;
	$start=($page_number-1)*$count_row_posted;
	$end=$start+$count_row_posted;
	if(($end+1)>$count_row_all)
	{
		$end=$count_row_all-1;
	}
	$data['count_row_posted']=$count_row_posted;
	$data['page_number']=$page_number;
	$data['count_row_all']=$count_row_all;
	$data['start']=$start;
	$data['end']=$end;
	$data['count_page']=$count_page;
	return $data;
}
function substrhtml($str,$start,$len){

    $str_clean = substr(strip_tags($str),$start,$len);
    $pos = strrpos($str_clean, " ");
    if($pos === false) {
        $str_clean = substr(strip_tags($str),$start,$len);  
        }else
        $str_clean = substr(strip_tags($str),$start,$pos);

    if(preg_match_all('/\<[^>]+>/is',$str,$matches,PREG_OFFSET_CAPTURE)){

        for($i=0;$i<count($matches[0]);$i++){

            if($matches[0][$i][1] < $len){

                $str_clean = substr($str_clean,0,$matches[0][$i][1]) . $matches[0][$i][0] . substr($str_clean,$matches[0][$i][1]);

            }else if(preg_match('/\<[^>]+>$/is',$matches[0][$i][0])){

                $str_clean = substr($str_clean,0,$matches[0][$i][1]) . $matches[0][$i][0] . substr($str_clean,$matches[0][$i][1]);

                break;

            }

        }

        return $str_clean;

    }else{
        $string = substr($str,$start,$len);
         $pos = strrpos($string, " ");
        if($pos === false) {
            return substr($str,$start,$len);
        }
            return substr($str,$start,$pos);

    }

}
function meta($mysqli,$page,$slug)
{
//	echo $slug;
	$data=array();
	$settings=new settings($mysqli);
	$settings->get_by_id(1);
	$data['author']=$settings->author;
	$data['publisher']=$settings->publisher;
	$data['title']=$settings->title;
	$data['meta_description']=$settings->meta_description;
	$data['meta_keyword']=$settings->meta_keyword;
//	echo print_r($data);
	switch($page)
	{
		case "post.php":
			require_once(CLASS_DIR."post.class.php");
			$p = new post($mysqli);
			$p->get_by_seo_url($slug);
			$data['title'] .= " - ".$p->html_title;
			$data['meta_description'].=" - ".$p->meta_description;
			$data['meta_keyword'].=",".$p->meta_keyword;
			$data['author'].=",".$p->created_by;
			$data['publisher'].=",".$p->created_by;
			break;
		case "page.php":
			require_once(CLASS_DIR."page.class.php");
			$p = new page($mysqli);
			$p->get_by_seo_url($slug);
			$data['title'].=" - ".$p->html_title;
			$data['meta_description'].=" - ".$p->meta_description;
			$data['meta_keyword'].=",".$p->meta_keyword;
			$data['author'].=",".$p->created_by;
			$data['publisher'].=",".$p->created_by;
			break;
		case "post-category.php":
			require_once(CLASS_DIR."post_category.class.php");
			$pc = new post_category($mysqli);
			$pc->get_by_slug($slug);
//			echo print_r($pc);
			$data['title'].=" - ".$pc->post_category_name;
			$data['meta_description'].=" - ".$pc->post_category_name;
			$data['meta_keyword'].=",".$pc->post_category_name;
			break;
		case "page-category.php":
			require_once(CLASS_DIR."page_category.class.php");
			$pc = new page_category($mysqli);
			$pc->get_by_slug($slug);
			$data['title'].=" - ".$pc->page_category_name;
			$data['meta_description'].=" - ".$pc->page_category_name;
			$data['meta_keyword'].=",".$pc->page_category_name;
			break;
		default;
			break;
	}

	return $data;
}

##########################################################################################################
# IMAGE FUNCTIONS																						 #
# You do not need to alter these functions																 #
##########################################################################################################
function resizeImage($image,$width,$height,$scale) {
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}
//You do not need to alter these functions
function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}
//You do not need to alter these functions
function getHeight($image) {
	$size = getimagesize($image);
	$height = $size[1];
	return $height;
}
//You do not need to alter these functions
function getWidth($image) {
	$size = getimagesize($image);
	$width = $size[0];
	return $width;
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>