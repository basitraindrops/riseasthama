<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')<>"")
{
	header("Location:dashboard.php");
}


require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');


?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
    <div class="image-container set-full-height" style="background-image: url('assets/img/backgroundregister.png')">
        <!--   Creative Tim Branding   -->

        <div class="logo-container">
            <div class="logo">

            </div>

        </div>

        <div id="terms-modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-body">
                        <h2>Terms and conditions</h2>
                    </div>
                </div>
            </div>
        </div>


        <!--   Big container   -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form method="post" action="intro.php" id="payment-form">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

                                <div class="wizard-header">

                                    <div class="wizard-navigation">
                                        <div class="progress-with-circle">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                        </div>
                                        <ul>
                                            <li>
                                                <a href="#captain" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-list"></i>
                                                    </div>

                                                </a>
                                            </li>
                                            <li>
                                                <a href="#details" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-briefcase"></i>
                                                    </div>

                                                </a>
                                            </li>
                                            <li>
                                                <a href="#description" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-pencil"></i>
                                                    </div>

                                                </a>
                                            </li>
                                        </ul>

                                    </div>

                                </div>


                                <div class="tab-content">
                                    <div class="tab-pane" id="captain">

                                        <h2 class="info-text">Welcome to RISE</h2>
                                        <h4 class="info-text">Who needs help?</h4>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <div class="col-sm-4 col-sm-offset-2">
                                                    <div class="choice" data-toggle="wizard-checkbox">
                                                        <input type="checkbox" name="jobb" value="Design">
                                                        <div class="btn-next card card-checkboxes card-hover-effect">

                                                            <p>Myself</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="choice" data-toggle="wizard-checkbox">
                                                        <input type="checkbox" name="jobb" value="Design">
                                                        <div class="btn-next card card-checkboxes card-hover-effect">
                                                            <p>A loved one</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="info-text"> Your RISE profile</h5>
                                            </div>


                                            <div class="col-sm-10 col-sm-offset-1">

                                                <div class="form-group">
                                                    <label>WHAT IS YOUR NAME?</label>
                                                    <input type="text" class="form-control" id="inputname" name="inputname" required="true" placeholder="e.g John Doe"> </div>
                                                <div class="form-group">
                                                    <label>WHAT IS YOUR EMAIL ADDRESS?</label>
                                                    <input name="email" type="email" class="form-control" placeholder="john@gmail.com">
                                                </div>




                                                <div class="form-button" style="max-width: 200px;margin: 0 auto;">
                                                    <input type='button' class='btn-next btn btn-fill btn-warning btn-wd' name='next' value='Next' />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <h2 class="info-text">Last step - secure payment </h2>
                                            <h5 class="info-text">We charge $49, once.<br/>You will get lifetime access to the RISE platform where you can read the full method, watch video summaries, ask us anything and get support from the RISE community.</h5>
                                            <div class="col-sm-6 col-sm-offset-1">

                                                <div class="paymentwrapper">

                                                    <img class="paymentmethod" src="assets/img/creditcard.png">


                                                    <div class="col-sm-12 form-fields">

                                                        <label>CREDITCARD HOLDER NAME</label>
                                                        <input type="text" name="name" placeholder="E.g. John Doe" class="form-control" style="background-color: white; ">
                                                    </div>
                                                    <div class="col-sm-12 form-fields">
                                                        <label>CARD DATA</label><br />
                                                        <div id="card-element" class="field"></div>
                                                    </div>

                                                    <div class="col-sm-6 form-fields"> <br />
                                                        <label>ZIP CODE</label>
                                                        <input type="text" name="zip" placeholder="90210" class="form-control " style="background-color: white;">
                                                    </div>
                                                    <div class="form-button">
                                                        <button type="submit" name="paymentwith" value="stripes" class="btn btn-fill btn-warning btn-wd" id="proceed">Pay $49</button>
                                                        <div align="center"><b>Or</b></div>
                                                        <button name="paymentwith" id="paypal" type="button" value="paypal" class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button>
                                                        <!--<button type="submit" name="submit"  class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button>   -->
                                                    </div>
                                                    <div class="terms">
                                                        <h5 class="info-text">By proceeding, you are agree with our
                                                            <a href="#terms-modal" data-toggle="modal"> terms and conditions</a></h5>

                                                    </div>
                            </form>
                            <div id="card-errors" class="error"></div>


                            <script src="https://js.stripe.com/v2/"></script>
                            <script src="https://js.stripe.com/v3/"></script>
                            <div class="form-group form-payment">
                                <style type="text/css">
                                    .field {
                                        background: white;
                                        box-sizing: border-box;
                                        font-weight: 400;
                                        border: 1px solid #CFD7DF;
                                        border-radius: 3px;
                                        color: #32315E;
                                        outline: none;
                                        height: 48px;
                                        line-height: 48px;
                                        padding: 0 20px;
                                        cursor: text;
                                        width: 76%;
                                    }

                                    .field::-webkit-input-placeholder {
                                        color: #CFD7DF;
                                    }

                                    .field::-moz-placeholder {
                                        color: #CFD7DF;
                                    }

                                    .field:-ms-input-placeholder {
                                        color: #CFD7DF;
                                    }

                                    .field:focus,
                                    .field.StripeElement--focus {
                                        border-color: #F99A52;
                                    }

                                </style>

                                <script type="text/javascript">
                                    //    var stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
                                    var stripe = Stripe('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
                                    var elements = stripe.elements();

                                    var card = elements.create('card', {
                                        hidePostalCode: true,
                                        style: {
                                            base: {
                                                iconColor: '#F99A52',
                                                color: '#32315E',
                                                lineHeight: '48px',
                                                fontWeight: 400,
                                                fontFamily: '"Helvetica Neue", "Helvetica", sans-serif',
                                                fontSize: '15px',

                                                '::placeholder': {
                                                    color: '#CFD7DF',
                                                }
                                            },
                                        }
                                    });
                                    card.mount('#card-element');

                                    function setOutcome(result) {
                                        var successElement = document.querySelector('.success');
                                        var errorElement = document.querySelector('.error');
                                        successElement.classList.remove('visible');
                                        errorElement.classList.remove('visible');
                                        var form = document.querySelector('form');

                                        if (result.token) {
                                            // Use the token to create a charge or a customer
                                            // https://stripe.com/docs/charges
                                            //    successElement.querySelector('#token').value = result.token.id;
                                            var hiddenInput = document.createElement('input');
                                            hiddenInput.setAttribute('type', 'hidden');
                                            hiddenInput.setAttribute('name', 'stripeToken');
                                            hiddenInput.setAttribute('value', result.token.id);
                                            form.appendChild(hiddenInput);
                                            form.submit();

                                        } else if (result.error) {
                                            $("#proceed").prop("disabled", false);
                                            errorElement.textContent = result.error.message;
                                            errorElement.classList.add('visible');
                                        }
                                    }

                                    card.on('change', function(event) {
                                        setOutcome(event);
                                    });

                                    document.querySelector('form').addEventListener('submit', function(e) {
                                        $("#proceed").prop("disabled", true);
                                        e.preventDefault();
                                        var form = document.querySelector('form');
                                        var extraDetails = {
                                            name: form.querySelector('input[name=name]').value,
                                            address_zip: form.querySelector('input[name=zip]').value
                                        };
                                        stripe.createToken(card, extraDetails).then(setOutcome);
                                    });

                                </script>




                            </div>
                            </div>
                            </div>
                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group">
                                    <img src="assets/img/secure.png">
                                    <p class="description"></p>
                                </div>
                                <div class="form-group">
                                    <label>Lifetime refund policty</label>
                                    <p class="description">If at any point you feel that the program is not useful then we will refund the entire amount. No questions asked</p>
                                </div>
                                <div class="form-group">
                                    <label>You cannot afford $49?</label>
                                    <p class="description">Send us a message at hope@riseasthma.com, briefly describe your situation to us and we will give you access for free</p>
                                </div>
                            </div>

                            </div>
                            </div>
                            <div class="wizard-footer col-md-4 col-md-offset-4">


                                <div class="clearfix"></div>
                            </div>
                            </form>
                            </div>
                        </div>
                        <!-- wizard container -->
                    </div>
                </div>
                <!-- row -->
            </div>
            <!--  big container -->

            <div class="footer">
                <div class="container text-center">
                    Already have an account? Sign in <a href="<?php echo SITE_URL; ?>page_login.php">here.</a>
                </div>
                <div>
                    <a href="<?php echo SITE_URL;?>"><img src="assets/img/logo.png" class="img-responsive logo-footer"></a>
                </div>
            </div>
        </div>

</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('document').ready(function() {
        $("#paypal").click(function() {
            var datastring = $("#payment-form").serialize();
            $('#payment-form').attr('action', "paypal.php");
            $("#payment-form").submit();
        });
    });

</script>

</html>
<?php
/*-************************************************************

<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')<>"")
{
    header("Location:dashboard.php");
}


require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');


?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="assets/img/favicon.png" />
        <title>Paper Bootstrap Wizard by Creative Tim</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />

        <!-- Fonts and Icons -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/themify-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url('assets/img/backgroundregister.png')">
            <!--   Creative Tim Branding   -->

            <div class="logo-container">
                <div class="logo">

                </div>

            </div>

            <div id="terms-modal" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-body">
                            <h2>Terms and conditions</h2>
                        </div>
                    </div>
                </div>
            </div>


            <!--   Big container   -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">

                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="azure" id="wizard">
                                <form method="post" action="intro.php" id="payment-form">
                                    <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

                                    <div class="wizard-header">

                                        <div class="wizard-navigation">
                                            <div class="progress-with-circle">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <a href="#captain" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-list"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#details" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-briefcase"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#description" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-pencil"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                            </ul>

                                        </div>

                                    </div>


                                    <div class="tab-content">
                                        <div class="tab-pane" id="captain">

                                            <h2 class="info-text">Welcome to RISE</h2>
                                            <h4 class="info-text">Who needs help?</h4>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <div class="col-sm-4 col-sm-offset-2">
                                                        <div class="choice" data-toggle="wizard-checkbox">
                                                            <input type="checkbox" name="jobb" value="Design">
                                                            <div class="btn-next card card-checkboxes card-hover-effect">

                                                                <p>Myself</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="choice" data-toggle="wizard-checkbox">
                                                            <input type="checkbox" name="jobb" value="Design">
                                                            <div class="btn-next card card-checkboxes card-hover-effect">
                                                                <p>A loved one</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="details">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h2 class="info-text"> Your RISE profile</h5>
                                                </div>


                                                <div class="col-sm-10 col-sm-offset-1">

                                                    <div class="form-group">
                                                        <label>WHAT IS YOUR NAME?</label>
                                                        <input type="text" class="form-control" id="inputname" name="inputname" required="true" placeholder="e.g John Doe"> </div>
                                                    <div class="form-group">
                                                        <label>WHAT IS YOUR EMAIL ADDRESS?</label>
                                                        <input name="email" type="email" class="form-control" placeholder="john@gmail.com">
                                                    </div>




                                                    <div class="form-button" style="max-width: 200px;margin: 0 auto;">
                                                        <input type='button' class='btn-next btn btn-fill btn-warning btn-wd' name='next' value='Next' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="description">
                                            <div class="row">
                                                <h2 class="info-text">Last step - secure payment </h2>
                                                <h5 class="info-text">We charge $49 and only after 30 days from signing up, <br/>to make sure that you are satisfied first.</h5>
                                                <div class="col-sm-6 col-sm-offset-1">

                                                    <div class="paymentwrapper">

                                                        <img class="paymentmethod" src="assets/img/creditcard.png">

                                                        <!-- 
                                                            <div class="col-sm-12 form-fields">

                                                                <label>CREDITCARD HOLDER NAME</label>
                                                                <input type="text" name="name" placeholder="E.g. John Doe" class="form-control" style="background-color: white; ">
                                                            </div>  -->
                                                        <div class="col-sm-12 form-fields">
                                                            <label>CARD DATA</label><br />
                                                            <div id="card-element" class="field"></div>
                                                        </div>

                                                        <!--   <div class="col-sm-6 form-fields"> <br /> 
                                                                <label>ZIP CODE</label> 
                                                                <input type="text" name="zip" placeholder="90210" class="form-control " style="background-color: white;">
                                                        </div>  -->
                                                        <div class="form-button">
                                                            <button type="submit" name="paymentwith" value="stripes" class="btn btn-fill btn-warning btn-wd" id="proceed">Pay $49</button>
                                                            <div align="center"><b>Or</b></div>
                                                            <button name="paymentwith" id="paypal" type="button" value="paypal" class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button>
                                                            <!--<button type="submit" name="submit"  class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button>   -->
                                                        </div>
                                                        <div class="terms">
                                                            <h5 class="info-text">By proceeding, you are agree with our
                                                                <a href="#terms-modal" data-toggle="modal"> terms and conditions</a></h5>

                                                        </div>
                                </form>
                                <div id="card-errors" class="error"></div>


                                <script src="https://js.stripe.com/v2/"></script>
                                <script src="https://js.stripe.com/v3/"></script>
                                <div class="form-group form-payment">
                                    <style type="text/css">
                                        .field {
                                            background: white;
                                            box-sizing: border-box;
                                            font-weight: 400;
                                            border: 1px solid #CFD7DF;
                                            border-radius: 3px;
                                            color: #32315E;
                                            outline: none;
                                            height: 48px;
                                            line-height: 48px;
                                            padding: 0 20px;
                                            cursor: text;
                                            width: 76%;
                                        }

                                        .field::-webkit-input-placeholder {
                                            color: #CFD7DF;
                                        }

                                        .field::-moz-placeholder {
                                            color: #CFD7DF;
                                        }

                                        .field:-ms-input-placeholder {
                                            color: #CFD7DF;
                                        }

                                        .field:focus,
                                        .field.StripeElement--focus {
                                            border-color: #F99A52;
                                        }

                                    </style>

                                    <script type="text/javascript">
                                        //    var stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
                                        var stripe = Stripe('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
                                        var elements = stripe.elements();

                                        var card = elements.create('card', {
                                            hidePostalCode: true,
                                            style: {
                                                base: {
                                                    iconColor: '#F99A52',
                                                    color: '#32315E',
                                                    lineHeight: '48px',
                                                    fontWeight: 400,
                                                    fontFamily: '"Helvetica Neue", "Helvetica", sans-serif',
                                                    fontSize: '15px',

                                                    '::placeholder': {
                                                        color: '#CFD7DF',
                                                    }
                                                },
                                            }
                                        });
                                        card.mount('#card-element');

                                        function setOutcome(result) {
                                            //var successElement = document.querySelector('.success');
                                            var errorElement = document.querySelector('.error');
                                            //  successElement.classList.remove('visible');
                                            errorElement.classList.remove('visible');
                                            var form = document.querySelector('form');

                                            if (result.token) {
                                                // Use the token to create a charge or a customer
                                                // https://stripe.com/docs/charges
                                                //    successElement.querySelector('#token').value = result.token.id;
                                                var hiddenInput = document.createElement('input');
                                                hiddenInput.setAttribute('type', 'hidden');
                                                hiddenInput.setAttribute('name', 'stripeToken');
                                                hiddenInput.setAttribute('value', result.token.id);
                                                form.appendChild(hiddenInput);
                                                form.submit();

                                            } else if (result.error) {
                                                $("#proceed").prop("disabled", false);
                                                errorElement.textContent = result.error.message;
                                                errorElement.classList.add('visible');
                                            }
                                        }

                                        card.on('change', function(event) {
                                            setOutcome(event);
                                        });

                                        document.querySelector('form').addEventListener('submit', function(e) {
                                                    $("#proceed").prop("disabled", true);
                                                    e.preventDefault();
                                                    var form = document.querySelector('form');
                                                    var extraDetails = {
                                                            /*name: form.querySelector('input[name=name]').value,
    address_zip: form.querySelector('input[name=zip]').value
  };
  stripe.createToken(card, extraDetails).then(setOutcome);
});

                                    </script>




                                </div>
                                </div>
                                </div>
                                <div class="col-sm-4 col-sm-offset-1">
                                    <div class="form-group">
                                        <img src="assets/img/secure.png">
                                        <p class="description"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Lifetime refund policty</label>
                                        <p class="description">If at any point you feel that the program is not useful then we will refund the entire amount. No questions asked</p>
                                    </div>
                                    <div class="form-group">
                                        <label>You cannot afford $49?</label>
                                        <p class="description">Send us a message at hope@riseasthma.com, briefly describe your situation to us and we will give you access for free</p>
                                    </div>
                                </div>

                                </div>
                                </div>
                                <div class="wizard-footer col-md-4 col-md-offset-4">


                                    <div class="clearfix"></div>
                                </div>
                                </form>
                                </div>
                            </div>
                            <!-- wizard container -->
                        </div>
                    </div>
                    <!-- row -->
                </div>
                <!--  big container -->

                <div class="footer">
                    <div class="container text-center">
                        Already have an account? Sign in <a href="<?php echo SITE_URL; ?>page_login.php">here.</a>
                    </div>
                    <div>
                        <a href="<?php echo SITE_URL;?>"><img src="assets/img/logo.png" class="img-responsive logo-footer"></a>
                    </div>
                </div>
            </div>

    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $('document').ready(function() {
            $("#paypal").click(function() {
                var datastring = $("#payment-form").serialize();
                $('#payment-form').attr('action', "paypal.php");
                $("#payment-form").submit();
            });
        });

    </script>

    </html>

    ***********************************/ ?>
