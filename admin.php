<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <script src="https://cdn.ckeditor.com/4.6.1/standard-all/ckeditor.js"></script>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>

<body>

    <div class="wrapper">

        <?php require("admin/sidebaradmin.php"); ?>


        <div class="main-panel">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">

                        <div class=" col-md-1 navbar-minimize">

                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>

                        </div>
                        <button type="button" class="btn btn-warning btn-fill btn-round btn-icon" data-toggle="modal" data-target="#addnewcaregivermodal">Add new</button>
                        <div class="col-md-9 nav-container">

                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="notification">5</span>
                                        <p class="hidden-md hidden-lg">
                                            Notifications
                                            <b class="caret"></b>
                                        </p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Notification 1</a></li>
                                        <li><a href="#">Notification 2</a></li>
                                        <li><a href="#">Notification 3</a></li>
                                        <li><a href="#">Notification 4</a></li>
                                        <li><a href="#">Another notification</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown dropdown-with-icons">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-list"></i>
                                        <p class="hidden-md hidden-lg">
                                            More
                                            <b class="caret"></b>
                                        </p>
                                    </a>
                                    <ul class="dropdown-menu dropdown-with-icons">
                                        <li>
                                            <a href="#">
                                                <i class="pe-7s-mail"></i> Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="pe-7s-help1"></i> Help Center
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="pe-7s-tools"></i> Settings
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">
                                                <i class="pe-7s-lock"></i> Lock Screen
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="text-danger">
                                                <i class="pe-7s-close-circle"></i> Log out
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>

                        </div>
                    </div>

                </div>

            </nav>

            <?php require("admin/addnew-modal.php"); ?>


            <div class="content">
                <div class="container-fluid">


                    <div class="col-md-10 col-md-offset-1">

                        <div class="card">

                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>

                            <table id="bootstrap-table2" class="table">
                                <thead>
                                    <th data-field="state" data-checkbox="true"></th>
                                    <th data-field="id" class="text-center">ID</th>
                                    <th data-field="name" data-sortable="true">Name</th>
                                    <th data-field="salary" data-sortable="true">Date registered</th>
                                    <th data-field="country" data-sortable="true">Who needs help</th>
                                    <th data-field="city">Sent to doctor</th>
                                    <th data-field="actions" class="td-actions text-right" data-events="operateEvents" data-formatter="operateFormatter">Actions</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>Dakota Rice</td>
                                        <td>13.05.2017</td>
                                        <td>Herself</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>Dakota Rice</td>
                                        <td>13.05.2017</td>
                                        <td>Herself</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>Dakota Rice</td>
                                        <td>13.05.2017</td>
                                        <td>Herself</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>Dakota Rice</td>
                                        <td>13.05.2017</td>
                                        <td>Herself</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>7</td>
                                        <td>Alden Chen</td>
                                        <td>$63,929</td>
                                        <td>Finland</td>
                                        <td>Gary</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1</td>
                                        <td>Dakota Rice</td>
                                        <td>13.05.2017</td>
                                        <td>Herself</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <!--  end card  -->


                    </div>
                </div>




            </div>


            <?php require("common/footer.php"); ?>

        </div>
    </div>


</body>
<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="../assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="../assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="../assets/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="../assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="../assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="../assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="../assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="../assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="../assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  bootstrap Table Plugin    -->
<script src="../assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="../assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="../assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="../assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>
<script type="text/javascript">
    var $table = $('#bootstrap-table2');

    function operateFormatter(value, row, index) {
        return [
            '<a rel="tooltip" title="View" class="btn btn-simple btn-info btn-icon table-action view" href="javascript:void(0)">',
            '<i class="fa fa-image"></i>',
            '</a>',
            '<a rel="tooltip" title="Edit" class="btn btn-simple btn-warning btn-icon table-action edit" href="javascript:void(0)">',
            '<i class="fa fa-edit"></i>',
            '</a>',
            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)">',
            '<i class="fa fa-remove"></i>',
            '</a>'
        ].join('');
    }

    $().ready(function() {
        window.operateEvents = {
            'click .view': function(e, value, row, index) {
                info = JSON.stringify(row);

                swal('You click view icon, row: ', info);
                console.log(info);
            },
            'click .edit': function(e, value, row, index) {
                info = JSON.stringify(row);

                swal('You click edit icon, row: ', info);
                console.log(info);
            },
            'click .remove': function(e, value, row, index) {
                console.log(row);
                $table.bootstrapTable('remove', {
                    field: 'id',
                    values: [row.id]
                });
            }
        };

        $table.bootstrapTable({
            toolbar: ".toolbar",
            clickToSelect: true,
            showRefresh: true,
            search: true,
            showToggle: true,
            showColumns: true,
            pagination: true,
            searchAlign: 'left',
            pageSize: 8,
            clickToSelect: false,
            pageList: [8, 10, 25, 50, 100],

            formatShowingRows: function(pageFrom, pageTo, totalRows) {
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber) {
                return pageNumber + " rows visible";
            },
            icons: {
                refresh: 'fa fa-refresh',
                toggle: 'fa fa-th-list',
                columns: 'fa fa-columns',
                detailOpen: 'fa fa-plus-circle',
                detailClose: 'fa fa-minus-circle'
            }
        });

        //activate the tooltips after the data table is initialized
        $('[rel="tooltip"]').tooltip();

        $(window).resize(function() {
            $table.bootstrapTable('resetView');
        });


    });

</script>


</html>
