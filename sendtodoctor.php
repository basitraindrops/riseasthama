<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')==""  && $sessionObj->read("front_user_id")=="" && $sessionObj->read("front_login_as")=="")
{
	header("Location:page_login.php");
}
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'SENDTODOCTOR';
require_once('common/progress_show.php');

if($sessionObj->read('front_user_error') != '' ){
    $error =  $sessionObj->read('front_user_error'); 
}else{
    $error = '';
}
if(isset($_POST['doctor_name'])){
    
    $settings=new settings($mysqli);
    $settings->get_by_id(4);
    //require_once CLASS_DIR.'front_user_doctor.class.php';
    //require_once(CLASS_DIR.'app_setting.class.php');
    //require_once(INC_DIR."functions.php");
    $settings=new settings($mysqli);
    $settings->get_by_id(4);
    //$user_obj = new front_user_doctor($mysqli);
    $encryptObj = new encryption();
    $password = $_POST['doctor_email'];//$encryptObj->random_password(8);
    $created_by = decrypt($sessionObj->read("front_user_email"),$settings->salt_key);
    //$insert = $user_obj->insert($user_nama=$_POST['doctor_name'], $_POST['doctor_email'], $user_name=$_POST['doctor_name'], $user_address='', $province_id=0, $kota_id=0, $no_telp='', $no_hp='', $no_npwp='', $no_ktp_sim='', $tempat_lahir='', $tanggal_lahir=date("Y-m-d H:i:s"), $jenis_kelamin='', $pendidikan=0, $pekerjaan=0, $nama_perusahaan='', $jabatan='', $user_password=$encryptObj->encrypt_password($password), $forgot_password_key='', $forgot_status=0, $status=1,$created_by );
    require_once CLASS_DIR.'front_user_patient.class.php';
    $up=new front_user_patient($mysqli);
    $encryptObj = new encryption();
    $insert = $up->insert($user_nama=$_POST['doctor_name'], $_POST['doctor_email'], $user_name=$_POST['doctor_name'], $user_address='', $province_id=0, $kota_id=0, $no_telp='', $no_hp='', $no_npwp='', $no_ktp_sim='', $tempat_lahir='', $tanggal_lahir=date("Y-m-d H:i:s"), $jenis_kelamin='', $pendidikan=0, $pekerjaan=0, $nama_perusahaan='', $jabatan='', $user_password=$encryptObj->encrypt_password($_POST['doctor_email']), $forgot_password_key='', $forgot_status=0, $payment_register_type='1', $age='', $country='', $status=1, $created_by = $created_by, $user_role = 'doctor');
     require 'inc/mailer/PHPMailerAutoload.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Set who the message is to be sent from
    $mail->setFrom(ADMIN_EMAIL, 'asthmamethod.com');
    //Set an alternative reply-to address
    $mail->addReplyTo(ADMIN_EMAIL, 'asthmamethod.com');
    //Set who the message is to be sent to
    $mail->addAddress($_POST['doctor_email'], $_POST['doctor_name']);
    //Set the subject line
    $mail->Subject = 'Share RISE with you';
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $message = 'Hi '.$_POST['doctor_name']."<br/>";
    $message.= "One of your Patient share RISE Asthma method with you.<br/><br/>Please find below your Login Detail<br/><br/>Login Detail : <br/>Email : ".$_POST['doctor_email']."<br/>Password : ".$password."<br/><br/>For login click <a href='".SITE_URL."/page_login.php'>here</a>";
    $message.= "Regards<br/><br/>";
    $message.= "Web Admin";
    $mail->msgHTML($message);
    //Replace the plain text body with one created manually
    $mail->AltBody = 'Email from asthmamethod.com';
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    $mail->isSendmail();
    $mail->Sendmail = '/usr/sbin/sendmail';
    //$mail->isSMTP();

    //send the message, check for errors
    if (!$mail->send()) {
        //die( "Mailer Error: " . $mail->ErrorInfo);

    }
    $error = 'Successfull invitation send!';
    //$sessionObj->write('front_user_error','Successfull invitation send!');
    /*header('Location: sendtodoctor.php');
    exit;*/
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <!-- <link href="../assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <!-- <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="<?php echo SECURE ?>://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='<?php echo SECURE ?>://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!-- <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <?php require('common/header.php'); ?>
</head>

<body>

    <div class="wrapper">

        <?php require("common/sidebar.php"); ?>


        <div class="main-panel">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">

                        <div class=" col-md-1 navbar-minimize">

                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
                        <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                        <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                    </button>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <?php require("common/notifications.php"); ?>
                                <?php require("common/topsettings.php"); ?>

                            </ul>

                        </div>
                    </div>

                </div>

            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <div class="header-intro">
                                    <!-- <img class="img-responsive" src="../assets/img/sendtodoctor-icon.png" style="margin: 0 auto;" /> -->
                                    <img class="img-responsive" src="assets/img/sendtodoctor-icon.png" style="margin: 0 auto;" />
                                    <h1>Share RISE with your doctor</h1>
                                    <p>We want you to work with your doctor to make the most out of RISE. Fill in the form bellow and your doctor will receive an invitation to join RISE at no cost</p>
                                </div>
                            </div>

                            <form method="post" action="">

                                <div class="form-group">
                                    <label>What is your doctor's name?</label>
                                    <input type="text" name="doctor_name" placeholder="Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>What is their email address?</label>
                                    <input type="email" name="doctor_email" placeholder="Enter email" class="form-control">
                                </div>


                                <div class="footer text-center"><br/>
                                    <button type="submit" class="btn btn-fill btn-warning btn-wd" onclick="demo.showNotification('top','center')" style="margin-bottom: 40px;">Send</button>
                                </div>
                            </form>



                        </div>


                    </div>
                </div>
            </div>
            <?php //require("common/footer.php"); ?>
        </div>
        <!-- container fluid    -->



    </div>



</body>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>
<?php 
    
    if($error != ''){
    ?>
<script type="text/javascript">
    $.notify({
        message: "<?php echo $error; ?>"
    }, {
        type: 'success'
    });

</script>
<?php 
   //$sessionObj->destroy('front_user_error');
 }  ?>

</html>

