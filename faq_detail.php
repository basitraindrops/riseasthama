<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')=="" && $sessionObj->read("front_user_id")=="" && $sessionObj->read("front_login_as")=="" )
{
    header("Location:page_login.php");
}


require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'FAQ_DETAIL';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <!-- <link href="../assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />


    <!--  Light Bootstrap Dashboard core CSS    -->
    <!-- <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>assets/css/faq.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!-- <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style type="text/css">


    </style>
    <?php require('common/header.php'); ?>
</head>

<body>

    <?php require("common/sidebar.php"); ?>


    <div class="wrapper" id="wrapper">


        <div class="main-panel" id="main">

            <?php require("common/top_new.php");
             ?>
            <?php 
              
                $f_id = $_REQUEST['id'];
                require_once(CLASS_DIR."faq_topic.class.php");
                $pb=new faq_topic($mysqli);
                $pb->faq_detail_topic($f_id);   
                
                //echo "<pre>";print_r($data);exit; ?>
            <div class="content">
                <div class="faq_search">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <input type="text" name="search" id="txt_search" placeholder="Search">
                    <input type="hidden" name="id" id="f_id" value=<?php echo $_REQUEST[ 'id']; ?> >
                </div>
                <div class="faq_breadcum">
                    <a href="faq.php" class="all_faq">All Topics</a> > <a href="" class="child_faq"><?php echo $pb->faq_cat_name; ?></a>
                </div>
                <div class="faq_main" id="default">

                    <div style="width:100%">

                        <?php 
                require(CLASS_DIR."faq.class.php");
                $pb=new faq($mysqli);
                $f_id=$_REQUEST['id'];
                $data=$pb->faq_detail($f_id);
               //echo "<pre>";print_r($data);exit;
              for($i=0; $i<count($data); $i++) { 
               // $result = substr($data[$i]->faq_cat_des, 0, 200);?>
                        <div class="block_faq block_new">
                            <div class="img_block detail_faq">


                                <a href="faq_main_detail.php?id=<?php echo $data[$i]->faq_cat_id; ?>">
                                    <h4>
                                        <?php echo $data[$i]->faq_cat_name; ?>
                                    </h4>
                                </a>
                                <p style="font-size:12px;">
                                     <?php
                                           
                                            $string =  htmlspecialchars_decode($data[$i]->faq_cat_des);
                                             if (strlen($string) > 170) {                   
                                                 $stringCut = substr($string, 0, 170);
                                                 $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'......'; 
                                                } 
                                             ?>
                                            <?php echo $string ?>
                                </p>
                            </div>


                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            <!-- container fluid    -->

            <?php //require("common/footer.php"); ?>

        </div>
    </div>


</body>
<script>
    $('#txt_search').change(function() {
        var txt = document.getElementById('txt_search').value;
        var id = document.getElementById('f_id').value;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "search_faq_detail.php",
            data: {
                txt_val: txt,
                f_id: id
            },
            success: function(result) {
                var htmlText = '';
            
              
                htmlText += '<div style="width:100%;">';
                 if (result == "" || result == 'undefined' || result == undefined) 
                {
                    htmlText += '<p>No result found!</p>';
                }
                else
                {
                    for (var key in result) {

                        htmlText += ' <div class="block_faq block_new"> <div class="img_block detail_faq">';
                        htmlText += ' <a href="faq_main_detail.php?id=' + result[key].id + '"> <h4> ' + result[key].name + '</h4></a>';
                        htmlText += '<p style="font-size:12px"> ' + result[key].des + '</p></div>';
                        
                        htmlText += '</div>';
                     }
                }
               
                htmlText += '</div>';
                htmlText += '</div>';
                $('#default').html('');
                $('#default').append(htmlText);
            }

        });
    });

</script>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>



</html>
