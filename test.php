</!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.2.1.js" type="text/javascript"></script>

<script src="https://player.vzaar.com/libs/flashtakt/client.js" type="text/javascript"></script>

<iframe id="vzvd-10517314" name="vzvd-10517314" title="video player" class="video-player" type="text/html" width="576" height="324" frameborder="0" allowFullScreen allowTransparency="true" mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/10517314/player?apiOn=true"></iframe>

<div id="play_button">Play</div>

<div id="get_time_button">get_time_button</div>



<script type="text/javascript">
   
    window.addEventListener("load", function() {
        var vzp = new vzPlayer("vzvd-10517314");
        // adding play2() API method to DOM element:
        var myPlayButton = document.getElementById("play_button");


        vzp.ready(function(){

            vzp.pause();
            
            myPlayButton.addEventListener("click", function() {
                //vzp.loadVideo(10517314)
                console.log('PLAY');
                vzp.play2();
            });


            // adding getTime(callback) API method to DOM element:
            var getTimeButton = document.getElementById("get_time_button");
            getTimeButton.addEventListener("click", function() {
                vzp.getTime(function(time) {
                    console.log(time);
                });
            });


            // set up "playState" event listener for player status:
            vzp.ready(function() {
                vzp.addEventListener("progress", function(state) {
                    console.log(state);
                });
            });
        });
    });
</script>
</body>
</html>