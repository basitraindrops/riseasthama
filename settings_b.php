<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')=="")
{    
	header("Location:page_login.php");
}


require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
$error2="";


$settings=new settings($mysqli);
$settings->get_by_id(4);


//echo $sessionObj->read('email');
if(isset($_POST['new_password']))
{
	if($sessionObj->read("front_login_as")=="patient")
	{
		require_once(CLASS_DIR.'front_user_patient.class.php');
		$user_obj=new front_user_patient($mysqli);
		$encryptObj = new encryption();
		if($user_obj->loginby_email_password($front_user_email,$_POST['old_password']))
		{
			$user_obj=new front_user_patient($mysqli);
			$user_obj->update_password($encryptObj->encrypt_password($_POST['new_password']),$front_user_email,$front_user_id);
			$error="Success updating password!";
		}
		else
		{
			$error2="Error. Old Password is wrong!";
		}
	}
	elseif($sessionObj->read("front_login_as")=="doctor")
	{
		require_once(CLASS_DIR.'front_user_doctor.class.php');
		$user_obj=new front_user_doctor($mysqli);
		$encryptObj = new encryption();
		if($user_obj->loginby_email_password($front_user_email,$_POST['old_password']))
		{
			$user_obj=new front_user_doctor($mysqli);
			$user_obj->update_password($encryptObj->encrypt_password($_POST['new_password']),$front_user_email,$front_user_id);
			$error="Success updating password!";
		}
		else
		{
			$error2="Error. Old Password is wrong!";
		}
	}
}

//update user details
if(isset($_POST['user_update']))
{   


    $error = '';
    $user_obj->get_by_user_email(decrypt($sessionObj->read("front_user_email"),$settings->salt_key));

    $user_obj->age = $_POST['age'];
    $user_obj->country = $_POST['country'];
    $user_obj->user_name = $_POST['user_name'];
    $old_email = $user_obj->user_email;
    if($user_obj->user_email != $_POST['user_email']){
        if(!$user_obj->updateMail($_POST['user_email'])){
            $error .= "<br>email is already exist.";
        }
    }
    if($error == ''){
        if($user_obj->save()){

            if($old_email != $_POST['user_email']){             
                //$sessionObj->destroy('front_user_email');        
                $sessionObj->write('front_user_email',encrypt($_POST['user_email'],$settings->salt_key));
                setcookie('front_email', encrypt($_POST['user_email'],$settings->salt_key), time() + (86400 * 30), sys_get_temp_dir());           
            }

            $error="Success updating datail!";
        }
    }
}

//update user deails


if($sessionObj->read("front_login_as")=="patient")
{
	require_once(CLASS_DIR.'front_user_patient.class.php');
	$user_obj=new front_user_patient($mysqli);
	$user_obj->get_by_user_email(decrypt($sessionObj->read("front_user_email"),$settings->salt_key));

}
else
{
	require_once(CLASS_DIR.'front_user_doctor.class.php');
	$user_obj=new front_user_doctor($mysqli);
	$user_obj->get_by_user_email(decrypt($sessionObj->read("front_user_email"),$settings->salt_key));

}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<?php echo SITE_URL; ?>assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <script src="https://cdn.ckeditor.com/4.6.1/standard-all/ckeditor.js"></script>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo SITE_URL; ?>assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>

<body>

    <div class="wrapper">
        <?php require("common/sidebar.php"); ?>
        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div class=" col-md-1 navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
                        </div>
                        <div class="col-md-9 nav-container">
                            <ul class="nav nav-icons" role="tablist">
                                <li class="active">
                                    <a href="#myprofile" role="tab" data-toggle="tab">
                                      My profile
                                    </a>
                                </li>                                
                                <li>
                                    <a href="#changepassword" role="tab" data-toggle="tab">
                                      Change password
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <?php require("common/notifications.php"); ?>
                                <?php require("common/topsettings.php"); ?>


                        </div>
                    </div>

                </div>

            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="tab-content">

                            <div class="tab-pane active" id="myprofile">
                                <div class="card">
                                    <div class="header sendtodoctor">
                                        <form method="post" action="">
                                            <input type="hidden" name="user_update" value="1">
                                            <div class="form-group">
                                                <div class="avtar">
                                                    <img data-name="<?php echo isset($user_obj->user_name)?$user_obj->user_name:'';?>" class="profile"/> 
                                                </div>
                                                <label>E-mail</label>
                                                <input name="user_email" value="<?php echo isset($user_obj->user_email)?$user_obj->user_email:'';?>" placeholder="Enter current email" class="form-control">
                                            

                                                <label>Name</label>
                                                <input name="user_name" value="<?php echo isset($user_obj->user_name)?$user_obj->user_name:'';?>" placeholder="name" class="form-control">

                                                <label>Age</label>
                                                <input name="age" value="<?php echo isset($user_obj->age)?$user_obj->age:'';?>" placeholder="What is your age?" class="form-control">

                                                <label>Country</label>
                                                <input name="country" value="<?php echo isset($user_obj->country)?$user_obj->country:'';?>" placeholder="What country do you live in" class="form-control">

                                            </div>
                                            <div class="footer text-center"><br/>
                                                <button type="submit" class="btn btn-fill btn-warning btn-wd" >Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                          

                            <div class="tab-pane" id="changepassword">
                                <div class="card">
                                    <div class="header sendtodoctor">
                                        <form method="#" action="#">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="email" placeholder="Enter current password" name="current_password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>New password</label>
                                                <input type="password" placeholder="Enter new password" class="form-control">
                                            </div>
                                            <div class="footer text-center"><br/>
                                                <button type="submit" class="btn btn-fill btn-warning btn-wd" onclick="demo.showNotification('top','center')">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- container fluid    -->

        <?php require("common/footer.php"); ?>

    </div>
    </div>


</body>
<?php require("common/includejs.php"); ?>

</html>
