<?php 
    include 'inc/config.php'; // Configuration php file
    require(INC_DIR.'init.php');
    if($sessionObj->read('front_user_email')<>"")
    {
    	header("Location:dashboard.php");
    }
    require(CLASS_DIR.'encryption.class.php');
    require_once(CLASS_DIR.'security.class.php');
?>

<!DOCTYPE html>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <link rel="stylesheet" type="text/css" href="assets/css/index_page.css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>RISE Asthma Method</title>
    <?php require('common/header.php'); ?>
</head>
<style type="text/css">
.disable{
 opacity: 0.2;
 pointer-events: auto;
}

</style>
<body style="padding:0px;">
<section id="top">
	<img class="top_logo" src="assets/img/logo3.png">
</section>
<section id="welcome_page">
		<form class="welcomepage_form" method="post" name="registration" id="payment-form" action="intro.php">
				<div class="top_first">
				<div class="first_section">
						<div class="first_left">
							<h1>welcome to RISE</h1>
							<h3>who need help?</h3>
							<button class="my_self btn-first">My Self</button>
							<button class="select_one btn-first">A Loved one</button>
							<a class="a_first-section" href="<?php echo SITE_URL; ?>page_login.php">Already have an account?<span> sign in now.</span></a>
						</div>
						<div class="first_right">
						   <img class="bgimg" src="assets/img/drink.png">
						</div>
				</div>
				</div>
				<div class="second_section">
					<div class="second_left">
						<h1>Your Rise Profile</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non dictum dolor, vel elementum lorem. </p>
						<label>what is your name?</label><input type="text" placeholder="e.g.John Smith" name="inputname" class="form-txt" autocomplete="off" id="inputname" onkeypress="return validateName();"/>
						<label id="name-error" class="error" for="name"></label>
					    <label>what is your email address?</label><input type="text" placeholder="your@email.com"  name="email" id="email" onkeypress='return validateDetail();' class="form-txt" autocomplete="off"/>
					     <label id="email-error" class="error" for="email"></label>
					    <button class="next_btn btn-second">next <i class="fa fa-angle-right" aria-hidden="true"></i>
</button>
					    
					</div>
					<div class="second_right">
					<div class="second_right_img">
						<img src="assets/img/2.png">
					</div>
						
						<h3>You're in good company!</h3>
						<p>Over 213,451 user use RISE to analye 213,565 sites</p>

					</div>
				</div>
				<div class="third_section">
					<div class="third_left">
						<h1>let's customize your account.....</h1>
						<p>Tell us about yourself and your organization so we can customize your account just for you:</p>
						<img src="assets/img/creditcard.png">
						<input  id="stripe_number" name="stripe_number" placeholder="Card number"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onfocus="" onkeypress='' class="input_cardno form-txt">
						<div class="input_bottom">
						<input old_val="2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" name="stripe_expirydate" placeholder="MM/YY" maxlength="5" id="expirydate"  class="input_date form-txt" onkeypress='return validateExpire(event);'>
						 <input type="text" name="stripe_cvc" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="CVV" class="input_cvv form-txt" onkeypress='return validateCVV(event);' maxlength="3" minlength="3">
						</div>
						<div id="card-errors" class="error">
						<label id="credit_card-error" class="error col-sm-12" for="credit_card"></label><br/>
						<label id="expirydate-error" class="error col-sm-12" for="credit_card"></label><br/>
						<label id="cvv-error" class="error col-sm-12" for="credit_card"></label>
						</div>
						<button class="pay_btn btn-credit" type="submit">pay $49</button>
						<div class="over_hr">
							<span class="or">OR</span>
							<hr class="">
						</div>
						<button class="paypal_btn btn-paypal" type="submit">Pay with Paypal</button>
						<a href=""> by proceeding, you are agree with our <span>tearms and condition</span></a>
					</div>
					<div class="third_right">
						<img src="assets/img/3.png">
						<h3>stop guessing....stop seeing!</h3>
						<p>in just 5 minutes you 'll be abel to discover and replay your visitors journys.</p>
					</div>
				</div>
		  </form>
</section>
 <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
 <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="https://js.stripe.com/v2/"></script>
<script src="assets/js/register-validation.js" type="text/javascript"></script>

</body>
