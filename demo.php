<!DOCTYPE html>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <link rel="stylesheet" type="text/css" href="assets/css/page.css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php require('common/header.php'); ?>
</head>
<body>
<section id="top_head">
	<img class="top_logo" src="assets/img/logo.png">
</section>
<section id="welcome_page">
			<form class="welcomepage_form" method="post" name="registration" id="payment-form" action="intro.php">
				<div class="first_section">
						<div class="first_left">
							<h1>welcome to RISE</h1>
							<h3>who need help?</h3>
							<button class="btn-first">My Self</button>
							<button class="btn-first">A Loved one</button>
							<a href="#">Already have an account? <ul>sign in now.</ul></a>
						</div>
						<div class="first_right">
						   <img class="bgimg" src="assets/img/drink.png">
						</div>
				</div>
				<div class="second_section">
					<div class="second_left">
						<h1>Your Rise Profile</h1>
						<label>what is your name?</label><input type="text" placeholder="e.g.John Smith" name="inputname"/>
					    <label>what is your email address?</label><input type="text" placeholder="your@email.com" name="email" id="email" onkeypress='return validateDetail();'/>
					   	 <label id="email-error" class="error" for="email"></label>
					    <button class="btn-second">next </button>

					    <a href="#">Already have an account? <ul>sign in now.</ul></a>
					</div>
					<div class="second_right">
						<img src="assets/img/2.png">
						<h3>You're in good company!</h3>
						<p>over 213,451 user use RISE to analye 213,565 sites</p>

					</div>
				</div>
				<div class="third_section">
					<div class="third_left">
						<h1>let's customize your account.....</h1>
						<p>tell us about yourself and your organization so we can customize your account just for you:</p>
						<img src="assets/img/creditcard.png">
						 <input id="stripe_number" name="stripe_number" placeholder="Card number"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onfocus="" onkeypress=''>
						<input old_val="2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" name="stripe_expirydate" placeholder="MM/YY" maxlength="5" id="expirydate" onkeypress='return validateExpire(event);'>
						 <input type="text" name="stripe_cvc" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="CVV" class="form-control credit-validation " onkeypress='return validateExpire(event);' maxlength="3" minlength="3">
						
						<div id="card-errors" class="error">
						<label id="credit_card-error" class="error col-sm-12" for="credit_card"></label>
						<label id="expirydate-error" class="error col-sm-12" for="credit_card"></label>
						<label id="cvv-error" class="error col-sm-12" for="credit_card"></label>
						</div>
						<button class="btn-credit" type="submit">pay $49</button>
						<button class="btn-paypal">Pay with Paypal</button>
						<a href=""> by proceeding, you are agree with our <span>tearms and condition</span></a>
					</div>
					<div class="third_right">
						<img src="assets/img/3.png">
						<h3>stop guessing....stop seeing!</h3>
						<p>in just 5 minutes you 'll be abel to discover and replay your visitors journys.</p>
					</div>
				</div>
		  </form>
</section>

 <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
 <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/js/register-validation.js" type="text/javascript"></script>

</body>
