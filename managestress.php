<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')==""   && $sessionObj->read("front_user_id")=="" && $sessionObj->read("front_login_as")=="" )
{
    header("Location:page_login.php");
}


require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');


$page_name = 'MANAGE_STRESS';
require_once('common/progress_show.php');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="../assets/img/favicon.ico"> -->
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <!-- <link href="../assets/css/bootstrap.min.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <!-- <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />
    <link href="<?php echo SITE_URL; ?>assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>assets/css/faq.css" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link href="<?php echo SECURE ?>://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='<?php echo SECURE ?>://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <!-- <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" /> -->
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?php require('common/header.php'); ?>    
</head>

<body>

    <div class="wrapper">

        <?php require("common/sidebar.php"); ?>


        <div class="main-panel">

            <?php require("common/topnav.php"); ?>

            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="tab-content">
                        <div class="tab-pane active" id="fullchapter">
                            <?php require("method/stress/fullchapter.php"); ?>
                        </div>
                            <?php require("method/stress/textsummary.php"); ?>
                            <?php require("method/stress/faq_detail.php"); ?>

                        </div>

                    </div>
                </div>
                <!--  col -->
                 <div class="content">
                    <a href="summary.php">
                        <p><em>Next Lession</em></p>
                        <h6>EVERYTHING TOGETHER</h6>
                        <b class="caret"></b>
                    </a>
                </div>



            </div>
            <!-- container fluid    -->

            <?php //require("common/footer.php"); ?>

        </div>
    </div>


</body>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>

</html>
