<?php
include 'inc/config.php'; // Configuration php file
require(CLASS_DIR.'user.class.php');
require(INC_DIR.'init.php');
//$sessionObj=new SecureSession();
//$sessionPath = sys_get_temp_dir();
//session_save_path($sessionPath);
//session_start();
if($sessionObj->read('front_user_email')!=""  && $sessionObj->read("front_user_id") !=""  )
{	$user=new user($mysqli);
	if($sessionObj->read('user_email')){
		$user->logout($sessionObj->read('user_email'));
	}else{
		$user->logout($sessionObj->read('front_user_email'));
	}
	//$_SESSION['email']="";

	$sessionObj->destroy('front_user_email');	
	$sessionObj->destroy('front_login_as');
	setcookie("front_email", "", time() - 3600);
	session_destroy();
	header("Location:page_login.php");
}else{
	header("Location:index.php");
}

?>