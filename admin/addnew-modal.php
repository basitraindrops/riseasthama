<div id="addnewcaregivermodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="container-fluid">
                <div class="header sendtodoctor">
                    <form method="#" action="#">
                        <div class="form-group">
                            <br/>
                            <label>Name</label>
                            <input type="name" placeholder="Client name" class="form-control">
                            <br/>
                            <label>E-mail</label>
                            <input type="age" placeholder="Client e-mail address" class="form-control">
                            <br/>

                        </div>
                        <div class="footer text-center"><br/>
                            <button type="submit" class="btn btn-fill btn-warning btn-wd" onclick="demo.showNotification('top','center')">Create new</button>

                            <br/>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
