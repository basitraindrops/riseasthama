<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user_group.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['menu_id'])&&isset($_POST['user_group_id'])&&isset($_POST['action']))
{
	if($_POST["action"]=="remove")
	{
		require_once(CLASS_DIR."user_group_menu.class.php");
		$prv=new user_group_menu($mysqli);
		$prv->delete($_POST['menu_id'],$_POST['user_group_id']);
	}
	else if($_POST["action"]=="add")
	{
		require_once(CLASS_DIR."user_group_menu.class.php");
		$prv=new user_group_menu($mysqli);
		$prv->get1($_POST['menu_id'],$_POST['user_group_id']);
		if($prv->user_group_id<>null)
		{
			$prv->delete($_POST['menu_id'],$_POST['user_group_id']);
		}
		else
		{
			$prv->insert($_POST['menu_id'],$_POST['user_group_id'],$sessionObj->read('user_name'));
		}
	}
}
?>