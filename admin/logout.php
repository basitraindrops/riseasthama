<?php
include 'inc/config.php'; // Configuration php file
require_once(CLASS_DIR.'user.class.php');
require_once(INC_DIR.'init.php');
//$sessionObj=new SecureSession();
//$sessionPath = sys_get_temp_dir();
//session_save_path($sessionPath);
//session_start();

$user=new user($mysqli);
$user->logout($sessionObj->read('user_email'));

//$_SESSION['email']="";
$sessionObj->destroy('user_email');
$sessionObj->destroy('user_name');
$sessionObj->destroy('user_id');
$sessionObj->destroy('first_name');
$sessionObj->destroy('last_name');
setcookie("email", "", time() - 3600);
session_destroy();
header("Location:index.php");
?>