<?php 
include 'inc/config.php'; // Configuration php file
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')!="")
{
	header("Location:index.php");
}


require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

//session_start();
//include("simple-php-captcha.php");
//$_SESSION['captcha'] = simple_php_captcha();

/* new password
$enc = new encryption();
echo $enc->encrypt_password("P@ssw0rd");
 */

//echo $sessionObj->read('captcha_code');
//echo date("Y-m-d H++7:i:s");
$error_message="";
$status = true;
//$captcha = @$_POST['captcha']; // the user's entry for the captcha code

//echo $captcha."xx";
// Only try to validate the captcha if the form has no errors
// This is especially important for ajax calls

	if(isset($_POST['login_email'])&& isset($_POST['login_password']))
	{
		if($_POST['login_email']=="")
		{
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please Fill your Email!
							</div>';
							$status = false;
		}
		if($_POST['login_password']=="")
		{
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please Fill your Password!
							</div>';
							$status = false;
		}
		//echo $_POST['captcha_code']. " ".$sessionObj->read('captcha_code') ;
		/*if(strcasecmp($sessionObj->read('captcha_code'), $_POST['captcha_code']) != 0){  
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please enter correct captcha!
							</div>';
							$status = false;
		}else{// Captcha verification is Correct. Final Code Execute here!		
		}*/

		if($status == true)
		{
			$user_obj = new user($mysqli);
			require_once(CLASS_DIR.'app_setting.class.php');
			require_once(INC_DIR."functions.php");
			$settings=new settings($mysqli);
			$settings->get_by_id(1);
			if($user_obj->loginby_email_password($_POST['login_email'],$_POST['login_password']))
			{
//				die(print_r($user_obj));
				$sessionObj->write('user_email',encrypt($user_obj->user_email,$settings->salt_key));
				$sessionObj->write('user_name',encrypt($user_obj->user_name,$settings->salt_key));
				$sessionObj->write('user_id',encrypt($user_obj->user_id,$settings->salt_key));
				$sessionObj->write('first_name',$user_obj->user_first_name);
				$sessionObj->write('last_name',$user_obj->user_last_name);
//				die($sessionObj->read('user_email').$sessionObj->read('user_name').$sessionObj->read('user_id'));
				if(isset($_POST['remember_me']))
				{
					if($_POST['remember_me']=="Yes")
					{
						setcookie('email', encrypt($user_obj->user_email,$settings->salt_key), time() + (86400 * 30), sys_get_temp_dir());
//						die($_COOKIE['email'])
					}
				}
//				die("mau redirect");
				header("Location:index.php");
			}
			else
			{
				$error_message.='<div class="alert alert-danger">
									<i class="fa fa-bell-o"></i>  Username or Password wrong!
								</div>';
								$status = false;

			}
			
		}
	}

?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">

    <title>Rise Asthmamethod Administration - LOGIN</title>

    <meta name="description" content="<?php echo $template['description'] ?>">
    <meta name="author" content="<?php echo $template['author'] ?>">
    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo SITE_URL;?>assets/img/logosettings.png">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="<?php echo ADMIN_URL;?>img/icon152.png" sizes="152x152">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>css/bootstrap.css">

    <!-- Related styles of various javascript plugins -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>css/plugins.css">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>css/main.css">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
    <script src="<?php echo ADMIN_URL;?>js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>

<body class="login">
    <!-- Login Container -->
    <div id="login-container">
        <div id="login-logo" style="background-color: green;">
            <a href="">
                    <img src="<?php echo SITE_URL;?>assets/img/logo.png" width="200" alt="logo">
                </a>
            <h3><b>Administrator Area</b></h3>
        </div>

        <!-- Login Buttons 
            <div id="login-buttons">
                <h5 class="page-header-sub">Login with..</h5>
                <button id="login-btn-facebook" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</button>
                <button id="login-btn-twitter" class="btn btn-info"><i class="fa fa-twitter"></i> Twitter</button>
                <button id="login-btn-email" class="btn btn-default">or Email <i class="fa fa-envelope"></i></button>
            </div>-->
        <!-- END Login Buttons -->

        <!-- Login Form -->
        <form id="login-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal">
            <div class="form-group">
                <?php if($status==false) { echo $error_message; }?>
                <!--<a href="javascript:void(0)" class="login-back"><i class="fa fa-arrow-left"></i></a>-->
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <input type="text" id="login-email" name="login_email" placeholder="Email or Username.." class="form-control" value="<?php echo isset($_POST['login_email']) ? $_POST['login_email'] : '';  ?>" autocomplete="off">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <input type="password" id="login-password" name="login_password" placeholder="Password.." class="form-control" autocomplete="off">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                    </div>
                </div>
            </div>
            <?php /* ?>
            <img src="captcha.php?rand=<?php echo rand();?>" id='captchaimg'><br/> Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh.</td>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">

                        <input type="text" id="captcha_code" name="captcha_code" placeholder="Prove you're human.." class="form-control" autocomplete="off">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                    </div>
                </div>
            </div>
            <?php */ ?>
            <div class="clearfix">
                <div class="btn-group btn-group-sm pull-right">
                    <button type="button" id="login-button-pass" class="btn btn-warning" data-toggle="tooltip" title="Forgot pass?"><i class="fa fa-lock"></i></button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i> Login</button>
                </div>
                <div class="input-switch pull-left" data-toggle="tooltip" title="Remember me" data-on="success" data-off="danger" data-on-label="<i class='fa fa-check fa-white'></i>" data-off-label="<i class='fa fa-times'></i>">
                    <input type="checkbox" name="remember_me" value="Yes">
                </div>
                <div class="col-xs-12" style="text-align:right;">
                    <br/><br/><br/>
                    <!--					<a href="register_perusahaan.php">Pendaftaran Perusahaan &gt;&gt;</a>-->
                </div>
            </div>
        </form>
        <div class="form-group">
        </div>
        <!-- END Login Form -->
    </div>
    <!-- END Login Container -->

    <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        !window.jQuery && document.write(unescape('%3Cscript src="<?php echo ADMIN_URL;?>js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));

    </script>

    <!-- Bootstrap.js -->
    <script src="<?php echo ADMIN_URL;?>js/vendor/bootstrap.min.js"></script>

    <!-- Jquery plugins and custom javascript code -->
    <script src="<?php echo ADMIN_URL;?>js/plugins.js"></script>
    <script src="<?php echo ADMIN_URL;?>js/main.js"></script>

    <!-- Javascript code only for this page -->
    <script>
        $(function() {
            var loginButtons = $('#login-buttons');
            var loginForm = $('#login-form');

            // Reveal login form
            $('#login-btn-email').click(function() {
                loginButtons.slideUp(600);
                loginForm.slideDown(450);
            });

            // Hide login form
            $('.login-back').click(function() {
                loginForm.slideUp(450);
                loginButtons.slideDown(600);
            });
        });

    </script>
    <script type='text/javascript'>
        function refreshCaptcha() {
            var img = document.images['captchaimg'];
            img.src = img.src.substring(0, img.src.lastIndexOf("?")) + "?rand=" + Math.random() * 1000;
        }

    </script>
    <!-- <script>
        window.$crisp = [];
        window.CRISP_WEBSITE_ID = "f3895f87-8f79-43d8-81b4-f4f8f59de2d4";
        (function() {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.im/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();

    </script> -->
</body>

</html>
<?php
$mysqli->close();
?>
