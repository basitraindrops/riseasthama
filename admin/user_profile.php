<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
require_once(CLASS_DIR."user_internal.class.php");
require_once(CLASS_DIR."user.class.php");

$error="";
$error2="";

if(isset($_POST['user_first_name']))
{
	//echo $sessionObj->read('email');
	$ui=new user_internal($mysqli);
	$ui->get_by_user_id($user_id);
	$directory="file_ui/".$user_id;
	if(!file_exists($directory))
		mkdir($directory);
	$tmp_directory="tmp/";

	//	echo $_POST['user_photo_file_hidden'],$_POST['user_ktp_file_hidden'];

	//PHOTO FILE
	$moved_file = $directory."/".$_POST['user_photo_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['user_photo_file_hidden'];

	$moved_file2 = $directory."/".$_POST['user_ktp_file_hidden'];
	$uploaded_file2=$tmp_directory.$_POST['user_ktp_file_hidden'];

	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	if(file_exists($uploaded_file2))
		rename($uploaded_file2,$moved_file2);
//	echo $user_name,$user_id,$_POST['ui_nip'];
	$ui->update($_POST['ui_nip']
		,$user_name
		,$ui->ui_id
		);
//			echo $_POST['status']." ".$sessionObj->read('user_name')." ".$sessionObj->read('user_id');
//	$ui->update_status($_POST['status'],$sessionObj->read('user_name'),$sessionObj->read('user_id'));
		$date=date("Y-m-d H:i:s",strtotime($_POST['user_birth_date']));
		//$date=date_format($date,"Y-m-d H:i:s");

		$user=new user($mysqli);
		$user->update($user_name
			,$user_email
			,$_POST['user_first_name']
			,$_POST['user_last_name']
			,$_POST['user_birth_place']
			,$date
			,$_POST['user_address']
			,$_POST['user_ktp_number']
			,$_POST['user_phone_number']
			,$_POST['user_ktp_file_hidden']
			,$_POST['user_photo_file_hidden']
			,$_POST['user_pendidikan_terakhir']
			,$_POST['user_gender']
			,$_POST['user_marital_status']
			,$user_name
			,$user_id
			);

	$error="Success updating data!";
}
$ui_show=new user_internal($mysqli);
$ui_show->get_by_user_id($user_id);
require_once(CLASS_DIR."user.class.php");
$user_show=new user($mysqli);
$user_show->get_by_user_id($user_id);

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="user_internal.php">Internal User</a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Internal User"; } else { echo "Update Internal User"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->
<style type="text/css">
#checklist_user_name
{
	color:red;
	font-weight:bold;
}
#checklist_user_email
{
	color:red;
	font-weight:bold;
}
</style>
    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" class="form-horizontal form-box remove-margin"  enctype="multipart/form-data" >
	<input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Internal User"; } else { echo "Update Internal User"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Internal User"; } else { echo "Update Internal User"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="user_internal.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error2!="") { echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div>";  }?>
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_name">Username *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <?php echo $user_name ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_email">Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <?php echo $user_email; ?><span id="checklist_user_name"></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_first_name">First Name *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_first_name" name="user_first_name" class="form-control" value="<?php echo $user_show->user_first_name;  ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Last Name *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_last_name" name="user_last_name" class="form-control" value="<?php  echo $user_show->user_last_name; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group" id="ktp">
                <label class="control-label col-md-2" for="user_birth_place">Birth Place *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_birth_place" name="user_birth_place" class="form-control" value="<?php  echo $user_show->user_birth_place;  ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_date">Birth Date *</label>
                <div class="col-md-2">
                    <div class="input-group date input-datepicker" data-date="javascript:document.write(date_now());" data-date-format="mm-dd-yyyy">
                        <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                        <input type="text" id="user_birth_date" name="user_birth_date" class="form-control input-datepicker"  value="<?php  $date=date_create($user_show->user_birth_date); echo date_format($date,"m-d-Y");  ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_address">Address *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<textarea class="form-control" name="user_address" id="user_address"><?php  echo $user_show->user_address;  ?>
						</textarea>
					</div>
                </div>
            </div>
            <div class="form-group" id="ktp">
                <label class="control-label col-md-2" for="user_ktp_number">Citizenship Card ID *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_ktp_number" name="user_ktp_number" class="form-control" value="<?php  echo $user_show->user_ktp_number;  ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Phone/Mobile Phone *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_phone_number" name="user_phone_number" class="form-control" value="<?php echo $user_show->user_phone_number;  ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">Photo File *</label>
                <div class="col-md-4">
                    <div class="input-group">
						<div id="user_photo_file_div">
						<?php echo "<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='".$user_show->user_photo_file."'>";?>
						<input type="file" class="form-control" name="user_photo_file" id="user_photo_file"> Maximum 200 KB JPG File<br/>
				<?php  ?>
				<a href="file_ui/<?php echo $user_show->user_id; ?>/<?php echo $user_show->user_photo_file; ?>"><img src="file_ui/<?php echo $user_show->user_id; ?>/<?php echo $user_show->user_photo_file; ?>" width="150"></a>
				<?php  ?>

						</div>
					</div>
				</div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file"></label>
                <div class="col-md-4" id="message_upload_user_photo_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">Citizen Card File *</label>
                <div class="col-md-4">
                    <div class="input-group">
						<div id="user_ktp_file_div">
						<?php echo "<input type='hidden' name='user_ktp_file_hidden' id='user_ktp_file_hidden' value='".$user_show->user_ktp_file."'>"; ?>
						<input type="file" class="form-control" name="user_ktp_file" id="user_ktp_file"> Maximum 200 KB JPG File <br/>
				<?php  ?>
				<a href="file_ui/<?php echo $user_show->user_id; ?>/<?php echo $user_show->user_ktp_file; ?>"><img src="file_ui/<?php echo $user_show->user_id; ?>/<?php echo $user_show->user_ktp_file; ?>" width="150"></a>
				<?php  ?>
						</div>
					</div>
				</div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_ktp_file"></label>
                <div class="col-md-4" id="message_upload_user_ktp_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_pendidikan_terakhir">Latest Education *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<select name="user_pendidikan_terakhir" id="user_pendidikan_terakhir">
						<option value="Other" <?php  if($user_show->user_pendidikan_terakhir=="Other") { echo " selected='selected'"; }  ?>>Other</option>
						<option value="PhD" <?php if($user_show->user_pendidikan_terakhir=="PhD") { echo " selected='selected'"; }  ?>>PhD</option>
						<option value="Master Degree" <?php  if($user_show->user_pendidikan_terakhir=="Master Degree") { echo " selected='selected'"; }  ?>>Master Degree</option>
						<option value="Honours" <?php  if($user_show->user_pendidikan_terakhir=="Honours") { echo " selected='selected'"; }  ?>>Honours</option>
						<option value="Bachelor" <?php if($user_show->user_pendidikan_terakhir=="Bachelor") { echo " selected='selected'"; }  ?>>Bachelor</option>
						<option value="Diploma"<?php  if($user_show->user_pendidikan_terakhir=="Diploma") { echo " selected='selected'"; }  ?>>Diploma</option>
						<option value="Certificate"<?php  if($user_show->user_pendidikan_terakhir=="Certificate") { echo " selected='selected'";  } ?>>Certificate</option>
						<option value="High School" <?php if($user_show->user_pendidikan_terakhir=="High School") { echo " selected='selected'";  } ?>>High School</option>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_gender">Gender *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<label for="user_gender">
                        <input type="radio" id="user_gender" name="user_gender" value="pria" <?php if( $user_show->user_gender=="pria") { echo "checked"; } ?>>Male
                        <input type="radio" id="user_gender" name="user_gender" value="wanita" <?php if( $user_show->user_gender=="wanita") { echo "checked"; } else { echo ""; } ?>>Female
						</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_marital_status">Marital Status *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<select name="user_marital_status" id="user_marital_status">
						<option value="Single" <?php  if($user_show->user_marital_status=="Single") { echo " selected='selected'"; }  ?>>Single</option>
						<option value="Married" <?php if($user_show->user_marital_status=="Married") { echo " selected='selected'"; }  ?>>Married</option>
						<option value="Divorce" <?php if($user_show->user_marital_status=="Divorce") { echo " selected='selected'"; }  ?>>Divorce</option>
						<option value="Widow" <?php if($user_show->user_marital_status=="Widow") { echo " selected='selected'"; }  ?>>Widow</option>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group" id="nip">
                <label class="control-label col-md-2" for="ui_nip">Worker Card ID *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="ui_nip" name="ui_nip" class="form-control" value="<?php echo $ui_show->ui_nip; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="user_internal.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                user_name: {
                    required: true
                },user_email: {
                    required: true,
                    email: true
                },
				user_first_name: {
					required: true,
					minlength: 3
                },
				user_last_name: {
					required: true,
					minlength: 3
                },
				user_birth_place: {
					required: true,
					minlength: 3
                },
				user_birth_date: {
					required: true
                },
				user_address: {
					required: true,
					minlength: 10
                },
				user_ktp_number: {
					required: true
                },
				user_phone_number: {
					required: true
                },
				user_photo_file_hidden: {
					required: true
                },
				user_ktp_file_hidden: {
					required: true
                },
				user_pendidikan_terakhir: {
					required: true
                },
				user_gender: {
					required: true
                },
				user_marital_status: {
					required: true
                },
				ui_nip: {
					required: true
                }	
				},
            messages: {
                user_name: {
                    required: "Please input Username"
                },user_email: {
                    required: "Please input Email",
                    email: "Email Format is wrong, eg:indigomike7@gmail.com"
                },
				user_first_name: {
					required: "Please input First Name",
					minlength: "Minimum First Name length is 3 characters"
                },
				user_last_name: {
					required: "Please input Last Name",
					minlength: "Minimum Last Name length is 3 characters"
                },
				user_birth_place: {
					required: "Please input Birth Place",
					minlength: "Minimum Last Name length is 3 characters"
                },
				user_birth_date: {
					required: "Please input Birth Date"
                },
				user_address: {
					required: "Please input Address",
					minlength: "Minimum Address length is 10 character"
                },
				user_ktp_number: {
					required: "Please input Citizenship Card ID"
                },
				user_phone_number: {
					required: "Please input Phone/Mobile Phone Number"
                },
				user_photo_file_hidden: {
					required: "Please input Photo File"
                },
				user_ktp_file_hidden: {
					required: "Please input Citizenship Card File"
                },
				user_pendidikan_terakhir: {
					required: "Please input Latest Education"
                },
				user_gender: {
					required: "Please input Gender"
                },
				user_marital_status: {
					required: "Please input Marital Status"
                },
				ui_nip: {
					required: "Please input Worker Card ID"
                }	
			}
        });
    });
$(document).ready(function() {

	$("#user_photo_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Photo File Size is too Big");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format Photo File is not JPG/JPEG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_photo_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_photo_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_photo_file").html(data);
							$("#message_upload_user_photo_file").fadeOut(8000);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_photo_file_div").html("<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#user_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Photo File Size is too Big");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format Photo File is not JPG/JPEG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_ktp_file").html(data);
							$("#message_upload_user_ktp_file").fadeOut(8000);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_ktp_file_div").html("<input type='hidden' name='user_ktp_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);

	$("#user_name").change( function() {
		$.ajax
		({
			url:"check_user_name_ajax.php"
			,data:"user_name=" + $("#user_name").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_name").html(data2);
		  });

	});
	$("#user_email").change( function() {
		$.ajax
		({
			url:"check_user_email_ajax.php"
			,data:"user_email=" + $("#user_email").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_email").html(data2);
		  });

	});


});	
    function datenow() 
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        return today;     
    }

</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
