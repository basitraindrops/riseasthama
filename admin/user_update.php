<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
header("Location:page_login.php");
}
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
require_once(CLASS_DIR."user_internal.class.php");
require_once(CLASS_DIR."user.class.php");
$settings=new settings($mysqli);
$settings->get_by_id(1);
$user_email= decrypt($sessionObj->read("user_email"),$settings->salt_key);
require_once CLASS_DIR.'front_user_patient.class.php';
$up=new front_user_patient($mysqli);
$error="";
$error2="";





if($_POST){

    if(isset($_POST['user_name']) && $_POST['user_name'] != '' && !isset($_POST['patient_id']) && $_POST['patient_id'] == '' ){
       
       $encryptObj = new encryption();
       $insert = $up->insert($user_nama=$_POST['user_name'], $_POST['user_email'], $user_name=$_POST['user_name'], $user_address='', $province_id=0, $kota_id=0, $no_telp='', $no_hp='', $no_npwp='', $no_ktp_sim='', $tempat_lahir='', $tanggal_lahir=date("Y-m-d H:i:s"), $jenis_kelamin='', $pendidikan=0, $pekerjaan=0, $nama_perusahaan='', $jabatan='', $user_password=$encryptObj->encrypt_password($_POST['user_email']), $forgot_password_key='', $forgot_status=0, $payment_register_type='1', $age=$_POST['user_age'], $country='', $status=1, $created_by=$user_email,$profile_pic=$_POST['user_photo_file_hidden'],$user_role = 'patient');
        $user_id=$up->conn->insert_id;
        
        require '../inc/mailer/PHPMailerAutoload.php';
        /*auto login page after registration raindrops*/
        //Create a new PHPMailer instance
        $mail = new PHPMailer;
        //Set who the message is to be sent from
        $mail->setFrom(ADMIN_EMAIL, 'hope@riseasthma.com');
        //Set an alternative reply-to address
        $mail->addReplyTo(ADMIN_EMAIL, 'hope@riseasthma.com');
        //Set who the message is to be sent to
        $mail->addAddress('hope@riseasthma.com');
        //Set the subject line
        $mail->Subject = 'New User Registered in asthmamethod.com';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $message = "Hi <br/>";
        $message.= "A new user have been registered into asthmamethod.com named ".$_POST['user_name'].".<br/> IF you want to contact user you can mail on ".$_POST['user_email'].".<br/> </br> Thank you";
        $message.= "Regards<br/><br/>";
        $message.= "Web Admin";
        $mail->msgHTML($message);
        //Replace the plain text body with one created manually
        $mail->AltBody = 'Email from asthmamethod.com';
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
        $mail->isSendmail();
        $mail->Sendmail = '/usr/sbin/sendmail';
        //$mail->isSMTP();

        //send the message, check for errors
        if (!$mail->send()) {
            //die( "Mailer Error: " . $mail->ErrorInfo);
        }
        //Create a new PHPMailer instance
        $mail = new PHPMailer;
        //Set who the message is to be sent from
        $mail->setFrom(ADMIN_EMAIL, 'hope@riseasthma.com');
        //Set an alternative reply-to address
        $mail->addReplyTo(ADMIN_EMAIL, 'hope@riseasthma.com');
        //Set who the message is to be sent to
        $mail->addAddress($_POST['user_email'], $_POST['user_name']);
        //Set the subject line
        $mail->Subject = 'Welcome to RISE Asthma';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $message = 'Hi '.$_POST['user_name'].",<br/><br/>";

        $message.="Thank you so much for registering. We are thrilled to assist you in your asthma-free journey.<br/>";
        $message.='You can login into our platform by going to <a href="http://www.riseasthma.com">http://www.riseasthma.com</a> and click on "login" or by simply accessing <a href="https://asthmamethod.com/page_login.php">https://asthmamethod.com/page_login.php</a><br/><br/>';

      
        $message.= "Your login credentials are:<br/><br/>Email : ".$_POST['user_email']."<br/>Password : ".$_POST['user_email']."<br/><br/>";

        $message.='We strongly advise you to change your password from the settings menu.<br/><br/>';
        $message.='For any questions, feel free to reply to this email or login to RISE and use the live chat function.<br/><br/>';
        $message.= "Thank you,<br/>";
        $message.= "Cristian Andrei Andriesei<br/>";
        $message.= "RISE Asthma";
        $mail->msgHTML($message);
        //Replace the plain text body with one created manually
        $mail->AltBody = 'Email from asthmamethod.com';
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
        $mail->isSendmail();
        $mail->Sendmail = '/usr/sbin/sendmail';
        //$mail->isSMTP();

        //send the message, check for errors
        if (!$mail->send()) {
           // die( "Mailer Error: " . $mail->ErrorInfo);
        }

        $sessionObj->write('user_add_notification',"Success Inserting datail!");
        header('Location: front_client.php');
        exit;

    }else{
        
        $encryptObj = new encryption();
        if(isset($_POST['user_photo_file_hidden']) && $_POST['user_photo_file_hidden'] != ''){
            $profile_pic=$_POST['user_photo_file_hidden'];
        }else{
            $profile_pic=$_POST['old_user_photo_file'];
        }
        
        $insert = $up->update($user_nama=$_POST['user_name'], $_POST['user_email'], $user_name=$_POST['user_name'], $user_address='', $province_id=0, $kota_id=0, $no_telp='', $no_hp='', $no_npwp='', $no_ktp_sim='', $tempat_lahir='', $tanggal_lahir=date("Y-m-d H:i:s"), $jenis_kelamin='', $pendidikan=0, $pekerjaan=0, $nama_perusahaan='', $jabatan='', $forgot_password_key='', $forgot_status=0, $payment_register_type='1', $age=$_POST['user_age'], $country='', $status=1, $updated_by=$user_email,$updated_date=date('Y-m-d h:i:s'),$profile_pic,$_POST['patient_id']);
        $error="Success Updating datail!";  
         $patient_id =  $_POST['patient_id'];    
    }
    
}

if(isset($_REQUEST['id']) && $_REQUEST['id'] != ''){
    $id = $_REQUEST['id'];
    $up->get_by_user_id($id);
    
}elseif(isset($patient_id) && $patient_id != ''){
    $id = $patient_id;
    $up->get_by_user_id($id); ?>
    <script>
        var pageUrl = "<?php echo ADMIN_URL.'user_update.php?action=update&id='.$patient_id;?>";
        
        window.history.pushState({ path: pageUrl }, '', pageUrl);
    </script>
<?php 
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<?php


?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="front_client.php">Front CLient LIst</a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Front Client"; } else { echo "Update Front Client"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->
<style type="text/css">
#checklist_user_name
{
	color:red;
	font-weight:bold;
}
#checklist_user_email
{
	color:red;
	font-weight:bold;
}
</style>
    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" class="form-horizontal form-box remove-margin"  enctype="multipart/form-data" >
    <input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
    <?php if(isset($id) && $id != ''){ ?>
	   <input type="hidden" id="patient_id" name="patient_id" value="<?php echo $id;?>">
    <?php } ?>
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Front Client"; } else { echo "Update Front Client"; }  } /* ?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Front Client"; } else { echo "Update Front Client"; }  }?></small> */?></h4>
		<p align="left" style="margin-left:20px;"><a href="front_client.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
            <?php if($error2!="") { echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div>";  }?>
            <?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_name">User Name *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_name" name="user_name" class="form-control" value="<?php if(isset($up->user_name) && $up->user_name != '') { echo $up->user_name;}?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_email">Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_email" name="user_email" class="form-control"  value="<?php if(isset($up->user_email) && $up->user_email != '') { echo $up->user_email;}?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_age">Age </label>
                <div class="col-md-4">
                    <div class="input-group">
                        
                        <input type="number" max="100" min="0" id="user_age" name="user_age" class="form-control"  value="<?php if(isset($up->age) && $up->age != '') { echo $up->age;}?>">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">Photo File </label>
                <div class="col-md-4">
                    <div class="input-group">
                        <div id="user_photo_file_div">
                        <input type="file" class="form-control" name="user_photo_file" id="user_photo_file"> Maximum 200 KB JPG File<br/>
                        
                        </div>
                        <?php if(isset($up->profile_pic) && $up->profile_pic != '') { ?>
                        <input type="hidden" class="form-control" name="old_user_photo_file" value="<?php echo $up->profile_pic; ?>"> 
                        <img src="../img/profile_pic/<?php echo $up->profile_pic; ?>" width="150">
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file"></label>
                <div class="col-md-4" id="message_upload_user_photo_file">
                    <div class="input-group">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>

        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                user_name: {
                    required: true
                },user_email: {
                    required: true,
                    email: true
                }
					
				},
            messages: {
                user_name: {
                    required: "Please input Username"
                },user_email: {
                    required: "Please input Email",
                    email: "Email Format is wrong, eg:indigomike7@gmail.com"
                }	
			}
        });
    });
$(document).ready(function() {

	$("#user_photo_file").change(
		function()
	{
			var file = this.files[0];
			name = file.name;
			size = file.size;
			type = file.type;

			if(file.name.length < 1) {
			}
			else if(file.size > 1024 * 1024 * 2) {
				alert("Photo File Size is too Big" + file.size);
			}
			else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
				alert("Format Photo File is not JPG/JPEG");
			}
			else 
			{ 	
                var randomnumber = Math.floor(Math.random() * (100000 - 100 + 1)) + 100;
                name = randomnumber + '_' + file.name  ;		
                //alert("test");
				var fd = new FormData();
				fd.append('image_file', $('#user_photo_file')[0].files[0]);
                fd.append('session_id',$("#session_id").val());
				fd.append('image_name',name);
				$.ajax({
				url: "image_file_upload_ajax_fornt_client.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: fd , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				enctype: 'multipart/form-data',
				processData: false,  // tell jQuery not to process the data
				cache: false,        // To send DOMDocument or non processed data file it is set to false
				  beforeSend: function( data ) {
					$("#message_upload_user_photo_file").html('Uploading file...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						$("#message_upload_user_photo_file").html(data);
						$("#message_upload_user_photo_file").fadeOut(8000);
						if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
							$("#user_photo_file_div").html("<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='" +  name +"'><i class='fi fi-jpg'></i>" + name);
					}
				});
			}
		}
	);
	$("#user_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Photo File Size is too Big");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format Photo File is not JPG/JPEG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax_fornt_client.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_ktp_file").html(data);
							$("#message_upload_user_ktp_file").fadeOut(8000);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_ktp_file_div").html("<input type='hidden' name='user_ktp_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);

	$("#user_name").change( function() {
		$.ajax
		({
			url:"check_user_name_ajax.php"
			,data:"user_name=" + $("#user_name").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_name").html(data2);
		  });

	});
	$("#user_email").change( function() {
		$.ajax
		({
			url:"check_user_email_ajax.php"
			,data:"user_email=" + $("#user_email").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_email").html(data2);
		  });

	});


});	
    function datenow() 
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        return today;     
    }
    

</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
