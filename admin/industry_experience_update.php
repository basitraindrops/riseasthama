<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require(CLASS_DIR.'user.class.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
//echo $sessionObj->read('email');
if(isset($_POST['industry_experience_name'])&&isset($_GET['action']))
{
	if($_GET['action']=="add_new")
	{
		if(user_access_each($mysqli,"create_industry_experience",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."industry_experience.class.php");
		$ug=new industry_experience($mysqli);
		$ug->insert($_POST['industry_experience_name'],$user_name);
		header("Location:industry_experience.php");
	}
	else if($_GET['action']=="update")
	{
		if(user_access_each($mysqli,"update_industry_experience",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."industry_experience.class.php");
		$ug=new industry_experience($mysqli);
		$ug->update($_POST['industry_experience_name'],$user_name,$_GET['id']);
		$error="Success updating data!";
	}
}
if(isset($_GET['action'])&&isset($_GET['id']))
{
	if($_GET['action']=="update")
	{
		require_once(CLASS_DIR."industry_experience.class.php");
		$ug_show=new industry_experience($mysqli);
		$ug_show->get_by_industry_experience_id($_GET['id']);
	}
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="industry_experience.php">Industry Experience</a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Industry Experience"; } else { echo "Update Industry Experience"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]."?action=".$_GET['action']; if(isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Industry Experience"; } else { echo "Update Industry Experience"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Industry Experience"; } else { echo "Update existing Industry Experience"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="industry_experience.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Industry Experience Name *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="industry_experience_name" name="industry_experience_name" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $ug_show->industry_experience_name; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                industry_experience_name: {
                    required: true,
                    minlength: 3
                }
				},
            messages: {
                industry_experience_name: {
                    required: 'Mohon masukkan Nama Group',
                    minlength: 'Nama Group minimal 3 karakter'
                }            
			}
        });
    });
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
