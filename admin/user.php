<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>
<?php include 'inc/top.php';  ?>
<?php include 'inc/nav.php';  ?>

<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">User</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Datatables -->
    <h3 class="page-header page-header-top">User <small>User Data</small></h3>
	<div>
<!--	        <a href="user_update.php?action=add_new" id="" class="btn btn-success"><i class="fa fa-plus"></i> Add New User</a>-->
	</div>
    <table id="user-datatables" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th class="cell-small"></th>
                <th class="cell-small text-center hidden-xs hidden-sm">#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Username</th>
				<?php if(user_access_each($mysqli,"view_created_updated_log",$user_id)) { ?>
                <th>Created Date</th>
                <th>Created By</th>
                <th>Updated Date</th>
                <th>Updated By</th>
				<?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php 
			require_once(CLASS_DIR."user.class.php");
			$pb=new user($mysqli);
			$data=$pb->user_all();
			for($i=0; $i<count($data); $i++) { ?>
            <tr id="<?php echo $data[$i]->user_id; ?>">
                <td class="text-center">
				<?php if(user_access_each($mysqli,"view_user",$user_id)) { ?>
					<a href="user_view.php?id=<?php echo $data[$i]->user_id; ?>" data-toggle="tooltip" title="View User" class="btn btn-xs btn-success"><i class="hi hi-zoom-in"></i></a>
				<?php } ?>
				<?php if(user_access_each($mysqli,"administration_user_password",$user_id)) { ?>
					<a href="password_update.php?id=<?php echo $data[$i]->user_id; ?>" data-toggle="tooltip" title="Change Password" class="btn btn-xs btn-info"><i class="fa fa-key"></i></a>
				<?php } ?>
                </td>
                <td id="id<?php echo $data[$i]->user_id; ?>"><?php echo $data[$i]->user_id; ?></td>
                <td><?php echo $data[$i]->user_first_name; ?></td>
                <td><?php echo $data[$i]->user_last_name; ?></td>
                <td><?php echo $data[$i]->user_email; ?></td>
                <td><?php echo $data[$i]->user_name; ?></td>
				<?php if(user_access_each($mysqli,"view_created_updated_log",$user_id)) { ?>
                <td><?php echo $data[$i]->created_date; ?></td>
                <td><?php echo $data[$i]->created_by; ?></td>
                <td><?php echo $data[$i]->updated_date; ?></td>
                <td><?php echo $data[$i]->updated_by; ?></td>
				<?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <!-- END Datatables -->
	<div>
<!--	        <a href="user_update.php?action=add_new" id="" class="btn btn-success"><i class="fa fa-plus"></i> Add New user</a>-_>
	</div>

    <!-- END Datatables in the grid -->

</div>
<!-- END Page Content -->
<?php include 'inc/footer.php';  ?>
<script type="text/javascript">
    $(function(){

        // Hold our table to a variable
        var exampleDatatable = $('#user-datatables');

        /*
         * Function for handing the data after a cell has been edited
         *
         * From here you can send the data with ajax (for example) to handle in your backend
         *
         */
        var reqHandle = function(value, settings){

            // this, the edited td element
            console.log(this);

            // $(this).attr('id'), get the id of the edited td
            console.log($(this).attr('id'));

            // $(this).parent('tr').attr('id'), get the id of the row
            console.log($(this).parent('tr').attr('id'));

            // value, the new value the user submitted
            console.log(value);

            // settings, the settings of jEditable
            console.log(settings);

            // Here you can send and handle the data in your backend
            // ...

            // For this example, just return the data the user submitted
            return(value);
        };

        /*
         * Function for initializing jEditable handlers to the table
         *
         * For advance usage you can check http://www.appelsiini.net/projects/jeditable
         *
         */
        var initEditable = function(rowID){

            // Hold the elements that the jEditable with be initialized
            var elements;

            // If we don't have a rowID apply to all td elements with .editable-td class
            if (!rowID)
                elements = $('td.editable-td', editableTable.fnGetNodes());
            else
                elements = $('td.editable-td', editableTable.fnGetNodes(rowID));

            elements.editable(reqHandle, {
                "callback": function( sValue, y ) {
                    // Update the table cell after the edit has been processed
                    var aPos = editableTable.fnGetPosition(this);
                    editableTable.fnUpdate(sValue, aPos[0], aPos[1]);

                    // Little fix for responsive table after edit
                    exampleDatatable.css('width', '100%');
                },
                "submitdata": function ( value, settings ) {
                    // Sent some extra data
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": editableTable.fnGetPosition(this)[2]
                    };
                },
                indicator : '<i class="fa fa-spinner fa-spin"></i>',
                cssclass : 'remove-margin',
                submit: 'Ok',
                cancel: 'Cancel'
            });
        };

        /*
         * Function for deleting table row
         *
         */

        var delHandle = function(){

            // When the user clicks on a delete button
            $('body').on('click','a.delRow', function(){

				var aPos = editableTable.fnGetPosition(this.parentNode);
				var aData = editableTable.fnGetData(aPos[0]);
				var rowID = $(this).parents('tr').attr('id');
				$('#delete-modal').modal('show');
				$(".btn-success-delete").click(
					function() 
					{
						editableTable.fnDeleteRow(aPos[0]);
						$.ajax
							({
								url:"user_delete_ajax.php"
								,data:"user_id=" + rowID
								,dataType:"json"
								,method:"POST"
								,success:function() 
										{
											document.location="<?php echo $_SERVER['PHP_SELF'];?>";
										}
							});
						$('#delete-modal').modal('hide');
					}
				);
            });
        };

        /*
         * Function for adding table row
         *
         */
        var addHandle = function(){

            // When the user clicks on the 'Add New User' button
            $("#addRow").click(function(){

                // Here you can handle your backend data (eg: adding a row to database and return the id of the row)

                // ..

                // Create a new row and set it up
                var rowID = editableTable.fnAddData(['', '', '', '', '']);

                // Example id, here you should add the one you created in your backend
                var id = rowID[0] + 1;

                // Update the id cell, so that our table redraw and resort (new row goes first in datatable)
                editableTable.fnUpdate(id, rowID[0], 1);

                // Get the new row
                var nRow = editableTable.fnGetNodes(rowID[0]);

                /*
                 * In the following section you should set up your cells
                 */
                // Add id to tr element
                $(nRow).attr('id', id);

                // Setup first cell with the delete button
                $(nRow)
                    .children('td:nth-child(1)')
                    .addClass('text-center')
                    .html('<a href="javascript:void(0)" id="delRow' + id + '" class="btn btn-xs btn-danger delRow"><i class="fa fa-times"></i></a>');

                // Setup second cell (id)
                $(nRow)
                    .children('td:nth-child(2)')
                    .attr('id', 'id' + id)
                    .addClass('text-center');

                // Setup third cell (username)
                $(nRow)
                    .children('td:nth-child(3)')
                    .addClass('editable-td')
                    .attr('id', 'username' + id);

                // Setup fourth cell (email)
                $(nRow)
                    .children('td:nth-child(4)')
                    .addClass('editable-td')
                    .addClass('hidden-xs hidden-sm')
                    .attr('id', 'email' + id);

                // Setup fifth cell (notes)
                $(nRow)
                    .children('td:nth-child(5)')
                    .addClass('editable-td')
                    .addClass('hidden-xs hidden-sm')
                    .attr('id', 'notes' + id);

                // Setup your other cells the same way (if you have more)
                // ...

                // Initialize jEditable to the new row
                initEditable(rowID[0]);

                // Little fix for responsive table after adding a new row
                exampleDatatable.css('width', '100%');
            });
        };

        // Initialize Datatables
        var editableTable = exampleDatatable.dataTable({
            "aaSorting": [[ 1, 'desc' ]],
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": [0]
            }]
        });
        $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');

        // Initialize jEditable
	initEditable();

        // Handle rows deletion
        delHandle();

        // Handle new rows
        addHandle();
    });
</script>
<?php include 'inc/bottom.php';  ?>