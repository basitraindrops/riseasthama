<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
header("Location:page_login.php");
}
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
require_once(CLASS_DIR."user_internal.class.php");
//require_once(CLASS_DIR."user.class.php"); 
$settings=new settings($mysqli);
$settings->get_by_id(1);
$user_email= decrypt($sessionObj->read("user_email"),$settings->salt_key);
require_once CLASS_DIR.'manage_faq.class.php';
$up=new manage_faq($mysqli);
$error="";
$error2="";
?>
<?php
$error="";

if($_POST){

    if(isset($_POST['vitamind_faq_name'])&&isset($_POST['vitamind_faq_des'])&&($_POST['vitamind_faq_name'])!=''&&($_POST['vitamind_faq_des']))
    {
        if($_GET['action']=="add_new")
        {
            $vitamind_category=3;
            require_once(CLASS_DIR."manage_faq.class.php");
            $ug=new manage_faq($mysqli);
            $ug->insert_vitamind_faq($_POST['vitamind_faq_name'],$_POST['vitamind_faq_des'],$vitamind_category);
            $sessionObj->write('user_add_notification',"Success Inserting datail!");
            header('Location: vitamind_faq.php');
            exit;
        }
        else if($_GET['action']=="update")
        {
            $vitamind_category=3;
            require_once(CLASS_DIR."manage_faq.class.php");
            $ug=new manage_faq($mysqli);
            $ug->update_vitamind_faq($_POST['vitamind_faq_name'],$_POST['vitamind_faq_des'],$_REQUEST['id']);
            $sessionObj->write('user_add_notification',"Success updating datail!");
            header('Location: vitamind_faq.php');
            exit;
        }        
    }
}
if(isset($_REQUEST['id']) && $_REQUEST['id'] != ''){
    $id = $_REQUEST['id'];
    require_once(CLASS_DIR."manage_faq.class.php");
    $pb=new manage_faq($mysqli);
    $pb->user_faq_detail($id);
   
    
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<?php


?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add Vitamin D FAQ"; } else { echo "Update Vitamin D FAQ"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->
<style type="text/css">
#checklist_user_name
{
    color:red;
    font-weight:bold;
}
#checklist_user_email
{
    color:red;
    font-weight:bold;
}
</style>
<script src="js/ckeditor2/ckeditor.js"></script>
<script src="js/ckeditor2/samples/js/sample.js"></script>
    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="" method="post" class="form-horizontal form-box remove-margin"  enctype="multipart/form-data" >
    <input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
    <?php if(isset($id) && $id != ''){ ?>
       <input type="hidden" id="patient_id" name="patient_id" value="<?php echo $id;?>">
    <?php } ?>
        <!-- Form Header -->
         <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add Vitamin D FAQ"; } else { echo "Update Vitamin D FAQ"; }  }?>
        
        <p align="left" style="margin-left:20px;"><a href="vitamind_faq.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        <!-- Form Content -->
        <div class="form-box-content">
           
            <div class="form-group form-actions">
                <div class="form-group">
                    <label class="control-label col-md-2" for="user_age">Vitamin D FAQ Name</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" id="vitamind_faq_name" name="vitamind_faq_name" class="form-control" value="<?php if(isset($pb->faq_name) && $pb->faq_name != '') { echo $pb->faq_name;}?>">
                        </div>
                    </div>
               </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="user_age">Vitamin D FAQ Description</label>
                    <div class="col-md-10">
                        <div class="input-group">
                         <textarea rows="4" cols="50" name="vitamind_faq_des"  id="editor"> <?php if(isset($pb->faq_des) && $pb->faq_des != '') { echo $pb->faq_des;}?></textarea> 
                            <!--<textarea rows="4" cols="50" name="vitamind_faq_des" id="vitamind_faq_des"><?php //if(isset($pb->faq_des) && $pb->faq_des != '') { echo $pb->faq_des;}?>
                            </textarea>-->
                        </div>
                    </div>
               </div>
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>

        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->
<script>
    initSample();    
</script>
<?php include 'inc/footer.php'; // Footer and scripts ?>


<?php include 'inc/bottom.php'; // Close body and html tags ?>
