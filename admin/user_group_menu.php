<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(!isset($_GET['id']))
{
	header("Location: user.php");
}

require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>
<?php include 'inc/top.php';  ?>
<?php include 'inc/nav.php';  ?>

<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">User Group Menu</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Datatables -->
    <h3 class="page-header page-header-top">User Group Menu <small>User Group Menu Data</small></h3>
		<p align="left"><a href="user_group.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
    <table id="user-datatables" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Menu Name</th>
                <th>Add/Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php 
			require_once(CLASS_DIR."menu.class.php");
			$menu=new menu($mysqli);
			$data=$menu->menu_all_parent();
			for($i=0; $i<count($data); $i++) { 
				require_once(CLASS_DIR."user_group_menu.class.php");
				$ugm=new user_group_menu($mysqli);
				$ugm->get1($data[$i]->menu_id,$_GET['id']);

				?>
            <tr id="tr_<?php echo $data[$i]->menu_id; ?>">
                <td><?php echo $data[$i]->menu_name; ?></td>
                <td width="50px"><input type="checkbox" name="<?php echo $data[$i]->menu_id; ?>" id="<?php echo $data[$i]->menu_id; ?>" onClick="add_remove('<?php echo $data[$i]->menu_id;?>','<?php echo $_GET['id'] ?>');" <?php	if($ugm->get1($data[$i]->menu_id,$_GET['id'])){ echo " checked='checked' "; } ?>	></td>
            </tr>
			<?php
				$data2 = $menu->menu_all_children($data[$i]->menu_id);
				if(count($data2)>0)
				{
					for($j=0;$j<count($data2);$j++)
					{
						require_once(CLASS_DIR."user_group_menu.class.php");
						$ugm2=new user_group_menu($mysqli);
						$ugm2->get1($data2[$j]->menu_id,$_GET['id']);
						?>
						<tr id="tr_<?php echo $data2[$j]->menu_id; ?>">
							<td><?php //echo $data2[$j]->menu_id; 
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data[$i]->menu_name." --> ".$data2[$j]->menu_name; ?></td>
							<td><input type="checkbox" name="<?php echo $data2[$j]->menu_id; ?>" id="<?php echo $data2[$j]->menu_id; ?>" onClick="add_remove('<?php echo $data2[$j]->menu_id;?>','<?php echo $_GET['id'] ?>');" <?php	if($ugm2->get1($data2[$j]->menu_id,$_GET['id'])) {	echo " checked='checked' ";	}?>	></td>
						</tr>
						<?php
					}
				}
			?>
            <?php } ?>
        </tbody>
    </table>
    <!-- END Datatables -->
		<p align="left"><a href="user_group.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
	<div>

						
						<!-- Modal itself -->
            <div id="delete-modal" class="modal">
                <!-- Modal Dialog -->
                <div class="modal-dialog">
                    <!-- Modal Content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>Delete Data</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this data?</p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-success-delete">Yes</button>
                            <button class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </div>
                    <!-- END Modal Content -->
                </div>
                <!-- END Modal Dialog -->
            </div>
            <!-- END Modal itself -->

	</div>

    <!-- END Datatables in the grid -->

</div>
<!-- END Page Content -->
<?php include 'inc/footer.php';  ?>
<script type="text/javascript">
function add_remove(id,id2)
{
	if($('#' + id).is(':checked'))
	{
		$.ajax
		({
			url:"user_group_menu_ajax.php"
			,data:"menu_id=" + id + "&user_group_id=" + id2 + "&action=add"
			,dataType:"json"
			,method:"POST"
		});
	}
	else
	{
		$.ajax
		({
			url:"user_group_menu_ajax.php"
			,data:"menu_id=" + id + "&user_group_id=" + id2 + "&action=remove"
			,dataType:"json"
			,method:"POST"
		});
	}
}
</script>
<?php include 'inc/bottom.php';  ?>