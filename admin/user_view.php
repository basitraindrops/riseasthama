<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
if(isset($_GET['id']))
{
	require_once(CLASS_DIR."user.class.php");
	$user_show=new user($mysqli);
	$user_show->get_by_user_id($_GET['id']);
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="user.php">User</a></li>
        <li class="active"><a href="javascript:;">View User</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
        <!-- Form Header -->
        <h4 class="form-box-header">View User<small>View User</small></h4>
		<p align="left" style="margin-left:20px;"><a href="user.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_email">Email *</label>
							<?php echo $user_show->user_email; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_name">Username *</label>
							<?php echo $user_show->user_name; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">First Name *</label>
							<?php echo $user_show->user_first_name; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Last Name *</label>
							<?php echo $user_show->user_last_name; ?>
            </div>
            <div class="form-group" id="ktp">
                <label class="control-label col-md-2" for="val_number">Citizen Card ID *</label>
							<?php echo $user_show->user_ktp_number; ?>
            </div>
            <div class="form-group" id="ktp">
                <label class="control-label col-md-2" for="val_number">Birth Place *</label>
							<?php echo $user_show->user_birth_place; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_date">Birth Date *</label>
							<?php echo date("d-M-Y",strtotime($user_show->user_birth_date)); ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_address">Address *</label>
							<?php echo $user_show->user_address; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">Photo File *</label>
					<a href="file_ui/<?php echo $user_show->user_photo_file; ?>"><i class="fi fi-jpg"></i><?php echo $user_show->user_photo_file; ?></a>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">Citizen Card File *</label>
					<a href="file_ui/<?php echo $user_show->user_ktp_file; ?>"><i class="fi fi-jpg"></i><?php echo $user_show->user_photo_file; ?></a>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Phone/Mobile *</label>
							<?php echo $user_show->user_phone_number; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Gender *</label>
							<?php echo $user_show->user_gender; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Highest Education *</label>
							<?php echo $user_show->user_pendidikan_terakhir; ?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Marital Status *</label>
							<?php echo $user_show->user_marital_status; ?>
            </div>
		<p align="left"><a href="user.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a></p>
        </div>
        <!-- END Form Content -->
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->

<?php include 'inc/bottom.php'; // Close body and html tags ?>
