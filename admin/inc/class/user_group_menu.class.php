<?php
class user_group_menu
{
	public $ugm_id;
	public $menu_id;
	public $user_group_id;
	public $menu_name;
	public $menu_page;
	public $icon;
	public $parent_id;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	private $conn;
	function __construct($conn='',$ugm_id='',$menu_id='',$user_group_id='',$created_date='',$created_by='',$updated_date='',$updated_by="",$menu_name="",$menu_page="",$icon="",$parent_id="")
	{
		$this->conn = $conn;
		$this->ugm_id=$ugm_id;
		$this->menu_id = $menu_id;
		$this->user_group_id= $user_group_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
		$this->menu_name=$menu_name;
		$this->menu_page=$menu_page;
		$this->icon=$icon;
		$this->parent_id=$parent_id;
	}
	function get_all_user_group($user_id)
	{
		$security = new security();
		$query="select ugm.*
		, m.menu_name
		, m.menu_page
		, m.icon
		, m.parent_id
		from user_group_menu ugm
		left join menu m
		on ugm.menu_id = m.menu_id
		left join user_group ug
		on ugm.user_group_id = ug.user_group_id
		left join user u
		on u.user_group_id = ug.user_group_id
		where u.user_id=? and m.menu_id <> '' order by m.menu_id desc";
		$stmt = $this->conn->prepare($query);
		$aa = ($security->xss_clean($user_id));
		$stmt->bind_param('i', $aa );
		$stmt->execute();

		$stmt->bind_result($ugm_id,$user_group_id,$menu_id,$created_date,$created_by,$updated_date,$updated_by,$menu_name,$menu_page,$icon,$parent_id);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			//echo $menu_id;
			$ugm = new user_group_menu($this->conn,$ugm_id,$menu_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by,$menu_name,$menu_page,$icon,$parent_id);
			$data[]=$ugm;
		}
		return $data;
	}
	function get1($menu_id,$user_group_id)
	{
		$security = new security();
		$query="select *
		from user_group_menu  
		where menu_id = ? and user_group_id=? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii', ($security->xss_clean($menu_id)), ($security->xss_clean($user_group_id)));
		$stmt->execute();

		$stmt->bind_result($ugm_id,$user_group_id,$menu_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
//		$this->__construct($this->conn,$ugm_id,$menu_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by,$menu_name="",$menu_page="",$icon="",$parent_id="");
//		echo $ugm_id;
		if($ugm_id!=0)
			return true;
		else
			return false;
		
	}
	function insert($menu_id,$user_group_id,$created_by)
	{
		$security = new security();
		$query="insert into user_group_menu
		(menu_id
		, user_group_id
		, created_date
		, created_by)
		values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('iiss',($security->xss_clean($menu_id)),($security->xss_clean($user_group_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($menu_id,$user_group_id)
	{
		$security = new security();
		$query="delete from user_group_menu where menu_id=? and user_group_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii',($security->xss_clean($menu_id)),($security->xss_clean($user_group_id)));
		$stmt->execute();
	}
}
?>