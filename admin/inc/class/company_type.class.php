<?php
class company_type
{
	public $company_type_id;
	public $company_type_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$company_type_id='',$company_type_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->company_type_id = $company_type_id;
		$this->company_type_name = $company_type_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_company_type_id($company_type_id)
	{
		$security = new security();
		$query="select *
		from company_type
		where company_type_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($company_type_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($company_type_id,$company_type_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$company_type_id,$company_type_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function company_type_all()
	{
		$query="select * 
		from company_type 
		order by company_type_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($company_type_id,$company_type_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$company_type=new company_type($this->conn,$company_type_id,$company_type_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$company_type;
		}
		return $data;
	}
	function update($company_type_name,$updated_by,$company_type_id)
	{
		$security = new security();
		$query="update company_type set company_type_name=?,updated_date=?,updated_by=? where 
		company_type_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($company_type_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($company_type_id)));
		$stmt->execute();
	}
	function insert($company_type_name,$created_by)
	{
		$security = new security();
		$query="insert into company_type(company_type_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($company_type_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($company_type_id)
	{
		$security = new security();
		$query="delete from company_type where company_type_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($company_type_id)));
		$stmt->execute();
	}
}
?>