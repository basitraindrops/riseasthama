<?php
error_reporting(0);
class manage_faq
{
	
	public $faq_id;
	public $faq_name;
	public $faq_des;
	public $faq_category;
	public $story_faq_name;
	public $story_faq_des;
	public $story_category;
	public $asthma_faq_name;
	public $asthma_faq_des;
	public $asthma_category;
	public $omega3_faq_name;
	public $omega3_faq_des;
	public $omega3_category;
	public $pressure_faq_name;
	public $pressure_faq_des;
	public $pressure_category;
	public $homeostasis_faq_name;
	public $homeostasis_faq_des;
	public $homeostasis_category;
	public $stress_faq_name;
	public $stress_faq_des;
	public $stress_category;
	public $together_faq_name;
	public $together_faq_des;
	public $together_category;
	
	
	private $conn;
	function __construct($conn='',$faq_id='',$faq_name='',$faq_des='',$faq_category,$story_faq_name='',$story_faq_des='',$tory_category,$asthma_faq_name='',$asthma_faq_des='',$asthma_category,$omega3_faq_name='',$omega3_faq_des='',$omega3_category,$pressure_faq_name,$pressure_faq_des='',$pressure_category='',$homeostasis_faq_name,$homeostasis_faq_des='',$homeostasis_category='',$stress_faq_name,$stress_faq_des,$stress_category='',$together_faq_name='',$together_faq_des,$together_category)
	{
		$this->conn = $conn;
		$this->faq_id = $faq_id;
		$this->faq_name = $faq_name;
		$this->faq_des = $faq_des;
		$this->faq_category = $faq_category;
		$this->story_faq_name = $story_faq_name;
		$this->story_faq_des = $story_faq_des;
		$this->story_category = $story_category;
		$this->asthma_faq_name = $asthma_faq_name;
		$this->asthma_faq_des = $asthma_faq_des;
		$this->asthma_category = $asthma_category;
		$this->omega3_faq_name = $omega3_faq_name;
		$this->omega3_faq_des = $omega3_faq_des;
		$this->omega3_category = $omega3_category;
		$this->pressure_faq_name = $pressure_faq_name;
		$this->pressure_faq_des = $pressure_faq_des;
		$this->pressure_category = $pressure_category;
		$this->homeostasis_faq_name = $homeostasis_faq_name;
		$this->homeostasis_faq_des = $homeostasis_faq_des;
		$this->homeostasis_category = $homeostasis_category;
		$this->stress_faq_name = $stress_faq_name;
		$this->stress_faq_des = $stress_faq_des;
		$this->stress_category = $stress_category;
		$this->together_faq_name = $together_faq_name;
		$this->together_faq_des = $together_faq_des;
		$this->together_category = $together_category;

	
	}
	
	function insert_faq($faq_name,$faq_des)
	{

		$security = new security();
		$query="insert into faq_category(faq_cat_name,faq_cat_des)values(?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ss',($security->xss_clean($faq_name))
							,($security->xss_clean($faq_des))
							);
		$stmt->execute();

    	
	}
	function insert_story_faq($story_faq_name,$story_faq_des,$story_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($story_faq_name))
							,($security->xss_clean($story_faq_des)),
							($security->xss_clean($story_category))
							);
		$stmt->execute();

    	
	}
	function insert_asthma_faq($asthma_faq_name,$asthma_faq_des,$asthma_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($asthma_faq_name))
							,($security->xss_clean($asthma_faq_des)),
							($security->xss_clean($asthma_category))
							);
		$stmt->execute();
	}
	function insert_vitamind_faq($vitamind_faq_name,$vitamind_faq_des,$vitamind_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($vitamind_faq_name))
							,($security->xss_clean($vitamind_faq_des)),
							($security->xss_clean($vitamind_category))
							);
		$stmt->execute();
	}
	function insert_omega_faq($omega3_faq_name,$omega3_faq_des,$omega3_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($omega3_faq_name))
							,($security->xss_clean($omega3_faq_des)),
							($security->xss_clean($omega3_category))
							);
		$stmt->execute();
	}
	function insert_pressure_faq($pressure_faq_name,$pressure_faq_des,$pressure_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($pressure_faq_name))
							,($security->xss_clean($pressure_faq_des)),
							($security->xss_clean($pressure_category))
							);
		$stmt->execute();
	}
	function insert_homeostasis_faq($homeostasis_faq_name,$homeostasis_faq_des,$homeostasis_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($homeostasis_faq_name))
							,($security->xss_clean($homeostasis_faq_des)),
							($security->xss_clean($homeostasis_category))
							);
		$stmt->execute();
	}
	function insert_stress_faq($stress_faq_name,$stress_faq_des,$stress_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($stress_faq_name))
							,($security->xss_clean($stress_faq_des)),
							($security->xss_clean($stress_category))
							);
		$stmt->execute();
	}
	function insert_together_faq($together_faq_name,$together_faq_des,$together_category)
	{

		$security = new security();
		$query="insert into faq(faq_name,faq_des,faq_category_id)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssi',($security->xss_clean($together_faq_name))
							,($security->xss_clean($together_faq_des)),
							($security->xss_clean($together_category))
							);
		$stmt->execute();
	}
	function delete($faq_id)
	{
		$security = new security();
		$query="delete from faq where faq_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',$faq_id);
		$stmt->execute();
	}
	function faq_detail($faq_cat)
	{
		
		$query="select * 
		from faq 
		where faq_category_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $faq_cat);
		$stmt->execute();

	
		$stmt->bind_result(
			 $faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{

		$title=new manage_faq($this->conn
			,$faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
			);
			$data[]=$title;
		}
		return $data;
	}
	function user_faq_detail($f_id)
	{
		
		$security = new security();
		$query="select *
		from faq
		where faq_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($f_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
			 $faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
			,$faq_id
			,$faq_name
			,$faq_des
			,$faq_category_id
			);
		
	}
	function update_story_faq($story_faq_name,$story_faq_des,$id)
	{
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($story_faq_name))
			,($security->xss_clean($story_faq_des))
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_omega_faq($omega3_faq_name,$omega3_faq_des,$id)
	{
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($omega3_faq_name))
			,($security->xss_clean($omega3_faq_des))
			
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_vitamind_faq($vitamind_faq_name,$vitamind_faq_des,$id)
	{
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($vitamind_faq_name))
			,($security->xss_clean($vitamind_faq_des))
			
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_asthma_faq($asthma_faq_name,$asthma_faq_des,$id)
	{
	
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($asthma_faq_name))
			,($security->xss_clean($asthma_faq_des))
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_pressure_faq($pressure_faq_name,$pressure_faq_des,$id)
	{
	
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($pressure_faq_name))
			,($security->xss_clean($pressure_faq_des))
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_homeostasis_faq($homeostasis_faq_name,$homeostasis_faq_des,$id)
	{
	
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($homeostasis_faq_name))
			,($security->xss_clean($homeostasis_faq_des))
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_stress_faq($stress_faq_name,$stress_faq_des,$id)
	{
	
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($stress_faq_name))
			,($security->xss_clean($stress_faq_des))
			
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function update_together_faq($together_faq_name,$together_faq_des,$id)
	{
	
		$security = new security();
		$query="update faq set faq_name=?
			,faq_des=?
			
		where 
		faq_id=?";
		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('ssi',($security->xss_clean($together_faq_name))
			,($security->xss_clean($together_faq_des))
			
			,($security->xss_clean($id))
			)or die(mysqli_error($this->conn));
		$stmt->execute();
	}
	function faq_category()
	{
		
		$query="select * 
		from faq_category";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
			 $faq_cat_id
			,$faq_cat_name
			,$faq_cat_des
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{

		$title=new manage_faq($this->conn
			 ,$faq_cat_id
			,$faq_cat_name
			,$faq_cat_des
			);
			$data[]=$title;
		}
		return $data;
	}
	
}
?>