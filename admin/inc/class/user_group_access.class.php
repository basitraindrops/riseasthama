<?php
class user_group_access
{
	public $uga_id;
	public $access_id;
	public $user_group_id;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	private $conn;
	function __construct($conn='',$uga_id='',$access_id='',$user_group_id='',$created_date='',$created_by='',$updated_date='',$updated_by="")
	{
		$this->conn = $conn;
		$this->uga_id=$uga_id;
		$this->access_id = $access_id;
		$this->user_group_id= $user_group_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function get1($access_id,$user_group_id)
	{
		$security = new security();
		$query="select *
		from user_group_access  
		where access_id = ? and user_group_id=? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii', ($security->xss_clean($access_id)), ($security->xss_clean($user_group_id)));
		$stmt->execute();

		$stmt->bind_result($uga_id,$access_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
//		$this->__construct($this->conn,$uga_id,$access_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by);
		if($uga_id!=0)
			return true;
		else
			return false;
	}
	function get2($access_name,$user_group_id)
	{
		$security = new security();
		$query="select uga.*
		from user_group_access uga
		left join access_settings a
		on uga.access_id = a.access_id
		where a.access_name = ? and uga.user_group_id=? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('si', ($security->xss_clean($access_name)), ($security->xss_clean($user_group_id)));
		$stmt->execute();

		$stmt->bind_result($uga_id,$access_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$uga_id,$access_id,$user_group_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function insert($access_id,$user_group_id,$created_by)
	{
		$security = new security();
		$query="insert into user_group_access
		(access_id
		, user_group_id
		, created_date
		, created_by)
		values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('iiss',($security->xss_clean($access_id)),($security->xss_clean($user_group_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($access_id,$user_group_id)
	{
		$security = new security();
		$query="delete from user_group_access where access_id=? and user_group_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ii',($security->xss_clean($access_id)),($security->xss_clean($user_group_id)));
		$stmt->execute();
	}
}
?>