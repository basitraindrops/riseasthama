<?php
class jenis_layanan
{
	public $jl_id;
	public $jl_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$jl_id='',$jl_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->jl_id = $jl_id;
		$this->jl_name = $jl_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_jl_id($jl_id)
	{
		$security = new security();
		$query="select *
		from front_jenis_layanan
		where jl_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($jl_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($jl_id,$jl_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$jl_id,$jl_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function jenis_layanan_all()
	{
		$query="select * 
		from front_jenis_layanan 
		order by jl_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($jl_id,$jl_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$jenis_layanan=new jenis_layanan($this->conn,$jl_id,$jl_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$jenis_layanan;
		}
		return $data;
	}
	function update($jl_name,$updated_by,$jl_id)
	{
		$security = new security();
		$query="update front_jenis_layanan set jl_name=?,updated_date=?,updated_by=? where 
		jl_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($jl_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($jl_id)));
		$stmt->execute();
	}
	function insert($jl_name,$created_by)
	{
		$security = new security();
		$query="insert into front_jenis_layanan(jl_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($jl_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($jl_id)
	{
		$security = new security();
		$query="delete from front_jenis_layanan where jl_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($jl_id)));
		$stmt->execute();
	}
}
?>