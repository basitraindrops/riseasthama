<?php
class front_quote
{
	public $quote_id;
	public $quote;
	public $status;
	public $must_show;
	
	
	function __construct($conn='',$quote_id='',$quote='',$status='',$must_show='')
	{
		$this->conn = $conn;
		$this->quote_id=$quote_id;
		$this->quote= $quote;
		$this->status=$status;
		$this->must_show= $must_show;		
	}

	function getAll()
	{
		$query="select * from quote order by quote_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$stmt->bind_result($quote_id,$quote,$status,$must_show);		
		$data=array();
		while($stmt->fetch())
		{
			$title=new front_quote($this->conn,$quote_id,$quote,$status,$must_show);
			$data[]=$title;
		}
		return $data;
	}

	function getRandomQuote(){

		
		$query ="SELECT * FROM quote  WHERE status = 1 AND must_show = 1 ORDER BY RAND() LIMIT 1";
		$res = $this->conn->query($query);
		if($res->num_rows > 0){
			$row = mysqli_fetch_assoc($res);
			return $row['quote'];	
		}

		$query ="SELECT * FROM quote  WHERE status = 1 ORDER BY RAND() LIMIT 1";
		$res = $this->conn->query($query);

		if($res->num_rows > 0){
			$row = mysqli_fetch_assoc($res);
			return $row['quote'];					
		}else{
			return 'Wlcome';
		}

	}

	function getById($title_id)
	{
		$security = new security();
		$query="select * from quote where quote_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		
		$id=$security->xss_clean($title_id);

		$stmt->bind_param('i', $id);
		$stmt->execute()or die(mysqli_error($this->conn));

		$stmt->bind_result($quote_id,$quote,$status,$must_show)or die(mysqli_error($this->conn));
		
	
		if($stmt->fetch()){
			$this->__construct($this->conn,$quote_id,$quote,$status,$must_show);			
			return true;
		}else{
			return false;
		}
		
	}	
	function save(){

		$this->update($this->quote,$this->status,$this->must_show);
		return true;

	}

	function update($quote,$status,$must_show,$quote_id)
	{

		$security = new security();

		if($must_show == 1){
			$q="update quote set must_show=0";
			$res = $this->conn->query($q);								
		}

		$query="update quote set quote=?,status=?,must_show=? where quote_id=?";

		$stmt = $this->conn->prepare($query) or die(mysqli_error($this->conn));
		$stmt->bind_param('sssi',($security->xss_clean($quote))
			,($security->xss_clean($status))
			,($security->xss_clean($must_show))			
			,($security->xss_clean($quote_id))			
		)or die(mysqli_error($this->conn));

		$stmt->execute()or die(mysqli_error($this->conn));
	
	}
	function insert($quote,$status,$must_show)
	{

		$security = new security();
		if($must_show == 1){
			$q="update quote set must_show=0";
			$res = $this->conn->query($q);								
		}

		$query="insert into quote(quote,status,must_show)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',
			($security->xss_clean($quote))
			,($security->xss_clean($status))
			,($security->xss_clean($must_show)))or die("error insert user".mysqli_error($this->conn));

		$stmt->execute()or die(mysqli_error($this->conn));		
	}	
}

?>
