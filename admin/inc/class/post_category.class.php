<?php
class post_category
{
	public $post_category_id;
	public $post_category_name;
	public $seo_url;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$post_category_id='',$post_category_name='',$seo_url='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->post_category_id = $post_category_id;
		$this->post_category_name = $post_category_name;
		$this->seo_url = $seo_url;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_post_category_id($post_category_id)
	{
		$security = new security();
		$query="select *
		from cms_post_category
		where post_category_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($post_category_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_slug($slug)
	{
		$security = new security();
		$query="select *
		from cms_post_category
		where seo_url = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($slug);
		$stmt->bind_param('s', $id);
		$stmt->execute();

		$stmt->bind_result($post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function post_category_all()
	{
		$query="select * 
		from cms_post_category 
		order by post_category_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$post_category=new post_category($this->conn,$post_category_id,$post_category_name,$seo_url,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$post_category;
		}
		return $data;
	}
	function update($post_category_name,$seo_url,$updated_by,$post_category_id)
	{
		$security = new security();
		$query="update cms_post_category set post_category_name=?,seo_url=?,updated_date=?,updated_by=? where 
		post_category_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssi',($security->xss_clean($post_category_name)),($security->xss_clean($seo_url)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($post_category_id)));
		$stmt->execute();
	}
	function insert($post_category_name,$seo_url,$created_by)
	{
		$security = new security();
		$query="insert into cms_post_category(post_category_name,seo_url,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($post_category_name)),($security->xss_clean($seo_url)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($post_category_id)
	{
		$security = new security();
		$query="delete from cms_post_category where post_category_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($post_category_id)));
		$stmt->execute();
	}
}
?>