<?php
class access
{
	public $access_id;
	public $access_name;
	public $parent_id;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $parent_name;
	private $conn;
	function __construct($conn='',$access_id='',$access_name='',$parent_id='',$created_date='',$created_by='',$updated_date="",$updated_by='',$parent_name='')
	{
		$this->conn = $conn;
		$this->access_id = $access_id;
		$this->access_name = $access_name;
		$this->parent_id = $parent_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
		$this->parent_name = $parent_name;
	}
	function get_by_access_id($access_id)
	{
		$security = new security();
		$query="select as1.*,as2.access_name as parent_name
		from access_settings as1
		left join access_settings as2
		on as1.parent_id = as2.access_id
		where as1.access_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($access_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($access_id,$access_name,$parent_id,$created_date,$created_by,$updated_by,$updated_date,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		
	}
	function access_all_parent()
	{
		$query="select *
		from access_settings
		where parent_id=0
		order by access_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new access($this->conn,$access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function access_all_children($parent_id)
	{
		$security = new security();
		$query="select m1.*
		, m2.access_name as parent_name
		from access_settings m1 left join access_settings m2 
		on m1.parent_id = m2.access_id 
		where m1.parent_id = ?
		order by m1.access_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($parent_id)));
		$stmt->execute();

		$stmt->bind_result($access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new access($this->conn,$access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function access_all()
	{
		$query="select as1.*,as2.access_name as parent_name 
		from access_settings as1
		left join access_settings as2
		on as1.parent_id = as2.access_id 
		order by as1.access_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$access=new access($this->conn,$access_id,$access_name,$parent_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
			$data[]=$access;
		}
		return $data;
	}
	function update($access_name,$parent_id,$updated_by,$access_id)
	{
		$security = new security();
		$query="update access_settings set access_name=?,parent_id=?,updated_date=?,updated_by=? where 
		access_id=?";
		$stmt = $this->conn->prepare($query) or die("sql query error :".mysqli_error());
		$stmt->bind_param('ssssi',($security->xss_clean($access_name)),($security->xss_clean($parent_id)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($access_id)));
		$stmt->execute() or die("sql execute error ".mysqli_error($this->conn));
	}
	function insert($access_name,$parent_id,$created_by)
	{
		$security = new security();
		$query="insert into access_settings(access_name,parent_id,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('siss',($security->xss_clean($access_name)),($security->xss_clean($parent_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($access_id)
	{
		$security = new security();
		$query="delete from access_settings where access_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($access_id)));
		$stmt->execute();
	}
}
?>