<?php
class payment
{
        public $id;
	public $user_id;
	public $amount;
	public $payment_type;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	public $conn;
	function __construct($conn=''
        ,$id=''
	,$user_id=''
	,$amount=''
	,$payment_type=''
	,$created_date=''
	,$created_by=''
	,$updated_date=''
	,$updated_by=''
	)
	{
		$this->conn = $conn;
                $this->id=$id;
		$this->user_id=$user_id;
		$this->amount= $amount;
		$this->payment_type=$payment_type;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
	}
	function get_by_id($id)
	{
		$security = new security();
		$query="select *
		from payment
		where id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result(
                        $id
			,$user_id
			,$amount
			,$payment_type
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
                        ,$id
			,$user_id
			,$amount
			,$payment_type
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
		
	}
	function payment_all()
	{
		$query="select * 
		from payment 
		order by id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result(
                        $id
			,$user_id
			,$amount
			,$payment_type
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
		);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
		$title=new front_user_patient($this->conn
                        ,$id
			,$user_id
			,$amount
			,$payment_type
			,$created_date
			,$created_by
			,$updated_date
			,$updated_by
			);
			$data[]=$title;
		}
		return $data;
	}
	function update(			
			$user_id
			,$amount
			,$payment_type
			,$updated_by
			,$id
)
	{
		$security = new security();
		$query="update payment set user_id=?
			,amount=?
			,payment_type=?
			,updated_date=?
			,updated_by=?
		where 
		id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssi',($security->xss_clean($user_id))
			,($security->xss_clean($amount))
			,($security->xss_clean($payment_type))
		,(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($id)));
		$stmt->execute();
	}
	function insert(			
			$user_id
			,$amount
			,$payment_type
			,$created_by
)
	{
		$security = new security();
		$query="insert into payment(
			user_id
			,amount
			,payment_type
			,created_date
			,created_by
		)values(?,?,?,?,?)";
		$stmt = $this->conn->prepare($query) or  die("error query".mysqli_error($this->conn));
		$stmt->bind_param('sisss',($security->xss_clean($user_id))
			,($security->xss_clean($amount))
			,($security->xss_clean($payment_type))
                        ,(date("Y-m-d H:i:s"))
                        ,($security->xss_clean($created_by)))or die("error insert user".mysqli_error($this->conn));
		$stmt->execute()or die(mysqli_error($this->conn));
	}
	function delete($id)
	{
		$security = new security();
		$query="delete from payment where id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($id)));
		$stmt->execute();
	}
}
?>
