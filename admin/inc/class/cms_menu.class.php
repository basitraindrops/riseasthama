<?php
class cms_menu
{
	public $cms_menu_id;
	public $cms_menu_name;
	public $cms_menu_page;
	public $parent_id;
	public $icon;
	public $menu_category_id;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	public $parent_name;
	private $conn;
	function __construct($conn='',$cms_menu_id='',$cms_menu_name='',$cms_menu_page='',$parent_id='',$icon="",$menu_category_id="",$created_date='',$created_by='',$updated_date='',$updated_by="",$parent_name='')
	{
		$this->conn = $conn;
		$this->cms_menu_id = $cms_menu_id;
		$this->cms_menu_name = $cms_menu_name;
		$this->cms_menu_page= $cms_menu_page;
		$this->parent_id= $parent_id;
		$this->icon=$icon;
		$this->menu_category_id=$menu_category_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
		$this->parent_name = $parent_name;
	}
	function get_by_cms_menu_id($cms_menu_id)
	{
		$security = new security();
		$query="select m1.*
		, m2.cms_menu_name as parent_name
		from cms_menu m1 left join cms_menu m2 
		on m1.parent_id = m2.cms_menu_id 
		where m1.cms_menu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($cms_menu_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		
	}
	function cms_menu_all()
	{
		$query="select m1.*
		, m2.cms_menu_name as parent_name
		from cms_menu m1 left join cms_menu m2 
		on m1.parent_id = m2.cms_menu_id 
		order by m1.cms_menu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new cms_menu($this->conn,$cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function cms_menu_all_parent($menu_category_id)
	{
		$query="select *
		from cms_menu
		where parent_id=0 and menu_category_id = ?
		order by cms_menu_id asc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($menu_category_id)));
		$stmt->execute();

		$stmt->bind_result($cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new cms_menu($this->conn,$cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
		return $data;
	}
	function cms_menu_all_children($parent_id)
	{
		$security = new security();
		$query="select m1.*
		, m2.cms_menu_name as parent_name
		from cms_menu m1 left join cms_menu m2 
		on m1.parent_id = m2.cms_menu_id 
		where m1.parent_id = ?
		order by m1.cms_menu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($parent_id)));
		$stmt->execute();

		$stmt->bind_result($cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new cms_menu($this->conn,$cms_menu_id,$cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function update($cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$updated_by,$cms_menu_id)
	{
		$security = new security();
		$query="update cms_menu 
		set cms_menu_name=?
		, cms_menu_page=?
		, parent_id=?
		, icon=?
		,menu_category_id=?
		, updated_date=?
		, updated_by=? where 
		cms_menu_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssisissi',($security->xss_clean($cms_menu_name)),($security->xss_clean($cms_menu_page)),($security->xss_clean($parent_id)),($security->xss_clean($icon)),($security->xss_clean($menu_category_id)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($cms_menu_id)));
		$stmt->execute();
	}
	function insert($cms_menu_name,$cms_menu_page,$parent_id,$icon,$menu_category_id,$created_by)
	{
		$security = new security();
		$query="insert into cms_menu
		(cms_menu_name
		, cms_menu_page
		, parent_id
		, icon
		, menu_category_id
		, created_date
		, created_by)
		values(?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssisiss',($security->xss_clean($cms_menu_name)),($security->xss_clean($cms_menu_page)),($security->xss_clean($parent_id)),($security->xss_clean($icon)),($security->xss_clean($menu_category_id)),date("Y-m-d H:i:s"),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($cms_menu_id)
	{
		$security = new security();
		$query="delete from cms_menu where cms_menu_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($cms_menu_id)));
		$stmt->execute();
	}
}
?>