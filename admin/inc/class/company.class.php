<?php
class company
{
	public $company_id;
	public $registration_number;
	public $registration_date;
	public $company_name;
	public $company_leader;
	public $company_npwp;
	public $company_npwp_file;
	public $company_address;
	public $company_country;
	public $company_province;
	public $company_kabupaten;
	public $company_zip_code;
	public $company_phone;
	public $company_fax;
	public $company_email;
	public $company_siup_file;
	public $company_domicilion_letter_file;
	public $company_situ_file;
	public $company_tdp_file;
	public $company_approval_status;
	public $company_category;
	public $company_jenis_izin;
	
	public $status;
	public $user_id;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;

	public $province_name;
	public $kabupaten_name;
	private $conn;
	
	function __construct($conn=''
	,$company_id=""
	,$registration_number=""
	,$registration_date=""
	,$company_name=""
	,$company_leader=""
	,$company_npwp=""
	,$company_npwp_file=""
	,$company_address=""
	,$company_country=""
	,$company_province=""
	,$company_kabupaten=""
	,$company_zip_code=""
	,$company_phone=""
	,$company_fax=""
	,$company_email=""
	,$company_siup_file=""
	,$company_domicilion_letter_file=""
	,$company_situ_file=""
	,$company_tdp_file=""
	,$company_approval_status=""
	,$company_category=""
	,$company_jenis_izin=""
	,$status=""
	,$user_id=""
	,$created_date=""
	,$created_by=""
	,$updated_date=""
	,$updated_by=""
	,$province_name=""
	,$kabupaten_name=""
	)
	{
		$this->conn = $conn;
		$this->company_id=$company_id;
		$this->registration_number=$registration_number;
		$this->registration_date=$registration_date;
		$this->company_name=$company_name;
		$this->company_leader=$company_leader;
		$this->company_npwp=$company_npwp;
		$this->company_npwp_file=$company_npwp_file;
		$this->company_address=$company_address;
		$this->company_country=$company_country;
		$this->company_province=$company_province;
		$this->company_kabupaten=$company_kabupaten;
		$this->company_zip_code=$company_zip_code;
		$this->company_phone=$company_phone;
		$this->company_fax=$company_fax;
		$this->company_email=$company_email;
		$this->company_siup_file=$company_siup_file;
		$this->company_domicilion_letter_file=$company_domicilion_letter_file;
		$this->company_situ_file=$company_situ_file;
		$this->company_tdp_file=$company_tdp_file;
		$this->company_approval_status=$company_approval_status;
		$this->company_category=$company_category;
		$this->company_jenis_izin=$company_jenis_izin;
		$this->status=$status;
		$this->user_id=$user_id;
		$this->created_date=$created_date;
		$this->created_by=$created_by;
		$this->updated_date=$updated_date;
		$this->updated_by=$updated_by;
		$this->province_name=$province_name;
		$this->kabupaten_name=$kabupaten_name;

	}
	function loginby_email_password($email,$password)
	{
		$query="select *
		from perusahaan where user_email = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$security = new security();
		$stmt->bind_param('s', ($security->xss_clean($email)));
		$stmt->execute();

		$stmt->bind_result(
					$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$encryptObj = new encryption();
		//echo $password;
		if($encryptObj->validate_password($password, $result_password))
		{
			$this->__construct($this->conn
					,$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$user_password
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name=""
					,$kabupaten_name=""
				);
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_by_company_id($company_id)
	{
		$query="select p.*
		,pr.province_name
		,kab.kabupaten_name 
		from perusahaan p
		left join province pr
		on pr.province_code = p.company_province
		left join kabupaten kab
		on kab.kabupaten_code = p.company_kabupaten
		where p.company_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $company_id);
		$stmt->execute();

		$stmt->bind_result(					
					$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name
					,$kabupaten_name
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
					,$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name
					,$kabupaten_name
			);
		
	}
	function get_by_user_id($company_id)
	{
		$query="select p.*
		,pr.province_name
		,kab.kabupaten_name 
		from perusahaan p
		left join province pr
		on pr.province_code = p.company_province
		left join kabupaten kab
		on kab.kabupaten_code = p.company_kabupaten
		where p.user_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i', $company_id);
		$stmt->execute();

		$stmt->bind_result(					
					$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name
					,$kabupaten_name
			);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn
					,$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name
					,$kabupaten_name
			);
		
	}
	function company_all()
	{
		$query="select *
		from perusahaan 
		order by company_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$company=new company($this->conn=""
					,$company_id
					,$registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$company_approval_status
					,$company_category
					,$company_jenis_izin
					,$status
					,$user_id
					,$created_date
					,$created_by
					,$updated_date
					,$updated_by
					,$province_name=""
					,$kabupaten_name=""
			);
			$data[]=$company;
		}
//		echo count($data);
		return $data;
	}
	function update_password($new_password,$user_name,$user_id)
	{
		$security = new security();
		$query="update perusahaan set user_password=?,updated_date=?,updated_by=? where company_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($new_password)),(date("Y-m-d H:i:s")),($security->xss_clean($user_name)),($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function update_status($status,$user_name,$user_id)
	{
		$security = new security();
		$query="update perusahaan set status=?,updated_date=?,updated_by=? where user_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('issi'
		,($security->xss_clean($status))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($user_name))
		,($security->xss_clean($user_id)));
		$stmt->execute();
	}
	function update($registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$status
					,$updated_by
					,$company_id)
	{
		$security = new security();
		$query="update menu 
		set registration_number=?
					,registration_date=?
					,company_name=?
					,company_leader=?
					,company_npwp=?
					,company_npwp_file=?
					,company_address=?
					,company_country=?
					,company_province=?
					,company_kabupaten=?
					,company_zip_code=?
					,company_phone=?
					,company_fax=?
					,company_email=?
					,company_siup_file=?
					,company_domicilion_letter_file=?
					,company_situ_file=?
					,company_tdp_file=?
					,status=?
					,updated_date=?
					,updated_by=?
					where 
					company_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssssssssssssi'
		,($security->xss_clean($registration_number))
					,($security->xss_clean($registration_date))
					,($security->xss_clean($company_name))
					,($security->xss_clean($company_leader))
					,($security->xss_clean($company_npwp))
					,($security->xss_clean($company_npwp_file))
					,($security->xss_clean($company_address))
					,($security->xss_clean($company_country))
					,($security->xss_clean($company_province))
					,($security->xss_clean($company_kabupaten))
					,($security->xss_clean($company_zip_code))
					,($security->xss_clean($company_phone))
					,($security->xss_clean($company_fax))
					,($security->xss_clean($company_email))
					,($security->xss_clean($company_siup_file))
					,($security->xss_clean($company_domicilion_letter_file))
					,($security->xss_clean($company_situ_file))
					,($security->xss_clean($company_tdp_file))
					,($security->xss_clean($status))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($updated_by))
		,($security->xss_clean($company_id)));
		$stmt->execute();
	}
	function approve($status
					,$company_category
					,$company_jenis_izin
					,$updated_by
					,$company_id)
	{
		$security = new security();
		$query="update perusahaan 
		set company_approval_status=?
		,company_category=?
		,company_jenis_izin=?
		,updated_date=?
		,updated_by=?
		where 
		company_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssssi'
		,($security->xss_clean($status))
		,($security->xss_clean($company_category))
		,($security->xss_clean($company_jenis_izin))
		,(date("Y-m-d H:i:s"))
		,($security->xss_clean($updated_by))
		,($security->xss_clean($company_id)));
		$stmt->execute();
	}
	function insert($registration_number
					,$registration_date
					,$company_name
					,$company_leader
					,$company_npwp
					,$company_npwp_file
					,$company_address
					,$company_country
					,$company_province
					,$company_kabupaten
					,$company_zip_code
					,$company_phone
					,$company_fax
					,$company_email
					,$company_siup_file
					,$company_domicilion_letter_file
					,$company_situ_file
					,$company_tdp_file
					,$status
					,$user_id
					,$created_by
	)
	{
		$security = new security();
		$query="insert into perusahaan
		(registration_number
					,registration_date
					,company_name
					,company_leader
					,company_npwp
					,company_npwp_file
					,company_address
					,company_country
					,company_province
					,company_kabupaten
					,company_zip_code
					,company_phone
					,company_fax
					,company_email
					,company_siup_file
					,company_domicilion_letter_file
					,company_situ_file
					,company_tdp_file
					,status
					,user_id
					,created_date
					,created_by)
		values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssssssssssssssssss'
					,($security->xss_clean($registration_number))
					,($security->xss_clean($registration_date))
					,($security->xss_clean($company_name))
					,($security->xss_clean($company_leader))
					,($security->xss_clean($company_npwp))
					,($security->xss_clean($company_npwp_file))
					,($security->xss_clean($company_address))
					,($security->xss_clean($company_country))
					,($security->xss_clean($company_province))
					,($security->xss_clean($company_kabupaten))
					,($security->xss_clean($company_zip_code))
					,($security->xss_clean($company_phone))
					,($security->xss_clean($company_fax))
					,($security->xss_clean($company_email))
					,($security->xss_clean($company_siup_file))
					,($security->xss_clean($company_domicilion_letter_file))
					,($security->xss_clean($company_situ_file))
					,($security->xss_clean($company_tdp_file))
					,($security->xss_clean($status))
					,($security->xss_clean($user_id))
					,(date("Y-m-d H:i:s"))
					,($security->xss_clean($created_by)));
		$stmt->execute();
	}

	function logout($email)
	{
		$query="update perusahaan set last_login=? where user_email = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ss', date("Y-m-d H:i:s"),$email);
		$stmt->execute();
	}
	function delete($company_id)
	{
		$query="delete from perusahaan where company_id = ?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',$company_id);
		$stmt->execute();
	}
}
?>