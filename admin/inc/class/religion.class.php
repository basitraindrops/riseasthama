<?php
class religion
{
	public $r_id;
	public $r_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$r_id='',$r_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->r_id = $r_id;
		$this->r_name = $r_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_r_id($r_id)
	{
		$security = new security();
		$query="select *
		from general_religion
		where r_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($r_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($r_id,$r_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$r_id,$r_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function religion_all()
	{
		$query="select * 
		from general_religion 
		order by r_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($r_id,$r_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$religion=new religion($this->conn,$r_id,$r_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$religion;
		}
		return $data;
	}
	function update($r_name,$updated_by,$r_id)
	{
		$security = new security();
		$query="update general_religion set r_name=?,updated_date=?,updated_by=? where 
		r_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($r_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($r_id)));
		$stmt->execute();
	}
	function insert($r_name,$created_by)
	{
		$security = new security();
		$query="insert into general_religion(r_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($r_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($r_id)
	{
		$security = new security();
		$query="delete from general_religion where r_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($r_id)));
		$stmt->execute();
	}
}
?>