<?php
class post
{
	public $post_id;
	public $post_title;
	public $post_content;
	public $html_title;
	public $meta_description;
	public $meta_keyword;
	public $seo_url;
	public $post_category_id;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$post_id='',$post_title='',$post_content='',$html_title='',$meta_description='',$meta_keyword='',$seo_url='',$post_category_id='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->post_id = $post_id;
		$this->post_title = $post_title;
		$this->post_content = $post_content;
		$this->html_title = $html_title;
		$this->meta_description = $meta_description;
		$this->meta_keyword = $meta_keyword;
		$this->seo_url = $seo_url;
		$this->post_category_id = $post_category_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_post_id($post_id)
	{
		$security = new security();
		$query="select *
		from cms_post
		where post_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($post_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_seo_url($seo_url)
	{
		$security = new security();
		$query="select *
		from cms_post
		where seo_url = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($seo_url);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function post_all()
	{
		$query="select * 
		from cms_post 
		order by post_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$post=new post($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$post;
		}
		return $data;
	}
	function post_by_category_id($post_category_id)
	{
		$query="select * 
		from cms_post 
		order by post_id where post_category_id = ? desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($post_category_id)));
		$stmt->execute();

		$stmt->bind_result($post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$post=new post($this->conn,$post_id,$post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$post;
		}
		return $data;
	}
	function update($post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$updated_by,$post_id)
	{
		$security = new security();
		$query="update cms_post set post_title=?, post_content=?,html_title=?,meta_description=?,meta_keyword=? ,seo_url=?,post_category_id=?,updated_date=?,updated_by=? where 
		post_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssissi',($security->xss_clean($post_title)),($security->xss_clean($post_content)),($security->xss_clean($html_title)),($security->xss_clean($meta_keyword)),($security->xss_clean($meta_keyword)),($security->xss_clean($seo_url)),($security->xss_clean($post_category_id)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($post_id)));
		$stmt->execute();
	}
	function insert($post_title,$post_content,$html_title,$meta_description,$meta_keyword,$seo_url,$post_category_id,$created_by)
	{
		$security = new security();
		$query="insert into cms_post(post_title,post_content,html_title,meta_description,meta_keyword,seo_url,post_category_id,created_date,created_by)values(?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssiss',($security->xss_clean($post_title)),($security->xss_clean($post_content)),($security->xss_clean($html_title)),($security->xss_clean($meta_description)),($security->xss_clean($meta_keyword)),($security->xss_clean($seo_url)),($security->xss_clean($post_category_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($post_id)
	{
		$security = new security();
		$query="delete from cms_post where post_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($post_id)));
		$stmt->execute();
	}
}
?>