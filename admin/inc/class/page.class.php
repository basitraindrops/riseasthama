<?php
class page
{
	public $page_id;
	public $page_title;
	public $page_content;
	public $html_title;
	public $meta_description;
	public $meta_keyword;
	public $seo_url;
	public $page_category_id;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$page_id='',$page_title='',$page_content='',$html_title='',$meta_description='',$meta_keyword='',$seo_url='',$page_category_id='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->page_id = $page_id;
		$this->page_title = $page_title;
		$this->page_content = $page_content;
		$this->html_title = $html_title;
		$this->meta_description = $meta_description;
		$this->meta_keyword = $meta_keyword;
		$this->seo_url = $seo_url;
		$this->page_category_id = $page_category_id;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_page_id($page_id)
	{
		$security = new security();
		$query="select *
		from cms_page
		where page_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($page_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function get_by_seo_url($seo_url)
	{
		$security = new security();
		$query="select *
		from cms_page
		where seo_url = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($seo_url);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function page_all()
	{
		$query="select * 
		from cms_page 
		order by page_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$page=new page($this->conn,$page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$page;
		}
		return $data;
	}
	function page_by_category_id($page_category_id)
	{
		$query="select * 
		from cms_page 
		order by page_id where page_category_id = ? desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($page_category_id)));
		$stmt->execute();

		$stmt->bind_result($page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$page=new page($this->conn,$page_id,$page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$page;
		}
		return $data;
	}
	function update($page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$updated_by,$page_id)
	{
		$security = new security();
		$query="update cms_page set page_title=?, page_content=?,html_title=?,meta_description=?,meta_keyword=? ,seo_url=?,page_category_id=?,updated_date=?,updated_by=? where 
		page_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssissi',($security->xss_clean($page_title)),($security->xss_clean($page_content)),($security->xss_clean($html_title)),($security->xss_clean($meta_description)),($security->xss_clean($meta_keyword)),($security->xss_clean($seo_url)),($security->xss_clean($page_category_id)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($page_id)));
		$stmt->execute();
	}
	function insert($page_title,$page_content,$html_title,$meta_description,$meta_keyword,$seo_url,$page_category_id,$created_by)
	{
		$security = new security();
		$query="insert into cms_page(page_title,page_content,html_title,meta_description,meta_keyword,seo_url,page_category_id,created_date,created_by)values(?,?,?,?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssiss',($security->xss_clean($page_title)),($security->xss_clean($page_content)),($security->xss_clean($html_title)),($security->xss_clean($meta_description)),($security->xss_clean($meta_keyword)),($security->xss_clean($seo_url)),($security->xss_clean($page_category_id)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($page_id)
	{
		$security = new security();
		$query="delete from cms_page where page_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($page_id)));
		$stmt->execute();
	}
}
?>