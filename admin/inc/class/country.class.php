<?php
class country
{
	public $country_id;
	public $country_code;
	public $country_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$country_id='',$country_code='',$country_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->country_id = $country_id;
		$this->country_code = $country_code;
		$this->country_name = $country_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_country_id($country_id)
	{
		$security = new security();
		$query="select *
		from country
		where country_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($country_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($country_id,$country_code,$country_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$country_id,$country_code,$country_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function country_all()
	{
		$query="select * 
		from country 
		order by country_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($country_id,$country_code,$country_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$country=new country($this->conn,$country_id,$country_code,$country_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$country;
		}
		return $data;
	}
	function update($country_code,$country_name,$updated_by,$country_id)
	{
		$security = new security();
		$query="update country set country_code=?,country_name=?,updated_date=?,updated_by=? where 
		country_id=?";
		$stmt = $this->conn->prepare($query);
//		echo ($security->xss_clean($country_code));
		$stmt->bind_param('ssssi',($security->xss_clean($country_name)),($security->xss_clean($country_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($country_id)));
		$stmt->execute();
	}
	function insert($country_code,$country_name,$created_by)
	{
		$security = new security();
		$query="insert into country(country_code, country_name,created_date,created_by)values(?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssss',($security->xss_clean($country_code)),($security->xss_clean($country_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($country_id)
	{
		$security = new security();
		$query="delete from country where country_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($country_id)));
		$stmt->execute();
	}
}
?>