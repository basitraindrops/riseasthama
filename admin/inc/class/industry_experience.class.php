<?php
class industry_experience
{
	public $industry_experience_id;
	public $industry_experience_name;
	public $created_date;
	public $created_by;
	public $updated_by;
	public $updated_date;
	private $conn;
	function __construct($conn='',$industry_experience_id='',$industry_experience_name='',$created_date='',$created_by='',$updated_date="",$updated_by='')
	{
		$this->conn = $conn;
		$this->industry_experience_id = $industry_experience_id;
		$this->industry_experience_name = $industry_experience_name;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_by = $updated_by;
		$this->updated_date = $updated_date;
	}
	function get_by_industry_experience_id($industry_experience_id)
	{
		$security = new security();
		$query="select *
		from industry_experience
		where industry_experience_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($industry_experience_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($industry_experience_id,$industry_experience_name,$created_date,$created_by,$updated_by,$updated_date);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$industry_experience_id,$industry_experience_name,$created_date,$created_by,$updated_date,$updated_by);
		
	}
	function industry_experience_all()
	{
		$query="select * 
		from industry_experience 
		order by industry_experience_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($industry_experience_id,$industry_experience_name,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$industry_experience=new industry_experience($this->conn,$industry_experience_id,$industry_experience_name,$created_date,$created_by,$updated_date,$updated_by);
			$data[]=$industry_experience;
		}
		return $data;
	}
	function update($industry_experience_name,$updated_by,$industry_experience_id)
	{
		$security = new security();
		$query="update industry_experience set industry_experience_name=?,updated_date=?,updated_by=? where 
		industry_experience_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sssi',($security->xss_clean($industry_experience_name)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($industry_experience_id)));
		$stmt->execute();
	}
	function insert($industry_experience_name,$created_by)
	{
		$security = new security();
		$query="insert into industry_experience(industry_experience_name,created_date,created_by)values(?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('sss',($security->xss_clean($industry_experience_name)),(date("Y-m-d H:i:s")),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($industry_experience_id)
	{
		$security = new security();
		$query="delete from industry_experience where industry_experience_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($industry_experience_id)));
		$stmt->execute();
	}
}
?>