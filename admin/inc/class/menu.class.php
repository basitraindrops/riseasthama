<?php
class menu
{
	public $menu_id;
	public $menu_name;
	public $menu_page;
	public $parent_id;
	public $icon;
	public $created_date;
	public $created_by;
	public $updated_date;
	public $updated_by;
	public $parent_name;
	private $conn;
	function __construct($conn='',$menu_id='',$menu_name='',$menu_page='',$parent_id='',$icon="",$created_date='',$created_by='',$updated_date='',$updated_by="",$parent_name='')
	{
		$this->conn = $conn;
		$this->menu_id = $menu_id;
		$this->menu_name = $menu_name;
		$this->menu_page= $menu_page;
		$this->parent_id= $parent_id;
		$this->icon=$icon;
		$this->created_date = $created_date;
		$this->created_by = $created_by;
		$this->updated_date = $updated_date;
		$this->updated_by = $updated_by;
		$this->parent_name = $parent_name;
	}
	function get_by_menu_id($menu_id)
	{
		$security = new security();
		$query="select m1.*
		, m2.menu_name as parent_name
		from menu m1 left join menu m2 
		on m1.parent_id = m2.menu_id 
		where m1.menu_id = ? limit 0,1";
		$stmt = $this->conn->prepare($query);
		$id=$security->xss_clean($menu_id);
		$stmt->bind_param('i', $id);
		$stmt->execute();

		$stmt->bind_result($menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$stmt->fetch();
		$this->__construct($this->conn,$menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		
	}
	function menu_all()
	{
		$query="select m1.*
		, m2.menu_name as parent_name
		from menu m1 left join menu m2 
		on m1.parent_id = m2.menu_id 
		order by m1.menu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new menu($this->conn,$menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function menu_all_parent()
	{
		$query="select *
		from menu
		where parent_id=0
		order by menu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$stmt->bind_result($menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new menu($this->conn,$menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function menu_all_children($parent_id)
	{
		$security = new security();
		$query="select m1.*
		, m2.menu_name as parent_name
		from menu m1 left join menu m2 
		on m1.parent_id = m2.menu_id 
		where m1.parent_id = ?
		order by m1.menu_id desc";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($parent_id)));
		$stmt->execute();

		$stmt->bind_result($menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name);
		//$data = $result->fetch_array(MYSQLI_BOTH);
		$data=array();
		while($stmt->fetch())
		{
			$prv=new menu($this->conn,$menu_id,$menu_name,$menu_page,$parent_id,$icon,$created_date,$created_by,$updated_date,$updated_by,$parent_name="");
			$data[]=$prv;
		}
//		echo count($data);
		return $data;
	}
	function update($menu_name,$menu_page,$parent_id,$icon,$updated_by,$menu_id)
	{
		$security = new security();
		$query="update menu 
		set menu_name=?
		, menu_page=?
		, parent_id=?
		, icon=?
		, updated_date=?
		, updated_by=? where 
		menu_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssssi',($security->xss_clean($menu_name)),($security->xss_clean($menu_page)),($security->xss_clean($parent_id)),($security->xss_clean($icon)),(date("Y-m-d H:i:s")),($security->xss_clean($updated_by)),($security->xss_clean($menu_id)));
		$stmt->execute();
	}
	function insert($menu_name,$menu_page,$parent_id,$icon,$created_by)
	{
		$security = new security();
		$query="insert into menu
		(menu_name
		, menu_page
		, parent_id
		, icon
		, created_date
		, created_by)
		values(?,?,?,?,?,?)";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('ssssss',($security->xss_clean($menu_name)),($security->xss_clean($menu_page)),($security->xss_clean($parent_id)),($security->xss_clean($icon)),date("Y-m-d H:i:s"),($security->xss_clean($created_by)));
		$stmt->execute();
	}
	function delete($menu_id)
	{
		$security = new security();
		$query="delete from menu where menu_id=?";
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param('i',($security->xss_clean($menu_id)));
		$stmt->execute();
	}
}
?>