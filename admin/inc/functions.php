<?php
function user_access($mysqli,$user_id)
{
//	require_once(CLASS_DIR.'encryption.class.php');
	require_once(CLASS_DIR."user_access.class.php");
//	$sessionObj=new SecureSession();
	$ua = new user_access($mysqli);
	$post_name=basename($_SERVER['PHP_SELF']);
	switch($post_name)
	{
		case "religion.php":
			$ua->get2("religion",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "agama_delete_ajax.php":
			$ua->get2("delete_agama",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province.php":
			$ua->get2("province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province_delete_ajax.php":
			$ua->get2("delete_province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group.php":
			$ua->get2("view_user_group",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_delete_ajax.php":
			$ua->get2("delete_user_group",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "access.php":
			$ua->get2("view_access_settings",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "access_delete_ajax.php":
			$ua->get2("delete_access_settings",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "kabupaten.php":
			$ua->get2("view_kabupaten",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "kabupaten_delete_ajax.php":
			$ua->get2("delete_kabupaten",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "menu.php":
			$ua->get2("view_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "menu_delete_ajax.php":
			$ua->get2("delete_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province.php":
			$ua->get2("view_province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "province_delete_ajax.php":
			$ua->get2("delete_province",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_menu.php":
			$ua->get2("user_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_menu_ajax.php":
			$ua->get2("user_menu",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_access.php":
			$ua->get2("user_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_access_ajax.php":
			$ua->get2("user_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_access.php":
			$ua->get2("user_group_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_group_access_ajax.php":
			$ua->get2("user_group_access",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "password_update.php":
			$ua->get2("administration_user_password",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "perusahaan_delete_ajax.php":
			$ua->get2("delete_perusahaan",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		case "user_internal_delete_ajax.php":
			$ua->get2("delete_user_internal",$user_id);
			if(!isset($ua->ua_id))
				header("Location:index.php");
			break;
		default:
			break;

	}
}
function user_access_each($mysqli,$access_name,$user_id)
{
	require_once(CLASS_DIR."user_access.class.php");
	$ua = new user_access($mysqli);
	$ua->get2($access_name,$user_id);
	if(!isset($ua->ua_id))
		return false;
	else
		return true;	
}
function generate_password($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=|~`';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}
function get_directory($user_id,$mysqli)
{
	require_once(CLASS_DIR.'user.class.php');
	require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user_internal.class.php');
	$user=new user($mysqli);
	$user->get_by_user_id($user_id);
	$dir="";
	if($user->user_group_id==2)
	{
		$dir="file/";
		$company=new company($mysqli);
		$company->get_by_user_id($user_id);
		$dir.=$company->registration_number."/";
	}
	else
	{
		$dir="file_ui";
		$ui=new user_internal($mysqli);
		$ui->get_by_user_id($user_id);
		$dir.=$ui->registration_number."/";
	}
	return $dir;
}
function get_profile_picture($user_id,$mysqli)
{
	require_once(CLASS_DIR.'user.class.php');
	require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user_internal.class.php');
	$user=new user($mysqli);
	$user->get_by_user_id($user_id);
	$pic=$user->user_photo_file;
	return $pic;
}
function format_uri( $string, $separator = '-' )
{
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $special_cases = array( '&' => 'and', "'" => '');
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return $string;
}
function dirToArray($dir) { 
   
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 
   
   return $result; 
} 
function encrypt($string,$secretKey)
{
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, pad_key($secretKey), $string,
								  MCRYPT_MODE_ECB, $iv);
	return base64_encode($crypttext);
}
	
function decrypt($string,$secretKey)
{
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
//	echo $iv."<br/>";
	$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, pad_key($secretKey), base64_decode($string),
                                MCRYPT_MODE_ECB, $iv);

// Drop nulls from end of string
	$decrypttext = rtrim($decrypttext, "\0");
	return $decrypttext;

}
function pad_key($key){
    // key is too large
    if(strlen($key) > 32) return false;

    // set sizes
    $sizes = array(16,24,32);

    // loop through sizes and pad key
    foreach($sizes as $s){
        while(strlen($key) < $s) $key = $key."\0";
        if(strlen($key) == $s) break; // finish if the key matches a size
    }

    // return
    return $key;
}
?>