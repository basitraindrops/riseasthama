<?php
ob_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
require(CLASS_DIR.'session_encrypt.class.php');
global $sessionObj;
global $user_email;
global $user_name;
global $user_id;
global $first_name;
global $last_name;

$sessionObj=new SecureSession();
$sessionPath = sys_get_temp_dir();
session_save_path($sessionPath);
session_start();

include_once(CLASS_DIR.'security.class.php');


require(INC_DIR.'database.php');
require_once(CLASS_DIR.'app_setting.class.php');
require_once(INC_DIR.'functions.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'user_group_menu.class.php');
global $user_obj,$settings;
if(isset($_COOKIE['email'])&&$sessionObj->read('user_email')=="")
{
	$user_obj=new user($mysqli);
	$settings=new settings($mysqli);
	$settings->get_by_id(1);
	$user_obj->get_by_email(decrypt($_COOKIE['email'],$settings->salt_key));
	if($user_obj->user_email!="")
	{
		$sessionObj->write('user_email',encrypt($user_obj->user_email,$settings->salt_key));
		$sessionObj->write('user_name',encrypt($user_obj->user_name,$settings->salt_key));
		$sessionObj->write('user_id',encrypt($user_obj->user_id,$settings->salt_key));
		$sessionObj->write('first_name',$user_obj->user_first_name);
		$sessionObj->write('last_name',$user_obj->user_last_name);
		$first_name=$user_obj->user_first_name;
		$last_name=$user_obj->user_last_name;
	}
	else
	{
		header("location:cookies_error.php");
	}

}

date_default_timezone_set('Asia/Jakarta');

// Template variables
$template = array(
    'name'        => 'uAdmin',
    'version'     => '1.6.1',
    'author'      => 'pixelcave',
    'title'       => 'RELASI - Admin',
    'description' => 'RELASI - Admin v1.0.1',
    'header'      => '', // 'fixed-top', 'fixed-bottom'
    'layout'      => '', // 'fixed'
    'theme'       => '', // 'deepblue', 'deepwood', 'deeppurple', 'deepgreen', '' empty for default
    'active_page' => basename($_SERVER['PHP_SELF'])
);
//echo $sessionObj->read("user_id");
// Primary navigation array (the primary navigation will be created automatically based on this array)
if($sessionObj->read("user_id")!="")
{
	require_once(CLASS_DIR.'user_group_menu.class.php');
	$settings=new settings($mysqli);
	$settings->get_by_id(1);

	require_once(CLASS_DIR.'user.class.php');
	$user_obj=new user($mysqli);
	require_once(CLASS_DIR.'user_group_menu.class.php');
	$settings=new settings($mysqli);
	$settings->get_by_id(1);
	$user_obj->get_by_email(decrypt($sessionObj->read("user_email"),$settings->salt_key));
	
	if($user_obj->user_email!="")
	{
			/* DECRYPT WITH SALT KEY */
			$user_email= decrypt($sessionObj->read("user_email"),$settings->salt_key);
			$user_name= decrypt($sessionObj->read("user_name"),$settings->salt_key);
			$user_id= decrypt($sessionObj->read("user_id"),$settings->salt_key);


		//	echo $user_id." ".$settings->salt_key." ".$sessionObj->read("user_id");
			$ugm = new user_group_menu($mysqli);
			$user_all_menu = $ugm->get_all_user_group($user_id);
		//	echo print_r($user_all_menu);
			$primary_nav = array(
				array(
					'name'  => 'Dashboard',
					'url'   => ADMIN_URL,
					'icon'  => 'fa fa-home',
					'parent_id'=>0,
					'menu_id'=>1
				)
			);
		//print_r($user_all_menu); 
			if(count($user_all_menu)>0)
			{
				$temp1=array();
				for($i=0;$i<count($user_all_menu);$i++)
				{
					if($user_all_menu[$i]->parent_id==0)
					{
						$each_root=array(
							'name'=>$user_all_menu[$i]->menu_name
							,'url'=>$user_all_menu[$i]->menu_page
							,'icon'=>$user_all_menu[$i]->icon
							,'parent_id'=>$user_all_menu[$i]->parent_id
							,'menu_id'=>$user_all_menu[$i]->menu_id
							,'sub'=>array()
							);
						$primary_nav[]=$each_root;
					}
				}
				for($i=0;$i<count($primary_nav);$i++)
				{
					for($j=0;$j<count($user_all_menu);$j++)
					{
						if($user_all_menu[$j]->parent_id==$primary_nav[$i]['menu_id'])
						{
							$each_child=array(
								'name'=>$user_all_menu[$j]->menu_name
								,'url'=>$user_all_menu[$j]->menu_page
								,'icon'=>$user_all_menu[$j]->icon
								,'menu_id'=>$user_all_menu[$j]->menu_id
								);
							$primary_nav[$i]['sub'][]=$each_child;
						}
					}
				}
			}


		//echo print_r($primary_nav);

			/* user access */
			include_once(INC_DIR."functions.php");
			user_access($mysqli,$user_id);
	}
/*	else
	{
		header("location:session_error.php");
	}
*/
}


?>