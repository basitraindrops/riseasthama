<?php
/**
 * bottom.php
 *
 * Author: pixelcave
 *
 * The last block of code used in every page of the template
 * We put it in a separate file for consistency. The reason we
 * separated footer.php and bottom.php is for enabling us putting
 * between them extra javascript code needed for specific pages
 *
 */
?>
<script>
window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 5000);
</script>
    </body>
</html>