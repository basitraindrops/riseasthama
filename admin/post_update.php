<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
//echo $sessionObj->read('email');
if(isset($_POST['post_title'])&&isset($_GET['action']))
{
	$seo_url=format_uri( $_POST['post_title'], $separator = '-' );
	if($_GET['action']=="add_new")
	{
		if(user_access_each($mysqli,"create_post",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."post.class.php");
		$ug=new post($mysqli);
//		for($i=1;$i<2000;$i++)
//		{
		$ug->insert($_POST['post_title'],$_POST['post_content'],$_POST['html_title'],$_POST['meta_description'],$_POST['meta_keyword'],$seo_url,$_POST['post_category_id'],$user_name);
//		}
		header("Location:post.php");
	}
	else if($_GET['action']=="update")
	{
		if(user_access_each($mysqli,"update_post",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."post.class.php");
		$ug=new post($mysqli);
		$ug->update($_POST['post_title'],$_POST['post_content'],$_POST['html_title'],$_POST['meta_description'],$_POST['meta_keyword'],$seo_url,$_POST['post_category_id'],$user_name,$_GET['id']);
		$error="Success updating data!";
	}
}
if(isset($_GET['action'])&&isset($_GET['id']))
{
	if($_GET['action']=="update")
	{
		require_once(CLASS_DIR."post.class.php");
		$ug_show=new post($mysqli);
		$ug_show->get_by_post_id($_GET['id']);
	}
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<!-- Post Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="post.php">Post</a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Post"; } else { echo "Update Post"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]."?action=".$_GET['action']; if(isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Post"; } else { echo "Update Post"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Post"; } else { echo "Update existing Post"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="post.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Post Title *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="post_title" name="post_title" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $ug_show->post_title; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Content *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <textarea id="post_content" name="post_content" class="form-control">
						<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo html_entity_decode($ug_show->post_content); } ?>
						</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">HTML Title *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="html_title" name="html_title" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $ug_show->html_title; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Meta Description *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="meta_description" name="meta_description" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $ug_show->meta_description; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Meta Keyword *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="meta_keyword" name="meta_keyword" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $ug_show->meta_keyword; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Category *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <select id="post_category_id" name="post_category_id" class="form-control">
						<?php require_once(CLASS_DIR."post_category.class.php");
						$pc=new post_category($mysqli);
						$data=$pc->post_category_all();
						for($i=0;$i<count($data);$i++)
						{
							?>
							<option value="<?php echo $data[$i]->post_category_id; ?>" <?php 
								if(isset($_GET['action'])&&isset($_GET['id'])){ if($ug_show->post_category_id==$data[$i]->post_category_id) { echo  ' selected="selected" '; } } ?>><?php echo $data[$i]->post_category_name; ?></option>
							<?php
						}
						?>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Post Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                post_title: {
                    required: true,
                    minlength: 3
                }
				},
            messages: {
                post_title: {
                    required: 'Mohon masukkan Nama Group',
                    minlength: 'Nama Group minimal 3 karakter'
                }            
			}
        });
    });
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
