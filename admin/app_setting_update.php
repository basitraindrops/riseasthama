<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
//echo $sessionObj->read('email');
if(isset($_POST['title'])&&isset($_GET['action']))
{
	if(user_access_each($mysqli,"app_setting",$user_id)==false) 
	{
		header("Location:index.php");
		exit();
	}
	if($_GET['action']=="add_new")
	{
		if(user_access_each($mysqli,"create_app_setting",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."app_setting.class.php");
		$s=new settings($mysqli);
		$s->insert($_POST['description'],$_POST['title']
			,$_POST['meta_description']
			,$_POST['meta_keyword']
			,$_POST['admin_email']
			,$_POST['no_reply_email']
			,$_POST['cs_email_1']
			,$_POST['cs_email_2']
			,$_POST['cs_email_3']
			,$_POST['cs_email_4']
			,$_POST['author']
			,$_POST['publisher']
			,$_POST['time_zone']
			,$_POST['primary_address']
			,$_POST['primary_phone']
			,$_POST['primary_mobile_phone']
			,$_POST['facebook']
			,$_POST['twitter']
			,$_POST['instagram']
			,$_POST['linked_in']
			,$_POST['google_plus']
			,$_POST['salt_key'],$user_name);
		$error="Success updating data!";
		header("Location:app_setting.php");
	}
	else if($_GET['action']=="update")
	{
		if(user_access_each($mysqli,"update_app_setting",$user_id)==false) 
		{
			header("Location:index.php");
		}
		require_once(CLASS_DIR."app_setting.class.php");
		$s=new settings($mysqli);
		$s->update($_POST['description'],$_POST['title']
			,$_POST['meta_description']
			,$_POST['meta_keyword']
			,$_POST['admin_email']
			,$_POST['no_reply_email']
			,$_POST['cs_email_1']
			,$_POST['cs_email_2']
			,$_POST['cs_email_3']
			,$_POST['cs_email_4']
			,$_POST['author']
			,$_POST['publisher']
			,$_POST['time_zone']
			,$_POST['primary_address']
			,$_POST['primary_phone']
			,$_POST['primary_mobile_phone']
			,$_POST['facebook']
			,$_POST['twitter']
			,$_POST['instagram']
			,$_POST['linked_in']
			,$_POST['google_plus']
			,$_POST['salt_key'],$user_name,$_GET['id']);
		$error="Success updating data!";
	}
}
$s_show=array();
if(isset($_GET['action'])&&isset($_GET['id']))
{
	if($_GET['action']=="update")
	{
		require_once(CLASS_DIR."app_setting.class.php");
		$s_show=new settings($mysqli);
		$s_show->get_by_id($_GET['id']);
	}
}



?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Application Settings"; } else { echo "Update Application Settings"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]."?action=".$_GET['action']; if(isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Application Settings"; } else { echo "Update Application Settings"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Settings"; } else { echo "Update existing Settings"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="app_setting.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Description *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <textarea id="description" name="description"><?php  if(isset($_GET['id'])) echo $s_show->description; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Meta Title *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="title" name="title" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->meta_title; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Meta Description *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <textarea id="meta_description" name="meta_description" class="form-control"><?php if(isset($_GET['id'])) echo $s_show->meta_description;  ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Meta Keyword *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <textarea id="meta_keyword" name="meta_keyword" class="form-control"><?php if(isset($_GET['id'])) echo $s_show->meta_keyword;  ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Admin Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="admin_email" name="admin_email" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->admin_email; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">No-Reply Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="no_reply_email" name="no_reply_email" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->no_reply_email; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">CS Email 1*</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="cs_email_1" name="cs_email_1" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->cs_email_1; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">CS Email 2*</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="cs_email_2" name="cs_email_2" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->cs_email_2; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">CS Email 3*</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="cs_email_3" name="cs_email_3" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->cs_email_3; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">CS Email 4*</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="cs_email_4" name="cs_email_4" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->cs_email_4; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Author *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="author" name="author" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->author; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Publisher *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="publisher" name="publisher" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->publisher; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Timezone *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="time_zone" name="time_zone" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->time_zone; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Primary Address *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <textarea id="primary_address" name="primary_address" class="form-control"><?php if(isset($_GET['id']))  echo $s_show->primary_address; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Primary Phone *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="primary_phone" name="primary_phone" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->primary_phone; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Primary Mobile Phone *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="primary_mobile_phone" name="primary_mobile_phone" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->primary_mobile_phone; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Facebook *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="facebook" name="facebook" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->facebook; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Twitter *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="twitter" name="twitter" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->twitter; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Instagram *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="instagram" name="instagram" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->instagram; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Linked In *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="linked_in" name="linked_in" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->linked_in; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Google+ *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="google_plus" name="google_plus" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->google_plus; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Salt Key *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="salt_key" name="salt_key" class="form-control" value="<?php if(isset($_GET['id']))  echo $s_show->salt_key; ?>"> &nbsp;&nbsp;<span class="fa fa-info"></span><font color="red"><b>must be 32 characters long</b></font>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
		<p align="left" style="margin-left:20px;"><a href="app_setting.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                title: {
                    required: true,
                    minlength: 3
                },
                meta_description: {
                    required: true,
                    minlength: 3
                },
                description: {
                    required: true,
                    minlength: 3
                },
                meta_keyword: {
                    required: true,
                    minlength: 3
                },
                admin_email: {
                    required: true,
                    minlength: 3
                },
                cs_email_1: {
                    required: true,
                    minlength: 3
                },
                cs_email_2: {
                    required: true,
                    minlength: 3
                },
                cs_email_3: {
                    required: true,
                    minlength: 3
                },
                cs_email_4: {
                    required: true,
                    minlength: 3
                },
                author: {
                    required: true,
                    minlength: 3
                },
                publisher: {
                    required: true,
                    minlength: 3
                },
                time_zone: {
                    required: true,
                    minlength: 3
                },
                primary_address: {
                    required: true,
                    minlength: 3
                },
                primary_phone: {
                    required: true,
                    minlength: 3
                },
                primary_mobile_phone: {
                    required: true,
                    minlength: 3
                },
                facebook: {
                    required: true,
                    minlength: 3
                },
                twitter: {
                    required: true,
                    minlength: 3
                },
                instagram: {
                    required: true,
                    minlength: 3
                },
                linked_in: {
                    required: true,
                    minlength: 3
                },
                google_plus: {
                    required: true,
                    minlength: 3
                },
                salt_key: {
                    required: true,
                    minlength: 3
                }

				},
            messages: {
			}
        });
    });
</script>


<?php include 'inc/bottom.php'; // Close body and html tags ?>
