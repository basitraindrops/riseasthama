<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['widget_id']))
{
	require_once(CLASS_DIR."widget.class.php");
	$prv=new widget($mysqli);
	$prv->delete($_POST['widget_id']);
}


?>