<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
if(isset($_POST['status'])&&isset($_POST['user_password'])&&isset($_POST['user_group_id']))
{
	if(user_access_each($mysqli,"approve_user_internal",$user_id)==false) 
	{
		header("Location:index.php");
		exit();
	}

	//Encrypt before insert
	$encryptObj = new encryption();
	$encrypted_password=$encryptObj->encrypt_password($_POST['user_password']);
			$user=new user($mysqli);


	require_once(CLASS_DIR."user_internal.class.php");
	require_once(CLASS_DIR."user.class.php");
	$user_internal=new user_internal($mysqli);
//	echo $_POST['status'],$sessionObj->read('user_name'),$_GET['id'];
	$user_internal->update_status($_POST['status'],$user_name,$_GET['id']);

	$user_internal->get_by_ui_id($_GET['id']);
	$user=new user($mysqli);
	$user->update_group($_POST['user_group_id'],$user_name,$user_internal->user_id);
	if($_POST['user_password']!="")
		$user->update_password($encrypted_password,$user_name,$user_internal->user_id);
	$error="Successfully update Approval";
}
if(isset($_GET['id']))
{
		require_once(CLASS_DIR."user_internal.class.php");
		require_once(CLASS_DIR."user.class.php");
		$user_internal_show=new user_internal($mysqli);
		$user_internal_show->get_by_ui_id($_GET['id']);
		if(isset($user_internal_show->ui_id))
		{
			$user=new user($mysqli);
			$user->get_by_user_id($user_internal_show->user_id);
		}
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="user_internal.php">User Internal</a></li>
        <li class="active"><a href="#" >Approval Perusahaan</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; if(isset($_GET['id'])) { echo "?id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php echo "Approval Perusahaan"; ?> <small> <?php  echo "Approval perusahaan"; ?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="user_internal.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>"; 
			/* SEND EMAIL APPROVAL 
			$to      = $user->user_email;
			$subject = 'Status Pendaftaran anda di PLN Jasa Sertifikasi';
			if($user_internal_show->approval_status=="Proses Verifikasi")
			{
				$message = 'Yth. '.$user->user_last_name.','.$user->user_first_name."<br/>";
				$message.= "Permohonan Registrasi User anda nomor ".$user_internal_show->registration_number." Pada tanggal ".date("d-M-Y",strtotime($user_internal_show->registration_date))." sedang dalam proses verifikasi data.<br/><br/>
				Hormat Kami<br/><br/>PT. PLN (Persero) Jasa Sertifikasi<br/><br/>";
			}
			else if($user_internal_show->approval_status=="Disetujui")
			{
				$message = 'Yth. '.$user->user_last_name.','.$user->user_first_name."<br/>";
				$message.= "Permohonan Registrasi User anda nomor ".$user_internal_show->registration_number." Pada tanggal ".date("d-M-Y",strtotime($user_internal_show->registration_date))." telah disetujui,<br/>";
				$message.=" dengan rincian user :<br/>email: ".$user->user_email."<br/>Password : ".$user->user_password."<br/>untuk keamanan Silahkan lakukan perubahan password saat login";
				$message.="<br/><br/>Hormat Kami<br/><br/>PT. PLN (Persero) Jasa Sertifikasi<br/><br/>";
			}
			else
			{
				$message = 'Yth. '.$user->user_last_name.','.$user->user_first_name."<br/>";
				$message.= "Permohonan Registrasi User anda nomor ".$user_internal_show->registration_number." Pada tanggal ".date("d-M-Y",strtotime($user_internal_show->registration_date))." dinyatakan ditolak, silakan melakukan registrasi ulang kembali dengan melengkapi seluruh isian yang dipersyaratkan..<br/><br/>
				Hormat Kami<br/><br/>PT. PLN (Persero) Jasa Sertifikasi<br/><br/>";
			}
			$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
			'Reply-To: '.ADMIN_EMAIL . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
*/
			}?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Internal User Full Name </label>
                <div class="col-md-2">
                    <div class="input-group">
						<?php echo $user->user_first_name." ".$user->user_last_name;  ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Email</label>
                <div class="col-md-2">
                    <div class="input-group">
						<?php echo $user->user_email;  ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Worker Card ID</label>
                <div class="col-md-2">
                    <div class="input-group">
						<?php echo $user_internal_show->ui_nip;  ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Status</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<input type="radio" name="status" value="1" <?php if($user_internal_show->status==1) echo ' checked ';?>>Active<br/>
						<input type="radio" name="status" value="0" <?php if($user_internal_show->status==0) echo ' checked ';?>>Not Active
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">User Group</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <select id="user_group_id" name="user_group_id" class="form-control">
						<?php
						require_once(CLASS_DIR.'user_group.class.php');
						$ug=new user_group($mysqli);
						$data=$ug->user_group_all();
						for($i=0;$i<count($data);$i++) {?>
						<option value="<?php echo $data[$i]->user_group_id;?>" <?php if($user->user_group_id==$data[$i]->user_group_id){ echo " selected='selected' ";} ?>><?php echo $data[$i]->user_group_name;?></option>
						<?php }?>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Password *</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_password" name="user_password" class="form-control" value="<?php generate_password(); ?>"><a href="javascript:;" class="btn btn-default" onClick="generate_password();">Generate Password</a>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="user_internal.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                user_internal_name: {
                    required: true,
                    minlength: 3
                },
                user_internal_code: {
                    required: true,
                    number: true
                }            },
            messages: {
                user_internal_name: {
                    required: 'Please enter a user_internal Name',
                    minlength: 'Your user_internal Name must consist of at least 3 characters'
                },
                user_internal_code: {
                    required: 'Please enter user_internal Code',
                    minlength: 'You must enter number only'
                }            
			}
        });
    });
String.prototype.pick = function(min, max) {
    var n, chars = '';

    if (typeof max === 'undefined') {
        n = min;
    } else {
        n = min + Math.floor(Math.random() * (max - min));
    }

    for (var i = 0; i < n; i++) {
        chars += this.charAt(Math.floor(Math.random() * this.length));
    }

    return chars;
};


// Credit to @Christoph: http://stackoverflow.com/a/962890/464744
String.prototype.shuffle = function() {
    var array = this.split('');
    var tmp, current, top = array.length;

    if (top) while (--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
    }

    return array.join('');
};
function generate_password()
{
	var specials = '!@#$%^&*()_+{}:"<>?\|[];\',./`~';
	var lowercase = 'abcdefghijklmnopqrstuvwxyz';
	var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var numbers = '0123456789';

	var all = specials + lowercase + uppercase + numbers;

	var password = '';
	password += specials.pick(1);
	password += lowercase.pick(1);
	password += uppercase.pick(1);
	password += all.pick(3, 10);
	password = password.shuffle();
	//alert("test");
	$("#user_password").val(password);
}
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
