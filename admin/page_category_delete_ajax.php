<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['page_category_id']))
{
	require_once(CLASS_DIR."page_category.class.php");
	$prv=new page_category($mysqli);
	$prv->delete($_POST['page_category_id']);
}


?>