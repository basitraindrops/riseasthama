<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
$error2="";
//echo $sessionObj->read('email');
if(isset($_POST['new_password']))
{
	require_once(CLASS_DIR."user.class.php");
	$user=new user($mysqli);
	$encryptObj = new encryption();
	if($user->loginby_email_password($user_email,$_POST['old_password']))
	{
		$user=new user($mysqli);
		$user->update_password($encryptObj->encrypt_password($_POST['new_password']),$user_name,$user_id);
		$error="Success updating password!";
	}
	else
	{
		$error2="Error. Old Password is wrong!";
	}
}
require_once(CLASS_DIR."user.class.php");
$user_show=new user($mysqli);
//echo decrypt($sessionObj->read("user_email"),$settings->salt_key).$sessionObj->read("user_name");
$user_show->get_by_email($sessionObj->read("user_email"));
?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="user_profile.php">User Profile</a></li>
        <li class="active"><a href="javascript:;">Change Password</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; if(isset($_GET['id'])) { echo "?id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert user"; } else { echo "Update user"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add new user"; } else { echo "Update existing user"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="user.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
			<?php if($error2!="") { echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div>";  }?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Username </label>
                <div class="col-md-4">
                    <div class="input-group">
						<?php echo $user_name;  ?>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Email </label>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo $user_email;  ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Old Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="old_password" name="old_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">New Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="new_password" name="new_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Confirm Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="user.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                old_password: {
                    required: true,
                    minlength: 8
                },
                new_password: {
                    required: true,
                    minlength: 8
                },
                confirm_password: {
                    required: true,
                    equalTo: '#new_password'
                }            },
            messages: {
                old_password: {
                    required: 'Please enter Old Password',
                    minlength: 'Your Old Password must consist of at least 8 characters'
                },
                new_password: {
                    required: 'Please enter new Password',
                    minlength: 'Your new Password must consist of at least 8 characters'
                },
                confirm_password: {
                    required: 'Please enter field "Confirm Password"',
                    equalTo: 'Confirm Password must be the same value to New Password'
                }            
			}
        });
    });
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
