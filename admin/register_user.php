<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');

require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
$error2="";
//echo $sessionObj->read('email');
if(isset($_POST['user_first_name']))
{
	//require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user.class.php');
	//require_once(CLASS_DIR.'no_urut.class.php');
	//$company=new company($mysqli);
	$user=new user($mysqli);
	//$no_urut_obj=new no_urut($mysqli);
	//$no_urut=$no_urut_obj->get_index_next($status="perusahaan");
	$directory="file/";
	if(!file_exists($directory))
		mkdir($directory);
	$tmp_directory="tmp/";


	//Encrypt before insert
	$encryptObj = new encryption();
	$encrypted_password=$encryptObj->encrypt_password($_POST['user_password']);
	
	$user->get_by_email_gamer($_POST['user_email']);
	if(isset($user->user_id))
	{
		$error2.="Email telah dipakai, mohon gunakan Email lain<br/>";
	}

	if($error2==""){
	$user->insert_gamer($_POST['user_email']
				,$encrypted_password
				,$_POST['user_first_name']
				,$_POST['user_last_name']
				,$user_last_login=date("Y-m-d H:i:s")
				,2
				,$_POST['user_birth_place']
				,date("Y-m-d",strtotime($_POST['user_birth_date']))
				,$_POST['user_address']
				,$_POST['user_phone_number']
				,$_POST['user_photo_file_hidden']
				,$_POST['user_email']
				,$_POST['user_id']
				,$_POST['user_game']
	);
	
	$error="Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftarkan perusahaan anda";

		$to      = $_POST['user_email'];
		$subject = 'Sukses Pendaftaran Anda';
		$message = 'Hi '.$_POST['user_last_name'].",".$_POST['user_first_name']."<br/>";
		$message.= "Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftar<br/><br/>";
		$message.= "Regards<br/><br/>";
		$message.= "Web Admin";
		$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
		'Reply-To: '.ADMIN_EMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

	}
}
//echo $error;
//echo $error2;
?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="page_login.php"><i class="fa fa-home"></i></a></li>
        <li><a href="register_user.php">Pendaftaran User</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-box remove-margin">
	<input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
        <!-- Form Header -->
        <h4 class="form-box-header">Pendaftaran User <small>Pendaftaran User</small></h4>
		<p align="left"><a href="index.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            
			<?php 
			if($error!="") { echo '<div class="form-group"><div class="alert alert-success">'.$error."</div></div>";  }else{
			if($error2!="") { echo '<div class="form-group"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div></div>";  }
			?>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_complete_name">Nama Awal *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_first_name" name="user_first_name" class="form-control" value="<?php if(isset($_POST['user_first_name'])){ echo $_POST['user_first_name']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_complete_name">Nama Akhir (Nama Keluarga) *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_last_name" name="user_last_name" class="form-control" value="<?php if(isset($_POST['user_first_name'])){ echo $_POST['user_first_name']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_email">User Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_email" name="user_email" class="form-control" value="<?php if(isset($_POST['user_email'])){ echo $_POST['user_email']; } ?>"><span id="checklist_user_email"></span>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_place">Tempat Lahir *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_birth_place" name="user_birth_place" class="form-control" value="<?php if(isset($_POST['user_birth_place'])){ echo $_POST['user_birth_place']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_date">Tanggal Lahir *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_birth_date" name="user_birth_date" class="form-control input-datepicker" value="<?php if(isset($_POST['user_birth_date'])){ echo $_POST['user_birth_date']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_address">Alamat *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<textarea class="form-control" name="user_address" id="user_address" rows="10" cols="200" style="height:200px;"><?php if(isset($_POST['user_address'])){ echo $_POST['user_address']; } ?>
						</textarea>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Nomor Telpon / Mobile *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_phone_number" name="user_phone_number" class="form-control" value="<?php if(isset($_POST['user_phone_number'])){ echo $_POST['user_phone_number']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">File Photo *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="user_photo_file_div">
						<input type="file" class="form-control" name="user_photo_file" id="user_photo_file" style="width:300px;" > 100 KB File JPEG
						</div>
					</div>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file"></label>
                <div class="col-md-4" id="message_upload_user_photo_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
           <div class="form-group">
                <label class="control-label col-md-2" for="user_id">User ID *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<input type="text" class="form-control" name="user_id" id="user_id" value="<?php if(isset($_POST['user_id'])){ echo $_POST['user_id']; } ?>">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_game">Game *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<input type="text" class="form-control" name="user_game" id="user_game" value="">
					</div>
                </div>
            </div> 
           <div class="form-group">
                <label class="control-label col-md-2" for="user_password">User Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="user_password" name="user_password" class="form-control" value="">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_confirm_password">Confirm Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="user_confirm_password" name="user_confirm_password" class="form-control" value="">
					</div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="index.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                user_first_name: {
                    required: true,
                    minlength: 3
                },
				user_last_name: {
                    required: true,
                    minlength: 3
                },
				user_birth_place: 
				{
					required: true,
					minlength: 3
				},
				user_birth_date: 
				{
					required: true
				},
				user_address: 
				{
					required: true,
					minlength: 3
				},
				user_phone_number: 
				{
					required: true,
					minlength: 3
				},
				user_email: 
				{
					required: true,
					email:true
				},
				user_id: 
				{
					required: true
				},
				user_game: 
				{
					required: true
				},
				user_password: 
				{
					required: true,
					minlength: 8
				},
				user_confirm_password: {
                    required: true,
                    equalTo: "#user_password"
                }
				},
            messages: {
               user_first_name: {
                    required: "Mohon isi Nama Awal",
                    minlength: "Nama Awal minimum 3 karakter"
                },
				user_last_name: {
                    required: "Mohon isi Nama Akhir",
                    minlength: "Nama Akhir minimum 3 karakter"
                },
				user_birth_place: 
				{
					required: "Mohon isi tempat lahir",
					minlength: "Tempat lahir minimum 3 karakter"
				},
				user_birth_date: 
				{
					required: "Mohon isi tanggal lahir"
				},
				user_address: 
				{
					required: "Mohon isi alamat anda",
					minlength: "Alamat anda minimum 3 karakter"
				},
				user_ktp_number: 
				{
					required: "Mohon isi nomor KTP"
				},
				user_phone_number: 
				{
					required: "Mohon isi nomor telepon",
					number:"Nomor telepon diisi dengan angka"
				},
				user_ktp_file: 
				{
					required: "Mohon pilih File KTP/SIM"
				},
				user_email: 
				{
					required: "Mohon isi email anda",
					email:"Email diisi dengan format email yang valid"
				},
				user_id: 
				{
					required: "Mohon isi User ID anda",
				},
				user_game: {
                    required: "Mohon Isi game",
                },
				user_password: 
				{
					required: "Mohon isi password",
					minlength: 'Password minimum panjangnya 8'
				},
				user_confirm_password: {
                    required: "Mohon konfirmasi password",
                    equalTo: "Password dan konfirmasi password tidak sama"
                }
			}
        });
    });
$(document).ready(function() {
	$("#user_photo_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File Photo anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format File Photo anda bukan JPEG/JPG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_photo_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_photo_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_photo_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_photo_file_div").html("<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	
	$("#user_email").change( function() {
		$.ajax
		({
			url:"check_user_email_gamer_ajax.php"
			,data:"user_email=" + $("#user_email").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_email").html(data2);
		  });

	});
    $("#user_phone_number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>

<?php
			}
	include 'inc/bottom.php'; // Close body and html tags ?>
