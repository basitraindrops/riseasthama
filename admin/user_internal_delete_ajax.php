<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');



if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['ui_id']))
{
	require_once(CLASS_DIR."user_internal.class.php");
	require_once(CLASS_DIR."user.class.php");
	$user_internal=new user_internal($mysqli);
	$user_internal->get_by_ui_id($_POST['ui_id']);
	if($user_internal->user_id<>null)
	{
		$user=new user($mysqli);
		$user->delete($user_internal->user_id);
	}
	$user_internal->delete($_POST['ui_id']);
}


?>