<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

require_once(CLASS_DIR.'company.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
?>

<?php
$error="";
//echo $sessionObj->read('email');
if(isset($_POST['new_password'])&&isset($_GET['id']))
{
	if(user_access_each($mysqli,"change_password_perusahaan",$sessionObj)) 
	{ 
		require_once(CLASS_DIR."company.class.php");
		$company=new company($mysqli);
		$encryptObj = new encryption();
		$company->update_password($encryptObj->encrypt_password($_POST['new_password']),$sessionObj->read('user_name'),$_GET['id']);
		$error="Success updating password!";
	}
}
if(isset($_GET['id']))
{
	require_once(CLASS_DIR."company.class.php");
	$company_show=new company($mysqli);
	$company_show->get_by_company_id($_GET['id']);
}
//die();
?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="perusahaan.php">Perusahaan</a></li>
        <li class="active"><a href="javascript:;">Ubah Password Pengguna</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; if(isset($_GET['id'])) { echo "?id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header">Ubah Password<small>Ubah Password</small></h4>
		<p align="left" style="margin-left:20px;"><a href="perusahaan.php" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;&nbsp;Kembali</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Nama Pengguna *</label>
                <div class="col-md-4">
                    <div class="input-group">
						<?php if(isset($_GET['id'])){ echo $company_show->user_complete_name; } ?>
                    </div>
                </div>
            </div>
           <div class="form-group">
                <label class="control-label col-md-2" for="val_companyname">Email Pengguna*</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php if(isset($_GET['id'])){ echo $company_show->user_email; } ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">New Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="new_password" name="new_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Confirm Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="perusahaan.php" class="btn btn-default"><i class="fa fa-reply"></i>&nbsp;&nbsp;Kembali</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                new_password: {
                    required: true,
                    minlength: 8
                },
                confirm_password: {
                    required: true,
                    equalTo: '#new_password'
                }            },
            messages: {
                new_password: {
                    required: 'Please enter new Password',
                    minlength: 'Your new Password must consist of at least 8 characters'
                },
                confirm_password: {
                    required: 'Please enter field "Confirm Password"',
                    equalTo: 'Confirm Password must be the same value to New Password'
                }            
			}
        });
    });
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
