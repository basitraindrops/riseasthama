<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['post_category_id']))
{
	require_once(CLASS_DIR."post_category.class.php");
	$prv=new post_category($mysqli);
	$prv->delete($_POST['post_category_id']);
}


?>