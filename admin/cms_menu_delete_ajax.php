<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['cms_menu_id']))
{
	require_once(CLASS_DIR."cms_menu.class.php");
	$prv=new cms_menu($mysqli);
	$prv->delete($_POST['cms_menu_id']);
}
?>