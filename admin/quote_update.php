<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
header("Location:page_login.php");
}
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
require_once(CLASS_DIR."user_internal.class.php");
require_once(CLASS_DIR."user.class.php");


$error="";
$error2="";

require_once CLASS_DIR.'front_quote.class.php';
$up=new front_quote($mysqli);

if(isset($_GET['quote_id']) && $_GET['quote_id']){    

    if(!$up->getById($_GET['quote_id'])){
        echo 'no record found';
        exit;
    }
}

if($_POST){
    $settings=new settings($mysqli);
    $settings->get_by_id(1);    
    $user_email= decrypt($sessionObj->read("user_email"),$settings->salt_key);
    

    if(isset($_POST['quote_id']) && $_POST['quote_id']>0 ){
        $up->update($_POST['quote'],$_POST['status'],$_POST['must_show'],$_POST['quote_id']);               
    }else{
        $insert = $up->insert($_POST['quote'],$_POST['status'],$_POST['must_show']);       
        $user_id=$up->conn->insert_id;        
    }
    
    $error="Success updating datail!";      
    header("Location:quote_manage.php");
    
}
?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<?php


?>
<!-- Page Content -->
<div id="page-content">

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" class="form-horizontal form-box remove-margin"  enctype="multipart/form-data" >

       <input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
       <input type="hidden" id="quote_id" name="quote_id" value="<?php echo $up->quote_id ?>">

        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Front Client"; } else { echo "Update Front Client"; }  } /* ?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add New Front Client"; } else { echo "Update Front Client"; }  }?></small> */?></h4>
        <p align="left" style="margin-left:20px;">
            <a href="quote_manage.php" class="btn btn-default"><i class="fa fa-reply"></i> Back </a>
        </p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
            <?php if($error2!="") { echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div>";  }?>
            <?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="quote">Quote *</label>
                <div class="col-md-4">
                    <div class="input-group">                
                        <input type="text" id="quote" name="quote" class="form-control" value="<?php echo $up->quote ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="status">Status</label>
                <div class="col-md-4">
                    <div class="input-group"> 
                        <div class="radio_vut">                       
                            <input type="radio" id="status1" name="status" class="form-control" value="1" <?php echo $up->status == 0?"":"checked" ?> >
                            <span>Active</span>
                        </div>
                        <div class="radio_vut">                       
                            <input type="radio" id="status2" name="status" class="form-control" value="0" <?php echo $up->status == 0 ?"checked":"" ?>>
                            <span>Deactive</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="must_show">show this</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="radio_vut">                       
                            <input type="radio" id="must_show1" name="must_show" class="form-control" value="1" <?php echo $up->must_show == 0?"":"checked" ?>>
                            <span>Yes</span>
                        </div>
                        <div class="radio_vut">                           
                            <input type="radio" id="must_show2" name="must_show" class="form-control" value="0" <?php echo $up->must_show == 0 ?"checked":"" ?>>
                            <span>No</span>                       
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>

        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                user_name: {
                    required: true
                },user_email: {
                    required: true,
                    email: true
                }
                    
                },
            messages: {
                user_name: {
                    required: "Please input Username"
                },user_email: {
                    required: "Please input Email",
                    email: "Email Format is wrong, eg:indigomike7@gmail.com"
                }   
            }
        });
    });
$(document).ready(function() {

    $("#user_photo_file").change(
        function()
    {
            var file = this.files[0];
            name = file.name;
            size = file.size;
            type = file.type;

            if(file.name.length < 1) {
            }
            else if(file.size > 100000) {
                alert("Photo File Size is too Big");
            }
            else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
                alert("Format Photo File is not JPG/JPEG");
            }
            else 
            {   
                var randomnumber = Math.floor(Math.random() * (100000 - 100 + 1)) + 100;
                name = randomnumber + '_' + file.name  ;        
                //alert("test");
                var fd = new FormData();
                fd.append('image_file', $('#user_photo_file')[0].files[0]);
                fd.append('session_id',$("#session_id").val());
                fd.append('image_name',name);
                $.ajax({
                url: "image_file_upload_ajax_fornt_client.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: fd , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                enctype: 'multipart/form-data',
                processData: false,  // tell jQuery not to process the data
                cache: false,        // To send DOMDocument or non processed data file it is set to false
                  beforeSend: function( data ) {
                    $("#message_upload_user_photo_file").html('Uploading file...');

                  },
                success: function(data)   // A function to be called if request succeeds
                    {
                        $("#message_upload_user_photo_file").html(data);
                        $("#message_upload_user_photo_file").fadeOut(8000);
                        if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
                            $("#user_photo_file_div").html("<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='" +  name +"'><i class='fi fi-jpg'></i>" + name);
                    }
                });
            }
        }
    );
    $("#user_ktp_file").change(
        function()
        {
                var file = this.files[0];
                name = file.name;
                size = file.size;
                type = file.type;

                if(file.name.length < 1) {
                }
                else if(file.size > 100000) {
                    alert("Photo File Size is too Big");
                }
                else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
                    alert("Format Photo File is not JPG/JPEG");
                }
                else 
                {           
//                  alert("test");
                    var fd = new FormData();
                    fd.append('image_file', $('#user_ktp_file')[0].files[0]);
                    fd.append('session_id',$("#session_id").val());
                    $.ajax({
                    url: "image_file_upload_ajax_fornt_client.php", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    enctype: 'multipart/form-data',
                    processData: false,  // tell jQuery not to process the data
                    cache: false,        // To send DOMDocument or non processed data file it is set to false
                      beforeSend: function( data ) {
                        $("#message_upload_user_ktp_file").html('Uploading file...');

                      },
                    success: function(data)   // A function to be called if request succeeds
                        {
                            $("#message_upload_user_ktp_file").html(data);
                            $("#message_upload_user_ktp_file").fadeOut(8000);
                            if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
                                $("#user_ktp_file_div").html("<input type='hidden' name='user_ktp_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
                        }
                    });
                }
        }
    );

    $("#user_name").change( function() {
        $.ajax
        ({
            url:"check_user_name_ajax.php"
            ,data:"user_name=" + $("#user_name").val()
            ,method:"POST"
        }).done
        (
            function( data2 ) {
            $("#checklist_user_name").html(data2);
          });

    });
    $("#user_email").change( function() {
        $.ajax
        ({
            url:"check_user_email_ajax.php"
            ,data:"user_email=" + $("#user_email").val()
            ,method:"POST"
        }).done
        (
            function( data2 ) {
            $("#checklist_user_email").html(data2);
          });

    });


}); 
    function datenow() 
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        return today;     
    }

</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
