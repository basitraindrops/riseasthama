<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require_once(INC_DIR.'init.php');

require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
$error2="";
//echo $sessionObj->read('email');
if(isset($_POST['company_name']))
{
	require_once(CLASS_DIR.'company.class.php');
	require_once(CLASS_DIR.'user.class.php');
	require_once(CLASS_DIR.'no_urut.class.php');
	$company=new company($mysqli);
	$user=new user($mysqli);
	$no_urut_obj=new no_urut($mysqli);
	$no_urut=$no_urut_obj->get_index_next($status="perusahaan");
	$directory="file/".$no_urut;
	if(!file_exists($directory))
		mkdir($directory);
	$tmp_directory="tmp/";



	//NPWP FILE
	$moved_file = $directory."/".$_POST['company_npwp_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['company_npwp_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//SIUP FILE
	$moved_file = $directory."/".$_POST['company_siup_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['company_siup_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//DOMICILION FILE
	$moved_file = $directory."/".$_POST['company_domicilion_letter_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['company_domicilion_letter_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//SITU FILE
	$moved_file = $directory."/".$_POST['company_situ_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['company_situ_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//TDP FILE
	$moved_file = $directory."/".$_POST['company_tdp_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['company_tdp_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//KTP FILE
	$moved_file = $directory."/".$_POST['user_ktp_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['user_ktp_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);
	//PHOTO FILE
	$moved_file = $directory."/".$_POST['user_photo_file_hidden'];
	$uploaded_file=$tmp_directory.$_POST['user_photo_file_hidden'];
	if(file_exists($uploaded_file))
		rename($uploaded_file,$moved_file);


	//Encrypt before insert
	$encryptObj = new encryption();
	$encrypted_password=$encryptObj->encrypt_password($_POST['user_password']);
	
	$user->get_by_email($_POST['user_email']);
	if(isset($user->user_id))
	{
		$error2.="Email telah dipakai, mohon gunakan Email lain<br/>";
	}


	if($error2==""){
	$user->insert(''
				,$_POST['user_email']
				,$encrypted_password
				,$_POST['user_first_name']
				,$_POST['user_last_name']
				,$user_last_login=date("Y-m-d H:i:s")
				,2
				,$_POST['user_birth_place']
				,date("Y-m-d",strtotime($_POST['user_birth_date']))
				,$_POST['user_address']
				,$_POST['user_ktp_number']
				,$_POST['user_phone_number']
				,$_POST['user_ktp_file_hidden']
				,$_POST['user_photo_file_hidden']
				,$user_pendidikan_terakhir=''
				,$user_gender=''
				,$user_marital_status=''
				,$_POST['user_email']
	);
	$company->insert(
		$no_urut
		,date("Y-m-d H:i:s")
		,$_POST['company_name']
		,$_POST['company_leader']
		,$_POST['company_npwp']
		,$_POST['company_npwp_file_hidden']
		,$_POST['company_address']
		,$_POST['company_country']
		,$_POST['company_province']
		,$_POST['company_kabupaten']
		,$_POST['company_zip_code']
		,$_POST['company_phone']
		,$_POST['company_fax']
		,$_POST['company_email']
		,$_POST['company_siup_file_hidden']
		,$_POST['company_domicilion_letter_file_hidden']
		,$_POST['company_situ_file_hidden']
		,$_POST['company_tdp_file_hidden']
		,$status=0
		,$mysqli->insert_id
		,$_POST['user_email']
		);
	$error="Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftarkan perusahaan anda";

		$to      = $_POST['user_email'];
		$subject = 'Sukses Pendaftaran Perusahaan di Aplikasi PLN Jasa Sertifikasi';
		$message = 'Hi '.$_POST['user_last_name'].",".$_POST['user_first_name']."<br/>";
		$message.= "Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftarkan perusahaan anda<br/><br/>";
		$message.= "Regards<br/><br/>";
		$message.= "Web Admin";
		$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
		'Reply-To: '.ADMIN_EMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

	}
}
//echo $error;
//echo $error2;
?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="page_login.php"><i class="fa fa-home"></i></a></li>
        <li><a href="register_perusahaan.php">Pendaftaran Perusahaan</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-box remove-margin">
	<input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
        <!-- Form Header -->
        <h4 class="form-box-header">Pendaftaran Perusahaan <small>Pendaftaran Perusahaan</small></h4>
		<p align="left"><a href="index.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            
			<?php 
			if($error!="") { echo '<div class="form-group"><div class="alert alert-success">'.$error."</div></div>";  }else{
			if($error2!="") { echo '<div class="form-group"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div></div>";  }
			?>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_name">Nama Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_name" name="company_name" class="form-control" value="<?php if(isset($_POST['company_name'])){ echo $_POST['company_name']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_leader">Nama Pemimpin Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_leader" name="company_leader" class="form-control" value="<?php if(isset($_POST['company_leader'])){ echo $_POST['company_leader']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_npwp">Nomor NPWP Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_npwp" name="company_npwp" class="form-control" value="<?php if(isset($_POST['company_npwp'])){ echo $_POST['company_npwp']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_npwp_file">File NPWP Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="company_npwp_file_div">
						<input type="file" class="form-control" name="company_npwp_file" id="company_npwp_file" style="width:300px;" > 100KB File JPEG
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_npwp_file"></label>
                <div class="col-md-4" id="message_upload_company_npwp_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_address">Alamat Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<textarea class="form-control" name="company_address" id="company_address" rows="10" cols="200" style="height:200px;"><?php if(isset($_POST['company_address'])){ echo $_POST['company_address']; } ?>
						</textarea>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_country">Negara *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<input type="text" class="form-control" name="company_country" id="company_country" value="<?php if(isset($_POST['company_country'])){ echo $_POST['company_country']; } ?>">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_province">Provinsi *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <select id="company_province" name="company_province" class="form-control">
						<?php
						require_once(CLASS_DIR."province.class.php");
						$prov=new province($mysqli);
						$prov_data=$prov->province_all();
						for($i=0;$i<count($prov_data);$i++)
						{
							?>
							<option value="<?php echo $prov_data[$i]->province_code; ?>" <?php if(isset($_GET['action'])&&isset($_GET['id'])){  if($kab_show->province_code==$prov_data[$i]->province_code) { echo " selected "; }  } ?>><?php echo $prov_data[$i]->province_name; ?></option>
							<?php
						}
						?>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_kabupaten">Kabupaten *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="kabupaten">
                        <select id="company_kabupaten" name="company_kabupaten" class="form-control">
						</select>
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_zip_code">Kode Pos *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_zip_code" name="company_zip_code" class="form-control" value="<?php if(isset($_POST['company_zip_code'])){ echo $_POST['company_zip_code']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_phone">Nomor Telpon Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_phone" name="company_phone" class="form-control" value="<?php if(isset($_POST['company_phone'])){ echo $_POST['company_phone']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_fax">Nomor Fax Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_fax" name="company_fax" class="form-control" value="<?php if(isset($_POST['company_fax'])){ echo $_POST['company_fax']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_email">Email Perusahaan *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="company_email" name="company_email" class="form-control" value="<?php if(isset($_POST['company_email'])){ echo $_POST['company_email']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_siup_file">File Surat Izin Usaha *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="company_siup_file_div">
							<input type="file" class="form-control" name="company_siup_file" id="company_siup_file" style="width:300px;" > 200 KB File PDF
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_siup_file"></label>
                <div class="col-md-4" id="message_upload_company_siup_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_domicilion_letter_file">File Surat Izin Domisili *</label>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="company_domicilion_letter_file_div">
						<input type="file" class="form-control" name="company_domicilion_letter_file" id="company_domicilion_letter_file" style="width:300px;"> 200 KB File PDF
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_domicilion_letter_file"></label>
                <div class="col-md-4" id="message_upload_company_domicilion_letter_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_situ_file">File Surat Izin Tempat Usaha  *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="company_situ_file_div">
						<input type="file" class="form-control" name="company_situ_file" id="company_situ_file" style="width:300px;" > 200 KB File PDF
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_situ_file"></label>
                <div class="col-md-4" id="message_upload_company_situ_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_tdp_file">File Tanda Daftar *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="company_tdp_file_div">
						<input type="file" class="form-control" name="company_tdp_file" id="company_tdp_file" style="width:300px;" > 200 KB File PDF
						</div>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="company_tdp_file"></label>
                <div class="col-md-4" id="message_upload_company_tdp_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_complete_name">Nama Awal Pengguna *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_first_name" name="user_first_name" class="form-control" value="<?php if(isset($_POST['user_first_name'])){ echo $_POST['user_first_name']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_complete_name">Nama Akhir (Nama Keluarga) Pengguna *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_last_name" name="user_last_name" class="form-control" value="<?php if(isset($_POST['user_first_name'])){ echo $_POST['user_first_name']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_place">Tempat Lahir *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_birth_place" name="user_birth_place" class="form-control" value="<?php if(isset($_POST['user_birth_place'])){ echo $_POST['user_birth_place']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_birth_date">Tanggal Lahir *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_birth_date" name="user_birth_date" class="form-control input-datepicker" value="<?php if(isset($_POST['user_birth_date'])){ echo $_POST['user_birth_date']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_address">Alamat *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<textarea class="form-control" name="user_address" id="user_address" rows="10" cols="200" style="height:200px;"><?php if(isset($_POST['user_address'])){ echo $_POST['user_address']; } ?>
						</textarea>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_ktp_number">Nomor KTP *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_ktp_number" name="user_ktp_number" class="form-control" value="<?php if(isset($_POST['user_ktp_number'])){ echo $_POST['user_ktp_number']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_phone_number">Nomor Telpon / Mobile *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_phone_number" name="user_phone_number" class="form-control" value="<?php if(isset($_POST['user_phone_number'])){ echo $_POST['user_phone_number']; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_ktp_file">File KTP/SIM *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="user_ktp_file_div">
						<input type="file" class="form-control" name="user_ktp_file" id="user_ktp_file" style="width:300px;" > 100 KB File JPEG 
						</div>
					</div>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_ktp_file"></label>
                <div class="col-md-4" id="message_upload_user_ktp_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file">File Photo *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
						<div id="user_photo_file_div">
						<input type="file" class="form-control" name="user_photo_file" id="user_photo_file" style="width:300px;" > 100 KB File JPEG
						</div>
					</div>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-md-2" for="user_photo_file"></label>
                <div class="col-md-4" id="message_upload_user_photo_file">
                    <div class="input-group">
					</div>
                </div>
            </div>
           <div class="form-group">
                <label class="control-label col-md-2" for="user_email">User Email *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="user_email" name="user_email" class="form-control" value="<?php if(isset($_POST['user_email'])){ echo $_POST['user_email']; } ?>"><span id="checklist_user_email"></span>
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_password">User Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="user_password" name="user_password" class="form-control" value="">
					</div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="user_confirm_password">Confirm Password *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="password" id="user_confirm_password" name="user_confirm_password" class="form-control" value="">
					</div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="index.php" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                company_name: {
                    required: true,
                    minlength: 3
                },
				company_leader: {
                    required: true,
                    minlength: 3
                },
				company_npwp: {
                    required: true
                },
				company_address: {
                    required: true,
                    minlength: 3
                },
				company_country: {
                    required: true,
                    minlength: 3
                },
				company_province: {
                    required: true
                },
				company_kabupaten: {
                    required: true
                },
				company_zip_code: {
                    required: true,
                    number: true
                },
				company_phone: {
                    required: true,
                    number:true
                },
				company_fax: {
                    required: true,
                    number:true
                },
				company_email: {
                    required: true,
                    email: true
                },
				user_first_name: {
                    required: true,
                    minlength: 3
                },
				user_last_name: {
                    required: true,
                    minlength: 3
                },
				user_birth_place: 
				{
					required: true,
					minlength: 3
				},
				user_birth_date: 
				{
					required: true
				},
				user_address: 
				{
					required: true,
					minlength: 3
				},
				user_ktp_number: 
				{
					required: true
				},
				user_phone_number: 
				{
					required: true,
					minlength: 3
				},
				user_ktp_file: 
				{
					required: true
				},
				user_email: 
				{
					required: true,
					email:true
				},
				user_password: 
				{
					required: true,
					minlength: 8
				},
				user_confirm_password: {
                    required: true,
                    equalTo: "#user_password"
                }
				},
            messages: {
                company_name: {
                    required: "Mohon isi nama perusahaan",
                    minlength: "Nama perusahaan minimum 3 karakter"
                },
				company_leader: {
                    required: "Mohon isi nama pemimpin perusahaan",
                    minlength: "Nama pemimpin perusahaan minimum 3 karakter"
                },
				company_npwp: {
                    required: "Mohon isi nomor NPWP perusahaan"
                },
				company_address: {
                    required: "Mohon isi alamat perusahaan",
                    minlength: "Alamat perusahaan minimum 3 karakter"
                },
				company_country: {
                    required: "Mohon isi negara",
                    minlength: "Negara minimum 3 karakter"
                },
				company_province: {
                    required: "Mohon pilih provinsi"
                },
				company_kabupaten: {
                    required: "Mohon pilih kabupaten"
                },
				company_zip_code: {
                    required: "Mohon isi kode pos",
                    number: "Kode pos diisi dengan angka"
                },
				company_phone: {
                    required: "Mohon isi nomor telepon perusahaan",
                    number:"Nomor telepon diisi dengan angka"
                },
				company_fax: {
                    required: "Mohon isi nomor fax perusahaan",
                    number:"Nomor fax diisi dengan angka"
                },
				company_email: {
                    required: "Mohon isi email perusahaan",
                    email: "Email perusahaan diisi dengan format email yang valid"
                },
				user_first_name: {
                    required: "Mohon isi Nama Awal",
                    minlength: "Nama Awal minimum 3 karakter"
                },
				user_last_name: {
                    required: "Mohon isi Nama Akhir",
                    minlength: "Nama Akhir minimum 3 karakter"
                },
				user_birth_place: 
				{
					required: "Mohon isi tempat lahir",
					minlength: "Tempat lahir minimum 3 karakter"
				},
				user_birth_date: 
				{
					required: "Mohon isi tanggal lahir"
				},
				user_address: 
				{
					required: "Mohon isi alamat perusahaan",
					minlength: "Alamat perusahaan minimum 3 karakter"
				},
				user_ktp_number: 
				{
					required: "Mohon isi nomor KTP"
				},
				user_phone_number: 
				{
					required: "Mohon isi nomor telepon",
					number:"Nomor telepon diisi dengan angka"
				},
				user_ktp_file: 
				{
					required: "Mohon pilih File KTP/SIM"
				},
				user_email: 
				{
					required: "Mohon isi email anda",
					email:"Email diisi dengan format email yang valid"
				},
				user_password: 
				{
					required: "Mohon isi password",
					minlength: 'Password minimum panjangnya 8'
				},
				user_confirm_password: {
                    required: "Mohon konfirmasi password",
                    equalTo: "Password dan konfirmasi password tidak sama"
                }
			}
        });
    });
$(document).ready(function() {
	$("#company_npwp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File NPWP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format File NPWP anda bukan JPEG/JPG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_npwp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_npwp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_npwp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_npwp_file_div").html("<input type='hidden' name='company_npwp_file_hidden' id='company_npwp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#user_photo_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File Photo anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format File Photo anda bukan JPEG/JPG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_photo_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_photo_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_photo_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_photo_file_div").html("<input type='hidden' name='user_photo_file_hidden' id='user_photo_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#user_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KTP/SIM anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg' ) {
					alert("Format File KTP/SIM anda bukan JPEG/JPG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#user_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_user_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_user_ktp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#user_ktp_file_div").html("<input type='hidden' name='user_ktp_file_hidden' id='user_ktp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_siup_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File SIUP anda terlalu besar");
				}
				else if(file.type != 'application/pdf' ) {
					alert("Format File SIUP anda bukan PDF");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('pdf_file', $('#company_siup_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "pdf_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_siup_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_siup_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_siup_file_div").html("<input type='hidden' name='company_siup_file_hidden' id='company_siup_file_hidden' value='" +  file.name +"'><i class='fi fi-pdf'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_domicilion_letter_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File Domisili anda terlalu besar");
				}
				else if(file.type != 'application/pdf' ) {
					alert("Format File Domisili anda bukan PDF");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('pdf_file', $('#company_domicilion_letter_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "pdf_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_domicilion_letter_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_domicilion_letter_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_domicilion_letter_file_div").html("<input type='hidden' name='company_domicilion_letter_file_hidden' id='company_domicilion_letter_file_hidden' value='" +  file.name +"'><i class='fi fi-pdf'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_tdp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File TDP anda terlalu besar");
				}
				else if(file.type != 'application/pdf' ) {
					alert("Format File TDP anda bukan PDF");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('pdf_file', $('#company_tdp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "pdf_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_tdp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_tdp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_tdp_file_div").html("<input type='hidden' name='company_tdp_file_hidden' id='company_tdp_file_hidden' value='" +  file.name +"'><i class='fi fi-pdf'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_situ_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File Surat Izin Tempat Usaha anda terlalu besar");
				}
				else if(file.type != 'application/pdf' ) {
					alert("Format File Surat Izin Tempat Usaha anda bukan PDF");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('pdf_file', $('#company_situ_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "pdf_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_situ_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_situ_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_situ_file_div").html("<input type='hidden' name='company_situ_file_hidden' id='company_situ_file_hidden' value='" +  file.name +"'><i class='fi fi-pdf'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_province").change( function() {
		$.ajax
		({
			url:"company_kabupaten_ajax.php"
			,data:"province_code=" + $("#company_province").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#kabupaten").html(data2);
		  });

	});
	$("#user_name").change( function() {
		$.ajax
		({
			url:"check_user_name_ajax.php"
			,data:"user_name=" + $("#user_name").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_name").html(data2);
		  });

	});
	$("#user_email").change( function() {
		$.ajax
		({
			url:"check_user_email_ajax.php"
			,data:"user_email=" + $("#user_email").val()
			,method:"POST"
		}).done
		(
			function( data2 ) {
			$("#checklist_user_email").html(data2);
		  });

	});
    $("#company_zip_code").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#company_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#company_fax").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#user_phone_number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>

<?php
			}
	include 'inc/bottom.php'; // Close body and html tags ?>
