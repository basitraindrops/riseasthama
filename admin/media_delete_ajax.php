<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
$directory="cms_media/";
$error2="";
$session_id=session_id();

if(isset($_POST['file_name'])&&isset($_POST['session_id']))
{
	if($_POST['session_id']==$session_id)
	{
		//NPWP FILE
			$media = "cms_media/".$_POST['file_name'];
			if(file_exists($media))
			{
				unlink($media);
				$error2.="<font color='green'><b>File is removed!</b></font>";
			}
			else
			{
				$error2.="<font color='red'><b>No File name defined in CMS_MEDIA folder!</b></font>";
			}
	}
	else
	{
		$error2.="<font color='red'><b>No Session ID, File is not removed!</b></font>";
	}
}
else
{
	$error2.="<font color='red'><b>File is not removed!</b></font>";
}
echo $error2;

?>