<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['access_id'])&&isset($_POST['user_group_id'])&&isset($_POST['action']))
{
	if($_POST["action"]=="remove")
	{
		require_once(CLASS_DIR."user_group_access.class.php");
		$ua=new user_group_access($mysqli);
		$ua->delete($_POST['access_id'],$_POST['user_group_id']);
	}
	else if($_POST["action"]=="add")
	{
		require_once(CLASS_DIR."user_group_access.class.php");
		$ua=new user_group_access($mysqli);
		$ua->get1($_POST['access_id'],$_POST['user_group_id']);
//		echo "after get".print_r($ua);
		if($ua->user_group_id<>null)
		{
			$ua->delete($_POST['access_id'],$_POST['user_group_id']);
		}
		else
		{
//			echo "insert";
			$ua->insert($_POST['access_id'],$_POST['user_group_id'],$sessionObj->read('user_name'));
		}
	}
}
?>