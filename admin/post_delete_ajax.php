<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:post_login.php");
}

if(isset($_POST['post_id']))
{
	require_once(CLASS_DIR."post.class.php");
	$prv=new post($mysqli);
	$prv->delete($_POST['post_id']);
}


?>