<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');

if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}


?>
<?php include INC_DIR.'top.php'; ?>
<?php include INC_DIR.'nav.php'; ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a href="">Dashboard</a></li>
    </ul>

</div>
<!-- END Page Content -->

<?php include INC_DIR.'footer.php'; // Footer and scripts ?>


<?php include INC_DIR.'bottom.php'; // Close body and html tags ?>