<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}



require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php
$error="";
if(isset($_POST['menu_name'])&&isset($_GET['action']))
{
	if($_GET['action']=="add_new")
	{
		if(user_access_each($mysqli,"create_menu",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."menu.class.php");
		$m=new menu($mysqli);
		$m->insert($_POST['menu_name'],$_POST['menu_page'],$_POST['parent_id'],$_POST['icon'],$user_name);
		header("Location:menu.php");
	}
	else if($_GET['action']=="update")
	{
		if(user_access_each($mysqli,"update_menu",$user_id)==false) 
		{
			header("Location:index.php");
			exit();
		}
		require_once(CLASS_DIR."menu.class.php");
		$m=new menu($mysqli);
		$m->update($_POST['menu_name'],$_POST['menu_page'],$_POST['parent_id'],$_POST['icon'],$user_name,$_GET['id']);
		$error="Success updating data!";
	}
}
if(isset($_GET['action'])&&isset($_GET['id']))
{
	if($_GET['action']=="update")
	{
		require_once(CLASS_DIR."menu.class.php");
		$m_show=new menu($mysqli);
		$m_show->get_by_menu_id($_GET['id']);
	}
}

?>
<?php include 'inc/top.php'; // Meta data and header ?>
<?php include 'inc/nav.php'; // Navigation content ?>
<?php include(CONTROLLER_DIR."phone_book_update.php");?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="menu.php">Menu</a></li>
        <li class="active"><a href="javascript:;"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Menu"; } else { echo "Update Menu"; }  }?></a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]."?action=".$_GET['action']; if(isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>" method="post" class="form-horizontal form-box remove-margin">
        <!-- Form Header -->
        <h4 class="form-box-header"><?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Insert Menu"; } else { echo "Update Menu"; }  }?> <small> <?php if(isset($_GET['action'])) { if($_GET['action']=="add_new") { echo "Add new Menu"; } else { echo "Update existing Menu"; }  }?></small></h4>
		<p align="left" style="margin-left:20px;"><a href="menu.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            <div class="form-group">
			<?php if($error!="") { echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error."</div>";  }?>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_username">Menu Name *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-asterisk fa-fw"></i></span>
                        <input type="text" id="menu_name" name="menu_name" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $m_show->menu_name; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Menu Page</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="menu_page" name="menu_page" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $m_show->menu_page; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Parent *</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <select id="parent_id" name="parent_id" class="form-control">
						<option value="0"></option>
						<?php
						require_once(CLASS_DIR."menu.class.php");
						$m=new menu($mysqli);
						$menu_data=$m->menu_all();
						for($i=0;$i<count($menu_data);$i++)
						{
							?>
							<option value="<?php echo $menu_data[$i]->menu_id; ?>" <?php if(isset($_GET['action'])&&isset($_GET['id'])){  if($m_show->parent_id==$menu_data[$i]->menu_id) { echo " selected "; }  } ?>><?php echo $menu_data[$i]->menu_name; ?></option>
							<?php
						}
						?>
						</select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="val_number">Icon</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        <input type="text" id="icon" name="icon" class="form-control" value="<?php if(isset($_GET['action'])&&isset($_GET['id'])){ echo $m_show->icon; } ?>">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                </div>
            </div>
		<p align="left"><a href="menu.php" class="btn btn-default"><i class="fa fa-reply"></i> Back</a></p>
        </div>
        <!-- END Form Content -->
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php include 'inc/footer.php'; // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script>
    $(function(){

        /* For advanced usage and examples please check out
         *  Jquery Validation   -> https://github.com/jzaefferer/jquery-validation
         */

        /* Initialize Form Validation */
        $('#form-validation').validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function(error, e) {
                e.parents('.form-group > div').append(error);
            },
            highlight: function(e){
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.help-block').remove();
            },
            success: function(e){
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.help-block').remove();
                e.closest('.help-inline').remove();
            },
            rules: {
                menu_name: {
                    required: true,
                    minlength: 3
                },
                menu_code: {
                    required: true,
                    number: true
                }            },
            messages: {
                menu_name: {
                    required: 'Please enter a menu Name',
                    minlength: 'Your menu Name must consist of at least 3 characters'
                },
                menu_code: {
                    required: 'Please enter menu Code',
                    minlength: 'You must enter number only'
                }            
			}
        });
    });
$(document).ready(function() {
    $("#menu_code").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>

<?php include 'inc/bottom.php'; // Close body and html tags ?>
