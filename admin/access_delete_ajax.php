<?php
include getcwd()."/inc/config.php"; // Configuration php file
require(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');



if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['access_id']))
{
	require_once(CLASS_DIR."access.class.php");
	$prv=new access($mysqli);
	$prv->delete($_POST['access_id']);
}


?>