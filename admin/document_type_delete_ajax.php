<?php
include getcwd()."/inc/config.php"; // Configuration php file
require_once(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');
if($sessionObj->read('user_email')=="")
{
	header("Location:page_login.php");
}

if(isset($_POST['dt_id']))
{
	require_once(CLASS_DIR."front_document_type.class.php");
	$prv=new front_document_type($mysqli);
	$prv->delete($_POST['dt_id']);
}


?>