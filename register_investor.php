<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');

require(CLASS_DIR.'user.class.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

if($sessionObj->read('front_user_email')!="")
{
	header("Location:index.php");
}
?>

<?php
$error="";
$error2="";
//echo $sessionObj->read('email');
if(isset($_POST['investor_email']))
{
	require_once(CLASS_DIR.'front_user_investor.class.php');
	require_once(CLASS_DIR.'front_info_personal_investment.class.php');
	require_once(CLASS_DIR."front_document_pendukung.class.php");
	require_once(CLASS_DIR."front_info_investor_company.class.php");
	require_once(CLASS_DIR."front_info_company_investment.class.php");
	if($_POST['investor_type']=="personal")
	{
		$fud=new front_user_investor($mysqli);
		$fud->insert(			
				$_POST['investor_name']
				,$_POST['investor_email']
				,null
				,$_POST['investor_address']
				,$_POST['investor_province']
				,$_POST['investor_kota']
				,$_POST['investor_phone']
				,$_POST['investor_mobile']
				,$_POST['investor_npwp']
				,$_POST['investor_ktp_sim']
				,$_POST['investor_tempat_lahir']
				,date("Y-m-d",strtotime($_POST['investor_dob']))
				,$_POST['investor_gender']
				,$_POST['investor_education']
				,$_POST['investor_job']
				,$_POST['investor_company']
				,$_POST['investor_jabatan']
				,null
				,null
				,0
				,0
				,$_POST['investor_email']
			);
			$fu_id=$fud->conn->insert_id;
			$pd=new front_info_personal_investment($mysqli);
			$pd->insert(			
			$fu_id
			,$_POST['investor_max_time']
			,$_POST['investor_minimum']
			,$_POST['investor_dana']
			,$_POST['investor_ktp_file_hidden']
			,$_POST['investor_sim_file_hidden']
			,$_POST['investor_email']
			);	

			
				$directory=ROOT_DIR."\\file\\investor\\".$fu_id;
//				echo $directory;
				if(!file_exists($directory))
					mkdir($directory,0777,true);
				chmod($directory,777);
//				echo fileperms($directory);
				$tmp_directory="tmp/";



				//KTP
				$moved_file = $directory."\\".$_POST['investor_ktp_file_hidden'];
//				echo $moved_file;
				$uploaded_file=$tmp_directory.$_POST['investor_ktp_file_hidden'];
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 1";
					rename($uploaded_file,$moved_file);
				}
				//SIM FILE
				$moved_file = $directory."\\".$_POST['investor_sim_file_hidden'];
				$uploaded_file=$tmp_directory.$_POST['investor_sim_file_hidden'];
//				echo $moved_file;
//				echo is_writable($moved_file) ? "ok" : "no";
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 2";
					rename($uploaded_file,$moved_file);
				}
				//echo print_r($_FILES);
				if(isset($_FILES))
				{
					for($i=0; $i<count($_FILES['personal_pendukung_file']['name']); $i++)
					{
						if($_FILES['personal_pendukung_file']['size'][$i]>0)
						{
							$target_path = $directory."/";
							$target_path = $target_path .$_FILES['personal_pendukung_file']['name'][$i] ; 

							if(move_uploaded_file($_FILES['personal_pendukung_file']['tmp_name'][$i], $target_path)) 
							{
								echo "The file has been uploaded successfully <br />";
							} 
							else
							{
								echo "There was an error uploading the file, please try again! <br />";
							}

							$dp=new front_document_pendukung($mysqli);
							$dp->insert($_FILES['personal_pendukung_file']['name'][$i]
							,$fu_id
							,0
							,$_POST['document_type_personal'][$i]
							,$_POST['investor_email']
							);
						}

					}
				}

		header("Location:success_registration_investor.php");


	}

	else
	{
		$fud=new front_user_investor($mysqli);
		$fud->insert(			
				''
				,$_POST['investor_email']
				,null
				,''
				,0
				,0
				,0
				,''
				,''
				,''
				,""
				,date("Y-m-d",strtotime("1/1/1970"))
				,''
				,0
				,0
				,''
				,''
				,null
				,null
				,0
				,0
				,$_POST['investor_email']
			);
			$fu_id=$fud->conn->insert_id;
			echo "sukses user insert";
			$dc=new front_info_investor_company($mysqli);
			$dc->insert(			
			$fu_id
			,$_POST['company_name']
			,$_POST['company_address']
			,$_POST['company_province']
			,$_POST['investor_kota']
			,$_POST['company_no_npwp']
			,$_POST['company_no_sk']
			,$_POST['company_no_siup']
			,$_POST['company_no_akte']
			,$_POST['company_no_tdp']
			,$_POST['company_jenis_bh']
			,$_POST['company_no_phone']
			,$_POST['company_nama_penanggung']
			,$_POST['company_jabatan_penanggung']
			,$_POST['company_penanggung_phone']
			,$_POST['company_penanggung_mobile']
			,$_POST['investor_email']);

			$pd=new front_info_company_investment($mysqli);
			$pd-> insert(			
			$fu_id
			,$_POST['company_max_time']
			,$_POST['company_minimum']
			,$_POST['company_dana']
			,$_POST['company_akte_file_hidden']
			,$_POST['company_npwp_file_hidden']
			,$_POST['investor_email']
			);	

			
				$directory=ROOT_DIR."\\file\\investor\\".$fu_id;
//				echo $directory;
				if(!file_exists($directory))
					mkdir($directory,0777,true);
				chmod($directory,777);
//				echo fileperms($directory);
				$tmp_directory="tmp/";



				//KTP
				$moved_file = $directory."\\".$_POST['company_akte_file_hidden'];
//				echo $moved_file;
				$uploaded_file=$tmp_directory.$_POST['company_akte_file_hidden'];
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 1";
					rename($uploaded_file,$moved_file);
				}
				//KK FILE
				$moved_file = $directory."\\".$_POST['company_npwp_file_hidden'];
				$uploaded_file=$tmp_directory.$_POST['company_npwp_file_hidden'];
//				echo $moved_file;
//				echo is_writable($moved_file) ? "ok" : "no";
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 2";
					rename($uploaded_file,$moved_file);
				}
//				echo print_r($_FILES);
				if(isset($_FILES))
				{
					for($i=0; $i<count($_FILES['company_pendukung_file']['name']); $i++)
					{
						if($_FILES['company_pendukung_file']['size'][$i]>0)
						{
							$target_path = $directory."/";
							$target_path = $target_path .$_FILES['company_pendukung_file']['name'][$i] ; 

							if(move_uploaded_file($_FILES['company_pendukung_file']['tmp_name'][$i], $target_path)) 
							{
								echo "The file has been uploaded successfully <br />";
							} 
							else
							{
								echo "There was an error uploading the file, please try again! <br />";
							}

							$dp=new front_document_pendukung($mysqli);
							$dp->insert($_FILES['company_pendukung_file']['name'][$i]
							,$fu_id
							,0
							,$_POST['document_type'][$i]
							,$_POST['investor_email']
							);
						}
					}
				}

		header("Location:success_registration_investor.php");



	}

		$to      = $_POST['investor_email'];
		$subject = 'Sukses Pendaftaran Perusahaan di Aplikasi RELASI';
		$message = 'Hi '.$_POST['investor_email']."<br/>";
		$message.= "Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftarkan perusahaan anda<br/><br/>";
		$message.= "Regards<br/><br/>";
		$message.= "Web Admin";
		$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
		'Reply-To: '.ADMIN_EMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

}
//echo $error;
//echo $error2;
?>
<?php include INC_DIR.'top.php'; ?>
<?php// include INC_DIR.'nav.php'; ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="<?php echo SITE_URL;?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo SITE_URL;?>register_investor.php">Register as Investor</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-box remove-margin">
	<input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
        <!-- Form Header -->
        <h4 class="form-box-header">Register as Investor <small>Please fill in the form to register as investor</small></h4>
		<p align="left"><a href="<?php echo SITE_URL; ?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to Home</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            
<?php 
if($error!="") 
{
	echo '<div class="form-group"><div class="alert alert-success">'.$error."</div></div>";  
}
if($error2!="") 
{ 
	echo '<div class="form-group"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div></div>";  
}
			?>
			<script type="text/javascript">
			</script>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#reg_as_investor_1" data-toggle="tab" id="email_and_type_investor">Email and Type Investor</a></li>
						<li><a href="#reg_as_investor_personal_1" data-toggle="tab" id="personal_investor_1">Step 1 for Personal Investor</a></li>
						<li><a href="#reg_as_investor_personal_2" data-toggle="tab" id="personal_investor_2">Step 2 for Personal Investor</a></li>
						<li><a href="#reg_as_investor_company_1" data-toggle="tab" id="company_investor_1">Step 1 for Company Investor</a></li>
						<li><a href="#reg_as_investor_company_2" data-toggle="tab" id="company_investor_2">Step 2 for Company Investor</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="reg_as_investor_1">
						<!-- Start reg_as_investor_1 -->
							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_email">Email *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_email" name="investor_email" class="form-control" value="<?php if(isset($_POST['investor_email'])){ echo $_POST['investor_email']; } ?>" size="300">
										<span id="investor_email_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_email_message"></span><input type="hidden" id="stat_email_available" name="stat_email_available">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="investor_type">Jenis Investor *</label>
								<div class="col-md-4">
									<div class="input-group" style="vertical-align:middle">
										<input type="radio" id="investor_type_personal" style="margin-right:5px; margin-top:7px;" name="investor_type"  value="personal" <?php if(isset($_POST['investor_type'])){ if($_POST['investor_type']=="personal") { echo ' checked="checked" '; } } ?>>Perorangan <span>&nbsp;&nbsp;&nbsp;</span>


										<input type="radio" id="investor_type_company"  style="margin-right:5px; margin-top:7px;" name="investor_type"  value="company" <?php if(isset($_POST['investor_type'])){ if($_POST['investor_type']=="company") { echo ' checked="checked" '; } } ?>>Perusahaan<br/>
										<span id="investor_type_error" style="color:red; font-weight:bold;"></span>
									</div>
								</div>
							</div>
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btnNext"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						
						<!-- Eof reg_as_investor_1 -->
						</div>


						<div class="tab-pane" id="reg_as_investor_personal_1">
						<!-- Start Step 1 of Personal Investor -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_name">Nama *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_name" name="investor_name" class="form-control" value="<?php if(isset($_POST['investor_name'])){ echo $_POST['investor_name']; } ?>" size="300">
										<span id="investor_name_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_name_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_address">Alamat *</label>
								<div class="col-md-4">
									<div class="input-group">
										<textarea id="investor_address" name="investor_address" width="">
										<?php if(isset($_POST['investor_address'])){ echo $_POST['investor_address']; } ?>
										</textarea>
										<span id="investor_address_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_address_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_province">Provinsi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="investor_province" name="investor_province">
										<?php
										include_once CLASS_DIR."province.class.php";
										$p=new province($mysqli);
										$data=$p->province_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->province_id."'>".$data[$i]->province_name."</option>";
										}
										?>
										<?php if(isset($_POST['investor_address'])){ echo $_POST['investor_address']; } ?>
										</select>
										<span id="investor_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_province_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_kota">Kota *</label>
								<div class="col-md-4">
									<div class="input-group">
										<div id="investor_kota_div">
										<select id="investor_kota" name="investor_kota">
										</select>
										</div>
										<span id="investor_kota_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_kota_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_phone">No Telephone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_phone" name="investor_phone" class="form-control" value="<?php if(isset($_POST['investor_phone'])){ echo $_POST['investor_phone']; } ?>" size="300">
										<span id="investor_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_phone_message"></span>
									</div>
								</div>
							</div>


							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_mobile">No Handphone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_mobile" name="investor_mobile" class="form-control" value="<?php if(isset($_POST['investor_mobile'])){ echo $_POST['investor_mobile']; } ?>" size="300">
										<span id="investor_mobile_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_mobile_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_npwp">No NPWP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_npwp" name="investor_npwp" class="form-control" value="<?php if(isset($_POST['investor_npwp'])){ echo $_POST['investor_npwp']; } ?>" size="300">
										<span id="investor_npwp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_npwp_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_ktp_sim">No KTP/SIM *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_ktp_sim" name="investor_ktp_sim" class="form-control" value="<?php if(isset($_POST['investor_ktp_sim'])){ echo $_POST['investor_ktp_sim']; } ?>" size="300">
										<span id="investor_ktp_sim_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_ktp_sim_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_tempat_lahir">Tempat Lahir *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="investor_tempat_lahir" name="investor_tempat_lahir">
										<?php
										include_once CLASS_DIR."kabupaten.class.php";
										$p=new kabupaten($mysqli);
										$data=$p->kabupaten_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->kabupaten_id."'>".$data[$i]->kabupaten_name."</option>";
										}
										?>
										<?php if(isset($_POST['investor_address'])){ echo $_POST['investor_address']; } ?>
										</select>
										<span id="investor_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_province_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_dob">Tanggal lahir *</label>
								<div class="col-md-4">
									<div class="input-group">
				                        <input type="text" id="investor_dob" name="investor_dob" class="form-control input-datepicker" value="<?php if(isset($_POST['user_birth_date'])){ echo $_POST['user_birth_date']; } ?>">
										<span id="investor_dob_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_dob_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="investor_gender">Jenis Kelamin *</label>
								<div class="col-md-4">
									<div class="input-group" style="vertical-align:middle">
										<input type="radio" id="investor_gender_male" style="margin-right:5px; margin-top:7px;" name="investor_gender"  value="male" <?php if(isset($_POST['investor_gender'])){ if($_POST['investor_gender']=="male") { echo ' checked="checked" '; } } ?>>Laki laki <span>&nbsp;&nbsp;&nbsp;</span>


										<input type="radio" id="investor_gender_female"  style="margin-right:5px; margin-top:7px;" name="investor_gender"  value="female" <?php if(isset($_POST['investor_gender'])){ if($_POST['investor_gender']=="female") { echo ' checked="checked" '; } } ?>>Perempuan<br/>
										<span id="investor_gender_error" style="color:red; font-weight:bold;"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_education">Pendidikan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="investor_education" name="investor_education">
										<?php
										include_once CLASS_DIR."education_level.class.php";
										$p=new education_level($mysqli);
										$data=$p->education_level_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->education_level_id."'>".$data[$i]->education_level_name."</option>";
										}
										?>
										<?php if(isset($_POST['investor_education'])){ echo $_POST['investor_education']; } ?>
										</select>
										<span id="investor_education_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_education_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_job">Pekerjaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="investor_job" name="investor_job">
										<?php
										include_once CLASS_DIR."title.class.php";
										$p=new title($mysqli);
										$data=$p->title_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->title_id."'>".$data[$i]->title_name."</option>";
										}
										?>
										<?php if(isset($_POST['investor_job'])){ echo $_POST['investor_job']; } ?>
										</select>
										<span id="investor_job_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_job_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_company">Nama Perusahaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_company" name="investor_company" class="form-control" value="<?php if(isset($_POST['investor_company'])){ echo $_POST['investor_company']; } ?>" size="300">
										<span id="investor_company_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_company_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_company">Jabatan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_jabatan" name="investor_jabatan" class="form-control" value="<?php if(isset($_POST['investor_jabatan'])){ echo $_POST['investor_jabatan']; } ?>" size="300">
										<span id="investor_jabatan_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_jabatan_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_personal"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						


						<!-- Eof Step 1 of Personal Investor -->

						</div>
						<div class="tab-pane" id="reg_as_investor_personal_2">
						
						<!-- Start Step 2 of Personal Investor -->
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_dana">Dana Investasi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_dana" name="investor_dana" class="form-control" value="<?php if(isset($_POST['investor_dana'])){ echo $_POST['investor_dana']; } ?>" size="300">
										<span id="investor_dana_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_dana_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_max_time">Lama Pinjaman Max (Bulan) *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_max_time" name="investor_max_time" class="form-control" value="<?php if(isset($_POST['investor_max_time'])){ echo $_POST['investor_max_time']; } ?>" size="300">
										<span id="investor_max_time_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_max_time_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_minimum">Minimum Bunga Per Bulan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="investor_minimum" name="investor_minimum" class="form-control" value="<?php if(isset($_POST['investor_minimum'])){ echo $_POST['investor_minimum']; } ?>" size="300">
										<span id="investor_minimum_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="investor_minimum_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group col-md-12 added_content" id="added_content_2" style="display:none;">
										<select name="document_type_personal[]" id="document_type_personal[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="personal_pendukung_file[]" id="personal_pendukung_file[]" style="width:300px;" class="pendukung_upload" multiple="multiple"><span style="float:left;"> </span>
												<br/>
							</div>
							<div id="uploaded_pendukung">
							</div>

								<div class="form-group col-md-12" style="height:auto;">
									<label class="control-label col-md-2" for="personal_pendukung_file">Data Pendukung</label>
									<div id="multiple_file_div_personal" class="col-md-10">
										<input type="hidden" name="counter" id="counter" value="1">
										<select name="document_type_personal[]" id="document_type_personal[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="personal_pendukung_file[]" id="personal_pendukung_file[]" style="width:300px;" class="pendukung_upload" multiple="multiple"><span style="float:left;"> </span>
												<br/>
									</div>

							</div>
							<div class="form-group" style="height:auto; text-align:center;">
							<button type="button" class="btn" id="tambah_data_pendukung_investor_personal">Tambah Data Pendukung Lainnya</button>
							</div>
							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">KTP *</label>
											<div id="investor_ktp_file_div">
											<input type="hidden" name="investor_ktp_file_hidden" id="investor_ktp_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_ktp" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_ktp" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_ktp">
												<input type="file" class="form-control" name="investor_ktp_file" id="investor_ktp_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_ktp">
												<button type="button" class="btn" onclick="webcamstart();">Activate Webcam</button>
															<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>
														<div id="webcam_start">
														<div id="my_camera" style="width:100px; height:70px;"></div><br/>
														<div id="my_result"></div>

														<script type="text/javascript">
															function webcamstart()
															{
																Webcam.attach( '#my_camera' ); $('#webcam_start').show(); $('#my_result').innerHTML='';
															}
															function webcamstop()
															{
																Webcam.reset(); $('#webcam_start').hide(); $('#my_result').innerHTML='';
															}
															function take_snapshot() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_investor_ktp_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#investor_ktp_file_div").html("<input type='hidden' name='investor_ktp_file_hidden' id='investor_ktp_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_investor_ktp_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button" onclick="take_snapshot();" class="btn">Take Snapshot</button>
														<button type="button" onclick="webcamstop();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="investor_ktp_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="investor_ktp_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="investor_ktp_file"></label>
								<div class="col-md-4" id="message_upload_investor_ktp_file">
									<div class="input-group">
									</div>
								</div>
							</div>

							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">SIM *</label>
											<div id="investor_sim_file_div">
											<input type="hidden" name="investor_sim_file_hidden" id="investor_sim_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_sim" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_sim" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_sim">
												<input type="file" class="form-control" name="investor_sim_file" id="investor_sim_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_sim">
												<button type="button" class="btn" onclick="webcamstart2();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_2">
														<div id="my_camera_2" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_2"></div>

														<script type="text/javascript">
															function webcamstart2()
															{
																Webcam.attach( '#my_camera_2' ); $('#webcam_start_2').show();  $('#my_result_2').innerHTML='';
															}
															function webcamstop2()
															{
																Webcam.reset(); $('#webcam_start_2').hide();  $('#my_result_2').innerHTML='';
															}
															function take_snapshot2() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_2').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_investor_sim_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#investor_sim_file_div").html("<input type='hidden' name='investor_sim_file_hidden' id='investor_sim_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_investor_sim_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
															$("#investor_sim_file_div").html("<input type='hidden' name='investor_sim_file_hidden' id='investor_sim_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
																	});
																} );
															}
														</script>

														<button type="button"  onclick="take_snapshot2();" class="btn">Take Snapshot</button>
														<button type="button"  onclick="webcamstop2();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="investor_sim_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="investor_sim_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="investor_sim_file"></label>
								<div class="col-md-4" id="message_upload_investor_sim_file">
									<div class="input-group">
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_submit_personal"><i class="fa fa-floppy-o"></i> Submit</button>
							</div>						
						
						<!-- Eof Step 2 of Personal Investor -->

						</div>
						<div class="tab-pane" id="reg_as_investor_company_1">

						<!-- Start Step 1 of Company Investor -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="investor_name">Nama Perusahaan*</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_name" name="company_name" class="form-control" value="<?php if(isset($_POST['company_name'])){ echo $_POST['company_name']; } ?>" size="300">
										<span id="company_name_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_name_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_address">Alamat *</label>
								<div class="col-md-4">
									<div class="input-group">
										<textarea id="company_address" name="company_address">
										<?php if(isset($_POST['company_address'])){ echo $_POST['company_address']; } ?>
										</textarea>
										<span id="company_address_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_address_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_province">Provinsi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="company_province" name="company_province">
										<?php
										include_once CLASS_DIR."province.class.php";
										$p=new province($mysqli);
										$data=$p->province_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->province_id."'>".$data[$i]->province_name."</option>";
										}
										?>
										</select>
										<span id="company_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_province_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_kota">Kota *</label>
								<div class="col-md-4">
									<div class="input-group">
										<div id="company_kota_div">
										<select id="company_kota" name="investor_kota">
										</select>
										</div>
										<span id="company_kota_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_kota_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_npwp">No NPWP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_npwp" name="company_no_npwp" class="form-control" value="<?php if(isset($_POST['company_no_npwp'])){ echo $_POST['company_no_npwp']; } ?>" size="300">
										<span id="company_no_npwp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_npwp_message"></span>
									</div>
								</div>
							</div>


							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_sk">No SK Badan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_sk" name="company_no_sk" class="form-control" value="<?php if(isset($_POST['company_no_sk'])){ echo $_POST['company_no_sk']; } ?>" size="300">
										<span id="company_no_sk_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_sk_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_siup">No SIUP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_siup" name="company_no_siup" class="form-control" value="<?php if(isset($_POST['company_no_siup'])){ echo $_POST['company_no_siup']; } ?>" size="300">
										<span id="company_no_siup_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_siup_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_akte">No Akte *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_akte" name="company_no_akte" class="form-control" value="<?php if(isset($_POST['company_no_akte'])){ echo $_POST['company_no_akte']; } ?>" size="300">
										<span id="company_no_akte_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_akte_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_tdp">No TDP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_tdp" name="company_no_tdp" class="form-control" value="<?php if(isset($_POST['company_no_tdp'])){ echo $_POST['company_no_tdp']; } ?>" size="300">
										<span id="company_no_tdp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_tdp_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_jenis_bh">Jenis Badan Hukum *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="company_jenis_bh" name="company_jenis_bh">
										<?php
										include_once CLASS_DIR."badan_hukum.class.php";
										$p=new badan_hukum($mysqli);
										$data=$p->badan_hukum_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->bh_id."'>".$data[$i]->bh_name."</option>";
										}
										?>
										</select>
										<span id="company_jenis_bh_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_jenis_bh_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_phone">No Telephone Perusahaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_phone" name="company_no_phone" class="form-control" value="<?php if(isset($_POST['company_no_phone'])){ echo $_POST['company_no_phone']; } ?>" size="300">
										<span id="company_no_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_phone_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_nama_penanggung">Nama Penanggung Jawab *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_nama_penanggung" name="company_nama_penanggung" class="form-control" value="<?php if(isset($_POST['company_nama_penanggung'])){ echo $_POST['company_nama_penanggung']; } ?>" size="300">
										<span id="company_nama_penanggung_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_nama_penanggung_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_jabatan_penanggung">Jabatan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_jabatan_penanggung" name="company_jabatan_penanggung" class="form-control" value="<?php if(isset($_POST['company_jabatan_penanggung'])){ echo $_POST['company_jabatan_penanggung']; } ?>" size="300">
										<span id="company_jabatan_penanggung_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_jabatan_penanggung_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_penanggung_phone">No Telephone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_penanggung_phone" name="company_penanggung_phone" class="form-control" value="<?php if(isset($_POST['company_penanggung_phone'])){ echo $_POST['company_penanggung_phone']; } ?>" size="300">
										<span id="company_penanggung_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_penanggung_phone_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_penanggung_mobile">No Handphone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_penanggung_mobile" name="company_penanggung_mobile" class="form-control" value="<?php if(isset($_POST['company_penanggung_mobile'])){ echo $_POST['company_penanggung_mobile']; } ?>" size="300">
										<span id="company_penanggung_mobile_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_penanggung_mobile_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_company"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						

						
						<!-- Eof Step 1 of Company Investor -->


						</div>
						<div class="tab-pane" id="reg_as_investor_company_2">
						
						<!-- Step 2 of Company Investor -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_dana">Dana Investasi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_dana" name="company_dana" class="form-control" value="<?php if(isset($_POST['company_dana'])){ echo $_POST['company_dana']; } ?>" size="300">
										<span id="company_dana_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_dana_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_max_time">Lama Pinjaman Max (Bulan) *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_max_time" name="company_max_time" class="form-control" value="<?php if(isset($_POST['company_max_time'])){ echo $_POST['company_max_time']; } ?>" size="300">
										<span id="company_max_time_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_max_time_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_minimum">Minimum Bunga Per Bulan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_minimum" name="company_minimum" class="form-control" value="<?php if(isset($_POST['company_minimum'])){ echo $_POST['company_minimum']; } ?>" size="300">
										<span id="company_minimum_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_minimum_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group col-md-12 added_content" id="added_content" style="display:none;">
										<select name="document_type[]" id="document_type[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="company_pendukung_file[]" id="company_pendukung_file" style="width:300px;" class="pendukung_upload"><span style="float:left;"> </span>
												<br/>
							</div>
							<div id="uploaded_pendukung">
							</div>

								<div class="form-group col-md-12" style="height:auto;">
									<label class="control-label col-md-2" for="company_pendukung_file">Data Pendukung</label>
									<div id="multiple_file_div_company" class="col-md-10">
										<input type="hidden" name="counter" id="counter" value="1">
										<select name="document_type[]" id="document_type[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="company_pendukung_file[]" id="company_pendukung_file" style="width:300px;" class="pendukung_upload"><span style="float:left;"> </span>
												<br/>
									</div>

							</div>
							<div class="form-group" style="height:auto; text-align:center;">
							<button type="button" class="btn" id="tambah_data_pendukung_investor_company">Tambah Data Pendukung Lainnya</button>
							</div>

							
							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">Akte *</label>
											<div id="company_akte_file_div">
											<input type="hidden" name="company_akte_file_hidden" id="company_akte_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_akte" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_akte" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_akte">
												<input type="file" class="form-control" name="company_akte_file" id="company_akte_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_akte">
												<button type="button" class="btn" onclick="webcamstart3();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_3">
														<div id="my_camera_3" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_3"></div>

														<script type="text/javascript">
															function webcamstart3()
															{
																Webcam.attach( '#my_camera_3' ); $('#webcam_start_3').show();  $('#my_result_3').innerHTML='';
															}
															function webcamstop3()
															{
																Webcam.reset(); $('#webcam_start_3').hide();  $('#my_result_3').innerHTML='';
															}
															function take_snapshot3() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_3').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_company_akte_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#company_akte_file_div").html("<input type='hidden' name='company_akte_file_hidden' id='company_akte_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_company_akte_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button" onclick="take_snapshot3();" class="btn">Take Snapshot</button>
														<button type="button" onclick="webcamstop3();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="company_akte_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="company_akte_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="company_akte_file"></label>
								<div class="col-md-4" id="message_upload_company_akte_file">
									<div class="input-group">
									</div>
								</div>
							</div>

							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">NPWP *</label>
											<div id="company_npwp_file_div">
											<input type="hidden" name="company_npwp_file_hidden" id="company_npwp_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_npwp" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_npwp" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_npwp">
												<input type="file" class="form-control" name="company_npwp_file" id="company_npwp_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_npwp">
												<button type="button" class="btn" onclick="webcamstart4();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_4">
														<div id="my_camera_4" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_4"></div>

														<script type="text/javascript">
															function webcamstart4()
															{
																Webcam.attach( '#my_camera_4' ); $('#webcam_start_4').show(); $('#investor_ktp_file_div').hide(); $('#my_result_4').innerHTML='';
															}
															function webcamstop4()
															{
																Webcam.reset(); $('#webcam_start_4').hide(); $('#company_npwp_file_div').show(); $('#my_result_4').innerHTML='';
															}
															function take_snapshot4() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_4').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_company_npwp_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#company_npwp_file_div").html("<input type='hidden' name='company_npwp_file_hidden' id='company_npwp_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_company_npwp_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button"  onclick="take_snapshot4();" class="btn">Take Snapshot</button>
														<button type="button"  onclick="webcamstop4();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="company_npwp_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="company_npwp_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="company_npwp_file"></label>
								<div class="col-md-4" id="message_upload_company_npwp_file">
									<div class="input-group">
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_submit_company"><i class="fa fa-floppy-o"></i> Submit</button>
							</div>						
						
						<!-- Eof Step 2 of Personal Investor -->


						</div>
					</div>

        </div>
        <!-- END Form Content -->
		<p align="left"><a href="<?php echo SITE_URL;?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to Home</a></p>
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php  include('inc/footer.php'); // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script type="text/javascript">

		function state_choose_investor_type()
		{
			$('#email_and_type_investor').show();
			$('#email_and_type_investor').trigger('click');
			$('#personal_investor_1').hide();
			$('#personal_investor_2').hide();
			$('#company_investor_1').hide();
			$('#company_investor_2').hide();
		}
		function check_availability_email()
		{
			var stat;
			 $.ajax({
				url: "check_front_user_email_investor_ajax.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'user_email=' + $("#investor_email").val()  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#investor_email_message").html('Checking email availability in database...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						if(data=="1")
						{
							$("#investor_email_message").html("<span  style=\"color:red; font-weight:bold;\"><i class='gi gi-remove_2'></i>Email tidak tersedia dan telah digunakan</span>");
							$("#stat_email_available").val("");
						}
						else
						{
							$("#investor_email_message").html("<span  style=\"color:green; font-weight:bold;\"><i class='gi gi-ok_2'></i>Email bisa digunakan</span>");
							$("#stat_email_available").val("bisa");
						}
					}
				});
				if($("#stat_email_available").val()=="bisa")
				{
					stat=true;
				}
				else
				{
					stat=false;
				}
//				alert(stat);
			return stat;
		}
		function get_kabupaten_by_province_id_2(id)
		{
			 $.ajax({
				url: "ajax_get_kabupaten_by_province_id.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'province_id=' + id  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#company_kota_div").html('Getting Kabupaten By Province you select...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						$("#company_kota_div").html(data);
					}
				});
		}
		function get_kabupaten_by_province_id(id)
		{
			 $.ajax({
				url: "ajax_get_kabupaten_by_province_id.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'province_id=' + id  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#investor_kota_div").html('Getting Kabupaten By Province you select...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						$("#investor_kota_div").html(data);
					}
				});
		}
		function validateEmail(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
		}
$(document).ready(function() {
						//alert("test");
/* AUTOMATIC WINDOW LOAD SCRIPT */

			$("#tambah_data_pendukung_investor_company").click(
					function()
					{
						$("#multiple_file_div_company").append($("#added_content").html());
					}
				)

			$("#tambah_data_pendukung_investor_personal").click(
					function()
					{
						$("#multiple_file_div_personal").append($("#added_content_2").html());
					}
				)


			state_choose_investor_type();

/* EOF AUTOMATIC WINDOW LOAD SCRIPT */

/* SCRIPT FOR SUBMIT COMPANY INVESTOR */

			$("#company_dana").change(
				function()
				{
					$("#company_dana_error").html("");
					$("#company_dana_message").html("");
				}
				);

			$("#company_max_time").change(
				function()
				{
					$("#company_max_time_error").html("");
					$("#company_max_time_message").html("");
				}
				);

			$("#company_minimum").change(
				function()
				{
					$("#company_minimum_error").html("");
					$("#company_minimum_message").html("");
				}
				);

			$("#btn_submit_company").click(
					function()
					{
						//alert("test");
						var statnext;
						statnext=true;
						if($("#company_dana").val().length<1)
						{
							statnext=false;
							$("#company_dana_error").html("Please fill in your Dana");
						}
						if($("#company_max_time").val().length<1)
						{
							statnext=false;
							$("#company_max_time_error").html("Please fill in your Maximum Lama Investasi");
						}
						if($("#company_minimum").val().length<1)
						{
							statnext=false;
							$("#company_minimum_error").html("Please fill in your Minimum");
						}
						
//						alert($("#company_akte_file_hidden").val());
						if($("#company_akte_file_hidden").val().length<1)
						{
							statnext=false;
							$("#company_akte_file_error").html("Please Upload your Akte File");
						}

						if($("#company_npwp_file_hidden").val().length<1)
						{
							statnext=false;
							$("#company_npwp_file_error").html("Please Upload Your NPWP File");
						}

						if(statnext==true)
						{
							$( "#form-validation" ).submit();
						}
						if(statnext==true)
						{
							$( "#form-validation" ).submit();
						}
					}
				);

/* EOF SCRIPT FOR SUBMIT COMPANY INVESTOR */



/* SCRIPT FOR NEXT COMPANY INVESTOR */

			$("#company_name").change(
					function()
					{
						$("#company_name_error").html("");
						$("#company_name_message").html("");
					}
				);

			$("#company_no_npwp").change(
					function()
					{
						$("#company_no_npwp_error").html("");
						$("#company_no_npwp_message").html("");
					}
				);

			$("#company_no_sk").change(
					function()
					{
						$("#company_no_sk_error").html("");
						$("#company_no_sk_message").html("");
					}
				);

			$("#company_no_siup").change(
					function()
					{
						$("#company_no_siup_error").html("");
						$("#company_no_siup_message").html("");
					}
				);

			$("#company_no_akte").change(
					function()
					{
						$("#company_no_akte_error").html("");
						$("#company_no_akte_message").html("");
					}
				);

			$("#company_no_tdp").change(
					function()
					{
						$("#company_no_tdp_error").html("");
						$("#company_no_tdp_message").html("");
					}
				);

			$("#company_no_phone").change(
					function()
					{
						$("#company_no_phone_error").html("");
						$("#company_no_phone_message").html("");
					}
				);

			$("#company_nama_penanggung").change(
					function()
					{
						$("#company_nama_penanggung_error").html("");
						$("#company_nama_penanggung_message").html("");
					}
				);

			$("#company_jabatan_penanggung").change(
					function()
					{
						$("#company_jabatan_penanggung_error").html("");
						$("#company_jabatan_penanggung_message").html("");
					}
				);

			$("#company_penanggung_phone").change(
					function()
					{
						$("#company_penanggung_phone_error").html("");
						$("#company_penanggung_phone_message").html("");
					}
				);

			$("#company_penanggung_mobile").change(
					function()
					{
						$("#company_penanggung_mobile_error").html("");
						$("#company_penanggung_mobile_message").html("");
					}
				);


			$("#btn_next_company").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#company_name").val().length<4)
						{
							statnext=false;
							$("#company_name_error").html("Please fill in your Company name, must be 4 characters or more");
						}
						if($("#company_no_npwp").val().length<4)
						{
							statnext=false;
							$("#company_no_npwp_error").html("Please fill in your NPWP Number, must be 4 characters or more");
						}
						if($("#company_no_sk").val().length<4)
						{
							statnext=false;
							$("#company_no_sk_error").html("Please fill in your SK number, must be 4 characters or more");
						}
						if($("#company_no_siup").val().length<4)
						{
							statnext=false;
							$("#company_no_siup_error").html("Please fill in your SIUP Number, must be 4 characters or more");
						}
						if($("#company_no_akte").val().length<4)
						{
							statnext=false;
							$("#company_no_akte_error").html("Please fill in your Akte Number, must be 4 characters or more");
						}
						if($("#company_no_tdp").val().length<4)
						{
							statnext=false;
							$("#company_no_tdp_error").html("Please fill in your TDP Number, must be 4 characters or more");
						}
						if($("#company_no_phone").val().length<4)
						{
							statnext=false;
							$("#company_no_phone_error").html("Please fill in your Company Phone Number, must be 4 characters or more");
						}
						if($("#company_nama_penanggung").val().length<4)
						{
							statnext=false;
							$("#company_nama_penanggung_error").html("Please fill in your Company Nama Penanggung, must be 4 characters or more");
						}
						if($("#company_jabatan_penanggung").val().length<4)
						{
							statnext=false;
							$("#company_jabatan_penanggung_error").html("Please fill in your Company Jabatan Penanggung, must be 4 characters or more");
						}
						if($("#company_penanggung_phone").val().length<4)
						{
							statnext=false;
							$("#company_penanggung_phone_error").html("Please fill in your Company Telephone Penanggung, must be 4 characters or more");
						}
						if($("#company_penanggung_mobile").val().length<4)
						{
							statnext=false;
							$("#company_penanggung_mobile_error").html("Please fill in your Company Handphone Penanggung, must be 4 characters or more");
						}
						
						if(statnext==true)
						{
							$('#personal_investor_1').hide();
							$('#personal_investor_2').hide();
							$("#personal_investor_2").hide;
							$('#company_investor_1').hide();
							$('#company_investor_2').show();
							$('#company_investor_2').trigger('click');
							$("#email_and_type_investor").hide();

						}
					}
				);

			$("#company_province").change(
					function()
					{
						get_kabupaten_by_province_id_2($("#company_province").val())
					}
				);


/* EOF SCRIPT FOR NEXT COMPANY INVESTOR */


/* SCRIPT FOR SUBMIT PERSONAL INVESTOR */

			$("#investor_dana").change(
				function()
				{
					$("#investor_dana_error").html("");
					$("#investor_dana_message").html("");
				}
				);

			$("#investor_max_time").change(
				function()
				{
					$("#investor_max_time_error").html("");
					$("#investor_max_time_message").html("");
				}
				);

			$("#investor_minimum").change(
				function()
				{
					$("#investor_minimum_error").html("");
					$("#investor_minimum_message").html("");
				}
				);

			$("#btn_submit_personal").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#investor_dana").val().length<1)
						{
							statnext=false;
							$("#investor_dana_error").html("Please fill in your Dana");
						}
						if($("#investor_max_time").val().length<1)
						{
							statnext=false;
							$("#investor_max_time_error").html("Please fill in your Maximum Lama Investasi");
						}
						if($("#investor_minimum").val().length<1)
						{
							statnext=false;
							$("#investor_minimum_error").html("Please fill in your Minimum");
						}
//						alert($("#investor_ktp_file_hidden").attr('value'));
						if($("#investor_ktp_file_hidden").val().length<1)
						{
							statnext=false;
							$("#investor_ktp_file_error").html("Please Upload your KTP File");
						}
						if($("#investor_sim_file_hidden").val().length<1)
						{
							statnext=false;
							$("#investor_sim_file_error").html("Please Upload Your SIM File");
						}
						if(statnext==true)
						{
							$( "#form-validation" ).submit();
						}
					}
				);

/* EOF SCRIPT FOR SUBMIT PERSONAL INVESTOR */

/* SCRIPT FOR NEXT PERSONAL INVESTOR */

			$("#investor_name").change(
					function()
					{
						$("#investor_name_error").html("");
						$("#investor_name_message").html("");

					}
				);

			$("#investor_phone").change(
					function()
					{
						$("#investor_phone_error").html("");
						$("#investor_phone_message").html("");

					}
				);
			$("#investor_mobile").change(
					function()
					{
						$("#investor_mobile_error").html("");
						$("#investor_mobile_message").html("");

					}
				);
			$("#investor_npwp").change(
					function()
					{
						$("#investor_npwp_error").html("");
						$("#investor_npwp_message").html("");

					}
				);
			$("#investor_ktp_sim").change(
					function()
					{
						$("#investor_ktp_sim_error").html("");
						$("#investor_ktp_sim_message").html("");

					}
				);
			$("#investor_dob").change(
					function()
					{
						$("#investor_dob_error").html("");
						$("#investor_dob_message").html("");

					}
				);
			$("#investor_company").change(
					function()
					{
						$("#investor_company_error").html("");
						$("#investor_company_message").html("");

					}
				);
			$("#investor_jabatan").change(
					function()
					{
						$("#investor_jabatan_error").html("");
						$("#investor_jabatan_message").html("");

					}
				);
			
			$("#investor_gender_male").click(
					function()
					{
						$("#investor_gender_error").html("");

					}
				);
			$("#investor_gender_female").click(
					function()
					{
						$("#investor_gender_error").html("");

					}
				);
			$("#btn_next_personal").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#investor_name").val().length<4)
						{
							statnext=false;
							$("#investor_name_error").html("Please fill in your name, must be 4 characters or more");
						}
/*
						if(document.getElementById("investor_address").value.length<4)
						{
							$("#investor_address_error").html("Please fill in your address, must be 4 characters or more");
						}
*/
						if($("#investor_phone").val().length<4)
						{
							statnext=false;
							$("#investor_phone_error").html("Please fill in your phone, must be 4 characters or more");
						}
						if($("#investor_mobile").val().length<4)
						{
							statnext=false;
							$("#investor_mobile_error").html("Please fill in your Mobile Phone, must be 4 characters or more");
						}
						if($("#investor_npwp").val().length<4)
						{
							statnext=false;
							$("#investor_npwp_error").html("Please fill in your NPWP, must be 4 characters or more");
						}
						if($("#investor_ktp_sim").val().length<4)
						{
							statnext=false;
							$("#investor_ktp_sim_error").html("Please fill in your KTP or SIM, must be 4 characters or more");
						}
						if($("#investor_dob").val().length<4)
						{
							statnext=false;
							$("#investor_dob_error").html("Please choose your birth date");
						}
						if($("#investor_company").val().length<4)
						{
							statnext=false;
							$("#investor_company_error").html("Please fill in your Company, must be 4 characters or more");
						}
						if($("#investor_jabatan").val().length<4)
						{
							statnext=false;
							$("#investor_jabatan_error").html("Please fill in your Jabatan, must be 4 characters or more");
						}
						if(!$('#investor_gender_male').is(':checked') && !$('#investor_gender_female').is(':checked'))
						{
							statnext=false;
							$("#investor_gender_error").html("Please choose your Gender");
						}
						else
						{
							$("#investor_gender_error").html("");
						}
						
						if(statnext==true)
						{
							$('#personal_investor_1').hide();
							$('#personal_investor_2').show();
							$("#personal_investor_2").trigger('click');
							$('#company_investor_1').hide();
							$('#company_investor_2').hide();
							$("#email_and_type_investor").hide();

						}
					}
				);
			$("#investor_province").change(
					function()
					{
						get_kabupaten_by_province_id($("#investor_province").val())
					}
				);

/* EOF SCRIPT FOR NEXT PERSONAL INVESTOR */

/* SCRIPT FOR CHOOSE PERSONAL OR INVESTOR */
			$("#investor_type_personal").click(
					function()
					{
						$("#investor_type_error").html("");

					}
				);
			$("#investor_type_company").click(
					function()
					{
						$("#investor_type_error").html("");

					}
				);
			$("#investor_email").change(
					function()
					{
						check_availability_email();
						$("#investor_email_error").html("");
						$("#investor_email_message").html("");

					}
				);
			$("#btnNext").click(
					function()
					{
						var step1_pass;
						step1_pass=true;
						var mailerror;
//						alert(step1_pass);
						if (!validateEmail($("#investor_email").val()))
						{
							mailerror="Please fill in email with valid format";
							step1_pass=false;
						}
						
						if ($("#investor_email").val().length <5)
						{
							mailerror +="<br/>Please fill in email";
							step1_pass=false;
						}

						if(!$('#investor_type_personal').is(':checked') && !$('#investor_type_company').is(':checked'))
						{
							step1_pass=false;
							$("#investor_type_error").html("Please choose your Investor type");
						}
						else
						{
							$("#investor_type_error").html("");
						}
						//alert(step1_pass);
						if(step1_pass==true)
						{
							$("#investor_email_error").html("");
							step1_pass=check_availability_email();
						}
						else
						{
							$("#investor_email_error").html(mailerror);
						}
						if (step1_pass==true) 
						{
							if($('#investor_type_personal').is(':checked'))
							{
								$('#personal_investor_1').show();
								$("#personal_investor_1").trigger('click');
								$('#company_investor_1').hide();
								$('#company_investor_2').hide();
								$('#personal_investor_2').hide();
								$("#email_and_type_investor").hide();
							}
							else if($('#investor_type_company').is(':checked'))
							{
								$('#company_investor_1').show();
								$('#company_investor_1').trigger('click');
								$('#personal_investor_1').hide();
								$('#personal_investor_2').hide();
								$('#company_investor_2').hide();
								$("#email_and_type_investor").hide();
							}

						}
					}
				);

/* EOF SCRIPT FOR CHOOSING PERSONAL OR INVESTOR */

/* SCRIPT FOR UPLOAD FILE FOR PERSONAL */

	$("#investor_sim_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File SIM anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File SIM anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#investor_sim_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_investor_sim_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_investor_sim_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#investor_sim_file_div").html("<input type='hidden' name='investor_sim_file_hidden' id='investor_sim_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#investor_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KTP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KTP anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#investor_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_investor_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_investor_ktp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#investor_ktp_file_div").html("<input type='hidden' name='investor_ktp_file_hidden' id='investor_ktp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);

/* EOF SCRIPT FOR UPLOADING FILE PERSONAL */

/* SCRIPT FOR UPLOADING FILE COMPANY */

	$("#company_akte_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File Akte anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File Akte anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_akte_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_akte_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_akte_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_akte_file_div").html("<input type='hidden' name='company_akte_file_hidden' id='company_akte_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_npwp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File NPWP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File NPWP anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_npwp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_npwp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_npwp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_npwp_file_div").html("<input type='hidden' name='company_npwp_file_hidden' id='company_npwp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	/* EOF SCRIPT FOR UPLOADING FILE FOR COMPANY */


});
</script>

<?php
			
//include INC_DIR.'footer.php'; // Footer and scripts ?>


<?php include INC_DIR.'bottom.php'; // Close body and html tags ?>