<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');

require(CLASS_DIR.'user.class.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

if($sessionObj->read('front_user_email')!="")
{
	header("Location:index.php");
}
?>

<?php
$error="";
$error2="";
//echo $sessionObj->read('email');
if(isset($_POST['debitur_email']))
{
	require_once(CLASS_DIR.'front_user_debitur.class.php');
	require_once(CLASS_DIR.'front_info_personal_debetment.class.php');
	require_once(CLASS_DIR."front_document_pendukung.class.php");
	require_once(CLASS_DIR."front_info_debitur_company.class.php");
	require_once(CLASS_DIR."front_info_company_debetment.class.php");
	if($_POST['debitur_type']=="personal")
	{
		$fud=new front_user_debitur($mysqli);
		$fud->insert(			
				$_POST['debitur_name']
				,$_POST['debitur_email']
				,null
				,$_POST['debitur_address']
				,$_POST['debitur_province']
				,$_POST['debitur_kota']
				,$_POST['debitur_phone']
				,$_POST['debitur_mobile']
				,$_POST['debitur_npwp']
				,$_POST['debitur_ktp_sim']
				,$_POST['debitur_tempat_lahir']
				,date("Y-m-d",strtotime($_POST['debitur_dob']))
				,$_POST['debitur_gender']
				,$_POST['debitur_education']
				,$_POST['debitur_job']
				,$_POST['debitur_company']
				,$_POST['debitur_jabatan']
				,null
				,null
				,0
				,0
				,$_POST['debitur_email']
			);
			$fu_id=$fud->conn->insert_id;
			$pd=new front_info_personal_debetment($mysqli);
			$pd->insert(			
						$fu_id
						,$_POST['debitur_income']
						,$_POST['debitur_status']
						,$_POST['debitur_jumlah_anak']
						,$_POST['debitur_stt']
						,$_POST['debitur_ktp_file_hidden']
						,$_POST['debitur_kk_file_hidden']
						,$_POST['debitur_email']
			);	

			
				$directory=ROOT_DIR."\\file\\debitur\\".$fu_id;
//				echo $directory;
				if(!file_exists($directory))
					mkdir($directory,0777,true);
				chmod($directory,777);
//				echo fileperms($directory);
				$tmp_directory="tmp/";



				//KTP
				$moved_file = $directory."\\".$_POST['debitur_ktp_file_hidden'];
//				echo $moved_file;
				$uploaded_file=$tmp_directory.$_POST['debitur_ktp_file_hidden'];
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 1";
					rename($uploaded_file,$moved_file);
				}
				//KK FILE
				$moved_file = $directory."\\".$_POST['debitur_kk_file_hidden'];
				$uploaded_file=$tmp_directory.$_POST['debitur_kk_file_hidden'];
//				echo $moved_file;
//				echo is_writable($moved_file) ? "ok" : "no";
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 2";
					rename($uploaded_file,$moved_file);
				}
				//echo print_r($_FILES);
				if(isset($_FILES))
				{
					for($i=0; $i<count($_FILES['personal_pendukung_file']['name']); $i++)
					{
						if($_FILES['personal_pendukung_file']['size'][$i]>0)
						{
							$target_path = $directory."/";
							$target_path = $target_path .$_FILES['personal_pendukung_file']['name'][$i] ; 

							if(move_uploaded_file($_FILES['personal_pendukung_file']['tmp_name'][$i], $target_path)) 
							{
								echo "The file has been uploaded successfully <br />";
							} 
							else
							{
								echo "There was an error uploading the file, please try again! <br />";
							}

							$dp=new front_document_pendukung($mysqli);
							$dp->insert($_FILES['personal_pendukung_file']['name'][$i]
							,$fui_id=0
							,$fu_id
							,$_POST['document_type_personal'][$i]
							,$_POST['debitur_email']
							);
						}

					}
				}

		header("Location:success_registration_debitur.php");


	}

	else
	{
		$fud=new front_user_debitur($mysqli);
		$fud->insert(			
				''
				,$_POST['debitur_email']
				,null
				,''
				,0
				,0
				,0
				,''
				,''
				,''
				,""
				,date("Y-m-d",strtotime("1/1/1970"))
				,''
				,0
				,0
				,''
				,''
				,null
				,null
				,0
				,0
				,$_POST['debitur_email']
			);
			$fu_id=$fud->conn->insert_id;
			echo "sukses user insert";
			$dc=new front_info_debitur_company($mysqli);
			$dc->insert(			
			$fu_id
			,$_POST['company_name']
			,$_POST['company_address']
			,$_POST['company_province']
			,$_POST['debitur_kota']
			,$_POST['company_no_npwp']
			,$_POST['company_no_sk']
			,$_POST['company_no_siup']
			,$_POST['company_no_akte']
			,$_POST['company_no_tdp']
			,$_POST['company_jenis_bh']
			,$_POST['company_no_phone']
			,$_POST['company_nama_penanggung']
			,$_POST['company_jabatan_penanggung']
			,$_POST['company_penanggung_phone']
			,$_POST['company_penanggung_mobile']
			,$_POST['debitur_email']);

			$pd=new front_info_company_debetment($mysqli);
			$pd-> insert(			
			$fu_id
			,$_POST['company_omset']
			,$_POST['company_nilai_asset']
			,$_POST['company_ktp_file_hidden']
			,$_POST['company_kk_file_hidden']
			,$_POST['debitur_email']
			);	

			
				$directory=ROOT_DIR."\\file\\debitur\\".$fu_id;
//				echo $directory;
				if(!file_exists($directory))
					mkdir($directory,0777,true);
				chmod($directory,777);
//				echo fileperms($directory);
				$tmp_directory="tmp/";



				//KTP
				$moved_file = $directory."\\".$_POST['company_ktp_file_hidden'];
//				echo $moved_file;
				$uploaded_file=$tmp_directory.$_POST['company_ktp_file_hidden'];
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 1";
					rename($uploaded_file,$moved_file);
				}
				//KK FILE
				$moved_file = $directory."\\".$_POST['company_kk_file_hidden'];
				$uploaded_file=$tmp_directory.$_POST['company_kk_file_hidden'];
//				echo $moved_file;
//				echo is_writable($moved_file) ? "ok" : "no";
				if (file_exists($uploaded_file) && (!file_exists($moved_file)) || is_writable($moved_file))
				{
//					echo "test 2";
					rename($uploaded_file,$moved_file);
				}
//				echo print_r($_FILES);
				if(isset($_FILES))
				{
					for($i=0; $i<count($_FILES['company_pendukung_file']['name']); $i++)
					{
						if($_FILES['company_pendukung_file']['size'][$i]>0)
						{
							$target_path = $directory."/";
							$target_path = $target_path .$_FILES['company_pendukung_file']['name'][$i] ; 

							if(move_uploaded_file($_FILES['company_pendukung_file']['tmp_name'][$i], $target_path)) 
							{
								echo "The file has been uploaded successfully <br />";
							} 
							else
							{
								echo "There was an error uploading the file, please try again! <br />";
							}

							$dp=new front_document_pendukung($mysqli);
							$dp->insert($_FILES['company_pendukung_file']['name'][$i]
							,$fui_id=0
							,$fu_id
							,$_POST['document_type'][$i]
							,$_POST['debitur_email']
							);
						}
					}
				}

		header("Location:success_registration_debitur.php");



	}

		$to      = $_POST['debitur_email'];
		$subject = 'Sukses Pendaftaran Perusahaan di Aplikasi RELASI';
		$message = 'Hi '.$_POST['debitur_email']."<br/>";
		$message.= "Anda telah sukses melakukan pendaftaran, Mohon tunggu sampai tim kami memverifikasi data anda. Terimakasih telah mendaftarkan perusahaan anda<br/><br/>";
		$message.= "Regards<br/><br/>";
		$message.= "Web Admin";
		$headers = 'From: '.ADMIN_EMAIL . "\r\n" .
		'Reply-To: '.ADMIN_EMAIL . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

}
//echo $error;
//echo $error2;
?>
<?php include INC_DIR.'top.php'; ?>
<?php// include INC_DIR.'nav.php'; ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="<?php echo SITE_URL;?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo SITE_URL;?>register_debitur.php">Register as Debitur</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
    <form id="form-validation" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-box remove-margin">
	<input type="hidden" id="session_id" name="session_id" value="<?php echo session_id();?>">
        <!-- Form Header -->
        <h4 class="form-box-header">Register as Debitur <small>Please fill in the form to register as debitur</small></h4>
		<p align="left"><a href="<?php echo SITE_URL; ?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to Home</a></p>
        <!-- Form Content -->
        <div class="form-box-content">
            
			<?php 
			if($error!="") 
			{
				echo '<div class="form-group"><div class="alert alert-success">'.$error."</div></div>";  
			}
			if($error2!="") 
			{ 
				echo '<div class="form-group"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$error2."</div></div>";  
			}
			?>
			<script type="text/javascript">
			</script>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#reg_as_debitur_1" data-toggle="tab" id="email_and_type_debitur">Email and Type Debitur</a></li>
						<li><a href="#reg_as_debitur_personal_1" data-toggle="tab" id="personal_debitur_1">Step 1 for Personal Debitur</a></li>
						<li><a href="#reg_as_debitur_personal_2" data-toggle="tab" id="personal_debitur_2">Step 2 for Personal Debitur</a></li>
						<li><a href="#reg_as_debitur_personal_3" data-toggle="tab" id="personal_debitur_3">Step 3 for Personal Debitur</a></li>
						<li><a href="#reg_as_debitur_company_1" data-toggle="tab" id="company_debitur_1">Step 1 for Company Debitur</a></li>
						<li><a href="#reg_as_debitur_company_2" data-toggle="tab" id="company_debitur_2">Step 2 for Company Debitur</a></li>
						<li><a href="#reg_as_debitur_company_3" data-toggle="tab" id="company_debitur_3">Step 3 for Company Debitur</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="reg_as_debitur_1">
						<!-- Start reg_as_debitur_1 -->
							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_email">Email *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_email" name="debitur_email" class="form-control" value="<?php if(isset($_POST['debitur_email'])){ echo $_POST['debitur_email']; } ?>" size="300">
										<span id="debitur_email_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_email_message"></span><input type="hidden" id="stat_email_available" name="stat_email_available">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="debitur_type">Jenis Debitur *</label>
								<div class="col-md-4">
									<div class="input-group" style="vertical-align:middle">
										<input type="radio" id="debitur_type_personal" style="margin-right:5px; margin-top:7px;" name="debitur_type"  value="personal" <?php if(isset($_POST['debitur_type'])){ if($_POST['debitur_type']=="personal") { echo ' checked="checked" '; } } ?>>Perorangan <span>&nbsp;&nbsp;&nbsp;</span>


										<input type="radio" id="debitur_type_company"  style="margin-right:5px; margin-top:7px;" name="debitur_type"  value="company" <?php if(isset($_POST['debitur_type'])){ if($_POST['debitur_type']=="company") { echo ' checked="checked" '; } } ?>>Perusahaan<br/>
										<span id="debitur_type_error" style="color:red; font-weight:bold;"></span>
									</div>
								</div>
							</div>
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btnNext"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						
						<!-- Eof reg_as_debitur_1 -->
						</div>


						<div class="tab-pane" id="reg_as_debitur_personal_1">
						<!-- Start Step 1 of Personal Debitur -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_name">Nama *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_name" name="debitur_name" class="form-control" value="<?php if(isset($_POST['debitur_name'])){ echo $_POST['debitur_name']; } ?>" size="300">
										<span id="debitur_name_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_name_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_address">Alamat *</label>
								<div class="col-md-4">
									<div class="input-group">
										<textarea id="debitur_address" name="debitur_address" width="">
										<?php if(isset($_POST['debitur_address'])){ echo $_POST['debitur_address']; } ?>
										</textarea>
										<span id="debitur_address_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_address_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_province">Provinsi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="debitur_province" name="debitur_province">
										<?php
										include_once CLASS_DIR."province.class.php";
										$p=new province($mysqli);
										$data=$p->province_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->province_id."'>".$data[$i]->province_name."</option>";
										}
										?>
										<?php if(isset($_POST['debitur_address'])){ echo $_POST['debitur_address']; } ?>
										</select>
										<span id="debitur_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_province_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_kota">Kota *</label>
								<div class="col-md-4">
									<div class="input-group">
										<div id="debitur_kota_div">
										<select id="debitur_kota" name="debitur_kota">
										</select>
										</div>
										<span id="debitur_kota_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_kota_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_phone">No Telephone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_phone" name="debitur_phone" class="form-control" value="<?php if(isset($_POST['debitur_phone'])){ echo $_POST['debitur_phone']; } ?>" size="300">
										<span id="debitur_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_phone_message"></span>
									</div>
								</div>
							</div>


							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_mobile">No Handphone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_mobile" name="debitur_mobile" class="form-control" value="<?php if(isset($_POST['debitur_mobile'])){ echo $_POST['debitur_mobile']; } ?>" size="300">
										<span id="debitur_mobile_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_mobile_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_npwp">No NPWP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_npwp" name="debitur_npwp" class="form-control" value="<?php if(isset($_POST['debitur_npwp'])){ echo $_POST['debitur_npwp']; } ?>" size="300">
										<span id="debitur_npwp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_npwp_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_ktp_sim">No KTP/SIM *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_ktp_sim" name="debitur_ktp_sim" class="form-control" value="<?php if(isset($_POST['debitur_ktp_sim'])){ echo $_POST['debitur_ktp_sim']; } ?>" size="300">
										<span id="debitur_ktp_sim_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_ktp_sim_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_tempat_lahir">Tempat Lahir *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="debitur_tempat_lahir" name="debitur_tempat_lahir">
										<?php
										include_once CLASS_DIR."kabupaten.class.php";
										$p=new kabupaten($mysqli);
										$data=$p->kabupaten_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->kabupaten_id."'>".$data[$i]->kabupaten_name."</option>";
										}
										?>
										<?php if(isset($_POST['debitur_address'])){ echo $_POST['debitur_address']; } ?>
										</select>
										<span id="debitur_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_province_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_dob">Tanggal lahir *</label>
								<div class="col-md-4">
									<div class="input-group">
				                        <input type="text" id="debitur_dob" name="debitur_dob" class="form-control input-datepicker" value="<?php if(isset($_POST['debitur_dob'])){ echo $_POST['debitur_dob']; } ?>">
										<span id="debitur_dob_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_dob_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="debitur_gender">Jenis Kelamin *</label>
								<div class="col-md-4">
									<div class="input-group" style="vertical-align:middle">
										<input type="radio" id="debitur_gender_male" style="margin-right:5px; margin-top:7px;" name="debitur_gender"  value="male" <?php if(isset($_POST['debitur_gender'])){ if($_POST['debitur_gender']=="male") { echo ' checked="checked" '; } } ?>>Laki laki <span>&nbsp;&nbsp;&nbsp;</span>


										<input type="radio" id="debitur_gender_female"  style="margin-right:5px; margin-top:7px;" name="debitur_gender"  value="female" <?php if(isset($_POST['debitur_gender'])){ if($_POST['debitur_gender']=="female") { echo ' checked="checked" '; } } ?>>Perempuan<br/>
										<span id="debitur_gender_error" style="color:red; font-weight:bold;"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_education">Pendidikan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="debitur_education" name="debitur_education">
										<?php
										include_once CLASS_DIR."education_level.class.php";
										$p=new education_level($mysqli);
										$data=$p->education_level_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->education_level_id."'>".$data[$i]->education_level_name."</option>";
										}
										?>
										<?php if(isset($_POST['debitur_education'])){ echo $_POST['debitur_education']; } ?>
										</select>
										<span id="debitur_education_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_education_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_job">Pekerjaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="debitur_job" name="debitur_job">
										<?php
										include_once CLASS_DIR."title.class.php";
										$p=new title($mysqli);
										$data=$p->title_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->title_id."'>".$data[$i]->title_name."</option>";
										}
										?>
										<?php if(isset($_POST['debitur_job'])){ echo $_POST['debitur_job']; } ?>
										</select>
										<span id="debitur_job_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_job_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_company">Nama Perusahaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_company" name="debitur_company" class="form-control" value="<?php if(isset($_POST['debitur_company'])){ echo $_POST['debitur_company']; } ?>" size="300">
										<span id="debitur_company_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_company_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_company">Jabatan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_jabatan" name="debitur_jabatan" class="form-control" value="<?php if(isset($_POST['debitur_jabatan'])){ echo $_POST['debitur_jabatan']; } ?>" size="300">
										<span id="debitur_jabatan_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_jabatan_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_personal"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						


						<!-- Eof Step 1 of Personal Debitur -->

						</div>
						<div class="tab-pane " id="reg_as_debitur_personal_2">
						
						<!-- Start Step 2 of Personal Debitur -->
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_income">Pendapatan per Bulan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_income" name="debitur_income" class="form-control" value="<?php if(isset($_POST['debitur_income'])){ echo $_POST['debitur_income']; } ?>" size="300">
										<span id="debitur_income_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_income_message"></span>
									</div>
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-2" for="debitur_status">Status *</label>
								<div class="col-md-4">
									<div class="input-group" style="vertical-align:middle">
										<input type="radio" id="debitur_status_menikah" style="margin-right:5px; margin-top:7px;" name="debitur_status"  value="menikah" <?php if(isset($_POST['debitur_status'])){ if($_POST['debitur_status']=="menikah") { echo ' checked="checked" '; } } ?>>Menikah <span>&nbsp;&nbsp;&nbsp;</span>


										<input type="radio" id="debitur_status_belum_menikah"  style="margin-right:5px; margin-top:7px;" name="debitur_status"  value="belum menikah" <?php if(isset($_POST['debitur_status'])){ if($_POST['debitur_status']=="belum menikah") { echo ' checked="checked" '; } } ?>>Belum Menikah<br/>

										<input type="radio" id="debitur_status_cerai"  style="margin-right:5px; margin-top:7px;" name="debitur_status"  value="cerai" <?php if(isset($_POST['debitur_status'])){ if($_POST['debitur_status']=="cerai") { echo ' checked="checked" '; } } ?>>Cerai<br/>
										
										<span id="debitur_status_error" style="color:red; font-weight:bold;"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_jumlah_anak">Jumlah Anak *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="debitur_jumlah_anak" name="debitur_jumlah_anak" class="form-control" value="<?php if(isset($_POST['debitur_jumlah_anak'])){ echo $_POST['debitur_jumlah_anak']; } ?>" size="300">
										<span id="debitur_jumlah_anak_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_jumlah_anak_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_stt">Status Tempat Tinggal *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="debitur_stt" name="debitur_stt">
										<?php
										include_once CLASS_DIR."stt.class.php";
										$p=new stt($mysqli);
										$data=$p->stt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->stt_id."'>".$data[$i]->stt_name."</option>";
										}
										?>
										</select>
										<span id="debitur_stt_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="debitur_stt_message"></span>
									</div>
								</div>
							</div>

						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_personal_2"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						
						
						<!-- Eof Step 2 of Personal Debitur -->

						</div>
						<div class="tab-pane" id="reg_as_debitur_personal_3">
						
						<!-- Start Step 3 of Personal Debitur -->
						
						
							<div class="form-group col-md-12 added_content" id="added_content_2" style="display:none;">
										<select name="document_type_personal[]" id="document_type_personal[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="personal_pendukung_file[]" id="personal_pendukung_file[]" style="width:300px;" class="pendukung_upload" multiple="multiple"><span style="float:left;"> </span>
												<br/>
							</div>
							<div id="uploaded_pendukung">
							</div>

								<div class="form-group col-md-12" style="height:auto;">
									<label class="control-label col-md-2" for="personal_pendukung_file">Data Pendukung</label>
									<div id="multiple_file_div_personal" class="col-md-10">
										<input type="hidden" name="counter" id="counter" value="1">
										<select name="document_type_personal[]" id="document_type_personal[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="personal_pendukung_file[]" id="personal_pendukung_file[]" style="width:300px;" class="pendukung_upload" multiple="multiple"><span style="float:left;"> </span>
												<br/>
									</div>

							</div>
							<div class="form-group" style="height:auto; text-align:center;">
							<button type="button" class="btn" id="tambah_data_pendukung_debitur_personal">Tambah Data Pendukung Lainnya</button>
							</div>


							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">KTP *</label>
											<div id="debitur_ktp_file_div">
											<input type="hidden" name="debitur_ktp_file_hidden" id="debitur_ktp_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_ktp" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_ktp" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_ktp">
												<input type="file" class="form-control" name="debitur_ktp_file" id="debitur_ktp_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_ktp">
												<button type="button" class="btn" onclick="webcamstart();">Activate Webcam</button>
															<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>
														<div id="webcam_start">
														<div id="my_camera" style="width:100px; height:70px;"></div><br/>
														<div id="my_result"></div>

														<script type="text/javascript">
															function webcamstart()
															{
																Webcam.attach( '#my_camera' ); $('#webcam_start').show();  $('#my_result').innerHTML='';
															}
															function webcamstop()
															{
																Webcam.reset(); $('#webcam_start').hide();  $('#my_result').innerHTML='';
															}
															function take_snapshot() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_debitur_ktp_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#debitur_ktp_file_div").html("<input type='hidden' name='debitur_ktp_file_hidden' id='debitur_ktp_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_debitur_ktp_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button" onclick="take_snapshot();" class="btn">Take Snapshot</button>
														<button type="button" onclick="webcamstop();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="debitur_ktp_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="debitur_ktp_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="debitur_ktp_file"></label>
								<div class="col-md-4" id="message_upload_debitur_ktp_file">
									<div class="input-group">
									</div>
								</div>
							</div>

							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">KK *</label>
											<div id="debitur_kk_file_div">
											<input type="hidden" name="debitur_kk_file_hidden" id="debitur_kk_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_kk" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_kk" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_kk">
												<input type="file" class="form-control" name="debitur_kk_file" id="debitur_kk_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_kk">
												<button type="button" class="btn" onclick="webcamstart2();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_2">
														<div id="my_camera_2" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_2"></div>

														<script type="text/javascript">
															function webcamstart2()
															{
																Webcam.attach( '#my_camera_2' ); $('#webcam_start_2').show();  $('#my_result_2').innerHTML='';
															}
															function webcamstop2()
															{
																Webcam.reset(); $('#webcam_start_2').hide();  $('#my_result_2').innerHTML='';
															}
															function take_snapshot2() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_2').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_debitur_kk_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#debitur_kk_file_div").html("<input type='hidden' name='debitur_kk_file_hidden' id='debitur_kk_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_debitur_kk_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button"  onclick="take_snapshot2();" class="btn">Take Snapshot</button>
														<button type="button"  onclick="webcamstop2();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="debitur_kk_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="debitur_kk_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="debitur_kk_file"></label>
								<div class="col-md-4" id="message_upload_debitur_kk_file">
									<div class="input-group">
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_submit_personal"><i class="fa fa-floppy-o"></i> Submit</button>
							</div>						
						
						<!-- Eof Step 3 of Personal Debitur -->

						</div>
						<div class="tab-pane" id="reg_as_debitur_company_1">

						<!-- Start Step 1 of Company Debitur -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="debitur_name">Nama Perusahaan*</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_name" name="company_name" class="form-control" value="<?php if(isset($_POST['company_name'])){ echo $_POST['company_name']; } ?>" size="300">
										<span id="company_name_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_name_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_address">Alamat *</label>
								<div class="col-md-4">
									<div class="input-group">
										<textarea id="company_address" name="company_address">
										<?php if(isset($_POST['company_address'])){ echo $_POST['company_address']; } ?>
										</textarea>
										<span id="company_address_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_address_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_province">Provinsi *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="company_province" name="company_province">
										<?php
										include_once CLASS_DIR."province.class.php";
										$p=new province($mysqli);
										$data=$p->province_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->province_id."'>".$data[$i]->province_name."</option>";
										}
										?>
										</select>
										<span id="company_province_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_province_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_kota">Kota *</label>
								<div class="col-md-4">
									<div class="input-group">
										<div id="company_kota_div">
										<select id="company_kota" name="company_kota">
										</select>
										</div>
										<span id="company_kota_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_kota_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_npwp">No NPWP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_npwp" name="company_no_npwp" class="form-control" value="<?php if(isset($_POST['company_no_npwp'])){ echo $_POST['company_no_npwp']; } ?>" size="300">
										<span id="company_no_npwp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_npwp_message"></span>
									</div>
								</div>
							</div>


							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_sk">No SK Badan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_sk" name="company_no_sk" class="form-control" value="<?php if(isset($_POST['company_no_sk'])){ echo $_POST['company_no_sk']; } ?>" size="300">
										<span id="company_no_sk_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_sk_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_siup">No SIUP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_siup" name="company_no_siup" class="form-control" value="<?php if(isset($_POST['company_no_siup'])){ echo $_POST['company_no_siup']; } ?>" size="300">
										<span id="company_no_siup_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_siup_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_akte">No Akte *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_akte" name="company_no_akte" class="form-control" value="<?php if(isset($_POST['company_no_akte'])){ echo $_POST['company_no_akte']; } ?>" size="300">
										<span id="company_no_akte_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_akte_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_tdp">No TDP *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_tdp" name="company_no_tdp" class="form-control" value="<?php if(isset($_POST['company_no_tdp'])){ echo $_POST['company_no_tdp']; } ?>" size="300">
										<span id="company_no_tdp_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_tdp_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_jenis_bh">Jenis Badan Hukum *</label>
								<div class="col-md-4">
									<div class="input-group">
										<select id="company_jenis_bh" name="company_jenis_bh">
										<?php
										include_once CLASS_DIR."badan_hukum.class.php";
										$p=new badan_hukum($mysqli);
										$data=$p->badan_hukum_all();
										for($i=0;$i<count($data);$i++)
										{
											echo "<option value='".$data[$i]->bh_id."'>".$data[$i]->bh_name."</option>";
										}
										?>
										</select>
										<span id="company_jenis_bh_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_jenis_bh_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_no_phone">No Telephone Perusahaan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_no_phone" name="company_no_phone" class="form-control" value="<?php if(isset($_POST['company_no_phone'])){ echo $_POST['company_no_phone']; } ?>" size="300">
										<span id="company_no_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_no_phone_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_nama_penanggung">Nama Penanggung Jawab *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_nama_penanggung" name="company_nama_penanggung" class="form-control" value="<?php if(isset($_POST['company_nama_penanggung'])){ echo $_POST['company_nama_penanggung']; } ?>" size="300">
										<span id="company_nama_penanggung_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_nama_penanggung_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_jabatan_penanggung">Jabatan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_jabatan_penanggung" name="company_jabatan_penanggung" class="form-control" value="<?php if(isset($_POST['company_jabatan_penanggung'])){ echo $_POST['company_jabatan_penanggung']; } ?>" size="300">
										<span id="company_jabatan_penanggung_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_jabatan_penanggung_message"></span>
									</div>
								</div>
							</div>
							<div class="form-group">			
								<label class="control-label col-md-2" for="company_penanggung_phone">No Telephone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_penanggung_phone" name="company_penanggung_phone" class="form-control" value="<?php if(isset($_POST['company_penanggung_phone'])){ echo $_POST['company_penanggung_phone']; } ?>" size="300">
										<span id="company_penanggung_phone_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_penanggung_phone_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_penanggung_mobile">No Handphone *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_penanggung_mobile" name="company_penanggung_mobile" class="form-control" value="<?php if(isset($_POST['company_penanggung_mobile'])){ echo $_POST['company_penanggung_mobile']; } ?>" size="300">
										<span id="company_penanggung_mobile_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_penanggung_mobile_message"></span>
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_company"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						

						
						<!-- Eof Step 1 of Company Debitur -->


						</div>

						<div class="tab-pane" id="reg_as_debitur_company_2">
						
						<!-- Step 2 of Company Debitur -->

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_omset">Omset Per Bulan *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_omset" name="company_omset" class="form-control" value="<?php if(isset($_POST['company_omset'])){ echo $_POST['company_dana']; } ?>" size="300">
										<span id="company_omset_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_omset_message"></span>
									</div>
								</div>
							</div>

							<div class="form-group">			
								<label class="control-label col-md-2" for="company_nilai_asset">Nilai Asset *</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" id="company_nilai_asset" name="company_nilai_asset" class="form-control" value="<?php if(isset($_POST['company_nilai_asset'])){ echo $_POST['company_nilai_asset']; } ?>" size="300">
										<span id="company_nilai_asset_error" style="color:red; font-weight:bold;"></span><br/>
										<span id="company_nilai_asset_message"></span>
									</div>
								</div>
							</div>
						
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_next_company_2"><i class="fa fa-arrow-right"></i> Next</button>
							</div>						
						
						<!-- Eof Step 2 of Company Debitur -->


						</div>
						<div class="tab-pane " id="reg_as_debitur_company_3">
						
						<!-- Step 3 of Company Debitur -->
							<div class="form-group col-md-12 added_content" id="added_content" style="display:none;">
										<select name="document_type[]" id="document_type[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="company_pendukung_file[]" id="company_pendukung_file" style="width:300px;" class="pendukung_upload"><span style="float:left;"> </span>
												<br/>
							</div>
							<div id="uploaded_pendukung">
							</div>

								<div class="form-group col-md-12" style="height:auto;">
									<label class="control-label col-md-2" for="company_pendukung_file">Data Pendukung</label>
									<div id="multiple_file_div_company" class="col-md-10">
										<input type="hidden" name="counter" id="counter" value="1">
										<select name="document_type[]" id="document_type[]" class=" col-md-3">
										<?php
										require_once CLASS_DIR."front_document_type.class.php";
										$doc=new front_document_type($mysqli);
										$data=$doc->dt_all();
										for($i=0;$i<count($data);$i++)
										{
											echo '<option value="'.$data[$i]->dt_id.'">'.$data[$i]->dt_name.'</option>';
										}
										?>
										</select>
													<input type="file" name="company_pendukung_file[]" id="company_pendukung_file" style="width:300px;" class="pendukung_upload"><span style="float:left;"> </span>
												<br/>
									</div>

							</div>
							<div class="form-group" style="height:auto; text-align:center;">
							<button type="button" class="btn" id="tambah_data_pendukung_debitur_company">Tambah Data Pendukung Lainnya</button>
							</div>

							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">KTP *</label>
											<div id="company_ktp_file_div">
											<input type="hidden" name="company_ktp_file_hidden" id="company_ktp_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_ktp_company" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_ktp_company" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_ktp_company">
												<input type="file" class="form-control" name="company_ktp_file" id="company_ktp_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_ktp_company">
												<button type="button" class="btn" onclick="webcamstart3();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_3">
														<div id="my_camera_3" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_3"></div>

														<script type="text/javascript">
															function webcamstart3()
															{
																Webcam.attach( '#my_camera_3' ); $('#webcam_start_3').show();  $('#my_result_3').innerHTML='';
															}
															function webcamstop3()
															{
																Webcam.reset(); $('#webcam_start_3').hide();  $('#my_result_3').innerHTML='';
															}
															function take_snapshot3() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_3').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_company_ktp_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#company_ktp_file_div").html("<input type='hidden' name='company_ktp_file_hidden' id='company_ktp_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_company_ktp_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button" onclick="take_snapshot3();" class="btn">Take Snapshot</button>
														<button type="button" onclick="webcamstop3();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="company_ktp_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="company_ktp_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="company_ktp_file"></label>
								<div class="col-md-4" id="message_upload_company_ktp_file">
									<div class="input-group">
									</div>
								</div>
							</div>

							<div class="form-group" style="height:auto;">
								<label class="control-label col-md-2" for="company_npwp_file">KK *</label>
											<div id="company_kk_file_div">
											<input type="hidden" name="company_kk_file_hidden" id="company_kk_file_hidden" value="">
											</div>
								<div class="col-md-4">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1_kk_company" data-toggle="tab"><i class="fa fa-folder-o"></i></a></li>
										<li><a href="#tab2_kk_company" data-toggle="tab"><i class="fa fa-camera"></i></a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1_kk_company">
												<input type="file" class="form-control" name="company_kk_file" id="company_kk_file" style="width:300px;" > 100KB Max Size, File Picture
										</div>
										<div class="tab-pane" id="tab2_kk_company">
												<button type="button" class="btn" onclick="webcamstart4();">Activate Webcam</button>
															<!--<script src="<?php echo SITE_URL; ?>media/webcam.js"></script>-->
														<div id="webcam_start_4">
														<div id="my_camera_4" style="width:100px; height:70px;"></div><br/>
														<div id="my_result_4"></div>

														<script type="text/javascript">
															function webcamstart4()
															{
																Webcam.attach( '#my_camera_4' ); $('#webcam_start_4').show();  $('#my_result_4').innerHTML='';
															}
															function webcamstop4()
															{
																Webcam.reset(); $('#webcam_start_4').hide();  $('#my_result_4').innerHTML='';
															}
															function take_snapshot4() {
																Webcam.snap( function(data_uri) {
																	document.getElementById('my_result_4').innerHTML = '<img src="'+data_uri+'"/>';
															$("#message_upload_company_kk_file").html('Uploading file...');
															var url_ajax='image_file_upload_ajax_2.php';
									Webcam.upload( data_uri, url_ajax, function(code, text2) {
																	var parsed = jQuery.parseJSON(text2);
																	$("#company_kk_file_div").html("<input type='hidden' name='company_kk_file_hidden' id='company_kk_file_hidden' value='" +  parsed.name +"'><i class='fi fi-jpg'></i>" + parsed.name);
															$("#message_upload_company_kk_file").html('<font color="green"><b>Success Upload Snapshot</b></font>');
																	});
																} );
															}
														</script>

														<button type="button"  onclick="take_snapshot4();" class="btn">Take Snapshot</button>
														<button type="button"  onclick="webcamstop4();" class="btn">Stop Webcam</button>
														</div>
										</div>
									</div>
								</div>
								<span id="company_kk_file_error" style="color:red; font-weight:bold;"></span><br/>
								<span id="company_kk_file_message"></span>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2" for="company_kk_file"></label>
								<div class="col-md-4" id="message_upload_company_kk_file">
									<div class="input-group">
									</div>
								</div>
							</div>
						
							<div class="form-group" style="text-align:center">
			                    <button type="button" class="btn btn-success" id="btn_submit_company"><i class="fa fa-floppy-o"></i> Submit</button>
							</div>						
						
						<!-- Eof Step 3 of Company Debitur -->


						</div>

					
					</div>

        </div>
        <!-- END Form Content -->
		<p align="left"><a href="<?php echo SITE_URL;?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to Home</a></p>
    </form>
    <!-- END Form Validation -->
</div>
<!-- END Page Content -->

<?php  include_once(INC_DIR.'/footer.php'); // Footer and scripts ?>

<!-- Javascript code only for this page -->
<script type="text/javascript">

		function state_choose_debitur_type()
		{
			$('#email_and_type_debitur').show();
			$('#email_and_type_debitur').trigger('click');
			$('#personal_debitur_1').hide();
			$('#personal_debitur_2').hide();
			$('#personal_debitur_3').hide();
			$('#company_debitur_1').hide();
			$('#company_debitur_2').hide();
			$('#company_debitur_3').hide();
		}
		function check_availability_email()
		{
			var stat;
			 $.ajax({
				url: "check_front_user_email_debitur_ajax.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'user_email=' + $("#debitur_email").val()  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#debitur_email_message").html('Checking email availability in database...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						if(data=="1")
						{
							$("#debitur_email_message").html("<span  style=\"color:red; font-weight:bold;\"><i class='gi gi-remove_2'></i>Email tidak tersedia dan telah digunakan</span>");
							$("#stat_email_available").val("");
						}
						else
						{
							$("#debitur_email_message").html("<span  style=\"color:green; font-weight:bold;\"><i class='gi gi-ok_2'></i>Email bisa digunakan</span>");
							$("#stat_email_available").val("bisa");
						}
					}
				});
				if($("#stat_email_available").val()=="bisa")
				{
					stat=true;
				}
				else
				{
					stat=false;
				}
//				alert(stat);
			return stat;
		}
		function get_kabupaten_by_province_id_2(id)
		{
			 $.ajax({
				url: "ajax_get_kabupaten_by_province_id_debitur.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'province_id=' + id  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#company_kota_div").html('Getting Kabupaten By Province you select...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						$("#company_kota_div").html(data);
					}
				});
		}
		function get_kabupaten_by_province_id(id)
		{
			 $.ajax({
				url: "ajax_get_kabupaten_by_province_id_debitur.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: 'province_id=' + id  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				  beforeSend: function( data ) {
					$("#debitur_kota_div").html('Getting Kabupaten By Province you select...');

				  },
				success: function(data)   // A function to be called if request succeeds
					{
						$("#debitur_kota_div").html(data);
					}
				});
		}
		function validateEmail(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
		}
$(document).ready(function() {
						//alert("test");
/* AUTOMATIC WINDOW LOAD SCRIPT */

			$("#tambah_data_pendukung_debitur_company").click(
					function()
					{
						$("#multiple_file_div_company").append($("#added_content").html());
					}
				)

			$("#tambah_data_pendukung_debitur_personal").click(
					function()
					{
						$("#multiple_file_div_personal").append($("#added_content_2").html());
					}
				)
			state_choose_debitur_type();

/* EOF AUTOMATIC WINDOW LOAD SCRIPT */

/* SCRIPT FOR SUBMIT COMPANY DEBITUR */

			$("#btn_submit_company").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#company_ktp_file_hidden").val().length<1)
						{
							statnext=false;
							$("#company_ktp_file_error").html("Please Upload your KTP File");
						}

						if($("#company_kk_file_hidden").val().length<1)
						{
							statnext=false;
							$("#company_kk_file_error").html("Please Upload Your KK File");
						}

						if(statnext==true)
						{
							$( "#form-validation" ).submit();
						}
					}
				);

/* EOF SCRIPT FOR SUBMIT COMPANY INVESTOR */

/* SCRIPT FOR SUBMIT COMPANY DEBITUR */

			$("#company_omset").change(
				function()
				{
					$("#company_omset_error").html("");
					$("#company_omset_message").html("");
				}
				);

			$("#company_nilai_asset").change(
				function()
				{
					$("#company_nilai_asset_error").html("");
					$("#company_nilai_asset_message").html("");
				}
				);


			$("#btn_next_company_2").click(
					function()
					{
						//alert("test");
						var statnext;
						statnext=true;
						if($("#company_omset").val().length<1)
						{
							statnext=false;
							$("#company_omset_error").html("Please fill in your Dana");
						}
						if($("#company_nilai_asset").val().length<1)
						{
							statnext=false;
							$("#company_nilai_asset_error").html("Please fill in your Maximum Lama Investasi");
						}
						if(statnext==true)
						{
							$('#email_and_type_debitur').hide();
							$('#personal_debitur_1').hide();
							$('#personal_debitur_2').hide();
							$('#personal_debitur_3').hide();
							$('#company_debitur_1').hide();
							$('#company_debitur_2').hide();
							$('#company_debitur_3').show();
							$('#company_debitur_3').trigger('click');	
						}
					}
				);

/* EOF SCRIPT FOR NEXT COMPANY INVESTOR 2 */



/* SCRIPT FOR NEXT COMPANY INVESTOR */

			$("#company_name").change(
					function()
					{
						$("#company_name_error").html("");
						$("#company_name_message").html("");
					}
				);

			$("#company_no_npwp").change(
					function()
					{
						$("#company_no_npwp_error").html("");
						$("#company_no_npwp_message").html("");
					}
				);

			$("#company_no_sk").change(
					function()
					{
						$("#company_no_sk_error").html("");
						$("#company_no_sk_message").html("");
					}
				);

			$("#company_no_siup").change(
					function()
					{
						$("#company_no_siup_error").html("");
						$("#company_no_siup_message").html("");
					}
				);

			$("#company_no_akte").change(
					function()
					{
						$("#company_no_akte_error").html("");
						$("#company_no_akte_message").html("");
					}
				);

			$("#company_no_tdp").change(
					function()
					{
						$("#company_no_tdp_error").html("");
						$("#company_no_tdp_message").html("");
					}
				);

			$("#company_no_phone").change(
					function()
					{
						$("#company_no_phone_error").html("");
						$("#company_no_phone_message").html("");
					}
				);

			$("#company_nama_penanggung").change(
					function()
					{
						$("#company_nama_penanggung_error").html("");
						$("#company_nama_penanggung_message").html("");
					}
				);

			$("#company_jabatan_penanggung").change(
					function()
					{
						$("#company_jabatan_penanggung_error").html("");
						$("#company_jabatan_penanggung_message").html("");
					}
				);

			$("#company_penanggung_phone").change(
					function()
					{
						$("#company_penanggung_phone_error").html("");
						$("#company_penanggung_phone_message").html("");
					}
				);

			$("#company_penanggung_mobile").change(
					function()
					{
						$("#company_penanggung_mobile_error").html("");
						$("#company_penanggung_mobile_message").html("");
					}
				);


			$("#btn_next_company").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#company_name").val().length<4)
						{
							statnext=false;
							$("#company_name_error").html("Please fill in your Company name, must be 4 characters or more");
						}
						if($("#company_no_npwp").val().length<4)
						{
							statnext=false;
							$("#company_no_npwp_error").html("Please fill in your NPWP Number, must be 4 characters or more");
						}
						if($("#company_no_sk").val().length<4)
						{
							statnext=false;
							$("#company_no_sk_error").html("Please fill in your SK number, must be 4 characters or more");
						}
						if($("#company_no_siup").val().length<4)
						{
							statnext=false;
							$("#company_no_siup_error").html("Please fill in your SIUP Number, must be 4 characters or more");
						}
						if($("#company_no_akte").val().length<4)
						{
							statnext=false;
							$("#company_no_akte_error").html("Please fill in your Akte Number, must be 4 characters or more");
						}
						if($("#company_no_tdp").val().length<4)
						{
							statnext=false;
							$("#company_no_tdp_error").html("Please fill in your TDP Number, must be 4 characters or more");
						}
						if($("#company_no_phone").val().length<4)
						{
							statnext=false;
							$("#company_no_phone_error").html("Please fill in your Company Phone Number, must be 4 characters or more");
						}
						if($("#company_nama_penanggung").val().length<4)
						{
							statnext=false;
							$("#company_nama_penanggung_error").html("Please fill in your Company Nama Penanggung, must be 4 characters or more");
						}
						if($("#company_jabatan_penanggung").val().length<4)
						{
							statnext=false;
							$("#company_jabatan_penanggung_error").html("Please fill in your Company Jabatan Penanggung, must be 4 characters or more");
						}
						if($("#company_penanggung_phone").val().length<4)
						{
							statnext=false;
							$("#company_penanggung_phone_error").html("Please fill in your Company Telephone Penanggung, must be 4 characters or more");
						}
						if($("#company_penanggung_mobile").val().length<4)
						{
							statnext=false;
							$("#company_penanggung_mobile_error").html("Please fill in your Company Handphone Penanggung, must be 4 characters or more");
						}
						
						if(statnext==true)
						{
							$('#personal_debitur_1').hide();
							$('#personal_debitur_2').hide();
							$("#personal_debitur_3").hide;
							$('#company_debitur_1').hide();
							$('#company_debitur_2').show();
							$('#company_debitur_2').trigger('click');
							$('#company_debitur_3').hide();
							$("#email_and_type_debitur").hide();

						}
					}
				);

			$("#company_province").change(
					function()
					{
						get_kabupaten_by_province_id_2($("#company_province").val())
					}
				);


/* EOF SCRIPT FOR NEXT COMPANY INVESTOR */


/* SCRIPT FOR SUBMIT PERSONAL INVESTOR */

			$("#btn_submit_personal").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#debitur_ktp_file_hidden").val().length<1)
						{
							statnext=false;
							$("#debitur_ktp_file_error").html("Please Upload your KTP File");
						}
						if($("#debitur_kk_file_hidden").val().length<1)
						{
							statnext=false;
							$("#debitur_kk_file_error").html("Please Upload Your SIM File");
						}
						if(statnext==true)
						{
							$( "#form-validation" ).submit();
						}
					}
				);

/* EOF SCRIPT FOR SUBMIT PERSONAL INVESTOR */

/* SCRIPT FOR NEXT PERSONAL INVESTOR 2 */

			$("#debitur_income").change(
				function()
				{
					$("#debitur_income_error").html("");
					$("#debitur_income_message").html("");
				}
				);

			$("#debitur_jumlah_anak").change(
				function()
				{
					$("#debitur_jumlah_anak_error").html("");
					$("#debitur_jumlah_anak_message").html("");
				}
				);

			$("#debitur_status_menikah").click(
				function()
				{
					$("#debitur_status_error").html("");
					$("#debitur_status_message").html("");
				}
				);

			$("#debitur_status_belum_menikah").click(
				function()
				{
					$("#debitur_status_error").html("");
					$("#debitur_status_message").html("");
				}
				);

			$("#debitur_status_cerai").click(
				function()
				{
					$("#debitur_status_error").html("");
					$("#debitur_status_message").html("");
				}
				);

			$("#btn_next_personal_2").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#debitur_income").val().length<4)
						{
							statnext=false;
							$("#debitur_income_error").html("Please fill in your Income");
						}
						if($("#debitur_jumlah_anak").val().length<1)
						{
							statnext=false;
							$("#debitur_jumlah_anak_error").html("Please fill in your Jumlah Anak");
						}
						if(!$('#debitur_status_menikah').is(':checked') && !$('#debitur_status_belum_menikah').is(':checked') && !$('#debitur_status_cerai').is(':checked'))
						{
							statnext=false;
							$("#debitur_status_error").html("Please choose your Status Perkawinan");
						}
						else
						{
							$("#debitur_status_error").html("");
						}
						if(statnext==true)
						{
							$('#personal_debitur_1').hide();
							$('#personal_debitur_2').hide();
							$("#personal_debitur_3").show;
							$("#personal_debitur_3").trigger('click');
							$('#company_debitur_1').hide();
							$('#company_debitur_2').hide();
							$('#company_debitur_3').hide();
							$("#email_and_type_debitur").hide();

						}
					}
				);

/* EOF SCRIPT FOR NEXT PERSONAL INVESTOR 2 */
/* SCRIPT FOR NEXT PERSONAL INVESTOR */

			$("#debitur_name").change(
					function()
					{
						$("#debitur_name_error").html("");
						$("#debitur_name_message").html("");

					}
				);

			$("#debitur_phone").change(
					function()
					{
						$("#debitur_phone_error").html("");
						$("#debitur_phone_message").html("");

					}
				);
			$("#debitur_mobile").change(
					function()
					{
						$("#debitur_mobile_error").html("");
						$("#debitur_mobile_message").html("");

					}
				);
			$("#debitur_npwp").change(
					function()
					{
						$("#debitur_npwp_error").html("");
						$("#debitur_npwp_message").html("");

					}
				);
			$("#debitur_ktp_sim").change(
					function()
					{
						$("#debitur_ktp_sim_error").html("");
						$("#debitur_ktp_sim_message").html("");

					}
				);
			$("#debitur_dob").change(
					function()
					{
						$("#debitur_dob_error").html("");
						$("#debitur_dob_message").html("");

					}
				);
			$("#debitur_company").change(
					function()
					{
						$("#debitur_company_error").html("");
						$("#debitur_company_message").html("");

					}
				);
			$("#debitur_jabatan").change(
					function()
					{
						$("#debitur_jabatan_error").html("");
						$("#debitur_jabatan_message").html("");

					}
				);
			
			$("#debitur_gender_male").click(
					function()
					{
						$("#debitur_gender_error").html("");

					}
				);
			$("#debitur_gender_female").click(
					function()
					{
						$("#debitur_gender_error").html("");

					}
				);
			$("#btn_next_personal").click(
					function()
					{
						var statnext;
						statnext=true;
						if($("#debitur_name").val().length<4)
						{
							statnext=false;
							$("#debitur_name_error").html("Please fill in your name, must be 4 characters or more");
						}
/*
						if(document.getElementById("debitur_address").value.length<4)
						{
							$("#debitur_address_error").html("Please fill in your address, must be 4 characters or more");
						}
*/
						if($("#debitur_phone").val().length<4)
						{
							statnext=false;
							$("#debitur_phone_error").html("Please fill in your phone, must be 4 characters or more");
						}
						if($("#debitur_mobile").val().length<4)
						{
							statnext=false;
							$("#debitur_mobile_error").html("Please fill in your Mobile Phone, must be 4 characters or more");
						}
						if($("#debitur_npwp").val().length<4)
						{
							statnext=false;
							$("#debitur_npwp_error").html("Please fill in your NPWP, must be 4 characters or more");
						}
						if($("#debitur_ktp_sim").val().length<4)
						{
							statnext=false;
							$("#debitur_ktp_sim_error").html("Please fill in your KTP or SIM, must be 4 characters or more");
						}
						if($("#debitur_dob").val().length<4)
						{
							statnext=false;
							$("#debitur_dob_error").html("Please choose your birth date");
						}
						if($("#debitur_company").val().length<4)
						{
							statnext=false;
							$("#debitur_company_error").html("Please fill in your Company, must be 4 characters or more");
						}
						if($("#debitur_jabatan").val().length<4)
						{
							statnext=false;
							$("#debitur_jabatan_error").html("Please fill in your Jabatan, must be 4 characters or more");
						}
						if(!$('#debitur_gender_male').is(':checked') && !$('#debitur_gender_female').is(':checked'))
						{
							statnext=false;
							$("#debitur_gender_error").html("Please choose your Gender");
						}
						else
						{
							$("#debitur_gender_error").html("");
						}
						
						if(statnext==true)
						{
							$('#personal_debitur_1').hide();
							$('#personal_debitur_2').show();
							$("#personal_debitur_2").trigger('click');
							$('#personal_debitur_3').hide();
							$('#company_debitur_1').hide();
							$('#company_debitur_2').hide();
							$('#company_debitur_3').hide();
							$("#email_and_type_debitur").hide();

						}
					}
				);
			$("#debitur_province").change(
					function()
					{
						get_kabupaten_by_province_id($("#debitur_province").val())
					}
				);

/* EOF SCRIPT FOR NEXT PERSONAL INVESTOR */

/* SCRIPT FOR CHOOSE PERSONAL OR INVESTOR */
			$("#debitur_type_personal").click(
					function()
					{
						$("#debitur_type_error").html("");

					}
				);
			$("#debitur_type_company").click(
					function()
					{
						$("#debitur_type_error").html("");

					}
				);
			$("#debitur_email").change(
					function()
					{
						check_availability_email();
						$("#debitur_email_error").html("");
						$("#debitur_email_message").html("");

					}
				);
			$("#btnNext").click(
					function()
					{
						var step1_pass;
						step1_pass=true;
						var mailerror;
//						alert(step1_pass);
						if (!validateEmail($("#debitur_email").val()))
						{
							mailerror="Please fill in email with valid format";
							step1_pass=false;
						}
						
						if ($("#debitur_email").val().length <5)
						{
							mailerror +="<br/>Please fill in email";
							step1_pass=false;
						}

						if(!$('#debitur_type_personal').is(':checked') && !$('#debitur_type_company').is(':checked'))
						{
							step1_pass=false;
							$("#debitur_type_error").html("Please choose your Debitur type");
						}
						else
						{
							$("#debitur_type_error").html("");
						}
						//alert(step1_pass);
						if(step1_pass==true)
						{
							$("#debitur_email_error").html("");
							step1_pass=check_availability_email();
						}
						else
						{
							$("#debitur_email_error").html(mailerror);
						}
						if (step1_pass==true) 
						{
							if($('#debitur_type_personal').is(':checked'))
							{
								$('#personal_debitur_1').show();
								$("#personal_debitur_1").trigger('click');
								$('#company_debitur_1').hide();
								$('#company_debitur_2').hide();
								$('#company_debitur_3').hide();
								$('#personal_debitur_2').hide();
								$('#personal_debitur_3').hide();
								$("#email_and_type_debitur").hide();
							}
							else if($('#debitur_type_company').is(':checked'))
							{
								$('#company_debitur_1').show();
								$('#company_debitur_1').trigger('click');
								$('#personal_debitur_1').hide();
								$('#personal_debitur_2').hide();
								$('#personal_debitur_3').hide();
								$('#company_debitur_2').hide();
								$('#company_debitur_3').hide();
								$("#email_and_type_debitur").hide();
							}

						}
					}
				);

/* EOF SCRIPT FOR CHOOSING PERSONAL OR INVESTOR */

/* SCRIPT FOR UPLOAD FILE FOR PERSONAL */

	$("#debitur_kk_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KK anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KK anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#debitur_kk_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_debitur_kk_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_debitur_kk_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#debitur_kk_file_div").html("<input type='hidden' name='debitur_kk_file_hidden' id='debitur_kk_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#debitur_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KTP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KTP anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#debitur_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_debitur_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_debitur_ktp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#debitur_ktp_file_div").html("<input type='hidden' name='debitur_ktp_file_hidden' id='debitur_ktp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);

/* EOF SCRIPT FOR UPLOADING FILE PERSONAL */

/* SCRIPT FOR UPLOADING FILE COMPANY */

	$("#company_ktp_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KTP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KTP anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_ktp_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_ktp_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_ktp_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_ktp_file_div").html("<input type='hidden' name='company_ktp_file_hidden' id='company_ktp_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$("#company_kk_file").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KK anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KK anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_kk_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_company_kk_file").html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_company_kk_file").html(data);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_kk_file_div").html("<input type='hidden' name='company_kk_file_hidden' id='company_kk_file_hidden' value='" +  file.name +"'><i class='fi fi-jpg'></i>" + file.name);
						}
					});
				}
		}
	);
	$(".pendukung_upload").change(
		function()
		{
				var file = this.files[0];
				name = file.name;
				size = file.size;
				type = file.type;

				if(file.name.length < 1) {
				}
				else if(file.size > 100000) {
					alert("Ukuran File KTP anda terlalu besar");
				}
				else if(file.type != 'image/jpg'  && file.type != 'image/jpeg'  && file.type != 'image/png' ) {
					alert("Format File KTP anda bukan JPEG/JPG/PNG");
				}
				else 
				{ 			
//					alert("test");
					var fd = new FormData();
					fd.append('image_file', $('#company_kk_file')[0].files[0]);
					fd.append('session_id',$("#session_id").val());
					$.ajax({
					url: "image_file_upload_ajax.php", // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: fd  , // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					cache: false,        // To send DOMDocument or non processed data file it is set to false
					  beforeSend: function( data ) {
						$("#message_upload_pendukung_file" + i).html('Uploading file...');

					  },
					success: function(data)   // A function to be called if request succeeds
						{
							$("#message_upload_pendukung_file" +i).html("<font color='green'>success : </font>" + file.name);
							if(data=="<font color='green'><b>Berhasil Upload data</b></font>")
								$("#company_kk_file_div").append("<input type='hidden' name='company_pendukung_file_hidden" + i + "' id='company_kk_file_hidden" + i + "' value='" +  file.name +"'>");
						}
					});
				}
		}
	);
	/* EOF SCRIPT FOR UPLOADING FILE FOR COMPANY */


});
</script>

<?php
			
//include INC_DIR.'footer.php'; // Footer and scripts ?>


<?php include INC_DIR.'bottom.php'; // Close body and html tags ?>