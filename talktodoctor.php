<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email') ==""   && $sessionObj->read("front_user_id") =="" && $sessionObj->read("front_login_as") =="" )
{
	header("Location:page_login.php");
}
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'TALKTODOCTOR';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <script src="https://cdn.ckeditor.com/4.6.1/standard-all/ckeditor.js"></script>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo SITE_URL; ?>assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="<?php echo SECURE ?>://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='<?php echo SECURE ?>://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <?php require('common/header.php'); ?>
</head>

<body>

    <div class="wrapper">

        <?php require("common/sidebar.php"); ?>


        <div class="main-panel">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">

                        <div class=" col-md-1 navbar-minimize">

                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <?php require("common/notifications.php"); ?>
                                <?php require("common/topsettings.php"); ?>

                            </ul>

                        </div>
                    </div>

                </div>

            </nav>

            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <div class="header-intro">
                                    <img class="img-responsive" src="assets/img/telemedicine-icon.png" style="
    margin: 0 auto;
" />
                                    <h1>RISE telemedicine</h1>
                                    <p>This feature is not available yet. Soon you will be able to talk with a doctor remotely and get the best guidance in following the RISE method. </p>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
            <!-- container fluid    -->

            <?php //require("common/footer.php"); ?>

        </div>
    </div>


</body>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>

</html>
