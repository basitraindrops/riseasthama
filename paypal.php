<?php 

include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email') !="")
{
    header("Location:dashboard.php");
}
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

require(dirname(__FILE__) . '/init.php');
require_once CLASS_DIR.'front_user_patient.class.php';
if($_POST['age_myself'] !='')
{
    $age=$_POST['age_myself'];
}
if($_POST['asthma_time_myself'] !='')
{
    $asthma_time=$_POST['asthma_time_myself'];
}
if($_POST['allergy_myself'] !='')
{
    $allergy=$_POST['allergy_myself'];
}
if($_POST['age'] !='')
{
    $age=$_POST['age'];
}
if($_POST['asthma_time'] !='')
{
    $asthma_time=$_POST['asthma_time'];
}
if($_POST['allergy'] !='')
{
    $allergy=$_POST['allergy'];
}
if($_POST){

    $up=new front_user_patient($mysqli);
    $encryptObj = new encryption(); 
    $insert = $up->insert($user_nama=$_POST['inputname'], $_POST['email'], $user_name=$_POST['inputname'], $user_address='', $province_id=0, $kota_id=0, $no_telp='', $no_hp='', $no_npwp='', $no_ktp_sim='', $tempat_lahir='', $tanggal_lahir=date("Y-m-d H:i:s"), $jenis_kelamin='', $pendidikan=0, $pekerjaan=0, $nama_perusahaan='', $jabatan='', $user_password=$encryptObj->encrypt_password($_POST['email']), $forgot_password_key='', $forgot_status=0, $payment_register_type='1', $age=$age, $country='', $status=1, $created_by=$_POST['email'],$user_role = 'patient',$asthma_time=$asthma_time,$allergy=$allergy);
    
	$user_id=$up->conn->insert_id; 
}

$paypal_url='https://www.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id='andrew4cr@gmail.com'; // Business email ID
/*$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id='raindropsinfotechbiz@gmail.com';*/// Business email ID*/

if($_REQUEST['item_number'] != ''){
    
    $item_no            = $_REQUEST['item_number'];
    $item_transaction   = $_REQUEST['tx']; // Paypal transaction ID
    $item_price         = $_REQUEST['amt']; // Paypal received amount
    $item_currency      = $_REQUEST['cc']; // Paypal received currency type
    //Rechecking the product price and currency details
    if($_REQUEST)
    {
        require_once CLASS_DIR.'payment.class.php';
        $p=new payment($mysqli);
        $p->insert($item_no, $amount=49.00, $payment_type='First Time Register - Paypal', $created_by=$item_no);
        $settings=new settings($mysqli);
        $settings->get_by_id(4);
        $up = new front_user_patient($mysqli);
        echo $item_no;
        
        if($up->get_by_user_id($item_no))
        {
            $sessionObj->write('front_user_email',encrypt($up->user_email,$settings->salt_key));
            $sessionObj->write('front_user_id',encrypt($up->user_id,$settings->salt_key));
            $sessionObj->write('front_login_as',"patient");
            if(isset($_POST['remember_me']))
            {
                if($_POST['remember_me']=="Yes")
                {
                    setcookie('front_email', encrypt($up->user_email,$settings->salt_key), time() + (86400 * 30), sys_get_temp_dir());
                    setcookie('front_login_as', "patient", time() + (86400 * 30), sys_get_temp_dir());;
                }
            }
            
            require 'inc/mailer/PHPMailerAutoload.php';

            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            //Set who the message is to be sent from
            $mail->setFrom(ADMIN_EMAIL, 'hope@riseasthma.com');
            //Set an alternative reply-to address
            $mail->addReplyTo(ADMIN_EMAIL, 'hope@riseasthma.com');
            //Set who the message is to be sent to
            $mail->addAddress($up->user_email, $up->user_nama);
            //Set the subject line
            $mail->Subject = 'Welcome to RISE Asthma';
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            if($age !='' && $allergy !='' && $asthma_time!='')
            {
                $message = 'Hi '. $up->user_nama.",<br/><br/>";
                        
                $message .= 'Thank you so much for registering. We are thrilled to assist you in your asthma-free journey.<br/>';
                $message .= 'You can login into our platform by going to http://www.riseasthma.com and click on "login" or by simply accessing https://asthmamethod.com/page_login.php.<br/><br/>';
                $message.= "Your login credentials are:<br/><br/>Email : ".$up->user_email."<br/>Password : ".$up->user_email."<br/><br/>";

                $message.= "Age is:".$age."<br/>How many years have they been having asthma:".$asthma_time."<br>Do they have any breathing related allergy:".$allergy."<br/><br/>";

                $message.='We strongly advise you to change your password from the settings menu.<br/><br/>';
                $message.='For any questions, feel free to reply to this email or login to RISE and use the live chat function.<br/><br/>';
                $message.= "Thank you,<br/>";
                $message.= "Cristian Andrei Andriesei<br/>";
                $message.= "RISE Asthma";
            }
            else
            {
                $message = 'Hi '. $up->user_nama.",<br/><br/>";
                $message.= "Thank you so much for registering. We are thrilled to assist you in your asthma-free journey.<br/>";
                $message.= 'You can login into our platform by going to <a href="http://www.riseasthma.com">http://www.riseasthma.com</a> and click on "login" or by simply accessing <a href="https://asthmamethod.com/page_login.php">https://asthmamethod.com/page_login.php</a>.<br/><br/>';
                $message.= 'Your login credentials are:<br/><br/>';
                $message.= "Email : ".$up->user_email."<br/>Password : ".$up->user_email."<br/><br/>";
                $message.= "We strongly advise you to change your password from the settings menu.<br/><br/>";
                $message.= "For any questions, feel free to reply to this email or login to RISE and use the live chat function.<br/><br/>";
                $message.= "Thank you,<br/>";
                $message.= "Cristian Andrei Andriesei<br/>";
                $message.= "RISE Asthma";
            }
            $mail->msgHTML($message);
            //Replace the plain text body with one created manually
            $mail->AltBody = 'Email from asthmamethod.com';
            //Attach an image file
            //$mail->addAttachment('images/phpmailer_mini.png');
            $mail->isSendmail();
            $mail->Sendmail = '/usr/sbin/sendmail';
            //$mail->isSMTP(); 

            //send the message, check for errors
            if (!$mail->send()) {
               // die( "Mailer Error: " . $mail->ErrorInfo);
                // echo "<pre>";
                // print_r($up);
                // echo "</pre>";
                // die("adsfa");
            }


            
            /*auto login page after registration raindrops*/
            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            //Set who the message is to be sent from
            $mail->setFrom(ADMIN_EMAIL, 'hope@riseasthma.com');
            //Set an alternative reply-to address
            $mail->addReplyTo(ADMIN_EMAIL, 'hope@riseasthma.com');
            //Set who the message is to be sent to
            $mail->addAddress('hope@riseasthma.com');
            //Set the subject line
            $mail->Subject = 'New User Registered in asthmamethod.com';
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $message = "Hi <br/>";
            $message.= "A new user have been registered into asthmamethod.com named ".$up->user_nama.".<br/> IF you want to contact user you can mail on ".$up->user_email.".<br/> </br> Thank you";
            $message.= "Regards<br/><br/>";
            $message.= "Web Admin";
            $mail->msgHTML($message);
            //Replace the plain text body with one created manually
            $mail->AltBody = 'Email from asthmamethod.com';
            //Attach an image file
            //$mail->addAttachment('images/phpmailer_mini.png');
            $mail->isSendmail();
            $mail->Sendmail = '/usr/sbin/sendmail';
            //$mail->isSMTP();

            //send the message, check for errors
            if (!$mail->send()) {
               // die( "Mailer Error: " . $mail->ErrorInfo);
                // echo "<pre>";
                // print_r($up);
                // echo "</pre>";
                // die("adsfa");
            }
            header("Location:intro.php");
        }else{
            exit('Some problem please contact admin');
        }
        

    }
    else
    {
        echo "<h1>Payment Failed</h1>";
    } 
}else{
?>

    <form action="<?php echo $paypal_url; ?>" method="post" name="frmPayPal1">
    <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="item_name" value="Rise Asthma Method Payment">
    <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
    <input type="hidden" name="userid" value="<?php echo $user_id;?>">
    <input type="hidden" name="amount" value="49">
    <input type="hidden" name="credits" value="510">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="handling" value="0">
    <input type="hidden" name="cancel_return" value="http://192.168.1.33/corephp/riseasthama/index.php">
    <input type="hidden" name="return" value="http://192.168.1.33/corephp/riseasthama/paypal.php">
    <input type="submit" style="display:none" id="submit" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script type="text/javascript">
 $( document ).ready(function() {
    $('#submit').trigger('click');
 });
 </script>      

<?php } ?>