<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')!=""  && $sessionObj->read("front_user_id") !="" && $sessionObj->read("front_login_as") !="" )
{
	header("Location:dashboard.php");
}


require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

//session_start();
//include("simple-php-captcha.php");
//$_SESSION['captcha'] = simple_php_captcha();

/* new password
$enc = new encryption();
echo $enc->encrypt_password("12345678");
*/

//echo $sessionObj->read('captcha_code');
//echo date("Y-m-d H++7:i:s");
$error_message="";
$status = true;
//$captcha = @$_POST['captcha']; // the user's entry for the captcha code

//echo $captcha."xx";
// Only try to validate the captcha if the form has no errors
// This is especially important for ajax calls

	if(isset($_POST['front_login_email'])&& isset($_POST['front_login_password']))
	{
		if($_POST['front_login_email']=="")
		{
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please Fill your Email!
							</div>';
							$status = false;
		}
		if($_POST['front_login_password']=="")
		{
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please Fill your Password!
							</div>';
							$status = false;
		}
		//echo $_POST['captcha_code']. " ".$sessionObj->read('captcha_code') ;
/*		if(strcasecmp($sessionObj->read('captcha_code'), $_POST['front_captcha_code']) != 0){  
			$error_message.='<div class="alert alert-danger">
								<i class="fa fa-bell-o"></i>  Please enter correct captcha!
							</div>';
							$status = false;
		}else{// Captcha verification is Correct. Final Code Execute here!		
		}

*/		if($status == true)
		{
			$login_as=$_POST['front_login_as'];
/*			if($login_as=="patient")
			{
 * 
 * */
 
 
					require_once CLASS_DIR.'front_user_patient.class.php';
					require_once(CLASS_DIR.'app_setting.class.php');
					require_once(INC_DIR."functions.php");
					$settings=new settings($mysqli);
					$settings->get_by_id(4);
					$user_obj = new front_user_patient($mysqli);
					if($user_obj->loginby_email_password($_POST['front_login_email'],$_POST['front_login_password']))
					{
						$sessionObj->write('front_user_email',encrypt($user_obj->user_email,$settings->salt_key));
						$sessionObj->write('front_user_id',encrypt($user_obj->user_id,$settings->salt_key));
						$sessionObj->write('front_login_as',"patient");
						if(isset($_POST['remember_me']))
						{
							if($_POST['remember_me']=="Yes")
							{
								setcookie('front_email', encrypt($user_obj->user_email,$settings->salt_key), time() + (86400 * 30), sys_get_temp_dir());
								setcookie('front_login_as', "patient", time() + (86400 * 30), sys_get_temp_dir());;
							}
						}
						header("Location:dashboard.php");
					}
					else
					{
						$error_message.='<div class="alert alert-danger">
											<i class="fa fa-bell-o"></i>  Username or Password wrong!
										</div>';
										echo $_POST['front_login_password'];
										$status = false;

					}
/*			}
			elseif($login_as=="doctor")
			{
					require_once CLASS_DIR.'front_user_doctor.class.php';
					require_once(CLASS_DIR.'app_setting.class.php');
					require_once(INC_DIR."functions.php");
					$settings=new settings($mysqli);
					$settings->get_by_id(4);
					$user_obj = new front_user_doctor($mysqli);
					if($user_obj->loginby_email_password($_POST['front_login_email'],$_POST['front_login_password']))
					{
						$sessionObj->write('front_user_email',encrypt($user_obj->user_email,$settings->salt_key));
						$sessionObj->write('front_user_id',encrypt($user_obj->user_id,$settings->salt_key));
						$sessionObj->write('front_login_as',"doctor");
						if(isset($_POST['remember_me']))
						{
							if($_POST['remember_me']=="Yes")
							{
								setcookie('front_email', encrypt($user_obj->user_email,$settings->salt_key), time() + (86400 * 30), sys_get_temp_dir());
								setcookie('front_login_as', "doctor", time() + (86400 * 30), sys_get_temp_dir());;
							}
						}
						header("Location:dashboard.php");
					}
					else
					{
						$error_message.='<div class="alert alert-danger">
											<i class="fa fa-bell-o"></i>  Username or Password wrong!
										</div>';
										$status = false;

					}
/*			}
 * 
 */
			
		}
	}

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
    <div class="image-container set-full-height" style="background-image: url('assets/img/loginbackground.png')">
        <!--   Creative Tim Branding   -->

        <div class="logo-container">
            <div class="logo">

            </div>

        </div>


        <!--   Big container   -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form action="page_login.php" method="post">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

                                <div class="wizard-header">

                                    <div class="wizard-navigation">
                                        <div class="progress-with-circle">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                        </div>
                                        <ul>

                                            <li>
                                                <a href="#details" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-briefcase"></i>
                                                    </div>

                                                </a>
                                            </li>

                                        </ul>

                                    </div>

                                </div>


                                <div class="tab-content">


                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="info-text">Welcome back to RISE</h2>
                                            </div>
                                            <div class="col-sm-10 col-sm-offset-1">

                                                <div class="form-group">
                                                <?php if($error_message!="") { echo $error_message; }?>
                                                    <label>E-MAIL ADDRESS</label>
                                                    <input type="text" class="form-control" name="front_login_email" id="exampleInputEmail1" placeholder="e.g john@gmail.com">
                                                    <label>PASSWORD</label>
                                                    <input type="password" class="form-control" name="front_login_password" id="exampleInputEmail1" placeholder="Enter password here">
<!--
                                                    <label>Please confirm You're Human</label><br/>
						     <img src="captcha.php?rand=<?php echo rand();?>" id='captchaimg'><br/>
        Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh.</td>

                            <input type="text" id="captcha_code" name="front_captcha_code" placeholder="Prove you're human.." class="form-control" autocomplete="off">
                            <label>Login As</label>
							<select name="front_login_as" id="front_login_as" class="form-control" style="width:100%;">
							<option value="patient">Login as User</option>
							<option value="doctor">Login as Doctor</option>
							</select>
							
							
						-->
                                                        <input type="checkbox" name="remember_me">&nbsp;&nbsp;Remember Me <br/>
                                                    <div class="footer text-center">
                                                        <button type="submit" class="btn btn-fill btn-warning btn-wd" onclick="$(this).prop( "disabled", true );">Login</button>
                                                        <div class="text-right"><br/><br/><a href="forgotpass.php">Forgot Password</a></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>



                                </div>


                            </form>
                        </div>
                    </div>
                    <!-- wizard container -->
                </div>
            </div>
            <!-- row -->
        </div>
        <!--  big container -->

        <div class="footer">
            <div class="container text-center">
                Don't have an account? Sign up <a href="http://www.creative-tim.com/product/paper-bootstrap-wizard">here.</a>
            </div>
            <div>
                <img src="assets/img/logo.png" class="img-responsive logo-footer">
            </div>
        </div>
    </div>

</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>

</html>
<?php
$mysqli->close();
?>