

<script src="http://192.168.1.37/riseshank/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="http://192.168.1.37/riseshank/assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://192.168.1.37/riseshank/assets/js/bootstrap.min.js" type="text/javascript"></script>

<script src="http://192.168.1.37/riseshank/assets/js/client.js" type="text/javascript"></script>
<div class="tab-pane active" id="fullchapter">
    <div class="card">
        <div class="scroll first" data-type="1">
            <div class="header">
                <h1>Reduce Inflammation with Omega 3</h1>
                <p>In this chapter, we are going to explore the benefits of Omega 3 fatty acids on asthma and allergies. We are going to see why fats are essential for us, what the Omega 3 to Omega 6 fatty acids imbalance is and how to simply increase Omega 3 intake and decrease the ingestion of Omega 6.</p>
            </div>

            <div class="video-container">

                <iframe id="vzvd-10494987" name="vzvd-10494987" src="https://view.vzaar.com/10494987/player?apiOn=true" ></iframe>
                <div  id="play_button">aaaaa</div>
                <!--<iframe id="vzvd-10494987" name="vzvd-10494987" title="vzaar video player" type="text/html" frameborder="0" allowFullScreen allowTransparency="true" mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/10494987/player?apiOn=true"></iframe>-->

            </div>
            <div class="nuttshell">
                <div class="nutshell-content">

                    <h2><span style="font-weight: 400;">In a Nutshell</span></h2>

                    <p>To accommodate the high energy demands of our large brains, early humans changed their diet to one that contained natural fats. Gradually, fats became a crucial element in forming the main structures of our central nervous system and providing the building blocks for the immune system. While living in nature provided us with a perfect balance between different kind of fats, the modern lifestyle has totally changed things. The Omega 3 to Omega 6 fatty acids ratio that used to be 1:1 is nowadays somewhere between 1:20 to 1:40. This imbalance is causing multiple health disorders and negatively affects our asthma symptoms. To correct this imbalance, we have to increase the Omega 3 intake and decrease the ingestion of Omega 6.
                    </p>

                </div>


                <div class="do">
                    <div class="dodont-content">

                        <h3>Increase Your Omega 3 Intake</h3>

                        <ul class="general-list">
                            <li>Take 3 to 4 g of a high quality Omega 3 fish oil per day if you don’t make any changes to the Omega 6 intake</li>
                            <li>Take 1 g of Omega 3 fish oil per day if you reduce your Omega 6 intake;</li>

                        </ul>
                    </div>
                </div>
                <div class="dont">
                    <div class="dodont-content">

                        <h3>Decrease Your Omega 6 Intake</h3>

                        <ul class="general-list">
                            <li>Avoid processed seed and vegetable oils, as well as all the processed foods that contain them. Sunflower, Corn, Soybean and Cottonseed oils are by far the worst. Better options are coconut oil, butter, or olive oil.</li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="scroll" data-type="2">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Nature’s Most Beautiful Creation</span></h2>

                <p><span style="font-weight: 400;">Nature has given you one of the most intricate and beautiful things it has ever created: your brain.</span></p>
                <p><span style="font-weight: 400;">The brain empowers you with reason; it controls your vital systems, your emotions, and memories. It fired our progress as a civilization, and it is the source of humanity&rsquo;s sublime arts - music, painting, poetry, and others.</span></p>
                <p><span style="font-weight: 400;">How did we evolve to be Sapiens ( Latin for &ldquo;Wise&rdquo;)?</span></p>
                <p><span style="font-weight: 400;">We don&rsquo;t know all the answers, as they are buried deep in our long evolutionary process, but we do know something astounding: in the average adult human, the brain represents about 2% of the body weight, but it accounts for 20% of the energy consumed </span><a href="https://paperpile.com/c/NNEtV5/HB2m"><span style="font-weight: 400;">[1]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">To accommodate the high energy demands of our large cerebrum, early humans had changed their diet to one that was three to four times denser in energy than those of our primate kin </span><a href="https://paperpile.com/c/NNEtV5/o5Ma"><span style="font-weight: 400;">[2]</span></a><span style="font-weight: 400;">. This new diet contained natural fats. This is one of the perks that we received from starting to hunt for wild fish and game.</span></p>
                <p><span style="font-weight: 400;">Gradually, our bodies evolved to employ fat not only as fuel but also for many other vital functions. They are forming the main structures of the brain, central nervous system and cell membranes, and provide the building blocks of our immune system </span><a href="https://paperpile.com/c/NNEtV5/t6or"><span style="font-weight: 400;">[3]</span></a> <a href="https://paperpile.com/c/NNEtV5/Dux1"><span style="font-weight: 400;">[4]</span></a> <a href="https://paperpile.com/c/NNEtV5/uYLi"><span style="font-weight: 400;">[5]</span></a><span style="font-weight: 400;">. They help absorb certain nutrients such Vitamins A, D, E and K and </span><span style="font-weight: 400;">antioxidants </span><a href="https://paperpile.com/c/NNEtV5/8twa"><span style="font-weight: 400;">[6]</span></a> <a href="https://paperpile.com/c/NNEtV5/5OrI"><span style="font-weight: 400;">[7]</span></a><span style="font-weight: 400;">. And, like </span><span style="font-weight: 400;">Vitamin D</span><span style="font-weight: 400;">, fats became a cornerstone of our body </span><a href="https://paperpile.com/c/NNEtV5/wqgt"><span style="font-weight: 400;">[8]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>


                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">To sustain the higher demand of energy required by the evolution of our brain, early humans had to look for new sources of nutrients. Fats started to fill in this significant role and gradually became key elements in the optimal functioning of our body.  </span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/1.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="3">

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Different Kinds of Fuel</span></h2>
                <p><span style="font-weight: 400;">The animals and plants that we hunted and gathered provided us with two major kind of fats: saturated (solid at room temperature) and unsaturated (liquid at room temperature).</span></p>
                <p><span style="font-weight: 400;">Without getting too scientific, unsaturated fats are divided (based on their chemical composition) into monounsaturated and polyunsaturated fats. Among the later, some are essentials fats (the body cannot synthesize them) such as Omega 3 and Omega 6. Others are nonessentials, like Omega 5 and Omega 9.</span></p>
                <p><span style="font-weight: 400;">The advantage of living in nature is that it supplied us with the perfect ratio between these types of fat. For example, we have evolved consuming an Omega 3 to Omega 6 fats ratio of somewhere around 2 to 1 </span><a href="https://paperpile.com/c/NNEtV5/2rik"><span style="font-weight: 400;">[9]</span></a><span style="font-weight: 400;">. ack then, we were in excellent health and didn&rsquo;t suffer from the chronic diseases that are currently killing Westerners by the millions </span><a href="https://paperpile.com/c/NNEtV5/5hxK"><span style="font-weight: 400;">[10]</span></a><span style="font-weight: 400;">. You can call this ratio nature&rsquo;s fat yin yang </span><a href="https://paperpile.com/c/NNEtV5/qqpc"><span style="font-weight: 400;">[11]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">However, as is usually the case in human history, we took this perfect functioning organism - the human body- &nbsp;and messed it up big time. Starting with the </span><span style="font-weight: 400;">Agricultural Revolution</span><span style="font-weight: 400;">, our lifestyle and diet changed drastically. The natural food intake equilibrium that we have maintained for millions of years became disrupted, resulting in various health disorders </span><a href="https://paperpile.com/c/NNEtV5/jcwW"><span style="font-weight: 400;">[12]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">One type of fat that took the biggest blow, due to these lifestyle changes, is the one that plays the most important role in maintaining our general health: &nbsp;</span><strong>the Omega 3 fatty acid. </strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><em><span style="font-weight: 400;">Humans have evolved from consuming a perfect natural ratio between multiple types of fats. In the last 10,000 years, in particular, the modern lifestyle disturbed the equilibrium leading to low levels of Omega-3 fatty acid in comparison to the rest of lipids. </span></em></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/2.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">The Natural Made Spaceship</span></h2>

                <p><span style="font-weight: 400;">To put things in perspective, let&rsquo;s think of our body as a spaceship, a magnificent piece of engineering designed to explore distant galaxies.</span></p>
                <p><span style="font-weight: 400;">To fuel this space shuttle, engineers use a mix of different types of liquid propellants, all playing an important role in powering the engines and keeping the command center running.</span></p>
                <p><span style="font-weight: 400;">Also, the quantity of fuel has to be perfectly correlated to ensure there is enough for launch, travel to Andromeda, and then back to Earth safely.</span></p>
                <p><span style="font-weight: 400;">What would happen if the engineers fail to achieve this right blend of fuel type? The craft might not launch, could get hindered, or cracks might even appear in the inner shell. </span><strong>The same parallel applies to us when the fats (and foods in general) we ingest are not proportionally balanced and do not respect the essential natural ratio </strong><a href="https://paperpile.com/c/NNEtV5/jcwW"><span style="font-weight: 400;">[12]</span></a><strong> .</strong></p>
                <p><span style="font-weight: 400;">What would happen if our spaceship gets filled with a larger quantity of fuel than is required? The ship might get encumbered, experience various technical problems, or even explode.</span><strong>This is identical in humans; when we consume a greater quantity of fats than we burn in our daily activities, they could deposit on our organs and lead to multiple health disorders &nbsp;</strong><a href="https://paperpile.com/c/NNEtV5/6jX9"><span style="font-weight: 400;">[13]</span></a><strong>.</strong></p>
                <p><span style="font-weight: 400;">Even if they are precise to the optimal functioning of our body, the natural fats have been a subject of debate for the last 50 years. Some have vilified them for being at the root of various human diseases. Others praised them as the Holy Grail in every healthy diet. However, as you can see above, it is a matter of context.</span></p>
                <p><span style="font-weight: 400;">Fat is just like water. It is essential for your health, but in large quantities, it may drown you.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">Remember the Omega 3 - Omega 6 natural ratio of 1:2? With the typical western diet, we have changed the ratio to somewhere between 1:20 and 1:50 </span><a href="https://paperpile.com/c/NNEtV5/LcOO"><span style="font-weight: 400;">[14]</span></a><span style="font-weight: 400;">. Researchers around the world have started to link this Omega 3 deficiency to obesity, heart problems, cancer, arthritis, poor memory, dry skin, mood swings or depression, poor circulation, and others </span><a href="https://paperpile.com/c/NNEtV5/1aZI"><span style="font-weight: 400;">[15]</span></a><span style="font-weight: 400;">.</span></p>
                <p><strong><strong>One of these disorders in the one that interests you the most: allergies and asthma </strong><a href="https://paperpile.com/c/NNEtV5/8qOu"><span style="font-weight: 400;">[16]</span></a> <a href="https://paperpile.com/c/NNEtV5/WNmX"><span style="font-weight: 400;">[17]</span></a> <a href="https://paperpile.com/c/NNEtV5/C3WX"><span style="font-weight: 400;">[18]</span></a><strong>. </strong></strong>
                </p>

                <p><span style="font-weight: 400;">Bottom line</span></p>
                <p><strong><em><span style="font-weight: 400;">Even if fats are imperative for our general health, intaking a disproportionate ratio of multiple fat types and ingesting a larger quantity than required in the burning process could lead to various health problems. Asthma is one of these disorders. </span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/3.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="4">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Omega 3 for Asthma and Allergies</span></h2>

                <p><span style="font-weight: 400;">The story of Omega 3 and asthma begins in 1970 when researchers first connected essential fatty acid imbalance to autoimmune diseases. A Danish study concluded &nbsp;Greenland Eskimos, consuming diets high in omega-3 fatty acids, rarely suffered from asthma and other disorders associated with excess inflammation </span><a href="https://paperpile.com/c/NNEtV5/6pwA"><span style="font-weight: 400;">[19]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Since then, a body of international research has begun exploring this connection in depth.</span></p>
                <p><span style="font-weight: 400;">There are now three (known) related links between Omega 3, allergies, and asthma.</span></p>
                <p><strong>First, Omega 3 exercises a direct influence on our immune system.</strong></p>
                <p><span style="font-weight: 400;">Researchers at the University of Copenhagen followed 700 women from the 24th week of pregnancy until their children were five years old. According to the results observed, only 17% of children whose mothers had received Omega 3 fish oil were troubled by a persistent wheeze or asthma, as opposed to 24% in the control group </span><a href="https://paperpile.com/c/NNEtV5/yUAI"><span style="font-weight: 400;">[20]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">According to a study in the Medical Journal of Australia, among 468 children, those who ate one serving of Omega 3 rich fish per week were 25% less likely to have asthma than those who did not eat fish </span><a href="https://paperpile.com/c/NNEtV5/2QVw"><span style="font-weight: 400;">[21]</span></a><span style="font-weight: 400;">. The study found for every four children prone to asthma, two would not get it if they included oily fish in their diet. Another study at the University of Waterloo confirms taking certain omega-3 fatty acid supplements during pregnancy can reduce the risk of childhood asthma by almost one-third </span><a href="https://paperpile.com/c/NNEtV5/4QqY"><span style="font-weight: 400;">[22]</span></a><strong>.</strong></p>
                <p><span style="font-weight: 400;">Also, in a recent one-year study done in France, asthmatics who took about 1 gram of Omega-3 fatty acids per day had greater improvements in breathing and lung capacity than the control group </span><a href="https://paperpile.com/c/NNEtV5/apgc"><span style="font-weight: 400;">[23]</span></a><span style="font-weight: 400;">. </span><strong>&nbsp; &nbsp;&nbsp;</strong>&nbsp;</p>
                <p><strong>Second, Omega-3 plays a crucial role in stress and anger management </strong><a href="https://paperpile.com/c/NNEtV5/n75D"><span style="font-weight: 400;">[24]</span></a> <a href="https://paperpile.com/c/NNEtV5/5Oo8"><span style="font-weight: 400;">[25]</span></a> <a href="https://paperpile.com/c/NNEtV5/Cp6w"><span style="font-weight: 400;">[26]</span></a><strong>.</strong></p>
                <p><span style="font-weight: 400;">As we have discussed in the introduction and will discuss more in Chapter 4 - Manage Stress, stress is a major factor in the appearances and exacerbations of asthma and allergies in humans.</span></p>
                <p><span style="font-weight: 400;">Many studies are linking Omega 3 deficiency to stress.</span></p>
                <p><span style="font-weight: 400;">As far back as 1986, forensic psychiatrist Matti Virkkunen has measured lipid levels in habitually violent and impulsive prison inmates. He did not measure Omega 3s but rather a variety of lipids from the omega-6 category. The observation that omega-6 fatty acid levels were high might suggest that omega-3 levels were low .</span></p>
                <p><span style="font-weight: 400;">In another Japanese study, researchers provided medical students with capsules containing from 1.5 grams to 1.8 grams of the Omega 3. He measured hostility at the start of the experiment and again three months later, immediately before exams. There was a much higher rate of stress and anxiety in the nineteen medical students who received the placebo during the high-stress period in comparison to the ones that took Omega-3 </span><a href="https://paperpile.com/c/NNEtV5/jB1f"><span style="font-weight: 400;">[27]</span></a><span style="font-weight: 400;">. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Also working with medical students, a more recent study gauging the impact of consuming more fish oil showed a marked reduction both in inflammation and, surprisingly, in anxiety. The psychological surveys clearly showed a substantial change in stress; those receiving the omega-3 showed a 20% reduction in anxiety compared to the placebo group </span><a href="https://paperpile.com/c/NNEtV5/jB1f"><span style="font-weight: 400;">[27]</span></a><span style="font-weight: 400;">.</span>&nbsp;</p>
                <p><span style="font-weight: 400;">I remember when I started to take a few grams of Omega-3 fish oil daily and how big of an impact it had on my general mood. I could focus better and had a deeper sense of presence and consciousness that conveyed calm and happiness.</span></p>
                <p><strong>And third, Omega 3 helps plug the &ldquo;leaky&rdquo; gut.</strong></p>
                <p><span style="font-weight: 400;">Do you remember the &ldquo;leaky&rdquo; gut - the intestinal disorder related to the inception and aggravations of asthma and allergies in our species?</span></p>
                <p><span style="font-weight: 400;">Along with Vitamin D and a healthy diet, there is substantial evidence that Omega 3 positively affects the right bacteria in our intestines and might help plug the increased intestinal permeability </span><a href="https://paperpile.com/c/NNEtV5/drUC"><span style="font-weight: 400;">[28]</span></a></p>
                <p><strong><span style="font-weight: 400;">According to a British study published in 2012, the Omega-3 fatty acid may help to support the growth and tolerance of probiotic bacterial strains in the human gut. This research shows the potential of combining Omega-3 with bacteria to promote probiotic survival in the gut and dampening the inflammatory response </span><a href="https://paperpile.com/c/NNEtV5/3nMV"><span style="font-weight: 400;">[29]</span></a><span style="font-weight: 400;">. </span></strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p>Compelling evidence from multiple sources connect Omega 3 with asthma in three related aspects: direct influence over the inflammatory response, stress and anxiety reduction, and fixing the “leaky” gut.</p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/4.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="5">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Why do People Think Fats are Bad?</span></h2>

                <p><span style="font-weight: 400;">Back in the mid-20th century, there was a major epidemic of heart disease running rampant in America, determined, in essence, by the modern western lifestyle. However, since this way of life was part of the American Dream, a scapegoat was indispensable. In the end, they found the perfect one: the consumption of fats. &nbsp;</span></p>
                <p><span style="font-weight: 400;">Notwithstanding, at the time, the claim that the fats are associated with heart diseases was not based on any experimental evidence in humans. This hypothesis relied only on assumptions, observational data and animal studies </span><a href="https://paperpile.com/c/NNEtV5/ODDJ"><span style="font-weight: 400;">[30]</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">The diet-heart hypothesis turned into public policy in 1977, before it was ever confirmed by solid scientific backup </span><a href="https://paperpile.com/c/NNEtV5/BaFw"><span style="font-weight: 400;">[31]</span></a><span style="font-weight: 400;">. </span></p>
                <p><span style="font-weight: 400;">The above might sound like just another conspiracy theory (which I am not a big fan of), but the facts don&rsquo;t lie. Draw your own conclusions. </span></p>
                <p><span style="font-weight: 400;">Once more, the problem with fats relates with the quantity we ingest in proportion to our lifestyle and the consumed ratio between the multiple fat types. </span></p>
                <p><strong><span style="font-weight: 400;">There is, nevertheless, a particular kind of fat that indeed raises the risk of heart diseases directly: the trans fats. Trans fats are monounsaturated found mainly in highly refined foods to increase their shelf life </span><a href="https://paperpile.com/c/NNEtV5/m2gM+un4A"><span style="font-weight: 400;">[32], [33]</span></a><span style="font-weight: 400;">. You can stay away from them by just avoiding junk food.</span></strong></p>

                <p><span style="font-weight: 400;">Bottom Line</span></p>
                <p><strong><em><span style="font-weight: 400;">The claim that the consumption of fats is associated with heart diseases lacks both empirical evidence and context.</span></em></strong></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/5.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Omega 3 - Taking Action</span></h2>

                <p><span style="font-weight: 400;">Reducing your asthma symptoms by optimizing the Omega 3 ingestion is almost effortless and comes with invaluable rewards.</span></p>
                <p><span style="font-weight: 400;">However, the important thing to remember is any recommendation for &nbsp;Omega 3 intake that does not take the background of Omega 6 consumption into account is wholly inadequate. So, we have to address both.</span></p>


                <h3><span style="font-weight: 400;">How to Increase Your Omega 3 Intake:</span></h3>

                <p><span style="font-weight: 400;">The simplest way to increase your Omega 3 intake is from fish oil supplements (the other option is through diet). &nbsp;</span></p>
                <p><span style="font-weight: 400;">Fish oil supplements are varying in quality. There is a substantial difference in the ingredients and therapeutic benefit of what is available today. Various important factors to consider are purity, freshness, potency, sustainability, cost, etc.</span></p>
                <p><span style="font-weight: 400;">Also, most fish oil supplements found in health food stores and pharmacies contain only 30% of Omega 3 fatty acids. The remaining 70% is mainly Omega 9 fatty acids, some Omega 6 fatty acids, cholesterol, and other lipids.</span></p>
                <p><strong>The best approach is to always choose the highest quality fish oil supplement that you can afford.</strong></p>
                <p><a href="https://chriskresser.com/the-definitive-fish-oil-buyers-guide/"><span style="font-weight: 400;">Here</span></a><span style="font-weight: 400;"> is a detailed guide on fish oil supplements.</span></p>

                <ul class="general-list">
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Take </span><strong>3 to 4 g</strong><span style="font-weight: 400;"> of Omega 3 fish oil per day if you don&rsquo;t make any changes to the Omega 6 intake;</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Take </span><strong>1 g</strong><span style="font-weight: 400;"> of Omega 3 fish oil per day if you reduce your Omega 6 intake;</span></li>
                </ul>

                <br/>


                <h3><span style="font-weight: 400;">Decrease Your Omega 6 Intake</span></h3>

                <p><span style="font-weight: 400;">The single most important thing you can do to reduce your Omega 6 intake is to avoid processed seed and vegetable oils high in Omega 6, as well as all the processed foods that contain them. Sunflower, Corn, Soybean and Cottonseed oils are by far the worst. </span></p>
                <p><span style="font-weight: 400;">The problem with these industrial seeds and vegetable oils is that they are highly refined products and are way too rich in Omega 6 fatty acids. They oxidize or go </span><span style="font-weight: 400;">rancid</span><span style="font-weight: 400;"> quickly. When oils undergo oxidation, they react with oxygen to form free radicals and other harmful compounds to our health. Not only should you not cook with them, but it is best to avoid them altogether.</span></p>
                <p><span style="font-weight: 400;">As an alternative, when it comes to high heat cooking, coconut oil is the wisest choice. This oil is semi-solid at room temperature, and it can last for months and years without going rancid. Butter and olive oil come next.</span></p>
                <p><span style="font-weight: 400;">On a side note, nuts and seeds are pretty high in Omega 6, but they are whole foods that have plenty of health benefits. They are fine to eat as long as if you are eating adequate amounts of Omega 3 fatty acids.</span></p>
            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/6.png">
            <!-- image    -->
        </div>
        <div class="scroll" data-type="6">
            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">For Children</span></h2>
                <p><span style="font-weight: 400;">First, a pure, high-quality Omega 3 fish oil should be included in a woman&rsquo;s supplement regimen before and during pregnancy, as well as during </span><a href="http://americanpregnancy.org/first-year-of-life/breastfeeding-overview/"><span style="font-weight: 400;">breastfeeding</span></a><span style="font-weight: 400;">.</span></p>
                <p><span style="font-weight: 400;">Children can have anywhere from 500 to 1,200 milligrams of fish oil per day, depending on their age, height, and weight. They can take fish oil capsules straight, or you can mix it in with their food.</span></p>
                <p><span style="font-weight: 400;">Please consult with your pediatrician before giving your children Omega 3 supplements.</span></p>
                <p><span style="font-weight: 400;">As far as Omega 6 is concerned, the same principles apply. Don&rsquo;t feed your kids fried foods in vegetable seed oils. </span></p>

            </div>
            <!-- sectiune capitol     -->

            <img class="img-responsive" src="assets/img/omega3/7.png">
            <!-- image    -->

            <div class="content content-full-width">
                <h2><span style="font-weight: 400;">Conclusion</span></h2>
                <p><strong><span style="font-weight: 400;">Our body is immensely complex and some of the mechanisms and processes that occur inside us are still a mystery to science, but the more we immerse ourselves in this quest for knowledge, the more we realize that things are connected. Evolution has dictated how our body should function, and Vitamin D, healthy natural fats, the right nutrients, stress management, are all linked and exercise a direct or indirect influence on one another. If we could only optimize this balance, we would be able to harvest the benefits of an asthma and allergy free existence. 
</span></strong></p>
            </div>
            <!-- sectiune capitol     -->
        </div>
        <div class="scroll" data-type="7">

            <div class="content content-full-width">


                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                <a data-target="#collapseOne" href="#" data-toggle="collapse" id="myBtn">
                                                       References (click to open)

                                                        <b class="caret"></b>
                                                    </a>
                            </h1>
                        </div>
                        <!--<div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">

                                <p><span style="font-weight: 400;">[1]</span> <a href="http://paperpile.com/b/NNEtV5/HB2m"><span style="font-weight: 400;">D. D. Clarke and L. Sokoloff, </span><em><span style="font-weight: 400;">Regulation of Cerebral Metabolic Rate</span></em><span style="font-weight: 400;">. Lippincott-Raven, 1999.</span></a></p>
                                <p><span style="font-weight: 400;">[2]</span> <a href="http://paperpile.com/b/NNEtV5/o5Ma"><span style="font-weight: 400;">W. R. Leonard, J. J. Snodgrass, and M. L. Robertson, &ldquo;Evolutionary Perspectives on Fat Ingestion and Metabolism in Humans,&rdquo; in </span><em><span style="font-weight: 400;">Fat Detection: Taste, Texture, and Post Ingestive Effects</span></em><span style="font-weight: 400;">, J.-P. Montmayeur and J. le Coutre, Eds. Boca Raton (FL): CRC Press/Taylor &amp; Francis, 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[3]</span> <a href="http://paperpile.com/b/NNEtV5/t6or"><span style="font-weight: 400;">C.-Y. Chang, D.-S. Ke, and J.-Y. Chen, &ldquo;Essential fatty acids and human brain,&rdquo; </span><em><span style="font-weight: 400;">Acta Neurol. Taiwan.</span></em><span style="font-weight: 400;">, vol. 18, no. 4, pp. 231&ndash;241, Dec. 2009.</span></a></p>
                                <p><span style="font-weight: 400;">[4]</span> <a href="http://paperpile.com/b/NNEtV5/Dux1"><span style="font-weight: 400;">W. J. Morrow, Y. Ohashi, J. Hall, J. Pribnow, S. Hirose, T. Shirai, and J. A. Levy, &ldquo;Dietary fat and immune function. I. Antibody responses, lymphocyte and accessory cell function in (NZB x NZW)F1 mice,&rdquo; </span><em><span style="font-weight: 400;">J. Immunol.</span></em><span style="font-weight: 400;">, vol. 135, no. 6, pp. 3857&ndash;3863, Dec. 1985.</span></a></p>
                                <p><span style="font-weight: 400;">[5]</span> <a href="http://paperpile.com/b/NNEtV5/uYLi"><span style="font-weight: 400;">D. S. Kelley and P. A. Daudu, &ldquo;Fat intake and immune response,&rdquo; </span><em><span style="font-weight: 400;">Prog. Food Nutr. Sci.</span></em><span style="font-weight: 400;">, vol. 17, no. 1, pp. 41&ndash;63, Jan. 1993.</span></a></p>
                                <p><span style="font-weight: 400;">[6]</span> <a href="http://paperpile.com/b/NNEtV5/8twa"><span style="font-weight: 400;">B. Dawson-Hughes, S. S. Harris, A. H. Lichtenstein, G. Dolnikowski, N. J. Palermo, and H. Rasmussen, &ldquo;Dietary fat increases vitamin D-3 absorption,&rdquo; </span><em><span style="font-weight: 400;">J. Acad. Nutr. Diet.</span></em><span style="font-weight: 400;">, vol. 115, no. 2, pp. 225&ndash;230, Feb. 2015.</span></a></p>
                                <p><span style="font-weight: 400;">[7]</span> <a href="http://paperpile.com/b/NNEtV5/5OrI"><span style="font-weight: 400;">J. D. Ribaya-Mercado, &ldquo;Influence of dietary fat on beta-carotene absorption and bioconversion into vitamin A,&rdquo; </span><em><span style="font-weight: 400;">Nutr. Rev.</span></em><span style="font-weight: 400;">, vol. 60, no. 4, pp. 104&ndash;110, Apr. 2002.</span></a></p>
                                <p><span style="font-weight: 400;">[8]</span> <a href="http://paperpile.com/b/NNEtV5/wqgt"><span style="font-weight: 400;">R. Uauy, &ldquo;Dietary fat quality for optimal health and well-being: overview of recommendations,&rdquo; </span><em><span style="font-weight: 400;">Ann. Nutr. Metab.</span></em><span style="font-weight: 400;">, vol. 54 Suppl 1, pp. 2&ndash;7, Jul. 2009.</span></a></p>
                                <p><span style="font-weight: 400;">[9]</span> <a href="http://paperpile.com/b/NNEtV5/2rik"><span style="font-weight: 400;">S. Guyenet and V. my C. Profile, &ldquo;A Practical Approach to Omega Fats.&rdquo; [Online]. Available: </span></a><a href="http://wholehealthsource.blogspot.co.id/2008/09/pracical-approach-to-omega-fats.html"><span style="font-weight: 400;">http://wholehealthsource.blogspot.co.id/2008/09/pracical-approach-to-omega-fats.html</span></a><a href="http://paperpile.com/b/NNEtV5/2rik"><span style="font-weight: 400;">. [Accessed: 26-Jan-2017]</span></a></p>
                                <p><span style="font-weight: 400;">[10]</span> <a href="http://paperpile.com/b/NNEtV5/5hxK"><span style="font-weight: 400;">M. P. Richards, &ldquo;A brief review of the archaeological evidence for Palaeolithic and Neolithic subsistence,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 56, no. 12, p. 16 p following 1262, Dec. 2002.</span></a></p>
                                <p><span style="font-weight: 400;">[11]</span> <a href="http://paperpile.com/b/NNEtV5/qqpc"><span style="font-weight: 400;">S. M. Grundy, &ldquo;What is the desirable ratio of saturated, polyunsaturated, and monounsaturated fatty acids in the diet?,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 66, no. 4 Suppl, p. 988S&ndash;990S, Oct. 1997.</span></a></p>
                                <p><span style="font-weight: 400;">[12]</span> <a href="http://paperpile.com/b/NNEtV5/jcwW"><span style="font-weight: 400;">A. P. Simopoulos, &ldquo;The importance of the ratio of omega-6/omega-3 essential fatty acids,&rdquo; </span><em><span style="font-weight: 400;">Biomed. Pharmacother.</span></em><span style="font-weight: 400;">, vol. 56, no. 8, pp. 365&ndash;379, Oct. 2002.</span></a></p>
                                <p><span style="font-weight: 400;">[13]</span> <a href="http://paperpile.com/b/NNEtV5/6jX9"><span style="font-weight: 400;">F. Rosqvist, D. Iggman, J. Kullberg, J. Cedernaes, H.-E. Johansson, A. Larsson, L. Johansson, H. Ahlstr&ouml;m, P. Arner, I. Dahlman, and U. Ris&eacute;rus, &ldquo;Overfeeding polyunsaturated and saturated fat causes distinct effects on liver and visceral fat accumulation in humans,&rdquo; </span><em><span style="font-weight: 400;">Diabetes</span></em><span style="font-weight: 400;">, vol. 63, no. 7, pp. 2356&ndash;2368, Jul. 2014.</span></a></p>
                                <p><span style="font-weight: 400;">[14]</span> <a href="http://paperpile.com/b/NNEtV5/LcOO"><span style="font-weight: 400;">T. L. Blasbalg, J. R. Hibbeln, C. E. Ramsden, S. F. Majchrzak, and R. R. Rawlings, &ldquo;Changes in consumption of omega-3 and omega-6 fatty acids in the United States during the 20th century,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 93, no. 5, pp. 950&ndash;962, May 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[15]</span> <a href="http://paperpile.com/b/NNEtV5/1aZI"><span style="font-weight: 400;">&ldquo;Omega-3 Fatty Acids: An Essential Contribution,&rdquo; </span><em><span style="font-weight: 400;">The Nutrition Source</span></em><span style="font-weight: 400;">, 18-Sep-2012. [Online]. Available: </span></a><a href="https://www.hsph.harvard.edu/nutritionsource/omega-3-fats/"><span style="font-weight: 400;">https://www.hsph.harvard.edu/nutritionsource/omega-3-fats/</span></a><a href="http://paperpile.com/b/NNEtV5/1aZI"><span style="font-weight: 400;">. [Accessed: 27-Jan-2017]</span></a></p>
                                <p><span style="font-weight: 400;">[16]</span> <a href="http://paperpile.com/b/NNEtV5/8qOu"><span style="font-weight: 400;">T. D. Mickleborough, A. A. Ionescu, and K. W. Rundell, &ldquo;Omega-3 Fatty acids and airway hyperresponsiveness in asthma,&rdquo; </span><em><span style="font-weight: 400;">J. Altern. Complement. Med.</span></em><span style="font-weight: 400;">, vol. 10, no. 6, pp. 1067&ndash;1075, Dec. 2004.</span></a></p>
                                <p><span style="font-weight: 400;">[17]</span> <a href="http://paperpile.com/b/NNEtV5/WNmX"><span style="font-weight: 400;">S. Tecklenburg-Lund, T. D. Mickleborough, L. A. Turner, A. D. Fly, J. M. Stager, and G. S. Montgomery, &ldquo;Randomized controlled trial of fish oil and montelukast and their combination on airway inflammation and hyperpnea-induced bronchoconstriction,&rdquo; </span><em><span style="font-weight: 400;">PLoS One</span></em><span style="font-weight: 400;">, vol. 5, no. 10, p. e13487, Oct. 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[18]</span> <a href="http://paperpile.com/b/NNEtV5/C3WX"><span style="font-weight: 400;">J. Miyata and M. Arita, &ldquo;Role of omega-3 fatty acids and their metabolites in asthma and allergic diseases,&rdquo; </span><em><span style="font-weight: 400;">Allergol. Int.</span></em><span style="font-weight: 400;">, vol. 64, no. 1, pp. 27&ndash;34, Jan. 2015.</span></a></p>
                                <p><span style="font-weight: 400;">[19]</span> <a href="http://paperpile.com/b/NNEtV5/6pwA"><span style="font-weight: 400;">J. Dyerberg, H. O. Bang, and N. Hjorne, &ldquo;Fatty acid composition of the plasma lipids in Greenland Eskimos,&rdquo; </span><em><span style="font-weight: 400;">Am. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 28, no. 9, pp. 958&ndash;966, Sep. 1975.</span></a></p>
                                <p><span style="font-weight: 400;">[20]</span> <a href="http://paperpile.com/b/NNEtV5/yUAI"><span style="font-weight: 400;">H. Bisgaard, &ldquo;The Copenhagen Prospective Study on Asthma in Childhood (COPSAC): design, rationale, and baseline data from a longitudinal birth cohort study,&rdquo; </span><em><span style="font-weight: 400;">Ann. Allergy Asthma Immunol.</span></em><span style="font-weight: 400;">, vol. 93, no. 4, pp. 381&ndash;389, Oct. 2004.</span></a></p>
                                <p><span style="font-weight: 400;">[21]</span> <a href="http://paperpile.com/b/NNEtV5/2QVw"><span style="font-weight: 400;">L. Hodge, C. M. Salome, J. K. Peat, M. M. Haby, W. Xuan, and A. J. Woolcock, &ldquo;Consumption of oily fish and childhood asthma risk | Medical Journal of Australia.&rdquo; [Online]. Available: </span></a><a href="https://www.mja.com.au/journal/1996/164/3/consumption-oily-fish-and-childhood-asthma-risk"><span style="font-weight: 400;">https://www.mja.com.au/journal/1996/164/3/consumption-oily-fish-and-childhood-asthma-risk</span></a><a href="http://paperpile.com/b/NNEtV5/2QVw"><span style="font-weight: 400;">. [Accessed: 27-Jan-2017]</span></a></p>
                                <p><span style="font-weight: 400;">[22]</span> <a href="http://paperpile.com/b/NNEtV5/4QqY"><span style="font-weight: 400;">&ldquo;Omega-3 supplements can help prevent childhood asthma, study finds,&rdquo; </span><em><span style="font-weight: 400;">ScienceDaily</span></em><span style="font-weight: 400;">. [Online]. Available: </span></a><a href="https://www.sciencedaily.com/releases/2016/12/161229113451.htm"><span style="font-weight: 400;">https://www.sciencedaily.com/releases/2016/12/161229113451.htm</span></a><a href="http://paperpile.com/b/NNEtV5/4QqY"><span style="font-weight: 400;">. [Accessed: 27-Jan-2017]</span></a></p>
                                <p><span style="font-weight: 400;">[23]</span> <a href="http://paperpile.com/b/NNEtV5/apgc"><span style="font-weight: 400;">T. Brick, Y. Schober, C. B&ouml;cking, J. Pekkanen, J. Genuneit, G. Loss, J.-C. Dalphin, J. Riedler, R. Lauener, W. A. Nockher, H. Renz, O. Vaarala, C. Braun-Fahrl&auml;nder, E. von Mutius, M. J. Ege, P. I. Pfefferle, and PASTURE study group, &ldquo;&omega;-3 fatty acids contribute to the asthma-protective effect of unprocessed cow&rsquo;s milk,&rdquo; </span><em><span style="font-weight: 400;">J. Allergy Clin. Immunol.</span></em><span style="font-weight: 400;">, vol. 137, no. 6, pp. 1699&ndash;1706.e13, Jun. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[24]</span> <a href="http://paperpile.com/b/NNEtV5/n75D"><span style="font-weight: 400;">N. Sinn, C. Milte, and P. R. C. Howe, &ldquo;Oiling the brain: a review of randomized controlled trials of omega-3 fatty acids in psychopathology across the lifespan,&rdquo; </span><em><span style="font-weight: 400;">Nutrients</span></em><span style="font-weight: 400;">, vol. 2, no. 2, pp. 128&ndash;170, Feb. 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[25]</span> <a href="http://paperpile.com/b/NNEtV5/5Oo8"><span style="font-weight: 400;">J. M. Bourre, &ldquo;Dietary omega-3 Fatty acids and psychiatry: mood, behaviour, stress, depression, dementia and aging,&rdquo; </span><em><span style="font-weight: 400;">J. Nutr. Health Aging</span></em><span style="font-weight: 400;">, vol. 9, no. 1, pp. 31&ndash;38, 2005.</span></a></p>
                                <p><span style="font-weight: 400;">[26]</span> <a href="http://paperpile.com/b/NNEtV5/Cp6w"><span style="font-weight: 400;">A. J. Richardson, J. R. Burton, R. P. Sewell, T. F. Spreckelsen, and P. Montgomery, &ldquo;Docosahexaenoic Acid for Reading, Cognition and Behavior in Children Aged 7&ndash;9 Years: A Randomized, Controlled Trial (The DOLAB Study),&rdquo; </span><em><span style="font-weight: 400;">PLoS One</span></em><span style="font-weight: 400;">, vol. 7, no. 9, p. e43909, Sep. 2012.</span></a></p>
                                <p><span style="font-weight: 400;">[27]</span> <a href="http://paperpile.com/b/NNEtV5/jB1f"><span style="font-weight: 400;">J. K. Kiecolt-Glaser, M. A. Belury, R. Andridge, W. B. Malarkey, and R. Glaser, &ldquo;Omega-3 supplementation lowers inflammation and anxiety in medical students: a randomized controlled trial,&rdquo; </span><em><span style="font-weight: 400;">Brain Behav. Immun.</span></em><span style="font-weight: 400;">, vol. 25, no. 8, pp. 1725&ndash;1734, Nov. 2011.</span></a></p>
                                <p><span style="font-weight: 400;">[28]</span> <a href="http://paperpile.com/b/NNEtV5/drUC"><span style="font-weight: 400;">B. S. Noriega, M. A. Sanchez-Gonzalez, D. Salyakina, and J. Coffman, &ldquo;Understanding the Impact of Omega-3 Rich Diet on the Gut Microbiota,&rdquo; </span><em><span style="font-weight: 400;">Case Rep. Med.</span></em><span style="font-weight: 400;">, vol. 2016, p. 3089303, Mar. 2016.</span></a></p>
                                <p><span style="font-weight: 400;">[29]</span> <a href="http://paperpile.com/b/NNEtV5/3nMV"><span style="font-weight: 400;">K. L. Bentley-Hewitt, C. E. De Guzman, J. Ansell, T. Mandimika, A. Narbad, and E. K. Lund, &ldquo;How fish oils could support our friendly bacteria,&rdquo; </span><em><span style="font-weight: 400;">Lipid Technology</span></em><span style="font-weight: 400;">, vol. 27, no. 8, pp. 179&ndash;182, Aug. 2015.</span></a></p>
                                <p><span style="font-weight: 400;">[30]</span> <a href="http://paperpile.com/b/NNEtV5/ODDJ"><span style="font-weight: 400;">S. L. Weinberg, &ldquo;The diet-heart hypothesis: a critique,&rdquo; </span><em><span style="font-weight: 400;">J. Am. Coll. Cardiol.</span></em><span style="font-weight: 400;">, vol. 43, no. 5, pp. 731&ndash;733, Mar. 2004.</span></a></p>
                                <p><span style="font-weight: 400;">[31]</span> <a href="http://paperpile.com/b/NNEtV5/BaFw"><span style="font-weight: 400;">A. H. Hite, R. D. Feinman, G. E. Guzman, M. Satin, P. A. Schoenfeld, and R. J. Wood, &ldquo;In the face of contradictory evidence: report of the Dietary Guidelines for Americans Committee,&rdquo; </span><em><span style="font-weight: 400;">Nutrition</span></em><span style="font-weight: 400;">, vol. 26, no. 10, pp. 915&ndash;924, Oct. 2010.</span></a></p>
                                <p><span style="font-weight: 400;">[32]</span> <a href="http://paperpile.com/b/NNEtV5/m2gM"><span style="font-weight: 400;">D. Mozaffarian and R. Clarke, &ldquo;Quantitative effects on cardiovascular risk factors and coronary heart disease risk of replacing partially hydrogenated vegetable oils with other fats and oils,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 63 Suppl 2, pp. S22&ndash;33, May 2009.</span></a></p>
                                <p><span style="font-weight: 400;">[33]</span> <a href="http://paperpile.com/b/NNEtV5/un4A"><span style="font-weight: 400;">D. Mozaffarian, A. Aro, and W. C. Willett, &ldquo;Health effects of trans-fatty acids: experimental and observational evidence,&rdquo; </span><em><span style="font-weight: 400;">Eur. J. Clin. Nutr.</span></em><span style="font-weight: 400;">, vol. 63 Suppl 2, pp. S5&ndash;21, May 2009.</span></a></p>
                                <h2><br /><br /></h2>


                            </div>
                        </div>-->
                    </div>
                </div>

            </div>
        </div>
        <!-- sectiune capitol     -->
    </div>
    <!--  card     -->
</div>
<!--  tabe    -->

<script type="text/javascript">
    

window.addEventListener("load", function() {

    var vzp = new vzPlayer("vzvd-10494987");
    vzp.play2();

    $(".video-container").mouseenter(function () {            
        vzp.play2();        
    });


    $('#play_button').click(function(){
        vzp.pause();
    })

    $(".video-container").mouseleave(function () {    
        vzp.pause();         
            
    });

    setTimeout(function() {   //calls click event after a certain time
        vzp.pause();             
    }, 10000);
    
    console.log('aaaaaaaaaaa');
})


$( document ).ready(function() {
                    
    
    
});

</script>
