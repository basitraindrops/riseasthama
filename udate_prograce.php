<?php
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'THE_STORY_BEHIND';
$settings=new settings($mysqli);
$settings->get_by_id(4);
$UID =  decrypt($sessionObj->read("front_user_id"),$settings->salt_key);

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
 	
	$dataid = $_POST['dataid'] - 1;
	$articleName = $_POST['page_name'];

	$q = 'SELECT * FROM article_readed AS ar 
		JOIN article AS a ON ar.article_id = a.article_id
		WHERE
		ar.user_id = '.$UID.' && a.article_name = "'.$articleName.'"';

		
	$res = $mysqli->query($q);	
	

	if($res->num_rows > 0){

		$val = mysqli_fetch_assoc($res);		
		if( $val['no_chapter_read'] < $dataid){
			$q = 'UPDATE article_readed AS ar 
				JOIN article AS a ON ar.article_id = a.article_id
				SET ar.no_chapter_read = '.$dataid.'
				WHERE a.article_name = "'.$articleName.'"  && ar.user_id = '.$UID;
			$res = $mysqli->query($q);	
		}
	}else{
		//$q ="INSERT INTO article_readed AS ar "		
		$q = "INSERT INTO `article_readed` (`user_id`, `article_id`, `no_chapter_read`)  SELECT '".$UID."',ar.article_id, '".$dataid."' from `article` AS ar WHERE ar.article_name = '".$articleName."'";

		$res = $mysqli->query($q);			
	}


	//Set user's last read status in database

	$q = 'SELECT * FROM last_read_page AS lr WHERE	lr.user_id = '.$UID;
	$res = $mysqli->query($q);	

	$json = array();
	$json['articleName'] = $articleName;
	$json['readAt'] = $dataid;
	$json = json_encode($json);

	if($res->num_rows > 0){
		$date = new DateTime();
		$date  = $date->format('Y-m-d H:i:s');

		//$val = mysqli_fetch_assoc($res);						\			
		$q = 'UPDATE last_read_page AS lr SET lr.last_read_date = "'.$date.'" , lr.page_detail = \''.$json.'\'  WHERE lr.user_id = '.$UID;
		
		$res = $mysqli->query($q);	
			
	}else{
		//$q ="INSERT INTO article_readed AS ar "		
		$q = "INSERT INTO `last_read_page` (`user_id`, `page_detail`) VALUES (".$UID.",'".$json."') ";
		$res = $mysqli->query($q);			
	}

	echo 'update';
}