<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email')=="" && $sessionObj->read("front_user_id")=="" )
{
    header("Location:page_login.php");
}


require_once(ROOT_DIR."/admin/inc/class/front_quote.class.php");

$pb=new front_quote($mysqli);
$quoteToShow = $pb->getRandomQuote();

require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'DASHBOARD';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo SITE_URL; ?>assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <?php require('common/header.php'); ?>

</head>

<body>

    <div class="wrapper wrapper-full-page intro-wrapper">
        <div class="intro-top-wrapper">
            <div class="intro-header">

                <h1> Welcome Back
                    <?php echo strtoupper(isset($user_obj->user_name)?$user_obj->user_name:'');?>!</h1>
                <h3>
                    <?php echo $quoteToShow; ?> </h3><br/>
                <a href="<?php echo isset($pageContinue)?$pageContinue:'thestory.php'; ?>"><button type="submit" class="btn btn-fill btn-warning btn-wd" action="/index.php" >Continue learning</button>    </a>
            </div>

        </div>
        <div class="col-md-6 col-md-offset-3 intro-intro">
            <br/>
            <ul class="nav nav-method">

                <li id="THE_STORY_BEHIND" <?php if($_SERVER[ 'SCRIPT_NAME']=="/thestory.php" || (isset($page_name) && $page_name=='THE_STORY_BEHIND' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>


                    <div class="link_block">
                        <a href="thestory.php">
                            <i class="pe-7s-map"></i>
                            <p>THE STORY BEHIND</p>
                        </a>
                    </div>
                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['THE_STORY_BEHIND']) && $tabDetail['THE_STORY_BEHIND']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['THE_STORY_BEHIND'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_STORY_BEHIND']['total_chapter'].'" now="'.$userTabDetail['THE_STORY_BEHIND']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_STORY_BEHIND']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }
                ?>
                    </div>

                </li>

                <li id="THE_CAUSES_OF_ASTHMA" <?php if($_SERVER[ 'SCRIPT_NAME']=="/introduction.php" || (isset($page_name) && $page_name=='THE_CAUSES_OF_ASTHMA' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>

                    <div class="link_block">
                        <a href="introduction.php">
                            <i class="pe-7s-science"></i>
                            <p>THE CAUSES OF ASTHMA</p>
                        </a>
                    </div>
                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['THE_CAUSES_OF_ASTHMA']) && $tabDetail['THE_CAUSES_OF_ASTHMA']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['THE_CAUSES_OF_ASTHMA'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_CAUSES_OF_ASTHMA']['total_chapter'].'" now="'.$userTabDetail['THE_CAUSES_OF_ASTHMA']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_CAUSES_OF_ASTHMA']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>

                </li>

                <li id="RI-VITAMIND" <?php if($_SERVER[ 'SCRIPT_NAME']=="/vitamind.php" || (isset($page_name) && $page_name=='RI-VITAMIND' ) ) { ?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>
                    <div class="link_block">
                        <a href="vitamind.php">
                            <i class="pe-7s-sun"></i>
                            <p>RI-VITAMIND</p>
                        </a>
                    </div>

                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['RI-VITAMIND']) && $tabDetail['RI-VITAMIND']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['RI-VITAMIND'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-VITAMIND']['total_chapter'].'" now="'.$userTabDetail['RI-VITAMIND']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-VITAMIND']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>

                </li>

                <li id="RI-OMEGA3" <?php if($_SERVER[ 'SCRIPT_NAME']=="/omega3.php" || (isset($page_name) && $page_name=='' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>

                    <div class="link_block">
                        <a href="omega3.php">
                            <i class="pe-7s-graph"></i>
                            <p>RI-OMEGA3</p>
                        </a>
                    </div>
                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['RI-OMEGA3']) && $tabDetail['RI-OMEGA3']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['RI-OMEGA3'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-OMEGA3']['total_chapter'].'" now="'.$userTabDetail['RI-OMEGA3']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-OMEGA3']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>
                </li>

                <li ID="INCREASE_HOMEOSTASIS" <?php if($_SERVER[ 'SCRIPT_NAME']=="/homeostasis.php" || (isset($page_name) && $page_name=='' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>

                    <div class="link_block">
                        <a href="homeostasis.php">
                            <i class="pe-7s-loop"></i>
                            <p>INCREASE HOMEOSTASIS</p>
                        </a>
                    </div>

                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['INCREASE_HOMEOSTASIS']) && $tabDetail['INCREASE_HOMEOSTASIS']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['INCREASE_HOMEOSTASIS'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['INCREASE_HOMEOSTASIS']['total_chapter'].'" now="'.$userTabDetail['INCREASE_HOMEOSTASIS']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['INCREASE_HOMEOSTASIS']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>
                </li>

                <li id="ELIMINATE_GUT_PRESSURE" <?php if($_SERVER[ 'SCRIPT_NAME']=="/eliminategutpressure.php" || (isset($page_name) && $page_name=='' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>
                    <div class="link_block">
                        <a href="eliminategutpressure.php">
                            <i class="pe-7s-leaf"></i>
                            <p>ELIMINATE GUT PRESSURE</p>
                        </a>
                    </div>
                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['ELIMINATE_GUT_PRESSURE']) && $tabDetail['ELIMINATE_GUT_PRESSURE']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['ELIMINATE_GUT_PRESSURE'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['ELIMINATE_GUT_PRESSURE']['total_chapter'].'" now="'.$userTabDetail['ELIMINATE_GUT_PRESSURE']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['ELIMINATE_GUT_PRESSURE']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>
                </li>

                <li id="MANAGE_STRESS" <?php if($_SERVER[ 'SCRIPT_NAME']=="/managestress.php" || (isset($page_name) && $page_name=='' ) ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>
                    <div class="link_block">
                        <a href="managestress.php">
                            <i class="pe-7s-smile"></i>
                            <p>MANAGE STRESS</p>
                        </a>
                    </div>

                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['MANAGE_STRESS']) && $tabDetail['MANAGE_STRESS']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['MANAGE_STRESS'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['MANAGE_STRESS']['total_chapter'].'" now="'.$userTabDetail['MANAGE_STRESS']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['MANAGE_STRESS']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>

                    </div>
                </li>

                <li id="EVERYTHING_TOGETHER" <?php if($_SERVER[ 'SCRIPT_NAME']=="/summary.php" ) {?> class="active clearfix"
                    <?php  }else{ ?> class="clearfix"
                    <?php } ?>>
                    <div class="link_block">
                        <a href="summary.php">
                            <i class="pe-7s-date"></i>
                            <p>EVERYTHING TOGETHER</p>
                        </a>
                    </div>
                    <div class="procres_block">
                        <?php

                if(isset($tabDetail['EVERYTHING_TOGETHER']) && $tabDetail['EVERYTHING_TOGETHER']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['EVERYTHING_TOGETHER'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['EVERYTHING_TOGETHER']['total_chapter'].'" now="'.$userTabDetail['EVERYTHING_TOGETHER']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['EVERYTHING_TOGETHER']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                    </div>
                </li>
            </ul>

            <br/>
        </div>

    </div>
</body>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>


<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>


</html>
