<?php
include getcwd()."/inc/config.php"; // Configuration php file
require(INC_DIR.'init.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

require_once(CLASS_DIR.'company.class.php');
require_once(CLASS_DIR.'user.class.php');
require_once(CLASS_DIR.'no_urut.class.php');
$company=new company($mysqli);
$user=new user($mysqli);
$directory="tmp/";
$error2="";
$session_id=session_id();

if(isset($_FILES)&&isset($_POST['session_id']))
{
	if($_POST['session_id']==$session_id)
	{
		$uploadfile = $directory."/".basename($_FILES['pdf_file']['name']);
		if($_FILES['pdf_file']['type']=="application/pdf" && $_FILES['pdf_file']['size']<204800)
		{
			if (move_uploaded_file($_FILES['pdf_file']['tmp_name'], $uploadfile)) 
			{
				$error2="<font color='green'><b>Berhasil Upload data</b></font>";
			} 
			else 
			{
				$error2.="<font color='red'><b>Gagal Upload File PDF!</b></font>";
			}
		}
		else
		{
			$error2.="<font color='red'><b>Format File PDF salah atau ukuran terlalu besar!</b></font>";
		}
	}
	else
	{
		$error2.="<font color='red'><b>No Session ID!</b></font>";
	}
}
else
{
	$error2.="<font color='red'><b>Data tidak terkirim semua!</b></font>";
}
echo $error2;
?>