<?php 
include 'inc/config.php'; // Configuration php file
require(INC_DIR.'init.php');
if($sessionObj->read('front_user_email') ==""   && $sessionObj->read("front_user_id") =="" && $sessionObj->read("front_login_as") =="" )
{
	header("Location:page_login.php");
}
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

$page_name = 'TALKTODOCTOR';
require_once('common/progress_show.php');

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="<?php //echo SITE_URL; ?>assets/img/favicon.ico"> -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RISE Asthma Method</title>
    <script src="https://cdn.ckeditor.com/4.6.1/standard-all/ckeditor.js"></script>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo SITE_URL; ?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo SITE_URL; ?>assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="<?php echo SECURE ?>://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='<?php echo SECURE ?>://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_URL; ?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <?php require('common/header.php'); ?>
</head>

<body>

    <div class="wrapper">

        <?php require("common/sidebar.php"); ?>


        <div class="main-panel">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">

                        <div class=" col-md-1 navbar-minimize">

                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <?php require("common/notifications.php"); ?>
                                <?php require("common/topsettings.php"); ?>

                            </ul>

                        </div>
                    </div>

                </div>

            </nav>

            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <div class="header-intro" style="text-align: left;">

                                    <h1>Privacy Policy</h1>
                                    <p style="text-align: left;"><strong>RISE Asthma (&ldquo;RISE Asthma&rdquo;) knows that you care about how your personal information is used and shared and takes your privacy seriously.</strong></p>
                                    <p><span style="font-weight: 400;">RISE Asthma is an e-health platform that assists people suffering from asthma and related conditions. &ldquo;</span><strong>RISE Asthma</strong><span style="font-weight: 400;">&rdquo; or the terms &ldquo;</span><strong>we</strong><span style="font-weight: 400;">&rdquo; or &ldquo;</span><strong>us</strong><span style="font-weight: 400;">&rdquo; or similar terms refer to RISE Asthma Behavioral, Inc. &ldquo;</span><strong>You</strong><span style="font-weight: 400;">&rdquo; or &ldquo;</span><strong>your</strong><span style="font-weight: 400;">&rdquo; or similar terms refer to you as a user of our Services (defined below).</span></p>
                                    <p><span style="font-weight: 400;">THIS PRIVACY POLICY IS BOTH AN AGREEMENT HEREBY ENTERED INTO BY YOU AND RISE Asthma, AND THE POLICY OF RISE Asthma IN MAKING THE SERVICES AVAILABLE TO YOU.</span></p>
                                    <ol>
                                        <li><strong> Our Promise to You.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We know you are entrusting us with some of your most personal and valuable information, including your personal health information. Your trust is built, in part, on our commitment to respect the privacy and confidentiality of your health information. We are committed to safeguarding and protecting your personal information, including health information.</span></p>
                                    <p><span style="font-weight: 400;">We are providing this Privacy Policy to inform you of our policies and procedures regarding the collection, use, and disclosure of the information that we collect and receive from users of our e-health platform at and through our website, www.RISEAsthma.com (the &ldquo;</span><strong>Site</strong><span style="font-weight: 400;">&rdquo;).</span></p>
                                    <p><span style="font-weight: 400;">The RISE Asthma e-health platform includes, without limitation, the following services (collectively, the &ldquo;</span><strong>Services</strong><span style="font-weight: 400;">&rdquo;):</span></p>
                                    <p><span style="font-weight: 400;">(a) the sale of products and services to help individuals with asthma prevention/control and related conditions, and</span></p>
                                    <p><span style="font-weight: 400;">(b) the provision of other information about RISE Asthma and our products and services through the Site.</span></p>
                                    <p><span style="font-weight: 400;">This Privacy Policy applies only to information that you provide to us through the Services. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms of Service [website link to Terms of Service] (the &ldquo;</span><strong>Terms</strong><span style="font-weight: 400;">&rdquo;).</span></p>
                                    <p><span style="font-weight: 400;">By accepting our Privacy Policy during registration, or by visiting the Site and/or using the Services, you expressly consent to our collection, use, and disclosure of your Personal Information (as defined below) in accordance with this Privacy Policy.</span></p>
                                    <p><span style="font-weight: 400;">As used in this Privacy Policy, the terms &ldquo;</span><strong>using</strong><span style="font-weight: 400;">&rdquo; and &ldquo;</span><strong>processing</strong><span style="font-weight: 400;">&rdquo; information include using cookies on a computer, subjecting the information to statistical or other analysis, and using or handling information in any way, including, without limitation, collecting, storing, evaluating, modifying, deleting, using, combining, disclosing, and transferring information within our organization or among our affiliates or with Medical Providers within the United States or internationally.</span></p>
                                    <ol start="2">
                                        <li><strong> Collection and Use of Information &ndash; In General.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">When using our Services, we will ask you for certain personally identifiable information. This refers to information about you that can be used to contact or identify you, and information on your use or potential use of the Services and related services (collectively, &ldquo;</span><strong>Personal</strong> <strong>Information</strong><span style="font-weight: 400;">&rdquo;). Personal Information that we might collect would include things like your name, phone number, gender, occupation, hometown, personal interests, credit card or other billing information, and your email address. We also may on occasion collect information you provide voluntarily in free-form text boxes on the Site and through responses to surveys, questionnaires and the like. If you communicate with us by, for example, email, facsimile or letter, any information provided in such communication may be collected as Personal Information.</span></p>
                                    <p><span style="font-weight: 400;">The main reason we collect Personal Information from you is to provide you a safe, smooth, efficient, and customized user experience. The collection of Personal Information also enables our users to establish a user account and profile that can be used to interact with us and other users through the Site. We only collect Personal Information we consider important to achieve that goal. You always have the option not to provide some, or any, Personal Information by either choosing not to become a registered user of the Services, or else by skipping the particular feature of the Services for which the Personal Information is being collected. You can use some of the Services anonymously, but once you become a registered user of the Services, we will ask you to provide Personal Information, such as:</span></p>
                                    <ul>
                                        <li><span style="font-weight: 400;"> Contact and identity information (e.g., mailing address and phone number), and</span></li>
                                        <li><span style="font-weight: 400;"> Other personal information as indicated (our forms indicate what information is required, and what information is optional).</span></li>
                                    </ul>
                                    <p><span style="font-weight: 400;">You are under no obligation to provide us with this Personal Information. We use your Personal Information to provide the Services and administer your inquiries. You may change some of the information that you provide. Please see &ldquo;</span><strong>Changing or Deleting Your Information</strong><span style="font-weight: 400;">&rdquo; below for further information.</span></p>
                                    <ol start="3">
                                        <li><strong> How We Use Your Non-Medical Personal Information.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">Some of the Personal Information we collect from you is unrelated to your receipt of our products and services through the Services. Examples of how we may use your Personal Information include, but are not limited to, the following:</span></p>
                                    <ul>
                                        <li><span style="font-weight: 400;"> Enable you to easily navigate the Services</span></li>
                                        <li><span style="font-weight: 400;"> Resolve service and billing problems via telephone or email</span></li>
                                        <li><span style="font-weight: 400;"> Troubleshoot technical problems</span></li>
                                        <li><span style="font-weight: 400;"> Bill any amounts due from you</span></li>
                                        <li><span style="font-weight: 400;"> Better understand users&rsquo; needs and interests</span></li>
                                        <li><span style="font-weight: 400;"> Personalize your experience</span></li>
                                        <li><span style="font-weight: 400;"> Detect and protect us against error, fraud, and other criminal activity</span></li>
                                        <li><span style="font-weight: 400;"> Enforce our Terms</span></li>
                                        <li><span style="font-weight: 400;"> Provide you with system or administrative messages, and as otherwise described to you at the time of collection</span></li>
                                        <li><span style="font-weight: 400;"> Provide you with further information and offers from us that we believe you may find useful or interesting</span></li>
                                    </ul>
                                    <p><span style="font-weight: 400;">If you decide at any time that you no longer wish to receive certain communications from us, please follow the unsubscribe instructions provided in any of the communications or select the appropriate option in your user profile. (See &ldquo;</span><strong>Changing or Deleting Your Information</strong><span style="font-weight: 400;">,&rdquo; below.) You cannot elect to unsubscribe some administrative communications, such as notification of new messages from your Treatment Providers. To stop receiving these communications, you will need to deactivate your account.</span></p>
                                    <ol start="4">
                                        <li><strong> Log Data.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">When you visit the Services, our servers automatically record information that your browser sends whenever you visit a website (&ldquo;</span><strong>Log</strong> <strong>Data</strong><span style="font-weight: 400;">&rdquo;). This Log Data may include information such as your computer&rsquo;s Internet Protocol (&ldquo;</span><strong>IP</strong><span style="font-weight: 400;">&rdquo;) address, browser type, or the webpage you were visiting before you came to our Services, pages of our website and Services that you visit, the time spent on those pages, information you search for on our Services, access times and dates, and other statistics. We use this information to monitor and analyze use of the Services and for the Services&rsquo; technical administration, to increase our Services&rsquo; functionality and user-friendliness, and to better tailor it to our visitors&rsquo; needs. For example, some of this information is collected so that when you visit the Services again, it will recognize you and provide information appropriate to your interests. We also use this information to verify that visitors to the Services meet the criteria required to process their requests.</span></p>
                                    <p><span style="font-weight: 400;">Generally, our service automatically collects usage information, such as the numbers and frequency of visitors to our site and its components, similar to TV ratings that indicate how many people watched a particular show. RISE Asthma only uses these data in aggregate form, that is, as a statistical measure, and not in a manner that would identify you personally. These type of aggregate data enable us to figure out how often users use parts of the Site or the Services so that we can improve the Services.</span></p>
                                    <ol start="5">
                                        <li><strong> Cookies.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We also use &ldquo;</span><strong>cookies</strong><span style="font-weight: 400;">&rdquo; to collect information. A cookie is a small data file that we transfer to your computer&rsquo;s hard disk for record-keeping purposes. We use cookies for two purposes. First, we may utilize persistent cookies to save your user credentials for future logins to the Services. Second, we may utilize session ID cookies to enable certain features of the Services, to better understand how you interact with the Services and to monitor aggregate usage by users of the Services and web traffic routing on the Services. Unlike persistent cookies, session cookies are deleted from your computer when you log off from the Services and then close your browser. We may work with third parties that place or read cookies on your browser to improve your user experience.</span></p>
                                    <p><span style="font-weight: 400;">You can instruct your browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. If you do not accept cookies, however, you may not be able to use all portions or all functionality of the Services.</span></p>
                                    <ol start="6">
                                        <li><strong> Web Beacons.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We may also occasionally use &ldquo;</span><strong>web</strong> <strong>beacons</strong><span style="font-weight: 400;">&rdquo; (also known as &ldquo;</span><strong>clear</strong> <strong>gifs</strong><span style="font-weight: 400;">,&rdquo; &ldquo;</span><strong>web</strong> <strong>bugs</strong><span style="font-weight: 400;">,&rdquo; &ldquo;</span><strong>1-pixel gifs</strong><span style="font-weight: 400;">,&rdquo; etc.) that allow us to collect non-personal information about your response to our email communications, and for other purposes. Web beacons are tiny images, placed on a Web page or e-mail, that can tell us if you have visited a particular area of the Services. For example, if you have given us permission to send you emails, we may send you an email urging you to use a certain feature of the Services. If you do respond to that email and use that feature, the web beacon will tell us that our email communication with you has been successful..</span></p>
                                    <p><span style="font-weight: 400;">Because Web beacons are used in conjunction with persistent cookies (described above), if you set your browser to decline or deactivate cookies, Web beacons cannot function.</span></p>
                                    <ol start="7">
                                        <li><strong> Emails.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We may use a third-party vendor to help us manage some of our email communications with you. While we may supply this vendor with email addresses of those we wish them to contact, your email address is never used for any purpose other than to communicate with you on our or your Medical Provider's behalf. When you click on a link in an email, you may temporarily be redirected through one of the vendor&rsquo;s servers (although this process will be invisible to you) which will register that you have clicked on that link, and have visited our Services. We also often receive a confirmation when you open an email from RISE Asthma if your computer supports this type of program. RISE Asthma uses this confirmation to help us make emails more useful to you.</span></p>
                                    <ol start="8">
                                        <li><strong> Messages and Transactions.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">Comments or questions sent to us using email or secure messaging forms will be shared with our staff who are most able to address your concerns. We will archive your messages once we have made our best effort to provide you with a complete and satisfactory response. However, these communications will not become part of your medical record (or other appropriate treatment record) unless and until you use the Services to obtain mental health advice or a consultation from a Treatment Provider.</span></p>
                                    <p><span style="font-weight: 400;">When you use a service on the secure section of the Services to interact directly with Treatment Providers, some information you provide may be documented in your medical record or other appropriate treatment record, and available for use to guide your treatment as a patient.</span></p>
                                    <ol start="9">
                                        <li><strong> Information Sharing and Disclosure</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We will not rent, sell, or share Personal Information about you with other people or non-affiliated companies except to provide the Services, when we otherwise have your permission, or under the following circumstances:</span></p>
                                    <ul>
                                        <li><strong>Communications in Response to User Submissions.</strong><span style="font-weight: 400;"> As part of the Site and the Services, you may receive from RISE Asthma and other users email and other communications relating to your requests, mental health services, and other transactions. When you transmit information relating to your mental health services needs, RISE Asthma and the Medical Providers you select may send you emails and other communications that they determine in their sole discretion relate to your mental health services needs.</span></li>
                                        <li><strong>Aggregate Information and Non-Identifying Information.</strong><span style="font-weight: 400;"> We may share aggregated information that does not include Personal Information and we may otherwise disclose non-identifying Information and Log Data with third parties for industry analysis, demographic profiling, and other purposes. Any aggregated information shared in these contexts will not contain your Personal Information.</span></li>
                                        <li><strong>Service Providers.</strong><span style="font-weight: 400;"> We may employ third-party companies and individuals to process your payments, facilitate our Services, to provide the Services on our behalf, to perform Services-related services (including, without limitation, maintenance services, database management, web analytics and improvement of the Services&rsquo; features), or to assist us in analyzing how our Services are used. These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</span></li>
                                        <li><strong>Compliance with Laws and Law Enforcement.</strong><span style="font-weight: 400;"> We cooperate with government and law enforcement officials and private parties to enforce and comply with the law. We will disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate to respond to claims and legal process (including but not limited to subpoenas), to protect the property and rights of RISE Asthma or a third party, to protect the safety of the public or any person, or to prevent or stop activity we may consider to be, or to pose a risk of being, any illegal, unethical or legally actionable activity. This includes, without limitation, exchanging information with Treatment Providers and law enforcement in response to Treatment Providers&rsquo; professional and legal responsibilities.</span></li>
                                        <li><strong>Business Transfers.</strong><span style="font-weight: 400;"> We may sell, transfer or otherwise share some or all of our assets, including your Personal Information, in connection with a merger, acquisition, reorganization or sale of assets, or in the event of bankruptcy.</span></li>
                                    </ul>
                                    <p><span style="font-weight: 400;">PLEASE NOTE THAT ANY INFORMATION, TEXT AND IMAGES THAT YOU POST OR DISCLOSE ON OR THROUGH PUBLIC PORTIONS OF THE SITE, OR ANY OTHER PUBLIC FORUMS, BECOMES PUBLIC INFORMATION AND MAY BE AVAILABLE TO VISITORS TO THE SITE AND/OR SEARCHABLE VIA THE INTERNET. Information regarding your activities in such Services may also be available for view by other users (for example, other users may be able to view a list of all postings you have made in all available forums). We urge you to exercise discretion and caution when deciding to disclose your Personal Information through a forum or otherwise through the Site. RISE Asthma IS NOT RESPONSIBLE FOR THE USE OF ANY PERSONAL INFORMATION YOU VOLUNTARILY DISCLOSE THROUGH A FORUM OR OTHERWISE THROUGH THE SITE OR THE SERVICES.</span></p>
                                    <ol>
                                        <li><strong> Changing or Deleting Your Information.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">You may review, update, correct or delete some portions of your Personal Information in your registration profile by making the appropriate modifications in your user account settings or by contacting us at </span><a href="mailto:hope@RISEAsthma.com"><span style="font-weight: 400;">hope@RISEAsthma.com</span></a><span style="font-weight: 400;">. Some Personal Information, such as your answers to online assessments, may not be updateable or deleted once submitted. If you delete certain information required to receive Services, such as a credit card on file, then you may no longer be able to receive Services and your account may be deactivated. If you would like us to remove your records from our system, please contact us and we will attempt to accommodate your request if we do not have any legal obligation to retain the records.</span></p>
                                    <p><span style="font-weight: 400;">Please note that we may need to retain certain information for recordkeeping purposes, and there may also be residual information that will remain within our databases and other records, which, irrespective of any efforts by us to delete information, will not be removed from them. We also reserve the right, from time to time, to re-contact former users of the Site. Finally, we are not responsible for removing information from the databases of third parties with whom we have already shared Personal Information about you.</span></p>
                                    <ol>
                                        <li><strong> Security.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We employ administrative, physical, and technical measures designed to safeguard and protect information under our control from unauthorized access, use, and disclosure. Except for appointment reminders, treatment referrals and prescription information, these measures include encrypting your communications by utilizing Secure Sockets Layer (&ldquo;SSL&rdquo;) software, and using a secured messaging service when we send your Personal Information electronically.</span></p>
                                    <p><span style="font-weight: 400;">Despite these measures, the confidentiality of any communication or material transmitted to or from us via the Services by Internet or email, or any electronic storage system, cannot be guaranteed. As a result, although we strive to protect your Personal Information, we cannot ensure or warrant the security of any information you transmit to us through or in connection with the Site or that is stored by us. You acknowledge and agree that any information you transmit through the Site or upload for storage in connection with the Site is so transmitted or stored at your own risk. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of any Account you might have with us has been compromised), you must immediately notify us of the problem by contacting us in accordance with the &ldquo;Contacting Us&rdquo; section below (note that if you choose to notify us via physical mail, this will delay the time it takes for us to respond to the problem). In addition, if you have privacy or data security related questions, please feel free to contact the office identified at the end of this document.</span></p>
                                    <ol start="2">
                                        <li><strong> International Transfer.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">Your information may be transferred to &mdash; and maintained on &mdash; computers located outside of your state, province, country or other governmental jurisdiction where the privacy laws may not be as protective as those in your jurisdiction. If you are located outside the United States and choose to provide information to us, we may transfer your Personal Information to the United States and process it there. Your submission of such information represents your agreement to that transfer.</span></p>
                                    <ol start="3">
                                        <li><strong> Links to Other Sites.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We may offer you the opportunity to access third-party content, services, or products by linking to a third party&rsquo;s website. If you choose to visit an advertiser by &ldquo;clicking on&rdquo; a banner ad or other type of advertisement, or click on another third party link, you will be directed to that third party&rsquo;s website. The fact that we may link to a website or present a banner ad or other type of advertisement is not an endorsement, authorization, or representation of our affiliation with that third party, nor is it an endorsement of their privacy or information security policies or practices. We do not exercise control over third-party websites. These other websites may place their own cookies or other files on your computer, collect data or solicit personal information from you. Other services follow different rules regarding the use or disclosure of the Personal Information you submit to them. Our Privacy Policy only applies to the Services and we are not responsible for the privacy practices or the content of other websites. You should check the privacy policies of those sites before providing your Personal Information to them.</span></p>
                                    <ol start="4">
                                        <li><strong> Children.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">The Services are not directed to children. We do not knowingly allow or solicit anyone under the age of 13 to participate independently in any of the Services. We do not knowingly collect personally identifiable information from children, except in the context of a Medical Provider&rsquo;s health consultation through the Services when a parent is present and has consented to treatment. If a parent or guardian becomes aware that his or her child has provided us with Personal Information without their consent, please contact us at </span><span style="font-weight: 400;">hope@RISEAsthma.com</span><span style="font-weight: 400;">. Access to the Services for dependents (children over the age of 3 or a spouse or domestic partner) is only permitted through the primary account holder&rsquo;s username and password. Minors are not allowed to use the Services without parental consent and assistance. If we become aware that a user of the Services is under the age of 13 and has provided us with Personal Information without verifiable parental consent, we may delete such information from our files and may deactivate the related account.</span></p>
                                    <ol start="5">
                                        <li><strong> Agreement and Changes.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">By using the Services, you agree to the current Privacy Policy and our Terms, into which this Privacy Policy is incorporated. We reserve the right, in our sole discretion, to modify, discontinue, or terminate the Services or to modify this Privacy Policy at any time. If we modify this Privacy Policy, we will notify you of such changes by posting them on the Services or providing you with notice of the modification. We will also indicate when such terms are effective below. By continuing to access or use the Services after we have posted a modification or have provided you with notice of a modification, you are indicating that you agree to be bound by the modified Privacy Policy. If the modified Privacy Policy is not acceptable to you, your only recourse is to cease using the Services.</span></p>
                                    <ol start="6">
                                        <li><strong>Contacting Us.</strong></li>
                                    </ol>
                                    <p><span style="font-weight: 400;">We encourage you to contact us at </span><a href="mailto:hope@RISEAsthma.com"><span style="font-weight: 400;">hope@RISEAsthma.com</span></a> <span style="font-weight: 400;">if you have any questions concerning our Privacy Policy or if you have any questions or concerns about our access, use, or disclosure of your Personal Information. Please note that email communications will not necessarily be secure; accordingly, you should not include credit card information, PHI, or other sensitive information in your email correspondence with us. If you would like to contact us via physical mail, our mailing address is: RISE Asthma, ___________________________________Attention: Security Officer.</span></p>
                                    <p><span style="font-weight: 400;">Last Revised: June 12, 2017</span></p>
                                    <p><span style="font-weight: 400;">Site copyright 2017 RISE Asthma unless otherwise noted. All rights reserved.</span></p>
                                    <p>&nbsp;</p>
                                    <h2><br /><br /></h2>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
            <!-- container fluid    -->

            <?php //require("common/footer.php"); ?>

        </div>
    </div>


</body>
<!-- Including Js Files -->
<?php require("common/includejs.php"); ?>

</html>
