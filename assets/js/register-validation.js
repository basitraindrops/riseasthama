
$('document').ready(function() {
      $('.first_section').show();
      $('.second_section').hide();
      $('.myself_section').hide();
      $('.third_section').hide();
      $('.select_one_section').hide();
      $("#top").addClass('top-head');
      $("#terms-modal").hide();

});

// $(".btn-myself").click(function() {
//             $('.first_section').hide();
//             $('.second_section').show();
//             $('.third_section').hide();
//             $('.myself_section').hide(); 
//             $('.select_one_section').hide();
//  }); 
// $(".btn-selectone").click(function() {
//             $('.first_section').hide();
//             $('.second_section').show();
//             $('.third_section').hide();
//             $('.myself_section').hide(); 
//             $('.select_one_section').hide();
//  }); 
// $(".btn-first").click(function() {
//             $('.first_section').hide();
//             $('.myself_section').show();
//             $('.select_one_section').hide();
//             $('.third_section').hide();
//             $("#name-error").hide();
//             $('.error').hide(); 
//             $(".btn-second").attr('disabled', 'disabled');
//             $(".btn-second").addClass('disable');
//             $(".btn-myself").attr('disabled', 'disabled');
//             $(".btn-myself").addClass('disable');
//             $("#top").removeClass('top-head');
//             $("#top").addClass('top-head-second');
//             return false;

//  });
// $(".btn-select-one").click(function() {
//             $('.first_section').hide();
//             $('.select_one_section').show();
//             $('.myself_section').hide();
//             $('.third_section').hide();
//             $("#name-error").hide();
//             $('.error').hide(); 
//             $(".btn-second").attr('disabled', 'disabled');
//             $(".btn-second").addClass('disable');
//             $(".btn-selectone").attr('disabled', 'disabled');
//             $(".btn-selectone").addClass('disable');
//             $("#top").removeClass('top-head');
//             $("#top").addClass('top-head-second');
//             return false;

//  });

/*$(document).on('blur', "#payment-form :input[name='email']", function(e) {
    validateDetail($(this).val())
})*/
function validateDetail()
{
    $("#email").removeClass('input-error');
    $("#email-error").html('');
    var email = $("#payment-form :input[name='email']").val();
    var name = $("#payment-form :input[name='inputname']").val();
    var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //var re=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!re.test(email)) {
      $(".btn-second").attr('disabled', 'disabled');
      $(".btn-second").addClass('disable');
    }
    if(re.test(email)) 
    {
      $(".btn-second").removeAttr('disabled', 'disabled');
      $(".btn-second").removeClass('disable');
    }
    if(email == '') 
    {
      $(".btn-second").attr('disabled', 'disabled');
      $(".btn-second").addClass('disable');
    } 
}
$(".btn-second").click(function() {
    var email = $("#payment-form :input[name='email']").val()
    $.ajax({
            type: "POST",
            dataType: "html",
            url: "emailcheck.php",
            data: {
                  email_val: email
            },
            success: function(result) {

             if (result == 'false') {
                $(".btn-second").attr('disabled', 'disabled');
                $(".btn-second").addClass('disable');
                $("#email-error").html('This email address is already in use.');
                $("#email-error").show();
                $('.first_section').hide();
                $("#email").addClass('input-error');
               
             } else {
                $(".btn-second").removeAttr('disabled', 'disabled');
                $("#email-error").html('');
                var mobile = (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));  
                if (mobile){ }else{
                   $('.second_section').hide();
                  $('.third_section').show();
                }
               // $('.second_section').hide();
                $(".btn-second").removeClass('disable');
                //$('.third_section').show();
                $('.first_section').hide();
                $("#card-errors").hide();
                $("input").removeClass('input-error');
              }
             
            }
          });
     return false;
});
$("#inputname").blur(function(){

    var name = $("#payment-form :input[name='inputname']").val();
    if (name == '') {
    $("#name-error").html('This field is required.');
    $("#name-error").show();
    $("#inputname").addClass('input-error');    
   }
    else
   {
    $("#name-error").html('');
    $("#name-error").hide();
    $("#inputname").removeClass('input-error');
   }
});
function validateName()
{
    $("#inputname").removeClass('input-error');
    $("#name-error").html('');
    $("#name-error").hide();
}

function validateNumber(number) {

    error = true
    number = number.trim();
    var count = number.length;
           
    if (count == 19) {
      sp = number.split(" ").length - 1;
      if (sp == 3) {
                    error = false;
      }
   }
  if (count == 16) {
      sp = number.split(" ").length - 1;
      if (sp == 0) {
       error = false;
      }
 }
  if (error) {

    $("#credit_card-error").html('Please Enter Valid Card Number!');
    $("#credit_card-error").show();
    $("#stripe_number").addClass('input-error');
    } 
    else 
    {
      $("#credit_card-error").html('');
      $("#credit_card-error").hide();
    }
  return error;
}
function validateExpiry(expiry) 
{
    error = false
    res = '';
    count = expiry.length;

    if (count == 5) {
        sp = expiry.split("/").length - 1;
        if (sp != 1) {
            error = true;
            res = ' lenght ';
        }
    } else {
        error = true;
        res = ' lenght2 ';
    }
    if (!error) {

        var $split = expiry.split("/");

        if ($split[0].length != 2 || $split[0] > 12) {
            error = true;
            res = ' lenght3 ';
        }

        if (!error) {
            var year = new Date().getFullYear().toString().substr(-2);
            if ($split[1] < year) {
                error = true;

                res = ' lenght5 ';
            }
        }
    }
    if (error) {
        $("#expirydate-error").html('Please enter valid Expiry Date!');
        $("#expirydate-error").show();
        $("#expirydate").addClass('input-error');
    
    } else {
        $("#expirydate-error").html('');
        $("#expirydate-error").hide();
    }

    return error;
}
function validateCVV(cvv) {

      error = false

      count = cvv.length;
      if (count != 3) {
          error = true
      }

      if (error) {
          $("#cvv-error").html('Please enter valid CVV !');
          $("#cvv-error").show();
          $(".input_cvv").addClass('input-error');
      } else {
          $("#cvv-error").html('');
          $("#cvv-error").hide();
      }
      return error;

  }
  function isNumber() {
      $("#age_myself-error").html('');
      $("#age_myself-error").hide();
      $("#age_myself").removeClass('input-error');
      $(".btn-myself").removeAttr('disabled', 'disabled');
      $(".btn-myself").removeClass('disable');

      $("#asthma_time_myself-error").html('');
      $("#asthma_time_myself-error").hide();
      $("#asthma_time_myself").removeClass('input-error');
      $(".btn-myself").removeAttr('disabled', 'disabled');
      $(".btn-myself").removeClass('disable');

    var asthma = $("#payment-form :input[name='asthma_time_myself']").val();
    var re =/^\d+$/;
    if (!re.test(asthma)) {
      $(".btn-myself").attr('disabled', 'disabled');
      $(".btn-myself").addClass('disable');
    }
    if(re.test(asthma)) 
    {
      $(".btn-myself").removeAttr('disabled', 'disabled');
      $(".btn-myself").removeClass('disable');
    }
    var age = $("#payment-form :input[name='age_myself']").val();
    if(age == '')
    {
      $(".btn-myself").attr('disabled', 'disabled');
      $(".btn-myself").addClass('disable');
      $("#age_myself-error").html('This field is required');
      $("#age_myself-error").show();
      $("#age_myself").addClass('input-error');

    }
   
   
}
function isNumber_paypal() {

    $("#age-error").html('');
    $("#age-error").hide();
    $("#asthma_time").removeClass('input-error');

    $("#asthma-error").html('');
    $("#asthma-error").hide();
    $("#asthma_time").removeClass('input-error');
  

    var asthma1 = $("#payment-form :input[name='asthma_time']").val();
    var re =/^\d+$/;
    if (!re.test(asthma1)) {
      $(".btn-selectone").attr('disabled', 'disabled');
      $(".btn-selectone").addClass('disable');
    }
    if(re.test(asthma1)) 
    {
      $(".btn-selectone").removeAttr('disabled', 'disabled');
      $(".btn-selectone").removeClass('disable');
    }
    var age = $("#payment-form :input[name='age']").val();
    if(age == '')
    {
      $(".btn-selectone").attr('disabled', 'disabled');
      $(".btn-selectone").addClass('disable');
      $("#age-error").html('This field is required');
      $("#age-error").show();
      $("#age").addClass('input-error');

    }
   

}
$( "#age_myself" ).change(function() {
    var age = $("#payment-form :input[name='age_myself']").val();
    if(age == '')
    {
      $("#age_myself-error").html('This field is required');
      $("#age_myself-error").show();
      $("#age_myself").addClass('input-error');
      $(".btn-myself").attr('disabled', 'disabled');
      $(".btn-myself").addClass('disable');

    }
    else
    {
      $("#age_myself-error").html('');
      $("#age_myself-error").hide();
      $("#age_myself").removeClass('input-error');
    }
});
$( "#asthma_time_myself" ).change(function() {
    var asthma = $("#payment-form :input[name='asthma_time_myself']").val();
    if(asthma == '')
    {
      $("#asthma_time_myself-error").html('This field is required');
      $("#asthma_time_myself-error").show();
      $("#asthma_time_myself").addClass('input-error');
      $(".btn-myself").attr('disabled', 'disabled');
      $(".btn-myself").addClass('disable');

    }
    else
    {
      $("#asthma_time_myself-error").html('');
      $("#asthma_time_myself-error").hide();
      $("#asthma_time_myself").removeClass('input-error');
    }
});
$( "#age" ).change(function() {
    var age = $("#payment-form :input[name='age']").val();
    if(age == '')
    {
      $("#age-error").html('This field is required');
      $("#age-error").show();
      $("#age").addClass('input-error');
      $(".btn-selectone").attr('disabled', 'disabled');
      $(".btn-selectone").addClass('disable');
    }
    else
    {
      $("#age-error").html('');
      $("#age-error").hide();
      $("#age").removeClass('input-error');
    }
});
$( "#asthma_time" ).change(function() {
    var asthma = $("#payment-form :input[name='asthma_time']").val();
    if(asthma == '')
    {
      $("#asthma-error").html('This field is required');
      $("#asthma-error").show();
      $("#asthma_time").addClass('input-error');
      $(".btn-selectone").attr('disabled', 'disabled');
      $(".btn-selectone").addClass('disable');
    }
    else
    {
      $("#asthma-error").html('');
      $("#asthma-error").hide();
      $("#asthma_time").removeClass('input-error');
    
    }
});
$(document).on('blur', "#payment-form :input[name='age_myself']", function(e) {
    $(this).val($(this).val().trim())
})
$(document).on('keyup', "#payment-form :input[name='age_myself']", function(e) {

     var count = $(this).val().length;
     var key = e.keyCode || e.which;
     aa = $(this).val();
     val = aa.replace(/[^0-9 ]/g, '');
     count2 = val.length; 
     val = val.slice(0, 3)
     count2 = val.length;
    if (aa !== val) {
      valTmp = $(this).val().replace(/[^0-9]/g, '');
      c3 = valTmp.length;
      if (c3 > 3) {
         valTmp = valTmp.slice(0, 3)
         $(this).val(function(index, value) {
         valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         valTmp = valTmp.trim();
        return valTmp;
        });
       } else {
        return $(this).val(val);
      }
   }
   if (count2 < 3) {
      if (count2 % 5 == 0) {
      $(this).val(function(index, value) {
      return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.trim();
      return val;
      });
     }
  }
});
$(document).on('blur', "#payment-form :input[name='asthma_time_myself']", function(e) {
    $(this).val($(this).val().trim())
})
$(document).on('keyup', "#payment-form :input[name='asthma_time_myself']", function(e) {

     var count = $(this).val().length;
     var key = e.keyCode || e.which;
     aa = $(this).val();
     val = aa.replace(/[^0-9 ]/g, '');
     count2 = val.length; 
     val = val.slice(0, 3)
     count2 = val.length;
    if (aa !== val) {
      valTmp = $(this).val().replace(/[^0-9]/g, '');
      c3 = valTmp.length;
      if (c3 > 3) {
         valTmp = valTmp.slice(0, 3)
         $(this).val(function(index, value) {
         valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         valTmp = valTmp.trim();
        return valTmp;
        });
       } else {
        return $(this).val(val);
      }
   }
   if (count2 < 3) {
      if (count2 % 5 == 0) {
      $(this).val(function(index, value) {
      return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.trim();
      return val;
      });
     }
  }
});
$(document).on('blur', "#payment-form :input[name='age']", function(e) {
    $(this).val($(this).val().trim())
})
$(document).on('keyup', "#payment-form :input[name='age']", function(e) {

     var count = $(this).val().length;
     var key = e.keyCode || e.which;
     aa = $(this).val();
     val = aa.replace(/[^0-9 ]/g, '');
     count2 = val.length; 
     val = val.slice(0, 3)
     count2 = val.length;
    if (aa !== val) {
      valTmp = $(this).val().replace(/[^0-9]/g, '');
      c3 = valTmp.length;
      if (c3 > 3) {
         valTmp = valTmp.slice(0, 3)
         $(this).val(function(index, value) {
         valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         valTmp = valTmp.trim();
        return valTmp;
        });
       } else {
        return $(this).val(val);
      }
   }
   if (count2 < 3) {
      if (count2 % 5 == 0) {
      $(this).val(function(index, value) {
      return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.trim();
      return val;
      });
     }
  }
});
$(document).on('blur', "#payment-form :input[name='asthma_time']", function(e) {
    $(this).val($(this).val().trim())
})
$(document).on('keyup', "#payment-form :input[name='asthma_time']", function(e) {

     var count = $(this).val().length;
     var key = e.keyCode || e.which;
     aa = $(this).val();
     val = aa.replace(/[^0-9 ]/g, '');
     count2 = val.length; 
     val = val.slice(0, 3)
     count2 = val.length;
    if (aa !== val) {
      valTmp = $(this).val().replace(/[^0-9]/g, '');
      c3 = valTmp.length;
      if (c3 > 3) {
         valTmp = valTmp.slice(0, 3)
         $(this).val(function(index, value) {
         valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         valTmp = valTmp.trim();
        return valTmp;
        });
       } else {
        return $(this).val(val);
      }
   }
   if (count2 < 3) {
      if (count2 % 5 == 0) {
      $(this).val(function(index, value) {
      return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.trim();
      return val;
      });
     }
  }
});

  //local
   //Stripe.setPublishableKey('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
   //live
   Stripe.setPublishableKey('pk_live_omxkWpyxuazQmrrsXjSH2DFD');
        var stripeResponseHandler = function(status, response) {
            var $form = $('#payment-form');
            if (response.error) {
                
                $("#card-errors").show();

                $("#credit_card-error").html('Please Enter Correct Detail!');
                $("#credit_card-error").show();
                $form.find('button').prop('disabled', false);
            } else {

                var token = response.id;
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                $("#credit_card-error").html('');
                $("#credit_card-error").hide();
                $form.get(0).submit();
            }
        };
 jQuery(function($) {
$('#payment-form').submit(function(e) {

    if ($('#payment-form').attr('action') == 'intro.php') {
        var $form = $(this);
        var expiry = $("#payment-form :input[name='stripe_expirydate']").val()
        var stripe = expiry.split("/");
        var cvv = $("#payment-form :input[name='stripe_cvc']").val();

        error = false;
        //$("#card-errors").hide();

        number = $("#payment-form :input[name='stripe_number']").val();

        errorNumber = validateNumber(number);
        errorExpiry = validateExpiry(expiry);
        errorCvv = validateCVV(cvv);


        if (errorNumber || errorExpiry || errorCvv) {
            $("#card-errors").show();
            return false;
        } else {

              $form.find('.btn').prop('disabled', true);

              // Request a token from Stripe:
              Stripe.card.createToken({
                  number: $("#payment-form :input[name='stripe_number']").val(), //values['stripe_number'],
                  cvc: $("#payment-form :input[name='stripe_cvc']").val(),
                  exp_month: stripe[0],
                  exp_year: stripe[1]
              }, stripeResponseHandler);
              // Prevent the form from being submitted:
              return false;
        }

    }
});
});

 $('document').ready(function() {

$(".btn-paypal").click(function() {
    var datastring = $("#payment-form").serialize();
    $('#payment-form').attr('action', "paypal.php");
    $("#payment-form").submit();
});

$(document).on('blur', "#payment-form :input[name='stripe_number']", function(e) {

    $(this).val($(this).val().trim())
    validateNumber($(this).val())
})
$(document).on('keyup', "#payment-form :input[name='stripe_number']", function(e) {
    $("#stripe_number").removeClass('input-error');
     var count = $(this).val().length;
     var key = e.keyCode || e.which;
     aa = $(this).val();
     val = aa.replace(/[^0-9 ]/g, '');
     count2 = val.length; 
     val = val.slice(0, 19)
     count2 = val.length;
    if (aa !== val) {
      valTmp = $(this).val().replace(/[^0-9]/g, '');
      c3 = valTmp.length;
      if (c3 > 16) {
         valTmp = valTmp.slice(0, 16)
         $(this).val(function(index, value) {
         valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
         valTmp = valTmp.trim();
        return valTmp;
        });
       } else {
        return $(this).val(val);
      }
   }
   if (count2 < 19) {
      if (count2 % 5 == 0) {
      $(this).val(function(index, value) {
      return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
      val = val.trim();
      return val;
      });
     }
  }
});  

   $(document).on('blur', "#payment-form :input[name='stripe_expirydate']", function(e) {
                validateExpiry($(this).val())
    })
    $(document).on('keyup', "#payment-form :input[name='stripe_expirydate']", function(e) {
        $("#expirydate").removeClass('input-error');
        oldVal = $(this).attr('old_val');
        oldCount = oldVal.length;
        var count = $(this).val().length;
       if (oldCount > count) {
          $(this).attr('old_val', $(this).val());
          return
      }
      val = $(this).val().replace(/[^0-9/]/g, '');
      if (!($(this).val() == val)) {
        $(this).val(val)
        $(this).attr('old_val', val);
        return
     }
    if (count > 5) {
        $(this).attr('old_val', $(this).val().slice(0, 5));
        $(this).focus().val($(this).val().slice(0, 5))
     }
     if ((parseInt($(this).val()) > 12 || $(this).val() == 00) && count == 2) {
        $("#card-errors").show();
        $("#expirydate-error").html('Please Enter Valid Month');
        $("#expirydate-error").show();
        $("#expirydate").addClass('input-error');
        $(this).attr('old_val', $(this).val());
        $(this).focus()
    } else if (count == 2) 
    { 
      if ($(this).val().indexOf('/') < 0) {
      $(this).attr('old_val', $(this).val() + "/");
      $(this).focus().val(function(index, value) {
      return value + "/";
      });
    } else {
      $(this).attr('old_val', "0" + $(this).val());
      $(this).focus().val(function(index, value) {
      return "0" + value;
    });
   }
    $("#expirydate-error").html('');
    $("#expirydate-error").hide();
    } 
    else 
    {
      $(this).attr('old_val', $(this).val());

      if ($(this).val().indexOf('/')) {
          var $split = $(this).val().split("/");
          var year = new Date().getFullYear().toString().substr(-2);

          if (count == 5 && $split[1] < year) {
              $(this).focus()
              $("#card-errors").show();
              $("#expirydate-error").html('Please Enter Valid Year');
              $("#expirydate").addClass('input-error');
              $("#expirydate-error").show();
              return false;
          } else {
              $("#expirydate").html('');
              $("#expirydate-error").hide();
          }
      }
    }
});



 $(document).on('keyup', "#payment-form :input[name='stripe_cvc']", function(e) {
    $(".input_cvv").removeClass('input-error');
    var count = $(this).val().length;
    val = $(this).val().replace(/[^0-9/]/g, '');

    if (!($(this).val() == val)) {
      $(this).val(val)
      return
    }

    if (count > 3) {
      $(this).focus().val($(this).val().slice(0, 3))
    }

    })
    $(document).on('blur', "#payment-form :input[name='stripe_cvc']", function(e) {
    validateCVV($(this).val())
   })
 });

  

var mobile = (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));  
    if (mobile) {
    
    /* $(".btn-second").click(function() {
         $('html, body').animate({
           scrollTop: $(".third_section").offset().top
          }, 2000);

      });

      $(".btn-myself").click(function() {
         $('html, body').animate({
           scrollTop: $(".myself_section").offset().top
          }, 2000);

      });

      $(".btn-selectone").click(function() {
         $('html, body').animate({
           scrollTop: $(".select_one_section").offset().top
          }, 2000);

      });

      $(".btn-first").click(function() {
          $('html, body').animate({
          scrollTop: $(".top_first").offset().top
          }, 2000);

      });

       $(".btn-select-one").click(function() {
          $('html, body').animate({
           scrollTop: $(".top_first").offset().top
          }, 2000);

      });*/
      $(".btn-first").click(function() {


        $(".first_section").fadeOut(1000,function(){
            
          $('.top_first').hide();
          $('.myself_section').show();
          $('html, body').animate({
              scrollTop: $('.myself_section').offset().top
          }, 'slow');
          $('.select_one_section').hide();
          $('.third_section').hide();
          $("#name-error").hide();
          $('.error').hide(); 
          $(".btn-second").attr('disabled', 'disabled');
          $(".btn-second").addClass('disable');
          $(".btn-selectone").attr('disabled', 'disabled');
          $(".btn-selectone").addClass('disable');
          $("#top").removeClass('top-head');
          $("#top").addClass('top-head-second');
            
        });
        //$('.first_section').hide();
       });

      $(".btn-select-one").click(function() {

        $(".first_section").fadeOut(1000,function(){
            
          $('.first_section').hide();
          $('.select_one_section').show();
          $('html, body').animate({
              scrollTop: $('.select_one_section').offset().top
          }, 'slow');
          $('.myself_section').hide();
          $('.third_section').hide();
          $("#name-error").hide();
          $('.error').hide(); 
          $(".btn-second").attr('disabled', 'disabled');
          $(".btn-second").addClass('disable');
          $(".btn-selectone").attr('disabled', 'disabled');
          $(".btn-selectone").addClass('disable');
          $("#top").removeClass('top-head');
          $("#top").addClass('top-head-second');
            
        });
       });

      $(".btn-myself").click(function() {

        $(".myself_section").fadeOut(1000,function(){
              $('.second_section').show();
            // alert("The paragraph is now hidden");
            $('html, body').animate({
             scrollTop: $('.second_section_main').offset().top
            }, 'slow');

          $('.first_section').hide();
          $('.select_one_section').hide();
          $('.third_section').hide();

        

            
        });
       // $('.myself_section').hide();
       

        // $('html, body').animate({
        //     scrollTop: $('.second_section_main').offset().top
        // }, 'slow');
      });

      $(".btn-selectone").click(function() {

        $(".select_one_section").fadeOut(1000,function(){
            
        $('.first_section').hide();
        $('.second_section').show();
       
        $('html, body').animate({
            scrollTop: $('.second_section').offset().top
         }, 'slow');
        $('.myself_section').hide(); 
        $('.select_one_section').hide();
        $('.third_section').hide();
            
        });
       }); 


      $(".btn-second").click(function() {
        
      //$('.third_section').hide();
         $('html, body').animate({
             scrollTop: $('.third_section').offset().top
        }, 'slow'); 
         
        $(".second_section").fadeOut(1000,function(){
          // alert("asdfdsa");
         $('.first_section').hide();
         $('.third_section').show();
         $('.second_section').hide();           
            
        });
      }); 
      
  }
  else
  {

      
      $(".btn-first").click(function() {
        $('.first_section').hide();
        $('.myself_section').show();
        $('.select_one_section').hide();
        $('.third_section').hide();
        $("#name-error").hide();
        $('.error').hide(); 
        $(".btn-second").attr('disabled', 'disabled');
        $(".btn-second").addClass('disable');
        $(".btn-myself").attr('disabled', 'disabled');
        $(".btn-myself").addClass('disable');
        $("#top").removeClass('top-head');
        $("#top").addClass('top-head-second');
        return false;

       });
      $(".btn-select-one").click(function() {
          $('.first_section').hide();
          $('.select_one_section').show();
          $('.myself_section').hide();
          $('.third_section').hide();
          $("#name-error").hide();
          $('.error').hide(); 
          $(".btn-second").attr('disabled', 'disabled');
          $(".btn-second").addClass('disable');
          $(".btn-selectone").attr('disabled', 'disabled');
          $(".btn-selectone").addClass('disable');
          $("#top").removeClass('top-head');
          $("#top").addClass('top-head-second');
          return false;

       });

      $(".btn-myself").click(function() {
        $('.first_section').hide();
        $('.second_section').show();
        $('.third_section').hide();
        $('.myself_section').hide(); 
        $('.select_one_section').hide();
       }); 
      $(".btn-selectone").click(function() {
          $('.first_section').hide();
          $('.second_section').show();
          $('.third_section').hide();
          $('.myself_section').hide(); 
          $('.select_one_section').hide();
      });

  } 



