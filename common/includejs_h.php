<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  for avtar -->
<script src="<?php echo SITE_URL; ?>assets/js/avtar/initial.js" type="text/javascript"></script>

<!--  Forms Validations Plugin -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery.validate.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo SITE_URL; ?>assets/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap-selectpicker.js"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

<!--  Charts Plugin -->
<script src="<?php echo SITE_URL; ?>assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="<?php echo SITE_URL; ?>assets/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery-jvectormap.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Wizard Plugin    -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  bootstrap Table Plugin    -->
<script src="<?php echo SITE_URL; ?>assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="<?php echo SITE_URL; ?>assets/js/jquery.datatables.js"></script>


<!--  Full Calendar Plugin    -->
<script src="<?php echo SITE_URL; ?>assets/js/fullcalendar.min.js"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="<?php echo SITE_URL; ?>assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo SITE_URL; ?>assets/js/demo.js"></script>

<!-- Tool tip js and css -->
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>assets/css/tooltip/tooltipster.bundle.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>assets/css/tooltip/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-noir.min.css" />

<script type="text/javascript" src="<?php echo SITE_URL; ?>assets/js/tooltip/tooltipster.bundle.min.js"></script>
<!-- Tool tip js and css -->
<div id="popover_content_wrapper" style="display: none">
  	<ul class="nav">
	    <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/sendtodoctor.php" ) { ?> class="active"
	        <?php   }  ?>>
	        <a href="sendtodoctor.php">
	            <p>FREQUENTLY ASKED QUESTIONS</p>
	        </a>
	    </li>
	    <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/sendtodoctor.php" ) { ?> class="active"
	        <?php   }  ?>>
	        <a href="sendtodoctor.php">
	            <p>SHARE RISE WITH YOUR DOCTOR</p>
	        </a>
	    </li>
	    <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/talktodoctor.php" ) { ?> class="active"
	        <?php   }  ?>>
	        <a href="talktodoctor.php">
	            <p>SPEAK WITH A DOCTOR</p>
	        </a>
	    </li>
	</ul>
</div>
<style>
    .scrolloff {
        pointer-events: none;
    }
</style>
<script type="text/javascript">

	page_name = '<?php echo isset($page_name)?$page_name:'' ?>';
	readAt = '<?php echo isset($readAt)?$readAt:'0'  ?>'


	function updatePrograceBar(){

		$('.procres_block').each(function(){
			perDiv = $(this).find('.persentage');
			finish = $(this).find('.icon-check-alt');

    		if(perDiv !== 'undefind'){
    			total = parseInt($(perDiv).attr('total'));
    			now =parseInt($(perDiv).attr('now'));    			

    			percentage = now*100/total;

    			clp1 = $(this).find('.clip1').find('.slice1');
    			clp2 = $(this).find('.clip2').find('.slice2');
    		
    			deg1 = 0;
				deg2 = 0;
    			if(!isNaN(percentage)){
	    			if(percentage < 50){
						deg1 = percentage*360/100;
						if(deg1 > 180){
							deg1 = 180;
						}					
	    				deg2 = 0;    				
	    			}else{
	    				deg1 = 180;
	    				deg2 = (percentage-50)*360/100;    		    				
	    			}
    			}
				$(clp1).css({ WebkitTransform: 'rotate('+deg1+'deg)'});      				
       			$(clp1).css({ '-moz-transform': 'rotate('+deg1+'deg)'});
				$(clp2).css({ WebkitTransform: 'rotate('+deg2+'deg)'});      				
       			$(clp2).css({ '-moz-transform': 'rotate('+deg2+'deg)'});

       			if(total == now){       				
       				$(perDiv).hide()
       				$(finish).show()
       			}


    			//$(this).html(now*100/total);
    		}
    	})
	}
	function udatePrograce(page_name,dataid){
		$.ajax({
		    url: "<?php echo SITE_URL ?>udate_prograce.php?",
		    data:"page_name="+page_name+"&dataid="+dataid,
		    method:'post',		    
		    success: function(data){		     	

		    	if(data == 'update'){
		    		el = $('#'+page_name).find('.persentage');
		    		if(el !== 'undefined'){
		    			$(el).attr('now',dataid-1);
		    		}
		     		updatePrograceBar() 
		     	}
		    }
		});
	}

	$( document ).ready(function() {
					
		//hide all chapter					
		$('.scroll').hide();
		//show only first chapter
    	$('.first').show();

		var win = $('.main-panel');			
		var updateRuning = false;

		//continue scrolling
		win.scroll(function() {  

			wH = $(window).height()						
			winHeight = $('.navbar').outerHeight(true)+$('.content').outerHeight(true);

			h = winHeight - $('.footer').height();					
			if(h <= (wH+win.scrollTop()) && !updateRuning){

				updateRuning = true;
				console.log('inside')
				$('.scroll').each(function(){
					if(!$(this).is(':visible')){
						updateRuning = false;
						dataid = $(this).attr('data-type');						
						udatePrograce(page_name,dataid)
						$(this).show();
						console.log($(this))
						last = $(this);
						return false; 
					}	
				})

				if(updateRuning){					
					dataid = $(last).attr('data-type');
					dataid = parseInt(dataid) + 1				
					udatePrograce(page_name,dataid)					
				}
		
			}						
		})


		//scroll to last read chapter
		if(readAt>0){
			readAt = readAt+1;
			dataid = 1;
    		for (var i = 1; i <= readAt; i++) {    			
    			dataid = i;    			
    			$('[data-type='+i+']').show();	
    			moveAt = $('[data-type='+i+']');	
    		}
    		
    		$('.main-panel').animate({
        		scrollTop: $(moveAt).offset().top
    		}, 2000);
    		
    	}

    	updatePrograceBar();    			
   

    	/* update progress bar */

	    $('.profile').initial({charCount:2,height:58,width:58,fontSize:24,fontWeight:400});  
	    $('.tooltip').tooltipster({
		    theme: 'tooltipster-noir',
		    maxWidth: 400,
			side: 'right'
		});
		//$('#example').popover(options);
		$(function(){
		    $('[rel=popover]').popover({ 
		      html : true, 
		      content: function() {
		        return $('#popover_content_wrapper').html();
		      }
		    });
		  });

		$(document).find('iframe').mouseenter(function() {

		 	console.log($('.main-panel'));//.style['overflow']="auto";
		 	//$('.main-panel').css({overflow: 'scroll'});
		});



        // you want to enable the pointer events only on click;

        $('.video-container').find('iframe').addClass('scrolloff'); // set the pointer events to none on doc ready

        $('.video-container').on('click', function () {
            $('.video-container').find('iframe').removeClass('scrolloff'); // set the pointer events true on click
        });

        // you want to disable pointer events when the mouse leave the canvas area;

        $(".video-container").mouseleave(function () {
            $('.video-container').find('iframe').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
        });
    



	});  
</script>