



<div class="sidebar" data-color="orange" data-image="../assets/img/full-screen-image-3.jpg">
    <div class="sidebar-wrapper">

        <div class="user">
            <div class="photo">
                <!-- <img src="../assets/img/default-avatar.png" /> -->
                
                <img data-name="<?php echo isset($user_obj->user_name)?$user_obj->user_name:'';?>" class="profile"/> 

            </div>
            <div class="info">
                <a href="settings.php"><?php echo isset($user_obj->user_name)?$user_obj->user_name:'Me';?></a>
            </div>
            <div class="">
                <p class="quote">Life is beautiful when you realize it. Grateful for it</p>
            </div>
        </div>



        <ul class="nav nav-method">

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/thestory.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="thestory.php">
                    <i class="pe-7s-map"></i>
                    <p>THE STORY BEHIND</p>
                </a>
            </li>
            <br/>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/introduction.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="introduction.php">
                    <i class="pe-7s-map"></i>
                    <p>THE CAUSES OF ASTHMA</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/vitamind.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="vitamind.php">
                    <i class="pe-7s-sun"></i>
                    <p>RI-VITAMIND</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/omega3.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="omega3.php">
                    <i class="pe-7s-graph"></i>
                    <p>RI-OMEGA3</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/homeostasis.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="homeostasis.php">
                    <i class="pe-7s-loop"></i>
                    <p>INCREASE HOMEOSTASIS</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/eliminategutpressure.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="eliminategutpressure.php">
                    <i class="pe-7s-leaf"></i>
                    <p>ELIMINATE GUT PRESSURE</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/managestress.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="managestress.php">
                    <i class="pe-7s-smile"></i>
                    <p>MANAGE STRESS</p>
                </a>
            </li>

            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/summary.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="summary.php">
                    <i class="pe-7s-date"></i>
                    <p>EVERYTHING TOGETHER</p>
                </a>
            </li>


            <br/>
            <li>
                <a data-toggle="collapse" href="#help">
                    <i class="pe-7s-help1"></i>
                    <p>HELP CENTER
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="help">
                    <ul class="nav">

                        <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/sendtodoctor.php" ) { ?> class="active"
                            <?php   }  ?>>
                            <a href="sendtodoctor.php">
                                <p>FREQUENTLY ASKED QUESTIONS</p>
                            </a>
                        </li>
                        <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/sendtodoctor.php" ) { ?> class="active"
                            <?php   }  ?>>
                            <a href="sendtodoctor.php">
                                <p>SHARE RISE WITH YOUR DOCTOR</p>
                            </a>
                        </li>
                        <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/talktodoctor.php" ) { ?> class="active"
                            <?php   }  ?>>
                            <a href="talktodoctor.php">
                                <p>SPEAK WITH A DOCTOR</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li <?php if($_SERVER[ 'SCRIPT_NAME']=="/forum.php" ) { ?> class="active"
                <?php   }  ?>>
                <a href="forum.php">
                    <i class="pe-7s-date"></i>
                    <p>COMMUNITY FORUM</p>
                </a>
            </li>
        </ul>


    </div>
</div>


