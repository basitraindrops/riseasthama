<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="notification">0</span>
        <p class="hidden-md hidden-lg">
            Notifications
            <b class="caret"></b>
        </p>
    </a>
    <ul class="dropdown-menu">
        <li><a href="#">You don't have any notifications</a></li>

    </ul>
</li>
