<nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">

                        <div class=" col-md-1 navbar-minimize">

                            <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
                        <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                        <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                    </button>
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <div class="col md-2 collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <?php require("common/notifications.php"); ?>
                                <?php require("common/topsettings.php"); ?>

                            </ul>

                        </div>
                    </div>

                </div>
                

            </nav>