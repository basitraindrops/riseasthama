<?php
require_once(ROOT_DIR."/admin/inc/class/front_quote.class.php");
$pb=new front_quote($mysqli);

$quoteToShow = $pb->getRandomQuote();

?>

<div class="sidebar" data-color="orange" data-image="assets/img/full-screen-image-3.jpg">
    <div class="sidebar-wrapper" id="div_main_effct">

        <div class="user">
            <a href="dashboard.php">
                <div class="photo">                            
                    <?php if(isset($user_obj->profile_pic) && $user_obj->profile_pic){ ?>
                    <div class="avtar-prof img" style="background-image:url('<?php echo 'img/profile_pic/'.$user_obj->profile_pic; ?>');">
                       <!-- <img src="<?php //echo 'img/profile_pic/'.$user_obj->profile_pic; ?>"> -->
                    </div>                                                 
                    <?php }else{ ?>
                    <div class="avtar">
                        <img data-name="<?php echo isset($user_obj->user_name)?$user_obj->user_name:'';?>" class="profile"/> 
                    </div>
                    <?php } ?>
                </div>
                <div class="info">
                    <?php echo isset($user_obj->user_name)?$user_obj->user_name:'Me';?>
                </div>
            </a>
            <div class="">
                <p class="quote"><?php echo $quoteToShow; ?></p>
            </div>
        </div>

        <ul class="nav nav-method" id="ul_main">

            <li id="THE_STORY_BEHIND" <?php if($_SERVER[ 'SCRIPT_NAME']=="/thestory.php" || (isset($page_name) && $page_name == 'THE_STORY_BEHIND') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>


                <div class="link_block">
                    <a href="thestory.php">
                        <i class="pe-7s-map"></i>
                        <p>THE STORY BEHIND</p>
                    </a>
                </div>
                <div class="procres_block">
                <?php

                if(isset($tabDetail['THE_STORY_BEHIND']) && $tabDetail['THE_STORY_BEHIND']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['THE_STORY_BEHIND'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_STORY_BEHIND']['total_chapter'].'" now="'.$userTabDetail['THE_STORY_BEHIND']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_STORY_BEHIND']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }
                ?>
                </div>
                
            </li>

            <li id="THE_CAUSES_OF_ASTHMA" <?php if($_SERVER[ 'SCRIPT_NAME']=="/introduction.php"  || (isset($page_name) && $page_name == 'THE_CAUSES_OF_ASTHMA') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>

                <div class="link_block">
                    <a href="introduction.php">
                        <i class="pe-7s-map"></i>
                        <p>THE CAUSES OF ASTHMA</p>
                    </a>
                </div>
                <div class="procres_block">
                <?php

                if(isset($tabDetail['THE_CAUSES_OF_ASTHMA']) && $tabDetail['THE_CAUSES_OF_ASTHMA']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['THE_CAUSES_OF_ASTHMA'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_CAUSES_OF_ASTHMA']['total_chapter'].'" now="'.$userTabDetail['THE_CAUSES_OF_ASTHMA']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['THE_CAUSES_OF_ASTHMA']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>

            </li>

            <li id="RI-VITAMIND" <?php if($_SERVER[ 'SCRIPT_NAME']=="/vitamind.php"  || (isset($page_name) && $page_name == 'RI-VITAMIND') ) { ?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>
                <div class="link_block">
                    <a href="vitamind.php">
                        <i class="pe-7s-sun"></i>
                        <p>RI-VITAMIND</p>
                    </a>
                </div>

                <div class="procres_block">
                <?php

                if(isset($tabDetail['RI-VITAMIND']) && $tabDetail['RI-VITAMIND']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['RI-VITAMIND'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-VITAMIND']['total_chapter'].'" now="'.$userTabDetail['RI-VITAMIND']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-VITAMIND']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>

            </li>

            <li id="RI-OMEGA3" <?php if($_SERVER[ 'SCRIPT_NAME']=="/omega3.php"  || (isset($page_name) && $page_name == '') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>

                <div class="link_block">
                    <a href="omega3.php">
                        <i class="pe-7s-graph"></i>
                        <p>RI-OMEGA3</p>
                    </a>
                </div>
                <div class="procres_block">
                <?php

                if(isset($tabDetail['RI-OMEGA3']) && $tabDetail['RI-OMEGA3']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['RI-OMEGA3'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-OMEGA3']['total_chapter'].'" now="'.$userTabDetail['RI-OMEGA3']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['RI-OMEGA3']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>
            </li>

            <li ID="INCREASE_HOMEOSTASIS" <?php if($_SERVER[ 'SCRIPT_NAME']=="/homeostasis.php"  || (isset($page_name) && $page_name == '') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>

                <div class="link_block">
                    <a href="homeostasis.php">
                        <i class="pe-7s-loop"></i>
                        <p>INCREASE HOMEOSTASIS</p>
                    </a>
                </div>

                <div class="procres_block">
                <?php

                if(isset($tabDetail['INCREASE_HOMEOSTASIS']) && $tabDetail['INCREASE_HOMEOSTASIS']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['INCREASE_HOMEOSTASIS'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['INCREASE_HOMEOSTASIS']['total_chapter'].'" now="'.$userTabDetail['INCREASE_HOMEOSTASIS']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['INCREASE_HOMEOSTASIS']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>
            </li>

            <li id="ELIMINATE_GUT_PRESSURE" <?php if($_SERVER[ 'SCRIPT_NAME']=="/eliminategutpressure.php"  || (isset($page_name) && $page_name == '') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>
                <div class="link_block">
                    <a href="eliminategutpressure.php">
                        <i class="pe-7s-leaf"></i>
                        <p>ELIMINATE GUT PRESSURE</p>
                    </a>
                </div>
                <div class="procres_block">
                <?php

                if(isset($tabDetail['ELIMINATE_GUT_PRESSURE']) && $tabDetail['ELIMINATE_GUT_PRESSURE']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['ELIMINATE_GUT_PRESSURE'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['ELIMINATE_GUT_PRESSURE']['total_chapter'].'" now="'.$userTabDetail['ELIMINATE_GUT_PRESSURE']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['ELIMINATE_GUT_PRESSURE']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>
            </li>

            <li id="MANAGE_STRESS" <?php if($_SERVER[ 'SCRIPT_NAME']=="/managestress.php"  || (isset($page_name) && $page_name == '') ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>
                <div class="link_block">
                    <a href="managestress.php">
                        <i class="pe-7s-smile"></i>
                        <p>MANAGE STRESS</p>
                    </a>
                </div>

                <div class="procres_block">
                <?php

                if(isset($tabDetail['MANAGE_STRESS']) && $tabDetail['MANAGE_STRESS']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['MANAGE_STRESS'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['MANAGE_STRESS']['total_chapter'].'" now="'.$userTabDetail['MANAGE_STRESS']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['MANAGE_STRESS']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>

                </div>
            </li>

            <li id="EVERYTHING_TOGETHER" <?php if($_SERVER[ 'SCRIPT_NAME']=="/summary.php" ) {?> class="active clearfix"
                <?php  }else{ ?> class="clearfix" <?php } ?>>
                <div class="link_block">
                    <a href="summary.php">
                        <i class="pe-7s-date"></i>
                        <p>EVERYTHING TOGETHER</p>
                    </a>
                </div>
                <div class="procres_block">
                <?php

                if(isset($tabDetail['EVERYTHING_TOGETHER']) && $tabDetail['EVERYTHING_TOGETHER']){

                    echo '<div class="lesson-link__progress brand--color brand--beforeBackground">
                        <div class="pie-progress pie-progress--progressed">
                            <i class="pie-progress__marker" style="display: none;"></i>
                            <i class="icon icon-check-alt" style="display: none;"></i>';
                        if(isset($userTabDetail['EVERYTHING_TOGETHER'])){
                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['EVERYTHING_TOGETHER']['total_chapter'].'" now="'.$userTabDetail['EVERYTHING_TOGETHER']['no_chapter_read'].'">';                    

                        }else{

                            echo '<div class="persentage pie-progress__progress pie-progress__progress--visible" total="'.$tabDetail['EVERYTHING_TOGETHER']['total_chapter'].'" now="">';
                        }

                    echo '  <div class="clip1">
                                <div class="slice1 brand--background brand--border" style="transform: rotate(0deg);">
                                </div>
                            </div>
                            <div class="clip2">
                                <div class="slice2 brand--background brand--border" style="transform: rotate(0deg);"></div>
                            </div>
                        </div>
                       </div>
                    </div>';

                }

                ?>
                </div>
            </li>

          <li id="HELP_CENTER" <?php if((isset($page_name) && $page_name == 'FAQ') || (isset($page_name) && $page_name == 'TALKTODOCTOR')|| (isset($page_name) && $page_name == 'SENDTODOCTOR') ) { ?> class="active help_center" <?php } else { ?> class="help" <?php } ?> >
                
                <div class="collapse" id="help">
                    <ul class="nav">

                        <li <?php if($_SERVER['SCRIPT_NAME']=="/faq.php" ) { ?> 
                            <?php   }  ?>>
                            <a href="faq.php">
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                <p>FREQUENTLY ASKED QUESTIONS</p>
                            </a>
                        </li>
                        <li <?php if($_SERVER['SCRIPT_NAME']=="/sendtodoctor.php" ) { ?> 
                            <?php   }  ?>>
                            <a href="sendtodoctor.php">
                              <i class="fa fa-hand-o-right" aria-hidden="true"></i> 
                                <p>SHARE RISE WITH YOUR DOCTOR</p>
                            </a>
                        </li>
                        <li <?php if($_SERVER['SCRIPT_NAME']=="/talktodoctor.php" ) { ?> 
                            <?php   }  ?>>
                            <a href="talktodoctor.php">
                             <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                <p>SPEAK WITH A DOCTOR</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <a data-toggle="collapse" href="#help">
                    <i class="pe-7s-help1"></i>
                    <p>HELP CENTER
                        <b class="caret"></b>
                    </p>
                </a>
            </li> 
            <li <?php if($_SERVER['SCRIPT_NAME']=="/forum.php" || (isset($page_name) && $page_name == 'FORUM')) {?> class="active clearfix community_forum"
                <?php  }else{ ?> class="clearfix community" <?php } ?>>
                <a href="forum.php">
                    <i class="pe-7s-date"></i>
                    <p>COMMUNITY FORUM</p>
                </a>
            </li>
        </ul>


    </div>
</div>

<style type="text/css">
    
.avtar-prof.img {
  background-position: center center !important;
  background-size: cover !important;
  height: 70px;
  width: 70px;
}

</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$( document ).ready(function() {
   
    $('li#HELP_CENTER').click( function() {
    $("ul#ul_main").toggleClass("current");
    $("#div_main_effct").toggleClass("current");
});
});

</script>

