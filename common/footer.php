<footer class="footer">
    <div class="container-fluid">
        <div class="col-md-10 col-md-offset-1">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="#">
                                Home
                            </a>
                    </li>

                    <!--        here you can add more links for the footer                       -->
                </ul>
            </nav>
            <p class="copyright pull-right">
                &copy; 2017 <a href="http://riseasthmamethod.com">riseasthmamethod.com</a>
            </p>
        </div>
    </div>
</footer>
