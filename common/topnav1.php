<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">

            <div class=" col-md-1 navbar-minimize">

                <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
						<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
						<i class="fa fa-navicon visible-on-sidebar-mini"></i>
					</button>
            </div>
            <div class="col-md-9 nav-container">
                <ul class="nav nav-icons" role="tablist">
                    <li class="active">
                        <a href="#fullchapter" role="tab" data-toggle="tab">
                                       
                                       Full chapter
                                    </a>
                    </li>
                    <li>
                        <a href="#textsummary" role="tab" data-toggle="tab">
                                   
                                        Text summary
                                    </a>
                    </li>
                    <li>
                         <a href="#faq_detail" role="tab" data-toggle="tab" id="faq">
                                   
                                        FAQ 
                                    </a>
                    </li>


                </ul>
            </div>
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
            <div class="col md-2 collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <?php require("common/notifications.php"); ?>
                    <?php require("common/topsettings.php"); ?>


                </ul>

            </div>
        </div>

    </div>

</nav>
