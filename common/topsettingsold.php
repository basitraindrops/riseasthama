<li class="dropdown dropdown-with-icons">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-list"></i>
        <p class="hidden-md hidden-lg">
            More
            <b class="caret"></b>
        </p>
    </a>
    <ul class="dropdown-menu dropdown-with-icons">
        <li>
            <a href="#">
                <i class="pe-7s-help1"></i> FAQ
            </a>
        </li>
        <li>
            <a href="/sendtodoctor.php">
                <i class="pe-7s-help1"></i> Send to doctor
            </a>
        </li>
        <li>
            <a href="/settings.php">
                <i class="pe-7s-tools"></i> Settings
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="#">
                <i class="pe-7s-lock"></i> Chat
            </a>
        </li>
        <li>
            <a href="logout.php" class="text-danger">
                <i class="pe-7s-close-circle"></i> Log out
            </a>
        </li>
    </ul>
</li>
