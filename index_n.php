<?php 
    include 'inc/config.php'; // Configuration php file
    require(INC_DIR.'init.php');
    if($sessionObj->read('front_user_email')<>"")
    {
    	header("Location:dashboard.php");
    }
    require(CLASS_DIR.'encryption.class.php');
    require_once(CLASS_DIR.'security.class.php');
?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="assets/img/favicon.png" />
        <title>RISE Asthma Method</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />

        <!-- Fonts and Icons -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/themify-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url('assets/img/backgroundregister.png')">
        <!--   Creative Tim Branding   -->
            <div class="logo-container">
                <div class="logo"></div>
            </div>

            <div id="terms-modal" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2>Terms and conditions</h2>
                        </div>
                    </div>
                </div>
            </div>


            <!--   Big container   -->
            <div class="container">
                <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="azure" id="wizard">
                                <form method="post" action="intro.php" id="payment-form">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->
                                    <div class="wizard-header">
                                        <div class="wizard-navigation">
                                            <div class="progress-with-circle">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <a href="#captain" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-list"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#details" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-briefcase"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#description" id="disbaleClick" data-toggle="tab">
                                                        <div class="icon-circle">
                                                            <i class="ti-pencil"></i>
                                                        </div>

                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="tab-content">
                                        <div class="tab-pane" id="captain">

                                            <h2 class="info-text">Welcome to RISE</h2>
                                            <h4 class="info-text">Who needs help?</h4>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <div class="col-sm-4 col-sm-offset-2">
                                                        <div class="choice" data-toggle="wizard-checkbox">
                                                            <input type="checkbox" name="jobb" value="Design">
                                                            <div class="btn-next card card-checkboxes card-hover-effect">

                                                                <p>Myself</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="choice" data-toggle="wizard-checkbox">
                                                            <input type="checkbox" name="jobb" value="Design">
                                                            <div class="btn-next card card-checkboxes card-hover-effect">
                                                                <p>A loved one</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="details">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h2 class="info-text"> Your RISE profile</h2>
                                                </div>
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <label>WHAT IS YOUR NAME?</label>
                                                        <input type="text" class="form-control" id="inputname" name="inputname" required= "true" placeholder="e.g John Doe"> 
                                                    </div>
                                                    <div class="form-group">
                                                        <label>WHAT IS YOUR EMAIL ADDRESS?</label>
                                                        <input name="email" type="email" class="form-control" placeholder="john@gmail.com">
                                                    </div>
                                                    <div class="form-button" style="max-width: 200px;margin: 0 auto;">
                                                        <input type='button' class='btn-next btn btn-fill btn-warning btn-wd' name='next' value='Next' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="description">
                                            <div class="row">
                                                <h2 class="info-text">Last step - secure payment </h2>
                                                <h5 class="info-text">We charge $49 and only after 30 days from signing up, <br/>to make sure that you are satisfied first.</h5>
                                                <div class="col-sm-6 col-sm-offset-1">
                                                    <div class="paymentwrapper">                                                    
                                                        <img class="paymentmethod" src="assets/img/creditcard.png">
                                                        <div class="col-sm-12 form-fields">
                                                            <input type="text" name="stripe_number"  onfocus="" placeholder="Card number." class="form-control" style="background-color: white; " onkeypress='' id="credit_card">
                                                        </div> 

														<div class="col-sm-6 form-fields paymentmethod-expirydate"> 
                                                            <input type="text" name="stripe_expirydate"  placeholder="MM/YY" class="form-control expirydate" maxlength="5" id="expirydate" onkeypress='return validateExpire(event);' style="background-color: white;">
                                                        </div> 

                                                        <div class="col-sm-6 form-fields paymentmethod-creditvalidation">
                                                                <input type="text" name="stripe_cvc"  placeholder="CVV" class="form-control credit-validation " onkeypress='return validateExpire(event);' maxlength="3" minlength="3" style="background-color: white;">
                                                        </div>

                                                        <div id="card-errors" class="error">

                                                            
                                                            <label id="credit_card-error" class="error col-sm-12" for="credit_card"></label>
                                                                       
                                                            <label id="expirydate-error" class="error col-sm-12" for="credit_card"></label>

                                                            <label id="cvv-error" class="error col-sm-12" for="credit_card"></label>
                                                           

                                                        </div>
                                                        <div class="form-button">
                                                            <button type="submit" name="paymentwith" value="stripes" class="btn btn-fill btn-warning btn-wd" id="proceed" >Pay $49</button>
                                                            <div align="center"><b>Or</b></div>
                                                            <button name="paymentwith" id="paypal" type="button" value="paypal" class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button> 
                                                        </div>
                                                        <div class="terms">
                                                            <h5 class="info-text">By proceeding, you are agree with our
                                                                <a href="#terms-modal" data-toggle="modal"> terms and conditions</a></h5>

                                                        </div>                                                    
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-4 col-sm-offset-1">
                                                    <div class="form-group">
                                                        <img src="assets/img/secure.png">
                                                        <p class="description"></p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Lifetime refund policty</label>
                                                        <p class="description">If at any point you feel that the program is not useful then we will refund the entire amount. No questions asked</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>You cannot afford $49?</label>
                                                        <p class="description">Send us a message at hope@riseasthma.com, briefly describe your situation to us and we will give you access for free</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </form>
                                <div class="wizard-footer col-md-4 col-md-offset-4">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                
                            
                        </div>
                    </div>
                    <!-- wizard container -->
                </div>
            </div>
            <!-- row -->
            <div class="footer">
                <div class="container text-center">
                    Already have an account? Sign in <a href="<?php echo SITE_URL; ?>page_login.php">here.</a>
                </div>
                <div>
                    <a href="<?php echo SITE_URL;?>"><img src="assets/img/logo.png" class="img-responsive logo-footer"></a>
                </div>
            </div>
        </div>
        <!--  big container -->

        
    </div>


<!--   Core JS Files   -->
<script src="https://js.stripe.com/v2/"></script>
<!-- <script src="https://js.stripe.com/v3/"></script> -->
      
    <style type="text/css">
.field {
  background: white;
  box-sizing: border-box;
  font-weight: 400;
  border: 1px solid #CFD7DF;
  border-radius: 3px;
  color: #32315E;
  outline: none;
  height: 48px;
  line-height: 48px;
  padding: 0 20px;
  cursor: text;
  width: 76%;
}

.field::-webkit-input-placeholder { color: #CFD7DF; }
.field::-moz-placeholder { color: #CFD7DF; }
.field:-ms-input-placeholder { color: #CFD7DF; }

.field:focus,
.field.StripeElement--focus {
  border-color: #F99A52;
}
        </style>
<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC_KEY;?>');
//Stripe.setPublishableKey('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
var stripeResponseHandler = function(status, response) {
    var $form = $('#payment-form');
    if (response.error) {
        $("#card-errors").show();

        $("#credit_card-error").html('Please Enter Correct Detail!');
        $("#credit_card-error").show();
        $form.find('button').prop('disabled', false);
    } else {
       
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        $("#credit_card-error").html('');        
        $("#credit_card-error").hide();
        $form.get(0).submit();
    }
};

function validateNumber(number){
  
    error = true
    number = number.trim();
    var count = number.length;              

    if(count == 19){
        sp = number.split(" ").length - 1;    
        if(sp == 3){
            error = false;
        }
    }

    
    if(error){
        $("#credit_card-error").html('Please Enter Valid Card Number!');
        $("#credit_card-error").show();
    }else{
        $("#credit_card-error").html('');
        $("#credit_card-error").hide();
    }

    return error;
}

function validateExpiry(expiry){
    error = false
    res = '';
    count = expiry.length;

    if(count == 5){
        sp = expiry.split("/").length - 1;   
        if(sp != 1){
           error = true; 
           res = ' lenght ';
        }
    }else{
        error = true; 
        res = ' lenght2 ';
    }    
    if(!error){

        var $split = expiry.split("/");

        if($split[0].length != 2 || $split[0] > 12){
            error = true; 
            res = ' lenght3 ';
        }

        if(!error){
            var year = new Date().getFullYear().toString().substr(-2);
            if($split[1] < year){
               error = true; 

               res = ' lenght5 ';
            }
        }        
    }
    if(error){
        $("#expirydate-error").html('Please enter valid Expiry Date!');
        $("#expirydate-error").show();
    }else{
        $("#expirydate-error").html('');
        $("#expirydate-error").hide();
    }

    return error;
}
function validateCVV(cvv){

    error = false

    count = cvv.length;
    if(count != 3){
        error = true
    }

    if(error){
        $("#cvv-error").html('Please enter valid CVV !');
        $("#cvv-error").show();
    }else{
        $("#cvv-error").html('');
        $("#cvv-error").hide();
    }
    return error;

}
jQuery(function($) {
    $('#payment-form').submit(function(e) {

        if($('#payment-form').attr('action') == 'intro.php'){
            var $form = $(this);
            var expiry = $("#payment-form :input[name='stripe_expirydate']").val()
            var stripe = expiry.split("/");
            var cvv = $("#payment-form :input[name='stripe_cvc']").val();
            
            error = false;
            //$("#card-errors").hide();

            number = $("#payment-form :input[name='stripe_number']").val();

            errorNumber = validateNumber(number);            
            errorExpiry = validateExpiry(expiry);
            errorCvv = validateCVV(cvv);


            if(errorNumber || errorExpiry || errorCvv){
                $("#card-errors").show();
                return false
            }else{

                $("#card-errors").hide();
                $form.find('button').prop('disabled', true);
                Stripe.card.createToken({ 
                    number: $("#payment-form :input[name='stripe_number']").val(),//values['stripe_number'],
                    cvc: $("#payment-form :input[name='stripe_cvc']").val(),
                    exp_month:stripe[0] ,
                    exp_year: stripe[1]
                }, stripeResponseHandler);
                return false;
            }
            
        }
    });
});

function validateExpire(event) {
    //var key = window.event ? event.keyCode : event.which;
    var key = event.keyCode || event.which;
    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if (key > 57 ) {
        return false;
    }
    else return true;
};
/*
function validateCvv(event) {
    var key = event.keyCode || event.which;
    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if (key > 57 || key == 191 ) {
        return false;
    }
    else return true;
};*/

function validateCcnumber(event) {
    var key = event.keyCode || event.which;
    
    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39 ) {
        return true;
    }
    else if (key > 57 ) {
        return false;
    }
    else {
        return true;
    }
};
$('document').ready(function(){
    $("#paypal").click(function(){
        var datastring = $("#payment-form").serialize();
        $('#payment-form').attr('action', "paypal.php");
        $("#payment-form").submit();
    });


    $(document).on('blur',"#payment-form :input[name='stripe_expirydate']", function(e){
        validateExpiry($(this).val())
    })
    $(document).on('keyup',"#payment-form :input[name='stripe_expirydate']", function(e){

        var count = $(this).val().length;
        val = $(this).val().replace(/[^0-9/]/g, '');
        
        if(!($(this).val() == val)){
            $(this).val(val)
            return
        }
        console.log(count+'  cpount')
        if(count > 5){
             $(this).focus().val($(this).val().slice(0, 5))              
        }

    
        if(( parseInt($(this).val()) > 12 || $(this).val() == 00 ) && count == 2 ){

          

           $("#card-errors").show();           

           $("#expirydate-error").html('Please Enter Valid Month');
           $("#expirydate-error").show();
           $(this).focus()
        }else if(count == 2){// && e.keyCode != 8){
            
            if($(this).val().indexOf('/') < 0){
                $(this).focus().val(function( index, value ) { return value + "/" ;   } );
            }else{
                $(this).focus().val(function( index, value ) { return "0"+value ;   } );
            }           
            $("#expirydate-error").html('');
            $("#expirydate-error").hide();
        }else{
            if($(this).val().indexOf('/')){
                var $split = $(this).val().split("/");
                var year = new Date().getFullYear().toString().substr(-2);
                if( count == 5 && $split[1] < year){
                    $(this).focus()
                    $("#card-errors").show();
                    $("#expirydate-error").html('Please Enter Valid Year');
                    $("#expirydate-error").show();
                }else{
                    $("#expirydate").html('');
                    $("#expirydate-error").hide()
                }
            }
        }

    });



    $(document).on('blur',"#payment-form :input[name='stripe_number']", function(e){
        $(this).val($(this).val().trim())        
        validateNumber($(this).val())
    })    
    $(document).on('keyup',"#payment-form :input[name='stripe_number']", function(e){
    
        var count = $(this).val().length;       
        var key = e.keyCode || e.which;

        aa = $(this).val();
        val = aa.replace(/[^0-9 ]/g, '');
        count2 = val.length;
        /*if(count2 > 16){*/
            val = val.slice(0, 19)
        //}
        count2 = val.length;


        if(aa !== val){
            valTmp = $(this).val().replace(/[^0-9]/g,'');
            c3 = valTmp.length;
            if(c3 > 16){
                valTmp = valTmp.slice(0, 16)
                $(this).val(function (index, value) {                      
                    valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                    valTmp = valTmp.trim();
                    return valTmp;
                });
            }else{
                return $(this).val(val);
            }
        }
        if(count2 < 19){            
            if(count2 % 5 == 0){
                $(this).val(function (index, value) {
                    return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                    val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                    val = val.trim();
                    return val;
                });
            }
        }

        
        /*if(count2 < 19 && key != 8 ){ 
            if(count % 5 == 0){ 
                $(this).val(function (index, value) {
                    return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                });
            }
        }*/
    });

    $(document).on('keyup',"#payment-form :input[name='stripe_cvc']", function(e){

        

        var count = $(this).val().length;
        val = $(this).val().replace(/[^0-9/]/g, '');
        
        if(!($(this).val() == val)){
            $(this).val(val)
            return
        }

        if(count > 3){
             $(this).focus().val($(this).val().slice(0, 3))              
        }

    })
    $(document).on('blur',"#payment-form :input[name='stripe_cvc']", function(e){
        validateCVV($(this).val())
    })

    // var input_field = document.getElementById('credit_card');
    // input_field.addEventListener('textInput', function(e) {
    //     // e.data will be the 1:1 input you done
    //     var char = e.data; // In our example = "a"
    //     alert(char);
       
    //     // If you want the keyCode..
    //     var keyCode = char.charCodeAt(0); // a = 97
    //     alert(keyCode);
     
    //     // Stop processing if "a" is pressed
    //     if (keyCode == 97) {
    //         e.preventDefault();
    //         return false;
    //     }
    //     return true;
    // });

  /*  $("#payment-form :input[name='stripe_number']").on('keypress change', function () {
        var count = $(this).val().length;
        if(count < 19){
            $(this).val(function (index, value) {
                return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            });
        }
    }); */
    /*$("#payment-form :input[name='stripe_expirydate']").focusout(function(){
       if($(this).val().indexOf('/')){
            var $split = $(this).val().split("/");
            var year = new Date().getFullYear().toString().substr(-2);
            if($split[1] < year){
                $("#card-errors").show();
                $("#card-errors").html('Please Enter Valid Year');
            }else{
                $("#card-errors").hide();
            }
        }
    });*/
});


</script>

</body>

</html>

