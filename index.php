<?php 
    include 'inc/config.php'; // Configuration php file
    require(INC_DIR.'init.php');
    if($sessionObj->read('front_user_email')<>"")
    {
    	header("Location:dashboard.php");
    }
    require(CLASS_DIR.'encryption.class.php');
    require_once(CLASS_DIR.'security.class.php');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <?php //require('common/header.php'); ?>
</head>

<body>
    <div class="image-container set-full-height" style="background-image: url('assets/img/backgroundregister.png')">
        <!--   Creative Tim Branding   -->
        <div class="logo-container">
            <div class="logo"></div>
        </div>

        <div id="terms-modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <h2>Terms and conditions</h2>

                        <p><span style="font-weight: 400;">Welcome! You are electing to join RISE Asthma for asthma prevention/control and related services over the Internet.</span></p>
                        <p><span style="font-weight: 400;">Please read the following important terms and conditions (&ldquo;</span><strong>Terms</strong><span style="font-weight: 400;">&rdquo;) carefully. These Terms govern your access to and use of the Services (defined in Section 2 below), and constitute a binding legal agreement among you, as a user of the Services, and RISE Asthma, and any other entities controlling, controlled by or under common control with the foregoing (collectively, &ldquo;RISE Asthma,&rdquo; &ldquo;we&rdquo; or &ldquo;us&rdquo;).</span></p>
                        <p><span style="font-weight: 400;">IMPORTANT: RISE Asthma is not a medical company and is not licensed to sell medical products or services. RISE Asthma is an e-health/self-help platform in that we offer to help those purchasing our product to prevent or control their asthma and/or related conditions. RISE offers a program from the experience of its founder and makes no claim that RISE Asthma will work for individuals purchasing RISE&rsquo;s products and services. Purchasers should review RISE Asthma with their doctors before making a purchase.</span></p>
                        <p><strong>USE OF RISE IS NOT FOR EMERGENCIES. IF YOU THINK YOU HAVE A MEDICAL OR MENTAL HEALTH EMERGENCY, OR IF AT ANY TIME YOU ARE CONCERNED ABOUT YOUR CARE OR TREATMENT, CALL 911 IN THE USA &nbsp;(OR USE THE EMERGENCY NUMBER IN YOUR COUNTRY) OR GO TO THE NEAREST OPEN CLINIC OR EMERGENCY ROOM.</strong></p>
                        <p><strong>IF YOU ARE CONSIDERING OR CONTEMPLATING SUICIDE OR FEEL THAT YOU ARE A DANGER TO YOURSELF OR TO OTHERS, YOU MUST DISCONTINUE USE OF THE SERVICES IMMEDIATELY, CALL 911 (OR USE THE EMERGENCY NUMBER IN YOUR COUNTRY), OR NOTIFY APPROPRIATE POLICE OR EMERGENCY MEDICAL PERSONNEL.</strong></p>
                        <ol>
                            <li><strong> Applicability of These Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your access to and use of the Services is conditioned on your compliance with these Terms. By becoming a registered user and/or accessing and/or using the Services, the Site, or any portion of the Services or the Site, you agree to be bound by these Terms and all applicable laws and regulations governing the Services. If you do not agree with these Terms, you are not authorized to access or use the Services for any purpose. Additional terms and conditions applicable to specific areas of the Site or to particular transactions are also posted in particular areas of the Site and, together with these general Terms, govern your use of those areas. If you do not agree with any of these additional terms and conditions, you are not authorized to access or use those areas of the Site.</span></p>
                        <p><span style="font-weight: 400;">If a third party, such as someone in your physician&rsquo;s or Treatment Provider&rsquo;s office, or a care coordinator or other third party, has been granted access by RISE Asthma to its Site for the purpose as setting up your user account, you will receive notice of that event and be asked to activate the account by email to the email account you provide. By activating the account you are agreeing to be bound by these Terms as stated above, and to grant access to that third party to the contents of your user account, RISE Asthma Profile, Treatment Provider notes, appointment reminders and record of sessions conducted through the Site, until such time as you revoke this access by emailing a secure message via your RISE Asthma account to the RISE Asthma Administrator requesting removal of that third party's access to your account.</span></p>
                        <ol start="2">
                            <li><strong> Our Services.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The RISE Asthma e-health platform includes, without limitation, the following services (collectively, the &ldquo;</span><strong>Services</strong><span style="font-weight: 400;">&rdquo;):</span></p>
                        <p><span style="font-weight: 400;">(a) the facilitation of electronic or telephonic communications with Treatment Providers, and</span></p>
                        <p><span style="font-weight: 400;">(b) the provision of other information about RISE Asthma and our products and services through our website, </span><a href="http://www.breakthrough.com/"><span style="font-weight: 400;">http://www.RISE Asthma.com</span></a><span style="font-weight: 400;"> (the &ldquo;</span><strong>Site</strong><span style="font-weight: 400;">&rdquo;).</span></p>
                        <p><span style="font-weight: 400;">THE SERVICES DO NOT INCLUDE THE PROVISION OF MEDICAL CARE, MENTAL HEALTH SERVICES, OR OTHER PROFESSIONAL SERVICES BY RISE Asthma. Rather, </span></p>
                        <ol start="3">
                            <li><strong> Delivery of Medical Advice.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">While the Services may provide access to certain general medical information, and also may provide messaging functionality to contact your Treatment Provider(s), the Services cannot and are not intended to provide medical advice. We advise you to always seek the advice of a physician or other qualified healthcare provider with any questions regarding your personal health or medical conditions. Never disregard, avoid, or delay in obtaining medical advice from your doctor or other qualified healthcare provider because of something you have read on the Site. If you have or suspect that you have a medical problem or condition, please contact a qualified healthcare professional immediately.</span></p>
                        <p><span style="font-weight: 400;">To the extent medical advice is provided to you by a Treatment Provider through the Services, such medical advice is based on your personal health data as provided by you and the local standards of care for your presenting symptoms. Responses are not provided by RISE Asthma, but are provided by your Treatment Provider.</span></p>
                        <p><span style="font-weight: 400;">THE CONTENT ON THE SITE (OTHER THAN A DIRECT RESPONSE FROM A QUALIFIED TREATMENT PROVIDER) IS NOT AND SHOULD NOT BE CONSIDERED MEDICAL ADVICE OR A SUBSTITUTE FOR INDIVIDUAL MEDICAL ADVICE, DIAGNOSIS, OR TREATMENT. YOU SHOULD ALWAYS TALK TO YOUR MEDICAL PROFESSIONALS FOR DIAGNOSIS AND TREATMENT, INCLUDING INFORMATION REGARDING WHICH DRUGS OR TREATMENT MAY BE APPROPRIATE FOR YOU. NONE OF THE INFORMATION ON THE SITE REPRESENTS OR WARRANTS THAT ANY PARTICULAR DRUG OR TREATMENT IS SAFE, APPROPRIATE, OR EFFECTIVE FOR YOU.</span></p>
                        <ol start="4">
                            <li><strong> Eligibility Requirements to Access Member Portions of the Site.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Access to certain portions of the Site and/or certain Content is restricted to registered users of our Services and their authorized representations.</span></p>
                        <p><strong>Payment policies</strong></p>
                        <p><span style="font-weight: 400;">You agree to promptly pay all fees and charges for RISE Asthma Services, and you authorize us to automatically deduct all applicable charges and fees from the payment account(s) you designate in your RISE Asthma user profile.</span></p>
                        <p><span style="font-weight: 400;">Any "linked" payment processing accounts with third parties (such as PayPal) will appear in your user profile on the Site, and you will be able to view at least certain summary information for all such linked payment accounts. You agree to be responsible for any telephone charges and/or Internet service fees you incur in accessing your account(s) through the Services.</span></p>
                        <p><span style="font-weight: 400;">Finally, in order to access the member-only portions of the Services, you must provide us with a current, valid email address so that we may contact you. By creating an Account, you agree to keep your email address updated.</span></p>
                        <p><span style="font-weight: 400;">There is no guarantee that you will be accepted as a registered user. </span></p>
                        <ol start="5">
                            <li><strong> Registered User Accounts.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">In order to access certain features of the Services you will be required to become a registered user of the Services by creating a RISE Asthma account (&ldquo;</span><strong>Account</strong><span style="font-weight: 400;">&rdquo;). To create an Account, you must be of legal age to form a binding contract. If you are not of legal age to form a binding contract, you may not register to use our Services. When you register, you will be asked to choose a password. You are responsible for safeguarding and maintaining the confidentiality of your password and you agree not to disclose your password to any third party. You will be solely responsible for any activities or actions taken under your Account, whether or not you have authorized such activities or actions. You must notify us immediately if you know or suspect that any unauthorized person is using your password or your Account (for example, your password has been lost or stolen, someone has attempted to use the Services through your account without your consent or your Account has been accessed without your permission). We strongly recommend that you do not use the Services on public computers. We also recommend that you do not store your password through your web browser or other software.</span></p>
                        <p><span style="font-weight: 400;">You agree that the information that you provide to us at all times, including during registration and in any information you upload to your RISE Asthma online profile maintained by or through the Services will be true, accurate, current, and complete. This information includes, but is not limited to, name, address, phone numbers, email addresses, payment information, and account numbers. Changes can be made in your user profile. Each time you log in to our Services, we will remind you to update your information, but you are solely responsible for the accuracy and completeness of your information. By using the Services, you are consenting to truthfully complete questions to the best of your knowledge and ability. By creating an Account, you expressly consent to the use of: (a) electronic means to complete these Terms and to provide you with any notices given pursuant to these Terms; and (b) electronic records to store information related to these Terms or your use of the Services. RISE Asthma cannot and will not be liable for any loss or damage arising from your failure to comply with the above requirements.</span></p>
                        <ol start="6">
                            <li><strong> Your RISE Asthma Profile.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your RISE Asthma profile will be established and maintained for you as a registered user of the Services to enter, store, and access your health information online, and for your Treatment Providers to communicate with you about your care. This may include history, current conditions, symptoms, complaints, allergies and medications. All of the information contained in your RISE Asthma profile will be maintained in accordance with our Terms and our Privacy Policy. You agree to provide accurate and complete information for your RISE Asthma profile, to periodically review such information, and to update information that you provide as needed. Please refer to our Privacy Policy for more information.</span></p>
                        <ol start="7">
                            <li><strong> Use of the Services by Children.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The provision of RISE Asthma through the Services is available for use by children age 3 and above, but the registered user for all patients under the age of 18 must be the patient&rsquo;s parent or legal guardian. If you register as the parent or legal guardian on behalf of a minor, you will be fully responsible for complying with our Terms and our Privacy Policy.</span></p>
                        <ol>
                            <li><strong> Privacy.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">When you use the Services, RISE Asthma will collect certain personally identifiable information from you as set forth in more detail in our Privacy Policy, which is hereby incorporated by reference. When you use the Services, RISE Asthma has access to, and in many cases will monitor, your usage of the Services as you send and receive Content (as defined in Section 14 below). By using the Services, you agree that RISE Asthma may collect, use, and disclose information you provide during your use of the Services as set forth in our Privacy Policy. As part of providing you the Services, we may need to provide you with certain communications, such as appointment reminders, service announcements and administrative messages. These communications are considered part of the Services and your Account, which you may not be able to opt out from receiving.</span></p>
                        <p><span style="font-weight: 400;">Secure electronic messaging is always preferred to insecure email, but under specific circumstances, insecure email communication containing protected health information (&ldquo;PHI&rdquo;) may take place between you and RISE Asthma.</span></p>
                        <ol start="8">
                            <li><strong> User Supplied Material.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">If you supply any comments, information, or material via the Site, you represent and warrant to us that you have the legal right to supply such material and that it will not violate any law or the rights of any person or entity. Except for any individually identifiable health information you submit to us, all information or material you supply to us through the Site shall be deemed and shall remain our property, and you hereby assign to RISE Asthma all right, title, and interest in and to any such information or material, without any restriction or obligation to you.</span></p>
                        <ol start="9">
                            <li><strong> Restrictions on Conduct.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The Services may be used and accessed for lawful purposes only. You agree to abide by all applicable local, state, national and foreign laws, treaties and regulations in connection with your use of the Services.</span></p>
                        <p><span style="font-weight: 400;">In addition, without limitation, you agree that you will not do any of the following while using or accessing the Services:</span></p>
                        <p><span style="font-weight: 400;">(a) upload, post, email or otherwise transmit any Content to which you do not have the lawful right to copy, transmit and display (including any Content that would violate any confidentiality or fiduciary obligations that you might have with respect to the Content);</span></p>
                        <p><span style="font-weight: 400;">(b) upload, post, email or otherwise transmit any Content that infringes the intellectual property rights or violates the privacy rights of any third party (including without limitation copyright, trademark, patent, trade secret, or other intellectual property right, or moral right or right of publicity);</span></p>
                        <p><span style="font-weight: 400;">(c) use the Services to collect or store personal data about other users without their express permission;</span></p>
                        <p><span style="font-weight: 400;">(d) knowingly include or use any false or inaccurate information in any profile;</span></p>
                        <p><span style="font-weight: 400;">(e) upload, post, email or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain letters, &ldquo;pyramid schemes&rdquo; or any other form of solicitation, as well as viruses or other computer code that may interrupt, destroy, limit the functionality of the Services, or interfere with the access of any other user to the Services;</span></p>
                        <p><span style="font-weight: 400;">(f) circumvent, disable, or otherwise interfere with security-related features of the Services or features that prevent or restrict use or copying of any Content;</span></p>
                        <p><span style="font-weight: 400;">(g) use any meta tags or other hidden text or metadata utilizing a RISE Asthma name, trademark, URL or product name;</span></p>
                        <p><span style="font-weight: 400;">(h) attempt to probe, scan or test the vulnerability of any RISE Asthma system or network or breach or impair or circumvent any security or authentication measures protecting the Services;</span></p>
                        <p><span style="font-weight: 400;">(i) attempt to decipher, decompile, disassemble, reverse engineer, or otherwise attempt to discover or determine the source code of any software or any proprietary algorithm used to provide the Services;</span></p>
                        <p><span style="font-weight: 400;">(j) use the Services in any way that competes with RISE Asthma, including, without limitation, misrepresenting one&rsquo;s identity or posing as a current or prospective patient in order to solicit or recruit Treatment Providers, directly or indirectly; or</span></p>
                        <p><span style="font-weight: 400;">(k) encourage or instruct any other person or entity to do any of the foregoing.</span></p>
                        <ol>
                            <li><strong> Termination; Cancellation.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">RISE Asthma is continually evolving and innovating. We may change our Services, our Site, the Content we offer, and the products or services you may access at any time. We may discontinue offering our Services or Site and we may suspend or terminate your right to use our Services or Site at any time, in the event that you breach these Terms, for any reason, or for no reason at all, in our sole discretion, and without prior notice to you. After such termination, RISE Asthma will have no further obligation to provide the Services, except to the extent we are obligated to provide you access to your health records or Treatment Providers are required to provide you with continuing care under their applicable legal, ethical and professional obligations to you.</span></p>
                        <p><span style="font-weight: 400;">Upon termination of your right to use our Services or Site or our termination of the Services or Site, all licenses and other rights granted to you by these Terms will immediately terminate.</span></p>
                        <p><span style="font-weight: 400;">You may terminate your Account at any time and for any reason by sending RISE Asthma notice or deactivating your account through your Profile. Upon any termination by you, your Account will no longer be accessible. Any cancellation request will be handled within 30 days of receipt of such a request by RISE Asthma.</span></p>
                        <p><span style="font-weight: 400;">Any suspension, termination, or cancellation will not affect your obligations to RISE Asthma under these Terms which by their nature are intended to survive such suspension, termination, or cancellation. For example, but not by way of limitation, upon any such suspension, termination, or cancellation the provisions of Section 14 (Ownership of Intellectual Property Rights), Section 16 (Third Party Websites; Interest-Based and Other Advertisements), Section 17 (Disclaimer of Warranties), Section 18 (Indemnification), Section 19 (Limitation of Liability), Section 21 (General Terms), Section 22 (Arbitration) and Section 23 (Governing Law and Forum for Disputes) shall survive and remain in full force and effect, but the provisions of Section 15 (Your License to Use the Services) shall be suspended, terminated or cancelled, as the case may be.</span></p>
                        <ol>
                            <li><strong> Ownership of Intellectual Property Rights.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The Services, the Site, and all information and/or content that you see, hear, or otherwise experience on the Site (collectively, &ldquo;</span><strong>Content</strong><span style="font-weight: 400;">&rdquo;) are protected by U.S. and international copyright, trademark, and other laws. We own or have the license to use all of the intellectual property rights relating to RISE Asthma, the Services, the Site, and the Content,</span> <span style="font-weight: 400;">including, without limitation, all intellectual property rights protected as patent pending or patented inventions, trade secrets, copyrights, trademarks, service marks, trade dress, or proprietary or confidential information, and whether or not they happened to be registered. You will not acquire any intellectual property rights in RISE Asthma by your use of the Services or the Site.</span></p>
                        <ol start="2">
                            <li><strong> Your License to Use the Services.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">When you use our Services or Site you may access intellectual property rights that we or our licensors own or license. Subject to your compliance with the terms and conditions of these Terms, RISE Asthma grants you a limited, non-exclusive, non-transferable and revocable license, without the right to sublicense, to access and use the Services and to download and print any Content provided by RISE Asthma solely for your personal and non-commercial purposes. You may not use, copy, adapt, modify, prepare derivative works based upon, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services or Content, except as expressly permitted in these Terms, without RISE Asthma&rsquo;s express prior written consent. No licenses or rights are granted to you by implication or otherwise under any intellectual property rights owned or controlled by RISE Asthma or its licensors, except for the licenses and rights expressly granted in these Terms. Unless otherwise expressly agreed in writing by RISE Asthma, the Services are only permitted to be used within the United States of America.</span></p>
                        <ol start="3">
                            <li><strong> Third-Party Websites; Advertisements.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We may make available, on our Site and as part of our Services, links to third-party websites or resources from third parties on the Site.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma is not responsible or liable for the availability or accuracy of, and RISE Asthma does not endorse, sponsor, or recommend such websites or resources, or the content, products, or services on or available from such websites or resources. When we make available such third-party links or resources on the Site or through the Services, you must look solely to the third party with respect to the content, products, or services they provide. We do not endorse and are not responsible for any of the content, products, or services provided by others. YOUR USE OF THE WEBSITES OR RESOURCES OF THIRD PARTIES IS AT YOUR OWN RISK. RISE Asthma AND ITS AFFILIATES WILL NOT BE LIABLE FOR ANY OF YOUR LOSSES ARISING OUT OF OR RELATING TO THE WEBSITES OR RESOURCES OF THIRD PARTIES.</span></p>
                        <ol start="4">
                            <li><strong> Disclaimer of Warranties.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your use of the Services and Content is at your sole discretion and risk. The Services and Content, and all materials, information, products and services included therein, are provided on an &ldquo;AS IS&rdquo; and &ldquo;AS AVAILABLE&rdquo; basis without warranties of any kind.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma AND ITS LICENSORS AND AFFILIATES EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, OR STATUTORY, RELATING TO THE SERVICES AND CONTENT, INCLUDING WITHOUT LIMITATION THE WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF PROPRIETARY RIGHTS, COURSE OF DEALING, OR COURSE OF PERFORMANCE. RISE Asthma AND ITS LICENSORS AND AFFILIATES MAKE NO WARRANTY THAT THE CONTENT YOU ACCESS ON OUR WEBSITE OR USING OUR SERVICE SATISFIES THE LAWS AND REGULATIONS REQUIRING THE DISCLOSURE OF INFORMATION FOR PRESCRIPTION DRUGS.</span></p>
                        <p><span style="font-weight: 400;">IN ADDITION, RISE Asthma AND ITS LICENSORS AND AFFILIATES DISCLAIM ANY WARRANTIES REGARDING SECURITY, ACCURACY, RELIABILITY TIMELINESS AND PERFORMANCE OF THE SERVICES OR THAT THE SERVICES WILL BE ERROR FREE OR THAT ANY ERRORS WILL BE CORRECTED. NO ADVICE OR INFORMATION PROVIDED TO YOU BY RISE Asthma WILL CREATE ANY WARRANTY THAT IS NOT EXPRESSLY STATED IN THESE TERMS OF SERVICE.</span></p>
                        <p><span style="font-weight: 400;">WE MAKE NO REPRESENTATIONS CONCERNING, AND DO NOT GUARANTEE, THE ACCURACY OF THE SERVICES, INCLUDING, BUT NOT LIMITED TO, ANY INFORMATION PROVIDED THROUGH THE SERVICES OR THEIR APPLICABILITY TO YOUR INDIVIDUAL CIRCUMSTANCES. OUR SERVICES AND SITE CONTENT ARE DEVELOPED FOR USE IN THE UNITED STATES AND RISE Asthma AND ITS LICENSORS AND AFFILIATES MAKE NO REPRESENTATION OR WARRANTY CONCERNING THE SERVICES OR SITE CONTENT WHEN THEY ARE USED IN ANY OTHER COUNTRY.</span></p>
                        <p><span style="font-weight: 400;">SOME JURISDICTIONS DO NOT PERMIT US TO EXCLUDE WARRANTIES IN THESE WAYS, SO IT IS POSSIBLE THAT THESE EXCLUSIONS WILL NOT APPLY TO OUR AGREEMENT WITH YOU. IN SUCH EVENT THE EXCLUSIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAW.</span></p>
                        <ol start="5">
                            <li><strong> Indemnification.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">You will indemnify, defend, and hold harmless RISE Asthma, RISE Asthma&rsquo;s licensors and affiliates and our respective directors, officers, employees, contractors, agents and representatives, from and against any and all claims, causes of action, demands, liabilities, losses, costs or expenses (including, but not limited to, reasonable attorneys&rsquo; fees and expenses) arising out of or relating to any of the following matters:</span></p>
                        <p><span style="font-weight: 400;">(a) your access to or use of the Services, the Site, or the Content;</span></p>
                        <p><span style="font-weight: 400;">(b) your violation of any of the provisions of these Terms of Service;</span></p>
                        <p><span style="font-weight: 400;">(c) any activity related to your Account by you or any other person accessing the Site or Services through your account, including, without limitation, negligent or wrongful conduct; or</span></p>
                        <p><span style="font-weight: 400;">(d) your violation of any third party right, including ,without limitation, any intellectual property right, publicity, confidentiality, property or privacy right.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma reserves the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will cooperate with us in asserting any available defenses.</span></p>
                        <ol start="6">
                            <li><strong> Limitation of Liability.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">IN NO EVENT WILL RISE Asthma OR RISE Asthma&rsquo;S LICENSORS OR AFFILIATES BE LIABLE TO YOU FOR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES, OR LOST PROFITS, ARISING OUT OF OR IN CONNECTION WITH YOUR USE OF THE SERVICES, THE SITE, OR THE CONTENT, WHETHER THE DAMAGES ARE FORESEEABLE AND WHETHER OR NOT RISE Asthma HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES IN ADVANCE. IF YOU ARE DISSATISFIED WITH THE SERVICES, THE SITE OR THE CONTENT, OR THE TERMS, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</span></p>
                        <p><span style="font-weight: 400;">NOTHING HEREIN SHALL LIMIT THE POTENTIAL PROFESSIONAL LIABILITY OF TREATMENT PROVIDERS OR OTHER LICENSED HEALTHCARE PROFESSIONALS ARISING FROM OR RELATED TO MEDICAL OR MENTAL HEALTH ADVICE, DIAGNOSIS, OR TREATMENT THEY PROVIDE TO YOU, EXCEPT AS PROVIDED UNDER APPLICABLE STATE OR LOCAL LAWS. IN MANY JURISDICTIONS, MEDICAL PROVIDERS ARE REQUIRED TO REPORT CONFIDENTIAL INFORMATION IF THEY HAVE REASON TO BELIEVE THAT A PATIENT IS LIKELY TO HARM OTHERS OR HIMSELF/HERSELF. IN NO EVENT SHALL RISE Asthma BE LIABLE FOR THE DISCLOSURE OF YOUR CONFIDENTIAL INFORMATION BY A TREATMENT PROVIDER FROM WHOM YOU RECEIVE MENTAL HEALTH SERVICES. RISE Asthma IS NOT LIABLE TO ANY PERSON OR USER FOR ANY HARM CAUSED BY THE NEGLIGENCE OR MISCONDUCT OF A MEDICAL PROVIDER PROVIDING SERVICES. IN NO EVENT WILL THE CUMULATIVE LIABILITY OF RISE Asthma OR RISE Asthma&rsquo;S LICENSORS OR AFFILIATES TO YOU, WHETHER IN CONTRACT, TORT, OR OTHERWISE, EXCEED $1,000.</span></p>
                        <p><span style="font-weight: 400;">EXCEPT AS OTHERWISE REQUIRED BY APPLICABLE LAW, ANY CLAIM OR CAUSE OF ACTION ARISING OUT OF OR RELATING TO YOUR USE OF THE SERVICES, THE SITE OR THE CONTENT OR OUR RELATIONSHIP WITH YOU, REGARDLESS OF THEORY, MUST BE BROUGHT WITHIN ONE (1) YEAR AFTER THE OCCURRENCE OF THE EVENT GIVING RISE TO THE CLAIM OR CAUSE OF ACTION OR BE FOREVER BARRED.</span></p>
                        <p><span style="font-weight: 400;">SOME JURISDICTIONS DO NOT PERMIT US TO LIMIT OUR LIABILITY IN THESE WAYS, SO IT IS POSSIBLE THAT THESE LIMITATIONS WILL NOT APPLY TO OUR AGREEMENT WITH YOU. IN SUCH EVENT THE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAW.</span></p>
                        <ol start="7">
                            <li><strong> Errors and Inaccuracies.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The information on the Site including, without limitation, information regarding pricing, may contain typographical errors or other errors or inaccuracies, and may not be complete or current. RISE Asthma reserves the right to correct any errors, inaccuracies, or omissions and to change or update information at any time without prior notice to you. RISE Asthma will not, however, guarantee that any such errors, inaccuracies, or omissions will be corrected. RISE Asthma reserves the right to refuse to fill any orders or provide Services that are based on inaccurate or erroneous information on the Site, including, without limitation, incorrect or out-of-date information regarding pricing, payment terms, or for any other lawful reason.</span></p>
                        <ol start="8">
                            <li><strong> General Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">These Terms of Service constitute the entire agreement between you and us relating to our Services, the Site, and the Content, replacing any prior or contemporaneous agreements, whether written or oral, unless you have signed a separate written agreement with us relating to our Services, the Site, or the Content. If there is any conflict between the Terms and a separate signed written agreement between you and us relating to our Services, the Site, or the Content, the signed written agreement will control. Only the executive officers of RISE Asthma have the authority to sign a separate signed written agreement between you and us.</span></p>
                        <p><span style="font-weight: 400;">Our licensors may be entitled to enforce this agreement as third-party beneficiaries. There are no other third-party beneficiaries to this agreement.</span></p>
                        <p><span style="font-weight: 400;">The failure by you or us to enforce any provision of the Terms will not constitute a waiver. If any court of law, having the jurisdiction to decide the matter, rules that any provision of the Terms is invalid or unenforceable, then the invalid or unenforceable provision shall be removed from the Terms or reformed by the court and given effect so as to best accomplish the essential purpose of the invalid or unenforceable provision, and all of the other provisions of the Terms shall continue to be valid and enforceable. Nothing contained in these Terms of Service shall limit the ability of a party to seek an injunction or other equitable relief without posting any bond. The titles of the Sections of the Terms are for convenience only and shall have no legal or contractual effect.</span></p>
                        <ol start="9">
                            <li><strong> Arbitration.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">For any claim (excluding claims for injunctive or other equitable relief) where the total amount of the award sought is less than $10,000.00 USD, the party requesting relief may elect to resolve the dispute in a cost effective manner through binding non-appearance-based arbitration. If a party elects arbitration, that party will initiate such arbitration through JAMS. The parties must comply with the following rules:</span></p>
                        <p><span style="font-weight: 400;">(a) the arbitrator shall be selected from JAMS and the arbitration shall be conducted in accordance with JAMS&rsquo; Comprehensive Arbitration Rules and Procedures, except as otherwise specified below;</span></p>
                        <p><span style="font-weight: 400;">(b) the arbitration shall be conducted by telephone, online and/or be solely based on written submissions, the specific manner shall be chosen by the party initiating the arbitration;</span></p>
                        <p><span style="font-weight: 400;">(b) the arbitration shall not involve any personal appearance by the parties or witnesses unless otherwise mutually agreed by the parties; and</span></p>
                        <p><span style="font-weight: 400;">(c) any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction.</span></p>
                        <p><span style="font-weight: 400;">Any dispute, controversy, or disagreement arising out of or relating to these Terms, the breach thereof, or the subject matter thereof, where the total amount of the award sought is $10,000.00 USD or greater, shall be settled exclusively by binding arbitration. The arbitrator shall be selected from JAMS and the arbitration shall be conducted in accordance with JAMS&rsquo; Comprehensive Arbitration Rules and Procedures The arbitration shall be held in the County of Santa Clara, California, unless the parties mutually agree to have such proceeding in some other locale. To the extent of the subject matter of the arbitration, the arbitration shall be binding not only on all parties to these Terms, but on any other entity controlled by, in control of or under common control with the party to the extent that such affiliate joins in the arbitration, and judgment on the award rendered by the arbitrator may be entered in any court having jurisdiction thereof.</span></p>
                        <ol>
                            <li><strong> Governing Law and Forum for Disputes.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">THESE TERMS OF SERVICE AND OUR RELATIONSHIP WITH YOU SHALL BE GOVERNED BY THE LAWS OF THE STATE OF DELAWARE, EXCLUDING ITS CHOICE OF LAWS RULES. YOU AND RISE Asthma EACH IRREVOCABLY AGREES THAT THE EXCLUSIVE VENUE FOR ANY ACTION OR PROCEEDING ARISING OUT OF OR RELATING TO THESE TERMS OF SERVICE OR OUR RELATIONSHIP WITH YOU, REGARDLESS OF THEORY, SHALL BE THE U.S. DISTRICT COURT FOR THE NORTHERN DISTRICT OF CALIFORNIA, OR THE STATE COURTS LOCATED IN SANTA CLARA COUNTY, CALIFORNIA. YOU AND RISE Asthma EACH IRREVOCABLY CONSENTS TO THE PERSONAL JURISDICTION OF THESE COURTS AND WAIVES ANY AND ALL OBJECTIONS TO THE EXERCISE OF JURISDICTION BY THESE COURTS AND TO THIS VENUE. NOTWITHSTANDING THE FOREGOING, HOWEVER, YOU AND RISE Asthma AGREE THAT RISE Asthma MAY COMMENCE AND MAINTAIN AN ACTION OR PROCEEDING SEEKING INJUNCTIVE OR OTHER EQUITABLE RELIEF IN ANY COURT OF COMPETENT JURISDICTION.</span></p>
                        <ol>
                            <li><strong> Changes to These Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We reserve the right to change our Terms at any time. Any changes that we make will become a part of our agreement with you when they are posted to our Site. Your continued use of our Services or the Site will constitute your agreement to the changes we have made. The last date these Terms were revised is set forth at the end of this document.</span></p>
                        <ol start="2">
                            <li><strong> Contacting Us.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We encourage you to contact us at </span><a href="mailto:hope@RISEAsthma.com"><span style="font-weight: 400;">hope@RISEAsthma.com</span></a><span style="font-weight: 400;">. </span><span style="font-weight: 400;">if you have any questions concerning our Terms. Please note that email communications will not necessarily be secure; accordingly, you should not include credit card information or other sensitive information in your email correspondence with us. If you would like to contact us via physical mail, our mailing address is: RISE Asthma, __________________________</span></p>
                        <p><span style="font-weight: 400;">Last Revised: June 12, 2017</span></p>
                        <p><span style="font-weight: 400;">Site copyright 2017 RISE Asthma unless otherwise noted. All rights reserved</span></p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p><strong>RISE ASTHMA TERMS OF SERVICE</strong></p>
                        <p><span style="font-weight: 400;">Welcome! You are electing to join RISE Asthma for asthma prevention/control and related services over the Internet.</span></p>
                        <p><span style="font-weight: 400;">Please read the following important terms and conditions (&ldquo;</span><strong>Terms</strong><span style="font-weight: 400;">&rdquo;) carefully. These Terms govern your access to and use of the Services (defined in Section 2 below), and constitute a binding legal agreement among you, as a user of the Services, and RISE Asthma, and any other entities controlling, controlled by or under common control with the foregoing (collectively, &ldquo;RISE Asthma,&rdquo; &ldquo;we&rdquo; or &ldquo;us&rdquo;).</span></p>
                        <p><span style="font-weight: 400;">IMPORTANT: RISE Asthma is not a medical company and is not licensed to sell medical products or services. RISE Asthma is an e-health/self-help platform in that we offer to help those purchasing our product to prevent or control their asthma and/or related conditions. RISE offers a program from the experience of its founder and makes no claim that RISE Asthma will work for individuals purchasing RISE&rsquo;s products and services. Purchasers should review RISE Asthma with their doctors before making a purchase.</span></p>
                        <p><strong>USE OF RISE IS NOT FOR EMERGENCIES. IF YOU THINK YOU HAVE A MEDICAL OR MENTAL HEALTH EMERGENCY, OR IF AT ANY TIME YOU ARE CONCERNED ABOUT YOUR CARE OR TREATMENT, CALL 911 IN THE USA &nbsp;(OR USE THE EMERGENCY NUMBER IN YOUR COUNTRY) OR GO TO THE NEAREST OPEN CLINIC OR EMERGENCY ROOM.</strong></p>
                        <p><strong>IF YOU ARE CONSIDERING OR CONTEMPLATING SUICIDE OR FEEL THAT YOU ARE A DANGER TO YOURSELF OR TO OTHERS, YOU MUST DISCONTINUE USE OF THE SERVICES IMMEDIATELY, CALL 911 (OR USE THE EMERGENCY NUMBER IN YOUR COUNTRY), OR NOTIFY APPROPRIATE POLICE OR EMERGENCY MEDICAL PERSONNEL.</strong></p>
                        <ol>
                            <li><strong> Applicability of These Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your access to and use of the Services is conditioned on your compliance with these Terms. By becoming a registered user and/or accessing and/or using the Services, the Site, or any portion of the Services or the Site, you agree to be bound by these Terms and all applicable laws and regulations governing the Services. If you do not agree with these Terms, you are not authorized to access or use the Services for any purpose. Additional terms and conditions applicable to specific areas of the Site or to particular transactions are also posted in particular areas of the Site and, together with these general Terms, govern your use of those areas. If you do not agree with any of these additional terms and conditions, you are not authorized to access or use those areas of the Site.</span></p>
                        <p><span style="font-weight: 400;">If a third party, such as someone in your physician&rsquo;s or Treatment Provider&rsquo;s office, or a care coordinator or other third party, has been granted access by RISE Asthma to its Site for the purpose as setting up your user account, you will receive notice of that event and be asked to activate the account by email to the email account you provide. By activating the account you are agreeing to be bound by these Terms as stated above, and to grant access to that third party to the contents of your user account, RISE Asthma Profile, Treatment Provider notes, appointment reminders and record of sessions conducted through the Site, until such time as you revoke this access by emailing a secure message via your RISE Asthma account to the RISE Asthma Administrator requesting removal of that third party's access to your account.</span></p>
                        <ol start="2">
                            <li><strong> Our Services.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The RISE Asthma e-health platform includes, without limitation, the following services (collectively, the &ldquo;</span><strong>Services</strong><span style="font-weight: 400;">&rdquo;):</span></p>
                        <p><span style="font-weight: 400;">(a) the facilitation of electronic or telephonic communications with Treatment Providers, and</span></p>
                        <p><span style="font-weight: 400;">(b) the provision of other information about RISE Asthma and our products and services through our website, </span><a href="http://www.breakthrough.com/"><span style="font-weight: 400;">http://www.RISE Asthma.com</span></a><span style="font-weight: 400;"> (the &ldquo;</span><strong>Site</strong><span style="font-weight: 400;">&rdquo;).</span></p>
                        <p><span style="font-weight: 400;">THE SERVICES DO NOT INCLUDE THE PROVISION OF MEDICAL CARE, MENTAL HEALTH SERVICES, OR OTHER PROFESSIONAL SERVICES BY RISE Asthma. Rather, </span></p>
                        <ol start="3">
                            <li><strong> Delivery of Medical Advice.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">While the Services may provide access to certain general medical information, and also may provide messaging functionality to contact your Treatment Provider(s), the Services cannot and are not intended to provide medical advice. We advise you to always seek the advice of a physician or other qualified healthcare provider with any questions regarding your personal health or medical conditions. Never disregard, avoid, or delay in obtaining medical advice from your doctor or other qualified healthcare provider because of something you have read on the Site. If you have or suspect that you have a medical problem or condition, please contact a qualified healthcare professional immediately.</span></p>
                        <p><span style="font-weight: 400;">To the extent medical advice is provided to you by a Treatment Provider through the Services, such medical advice is based on your personal health data as provided by you and the local standards of care for your presenting symptoms. Responses are not provided by RISE Asthma, but are provided by your Treatment Provider.</span></p>
                        <p><span style="font-weight: 400;">THE CONTENT ON THE SITE (OTHER THAN A DIRECT RESPONSE FROM A QUALIFIED TREATMENT PROVIDER) IS NOT AND SHOULD NOT BE CONSIDERED MEDICAL ADVICE OR A SUBSTITUTE FOR INDIVIDUAL MEDICAL ADVICE, DIAGNOSIS, OR TREATMENT. YOU SHOULD ALWAYS TALK TO YOUR MEDICAL PROFESSIONALS FOR DIAGNOSIS AND TREATMENT, INCLUDING INFORMATION REGARDING WHICH DRUGS OR TREATMENT MAY BE APPROPRIATE FOR YOU. NONE OF THE INFORMATION ON THE SITE REPRESENTS OR WARRANTS THAT ANY PARTICULAR DRUG OR TREATMENT IS SAFE, APPROPRIATE, OR EFFECTIVE FOR YOU.</span></p>
                        <ol start="4">
                            <li><strong> Eligibility Requirements to Access Member Portions of the Site.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Access to certain portions of the Site and/or certain Content is restricted to registered users of our Services and their authorized representations.</span></p>
                        <p><strong>Payment policies</strong></p>
                        <p><span style="font-weight: 400;">You agree to promptly pay all fees and charges for RISE Asthma Services, and you authorize us to automatically deduct all applicable charges and fees from the payment account(s) you designate in your RISE Asthma user profile.</span></p>
                        <p><span style="font-weight: 400;">Any "linked" payment processing accounts with third parties (such as PayPal) will appear in your user profile on the Site, and you will be able to view at least certain summary information for all such linked payment accounts. You agree to be responsible for any telephone charges and/or Internet service fees you incur in accessing your account(s) through the Services.</span></p>
                        <p><span style="font-weight: 400;">Finally, in order to access the member-only portions of the Services, you must provide us with a current, valid email address so that we may contact you. By creating an Account, you agree to keep your email address updated.</span></p>
                        <p><span style="font-weight: 400;">There is no guarantee that you will be accepted as a registered user. </span></p>
                        <ol start="5">
                            <li><strong> Registered User Accounts.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">In order to access certain features of the Services you will be required to become a registered user of the Services by creating a RISE Asthma account (&ldquo;</span><strong>Account</strong><span style="font-weight: 400;">&rdquo;). To create an Account, you must be of legal age to form a binding contract. If you are not of legal age to form a binding contract, you may not register to use our Services. When you register, you will be asked to choose a password. You are responsible for safeguarding and maintaining the confidentiality of your password and you agree not to disclose your password to any third party. You will be solely responsible for any activities or actions taken under your Account, whether or not you have authorized such activities or actions. You must notify us immediately if you know or suspect that any unauthorized person is using your password or your Account (for example, your password has been lost or stolen, someone has attempted to use the Services through your account without your consent or your Account has been accessed without your permission). We strongly recommend that you do not use the Services on public computers. We also recommend that you do not store your password through your web browser or other software.</span></p>
                        <p><span style="font-weight: 400;">You agree that the information that you provide to us at all times, including during registration and in any information you upload to your RISE Asthma online profile maintained by or through the Services will be true, accurate, current, and complete. This information includes, but is not limited to, name, address, phone numbers, email addresses, payment information, and account numbers. Changes can be made in your user profile. Each time you log in to our Services, we will remind you to update your information, but you are solely responsible for the accuracy and completeness of your information. By using the Services, you are consenting to truthfully complete questions to the best of your knowledge and ability. By creating an Account, you expressly consent to the use of: (a) electronic means to complete these Terms and to provide you with any notices given pursuant to these Terms; and (b) electronic records to store information related to these Terms or your use of the Services. RISE Asthma cannot and will not be liable for any loss or damage arising from your failure to comply with the above requirements.</span></p>
                        <ol start="6">
                            <li><strong> Your RISE Asthma Profile.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your RISE Asthma profile will be established and maintained for you as a registered user of the Services to enter, store, and access your health information online, and for your Treatment Providers to communicate with you about your care. This may include history, current conditions, symptoms, complaints, allergies and medications. All of the information contained in your RISE Asthma profile will be maintained in accordance with our Terms and our Privacy Policy. You agree to provide accurate and complete information for your RISE Asthma profile, to periodically review such information, and to update information that you provide as needed. Please refer to our Privacy Policy for more information.</span></p>
                        <ol start="7">
                            <li><strong> Use of the Services by Children.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The provision of RISE Asthma through the Services is available for use by children age 3 and above, but the registered user for all patients under the age of 18 must be the patient&rsquo;s parent or legal guardian. If you register as the parent or legal guardian on behalf of a minor, you will be fully responsible for complying with our Terms and our Privacy Policy.</span></p>
                        <ol>
                            <li><strong> Privacy.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">When you use the Services, RISE Asthma will collect certain personally identifiable information from you as set forth in more detail in our Privacy Policy, which is hereby incorporated by reference. When you use the Services, RISE Asthma has access to, and in many cases will monitor, your usage of the Services as you send and receive Content (as defined in Section 14 below). By using the Services, you agree that RISE Asthma may collect, use, and disclose information you provide during your use of the Services as set forth in our Privacy Policy. As part of providing you the Services, we may need to provide you with certain communications, such as appointment reminders, service announcements and administrative messages. These communications are considered part of the Services and your Account, which you may not be able to opt out from receiving.</span></p>
                        <p><span style="font-weight: 400;">Secure electronic messaging is always preferred to insecure email, but under specific circumstances, insecure email communication containing protected health information (&ldquo;PHI&rdquo;) may take place between you and RISE Asthma.</span></p>
                        <ol start="8">
                            <li><strong> User Supplied Material.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">If you supply any comments, information, or material via the Site, you represent and warrant to us that you have the legal right to supply such material and that it will not violate any law or the rights of any person or entity. Except for any individually identifiable health information you submit to us, all information or material you supply to us through the Site shall be deemed and shall remain our property, and you hereby assign to RISE Asthma all right, title, and interest in and to any such information or material, without any restriction or obligation to you.</span></p>
                        <ol start="9">
                            <li><strong> Restrictions on Conduct.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The Services may be used and accessed for lawful purposes only. You agree to abide by all applicable local, state, national and foreign laws, treaties and regulations in connection with your use of the Services.</span></p>
                        <p><span style="font-weight: 400;">In addition, without limitation, you agree that you will not do any of the following while using or accessing the Services:</span></p>
                        <p><span style="font-weight: 400;">(a) upload, post, email or otherwise transmit any Content to which you do not have the lawful right to copy, transmit and display (including any Content that would violate any confidentiality or fiduciary obligations that you might have with respect to the Content);</span></p>
                        <p><span style="font-weight: 400;">(b) upload, post, email or otherwise transmit any Content that infringes the intellectual property rights or violates the privacy rights of any third party (including without limitation copyright, trademark, patent, trade secret, or other intellectual property right, or moral right or right of publicity);</span></p>
                        <p><span style="font-weight: 400;">(c) use the Services to collect or store personal data about other users without their express permission;</span></p>
                        <p><span style="font-weight: 400;">(d) knowingly include or use any false or inaccurate information in any profile;</span></p>
                        <p><span style="font-weight: 400;">(e) upload, post, email or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain letters, &ldquo;pyramid schemes&rdquo; or any other form of solicitation, as well as viruses or other computer code that may interrupt, destroy, limit the functionality of the Services, or interfere with the access of any other user to the Services;</span></p>
                        <p><span style="font-weight: 400;">(f) circumvent, disable, or otherwise interfere with security-related features of the Services or features that prevent or restrict use or copying of any Content;</span></p>
                        <p><span style="font-weight: 400;">(g) use any meta tags or other hidden text or metadata utilizing a RISE Asthma name, trademark, URL or product name;</span></p>
                        <p><span style="font-weight: 400;">(h) attempt to probe, scan or test the vulnerability of any RISE Asthma system or network or breach or impair or circumvent any security or authentication measures protecting the Services;</span></p>
                        <p><span style="font-weight: 400;">(i) attempt to decipher, decompile, disassemble, reverse engineer, or otherwise attempt to discover or determine the source code of any software or any proprietary algorithm used to provide the Services;</span></p>
                        <p><span style="font-weight: 400;">(j) use the Services in any way that competes with RISE Asthma, including, without limitation, misrepresenting one&rsquo;s identity or posing as a current or prospective patient in order to solicit or recruit Treatment Providers, directly or indirectly; or</span></p>
                        <p><span style="font-weight: 400;">(k) encourage or instruct any other person or entity to do any of the foregoing.</span></p>
                        <ol>
                            <li><strong> Termination; Cancellation.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">RISE Asthma is continually evolving and innovating. We may change our Services, our Site, the Content we offer, and the products or services you may access at any time. We may discontinue offering our Services or Site and we may suspend or terminate your right to use our Services or Site at any time, in the event that you breach these Terms, for any reason, or for no reason at all, in our sole discretion, and without prior notice to you. After such termination, RISE Asthma will have no further obligation to provide the Services, except to the extent we are obligated to provide you access to your health records or Treatment Providers are required to provide you with continuing care under their applicable legal, ethical and professional obligations to you.</span></p>
                        <p><span style="font-weight: 400;">Upon termination of your right to use our Services or Site or our termination of the Services or Site, all licenses and other rights granted to you by these Terms will immediately terminate.</span></p>
                        <p><span style="font-weight: 400;">You may terminate your Account at any time and for any reason by sending RISE Asthma notice or deactivating your account through your Profile. Upon any termination by you, your Account will no longer be accessible. Any cancellation request will be handled within 30 days of receipt of such a request by RISE Asthma.</span></p>
                        <p><span style="font-weight: 400;">Any suspension, termination, or cancellation will not affect your obligations to RISE Asthma under these Terms which by their nature are intended to survive such suspension, termination, or cancellation. For example, but not by way of limitation, upon any such suspension, termination, or cancellation the provisions of Section 14 (Ownership of Intellectual Property Rights), Section 16 (Third Party Websites; Interest-Based and Other Advertisements), Section 17 (Disclaimer of Warranties), Section 18 (Indemnification), Section 19 (Limitation of Liability), Section 21 (General Terms), Section 22 (Arbitration) and Section 23 (Governing Law and Forum for Disputes) shall survive and remain in full force and effect, but the provisions of Section 15 (Your License to Use the Services) shall be suspended, terminated or cancelled, as the case may be.</span></p>
                        <ol>
                            <li><strong> Ownership of Intellectual Property Rights.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The Services, the Site, and all information and/or content that you see, hear, or otherwise experience on the Site (collectively, &ldquo;</span><strong>Content</strong><span style="font-weight: 400;">&rdquo;) are protected by U.S. and international copyright, trademark, and other laws. We own or have the license to use all of the intellectual property rights relating to RISE Asthma, the Services, the Site, and the Content,</span> <span style="font-weight: 400;">including, without limitation, all intellectual property rights protected as patent pending or patented inventions, trade secrets, copyrights, trademarks, service marks, trade dress, or proprietary or confidential information, and whether or not they happened to be registered. You will not acquire any intellectual property rights in RISE Asthma by your use of the Services or the Site.</span></p>
                        <ol start="2">
                            <li><strong> Your License to Use the Services.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">When you use our Services or Site you may access intellectual property rights that we or our licensors own or license. Subject to your compliance with the terms and conditions of these Terms, RISE Asthma grants you a limited, non-exclusive, non-transferable and revocable license, without the right to sublicense, to access and use the Services and to download and print any Content provided by RISE Asthma solely for your personal and non-commercial purposes. You may not use, copy, adapt, modify, prepare derivative works based upon, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Services or Content, except as expressly permitted in these Terms, without RISE Asthma&rsquo;s express prior written consent. No licenses or rights are granted to you by implication or otherwise under any intellectual property rights owned or controlled by RISE Asthma or its licensors, except for the licenses and rights expressly granted in these Terms. Unless otherwise expressly agreed in writing by RISE Asthma, the Services are only permitted to be used within the United States of America.</span></p>
                        <ol start="3">
                            <li><strong> Third-Party Websites; Advertisements.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We may make available, on our Site and as part of our Services, links to third-party websites or resources from third parties on the Site.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma is not responsible or liable for the availability or accuracy of, and RISE Asthma does not endorse, sponsor, or recommend such websites or resources, or the content, products, or services on or available from such websites or resources. When we make available such third-party links or resources on the Site or through the Services, you must look solely to the third party with respect to the content, products, or services they provide. We do not endorse and are not responsible for any of the content, products, or services provided by others. YOUR USE OF THE WEBSITES OR RESOURCES OF THIRD PARTIES IS AT YOUR OWN RISK. RISE Asthma AND ITS AFFILIATES WILL NOT BE LIABLE FOR ANY OF YOUR LOSSES ARISING OUT OF OR RELATING TO THE WEBSITES OR RESOURCES OF THIRD PARTIES.</span></p>
                        <ol start="4">
                            <li><strong> Disclaimer of Warranties.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">Your use of the Services and Content is at your sole discretion and risk. The Services and Content, and all materials, information, products and services included therein, are provided on an &ldquo;AS IS&rdquo; and &ldquo;AS AVAILABLE&rdquo; basis without warranties of any kind.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma AND ITS LICENSORS AND AFFILIATES EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED, OR STATUTORY, RELATING TO THE SERVICES AND CONTENT, INCLUDING WITHOUT LIMITATION THE WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF PROPRIETARY RIGHTS, COURSE OF DEALING, OR COURSE OF PERFORMANCE. RISE Asthma AND ITS LICENSORS AND AFFILIATES MAKE NO WARRANTY THAT THE CONTENT YOU ACCESS ON OUR WEBSITE OR USING OUR SERVICE SATISFIES THE LAWS AND REGULATIONS REQUIRING THE DISCLOSURE OF INFORMATION FOR PRESCRIPTION DRUGS.</span></p>
                        <p><span style="font-weight: 400;">IN ADDITION, RISE Asthma AND ITS LICENSORS AND AFFILIATES DISCLAIM ANY WARRANTIES REGARDING SECURITY, ACCURACY, RELIABILITY TIMELINESS AND PERFORMANCE OF THE SERVICES OR THAT THE SERVICES WILL BE ERROR FREE OR THAT ANY ERRORS WILL BE CORRECTED. NO ADVICE OR INFORMATION PROVIDED TO YOU BY RISE Asthma WILL CREATE ANY WARRANTY THAT IS NOT EXPRESSLY STATED IN THESE TERMS OF SERVICE.</span></p>
                        <p><span style="font-weight: 400;">WE MAKE NO REPRESENTATIONS CONCERNING, AND DO NOT GUARANTEE, THE ACCURACY OF THE SERVICES, INCLUDING, BUT NOT LIMITED TO, ANY INFORMATION PROVIDED THROUGH THE SERVICES OR THEIR APPLICABILITY TO YOUR INDIVIDUAL CIRCUMSTANCES. OUR SERVICES AND SITE CONTENT ARE DEVELOPED FOR USE IN THE UNITED STATES AND RISE Asthma AND ITS LICENSORS AND AFFILIATES MAKE NO REPRESENTATION OR WARRANTY CONCERNING THE SERVICES OR SITE CONTENT WHEN THEY ARE USED IN ANY OTHER COUNTRY.</span></p>
                        <p><span style="font-weight: 400;">SOME JURISDICTIONS DO NOT PERMIT US TO EXCLUDE WARRANTIES IN THESE WAYS, SO IT IS POSSIBLE THAT THESE EXCLUSIONS WILL NOT APPLY TO OUR AGREEMENT WITH YOU. IN SUCH EVENT THE EXCLUSIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAW.</span></p>
                        <ol start="5">
                            <li><strong> Indemnification.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">You will indemnify, defend, and hold harmless RISE Asthma, RISE Asthma&rsquo;s licensors and affiliates and our respective directors, officers, employees, contractors, agents and representatives, from and against any and all claims, causes of action, demands, liabilities, losses, costs or expenses (including, but not limited to, reasonable attorneys&rsquo; fees and expenses) arising out of or relating to any of the following matters:</span></p>
                        <p><span style="font-weight: 400;">(a) your access to or use of the Services, the Site, or the Content;</span></p>
                        <p><span style="font-weight: 400;">(b) your violation of any of the provisions of these Terms of Service;</span></p>
                        <p><span style="font-weight: 400;">(c) any activity related to your Account by you or any other person accessing the Site or Services through your account, including, without limitation, negligent or wrongful conduct; or</span></p>
                        <p><span style="font-weight: 400;">(d) your violation of any third party right, including ,without limitation, any intellectual property right, publicity, confidentiality, property or privacy right.</span></p>
                        <p><span style="font-weight: 400;">RISE Asthma reserves the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will cooperate with us in asserting any available defenses.</span></p>
                        <ol start="6">
                            <li><strong> Limitation of Liability.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">IN NO EVENT WILL RISE Asthma OR RISE Asthma&rsquo;S LICENSORS OR AFFILIATES BE LIABLE TO YOU FOR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES, OR LOST PROFITS, ARISING OUT OF OR IN CONNECTION WITH YOUR USE OF THE SERVICES, THE SITE, OR THE CONTENT, WHETHER THE DAMAGES ARE FORESEEABLE AND WHETHER OR NOT RISE Asthma HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES IN ADVANCE. IF YOU ARE DISSATISFIED WITH THE SERVICES, THE SITE OR THE CONTENT, OR THE TERMS, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</span></p>
                        <p><span style="font-weight: 400;">NOTHING HEREIN SHALL LIMIT THE POTENTIAL PROFESSIONAL LIABILITY OF TREATMENT PROVIDERS OR OTHER LICENSED HEALTHCARE PROFESSIONALS ARISING FROM OR RELATED TO MEDICAL OR MENTAL HEALTH ADVICE, DIAGNOSIS, OR TREATMENT THEY PROVIDE TO YOU, EXCEPT AS PROVIDED UNDER APPLICABLE STATE OR LOCAL LAWS. IN MANY JURISDICTIONS, MEDICAL PROVIDERS ARE REQUIRED TO REPORT CONFIDENTIAL INFORMATION IF THEY HAVE REASON TO BELIEVE THAT A PATIENT IS LIKELY TO HARM OTHERS OR HIMSELF/HERSELF. IN NO EVENT SHALL RISE Asthma BE LIABLE FOR THE DISCLOSURE OF YOUR CONFIDENTIAL INFORMATION BY A TREATMENT PROVIDER FROM WHOM YOU RECEIVE MENTAL HEALTH SERVICES. RISE Asthma IS NOT LIABLE TO ANY PERSON OR USER FOR ANY HARM CAUSED BY THE NEGLIGENCE OR MISCONDUCT OF A MEDICAL PROVIDER PROVIDING SERVICES. IN NO EVENT WILL THE CUMULATIVE LIABILITY OF RISE Asthma OR RISE Asthma&rsquo;S LICENSORS OR AFFILIATES TO YOU, WHETHER IN CONTRACT, TORT, OR OTHERWISE, EXCEED $1,000.</span></p>
                        <p><span style="font-weight: 400;">EXCEPT AS OTHERWISE REQUIRED BY APPLICABLE LAW, ANY CLAIM OR CAUSE OF ACTION ARISING OUT OF OR RELATING TO YOUR USE OF THE SERVICES, THE SITE OR THE CONTENT OR OUR RELATIONSHIP WITH YOU, REGARDLESS OF THEORY, MUST BE BROUGHT WITHIN ONE (1) YEAR AFTER THE OCCURRENCE OF THE EVENT GIVING RISE TO THE CLAIM OR CAUSE OF ACTION OR BE FOREVER BARRED.</span></p>
                        <p><span style="font-weight: 400;">SOME JURISDICTIONS DO NOT PERMIT US TO LIMIT OUR LIABILITY IN THESE WAYS, SO IT IS POSSIBLE THAT THESE LIMITATIONS WILL NOT APPLY TO OUR AGREEMENT WITH YOU. IN SUCH EVENT THE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAW.</span></p>
                        <ol start="7">
                            <li><strong> Errors and Inaccuracies.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">The information on the Site including, without limitation, information regarding pricing, may contain typographical errors or other errors or inaccuracies, and may not be complete or current. RISE Asthma reserves the right to correct any errors, inaccuracies, or omissions and to change or update information at any time without prior notice to you. RISE Asthma will not, however, guarantee that any such errors, inaccuracies, or omissions will be corrected. RISE Asthma reserves the right to refuse to fill any orders or provide Services that are based on inaccurate or erroneous information on the Site, including, without limitation, incorrect or out-of-date information regarding pricing, payment terms, or for any other lawful reason.</span></p>
                        <ol start="8">
                            <li><strong> General Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">These Terms of Service constitute the entire agreement between you and us relating to our Services, the Site, and the Content, replacing any prior or contemporaneous agreements, whether written or oral, unless you have signed a separate written agreement with us relating to our Services, the Site, or the Content. If there is any conflict between the Terms and a separate signed written agreement between you and us relating to our Services, the Site, or the Content, the signed written agreement will control. Only the executive officers of RISE Asthma have the authority to sign a separate signed written agreement between you and us.</span></p>
                        <p><span style="font-weight: 400;">Our licensors may be entitled to enforce this agreement as third-party beneficiaries. There are no other third-party beneficiaries to this agreement.</span></p>
                        <p><span style="font-weight: 400;">The failure by you or us to enforce any provision of the Terms will not constitute a waiver. If any court of law, having the jurisdiction to decide the matter, rules that any provision of the Terms is invalid or unenforceable, then the invalid or unenforceable provision shall be removed from the Terms or reformed by the court and given effect so as to best accomplish the essential purpose of the invalid or unenforceable provision, and all of the other provisions of the Terms shall continue to be valid and enforceable. Nothing contained in these Terms of Service shall limit the ability of a party to seek an injunction or other equitable relief without posting any bond. The titles of the Sections of the Terms are for convenience only and shall have no legal or contractual effect.</span></p>
                        <ol start="9">
                            <li><strong> Arbitration.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">For any claim (excluding claims for injunctive or other equitable relief) where the total amount of the award sought is less than $10,000.00 USD, the party requesting relief may elect to resolve the dispute in a cost effective manner through binding non-appearance-based arbitration. If a party elects arbitration, that party will initiate such arbitration through JAMS. The parties must comply with the following rules:</span></p>
                        <p><span style="font-weight: 400;">(a) the arbitrator shall be selected from JAMS and the arbitration shall be conducted in accordance with JAMS&rsquo; Comprehensive Arbitration Rules and Procedures, except as otherwise specified below;</span></p>
                        <p><span style="font-weight: 400;">(b) the arbitration shall be conducted by telephone, online and/or be solely based on written submissions, the specific manner shall be chosen by the party initiating the arbitration;</span></p>
                        <p><span style="font-weight: 400;">(b) the arbitration shall not involve any personal appearance by the parties or witnesses unless otherwise mutually agreed by the parties; and</span></p>
                        <p><span style="font-weight: 400;">(c) any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction.</span></p>
                        <p><span style="font-weight: 400;">Any dispute, controversy, or disagreement arising out of or relating to these Terms, the breach thereof, or the subject matter thereof, where the total amount of the award sought is $10,000.00 USD or greater, shall be settled exclusively by binding arbitration. The arbitrator shall be selected from JAMS and the arbitration shall be conducted in accordance with JAMS&rsquo; Comprehensive Arbitration Rules and Procedures The arbitration shall be held in the County of Santa Clara, California, unless the parties mutually agree to have such proceeding in some other locale. To the extent of the subject matter of the arbitration, the arbitration shall be binding not only on all parties to these Terms, but on any other entity controlled by, in control of or under common control with the party to the extent that such affiliate joins in the arbitration, and judgment on the award rendered by the arbitrator may be entered in any court having jurisdiction thereof.</span></p>
                        <ol>
                            <li><strong> Governing Law and Forum for Disputes.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">THESE TERMS OF SERVICE AND OUR RELATIONSHIP WITH YOU SHALL BE GOVERNED BY THE LAWS OF THE STATE OF DELAWARE, EXCLUDING ITS CHOICE OF LAWS RULES. YOU AND RISE Asthma EACH IRREVOCABLY AGREES THAT THE EXCLUSIVE VENUE FOR ANY ACTION OR PROCEEDING ARISING OUT OF OR RELATING TO THESE TERMS OF SERVICE OR OUR RELATIONSHIP WITH YOU, REGARDLESS OF THEORY, SHALL BE THE U.S. DISTRICT COURT FOR THE NORTHERN DISTRICT OF CALIFORNIA, OR THE STATE COURTS LOCATED IN SANTA CLARA COUNTY, CALIFORNIA. YOU AND RISE Asthma EACH IRREVOCABLY CONSENTS TO THE PERSONAL JURISDICTION OF THESE COURTS AND WAIVES ANY AND ALL OBJECTIONS TO THE EXERCISE OF JURISDICTION BY THESE COURTS AND TO THIS VENUE. NOTWITHSTANDING THE FOREGOING, HOWEVER, YOU AND RISE Asthma AGREE THAT RISE Asthma MAY COMMENCE AND MAINTAIN AN ACTION OR PROCEEDING SEEKING INJUNCTIVE OR OTHER EQUITABLE RELIEF IN ANY COURT OF COMPETENT JURISDICTION.</span></p>
                        <ol>
                            <li><strong> Changes to These Terms.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We reserve the right to change our Terms at any time. Any changes that we make will become a part of our agreement with you when they are posted to our Site. Your continued use of our Services or the Site will constitute your agreement to the changes we have made. The last date these Terms were revised is set forth at the end of this document.</span></p>
                        <ol start="2">
                            <li><strong> Contacting Us.</strong></li>
                        </ol>
                        <p><span style="font-weight: 400;">We encourage you to contact us at </span><a href="mailto:hope@RISEAsthma.com"><span style="font-weight: 400;">hope@RISEAsthma.com</span></a><span style="font-weight: 400;">. </span><span style="font-weight: 400;">if you have any questions concerning our Terms. Please note that email communications will not necessarily be secure; accordingly, you should not include credit card information or other sensitive information in your email correspondence with us. If you would like to contact us via physical mail, our mailing address is: RISE Asthma, __________________________</span></p>
                        <p><span style="font-weight: 400;">Last Revised: June 12, 2017</span></p>
                        <p><span style="font-weight: 400;">Site copyright 2017 RISE Asthma unless otherwise noted. All rights reserved</span></p>
                        <p>&nbsp;</p>
                        <p><br /><br /></p>
                    </div>
                </div>
            </div>
        </div>


        <!--   Big container   -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form method="post" action="intro.php" id="payment-form">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->
                                <div class="wizard-header">
                                    <div class="wizard-navigation">
                                        <div class="progress-with-circle">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                        </div>
                                        <ul>
                                            <li>
                                                <a href="#captain" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-list"></i>
                                                    </div>

                                                </a>
                                            </li>
                                            <li>
                                                <a href="#details" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-briefcase"></i>
                                                    </div>

                                                </a>
                                            </li>
                                            <li>
                                                <a href="#description" id="disbaleClick" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-pencil"></i>
                                                    </div>

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="tab-content">
                                    <div class="tab-pane" id="captain">

                                        <h2 class="info-text">Welcome to RISE</h2>
                                        <h4 class="info-text">Who needs help?</h4>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <div class="col-sm-4 col-sm-offset-2">
                                                    <div class="choice" data-toggle="wizard-checkbox">
                                                        <input type="checkbox" name="jobb" value="Design">
                                                        <div class="btn-next card card-checkboxes card-hover-effect">

                                                            <p>Myself</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="choice" data-toggle="wizard-checkbox">
                                                        <input type="checkbox" name="jobb" value="Design">
                                                        <div class="btn-next card card-checkboxes card-hover-effect">
                                                            <p>A loved one</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="info-text"> Your RISE profile</h2>
                                            </div>
                                            <div class="col-sm-10 col-sm-offset-1">
                                                <div class="form-group">
                                                    <label>WHAT IS YOUR NAME?</label>
                                                    <input type="text" class="form-control" id="inputname" name="inputname" required="true" placeholder="e.g John Doe" id="next">
                                                     <label id="name-error" class="error" for="email"></label>
                                                </div>
                                                <div class="form-group">
                                                    <label>WHAT IS YOUR EMAIL ADDRESS?</label>
                                                    <input name="email" type="email" class="form-control" placeholder="john@gmail.com" id="email">
                                                     <label id="email-error" class="error" for="email"></label>
                                                </div>
                                                <div class="form-button" style="max-width: 200px;margin: 0 auto;">
                                                    <input type="button" name="next" class='btn-next btn btn-fill btn-warning btn-wd' value="Next" onclick="return validateEmail();" />
                                                 </div>
                                                <!-- <div class="form-button" style="max-width: 200px;margin: 0 auto;">
                                                    <input type='button' class='btn-next btn btn-fill btn-warning btn-wd' name='next' value='Next' onclick="return validateEmail();" />
                                                   
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <h2 class="info-text">Last step - secure payment </h2>
                                            <h5 class="info-text">We charge $49 and offer lifetime access <br/>to all the RISE resources within our platform</h5>
                                            <div class="col-sm-6 col-sm-offset-1">
                                                <div class="paymentwrapper">
                                                    <img class="paymentmethod" src="assets/img/creditcard.png">
                                                    <div class="col-sm-12 form-fields">
                                                        <input type="text" name="stripe_number" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onfocus="" placeholder="Card number." class="form-control" style="background-color: white; " onkeypress='' id="credit_card">
                                                    </div>

                                                    <div class="col-sm-6 form-fields paymentmethod-expirydate">
                                                        <input old_val="2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" name="stripe_expirydate" placeholder="MM/YY" class="form-control expirydate" maxlength="5" id="expirydate" onkeypress='return validateExpire(event);' style="background-color: white;">
                                                    </div>

                                                    <div class="col-sm-6 form-fields paymentmethod-creditvalidation">
                                                        <input type="text" name="stripe_cvc" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="CVV" class="form-control credit-validation " onkeypress='return validateExpire(event);' maxlength="3" minlength="3" style="background-color: white;">
                                                    </div>

                                                    <div id="card-errors" class="error">


                                                        <label id="credit_card-error" class="error col-sm-12" for="credit_card"></label>

                                                        <label id="expirydate-error" class="error col-sm-12" for="credit_card"></label>

                                                        <label id="cvv-error" class="error col-sm-12" for="credit_card"></label>


                                                    </div>
                                                    <div class="form-button">
                                                        <button type="submit" name="paymentwith" value="stripes" class="btn btn-fill btn-warning btn-wd" id="proceed">Pay $49</button><br/><br/>
                                                        <div align="center"><b>Or</b></div><br/>
                                                        <button name="paymentwith" id="paypal" type="button" value="paypal" class="btn btn-fill2 btn-warning btn-wd">Pay with <strong><em>PayPal</em></strong></button>
                                                    </div>
                                                    <div class="terms">
                                                        <h5 class="info-text">By proceeding, you are agree with our
                                                            <a href="#terms-modal" data-toggle="modal"> terms and conditions</a></h5>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 col-sm-offset-1">
                                                <div class="form-group">
                                                    <img src="assets/img/secure.png">
                                                    <p class="description"></p>
                                                </div>
                                                <div class="form-group">
                                                    <label>360 days refund policy</label>
                                                    <p class="description">If at any point you think that the program is not useful for you, we will refund the entire amount. No questions asked.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>You cannot afford $49?</label>
                                                    <p class="description">Send us a message at hope@riseasthma.com, briefly describe your situation to us and we will give you access for free.</p>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </form>
                            <div class="wizard-footer col-md-4 col-md-offset-4">
                                <div class="clearfix"></div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- wizard container -->
            </div>
        </div>
        <!-- row -->
        <div class="footer">
            <div class="container text-center">
                Already have an account? Sign in <a href="<?php echo SITE_URL; ?>page_login.php">here.</a>
            </div>
            <div>
                <a href="https://www.riseasthma.com/"><img src="assets/img/logo.png" class="img-responsive logo-footer"></a>
            </div>
        </div>
    </div>
    <!--  big container -->


    </div>


    <!--   Core JS Files   -->
    <script src="https://js.stripe.com/v2/"></script>
    <!-- <script src="https://js.stripe.com/v3/"></script> -->

    <style type="text/css">
        .field {
            background: white;
            box-sizing: border-box;
            font-weight: 400;
            border: 1px solid #CFD7DF;
            border-radius: 3px;
            color: #32315E;
            outline: none;
            height: 48px;
            line-height: 48px;
            padding: 0 20px;
            cursor: text;
            width: 76%;
        }

        .field::-webkit-input-placeholder {
            color: #CFD7DF;
        }

        .field::-moz-placeholder {
            color: #CFD7DF;
        }

        .field:-ms-input-placeholder {
            color: #CFD7DF;
        }

        .field:focus,
        .field.StripeElement--focus {
            border-color: #F99A52;
        }

    </style>
  
    <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
   
       <script type="text/javascript">

        function validateEmail() {

            var txt_name = document.getElementById('inputname').value;
            var txt_email = document.getElementById('email').value;

            if(txt_name == '')
            {
                //alert("Please enter your name");
                $("#name-error").html('Please enter your name');
                $("#name-error").show();
                return false;
            }
            if(txt_email == '')
            {
                // alert("Please enter your email address");
                $("#email-error").html('Please enter your email address');
                $("#email-error").show();
                return false;
            }


            $.ajax({
            type: "POST",
            dataType: "html",
            url: "emailcheck.php",
            data: {
                email_val: txt_email
            },
            success: function(result) { 
                
                if(result == 'false')
                {

                   // alert("You have already register with this address!");
                    $("#email-error").html('You have already register with this address!');
                    $("#email-error").show();
                    $("#details").show();
                    $('.btn-next').show();
                    $( ".btn-next" ).removeClass( "disabled" );
                    $("#description").hide();
                    return false;

                }
                else
                {
                    $("#description").show();
                     $("#details").hide();
                }
            }

            });
        }

    </script>

    <script type="text/javascript">
       // Stripe.setPublishableKey('<?php //echo STRIPE_PUBLIC_KEY;?>');
       Stripe.setPublishableKey('pk_test_pYpB3emxooq0Dwh5HGJ3MqUL');
        var stripeResponseHandler = function(status, response) {
            var $form = $('#payment-form');
            if (response.error) {
                $("#card-errors").show();

                $("#credit_card-error").html('Please Enter Correct Detail!');
                $("#credit_card-error").show();
                $form.find('button').prop('disabled', false);
            } else {

                var token = response.id;
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                $("#credit_card-error").html('');
                $("#credit_card-error").hide();
                $form.get(0).submit();
            }
        };

        function validateNumber(number) {

            error = true
            number = number.trim();
            var count = number.length;
           
            if (count == 19) {
                sp = number.split(" ").length - 1;
                if (sp == 3) {
                    error = false;
                }
            }
            if (count == 16) {
                sp = number.split(" ").length - 1;
                if (sp == 0) {
                    error = false;
                }
            }

            if (error) {
                $("#credit_card-error").html('Please Enter Valid Card Number!');
                $("#credit_card-error").show();
            } else {
                $("#credit_card-error").html('');
                $("#credit_card-error").hide();
            }

            return error;
        }

        function validateExpiry(expiry) {
            error = false
            res = '';
            count = expiry.length;

            if (count == 5) {
                sp = expiry.split("/").length - 1;
                if (sp != 1) {
                    error = true;
                    res = ' lenght ';
                }
            } else {
                error = true;
                res = ' lenght2 ';
            }
            if (!error) {

                var $split = expiry.split("/");

                if ($split[0].length != 2 || $split[0] > 12) {
                    error = true;
                    res = ' lenght3 ';
                }

                if (!error) {
                    var year = new Date().getFullYear().toString().substr(-2);
                    if ($split[1] < year) {
                        error = true;

                        res = ' lenght5 ';
                    }
                }
            }
            if (error) {
                $("#expirydate-error").html('Please enter valid Expiry Date!');
                $("#expirydate-error").show();
            } else {
                $("#expirydate-error").html('');
                $("#expirydate-error").hide();
            }

            return error;
        }

        function validateCVV(cvv) {

            error = false

            count = cvv.length;
            if (count != 3) {
                error = true
            }

            if (error) {
                $("#cvv-error").html('Please enter valid CVV !');
                $("#cvv-error").show();
            } else {
                $("#cvv-error").html('');
                $("#cvv-error").hide();
            }
            return error;

        }
        jQuery(function($) {
            $('#payment-form').submit(function(e) {

                if ($('#payment-form').attr('action') == 'intro.php') {
                    var $form = $(this);
                    var expiry = $("#payment-form :input[name='stripe_expirydate']").val()
                    var stripe = expiry.split("/");
                    var cvv = $("#payment-form :input[name='stripe_cvc']").val();

                    error = false;
                    //$("#card-errors").hide();

                    number = $("#payment-form :input[name='stripe_number']").val();

                    errorNumber = validateNumber(number);
                    errorExpiry = validateExpiry(expiry);
                    errorCvv = validateCVV(cvv);


                    if (errorNumber || errorExpiry || errorCvv) {
                        $("#card-errors").show();
                        return false
                    } else {

                        $("#card-errors").hide();
                        $form.find('button').prop('disabled', true);
                        Stripe.card.createToken({
                            number: $("#payment-form :input[name='stripe_number']").val(), //values['stripe_number'],
                            cvc: $("#payment-form :input[name='stripe_cvc']").val(),
                            exp_month: stripe[0],
                            exp_year: stripe[1]
                        }, stripeResponseHandler);
                        return false;
                    }

                }
            });
        });

        function validateExpire(event) {
            //var key = window.event ? event.keyCode : event.which;
            var key = event.keyCode || event.which;
            if (event.keyCode == 8 || event.keyCode == 46 ||
                event.keyCode == 37 || event.keyCode == 39) {
                return true;
            } else if (key > 57) {
                return false;
            } else return true;
        };
        /*
        function validateCvv(event) {
            var key = event.keyCode || event.which;
            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key > 57 || key == 191 ) {
                return false;
            }
            else return true;
        };*/

        function validateCcnumber(event) {
            var key = event.keyCode || event.which;

            if (event.keyCode == 8 || event.keyCode == 46 ||
                event.keyCode == 37 || event.keyCode == 39) {
                return true;
            } else if (key > 57) {
                return false;
            } else {
                return true;
            }
        };
        $('document').ready(function() {
            $("#paypal").click(function() {
                var datastring = $("#payment-form").serialize();
                $('#payment-form').attr('action', "paypal.php");
                $("#payment-form").submit();
            });


            $(document).on('blur', "#payment-form :input[name='stripe_expirydate']", function(e) {
                validateExpiry($(this).val())
            })
            $(document).on('keyup', "#payment-form :input[name='stripe_expirydate']", function(e) {

                oldVal = $(this).attr('old_val');

                oldCount = oldVal.length;
                var count = $(this).val().length;

                if (oldCount > count) {
                    $(this).attr('old_val', $(this).val());
                    return
                }


                val = $(this).val().replace(/[^0-9/]/g, '');

                if (!($(this).val() == val)) {
                    $(this).val(val)
                    $(this).attr('old_val', val);
                    return
                }

                if (count > 5) {
                    $(this).attr('old_val', $(this).val().slice(0, 5));
                    $(this).focus().val($(this).val().slice(0, 5))
                }


                if ((parseInt($(this).val()) > 12 || $(this).val() == 00) && count == 2) {
                    $("#card-errors").show();
                    $("#expirydate-error").html('Please Enter Valid Month');
                    $("#expirydate-error").show();

                    $(this).attr('old_val', $(this).val());
                    $(this).focus()
                } else if (count == 2) { // && e.keyCode != 8){

                    if ($(this).val().indexOf('/') < 0) {
                        $(this).attr('old_val', $(this).val() + "/");

                        $(this).focus().val(function(index, value) {
                            return value + "/";
                        });
                    } else {
                        $(this).attr('old_val', "0" + $(this).val());
                        $(this).focus().val(function(index, value) {
                            return "0" + value;
                        });
                    }
                    $("#expirydate-error").html('');
                    $("#expirydate-error").hide();
                } else {
                    $(this).attr('old_val', $(this).val());

                    if ($(this).val().indexOf('/')) {
                        var $split = $(this).val().split("/");
                        var year = new Date().getFullYear().toString().substr(-2);

                        if (count == 5 && $split[1] < year) {
                            $(this).focus()
                            $("#card-errors").show();
                            $("#expirydate-error").html('Please Enter Valid Year');
                            $("#expirydate-error").show();
                        } else {
                            $("#expirydate").html('');
                            $("#expirydate-error").hide()
                        }
                    }
                }

            });



            $(document).on('blur', "#payment-form :input[name='stripe_number']", function(e) {
               
                $(this).val($(this).val().trim())
                validateNumber($(this).val())
            })
            $(document).on('keyup', "#payment-form :input[name='stripe_number']", function(e) {

                var count = $(this).val().length;
                var key = e.keyCode || e.which;

                aa = $(this).val();
                val = aa.replace(/[^0-9 ]/g, '');
                count2 = val.length;
                /*if(count2 > 16){*/
                val = val.slice(0, 19)
                //}
                count2 = val.length;


                if (aa !== val) {
                    
                    valTmp = $(this).val().replace(/[^0-9]/g, '');
                    c3 = valTmp.length;
                    if (c3 > 16) {
                        valTmp = valTmp.slice(0, 16)
                        $(this).val(function(index, value) {
                            valTmp = valTmp.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            valTmp = valTmp.trim();
                            return valTmp;
                        });
                    } else {
                        return $(this).val(val);
                    }
                }
                if (count2 < 19) {
                        
                    if (count2 % 5 == 0) {
                        $(this).val(function(index, value) {
                            return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            val = val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                            val = val.trim();
                            return val;
                        });
                    }
                }


                /*if(count2 < 19 && key != 8 ){ 
                    if(count % 5 == 0){ 
                        $(this).val(function (index, value) {
                            return val.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                        });
                    }
                }*/
            });

            $(document).on('keyup', "#payment-form :input[name='stripe_cvc']", function(e) {



                var count = $(this).val().length;
                val = $(this).val().replace(/[^0-9/]/g, '');

                if (!($(this).val() == val)) {
                    $(this).val(val)
                    return
                }

                if (count > 3) {
                    $(this).focus().val($(this).val().slice(0, 3))
                }

            })
            $(document).on('blur', "#payment-form :input[name='stripe_cvc']", function(e) {
                validateCVV($(this).val())
            })

            // var input_field = document.getElementById('credit_card');
            // input_field.addEventListener('textInput', function(e) {
            //     // e.data will be the 1:1 input you done
            //     var char = e.data; // In our example = "a"
            //     alert(char);

            //     // If you want the keyCode..
            //     var keyCode = char.charCodeAt(0); // a = 97
            //     alert(keyCode);

            //     // Stop processing if "a" is pressed
            //     if (keyCode == 97) {
            //         e.preventDefault();
            //         return false;
            //     }
            //     return true;
            // });

            /*  $("#payment-form :input[name='stripe_number']").on('keypress change', function () {
                  var count = $(this).val().length;
                  if(count < 19){
                      $(this).val(function (index, value) {
                          return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                      });
                  }
              }); */
            /*$("#payment-form :input[name='stripe_expirydate']").focusout(function(){
               if($(this).val().indexOf('/')){
                    var $split = $(this).val().split("/");
                    var year = new Date().getFullYear().toString().substr(-2);
                    if($split[1] < year){
                        $("#card-errors").show();
                        $("#card-errors").html('Please Enter Valid Year');
                    }else{
                        $("#card-errors").hide();
                    }
                }
            });*/
        });

    </script>
 <!--    <script>
window.$crisp=[];window.CRISP_WEBSITE_ID="f3895f87-8f79-43d8-81b4-f4f8f59de2d4";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();

</script>
 -->
</body>

</html>
