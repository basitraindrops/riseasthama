<?php include 'inc/config.php'; // Configuration php file ?>
<?php
require(INC_DIR.'init.php');

require(CLASS_DIR.'user.class.php');
require(CLASS_DIR.'encryption.class.php');
require_once(CLASS_DIR.'security.class.php');

?>

<?php

?>
<?php include INC_DIR.'top.php'; ?>
<?php// include INC_DIR.'nav.php'; ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="<?php echo SITE_URL;?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo SITE_URL;?>register_investor.php">Register as Investor</a></li>
        <li>Registration Success</li>
    </ul>
    <!-- END Navigation info -->

	<div align="center">
		<i class="fa fa-check"></i>Terima kasih atas pendaftaran Anda sebagai investor. Aplikasi Anda akan diproses dalam waktu 3 hari kerja.
	</div>
<?php
			
//include INC_DIR.'footer.php'; // Footer and scripts ?>


<?php include INC_DIR.'bottom.php'; // Close body and html tags ?>