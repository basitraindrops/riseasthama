<div class="content">

    <div class="faq_search">
        <i class="fa fa-search" aria-hidden="true"></i>
        <input type="text" name="search" id="txt_search" placeholder="Search">
         <input type="hidden" name="id" id="f_id" value=<?php echo $_REQUEST[ 'id']; ?> >

    </div>
    <div class="faq_breadcum">
        <a href="faq.php" class="all_faq">All Topics</a> >
        <a href="faq_detail.php?id=<?php echo $d[0]->faq_cat_id; ?>" class="child_faq1">
            <?php echo $d[0]->faq_cat_name; ?> </a> > <a href="" class="child_faq"><?php echo $data[0]->faq_cat_name; ?></a>
    </div>
    <div class="faq_main" id="default">

        <div style="width:100%">


            <div>
                <h2>
                    <?php echo $data[0]->faq_cat_name; ?>
                </h2>
            </div>
            <div>
                
                    <p><?php echo htmlspecialchars_decode($data[0]->faq_cat_des); ?></p>
                    <?php //echo  htmlspecialchars_decode($data[0]->faq_cat_des); ?>
                    
            </div>
           <!-- <img class="img-responsive" src="assets/img/introduction/3.png">-->
        </div>
    </div>

</div>

<script>
    $('#txt_search').change(function() {
        var txt = document.getElementById('txt_search').value;
        var id = document.getElementById('f_id').value;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "search_faq_detail.php",
            data: {
                txt_val_detail: txt,
                faq_id: id
            },
            success: function(result) {
                var htmlText = '';
                htmlText += '<div style="width:100%;">';
                if (result == "" || result == 'undefined' || result == undefined) 
                {
                    htmlText += '<p>No result found!</p>';
                }
                else
                {
                    for (var key in result) {

                         htmlText += ' <div> <h4> ' + result[key].name + '</h4></div>';
                         htmlText += '<div><p> ' + result[key].des + '</p></div>';
                      
                     }
                   
                }
                htmlText += '</div>';
               
                //$('#default').html('');
                $('#default').html(htmlText);
            }
        });
    });

</script>
