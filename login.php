<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <title>RISE Asthma Method</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
    <div class="image-container set-full-height" style="background-image: url('assets/img/loginbackground.png')">
        <!--   Creative Tim Branding   -->

        <div class="logo-container">
            <div class="logo">

            </div>

        </div>


        <!--   Big container   -->
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <!--      Wizard container        -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="azure" id="wizard">
                            <form action="index.php" method="">
                                <!--        You can switch " data-color="azure" "  with one of the next bright colors: "blue", "green", "orange", "red"           -->

                                <div class="wizard-header">

                                    <div class="wizard-navigation">
                                        <div class="progress-with-circle">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                        </div>
                                        <ul>

                                            <li>
                                                <a href="#details" data-toggle="tab">
                                                    <div class="icon-circle">
                                                        <i class="ti-briefcase"></i>
                                                    </div>

                                                </a>
                                            </li>

                                        </ul>

                                    </div>

                                </div>


                                <div class="tab-content">


                                    <div class="tab-pane" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="info-text">Welcome back to RISE</h2>
                                            </div>
                                            <div class="col-sm-10 col-sm-offset-1">

                                                <div class="form-group">
                                                    <label>E-MAIL ADDRESS</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="e.g john@gmail.com">
                                                    <label>PASSWORD</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter password here">
                                                    <div class="footer text-center">
                                                        <button type="submit" class="btn btn-fill btn-warning btn-wd">Login</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>



                                </div>


                            </form>
                        </div>
                    </div>
                    <!-- wizard container -->
                </div>
            </div>
            <!-- row -->
        </div>
        <!--  big container -->

        <div class="footer">
            <div class="container text-center">
                Don't have an account? Sign up <a href="http://www.creative-tim.com/product/paper-bootstrap-wizard">here.</a>
            </div>
            <div>
                <img src="assets/img/logo.png" class="img-responsive logo-footer">
            </div>
        </div>
    </div>

</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>

</html>
